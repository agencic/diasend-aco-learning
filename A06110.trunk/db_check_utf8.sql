# Script to check if all relations are in place. 
#
select count(*) from deletion_log left join users on deletion_log.pcode = users.pcode where users.pcode is NULL;
select count(*) from device_ownership left join users on device_ownership.pcode = users.pcode where users.pcode is NULL;
select count(*) from dibs_transactions left join users on dibs_transactions.pcode = users.pcode where users.pcode is NULL;
select count(*) from food_collection_owner_patient left join users on food_collection_owner_patient.pcode = users.pcode where users.pcode is NULL;
select count(*) from hba1c left join users on hba1c.pcode = users.pcode where users.pcode is NULL;
select count(*) from lostpassword_keys left join users on lostpassword_keys.pcode = users.pcode where users.pcode is NULL;
select count(*) from moblog left join users on moblog.pcode = users.pcode where users.pcode is NULL;
select count(*) from registration_events left join users on registration_events.pcode = users.pcode where users.pcode is NULL;
select count(*) from user_licensecodes left join users on user_licensecodes.pcode = users.pcode where users.pcode is NULL;
select count(*) from user_ownership left join users on user_ownership.pcode = users.pcode where users.pcode is NULL;
select count(*) from user_properties left join users on user_properties.pcode = users.pcode where users.pcode is NULL;
select count(*) from user_strings left join users on user_strings.pcode = users.pcode where users.pcode is NULL;
select count(*) from admins left join super_admins on admins.super_admin = super_admins.name where super_admins.name is NULL;
select count(*) from food_collection_owner_clinic left join admins on food_collection_owner_clinic.admin = admins.name where admins.name is NULL;
select count(*) from clinic_users left join admins on clinic_users.admin = admins.name where admins.name is NULL;
select count(*) from pdf_profiles left join admins on pdf_profiles.admin = admins.name where admins.name is NULL;
select count(*) from user_ownership left join admins on user_ownership.admin = admins.name where admins.name is NULL;

