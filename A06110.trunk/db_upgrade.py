# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2006-2014 Diasend AB, Sweden, http://www.diasend.com
# -----------------------------------------------------------------------------

import MySQLdb
import sys
import getopt
from datetime import datetime
import random
import time
import re
import warnings

host    = "localhost"
passwd  = "password"
user    = "root"
port    = 3306
db      = "diasend_se"

connection = None

def ConnectToDatabase():
    """
    Establish connection to database
    """
    global connection

    if not connection:
        try:
            connection = MySQLdb.connect( host=host, user=user, passwd=passwd, port=port, db=db )
        except MySQLdb.MySQLError:
            connection = None   # to be sure

    if connection:
        return True
    else:
        return False

def DisconnectFromDatabase():
    """
    Disconnect from database.
    """
    global connection

    if connection:
        connection.commit()
        connection.close()
        connection = None        


def GetLastInsertID():
    """
    Return the ID returned during last insert
    """
    global connection

    if not connection:
        return None

    return connection.insert_id()
            

def PerformQuery( sqlQ, paramQ ):
    """
    Performs an sql query, returns True on success, else False
    """
    global connection

    # Are we conntected?
    if not connection:
        return False

    result = False

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, paramQ )
        result = cursor.fetchall ()
        cursor.close()

    except MySQLdb.MySQLError, msg:
        print msg
        result = False

    return result

def GetDBVersion():
    """
    Check the version of the current database layout
    """
    sqlQ = """
        SELECT * from metadata where `key` = 'db_version'
        """
    paramQ = ()

    _ret = PerformQuery(sqlQ, paramQ)
    
    if _ret == False:
        return None
    else:
        return _ret[0][1]

def SetDbVersion(ver):
    """
    Sets the database version
    """
    sqlQ = """
        DELETE FROM metadata WHERE `key` = 'db_version'
        """
    paramQ = ()
    PerformQuery(sqlQ, paramQ)

    sqlQ = """
        INSERT INTO metadata VALUES ('db_version', '%s')
        """ % (str(ver))

    return PerformQuery(sqlQ, paramQ)

def CreateDevice(r, deviceclass):
    """
    Creates a device entry in the devices table for each entry.
    """
    # Check if the serial number is a NULL char, in that case just return True
    # The db contains a trashy serial, but what shall we do?
    try:
        if r[0] == '' or ord(r[0][0]) == 0:
            return True
    except:
        print "wrong format: " + repr(r)
        return False

    # First check if the device already exists
    sqlQ = """
        SELECT * from devices WHERE serialnumber = %s
        """
    paramQ = (str(r[0]))

    _ret = PerformQuery(sqlQ, paramQ)

    if _ret == False:
        print sqlQ % paramQ
        return False

    # Result empty, insert the device
    if _ret == ():
        sqlQ = """
            INSERT INTO devices (serialnumber, devicetype, deviceclass) VALUES (%s,%s,%s)
            """
        paramQ = (str(r[0]), str(r[1]), deviceclass)

        return PerformQuery(sqlQ, paramQ)
    else:
        # The device already existed, but update it with the device type
        sqlQ = """
            UPDATE devices SET devicetype = %s WHERE serialnumber = %s
            """
        paramQ = (str(r[1]), str(r[0]))

        return PerformQuery(sqlQ, paramQ)


def CreateTerminal(t):
    """
    Creates a terminal entry in the terminals table for each entry.
    """
    # Check if the terminal number is a NULL char, in that case just return True
    # The db contains a trashy serial, but what shall we do?
    try:
        if t[0] == None or t[0] == '' or ord(t[0][0]) == 0:
            return True
    except:
        print "Wrong format: " + repr(t)
        return False

    # First check if the device already exists
    sqlQ = """
        SELECT * from terminals_new WHERE terminal_id = %s
        """
    paramQ = (str(t[0]))

    _ret = PerformQuery(sqlQ, paramQ)

    if _ret == False:
        print sqlQ % paramQ
        return False

    # Result empty, insert the terminal
    if _ret == ():
        sqlQ = """
            INSERT INTO terminals_new (terminal_id) VALUES (%s)
            """
        paramQ = (str(t[0]))

        return PerformQuery(sqlQ, paramQ)

    # Already existing...
    return True


def GetDbElementCached(table, elem, key, key_val, cache):
    
    if cache.has_key(key_val):
        # The value was in the cache
        return cache[key_val]
    
    sqlQ = "SELECT " + elem + " FROM " + table + " WHERE " + key + " = %s"
        
    paramQ = (key_val)

    _r = PerformQuery(sqlQ, paramQ)

    if _r == False:
        return False
    
    try:
        _ret = _r[0][0]
        cache[key_val] = _ret
        return _ret
        
    except:
        print "No key for: '" + key_val + "', did: " + sqlQ % paramQ
        return None


def CopyTelemeddata(term_cache, device_cache):
    """
    Copies everything from telemeddata to telemeddata_new
    """
    global connection

    # Are we conntected?
    if not connection:
        return False

    _ret = True

    sqlQ = "SELECT serialnumber, timestamp, value, ketones, terminal_id FROM telemeddata"

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, () )

    except MySQLdb.MySQLError, msg:
        return False

    while True:
        
        _r = cursor.fetchone()
        
        if _r == None:
            break

        # We now have a row, first check the ID of the terminal and meter
        term_seq = GetDbElementCached("terminals_new", "seq", "terminal_id", _r[4], term_cache)

        # There might be data in the database which is not sent by a sender -> vikindata stuff(?)
        if term_seq == False:
            term_seq = None

        device_seq = GetDbElementCached("devices", "seq", "serialnumber", _r[0], device_cache)

        if device_seq == False:
            print "Failed GetDbElementCached, was looking for device: '" + str(_r[0]) + "'"
            _ret = False
            break
    
        if _r[3]:
            _type = 'ketones'
            _val  = _r[3]
        else:
            _type = 'glucose'
            _val  = _r[2]
            
        # Ready for insert...
        sqlQ = """
            INSERT INTO telemeddata_new (device_id, terminal_id, timestamp, type, value) VALUES
            (%s,%s,%s,%s,%s)
            """
        paramQ =  (str(device_seq), str(term_seq), str(_r[1]), str(_type), str(_val))

        if device_seq:
            _r1 = PerformQuery(sqlQ, paramQ)
            if _r1 == False:
                print "Failed " + sqlQ % paramQ
                _ret = False
                break
        else:
            print "Value without serial -> throwing away, term_id: " + str(term_seq) + " dev_id " + str(device_seq) + " dev_ser " + str(_r[0]) + " term_ser " + str(_r[4])


    cursor.close()

    return _ret


def CopyInsulindata(term_cache, device_cache):
    """
    Copies everything from insulin_pump_data to insulin_pump_data_new
    """
    global connection

    # Are we conntected?
    if not connection:
        return False

    _ret = True

    sqlQ = "SELECT * FROM insulin_pump_data"

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, () )

    except MySQLdb.MySQLError, msg:
        return False

    while True:

        _r = cursor.fetchone()

        if _r == None:
            break

        # We now have a row, first check the ID of the terminal and meter
        term_seq = GetDbElementCached("terminals_new", "seq", "terminal_id", _r[6], term_cache)

        if term_seq == False:
            print "Failed GetDbElementCached"
            _ret = False
            break

        device_seq = GetDbElementCached("devices", "seq", "serialnumber", _r[1], device_cache)

        if device_seq == False:
            print "Failed GetDbElementCached"
            _ret = False
            break


        # Ready for insert...
        sqlQ = """
            INSERT INTO insulin_pump_data_new (device_id,  terminal_id,  timestamp, type, value) VALUES
            (%s,%s,%s,%s,%s)
            """
        paramQ =  (str(device_seq), str(term_seq), str(_r[3]), str(_r[2]), str(_r[4]))

        if device_seq and term_seq:
            _r1 = PerformQuery(sqlQ, paramQ)
            if _r1 == False:
                print "Failed " + sqlQ % paramQ
                _ret = False
                break
        else:
            print "Trash " + str(device_seq) + "  " + str(term_seq) + " " + _r[1] + " " + _r[6]


    cursor.close()

    return _ret


def CopyTelemeddataWithTerminalID():
    """
    Copies everything from telemeddata to telemeddata_new
    """
    global connection

    # Are we conntected?
    if not connection:
        return False

    _ret = True

    sqlQ = "SELECT *, UNIX_TIMESTAMP(timestamp) from telemeddata order by device_id, terminal_id, timestamp desc"

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, () )

    except MySQLdb.MySQLError, msg:
        return False

    old_pair = (None, None)
    last_id = None


        # result looks like this:
        #    +-----+-----------+-------------+---------------------+---------+-------+
        #    | seq | device_id | terminal_id | timestamp           | type    | value |
        #    +-----+-----------+-------------+---------------------+---------+-------+
        #    |   1 |       359 |         183 | 2005-08-17 08:54:00 | glucose |    53 |

    inserts = 0
    pairs = 0

    dt_now = datetime.now()

    while True:

        elem = cursor.fetchone()

        if elem == None:
            break

        if not old_pair == (elem[1], elem[2]):
            old_pair = (elem[1], elem[2])
            
            # Find last history event for this terminal/device pair
            sqlQ = """
                SELECT max(ts)
                FROM history,terminals,devices
                WHERE terminals.seq = %s
                    AND devices.seq= %s
                    AND devices.serialnumber = history.serial_meter
                    AND terminals.terminal_id = history.serial_terminal
            """
            paramQ = (str(elem[2]), str(elem[1]))
            res = PerformQuery(sqlQ, paramQ)
            if res == False:
                print "Failed find history event " + sqlQ % paramQ
                _ret = False
                break

            if len(res) > 0 and res[0][0] != None:
                time_str = str(res[0][0])
            else:
                # Check if the value is too new
                if time.mktime(dt_now.timetuple()) < int(elem[6]):
                    time_str = dt_now.isoformat(' ')
                else:
                    time_str = str(elem[3])
                
            # Create new transfer
            sqlQ = """
                INSERT INTO transfers (timestamp, terminal_id, device_id)
                VALUES (%s, %s, %s)
            """            
            paramQ = (time_str, str(elem[2]), str(elem[1]))
            if PerformQuery(sqlQ, paramQ) == False:
                print "Failed to insert transfer " + sqlQ % paramQ
                _ret = False
                break

            last_id = GetLastInsertID()
            
            pairs += 1
            
            if pairs % 100 == 0:
                print "Has inserted %d transfers so far" % pairs

        sqlQ = """
            INSERT INTO telemeddata_new (seq, device_id, transfer_id, timestamp, type, value)
            VALUES (%s, %s, %s, %s, %s, %s)
        """
        paramQ = (str(elem[0]), str(elem[1]), str(last_id), str(elem[3]), str(elem[4]), str(elem[5]))

        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed to insert " + sqlQ + paramQ
            _ret = False
            break
        
        inserts += 1
        
        if inserts % 10000 == 0:
            print "Has moved %d values so far" % (inserts)

    cursor.close()

    return _ret


def CopyTerminalContent():
    """
    Copies the contents from terminals to a new terminals
    """
    print "Creating new terminals table"
    # Create a new one
    sqlQ = """
        CREATE TABLE terminals_new (
        `seq` int unsigned NOT NULL auto_increment,
          `terminal_id` varchar(32) NOT NULL default '',
          `admin` varchar(32) NOT NULL default '',
          `hw_nr` varchar(32) default NULL,
          `sw_nr` varchar(32) default NULL,
          `bl_nr` varchar(32) default NULL,
          `phone` varchar(32) default NULL,
          `balance` int(11) default NULL,
          `nr_transfers` int(11) default NULL,
          `amount_data` int(11) default NULL,
          `total_amount_data` int(11) default NULL,
          `comment` tinytext,
          `location` tinytext,
          PRIMARY KEY (`seq`),
          UNIQUE KEY `terminal_id` (`terminal_id`)
        )DEFAULT CHARSET=utf8 
        """

    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False


    sqlQ = """
        SELECT * FROM terminals
        """

    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False

    print "Inserting in the new table"
    sqlQ = """
        INSERT INTO terminals_new (terminal_id,admin,hw_nr,sw_nr,bl_nr,phone,balance,nr_transfers,amount_data,total_amount_data,comment,location) VALUES
        (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
        """
    for r in _ret:
        paramQ = (r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11])

        _r = PerformQuery(sqlQ, paramQ)

        if _r == False:
            print "Failed " + sqlQ % paramQ
            return False

    return True

def CreateTerminalEntriesFromTable(table):
    """
    Finds terminals in a database and inserts them into the new terminal table.
    """

    sqlQ = "SELECT DISTINCT terminal_id FROM " + table + " ORDER BY terminal_id"
    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False

    for r in _ret:
        _ret = CreateTerminal(r)
        if False == _ret:
            return False

    return True


def CreateDeviceEntriesFromTable(table, type_key, deviceclass):
    """
    Finds devices in a database and inserts them into the devices table.
    """

    sqlQ = "SELECT DISTINCT serialnumber, " + type_key + " FROM " + table + " ORDER BY serialnumber"

    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False

    for r in _ret:
        _ret = CreateDevice(r, deviceclass)
        if False == _ret:
            print "Failed"
            return False

def DropTable(t):
    """
    Function that drops a table
    """
    sqlQ = "DROP TABLE " + t
    return PerformQuery(sqlQ, ())


def UserExist(pcode):
    """
    Checks if a user exist in the database
    """
    sqlQ = "SELECT pcode FROM users WHERE pcode = %s"
    paramQ = (str(pcode))
    
    _ret = PerformQuery(sqlQ, paramQ)

    if _ret == False:
        return False
    else:
        return len(_ret) > 0
        

def InsertIntoDeviceTable(results, device_class):
    """
    Inserts elements into the device table
    """
    sqlQ = "INSERT INTO devices (serialnumber, deviceclass) VALUES (%s, '" + device_class + "')"
    _os_sqlQ = "INSERT INTO device_ownership (pcode, device_id) VALUES (%s, %s)"

    _old_ser = None
    _last_id = None

    for r in results:
        _pcode = str(r[1])
        _ser   = str(r[0])
        # Check if the user exist
        if UserExist(_pcode):

            _new_ser = _ser.strip().upper()

            # Only insert one entry per device in the device table
            if _old_ser != _new_ser:
                paramQ = (_new_ser)

                if PerformQuery(sqlQ, paramQ) == False:
                    print "Failed: " + sqlQ % paramQ
                    print "Old ser: '" + str(_old_ser) + "' new ser: '" + _new_ser + "'"
                    return False
                    
                _last_id = GetLastInsertID()

            if _last_id:
                # Now insert an entry in the ownership table.
                paramQ = (_pcode, str(_last_id))
                if PerformQuery(_os_sqlQ, paramQ) == False:
                    print "Failed: " + sqlQ % paramQ
                    print "Old ser: '" + str(_old_ser) + "' new ser: '" + _new_ser + "'"
                    return False

            _old_ser = _new_ser


def MoveNewToCurrentTable(table):
    """
    Function that renames current table to table_old, and renames table_new to table
    """
    sqlQ = "RENAME TABLE " + table + " TO " + table + "_old"
    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False

    sqlQ = "RENAME TABLE " + table + "_new TO " + table
    return PerformQuery(sqlQ, ())


def GetDeviceId(serial):
    """
    Looks up a device ID for a serial number
    """
    sqlQ = """
        SELECT seq from devices WHERE serialnumber = %s
        """
    paramQ = (str(serial))

    ret = PerformQuery(sqlQ, paramQ)

    if ret == False:
        print sqlQ % paramQ
        return False
    
    if ret == ():
        return False
    else:
        return ret[0][0]


def GetTerminalId(serial):
    """
    Looks up a terminal ID for a serial number
    """
    sqlQ = """
        SELECT seq from terminals WHERE terminal_id = %s
        """
    paramQ = (str(serial))

    ret = PerformQuery(sqlQ, paramQ)

    if ret == False:
        print sqlQ % paramQ
        return False

    if ret == ():
        return False
    else:
        return ret[0][0]

def UpdateAnimasAlarmcodes():
    """
    Function which translates from "animas" alarm codes to "our codes"
    
    This is the alarm mapping function of the backend, same rules needed in the DB conversion,
    previously we inserted the raw codes.
    
    def GetAlarmCode(code):
        if code < 127:
            return ALARM_CALL_SERVICE
        elif code == 128:
            return ALARM_WRONG_BATTERY_TYPE
        elif code == 144: 
            return ALARM_CARTRIDGE_REPLACE
        elif code == 145:
            return ALARM_OCCLUSION_1
        elif code == 146: 
            return ALARM_OCCLUSION_2
        elif code == 147:
            return ALARM_OCCLUSION_3
        elif code == 148:  
            return ALARM_OCCLUSION_4
        elif code == 150:   
            return ALARM_POWER_AUTO_OFF
        elif code == 160:   
            return ALARM_DELIVERY_BASAL_SUSPENDED_15MIN
        elif code == 161:   
            return ALARM_DELIVERY_BOLUS_SUSPENDED
        elif code == 162:   
            return ALARM_DELIVERY_PRIME_NOT_DELIVERED
        elif code == 163:   
            return ALARM_CARTRIDGE_REMOVED
        elif code == 164:
            return ALARM_CARTRIDGE_EMPTY
        elif code == 165:   
            return ALARM_DELIVERY_STOPPED_EXCEED_TDD
        elif code == 166:   
            return ALARM_DELIVERY_STOPPED_EXCEED_BOLUS
        elif code == 171:   
            return ALARM_DELIVERY_STOPPED_EXCEED_BASAL
        elif code == 173:   
            return ALARM_CARTRIDGE_NO_COUNT
        elif code == 174:
            return ALARM_DELIVERY_BASAL_SUSPENDED
        elif code == 175:
            return ALARM_DELIVERY_STOPPED_USER
        elif code == 177:   
            return ALARM_BATTERY_LOW
        elif code == 178:   
            return ALARM_CARTRIDGE_LOW
        elif code == 179:   
            return ALARM_LOW_BLOOD_GLUCOSE
        elif code == 180:   
            return ALARM_HIGH_BLOOD_GLUCOSE
        else:
            return ALARM_CALL_SERVICE
    
    """

    sqlQ = """
        UPDATE telemeddata set value=%s where type='insulin_alarm' and value =%s 
        """

    if PerformQuery(sqlQ, (100012,128)) == False:
        return False

    if PerformQuery(sqlQ, (100026,144)) == False:
        return False

    if PerformQuery(sqlQ, (100032,145)) == False:
        return False

    if PerformQuery(sqlQ, (100033,146)) == False:
        return False

    if PerformQuery(sqlQ, (100034,147)) == False:
        return False

    if PerformQuery(sqlQ, (100035,148)) == False:
        return False

    if PerformQuery(sqlQ, (100004,150)) == False:
        return False

    if PerformQuery(sqlQ, (100111,160)) == False:
        return False

    if PerformQuery(sqlQ, (100113,161)) == False:
        return False
    
    if PerformQuery(sqlQ, (100112,162)) == False:
        return False
    
    if PerformQuery(sqlQ, (100024,163)) == False:
        return False
    
    if PerformQuery(sqlQ, (100020,164)) == False:
        return False
    
    if PerformQuery(sqlQ, (100107,165)) == False:
        return False
    
    if PerformQuery(sqlQ, (100109,166)) == False:
        return False
    
    if PerformQuery(sqlQ, (100108,171)) == False:
        return False
    
    if PerformQuery(sqlQ, (100027,173)) == False:
        return False
    
    if PerformQuery(sqlQ, (100110,174)) == False:
        return False
    
    if PerformQuery(sqlQ, (100106,175)) == False:
        return False
    
    if PerformQuery(sqlQ, (100011,177)) == False:
        return False

    if PerformQuery(sqlQ, (100022,178)) == False:
        return False
    
    if PerformQuery(sqlQ, (100201,179)) == False:
        return False
    
    if PerformQuery(sqlQ, (100200,180)) == False:
        return False
        
    sqlQ = """
        UPDATE telemeddata set value=%s where type='insulin_alarm' and value < %s
        """

    if PerformQuery(sqlQ, (100017,65536)) == False:
        return False
    
    return True

def UpgradeFromR1fToR1g():
    """
    Function that upgrades the database from version R1f to R1g
    """
    print "\n== Upgrading from version R1f to R1g =="

    print "Removing dangling admin properties"
    sqlQ = """
        DELETE FROM admin_properties
        WHERE admin not in (SELECT name from admins)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Update the admin_properties table to have a foreign key in the admins table"
    sqlQ = """
        ALTER TABLE admin_properties
        ADD FOREIGN KEY (`admin`) REFERENCES admins(`name`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Creating super_admin_properties table"
    sqlQ = """
        CREATE TABLE super_admin_properties (
            `super_admin` VARCHAR(10) NOT NULL,
            `property` VARCHAR(32) NOT NULL,
            `value` VARCHAR(32) NOT NULL,
            PRIMARY KEY(`super_admin`, `property`),
            FOREIGN KEY (`super_admin`) REFERENCES super_admins(`name`)
        )DEFAULT CHARSET=utf8 ENGINE=InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Convert the users table to InnoDB";
    sqlQ = """
        ALTER TABLE users ENGINE=InnoDB
        """;

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Will make the admin field of users to the same length as name in admins"
    sqlQ = """
        ALTER TABLE users
            CHANGE COLUMN `admin` `admin` VARCHAR(10) NOT NULL
        """;

    print "Remove users which does not have any admin"
    sqlQ = """
        DELETE FROM users
            WHERE admin NOT IN (SELECT name FROM admins)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Will make the admin field of users a foreign key to the admin table"
    sqlQ = """
        ALTER TABLE users
        ADD FOREIGN KEY (`admin`) REFERENCES admins(`name`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    return SetDbVersion('R1g')


def UpgradeFromR1gToR1h():
    """
    Function that upgrades the database from version R1g to R1h
    """
    print "\n== Upgrading from version R1g to R1h =="

    print "Adds a diabetes type column to the users table"
    sqlQ = """
        ALTER TABLE users
        ADD COLUMN diabetes_type ENUM('general', 'type_1', 'type_2', 'gestational', 'other') default 'general';
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
   
    return SetDbVersion('R1h')


def UpgradeFromR1hToR1i():
    """
    Function that upgrades the database from version R1h to R1i
    """
    print "\n== Upgrading from version R1h to R1i =="


    print "Will make pcode in user_properties a foreign key into the users table"
    sqlQ = """
        ALTER TABLE user_properties
        ADD FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False


    print "Will update the device ownership table to inno db"
    sqlQ = """
        ALTER TABLE device_ownership ENGINE=InnoDB
        """;

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Deleting dangling device ownerships"
    sqlQ = """
        DELETE FROM device_ownership
        WHERE pcode NOT IN (SELECT pcode FROM users)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False


    print "Will make pcode in device_ownership a foreign key into the users table"
    sqlQ = """
        ALTER TABLE device_ownership
        ADD FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "will make terminal_id in the terminal_properties table a foreign key to the terminals table"
    sqlQ = """
        ALTER TABLE terminal_properties
        ADD FOREIGN KEY (`terminal_id`) REFERENCES terminals(`seq`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Will make device_id in device_ownership a foreign key into the devices"
    sqlQ = """
        ALTER TABLE device_ownership
        ADD FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Update settings ID 68 to 67, was duplicated - part I"
    sqlQ = """
        UPDATE IGNORE device_settings
        SET setting_id = 67
        WHERE setting_id = 68
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7a"
        return False

    print "Update settings ID 68 to 67, was duplicated - part II"
    sqlQ = """
        DELETE FROM device_settings
        WHERE setting_id = 68
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7b"
        return False

    print "Will remove duplicates from terminal_permissions"
    sqlQ = """
        DELETE b.* FROM 
        terminal_permissions AS a, terminal_permissions AS b 
        WHERE a.terminal_id = b.terminal_id 
            AND a.seq < b.seq
            AND a.permission = b.permission
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False

    print "Will remove the seq field from terminal_permissions"
    sqlQ = """
        ALTER TABLE terminal_permissions
        DROP COLUMN seq
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False

    print "Will change the permission field to varchar"
    sqlQ = """
        ALTER TABLE terminal_permissions
        MODIFY COLUMN permission VARCHAR(32) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10"
        return False


    print "Will add primary key to terminal_permissions"
    sqlQ = """
        ALTER TABLE terminal_permissions
        ADD PRIMARY KEY(terminal_id, permission)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 11"
        return False

    print "Remove dangling communication"
    sqlQ = """
        DELETE FROM communication
        WHERE pcode not in (SELECT pcode FROM users)        
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 12"
        return False
    
    print "Convert the communication to inno DB"
    sqlQ = """
        ALTER TABLE communication ENGINE=InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 13"
        return False
    
    print "Add a foreign key from communications->pcode into users"
    sqlQ = """
        ALTER TABLE communication
        ADD FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 14"
        return False

    print "Convert the admin_hsa_ids to inno DB"
    sqlQ = """
        ALTER TABLE admin_hsa_ids ENGINE=InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 15"
        return False

    print "Add a foreign key from admin_hsa_ids->admin into admins"
    sqlQ = """
        ALTER TABLE admin_hsa_ids
        ADD FOREIGN KEY (`admin`) REFERENCES admins(`name`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 16"
        return False


    print "Create terminal_software_versions"
    sqlQ = """
        CREATE TABLE terminal_software_versions (
            `seq` int unsigned NOT NULL auto_increment,
            `hash` varchar(32) NOT NULL,
            `readable_version` varchar(32) NOT NULL,
            PRIMARY KEY (`seq`),
            UNIQUE KEY  (`hash`)
        )DEFAULT CHARSET=utf8 ENGINE=InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 17"
        return False

    
    return SetDbVersion('R1i')

def MoveStringsFromPatients():
    """
    Move strings from patient which is now stored in user_strings instead
    """
    sqlQ = """
        SELECT pcode, address,  postcode, city, hometel, mobiletel, worktel, email
        FROM users    
    """
    ret = PerformQuery(sqlQ, ())
    if not ret:
        print "Failed selecting patients"
    
    strings = ["bogus", "address", "postal_code", "city", "email", "home_tel", "mobile_tel", "work_tel"]
    sqlQ = """
        INSERT INTO user_strings
        VALUES (%s, %s, %s)
    """
    for line in ret:
        for i in range(1,7):
            if line[i]:
                paramQ = (str(line[0]), strings[i], str(line[i]))
                if PerformQuery(sqlQ, paramQ) == False:
                    return False
            
    return True

def UpgradeFromR1iToR1j():
    """
    Function that upgrades the database from version R1i to R1j
    """
    print "\n== Upgrading from version R1i to R1j =="


    print "Will create user_strings table"
    sqlQ = """
        CREATE TABLE user_strings (
        `pcode` varchar(32) NOT NULL,
        `string` varchar(30) NOT NULL,
        `value` varchar(512),
        PRIMARY KEY (`pcode`, `string`),
        FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
        )DEFAULT CHARSET=utf8 ENGINE=InnoDB    
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    if not MoveStringsFromPatients():
        print "Failed 2"
        return False
        
    print "Will drop unused columns of users"
    sqlQ = """
        ALTER TABLE users
        DROP COLUMN address,
        DROP COLUMN postcode,
        DROP COLUMN city,
        DROP COLUMN hometel,
        DROP COLUMN mobiletel,
        DROP COLUMN worktel,
        DROP COLUMN email
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    return SetDbVersion('R1j')


def UpgradeFromR1jToR1k():
    """
    Function that upgrades the database from version R1j to R1k
    """
    print "\n== Upgrading from version R1j to R1k =="


    print "Adding new device types, device_phone & device_combo"
    sqlQ = """
        ALTER TABLE devices
            MODIFY COLUMN deviceclass 
            ENUM('device_meter', 'device_pump', 'device_pen', 'device_pen_cap', 'device_phone', 'device_combo') DEFAULT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
        
    print "Adding new telemeddata types, carbs & training - this will take some time!"
    sqlQ = """
        ALTER TABLE telemeddata
            MODIFY COLUMN `type` 
            ENUM('ketones', 'glucose', 'pen_cap_off', 'insulin_basal','insulin_bolus','insulin_tdd','insulin_prime','insulin_alarm','insulin_bolus_basal','carbs','training') NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Updating the terminals table, adding new types - phone & user"
    sqlQ = """
        ALTER TABLE terminals
            MODIFY COLUMN terminalclass ENUM('terminal_diasend','terminal_smartpix','terminal_phone', 'terminal_user') DEFAULT 'terminal_diasend' NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Updating resolution of some Animas settings"
    sqlQ = """
        UPDATE device_settings LEFT JOIN devices ON device_settings.device_id = devices.seq
            SET device_settings.value=device_settings.value*10
            WHERE setting_id in (7,8,9)
                AND SUBSTR(devices.devicetype,1,6) = 'Animas'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Add mobile log table, for DSBUS study. Should be removed when study is done"
    sqlQ = """
        CREATE TABLE `moblog` (
            `id` int(11) NOT NULL auto_increment,
            `pcode` varchar(30) NOT NULL,
            `key` enum('glucose','insuline','carbs','practice','starval','txt','login','logout','page') NOT NULL,
            `value` text NOT NULL,
            `ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
            PRIMARY KEY  (`id`),
            FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
        ) DEFAULT CHARSET=utf8 ENGINE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Rename all occurrences of 'OneTouch Ultra Easy' to 'Ultra Mini/Easy'"
    sqlQ = """
        UPDATE devices SET devicetype = "OneTouch Ultra Mini/Easy" WHERE devicetype="OneTouch Ultra Easy";
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False


    return SetDbVersion('R1k')

def UpgradeFromR1kToR1m():
    """
    Function that upgrades the database from version R1k to R1m
    """
    print "\n== Upgrading from version R1k to R1m =="

    ret = UpgradeToR5UserFunctionality()
    if not ret:
        return False

    print "Modifying super_admin_properties table"
    sqlQ = """
        ALTER TABLE super_admin_properties
            MODIFY COLUMN super_admin VARCHAR(30) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Adding device_properties table"
    sqlQ = """
        CREATE TABLE `device_properties` (
          `device_id` int(10) unsigned NOT NULL,
          `property` varchar(32) NOT NULL,
          `value` varchar(32) default NULL,
          PRIMARY KEY  (`device_id`,`property`),
          CONSTRAINT `device_properties_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Adding new device type, device_user"
    sqlQ = """
        ALTER TABLE devices
            MODIFY COLUMN deviceclass 
            ENUM('device_meter', 'device_pump', 'device_pen', 'device_pen_cap', 'device_phone', 'device_combo', 'device_user') DEFAULT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False
    
    print "Updating the terminals table with methods for file transfer and manual input"
    sqlQ = """
        ALTER TABLE terminals
            MODIFY COLUMN terminalclass ENUM('terminal_diasend','terminal_smartpix','terminal_phone', 'terminal_file_transfer', 'terminal_web_ui', 'terminal_mobile_ui') DEFAULT 'terminal_diasend' NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Enable Diasend Mobile for all users with mobile phones registered"
    sqlQ = """
        INSERT INTO user_properties (pcode, property, value) (SELECT pcode, "diasendmobile", 1 FROM device_ownership,devices WHERE device_ownership.device_id = devices.seq AND devices.deviceclass = "device_phone")
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False
    
    print "Enable SMS Package for all admins with Diasend Mobile package enabled"
    sqlQ = """
        INSERT INTO admin_properties (admin, property, value) (SELECT admin, "sms_support", 1 FROM admin_properties WHERE property = "mobile_support");
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Creating HCP account ID's"
    if GenerateHCPaccountIDs() == False:
        print "Failed 7"
        return False        

    print "Move phones to user devices"
    if MovePhonesToUserDevices() == False:
        print "Failed 8"
        return False

    print "Don't forget to add File import devices!"
    
    return SetDbVersion('R1m')
    

def UpgradeFromR1mToR1n():
    """
    Function that upgrades the database from version R1m to R1n
    """
    print "\n== Upgrading from version R1m to R1n =="

    sqlQ = """
        ALTER TABLE users
            MODIFY COLUMN `goalmin` decimal(4,2) default '4.0',
            MODIFY COLUMN `goalmax` decimal(4,2) default '10.0'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    return SetDbVersion('R1n')

def UpgradeFromR1nToR1o():
    """
    Function that upgrades the database from version R1n to R1o
    """
    print "\n== Upgrading from version R1n to R1o =="

    sqlQ = """
        ALTER TABLE telemeddata_values
            MODIFY COLUMN type ENUM('temperature','duration','insulin_basal_tdd','insulin_bolus_ext','insulin_bolus_suggested','insulin_bolus_prog_meal','insulin_bolus_prog_corr')
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    return SetDbVersion('R1o')


def UpgradeFromR1oToR1p():
    """
    Function that upgrades the database from version R1o to R1p
    """
    print "\n== Upgrading from version R1o to R1p =="

    print "Removing duplicated rates"    
    # Removed entries where there are duplicated rates
    sqlQ = """
        DELETE b.* FROM 
            device_programs AS a, device_programs AS b 
            WHERE a.device_id = b.device_id
            AND a.type = b.type
            AND a.name = b.name
            AND a.period_start = b.period_start
            AND a.period_rate != b.period_rate
    """

    # There are duplicates in the database of some reason(?)
    print "creating tmp programs"
    sqlQ = """
        CREATE TEMPORARY TABLE device_programs_tmp
            AS SELECT DISTINCT * FROM device_programs
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    print "dropping device_programs"
    sqlQ = """
        DROP TABLE device_programs
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
    
    print "Recreating device_programs"
    sqlQ = """
        CREATE TABLE device_programs (
            `seq` int(10) NOT NULL auto_increment,
            `device_id` int(10) unsigned NOT NULL,
            `type` ENUM('basal_program', 'isf_program', 'ic_ratio_program', 'bg_target_program') NOT NULL,
            `name` varchar(30) NOT NULL,
            `period_start` time NOT NULL,
            `period_rate` int unsigned NOT NULL,
            PRIMARY KEY  (`seq`),
            FOREIGN KEY (`device_id`) REFERENCES devices(`seq`),
            UNIQUE KEY  (`device_id`, `type`, `name`, `period_start`)
        )DEFAULT CHARSET=utf8 ENGINE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "refilling device_programs"
    sqlQ = """
        INSERT INTO device_programs(device_id, type, name, period_start, period_rate)
            SELECT device_id, type, name, period_start, period_rate
                FROM device_programs_tmp
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Dropping tmp table"
    sqlQ = """
        DROP TABLE device_programs_tmp
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False
        
    print "Creating the device_program_values table"
    sqlQ = """
        CREATE TABLE `device_program_values` (
          `device_program_period_id` int(10),
          `type` ENUM('bg_deviation'),
          `value` int unsigned default NULL,
          PRIMARY KEY  (`device_program_period_id`, `type`),
          FOREIGN KEY (`device_program_period_id`) REFERENCES device_programs(`seq`)
        ) DEFAULT CHARSET=utf8 ENGINE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Creating the HbA1c table"
    sqlQ = """
        CREATE TABLE `hba1c` (
          `seq` int(10) NOT NULL auto_increment,
          `pcode` varchar(128) NOT NULL,
          `value` varchar(4) NOT NULL,
          `timestamp` datetime NOT NULL,
          `comment` varchar(20) default NULL,
          PRIMARY KEY  (`seq`),
          FOREIGN KEY  (`pcode`) REFERENCES users(`pcode`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False

    return SetDbVersion('R1p')

def UpgradeFromR1pToR1q():
    """
    Function that upgrades the database from version R1p to R1q
    """
    print "\n== Upgrading from version R1p to R1q =="


    print "Adding new device program types"
    sqlQ = """
        ALTER TABLE device_programs
            MODIFY COLUMN type 
                ENUM('basal_program','isf_program','ic_ratio_program','bg_target_program','bg_threshold_program','correction_program') NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
 
    return SetDbVersion('R1q')

def UpgradeFromR1qToR1r():
    """
    Function that upgrades the database from version R1q to R1r
    """
    print "\n== Upgrading from version R1q to R1r =="

    print "Adding new column dibs_id to registration_events table"
    sqlQ = """
        ALTER TABLE registration_events
            ADD COLUMN dibs_id int(11) DEFAULT NULL,
            ADD FOREIGN KEY (`dibs_id`) REFERENCES dibs_transactions(`id`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    print "Removing carbs data from all Animas IR and Ping pumps (because of bug, acknowledged by Animas)"
    sqlQ = """
        DELETE telemeddata.* FROM telemeddata LEFT JOIN devices ON device_id=devices.seq WHERE type="carbs" AND (devices.devicetype LIKE "Animas IR%%" OR devices.devicetype = "OneTouch Ping")
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    return SetDbVersion('R1r')

def UpgradeFromR1rToR1s():
    """
    Function that upgrades the database from version R1r to R1s
    """
    print "\n== Upgrading from version R1r to R1s =="


    print "Adding new telemeddata type: event - this will take some time!"
    sqlQ = """
        ALTER TABLE telemeddata
            MODIFY COLUMN `type` 
            ENUM('ketones', 'glucose', 'pen_cap_off', 'insulin_basal','insulin_bolus','insulin_tdd','insulin_prime','insulin_alarm','insulin_bolus_basal','carbs','training','event') NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
 
    return SetDbVersion('R1s')

def UpgradeFromR1sToR1t():
    """
    Function that upgrades the database from version R1s to R1t
    """
    print "\n== Upgrading from version R1s to R1t =="

    print "Adding clinic_users table"
    sqlQ = """
        CREATE TABLE `clinic_users` (
            `username` varchar(30) NOT NULL,
            `password` varchar(30) NOT NULL,
            `firstname` varchar(30) NOT NULL,
            `lastname` varchar(30) NOT NULL,
            `admin` varchar(10) NOT NULL,
            `comment` text,
            PRIMARY KEY  (`username`),
            FOREIGN KEY (`admin`) REFERENCES admins(`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    print "Adding login_activity table"
    sqlQ = """
        CREATE TABLE `login_activity` (
            `seq` int unsigned NOT NULL auto_increment,
            `timestamp` datetime NOT NULL,
            `ip` int unsigned NOT NULL,
            `username` varchar(128) NOT NULL,
            `password` blob,
            `event` enum('ok', 'archived', 'expired', 'failed') NOT NULL,
            `account_type` enum('-', 'patient_normal', 'patient_srpa', 'clinic_admin', 'clinic_user', 'super_admin') NOT NULL,
            PRIMARY KEY  (`seq`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Updating weight property for patients, to fix issue with previously missing weight dropdown items"
    sqlQ = """
        UPDATE user_properties SET value=value+2 WHERE value>=8 AND property = "weight";
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False
 
    return SetDbVersion('R1t')
    
def UpgradeFromR1tToR1u():
    """
    Function that upgrades the database from version R1t to R1u
    """
    print "\n== Upgrading from version R1t to R1u =="

    print "Adding pdf_profiles table"
    sqlQ = """
        CREATE TABLE `pdf_profiles` (
            `seq` int(10) NOT NULL auto_increment,
            `admin` varchar(10) NOT NULL,
            `name` varchar(32) NOT NULL,
            `data` blob NOT NULL,
            PRIMARY KEY  (`seq`),
            KEY `admin` (`admin`),
            CONSTRAINT FOREIGN KEY (`admin`) REFERENCES `admins` (`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    print "Adding browser column to login_activity"
    sqlQ = """
        ALTER TABLE login_activity ADD COLUMN browser VARCHAR(512);
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
 
    return SetDbVersion('R1u')

def UpgradeFromR1uToR1v():
    """
    Function that upgrades the database from version R1u to R1v
    """
    print "\n== Upgrading from version R1u to R1v =="

    print "Altering terminals table (terminalclass column)"
    sqlQ = """
    ALTER TABLE terminals MODIFY COLUMN terminalclass ENUM('terminal_diasend','terminal_smartpix','terminal_phone',
    'terminal_file_transfer','terminal_web_ui','terminal_mobile_ui', 'terminal_dda') 
    NOT NULL DEFAULT "terminal_diasend";
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    return SetDbVersion('R1v')

def UpgradeFromR1vToR1w():
    """
    Function that upgrades the database from version R1v to R1w
    """
    print "\n== Upgrading from version R1v to R1w =="

    print "Altering clinic_users table (adding properties column)"
    sqlQ = """
    ALTER TABLE clinic_users ADD COLUMN `properties` int(10) unsigned default '0'
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    return SetDbVersion('R1w')

def UpgradeFromR1wToR1x():
    """
    Function that upgrades the database from version R1w to R1x
    """
    print "\n== Upgrading from version R1w to R1x =="

    print "Altering users table (archived column)"
    sqlQ = """
    ALTER TABLE users MODIFY COLUMN archived ENUM('Yes', 'No', 'Deleted') NOT NULL default 'No'
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Altering login_activity table (event column)"
    sqlQ = """
    ALTER TABLE login_activity MODIFY COLUMN event enum('ok','archived','expired','failed','deleted') NOT NULL
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
        
    print "Adding deletion_log table"
    sqlQ = """
        CREATE TABLE `deletion_log` (
            `seq` int unsigned NOT NULL auto_increment,
            `timestamp` datetime NOT NULL,
            `ip` int unsigned NOT NULL,
            `pcode` VARCHAR(128) NOT NULL,
            PRIMARY KEY (`seq`),
            FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Adding lostpassword_keys table"
    sqlQ = """
        CREATE TABLE `lostpassword_keys` (
            `seq` int unsigned NOT NULL auto_increment,
            `timestamp` datetime NOT NULL,
            `ip` int unsigned NOT NULL,
            `pcode` VARCHAR(128) NOT NULL,
            `key` VARCHAR(128) NOT NULL,
            PRIMARY KEY (`seq`),
            FOREIGN KEY (`pcode`) REFERENCES users(`pcode`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False


    return SetDbVersion('R1x')
    
    
def UpgradeFromR1xToR1y():
    """
    Function that upgrades the database from version R1x to R1y
    """
    print "\n== Upgrading from version R1x to R1y =="

    print "Altering admins table (password length)"
    sqlQ = """
    ALTER TABLE admins MODIFY COLUMN `password` varchar(32) NOT NULL default ''
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Altering super_admins table (password length)"
    sqlQ = """
    ALTER TABLE super_admins MODIFY COLUMN `password` varchar(32) NOT NULL
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Altering clinic_users table (password length)"
    sqlQ = """
    ALTER TABLE clinic_users MODIFY COLUMN `password` varchar(32) NOT NULL
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    return SetDbVersion('R1y')

def UpgradeFromR1yToR1z():
    """
    Function that upgrades the database from version R1y to R1z
    """
    print "\n== Upgrading from version R1y to R1z =="

    print "Creating food_collection table"
    sqlQ = """
    CREATE TABLE `food_collection` (
        `seq` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(32) DEFAULT NULL,
        `last_edited` datetime DEFAULT NULL,
        `transferred_from_device` datetime DEFAULT NULL,
        `transferred_to_device` datetime DEFAULT NULL,
        `device_id` int(10) unsigned DEFAULT NULL,
        `is_backup` tinyint(1) DEFAULT '0',
        PRIMARY KEY (`seq`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Creating food_collection_owner_clinic table"
    sqlQ = """
    CREATE TABLE `food_collection_owner_clinic` (
        `food_collection_id` int(11) DEFAULT NULL,
        `admin` varchar(128) DEFAULT NULL,
        KEY `fk_food_collection_owner_clinic1` (`admin`),
        KEY `fk_food_collection_owner_clinic2` (`food_collection_id`),
        CONSTRAINT `fk_food_collection_owner_clinic1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_food_collection_owner_clinic2` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Creating food_collection_owner_patient table"
    sqlQ = """
    CREATE TABLE `food_collection_owner_patient` (
        `food_collection_id` int(11) DEFAULT NULL,
        `pcode` varchar(128) DEFAULT NULL,
        KEY `fk_food_collection_owner_patient1` (`pcode`),
        KEY `fk_food_collection_owner_patient2` (`food_collection_id`),
        CONSTRAINT `fk_food_collection_owner_patient1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_food_collection_owner_patient2` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Creating food_item table"
    sqlQ = """
    CREATE TABLE `food_item` (
        `seq` int(11) NOT NULL AUTO_INCREMENT,
        `food_collection_id` int(11) DEFAULT NULL,
        `name` varchar(32) DEFAULT NULL,
        `servings` varchar(32) DEFAULT NULL,
        `carbs` int(11) DEFAULT NULL,
        `fiber` int(11) DEFAULT NULL,
        `protein` int(11) DEFAULT NULL,
        `fat` int(11) DEFAULT NULL,
        `sort_order` int(11) DEFAULT NULL,
        `food_category_id` int(11) DEFAULT NULL,
        PRIMARY KEY (`seq`),
        KEY `fk_food_collection` (`food_collection_id`),
        CONSTRAINT `fk_food_collection` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    return SetDbVersion('R1z')
    
def UpgradeFromR1zToR2a():
    """
    Function that upgrades the database from version R1z to R2a
    """
    print "\n== Upgrading from version R1z to R2a =="

    print "Altering devices table (adding serial_obfuscated column)"
    sqlQ = """
    ALTER TABLE devices ADD COLUMN  `serial_obfuscated` varchar(30) DEFAULT NULL
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    
    return SetDbVersion('R2a')

def UpgradeFromR2aToR2b():
    """
    Function that upgrades the database from version R2a to R2b.
    """
    print "\n== Upgrading from version R2a to R2b =="

    print "Creating OAuth client table"
    sqlQ = """
    CREATE  TABLE `oauth_client` (
      `seq` INT NOT NULL AUTO_INCREMENT,
      `client_id` VARCHAR(128) NOT NULL ,
      `hash` VARCHAR(255) NOT NULL ,
      `salt` VARCHAR(40) NOT NULL ,
      `active` TINYINT(1) NOT NULL ,
      `rate_limit_count` INT NOT NULL ,
      PRIMARY KEY (`seq`) )
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Creating OAuth permission table"
    sqlQ = """
    CREATE  TABLE `oauth_client_permission` (
      `seq` INT NOT NULL AUTO_INCREMENT,
      `oauth_client_id` INT NOT NULL ,
      `account_type` VARCHAR(64) NOT NULL ,
      `resource_type` VARCHAR(64) NOT NULL ,
      PRIMARY KEY (`seq`) )
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """
    
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Creating Access Token table"
    sqlQ = """
    CREATE  TABLE `access_token` (
      `seq` INT NOT NULL AUTO_INCREMENT,
      `token` VARCHAR(255) NOT NULL ,
      `expire` DATETIME NOT NULL ,
      `access_count` INT NOT NULL ,
      `active` TINYINT(1) NOT NULL ,
      `oauth_client_permission_id` INT NOT NULL ,
      PRIMARY KEY (`seq`) ,
      INDEX `fk_access_token_oauth_client_permission1` (`oauth_client_permission_id` ASC) ,
      CONSTRAINT `fk_access_token_oauth_client_permission1`
        FOREIGN KEY (`oauth_client_permission_id` )
        REFERENCES `oauth_client_permission` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Creating Trial table"
    sqlQ = """
    CREATE  TABLE `trial` (
      `seq` INT NOT NULL AUTO_INCREMENT,
      `start` DATETIME NOT NULL ,
      `end` DATETIME NOT NULL ,
      `username` VARCHAR(128) NOT NULL ,
      `hash` VARCHAR(255) NOT NULL ,
      `salt` VARCHAR(40) NOT NULL ,
      PRIMARY KEY (`seq`) )
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Creating device owner trial table"
    sqlQ = """
    CREATE  TABLE `device_owner_trial` (
      `trial_id` INT NOT NULL ,
      `device_id` INT(10) UNSIGNED NOT NULL ,
      INDEX `fk_device_owner_trial_trial1` (`trial_id` ASC) ,
      INDEX `fk_device_owner_trial_devices1` (`device_id` ASC) ,
      CONSTRAINT `fk_device_owner_trial_trial1`
        FOREIGN KEY (`trial_id` )
        REFERENCES `trial` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT `fk_device_owner_trial_devices1`
        FOREIGN KEY (`device_id` )
        REFERENCES `devices` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Creating access token owner trial table"
    sqlQ = """
    CREATE  TABLE `access_token_owner_trial` (
      `access_token_id` INT NOT NULL ,
      `trial_id` INT NOT NULL ,
      INDEX `fk_access_token_owner_trial_access_token1` (`access_token_id` ASC) ,
      INDEX `fk_access_token_owner_trial_trial1` (`trial_id` ASC) ,
      CONSTRAINT `fk_access_token_owner_trial_access_token1`
        FOREIGN KEY (`access_token_id` )
        REFERENCES `access_token` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT `fk_access_token_owner_trial_trial1`
        FOREIGN KEY (`trial_id` )
        REFERENCES `trial` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False
   
    return SetDbVersion('R2b')

def UpgradeFromR2bToR2c():
    """
    Function that upgrades the database from version R2b to R2c.
    """
    print "\n== Upgrading from version R2b to R2c =="

    print "Create device setting group"
    sqlQ = """
    CREATE  TABLE `device_setting_group` (
      `seq` INT NOT NULL AUTO_INCREMENT ,
      `device_id` INT(10) UNSIGNED NOT NULL ,
      `transfer_id` INT(10) UNSIGNED NOT NULL ,
      PRIMARY KEY (`seq`) ,
      INDEX `fk_device_setting_group_devices1` (`device_id` ASC) ,
      INDEX `fk_device_setting_group_transfers1` (`transfer_id` ASC) ,
      CONSTRAINT `fk_device_setting_group_devices1`
        FOREIGN KEY (`device_id` )
        REFERENCES `devices` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
      CONSTRAINT `fk_device_setting_group_transfers1`
        FOREIGN KEY (`transfer_id` )
        REFERENCES `transfers` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Creating device setting table"
    sqlQ = """
    CREATE  TABLE `device_setting` (
      `seq` INT NOT NULL AUTO_INCREMENT ,
      `device_setting_group_id` INT NOT NULL ,
      `setting_id` INT(10) NULL ,
      `value` VARCHAR(255) NULL ,
      PRIMARY KEY (`seq`) ,
      INDEX `fk_device_setting_device_setting_group1` (`device_setting_group_id` ASC) ,
      CONSTRAINT `fk_device_setting_device_setting_group1`
        FOREIGN KEY (`device_setting_group_id` )
        REFERENCES `device_setting_group` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    sqlQ = """
    CREATE  TABLE `device_program` (
      `seq` INT(10) NOT NULL AUTO_INCREMENT ,
      `device_setting_group_id` INT NOT NULL ,
      `type` ENUM('basal_program','isf_program','ic_ratio_program','bg_target_program','bg_threshold_program','correction_program') NULL ,
      `name` VARCHAR(30) NULL ,
      `period_start` TIME NULL ,
      `period_rate` INT(10) NULL ,
      PRIMARY KEY (`seq`) ,
      INDEX `fk_device_program_device_setting_group1` (`device_setting_group_id` ASC) ,
      CONSTRAINT `fk_device_program_device_setting_group1`
        FOREIGN KEY (`device_setting_group_id` )
        REFERENCES `device_setting_group` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """
    print "Creating device program table"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Creating device program value table"
    sqlQ = """
    CREATE  TABLE `device_program_value` (
      `seq` INT(10) NOT NULL AUTO_INCREMENT ,
      `device_program_id` INT(10) NOT NULL ,
      `type` ENUM('bg_deviation') NULL ,
      `value` INT(10) NULL ,
      PRIMARY KEY (`seq`) ,
      INDEX `fk_device_program_value_device_program1` (`device_program_id` ASC) ,
      CONSTRAINT `fk_device_program_value_device_program1`
        FOREIGN KEY (`device_program_id` )
        REFERENCES `device_program` (`seq` )
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
    ENGINE = InnoDB DEFAULT CHARSET=utf8;
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    return SetDbVersion('R2c')

def UpgradeFromR2cToR2d():
    """
    Function that upgrades the database from version R2c to R2d.
    """
    print "\n== Upgrading from version R2c to R2d =="

    print "Add hash and salt to users table."
    sqlQ = 'ALTER TABLE users ADD COLUMN hash VARCHAR(255) NOT NULL DEFAULT "" AFTER pcode'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
    sqlQ = 'ALTER TABLE users ADD COLUMN salt VARCHAR(40) NOT NULL DEFAULT "" AFTER hash'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Add hash and salt to admins table."
    sqlQ = 'ALTER TABLE admins ADD COLUMN hash VARCHAR(255) NOT NULL DEFAULT "" AFTER name'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False
    sqlQ = 'ALTER TABLE admins ADD COLUMN salt VARCHAR(40) NOT NULL DEFAULT "" AFTER hash'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Add hash and salt to clinic_users table."
    sqlQ = 'ALTER TABLE clinic_users ADD COLUMN hash VARCHAR(255) NOT NULL DEFAULT "" AFTER username'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False
    sqlQ = 'ALTER TABLE clinic_users ADD COLUMN salt VARCHAR(40) NOT NULL DEFAULT "" AFTER hash'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Add hash and salt to super_admins table."
    sqlQ = 'ALTER TABLE super_admins ADD COLUMN hash VARCHAR(255) NOT NULL DEFAULT "" AFTER name'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False
    sqlQ = 'ALTER TABLE super_admins ADD COLUMN salt VARCHAR(40) NOT NULL DEFAULT "" AFTER hash'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False

    return SetDbVersion('R2d')

def UpgradeFromR2dToR2e():
    """
    Function that upgrades the database from version R2d to R2e.
    """
    print "\n== Upgrading from version R2d to R2e =="

    print "Add ownership type to device - user relation." 
    sqlQ = 'ALTER TABLE device_ownership ADD COLUMN primary_device BOOLEAN NOT NULL DEFAULT 0 AFTER extra'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Change primary key for table user_ownership."
    sqlQ = 'ALTER TABLE user_ownership DROP FOREIGN KEY user_ownership_ibfk_1'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
    sqlQ = 'ALTER TABLE user_ownership DROP PRIMARY KEY'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False
    sqlQ = 'ALTER TABLE user_ownership ADD COLUMN seq INT(10) NOT NULL AUTO_INCREMENT KEY FIRST'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False
    sqlQ = 'ALTER TABLE user_ownership ADD CONSTRAINT user_ownership_ibfk_1 FOREIGN KEY (pcode) REFERENCES users (pcode)'
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Add the device_debug_record table."
    sqlQ = """CREATE TABLE `device_debug_record` (
              `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `device_id` int(10) unsigned NOT NULL,
              `transfer_id` int(10) unsigned NOT NULL,
              `data` mediumblob NOT NULL,
              PRIMARY KEY (`seq`),
              FOREIGN KEY (`transfer_id`) REFERENCES `transfers` (`seq`),
              FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Altering metadata table (value length)"
    sqlQ = """
    ALTER TABLE metadata MODIFY COLUMN `value` varchar(1024) NOT NULL
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False
        
        
    print "Altering password tables (name and null dependency)"

    print "- clinic_users"
    sqlQ = """
    ALTER TABLE clinic_users CHANGE COLUMN password oldpassword VARCHAR(32) DEFAULT ""
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False

    print "- admins"
    sqlQ = """
    ALTER TABLE admins CHANGE COLUMN password oldpassword VARCHAR(32) DEFAULT ""
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False

    print "- super_admins"
    sqlQ = """
    ALTER TABLE super_admins CHANGE COLUMN password oldpassword VARCHAR(32) DEFAULT ""
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10"
        return False

    print "- users"
    sqlQ = """
    ALTER TABLE users CHANGE COLUMN password oldpassword VARCHAR(32) DEFAULT ""
    """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 11"
        return False

    return SetDbVersion('R2e')
    
def UpgradeFromR2eToR3a():
    """
    Function that upgrades the database from version R2e to R3a.
    """
    print "\n== Upgrading from version R2e to R3a =="

    print "Create the table food_collection_owner_language"
    sqlQ = """CREATE TABLE `food_collection_owner_language` (
                `food_collection_id` int(11) DEFAULT NULL,
                `language` varchar(128) DEFAULT NULL,
                supplier varchar(128) DEFAULT NULL,
                comment varchar(128) DEFAULT NULL,
                PRIMARY KEY (`food_collection_id`,`language`),
                FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Create the table notification"
    sqlQ = """CREATE TABLE `notification` (
                `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `enabled` tinyint(1) DEFAULT NULL,
                `criteria` int(10) unsigned DEFAULT '0',
                `valid_from` datetime DEFAULT NULL,
                `valid_to` datetime DEFAULT NULL,
                PRIMARY KEY (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Create the table notification_text"
    sqlQ = """CREATE TABLE `notification_text` (
                `notification_id` int(10) unsigned NOT NULL,
                `language` varchar(32) NOT NULL DEFAULT '',
                `header` tinytext,
                `body` text,
                PRIMARY KEY (`notification_id`,`language`),
                FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Create the table notification_view_log_patient"
    sqlQ = """CREATE TABLE `notification_view_log_patient` (
                `pcode` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                `notification_id` int(10) unsigned NOT NULL,
                `viewed` datetime DEFAULT NULL,
                PRIMARY KEY (`pcode`,`notification_id`),
                KEY `notification_id` (`notification_id`),
                FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`),
                FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Create the table notification_view_log_clinic"
    sqlQ = """CREATE TABLE `notification_view_log_clinic` (
                `admin` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                `notification_id` int(10) unsigned NOT NULL,
                `viewed` datetime DEFAULT NULL,
                PRIMARY KEY (`admin`,`notification_id`),
                KEY `notification_id` (`notification_id`),
                FOREIGN KEY (`admin`) REFERENCES `admins` (`name`),
                FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Create the table notification_view_log_clinic_user"
    sqlQ = """CREATE TABLE `notification_view_log_clinic_user` (
                `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                `notification_id` int(10) unsigned NOT NULL,
                `viewed` datetime DEFAULT NULL,
                PRIMARY KEY (`username`,`notification_id`),
                KEY `notification_id` (`notification_id`),
                FOREIGN KEY (`username`) REFERENCES `clinic_users` (`username`),
                FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    return SetDbVersion('R3a')

def UpgradeFromR3aToR3b():
    """
    Function that upgrades the database from version R3a to R3b.
    """
    print "\n== Upgrading from version R3a to R3b =="

    print "Create the table account"
    sqlQ = """CREATE TABLE `account` (
        `seq` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `username` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        `hash` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        `salt` VARCHAR(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        PRIMARY KEY (`seq`),
        UNIQUE INDEX `username_UNIQUE` (`username` ASC))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Create the table access_token_owner_account"
    sqlQ = """CREATE TABLE `access_token_owner_account` (
        `account_id` INT(10) UNSIGNED NOT NULL,
        `access_token_id` INT(11) NOT NULL,
        PRIMARY KEY (`account_id`, `access_token_id`),
        FOREIGN KEY (`account_id`) REFERENCES `account` (`seq`),
        FOREIGN KEY (`access_token_id`) REFERENCES `access_token` (`seq`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Create the table oauth_client_owner_account"
    sqlQ = """CREATE TABLE `oauth_client_owner_account` (
        `oauth_client_id` INT(11) NOT NULL,
        `account_id` INT(10) UNSIGNED NOT NULL,
        PRIMARY KEY (`oauth_client_id`, `account_id`),
        FOREIGN KEY (`oauth_client_id`) REFERENCES `oauth_client` (`seq`),
        FOREIGN KEY (`account_id`) REFERENCES `account` (`seq`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Alter admins - add new column rest_api_account_id"
    sqlQ = """ALTER TABLE admins
        ADD COLUMN rest_api_account_id INT(10) UNSIGNED DEFAULT NULL,
        ADD FOREIGN KEY (`rest_api_account_id`) REFERENCES `account` (`seq`)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Alter table oauth_client - add new column description"
    sqlQ = """ALTER TABLE oauth_client
        ADD COLUMN description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT "" """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    return SetDbVersion('R3b')

def UpgradeFromR3bToR3c():
    """
    Function that upgrades the database from version R3b to R3c.
    """
    print "\n== Upgrading from version R3b to R3c =="

    print "Add enum types IOB and Override to telemeddata_values"
    sqlQ = """ALTER TABLE telemeddata_values 
        MODIFY COLUMN type enum('temperature', 'duration', 'insulin_basal_tdd', 'insulin_bolus_ext', 'insulin_bolus_suggested', 'insulin_bolus_prog_meal', 'insulin_bolus_prog_corr', 'insulin_bolus_iob', 'insulin_bolus_override') NOT NULL DEFAULT 'temperature'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Change unsigned -> signed in telemeddata_values"
    sqlQ = """ALTER TABLE telemeddata_values MODIFY COLUMN `value` int(11) signed default null"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    return SetDbVersion('R3c')

def UpgradeFromR3cToR3d():
    """
    Function that upgrades the database from version R3c to R3d.
    """
    print "\n== Upgrading from version R3c to R3d =="

    print "Create the table pdf_generated"
    sqlQ = """CREATE TABLE `pdf_generated` (
        `seq` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `timestamp` DATETIME NOT NULL, 
        `filename` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        `clinic` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        `patient` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        `delivered_at` DATETIME DEFAULT NULL,
        `deliver_status` int(10) NOT NULL DEFAULT 0,        
        PRIMARY KEY (`seq`), 
        FOREIGN KEY (`patient`) REFERENCES `users` (`pcode`),
        FOREIGN KEY (`clinic`) REFERENCES `admins` (`name`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False


    return SetDbVersion('R3d')

def UpgradeFromR3dToR3e():
    """
    Function that upgrades the database from version R3d to R3e.
    """
    print "\n== Upgrading from version R3d to R3e =="

    print "Create the table emr_destination"
    sqlQ = """CREATE TABLE `emr_destination` (
        `seq` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `description` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        `destination` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
        PRIMARY KEY (`seq`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Create the table emr_destination_owner_account"
    sqlQ = """CREATE TABLE `emr_destination_owner_account` (
        `emr_destination_id` INT(10) UNSIGNED NOT NULL,
        `account_id` INT(10) UNSIGNED NOT NULL,
        PRIMARY KEY (`emr_destination_id`, `account_id`),
        FOREIGN KEY (`emr_destination_id`) REFERENCES `emr_destination` (`seq`),
        FOREIGN KEY (`account_id`) REFERENCES `account` (`seq`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Alter patients - add new column emr_destination_id"
    sqlQ = """ALTER TABLE users
        ADD COLUMN emr_destination_id INT(10) UNSIGNED DEFAULT NULL,
        ADD FOREIGN KEY (`emr_destination_id`) REFERENCES `emr_destination` (`seq`)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False
        
    print "Alter food_collection_owner_language - add new column filename"
    sqlQ = """ALTER TABLE `food_collection_owner_language` 
        ADD `filename` VARCHAR(128)  NULL DEFAULT NULL  AFTER `supplier`"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False
        
    print "Alter food_collection_owner_language - add new column pdf_blob"
    sqlQ = """ALTER TABLE `food_collection_owner_language`
        ADD `pdf_blob` MEDIUMBLOB  NULL  DEFAULT NULL  AFTER `filename`"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False


    return SetDbVersion('R3e')

def UpgradeFromR3eToR3f():
    """
    Function that upgrades the database from version R3e to R3f.
    """
    print "\n== Upgrading from version R3e to R3f =="

    print "Alter deletion_log - add new columns for devices and remove link to pcode"
    sqlQ = """ALTER TABLE deletion_log
        DROP FOREIGN KEY `deletion_log_ibfk_1`,
        ADD COLUMN n_meters INT(10) UNSIGNED DEFAULT NULL,
        ADD COLUMN n_pumps INT(10) UNSIGNED DEFAULT NULL,
        ADD COLUMN n_combodevices INT(10) UNSIGNED DEFAULT NULL,
        ADD COLUMN n_pens INT(10) UNSIGNED DEFAULT NULL,
        ADD COLUMN n_caps INT(10) UNSIGNED DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
        
    return SetDbVersion('R3f')

def UpgradeFromR3fToR3g():
    """
    Function that upgrades the database from version R3f to R3g.
    """
    print "\n== Upgrading from version R3f to R3g =="

    print "Create the table access_token_owner_patient"

    sqlQ = """CREATE TABLE `access_token_owner_patient` (
        `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
        `access_token_id` INT(11) NOT NULL,
        PRIMARY KEY (`pcode`, `access_token_id`),
        FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`),
        FOREIGN KEY (`access_token_id`) REFERENCES `access_token` (`seq`))
        ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Creating table login_token"
    sqlQ = """CREATE TABLE `login_token` (
          `seq` INT(11) NOT NULL AUTO_INCREMENT,
          `token` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL,
          `expire` DATETIME NULL,
          `access_count` INT NOT NULL DEFAULT 0,
          PRIMARY KEY (`seq`),
          UNIQUE INDEX `login_token_idx_1` (`token`)) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Create table oauth_client_owner_patient"
    sqlQ = """CREATE TABLE `oauth_client_owner_patient` (
          `seq` INT(11) NOT NULL AUTO_INCREMENT,
          `oauth_client_id` INT(11) NOT NULL,
          `pcode` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
          `state` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
          PRIMARY KEY (`seq`),
          UNIQUE INDEX `oauth_client_owner_patient_idx_1` (`oauth_client_id`, `pcode`), 
          CONSTRAINT `oauth_client_owner_patient_ibfk_1`
            FOREIGN KEY (`oauth_client_id`)
            REFERENCES `oauth_client` (`seq`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `oauth_client_owner_patient_ibfk_2`
            FOREIGN KEY (`pcode`)
            REFERENCES `users` (`pcode`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Create table oauth_client_owner_patient_history"
    sqlQ = """CREATE TABLE `oauth_client_owner_patient_history` (
          `seq` INT(11) NOT NULL AUTO_INCREMENT,
          `oauth_client_owner_patient_id` INT(11) NOT NULL,
          `timestamp` datetime NOT NULL,
          `admin` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' DEFAULT NULL,
          `clinic_user` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' DEFAULT NULL,
          `pcode` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_bin' DEFAULT NULL,
          `action` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
          PRIMARY KEY (`seq`),
          CONSTRAINT `oauth_client_owner_patient_history_ibfk_1`
            FOREIGN KEY (`oauth_client_owner_patient_id`)
            REFERENCES `oauth_client_owner_patient` (`seq`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `oauth_client_owner_patient_history_ibfk_2`
            FOREIGN KEY (`admin`)
            REFERENCES `admins` (`name`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `oauth_client_owner_patient_history_ibfk_3`
            FOREIGN KEY (`clinic_user`)
            REFERENCES `clinic_users` (`username`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `oauth_client_owner_patient_history_ibfk_4`
            FOREIGN KEY (`pcode`)
            REFERENCES `users` (`pcode`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Create table login_token_owner_account"
    sqlQ = """CREATE TABLE `login_token_owner_account` (
          `seq` INT(11) NOT NULL AUTO_INCREMENT,
          `account_id` INT(10) UNSIGNED NOT NULL,
          `login_token_id` INT(11) NOT NULL,
          PRIMARY KEY (`seq`),
          UNIQUE INDEX `login_token_owner_account_idx_1` (`login_token_id`),
          CONSTRAINT `account_cns`
            FOREIGN KEY (`account_id`)
            REFERENCES `account` (`seq`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `login_token_cns`
            FOREIGN KEY (`login_token_id`)
            REFERENCES `login_token` (`seq`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Create table oauth_client_setting"
    sqlQ = """CREATE TABLE `oauth_client_setting` (
          `seq` INT(11) NOT NULL AUTO_INCREMENT,
          `oauth_client_id` INT(11) NOT NULL,
          `key` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
          `value` VARCHAR(512) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL,
          PRIMARY KEY (`seq`),
          UNIQUE INDEX `oauth_client_setting_idx_1` (`oauth_client_id`, `key`),
          CONSTRAINT `oauth_client_id`
            FOREIGN KEY (`oauth_client_id`)
            REFERENCES `oauth_client` (`seq`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Increase terminals comment size"
    sqlQ = """ALTER TABLE terminals MODIFY comment TEXT"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False
        
    print "Increase admins email size, and setting diasend default address"
    sqlQ = """ALTER TABLE admins MODIFY email VARCHAR(60) NOT NULL DEFAULT 'info@diasend.com'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False
        
    print "Increase admins username size:"
    print " * admins"
    sqlQ = """ALTER TABLE admins MODIFY name VARCHAR(30)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9a"
        return False 
    print " * admin_hsa_ids"
    sqlQ = """ALTER TABLE admin_hsa_ids MODIFY admin VARCHAR(30) NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9b"
        return False 
    print " * admin_properties"
    sqlQ = """ALTER TABLE admin_properties MODIFY admin VARCHAR(30) NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9c"
        return False 
    print " * clinic_users"
    sqlQ = """ALTER TABLE clinic_users MODIFY admin VARCHAR(30) NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9d"
        return False 
    print " * food_collection_owner_clinic"
    sqlQ = """ALTER TABLE food_collection_owner_clinic MODIFY admin VARCHAR(30) DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9e"
        return False 
    print " * notification_view_log_clinic"
    sqlQ = """ALTER TABLE notification_view_log_clinic MODIFY admin VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9f"
        return False 
    print " * pdf_profiles"
    sqlQ = """ALTER TABLE pdf_profiles MODIFY admin VARCHAR(30) NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9g"
        return False 
    print " * terminals"
    sqlQ = """ALTER TABLE terminals MODIFY admin VARCHAR(30) NOT NULL DEFAULT ''"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9h"
        return False 
    print " * user_ownership"
    sqlQ = """ALTER TABLE user_ownership MODIFY admin VARCHAR(30) DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9i"
        return False 
        
    print "Change database engine to from MyISAM to InnoDB:"
    print " * database_servers"
    sqlQ = """ALTER TABLE database_servers ENGINE = InnoDB"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10a"
        return False 
    print " * errorlog"
    sqlQ = """ALTER TABLE errorlog ENGINE = InnoDB"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10b"
        return False 
    print " * metadata"
    sqlQ = """ALTER TABLE metadata ENGINE = InnoDB"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10c"
        return False 
    print " * terminal_locations"
    sqlQ = """ALTER TABLE terminal_locations ENGINE = InnoDB"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10d"
        return False 
    print " * terminal_permissions"
    sqlQ = """ALTER TABLE terminal_permissions ENGINE = InnoDB"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10e"
        return False 
        
    print "Moving Correction programs to ISF programs:"
    print " * device_program "
    sqlQ = """UPDATE device_program SET type='isf_program' WHERE type='correction_program'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 11a"
        return False 
    print " * device_programs"
    sqlQ = """UPDATE device_programs SET type='isf_program' WHERE type='correction_program'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 11b"
        return False 
        
    print "Deleting the Correction program enum type:"
    print " * device_program"
    sqlQ = """ALTER TABLE device_program
                MODIFY COLUMN type 
                    ENUM('basal_program','isf_program','ic_ratio_program','bg_target_program','bg_threshold_program') NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 12a"
        return False 
    print " * device_programs"
    sqlQ = """ALTER TABLE device_programs
                MODIFY COLUMN type 
                    ENUM('basal_program','isf_program','ic_ratio_program','bg_target_program','bg_threshold_program') NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 12b"
        return False 

    print "Adding BG Threshold to device program value"
    sqlQ = """ALTER TABLE device_program_value
                MODIFY COLUMN type 
                    ENUM('bg_deviation','bg_threshold') DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 13"

    print "Adding BG Threshold to device program values (old device setting structure)"
    sqlQ = """ALTER TABLE device_program_values
                MODIFY COLUMN type 
                    ENUM('bg_deviation','bg_threshold') DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 14"

    return SetDbVersion('R3g')
    
def UpgradeFromR3gToR4a():
    """
    Function that upgrades the database from version R3g to R4a.
    """
    print "\n== Upgrading from version R3g to R4a =="

    print "Create index on terminals table (admin)"
    sqlQ = """CREATE INDEX term_admin_ix on terminals (admin)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Increase terminals:phone field (used for comments)"
    sqlQ = """ALTER TABLE terminals MODIFY phone TEXT"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
        
    print "Add 'free' enum to registration_events"
    sqlQ = """ALTER TABLE registration_events MODIFY COLUMN payment_method ENUM('trial','payment','licensecode','free') DEFAULT 'free'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Increase size of value column in terminal_properties"
    sqlQ = """ALTER TABLE terminal_properties MODIFY COLUMN `value` VARCHAR(512)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Create index on telemeddata (this might take some time, be patient...)"
    sqlQ = """CREATE INDEX ix_telemeddata ON telemeddata (device_id,type,timestamp) USING btree"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False
        
    print "Removing table asmin_hsa_ids"
    sqlQ = """DROP TABLE admin_hsa_ids"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False
        
    print "Creating table audit_log"
    sqlQ = """CREATE TABLE audit_log (
                `seq` INT(10) NOT NULL auto_increment,
                `timestamp` datetime NOT NULL,
                `medium` VARCHAR(128) NOT NULL,
                `parent_clinic` VARCHAR(128) NULL DEFAULT NULL,
                `actor` VARCHAR(128) NULL DEFAULT NULL,
                `target` VARCHAR(128) NOT NULL,
                `action_type` ENUM('review','pdf_export','excel_export','profile_update','account_creation','deleted_account') NOT NULL,
                PRIMARY KEY (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False

    print "Creating table audit_log_field_change"
    sqlQ = """CREATE TABLE audit_log_field_change (
                `seq` INT(10) NOT NULL auto_increment,
                `audit_log_id` int(10) NOT NULL,
                `field` VARCHAR(128) NOT NULL,
                `from` VARCHAR(128) NULL default NULL,
                `to` VARCHAR(128) NOT NULL,
                PRIMARY KEY (`seq`),
                FOREIGN KEY (`audit_log_id`) REFERENCES `audit_log` (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False
        
    print "Creating table activity_source"
    sqlQ = """create table activity_source (
                `id` varchar(100) not null,
                `name` varchar(100),
                primary key (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False                
                
    print "Creating table activity"
    sqlQ = """create table activity (
                `seq` int(10) not null auto_increment,
                `timestamp` datetime not null,
                `type` varchar(100) not null,
                `start_time` datetime not null,
                `duration` int(10) not null,
                `calories` int(10) not null,
                `steps` int(10) not null,
                `distance` int(10) not null,
                `source` varchar(100) not null,
                `external_activity_id` varchar(100),
                `transfer_id` int(10) unsigned not null,
                primary key (`seq`),
                CONSTRAINT `activity_ibfk_1` FOREIGN KEY (`transfer_id`) REFERENCES `transfers` (`seq`),
                CONSTRAINT `activity_ibfk_2` FOREIGN KEY (`source`) REFERENCES `activity_source` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10"
        return False    
                       
    print "Creating table activity_value"
    sqlQ = """create table activity_value (
                `seq` int(10) not null auto_increment,
                `type` varchar(100) not null,
                `value` varchar(100) not null,
                `activity_id` int(10) not null,
                primary key (`seq`),
                CONSTRAINT `activity_value_ibfk_1` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`seq`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 11"
        return False   
                        
    print "Inserting default activity sources"
    sqlQ = """insert into activity_source values 
                ('movesapp', 'Moves'),
                ('healthgraph', 'RunKeeper'),
                ('fitbit', 'Fitbit'),
                ('jawbone_up', 'Jawbone UP'),
                ('nikeplus', 'Nike+')"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 12"
        return False

    print "Adding index to login_activity"
    sqlQ = """create index ix_login_activity on login_activity (timestamp, username, ip, event) using btree"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 13"
        return False

    print "Adding date of birth column to users table"
    sqlQ = """ALTER TABLE `users` ADD `dateofbirth` DATE  NULL  AFTER `pnumber`"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 14"
        return False

    print "Adding comment column to registration_events table"
    sqlQ = """ALTER TABLE `registration_events` ADD `comment` TEXT  NULL  AFTER `new_expire_date`"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 15"
        return False

    print "Add enum types for suggested meal/corr and total programmed bolus to telemeddata_values"
    sqlQ = """ALTER TABLE telemeddata_values 
        MODIFY COLUMN type enum('temperature', 'duration', 'insulin_basal_tdd', 'insulin_bolus_ext', 'insulin_bolus_suggested', 'insulin_bolus_prog_meal', 'insulin_bolus_prog_corr', 'insulin_bolus_iob', 'insulin_bolus_override', 'insulin_bolus_sugg_corr', 'insulin_bolus_sugg_meal', 'insulin_bolus_programmed') NOT NULL DEFAULT 'temperature'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 16"
        return False
        
    print "Adding email column to lostpassword_keys table"
    sqlQ = """ALTER TABLE `lostpassword_keys` ADD `email` VARCHAR(128)  BINARY  NOT NULL  DEFAULT ''  AFTER `pcode`"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 17"
        return False

    print "Adding status column to lostpassword_keys table"
    sqlQ = """ALTER TABLE `lostpassword_keys` ADD `status` ENUM('active', 'complete')  NOT NULL  DEFAULT 'active'  AFTER `key`"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 18"
        return False

    print "Allowing null on pcode, lostpassword_keys table"
    sqlQ = """ALTER TABLE `lostpassword_keys` CHANGE `pcode` `pcode` VARCHAR(128) BINARY  NULL  DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 19"
        return False
        
    return SetDbVersion('R4a');

def UpgradeFromR4aToR4b():
    """
    Function that upgrades the database from version R4a to R4b.
    """
    print "\n== Upgrading from version R4a to R4b =="
    
    print "Add uuid type on telemeddata_values table"
    sqlQ = """ALTER TABLE telemeddata_values 
        MODIFY COLUMN type enum('temperature', 'duration', 'insulin_basal_tdd', 'insulin_bolus_ext', 'insulin_bolus_suggested', 'insulin_bolus_prog_meal', 'insulin_bolus_prog_corr', 'insulin_bolus_iob', 'insulin_bolus_override', 'insulin_bolus_sugg_corr', 'insulin_bolus_sugg_meal', 'insulin_bolus_programmed', 'uuid') NOT NULL DEFAULT 'temperature'"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Add api_log table"
    sqlQ = """CREATE TABLE `api_log` (
                `seq` int(10) NOT NULL AUTO_INCREMENT,
                `timestamp` datetime NOT NULL,
                `request_uri` varchar(128) NOT NULL,
                `request_method` varchar(128) NOT NULL,
                `request_data` varchar(128) NOT NULL,
                `response_code` varchar(128) NOT NULL,
                `response_body` varchar(128) NOT NULL,
                `request_user_agent` varchar(128) NOT NULL,
                `api_client` varchar(128) NOT NULL,
                `api_scope` varchar(128) NOT NULL,
                `api_resource_name` varchar(128) NOT NULL,
                `api_resource_database` varchar(128) NOT NULL,
                `time_ms` int(11) DEFAULT NULL,
                PRIMARY KEY (`seq`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
        
    print "Add 'no_diabetes' type on users table"
    sqlQ = """ALTER TABLE users 
        MODIFY COLUMN diabetes_type enum('general','type_1','type_2','gestational','other','no_diabetes') COLLATE utf8_bin DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False        

    print "Drop access_token_owner_trial table"
    sqlQ = """DROP TABLE access_token_owner_trial""" 
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Drop device_owner_trial table"
    sqlQ = """DROP TABLE device_owner_trial"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Drop trial table"
    sqlQ = """DROP TABLE trial""" 
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False
        
    return SetDbVersion('R4b')


def UpgradeFromR4bToR4c():
    """
    Function that upgrades the database from version R4b to R4c.
    """
    print "\n== Upgrading from version R4b to R4c =="
    
    print "Increase data size on user_strings:value"
    sqlQ = """ALTER TABLE user_strings MODIFY COLUMN `value` text"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False
        
    print "Create table access_token_owner_cua"
    sqlQ = """CREATE TABLE `access_token_owner_cua` (
        `admin` varchar(30) COLLATE utf8_bin NOT NULL,
        `access_token_id` int(11) NOT NULL,
        PRIMARY KEY (`admin`, `access_token_id`),
        KEY `access_token_id` (`access_token_id`),
        CONSTRAINT `access_token_owner_cua_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`),
        CONSTRAINT `access_token_owner_cua_ibfk_2` FOREIGN KEY (`access_token_id`) REFERENCES `access_token` (`seq`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
   
    return SetDbVersion('R4c')
    
def UpgradeFromR4cToR4d():
    """
    Function that upgrades the database from version R4c to R4d.
    """
    print "\n== Upgrading from version R4c to R4d =="

    print "Create index on table history"
    sqlQ = """CREATE INDEX idx_history_terminal_device ON history(terminal_id, device_id)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Create index for timestamp on table history"
    sqlQ = """CREATE INDEX idx_history_timestamp ON history(timestamp)"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False
    
    print "Run analyze on table history"
    sqlQ = """ANALYZE TABLE history"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Create new devicetype device_cloud"
    sqlQ = """ALTER TABLE devices MODIFY COLUMN `deviceclass` 
        enum('device_meter','device_pump','device_pen','device_pen_cap','device_phone','device_combo','device_user', 'device_cloud')"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Update food entry sizes (1/2)"
    sqlQ = """ALTER TABLE food_item MODIFY COLUMN name VARCHAR(100) COLLATE utf8_bin DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Update food entry sizes (2/2)"
    sqlQ = """ALTER TABLE food_item MODIFY COLUMN servings VARCHAR(100) COLLATE utf8_bin DEFAULT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    return SetDbVersion('R4d')


def UpgradeFromR4dToR4e():
    """
    Function that upgrades the database from version R4d to R4e.
    """
    print "\n== Upgrading from version R4d to R4e =="

    print "Create table sim_card"
    sqlQ = """
        CREATE TABLE `sim_card` (
            `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `iccid` varchar(22) NOT NULL,
            `imsi` varchar(15) NOT NULL,
            `msisdn` varchar(16) NOT NULL,
            `order_no` varchar(10) DEFAULT NULL,
            PRIMARY KEY (`seq`),
            UNIQUE KEY `iccid` (`iccid`),
            UNIQUE KEY `imsi` (`imsi`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Add updated_by field to terminals"
    sqlQ = """
        ALTER TABLE terminals
            ADD COLUMN updated_by varchar(30) COLLATE utf8_bin DEFAULT ''
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Create table terminal_event_log"
    sqlQ = """
        CREATE TABLE `terminal_event_log` (
            `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `terminal_id` int(10) unsigned NOT NULL,
            `event_type` enum('creation','edition') COLLATE utf8_bin NOT NULL,
            `admin` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
            `phone` text COLLATE utf8_bin,
            `comment` text COLLATE utf8_bin,
            `location` tinytext COLLATE utf8_bin,
            `updated_by` varchar(30) COLLATE utf8_bin DEFAULT '',
            `created_at` datetime NOT NULL,
            `ended_at` datetime DEFAULT NULL,
            PRIMARY KEY (`seq`),
            CONSTRAINT `terminal_event_log_ibfk_1` FOREIGN KEY (`terminal_id`) REFERENCES `terminals` (`seq`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Create after_insert_terminals trigger"
    sqlQ = """
        CREATE TRIGGER after_insert_terminals
            AFTER INSERT ON terminals FOR EACH ROW
            BEGIN
                INSERT INTO terminal_event_log (terminal_id, event_type, admin, phone, comment, location, updated_by, created_at)
                    VALUES (NEW.seq, 'creation', NEW.admin, NEW.phone, NEW.comment, NEW.location, NEW.updated_by, NOW());
            END;
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Create after_update_terminals trigger"
    sqlQ = """
        CREATE TRIGGER after_update_terminals
            AFTER UPDATE ON terminals FOR EACH ROW
            BEGIN
                IF NOT ((OLD.admin <=> NEW.admin) AND
                    (OLD.phone <=> NEW.phone) AND
                    (OLD.comment <=> NEW.comment) AND
                    (OLD.location <=> NEW.location) AND
                    (OLD.updated_by <=> NEW.updated_by))
                THEN
                    UPDATE terminal_event_log
                        SET ended_at=NOW()
                        WHERE terminal_id=NEW.seq AND ended_at IS NULL;
                    INSERT INTO terminal_event_log (terminal_id, event_type, admin, phone, comment, location, updated_by, created_at)
                        VALUES (NEW.seq, 'edition', NEW.admin, NEW.phone, NEW.comment, NEW.location, NEW.updated_by, NOW());
                END IF;
            END;
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Create terminal_property_event_log"
    sqlQ = """
        CREATE TABLE `terminal_property_event_log` (
            `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `event_type` enum('creation','edition') COLLATE utf8_bin NOT NULL,
            `terminal_id` int(10) unsigned NOT NULL,
            `property` varchar(32) COLLATE utf8_bin NOT NULL,
            `value` varchar(512) COLLATE utf8_bin DEFAULT NULL,
            `created_at` datetime NOT NULL,
            `ended_at` datetime DEFAULT NULL,
            PRIMARY KEY (`seq`),
            CONSTRAINT `terminal_property_event_log_ibfk_1` FOREIGN KEY (`terminal_id`, `property`) REFERENCES `terminal_properties` (`terminal_id`, `property`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Create after_insert_terminal_properties trigger"
    sqlQ = """
        CREATE TRIGGER after_insert_terminal_properties
            AFTER INSERT ON terminal_properties FOR EACH ROW
            BEGIN
                INSERT INTO terminal_property_event_log (event_type, terminal_id, property, value, created_at)
                    VALUES ('creation', NEW.terminal_id, NEW.property, NEW.value, NOW());
            END;
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False

    print "Create after_update_terminal_properties trigger"
    sqlQ = """
        CREATE TRIGGER after_update_terminal_properties
            AFTER UPDATE ON terminal_properties FOR EACH ROW
            BEGIN
                UPDATE terminal_property_event_log
                    SET ended_at=NOW()
                    WHERE (terminal_property_event_log.terminal_id=NEW.terminal_id AND terminal_property_event_log.property=NEW.property AND ended_at IS NULL);
                INSERT INTO terminal_property_event_log (event_type, terminal_id, property, value, created_at)
                    VALUES ('edition', NEW.terminal_id, NEW.property, NEW.value, NOW());
            END;
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False

    print "Expand pdf_generated.clinic column"
    sqlQ = """ALTER TABLE pdf_generated MODIFY clinic VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL"""
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False 

    return SetDbVersion('R4e')

def UpgradeFromR4eToR4f():
    """
    Function that upgrades the database from version R4d to R4e.
    """
    print "\n== Upgrading from version R4e to R4f =="

  
    print "Drop constraint on telemeddata_flags"
    sqlQ = """
           ALTER TABLE telemeddata_flags DROP FOREIGN KEY `telemeddata_flags_ibfk_1`
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Change telemeddata_seq on telemeddata_flags to bigint"
    sqlQ = """
           ALTER TABLE telemeddata_flags CHANGE COLUMN telemeddata_seq telemeddata_seq bigint unsigned NOT NULL DEFAULT '0'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Drop constraint on telemeddata_values"
    sqlQ = """
           ALTER TABLE telemeddata_values DROP FOREIGN KEY `telemeddata_values_ibfk_1`
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Change telemeddata_seq on telemeddata_values to bigint"
    sqlQ = """
           ALTER TABLE telemeddata_values CHANGE COLUMN telemeddata_seq telemeddata_seq bigint unsigned NOT NULL DEFAULT '0'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Change seq on telemeddata to bigint"
    sqlQ = """
           ALTER TABLE telemeddata CHANGE COLUMN seq seq bigint unsigned NOT NULL AUTO_INCREMENT
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Add foreign key on telemeddata_flags"
    sqlQ = """
           ALTER TABLE telemeddata_flags ADD CONSTRAINT `telemeddata_flags_ibfk_1` FOREIGN KEY (`telemeddata_seq`) REFERENCES `telemeddata` (`seq`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Add foreign key on telemeddata_values"
    sqlQ = """
           ALTER TABLE telemeddata_values ADD CONSTRAINT `telemeddata_values_ibfk_1` FOREIGN KEY (`telemeddata_seq`) REFERENCES `telemeddata` (`seq`)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False

    return SetDbVersion('R4f')
    

def CheckExistsHCP(hcp):
    sqlQ = """
        SELECT * FROM admin_properties WHERE property='hcp_account_id' AND value = %s
    """
    paramQ = (str(hcp))
    results = PerformQuery(sqlQ, paramQ)

    if results:
       print "HCP ", hcp, " exists!"
       return True
    else:
       return False 
    
def GenerateHCP(): 
    # Loop around until we find a unique HCP ID
    while 1:
        hcp = ""
        for c in random.sample("0123456789", 7):
            hcp += c

        hcp = hcp[:2] + "-" + hcp[2:]

        if CheckExistsHCP(hcp) == False:
            return hcp    
    
def GenerateHCPaccountIDs():
    """
    Function that generates unique HCP IDs for all admins
    """

    sqlQ = """
        SELECT name FROM admins
    """
    results = PerformQuery(sqlQ, ())
    if results == False:
        print "Failed getting admin names"
        return False

    for line in results:
        hcp = GenerateHCP()
        sqlQ = """
            INSERT INTO admin_properties (admin, property, value) VALUES (%s, 'hcp_account_id', %s)
        """
        paramQ = (str(line[0]), str(hcp))
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed inserting properties"
            return False

    return True


def UpgradeToR5UserFunctionality():
    """
    Function that upgrades the database to R5web.
    """
    print "upgrading to R5web"

    print "Modifying communication table"
    sqlQ = """
        ALTER TABLE `communication`
            MODIFY COLUMN `pcode` VARCHAR(128) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Modifying device_ownership table"
    sqlQ = """
        ALTER TABLE `device_ownership`
            MODIFY COLUMN `pcode` VARCHAR(128) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

    print "Modifying moblog table"
    sqlQ = """
        ALTER TABLE `moblog`
            MODIFY COLUMN `pcode` VARCHAR(128) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False

    print "Modifying users table"
    sqlQ = """
        ALTER TABLE `users`
            MODIFY COLUMN `pcode` VARCHAR(128) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False

    print "Modifying user_properties table"
    sqlQ = """
        ALTER TABLE `user_properties`
            MODIFY COLUMN `pcode` VARCHAR(128) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Modifying user_strings table"
    sqlQ = """
        ALTER TABLE `user_strings`
            MODIFY COLUMN `pcode` VARCHAR(128) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Adding user_ownership table"
    sqlQ = """
        CREATE TABLE `user_ownership` (
            `pcode` varchar(128) NOT NULL,
            `admin` varchar(10),
            `comment` varchar(255) default NULL,
            `access_type` enum('access_normal','access_srpa','access_view') default 'access_normal',
            PRIMARY KEY  (`pcode`),
            KEY `admin` (`admin`),
            FOREIGN KEY (`pcode`) REFERENCES users(`pcode`),
            FOREIGN KEY (`admin`) REFERENCES admins(`name`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False
    
    print "Converting data"
    sqlQ = """
        INSERT INTO `user_ownership` (`pcode`,`admin`)
            SELECT `pcode`, `admin` FROM `users` WHERE `admin` <> ""
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False

    print "Removing foreign key constraint users_ibfk_1"
    sqlQ = """
        ALTER TABLE `users` DROP FOREIGN KEY `users_ibfk_1`
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False

    print "Dropping old column admin in table users"
    sqlQ = """
        ALTER TABLE users DROP COLUMN admin
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10"
        return False

    print "Adding dibs_transactions table"
    sqlQ = """
        CREATE TABLE `dibs_transactions` (
            `id` int(11) NOT NULL auto_increment,
            `pcode` varchar(128) NOT NULL,
            `referenceno` varchar(128) NOT NULL,
            `reply` varchar(128) NOT NULL,
            `replytext` varchar(128) NOT NULL,
            `verifyid` varchar(128) NOT NULL,
            `sum` varchar(128) NOT NULL,
            `currency` varchar(128) NOT NULL,
            `arraydata` text,
            `transactiontime` timestamp NOT NULL default CURRENT_TIMESTAMP,
            PRIMARY KEY  (`id`),
            KEY `pcode` (`pcode`),
            KEY `referenceno` (`referenceno`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 11"
        return False

    print "Adding licenscodes table"
    sqlQ = """
        CREATE TABLE `licensecodes` (
            `licensecode` varchar(8) NOT NULL,
            `distributor` varchar(128) NOT NULL,
            `subscription_level` enum('full','limited') default 'full',
            `subscription_time` enum('30_days','60_days','3_months','6_months','12_months') default '30_days',
            `limitation` enum('no_limit','only_new','only_registered') default 'no_limit',
            `validity_start` timestamp NOT NULL default CURRENT_TIMESTAMP,
            `validity_end` timestamp NOT NULL,
            `max_activations` varchar(8) NOT NULL,
            `comment` text,
            PRIMARY KEY  (`licensecode`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 12"
        return False

    print "Adding user_licensecodes table"
    sqlQ = """
        CREATE TABLE `user_licensecodes` (
            `licensecode` varchar(8) NOT NULL,
            `pcode` varchar(128) NOT NULL,
            PRIMARY KEY  (`licensecode`,`pcode`),
            CONSTRAINT `user_licensecodes_ibfk_1` FOREIGN KEY (`licensecode`) REFERENCES `licensecodes` (`licensecode`),
            CONSTRAINT `user_licensecodes_ibfk_2` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 13"
        return False

    print "Adding registration_events table"
    sqlQ = """
        CREATE TABLE `registration_events` (
            `seq` int(11) NOT NULL auto_increment,
            `pcode` varchar(128) NOT NULL,
            `event_type` enum('new_registration','subscription_renewal') default 'new_registration',
            `event_time` timestamp NOT NULL default CURRENT_TIMESTAMP,
            `payment_method` enum('trial','payment','licensecode') default 'trial',
            `payment_data` varchar(32),
            `new_expire_date` timestamp,
            PRIMARY KEY  (`seq`),
            KEY `pcode` (`pcode`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 14"
        return False

    return True 


def UpgradeFromR1eToR1f():
    """
    Function that upgrades the database from version R1e to R1f
    """
    print "\n== Upgrading from version R1e to R1f =="

    print "Remove old depricated tables"

    DropTable("history_old")
    DropTable("insulin_pump_basal_programs")
    DropTable("insulin_pump_data_flags")
    DropTable("insulin_pump_data")
    DropTable("insulin_pump_isf_programs")
    DropTable("insulin_pump_settings")
    DropTable("telemeddata_old")


    print "Update the telemeddata_values to hold insulin_bolus_ext aswell"

    sqlQ = """
        alter table telemeddata_values modify column type ENUM('temperature', 'duration', 'insulin_basal_tdd', 'insulin_bolus_ext') NOT NULL
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 0"
        return False


    print "Updating resolution of glucose and ketone values"

    sqlQ = """
     update telemeddata set value = value * 100 where type = 'ketones' or type = 'glucose'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False


    print "Creating the super admin table"

    sqlQ = """
        CREATE TABLE super_admins (
        `name` varchar(30) NOT NULL,
        `password` varchar(30) NOT NULL,
        `the_one_and_only` BOOL DEFAULT 0,
        `comment` TEXT,
        PRIMARY KEY (`name`)
        )DEFAULT CHARSET=utf8 TYPE=InnoDB    
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False


    print "Inserting diadmin into the superadmin table"
    sqlQ = """
        INSERT INTO super_admins
        VALUES ('diadmin', 'b0ttenf1ske', 1, NULL)
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 3"
        return False


    print "admins is updated to InnoDB"
    sqlQ = """
        ALTER TABLE admins
        ENGINE = InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 4"
        return False


    print "Update the admins table so each admin have a super_admin"
    sqlQ = """
        alter table admins
        add column super_admin VARCHAR(30) default 'diadmin'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5"
        return False

    print "Dropping the hours column of admins"
    sqlQ = """
        alter table admins
        drop column hours
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 5.1"
        return False

    print "Update the admins table to have a foreign key in the super_admin table"
    sqlQ = """
        alter table admins
        ADD FOREIGN KEY (`super_admin`) REFERENCES super_admins(`name`)        
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False

    print "Update the terminal table to not allow null transfers and null amount"
    sqlQ = """
        alter table terminals
        CHANGE COLUMN `nr_transfers` `nr_transfers` int(11) NOT NULL default 0
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 7"
        return False

    sqlQ = """
        alter table terminals
        CHANGE COLUMN `amount_data` `amount_data` int(11) NOT NULL default 0
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 8"
        return False

    print "Deleting depricated flag FLAG_BOLUS_COMBO"
    sqlQ = """
        DELETE FROM telemeddata_flags
        WHERE flag = 1002
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False

    print "Deleting depricated flag FLAG_BOLUS_TYPE_NORMAL"
    sqlQ = """
        DELETE FROM telemeddata_flags
        WHERE flag = 1009
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 10"
        return False

    print "Update Animas alarm codes"
    if UpdateAnimasAlarmcodes() == False:
        print "Failed 11"
        return False

    print "Changing the admin properties value type, from integers to varchars"
    sqlQ = """
        ALTER TABLE admin_properties MODIFY COLUMN value VARCHAR(32) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 12"
        return False

    print "Adding terminal class"
    sqlQ = """
        ALTER TABLE terminals ADD COLUMN terminalclass ENUM('terminal_diasend', 'terminal_smartpix') DEFAULT 'terminal_diasend'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 13"
        return False
    
    print "Modifying ecap extra properties"
    sqlQ = """
        UPDATE device_ownership SET extra = %s WHERE extra = %s
    """
    if PerformQuery(sqlQ, ("ST1", "bolus")) == False:
        print "Failed 14"
        return False
    if PerformQuery(sqlQ, ("LT1", "basal")) == False:
        print "Failed 15"
        return False

    print "Update the length of metadata values"
    sqlQ = """
        ALTER TABLE metadata MODIFY COLUMN value VARCHAR(64) NOT NULL
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 16"
        return False

    return SetDbVersion('R1f')


def UpgradeFromR1dToR1e():
    """
    Function that upgrades the database from version R1d to R1e
    """
    print "\n== Upgrading from version R1d to R1e =="

    print "Update the telemeddata table to hold insulin data aswell"
    sqlQ = """
        alter table telemeddata modify column type ENUM('ketones', 'glucose', 'pen_cap_off', 'insulin_basal','insulin_bolus','insulin_tdd','insulin_basal_tdd','insulin_prime','insulin_alarm','insulin_bolus_basal') NOT NULL
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 1"
        return False

    print "Update the telemeddata_values to hold durations and insulin_basal_tdd aswell"

    sqlQ = """
        alter table telemeddata_values modify column type ENUM('temperature', 'duration', 'insulin_basal_tdd') NOT NULL
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 2"
        return False

# Of some reasons there where 5 duplicated TDD:s in the database(?), will remove them...
    print "There are some duplicated TDD:s will remove them"
    sqlQ = """
        DELETE a.* FROM insulin_pump_data AS a, insulin_pump_data AS b
        WHERE a.type = %s
        AND b.type = %s
        AND a.timestamp = b.timestamp
        AND a.device_id = b.device_id
        AND a.seq != b.seq
        AND b.seq > a.seq;
    """
    paramQ = ("insulin_tdd", "insulin_tdd")
    results = PerformQuery(sqlQ, paramQ)
    if results == False:
        print "Failed 2.0.1"
        return False

    paramQ = ("insulin_basal_tdd", "insulin_basal_tdd")
    results = PerformQuery(sqlQ, paramQ)
    if results == False:
        print "Failed 2.0.2"
        return False


    print "Will move data from insulin_pump_data into telemeddata, all that do not flags"
    sqlQ = """
        SELECT * from insulin_pump_data
        WHERE seq NOT IN (SELECT insulin_pump_data_seq FROM insulin_pump_data_flags);
    """
    results = PerformQuery(sqlQ, ())
    if results == False:
        print "Failed 2.1"
        return False

# Data looks like this
#+-----+-----------+-------------+---------------------+---------------+-------+
#| seq | device_id | terminal_id | timestamp           | type          | value |
#+-----+-----------+-------------+---------------------+---------------+-------+
#|   1 |      2361 |         186 | 2006-10-31 16:02:00 | insulin_basal |     0 | 
#+-----+-----------+-------------+---------------------+---------------+-------+

    print "Inserting data w/o flags"

    sqlQ = """
        INSERT INTO telemeddata (device_id, terminal_id, timestamp, type, value)
        VALUES (%s,%s,%s,%s,%s)
    """

    for elem in results:
        
        paramQ =  (str(elem[1]), str(elem[2]), str(elem[3]), str(elem[4]), str(elem[5]))
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed 2.2"
            return False
        

    print "Will move data from insulin_pump_data into telemeddata, all that have flags"
    sqlQ = """
        SELECT * from insulin_pump_data, insulin_pump_data_flags 
        WHERE seq = insulin_pump_data_seq
        ORDER BY seq 
    """
    results = PerformQuery(sqlQ, ())
    if results == False:
        print "Failed 3"
        return False

    last_seq = None
    last_id = None

# Results look like this
#    mysql> SELECT * from insulin_pump_data, insulin_pump_data_flags          WHERE seq = insulin_pump_data_seq         ORDER BY seq limit 10;
#    +------+-----------+-------------+---------------------+-------------------+--------+-----------------------+------+
#    | seq  | device_id | terminal_id | timestamp           | type              | value  | insulin_pump_data_seq | flag |
#    +------+-----------+-------------+---------------------+-------------------+--------+-----------------------+------+
#    | 4249 |       317 |           5 | 2007-02-06 11:02:00 | insulin_basal     |   1860 |                  4249 | 1001 |

    print "Inserting data with flags"

    for elem in results:
        if elem[0] != last_seq:
            # First row with this seq, do an insert in the telemeddata table
            last_seq = elem[0]
            sqlQ = """
            INSERT INTO telemeddata (device_id, terminal_id, timestamp, type, value)
            VALUES (%s,%s,%s,%s,%s)
            """
            
            paramQ =  (str(elem[1]), str(elem[2]), str(elem[3]), str(elem[4]), str(elem[5]))
            if PerformQuery(sqlQ, paramQ) == False:
                print "Failed 4"
                return False

            last_id = GetLastInsertID()
        
        # Insert the flag into telemeddata_flags
        sqlQ = """
        INSERT INTO telemeddata_flags (telemeddata_seq, flag)
        VALUES (%s,%s)
        """

        paramQ =  (str(last_id), str(elem[7]))
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed 5"
            return False


    print "Creating device_settings table"
    sqlQ = """
    CREATE TABLE device_settings (
    `device_id` int unsigned NOT NULL,
    `setting_id` int unsigned NOT NULL,
    `value` varchar(255),
    PRIMARY KEY (`device_id`, `setting_id`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 6"
        return False


    print "Moving data from insulin_pump_settings to device_settings"
    
    sqlQ = """
    SELECT * FROM insulin_pump_settings
    ORDER BY serialnumber
    """
    results = PerformQuery(sqlQ, ())
    if results == False:
        print "Failed 7"
        return False
    
    # Results look like this    
#    +-----+-----------+--------------+---------+-------+
#    | seq | pumptype  | serialnumber | setting | value |
#    +-----+-----------+--------------+---------+-------+
#    |   1 | Animas IR | 29432-105335 | 1       | 0     |
#    +-----+-----------+--------------+---------+-------+

    sqlQ = """
        INSERT INTO device_settings (device_id, setting_id, value)
        VALUES (%s, %s, %s)
    """
    old_serial = None
    old_id     = None
    
    for elem in results:
        if old_serial != elem[2]:
            old_serial = elem[2]
            # Look up the device ID
            old_id = GetDeviceId(old_serial)

        paramQ = (old_id, str(elem[3]), str(elem[4]))
            
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed " + sqlQ % paramQ
            return False

    print "Merging insulin_pump_basal_programs and insulin_pump_isf_programs into device_programs"
    print "Creating table"
    sqlQ = """
    CREATE TABLE device_programs (
    `device_id` int unsigned NOT NULL,
    `type` ENUM('basal_program', 'isf_program') NOT NULL,
    `name` varchar(30) NOT NULL,
    `period_start` time NOT NULL,
    `period_rate` int unsigned NOT NULL,
    INDEX (`device_id`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 9"
        return False

    print "moving insulin_pump_isf_programs... "
    sqlQ = """
    SELECT * FROM insulin_pump_isf_programs
    """
    results = PerformQuery(sqlQ, ())
    
    if results == False:
        print "Failed 10"
        return False
    
    # Results look like this
#        +-----+-----------+------+----------+------+
#        | seq | device_id | name | time     | isf  |
#        +-----+-----------+------+----------+------+
#        |   1 |      4900 | 1    | 00:00:00 | 2775 | 
#        +-----+-----------+------+----------+------+
    sqlQ = """
        INSERT INTO device_programs (device_id, type, name, period_start, period_rate)
        VALUES (%s, %s, %s, %s ,%s)
    """
    
    for elem in results:
        paramQ = (str(elem[1]), 'isf_program', str(elem[2]), str(elem[3]), str(elem[4]))
            
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed 11"
            return False

    print "moving insulin_pump_basal_programs... "
    
    sqlQ = """
    SELECT * FROM insulin_pump_basal_programs
    """
    results = PerformQuery(sqlQ, ())
    
    if results == False:
        print "Failed 12"
        return False

# Results look like:
#        +-----+-----------+--------------+------+--------------+-------------+
#        | seq | pumptype  | serialnumber | name | period_start | period_rate |
#        +-----+-----------+--------------+------+--------------+-------------+
#        |   1 | Animas IR | 29432-105335 | 1    | 00:00:00     |         600 | 
#        +-----+-----------+--------------+------+--------------+-------------+    
    old_id = None
    old_serial = None
    
    sqlQ = """
    INSERT INTO device_programs (device_id, type, name, period_start, period_rate)
    VALUES (%s, %s, %s, %s ,%s)
    """

    for elem in results:
        if old_serial != elem[2]:
            old_serial = elem[2]

            # Look up the device ID
            old_id = GetDeviceId(old_serial)

        # insert 
        paramQ = (str(old_id), 'basal_program', str(elem[3]), str(elem[4]), str(elem[5]))
            
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed 13"
            return False

    print "Removing the balance column from terminals"
    sqlQ = "ALTER TABLE terminals DROP COLUMN balance"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 14"
        return False


    print "Moving basal_tdd to be a secondary value to tdd"
    sqlQ = "Select a.value, b.seq, a.seq from telemeddata as a, telemeddata as b where a.type = %s and b.type = %s and a.timestamp = b.timestamp and a.device_id = b.device_id"
    paramQ = ("insulin_basal_tdd", "insulin_tdd")
    results = PerformQuery(sqlQ, paramQ)
    
    if results == False:
        print "Failed 15"
        return False

# Result looks like this
#+--------+------+
#| value  | seq  |
#+--------+------+
#| 151900 | 3253 |

    print "Inserting..."

    sqlQ = """
        INSERT INTO telemeddata_values (telemeddata_seq, type, value)
        VALUES (%s, %s, %s)
    """

    # Also delete flags for the basal-tdd, they're already applied to the TDD aswell
    sqlDel = """
        DELETE FROM telemeddata_flags
        WHERE telemeddata_seq = %s
    """

    for elem in results:
        paramQ = (str(elem[1]), "insulin_basal_tdd", str(elem[0]))
        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed 16"
            return False

        paramQ = (str(elem[2]))
        if PerformQuery(sqlDel, paramQ) == False:
            print "Failed 16.1"
            return False


    print "Removing from telemeddata"
    sqlQ = """
        DELETE FROM telemeddata where type = 'insulin_basal_tdd'
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 17"
        return False

    print "Creating transfers table"
    sqlQ = """
        CREATE TABLE transfers (
        `seq` int unsigned NOT NULL auto_increment,
        `timestamp` datetime NOT NULL,
        `terminal_id` int unsigned NOT NULL,
        `device_id` int unsigned NOT NULL,
        INDEX (`terminal_id`, `timestamp`),
        PRIMARY KEY(`seq`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 19"
        return False

    print "Creating new telemeddata"
    sqlQ = """
        CREATE TABLE telemeddata_new (
        `seq` int unsigned NOT NULL auto_increment,
        `device_id` int unsigned NOT NULL,
        `transfer_id` int unsigned NOT NULL,
        `timestamp` datetime NOT NULL,
        `type` ENUM('ketones', 'glucose', 'pen_cap_off', 'insulin_basal','insulin_bolus','insulin_tdd','insulin_prime','insulin_alarm','insulin_bolus_basal') NOT NULL,
        `value` int unsigned NOT NULL,
        INDEX (`device_id`, `timestamp`),
        PRIMARY KEY(`seq`),
        FOREIGN KEY (`transfer_id`) REFERENCES transfers(`seq`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 19.1"
        return False

    print "moving data..."
    if not CopyTelemeddataWithTerminalID():
        print "Failed to copy to new telemeddata"
        return False

    print "Renaming telemeddata"
    sqlQ = "RENAME TABLE telemeddata TO telemeddata_old"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 20"
        return False

    print "Renaming telemeddata_new"
    sqlQ = "RENAME TABLE telemeddata_new TO telemeddata"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 21"
        return False

    print "Updating history to use device ID"
    sqlQ = """
        CREATE TABLE history_new (
        `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
        `terminal_id` int unsigned,
        `device_id` int unsigned,
        `entry` tinytext NOT NULL,
        INDEX (`timestamp`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 22"
        return False

    print "Selecting old values"
    sqlQ = "SELECT * from history"
    results = PerformQuery(sqlQ, ())
    if results == False:
            print "Failed 23"
            return False
    
    sqlQ = "INSERT INTO history_new VALUES (%s, %s, %s, %s)"
    

#    +-----+---------------------+-----------------+--------------+-------+
#    | seq | ts                  | serial_terminal | serial_meter | entry |
#    +-----+---------------------+-----------------+--------------+-------+    
    for elem in results:
        dev_id = GetDeviceId(elem[3])
        if dev_id == False:
            dev_id_str = None
        else:
            dev_id_str = str(dev_id)
        
        terminal_id = GetTerminalId(elem[2])
        if terminal_id == False:
            term_id_str = None
        else:
            term_id_str = str(terminal_id)
        
        if elem[1] == None:
            ts_str = "2000-01-01 00:00"
        else:
            ts_str = str(elem[1])
        
        paramQ = (ts_str, term_id_str, dev_id_str, str(elem[4]))

        if PerformQuery(sqlQ, paramQ) == False:
                print "Failed 24"
                return False

    print "Renaming history"
    sqlQ = "RENAME TABLE history TO history_old"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 25"
        return False

    print "Renaming history_new"
    sqlQ = "RENAME TABLE history_new TO history"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 26"
        return False

    print "Update the devices table to hold pen caps aswell"
    sqlQ = """
        alter table devices modify column deviceclass  enum('device_meter','device_pump','device_pen', 'device_pen_cap') default NULL
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 27"
        return False

    print "Update the device_ownership table to an extra field"
    sqlQ = """
        alter table device_ownership add column extra VARCHAR(32) default NULL
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 30"
        return False


    print "Update the goalmin and goalmax fields of patients to have one decimal"
    sqlQ = """
        alter table users modify column goalmin DECIMAL(3,1) DEFAULT 4
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 31"
        return False

    sqlQ = """
        alter table users modify column goalmax DECIMAL(3,1) DEFAULT 10
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 32"
        return False

    print "Creating admin_properties table"
    sqlQ = """
        CREATE TABLE admin_properties (
        `admin` VARCHAR(10) NOT NULL,
        `property` VARCHAR(32) NOT NULL,
        `value` int unsigned,
        PRIMARY KEY(`admin`, `property`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 33"
        return False

    print "Moving glucose unit from admins to admin_properties"
    sqlQ = "SELECT name, glucoseunit from admins"
    results = PerformQuery(sqlQ, ())
    if results == False:
        print "Failed 34"
        return False
    
#Result
#            +------------+-------------+
#            | name       | glucoseunit |
#            +------------+-------------+
    
    sqlQ = "INSERT INTO admin_properties VALUES (%s, %s, %s)"
    
    for elem in results:
    
        paramQ = (str(elem[0]), 'glucose_unit', str(elem[1]))

        if PerformQuery(sqlQ, paramQ) == False:
            print "Failed 35"
            return False


    print "Removing the glucoseunit column from admins"
    sqlQ = "ALTER TABLE admins DROP COLUMN glucoseunit"
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 36"
        return False


    print "Creating admin_properties table"
    sqlQ = """
        CREATE TABLE user_properties (
        `pcode` VARCHAR(32) NOT NULL,
        `property` VARCHAR(32) NOT NULL,
        `value` int unsigned,
        PRIMARY KEY(`pcode`, `property`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 37"
        return False

    print "Creating terminal properties table"
    sqlQ = """
        CREATE TABLE terminal_properties (
        `terminal_id` int unsigned NOT NULL,
        `property` VARCHAR(32) NOT NULL,
        `value` VARCHAR(32),
        PRIMARY KEY(`terminal_id`, `property`)
    )DEFAULT CHARSET=utf8 TYPE=InnoDB
    """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 38"
        return False

    print "Moving software info to terminal properties"
    sqlQ = "SELECT seq, hw_nr, sw_nr, bl_nr from terminals"
    results = PerformQuery(sqlQ, ())
    if results == False:
        print "Failed 39"
        return False

    sqlQ = "INSERT INTO terminal_properties VALUES (%s, %s, %s)"

    for elem in results:

        if elem[1] and elem[1] != 'None':
            paramQ = (str(elem[0]), 'hw_nr', str(elem[1]))
            if PerformQuery(sqlQ, paramQ) == False:
                print "Failed 40"
                return False            

        if elem[2] and elem[2] != 'None':
            paramQ = (str(elem[0]), 'sw_nr', str(elem[2]))
            if PerformQuery(sqlQ, paramQ) == False:
                print "Failed 41"
                return False            


        if elem[3] and elem[3] != 'None':
            paramQ = (str(elem[0]), 'bl_nr', str(elem[3]))
            if PerformQuery(sqlQ, paramQ) == False:
                print "Failed 42"
                return False

    print "Now removing the software info from the terminals table"
    sqlQ = """
        ALTER TABLE terminals
        DROP COLUMN hw_nr
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 43"
        return False

    sqlQ = """
        ALTER TABLE terminals
        DROP COLUMN sw_nr
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 44"
        return False


    sqlQ = """
        ALTER TABLE terminals
        DROP COLUMN bl_nr
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 45"
        return False
    
    sqlQ = """
        ALTER TABLE terminals
        DROP COLUMN total_amount_data
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 46"
        return False

    # Update devices table to use InnoDB
    print "Devices is updated to InnoDB"
    sqlQ = """
        ALTER TABLE devices
        ENGINE = InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 47"
        return False

    print "telemeddata_flags is updated to InnoDB"
    sqlQ = """
        ALTER TABLE telemeddata_flags
        ENGINE = InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 48"
        return False

    print "terminals is updated to InnoDB"
    sqlQ = """
        ALTER TABLE terminals
        ENGINE = InnoDB
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 49"
        return False

    # Introduce foreign keys to make the DB consistent
    print "Adding in foreign keys in telemeddata"
    sqlQ = """
        ALTER TABLE telemeddata
        ADD FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 50"
        return False
    

    print "Adding in foreign keys in device_programs"
    sqlQ = """
        ALTER TABLE device_programs
        ADD FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 51"
        return False

    print "Adding in foreign keys in device_settings"
    sqlQ = """
        ALTER TABLE device_settings
        ADD FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 52"
        return False

    print "Adding in foreign keys in transfers"
    sqlQ = """
        ALTER TABLE transfers
        ADD FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 53"
        return False

    print "Insert unknown terminal, since some transfers has an unknown terminal"
    sqlQ = """
        INSERT INTO terminals
        VALUES (0, 'unknown', 'unknown', NULL, 0, 0, NULL, NULL)
        """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed 54"
        return False
    
    sqlQ = """
        UPDATE terminals
        SET seq = 0
        WHERE terminal_id = 'unknown'
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 55"
        return False


    sqlQ = """
        ALTER TABLE transfers
        ADD FOREIGN KEY (`terminal_id`) REFERENCES terminals(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 56"
        return False


    print "Adding in foreign keys in telemeddata_flags"
    

    sqlQ = """
        ALTER TABLE telemeddata_flags 
        MODIFY COLUMN telemeddata_seq int(10) unsigned
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 57"
        return False
    
    
    sqlQ = """
        ALTER TABLE telemeddata_flags
        ADD FOREIGN KEY (`telemeddata_seq`) REFERENCES telemeddata(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 58"
        return False

    print "Adding in foreign keys in telemeddata_values"
    sqlQ = """
        ALTER TABLE telemeddata_values
        ADD FOREIGN KEY (`telemeddata_seq`) REFERENCES telemeddata(`seq`)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 59"
        return False

    print "Updating admin comment to be larger on terminals"
    sqlQ = """
        ALTER TABLE terminals
        MODIFY COLUMN phone TINYTEXT
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed 60"
        return False

    print "Dropping old depricated meters table"
    DropTable("meters")
    
    print "Dropping old depricated insulin_pumps table"
    DropTable("insulin_pumps")

    print "Dropping old depricated insulin_pen_cap_data table"
    DropTable("insulin_pen_cap_data")


    # TODO: drop history_old
    # TODO: drop telemeddata_old
    # TODO: drop insulin_pump_basal_programs
    # TODO: drop insulin_pump_isf_programs

    # TODO: drop insulin_pump_data
    # TODO: drop insulin_pump_data_flags
    # TODO: drop insulin_pump_settings

    # TODO: Remove insulin_pump_data_settings table
    return SetDbVersion('R1e')


def UpgradeFromR1cToR1d():
    """
    Function that upgrades the database from version R1c to R1d
    """
    print "\n== Upgrading from version R1c to R1d =="

    print "Create telemeddata_values table"
    sqlQ = """
        CREATE TABLE `telemeddata_values` (
          `telemeddata_seq` int(10) unsigned,
          `type` ENUM('temperature'),
          `value` int unsigned default NULL,
          PRIMARY KEY  (`telemeddata_seq`, `type`)
        ) DEFAULT CHARSET=utf8 TYPE=InnoDB
        """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed " + sqlQ
        return False

    print "Adding Support for pen cap off type in telemeddata"
    sqlQ = """
        alter table telemeddata modify column type ENUM('ketones', 'glucose', 'pen_cap_off') default NULL
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    # This part corrects an incorrect behaviour of flag insertion
    # Pump flags where inserted to telemeddata_flags!
    
    print "Getting all flags from telemeddata_flags"
    sqlQ = """
        SELECT * FROM telemeddata_flags where flag >= 1000
        """

    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False

    print "Inserting in insulin_pump_data_flags"
    sqlQ = """
        REPLACE INTO insulin_pump_data_flags (insulin_pump_data_seq, flag) VALUES
        (%s,%s)
        """
    for r in _ret:
        paramQ = (r[0],r[1])

        _r = PerformQuery(sqlQ, paramQ)

        if _r == False:
            print "Failed " + sqlQ % paramQ
            return False

    print "Removing pump flags from telemeddata_flags"
    sqlQ = """
        DELETE FROM telemeddata_flags where flag >= 1000
        """

    ret = PerformQuery(sqlQ, ())

    if ret == False:
        print "Failed " + sqlQ
        return False


    return SetDbVersion('R1d')


def UpgradeFromR1bToR1c():
    """
    Function that upgrades the database from version R1b to R1c
    """
    print "\n== Upgrading from version R1b to R1c =="
    
    
    print "Create insulin_pump_isf_programs table"
    sqlQ = """
        CREATE TABLE `insulin_pump_isf_programs` (
          `seq` int(10) unsigned NOT NULL auto_increment,
          `device_id` int(10) unsigned NOT NULL,
          `name` varchar(30) NOT NULL,
          `time` time NOT NULL,
          `isf` int(11) NOT NULL,
          PRIMARY KEY  (`seq`),
          KEY `device_id` (`device_id`)
        ) DEFAULT CHARSET=utf8
        """

    if PerformQuery(sqlQ, ()) == False:
        print "Failed " + sqlQ
        return False

    return SetDbVersion('R1c')


def UpgradeFromNoneToR1b():
    """
    Function that upgrades the database from Unknown version to R1b
    """
    print "\n== Upgrading from unknown early version to R1b =="
    
    
    print "Create devices table"
    sqlQ = """
        CREATE TABLE devices (
        `seq` int unsigned NOT NULL auto_increment,
         `serialnumber` varchar(30) NOT NULL default '0',
         `devicetype` tinytext default NULL,
         `deviceclass` ENUM('device_meter', 'device_pump', 'device_pen') default NULL,
         PRIMARY KEY  (`seq`),
         UNIQUE KEY (`serialnumber`)
         )DEFAULT CHARSET=utf8 
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed " + sqlQ
        return False


    print "Create device_ownership table"
    sqlQ = """
        CREATE TABLE device_ownership (
        `pcode` varchar(32) NOT NULL,
        `device_id` int unsigned NOT NULL,
        PRIMARY KEY  (`pcode`, `device_id`)
         )DEFAULT CHARSET=utf8 
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed " + sqlQ
        return False
            
    print "Copying from meters to devices"
    sqlQ = """
        SELECT serialnumber, pcode from meters ORDER BY serialnumber 
        """
    _ret = PerformQuery(sqlQ, ())
    if _ret == False:
        print "Failed"
        return False

    if InsertIntoDeviceTable(_ret, "device_meter") == False:
        print "Failed"
        return False
    
    print "Copying insulin_pumps from meters to devices"
    sqlQ = """
        SELECT serialnumber, pcode from insulin_pumps ORDER BY serialnumber
        """
    _ret = PerformQuery(sqlQ, ())
    if _ret == False:
        print "Failed"
        return False

    if InsertIntoDeviceTable(_ret, "device_pump") == False:
        print "Failed"
        return False

    print "Creating device entries for devices in the telemeddata table"
    if CreateDeviceEntriesFromTable("telemeddata", "metertype", "device_meter") == False:
        print "Failed"
        return False


    print "Creating device entries for devices in the insulin_pump_data table"
    if CreateDeviceEntriesFromTable("insulin_pump_data", "pumptype", "device_pump") == False:
        print "Failed"
        return False


    print "Transferring from old terminals to new"
    if CopyTerminalContent() == False:
        print "Failed"
        return False


    print "Creating terminal entries for terminals in the telemeddata table"
    if CreateTerminalEntriesFromTable("telemeddata") == False:
        print "Failed"
        return False


    print "Creating terminal entries for terminals in the insulin_pump_data table"
    if CreateTerminalEntriesFromTable("insulin_pump_data") == False:
        print "Failed"
        return False
                    

    print "Creating new telemeddata table"
    sqlQ = """
        CREATE TABLE telemeddata_new (
        `seq` int(11) NOT NULL auto_increment,
          `device_id` int unsigned NOT NULL,
          `terminal_id` int unsigned default NULL,
          `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
          `type` ENUM('ketones', 'glucose') default NULL,
          `value` int unsigned default NULL,
          PRIMARY KEY  (`seq`),
          INDEX (`device_id`),
          INDEX (`timestamp`)
         )DEFAULT CHARSET=utf8 
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    _term_cache = {}
    _device_cache = {}

    if CopyTelemeddata(_term_cache, _device_cache) == False:
        print "Failed"
        return False


    print "Creating new insulin_pump_data table"
    sqlQ = """
        CREATE TABLE insulin_pump_data_new (
      `seq` int unsigned NOT NULL auto_increment,
      `device_id` int unsigned NOT NULL,
      `terminal_id` int unsigned NOT NULL,
      `timestamp` datetime NOT NULL,
      `type` ENUM( 'insulin_basal', 'insulin_bolus', 'insulin_tdd', 'insulin_basal_tdd', 'insulin_prime', 'insulin_alarm') NOT NULL,
      `value` int unsigned NOT NULL,
      PRIMARY KEY  (`seq`),
      INDEX (`device_id`),
      INDEX (`timestamp`)
        )DEFAULT CHARSET=utf8 
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    # Do not share the device cache since the devices won't overlap anyway
    _device_cache = {}

    print "Copying insulin data"
    if CopyInsulindata(_term_cache, _device_cache) == False:
        print "Failed"
        return False


    print "Dropping unused flag_id from telemeddata_flags"
    sqlQ = """
        alter table telemeddata_flags drop column flag_id
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    print "Creating new key in telemeddata_flags"
    sqlQ = """
        alter table telemeddata_flags add primary key(telemeddata_seq, flag)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    print "Dropping unused flag_id from insulin_pump_data_flags"
    sqlQ = """
        alter table insulin_pump_data_flags drop column flag_id
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    print "Creating new key in insulin_pump_data_flags"
    sqlQ = """
        alter table insulin_pump_data_flags add primary key(insulin_pump_data_seq, flag)
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    print "Creating admin hsa id table"
    sqlQ = """
        CREATE TABLE admin_hsa_ids (
        `admin` varchar(10) NOT NULL,
        `id` varchar(30) NOT NULL,
        PRIMARY KEY  (`id`)
        )DEFAULT CHARSET=utf8 
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False


    print "Replacing old terminal table"
    if MoveNewToCurrentTable("terminals") == False:
        print "Failed"
        return False

    print "Replacing old insulin_pump_data"
    if MoveNewToCurrentTable("insulin_pump_data") == False:
        print "Failed"
        return False

    print "Replacing old telemeddata"
    if MoveNewToCurrentTable("telemeddata") == False:
        print "Failed"
        return False


    print "Deleting meters table"
#    DropTable("meters")
    print "Deleting insulin_pumps table"
#    DropTable("insulin_pumps")
    print "Deleting terminals_old table"
#    DropTable("terminals_old")
    print "Removing old telemeddata"
#    DropTable("telemeddata_old")
    print "Removing old insulin_pump_data"
#    DropTable("insulin_pump_data_old")
    
    print "Create metadata table"

    sqlQ = """
        CREATE TABLE metadata (
        `key` varchar(24) NOT NULL,
        `value` varchar(24) NOT NULL,
        PRIMARY KEY  (`key`)
        )DEFAULT CHARSET=utf8 
        """
    if PerformQuery(sqlQ, ()) == False:
        print "Failed"
        return False

    return SetDbVersion('R1b')


def usage():
    """
    Function displaying the usage
    """
    print "Arguments: -d <database> -u <username> -p <password> -h <host>"
    
def main(argv):
    """
    Main function
    """
    global host
    global passwd
    global user
    global db

    try:
        opts, args = getopt.getopt(argv[1:], "d:u:p:h:")
    except:
        usage()
        sys.exit()
    
    for o in opts:
        if len(o) == 0:
            continue

        opt = o[0]
        val = o[1]

        if opt == "-d":
            db = val
        elif opt == "-u":
            user = val
        elif opt == "-p":
            passwd = val
        elif opt == "-h":
            host = val

    # Turn warnings into errors
    warnings.filterwarnings('error', category=MySQLdb.Warning);

    if not ConnectToDatabase():
        print "Failed to connect to database"
        sys.exit()
    
    print "Connected to the database"

    print "db version " + str(GetDBVersion())
    
    if ('dev' in GetDBVersion()):
        print "WARNING: Development database detected. You probably need to upgrade manually."
    
    if GetDBVersion() == None:
        UpgradeFromNoneToR1b()

    if GetDBVersion() == 'R1b':
        UpgradeFromR1bToR1c()
    
    if GetDBVersion() == 'R1c':
        UpgradeFromR1cToR1d()

    if GetDBVersion() == 'R1d':
        UpgradeFromR1dToR1e()

    if GetDBVersion() == 'R1e':
        UpgradeFromR1eToR1f()

    if GetDBVersion() == 'R1f':
        UpgradeFromR1fToR1g()

    if GetDBVersion() == 'R1g':
        UpgradeFromR1gToR1h()

    if GetDBVersion() == 'R1h':
        UpgradeFromR1hToR1i()

    if GetDBVersion() == 'R1i':
        UpgradeFromR1iToR1j()
    
    if GetDBVersion() == 'R1j':
        UpgradeFromR1jToR1k()

    if GetDBVersion() == 'R1k':
        UpgradeFromR1kToR1m() 

    if GetDBVersion() == 'R1m':
        UpgradeFromR1mToR1n() 

    if GetDBVersion() == 'R1n':
        UpgradeFromR1nToR1o() 

    if GetDBVersion() == 'R1o':
        UpgradeFromR1oToR1p() 

    if GetDBVersion() == 'R1p':
        UpgradeFromR1pToR1q() 

    if GetDBVersion() == 'R1q':
        UpgradeFromR1qToR1r() 

    if GetDBVersion() == 'R1r':
        UpgradeFromR1rToR1s() 
    
    if GetDBVersion() == 'R1s':
        UpgradeFromR1sToR1t() 

    if GetDBVersion() == 'R1t':
        UpgradeFromR1tToR1u()

    if GetDBVersion() == 'R1u':
        UpgradeFromR1uToR1v() 

    if GetDBVersion() == 'R1v':
        UpgradeFromR1vToR1w() 
        
    if GetDBVersion() == 'R1w':
        UpgradeFromR1wToR1x() 
        
    if GetDBVersion() == 'R1x':
        UpgradeFromR1xToR1y() 
        
    if GetDBVersion() == 'R1y':
        UpgradeFromR1yToR1z() 
        
    if GetDBVersion() == 'R1z':
        UpgradeFromR1zToR2a() 

    if GetDBVersion() == 'R2a':
        UpgradeFromR2aToR2b()
    
    if GetDBVersion() == 'R2b':
        UpgradeFromR2bToR2c()

    if GetDBVersion() == 'R2c':
        UpgradeFromR2cToR2d()
    
    if GetDBVersion() == 'R2d':
        UpgradeFromR2dToR2e()
    
    if GetDBVersion() == 'R2e':
        UpgradeFromR2eToR3a()
    
    if GetDBVersion() == 'R3a':
        UpgradeFromR3aToR3b()

    if GetDBVersion() == 'R3b':
        UpgradeFromR3bToR3c()

    if GetDBVersion() == 'R3c':
        UpgradeFromR3cToR3d()

    if GetDBVersion() == 'R3d':
        UpgradeFromR3dToR3e()

    if GetDBVersion() == 'R3e':
        UpgradeFromR3eToR3f()
    
    if GetDBVersion() == 'R3f':
        UpgradeFromR3fToR3g()

    if GetDBVersion() == 'R3g':
        UpgradeFromR3gToR4a()

    if GetDBVersion() == 'R4a':
        UpgradeFromR4aToR4b()

    if GetDBVersion() == 'R4b':
        UpgradeFromR4bToR4c()
        
    if GetDBVersion() == 'R4c':
        UpgradeFromR4cToR4d()
        
    if GetDBVersion() == 'R4d':
        UpgradeFromR4dToR4e()

    if GetDBVersion() == 'R4e':
        UpgradeFromR4eToR4f()

    DisconnectFromDatabase()

    print "Done"

def MovePhonesToUserDevices():
    print " Getting info on all device_phone devices"
    sqlQ = """
        SELECT device_id,pcode FROM devices,device_ownership WHERE deviceclass = 'device_phone' AND devices.seq = device_ownership.device_id
        """

    _ret = PerformQuery(sqlQ, ())

    if _ret == False:
        print "Failed " + sqlQ
        return False
    print "  found ", len(_ret), " devices"

    print " Transform each device_phone to a device_user"

    for r in _ret:
        result = TransformPhoneToUserDevice(r[0], r[1])
        if result == False:
            print "Failed"
            return False

    return True

def TransformPhoneToUserDevice(device_id, pcode):
    ser = "DVU"
  
    # Select sane characters  
    ser += re.sub(r'[^A-Za-z0-9@._]', "", pcode[0:23])
    zeroes = "000000000000000000000000000000"
    
    # Being lazy here, since no large pcodes exist in the current items  
    ser += zeroes[0:30-len(ser)]

    # Rename the phone device
    sqlQ = """
        UPDATE devices SET serialnumber=%s, deviceclass='device_user', devicetype='User' WHERE seq = %s
    """
    paramQ = (str(ser), device_id)
    _r = PerformQuery(sqlQ, paramQ)

    if _r == False:
       print "Failed " + sqlQ % paramQ
       return False
    
    return True
    
    
if __name__ == "__main__":
    main(sys.argv)
