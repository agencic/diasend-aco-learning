-- MySQL dump 10.13  Distrib 5.1.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: diasend_se
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_token`
--

DROP TABLE IF EXISTS `access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_token` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_bin NOT NULL,
  `expire` datetime NOT NULL,
  `access_count` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `oauth_client_permission_id` int(11) NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_access_token_oauth_client_permission1` (`oauth_client_permission_id`),
  CONSTRAINT `fk_access_token_oauth_client_permission1` FOREIGN KEY (`oauth_client_permission_id`) REFERENCES `oauth_client_permission` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `access_token_owner_trial`
--

DROP TABLE IF EXISTS `access_token_owner_trial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_token_owner_trial` (
  `access_token_id` int(11) NOT NULL,
  `trial_id` int(11) NOT NULL,
  KEY `fk_access_token_owner_trial_access_token1` (`access_token_id`),
  KEY `fk_access_token_owner_trial_trial1` (`trial_id`),
  CONSTRAINT `fk_access_token_owner_trial_access_token1` FOREIGN KEY (`access_token_id`) REFERENCES `access_token` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_access_token_owner_trial_trial1` FOREIGN KEY (`trial_id`) REFERENCES `trial` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_hsa_ids`
--

DROP TABLE IF EXISTS `admin_hsa_ids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_hsa_ids` (
  `admin` varchar(10) COLLATE utf8_bin NOT NULL,
  `id` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin` (`admin`),
  CONSTRAINT `admin_hsa_ids_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_properties`
--

DROP TABLE IF EXISTS `admin_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_properties` (
  `admin` varchar(10) COLLATE utf8_bin NOT NULL,
  `property` varchar(32) COLLATE utf8_bin NOT NULL,
  `value` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`admin`,`property`),
  CONSTRAINT `admin_properties_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `name` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `hash` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `salt` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `oldpassword` varchar(32) COLLATE utf8_bin DEFAULT '',
  `title` text COLLATE utf8_bin NOT NULL,
  `email` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT 'diasend@breneman.se',
  `phone` text COLLATE utf8_bin NOT NULL,
  `support` text COLLATE utf8_bin NOT NULL,
  `misc` text COLLATE utf8_bin NOT NULL,
  `super_admin` varchar(30) COLLATE utf8_bin DEFAULT 'diadmin',
  PRIMARY KEY (`name`),
  KEY `super_admin` (`super_admin`),
  CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`super_admin`) REFERENCES `super_admins` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clinic_users`
--

DROP TABLE IF EXISTS `clinic_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_users` (
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `hash` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `salt` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `oldpassword` varchar(32) COLLATE utf8_bin DEFAULT '',
  `firstname` varchar(30) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(30) COLLATE utf8_bin NOT NULL,
  `admin` varchar(10) COLLATE utf8_bin NOT NULL,
  `comment` text COLLATE utf8_bin,
  `properties` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`username`),
  KEY `admin` (`admin`),
  CONSTRAINT `clinic_users_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `communication`
--

DROP TABLE IF EXISTS `communication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_bin NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pcode` (`pcode`),
  CONSTRAINT `communication_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB AUTO_INCREMENT=4589 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `database_servers`
--

DROP TABLE IF EXISTS `database_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_servers` (
  `server_id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) COLLATE utf8_bin NOT NULL,
  `username` varchar(255) COLLATE utf8_bin NOT NULL,
  `passwd` varchar(255) COLLATE utf8_bin NOT NULL,
  `port` int(11) NOT NULL,
  `db` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`server_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deletion_log`
--

DROP TABLE IF EXISTS `deletion_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deletion_log` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `pcode` (`pcode`),
  CONSTRAINT `deletion_log_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_debug_record`
--

DROP TABLE IF EXISTS `device_debug_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_debug_record` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `transfer_id` int(10) unsigned NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `transfer_id` (`transfer_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `device_debug_record_ibfk_1` FOREIGN KEY (`transfer_id`) REFERENCES `transfers` (`seq`),
  CONSTRAINT `device_debug_record_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_owner_trial`
--

DROP TABLE IF EXISTS `device_owner_trial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_owner_trial` (
  `trial_id` int(11) NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  KEY `fk_device_owner_trial_trial1` (`trial_id`),
  KEY `fk_device_owner_trial_devices1` (`device_id`),
  CONSTRAINT `fk_device_owner_trial_devices1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_device_owner_trial_trial1` FOREIGN KEY (`trial_id`) REFERENCES `trial` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_ownership`
--

DROP TABLE IF EXISTS `device_ownership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_ownership` (
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `extra` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `primary_device` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pcode`,`device_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `device_ownership_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`),
  CONSTRAINT `device_ownership_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_program`
--

DROP TABLE IF EXISTS `device_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_program` (
  `seq` int(10) NOT NULL AUTO_INCREMENT,
  `device_setting_group_id` int(11) NOT NULL,
  `type` enum('basal_program','isf_program','ic_ratio_program','bg_target_program','bg_threshold_program','correction_program') COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `period_start` time DEFAULT NULL,
  `period_rate` int(10) DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_device_program_device_setting_group1` (`device_setting_group_id`),
  CONSTRAINT `fk_device_program_device_setting_group1` FOREIGN KEY (`device_setting_group_id`) REFERENCES `device_setting_group` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=379032 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_program_value`
--

DROP TABLE IF EXISTS `device_program_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_program_value` (
  `seq` int(10) NOT NULL AUTO_INCREMENT,
  `device_program_id` int(10) NOT NULL,
  `type` enum('bg_deviation') COLLATE utf8_bin DEFAULT NULL,
  `value` int(10) DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_device_program_value_device_program1` (`device_program_id`),
  CONSTRAINT `fk_device_program_value_device_program1` FOREIGN KEY (`device_program_id`) REFERENCES `device_program` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5637 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_program_values`
--

DROP TABLE IF EXISTS `device_program_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_program_values` (
  `device_program_period_id` int(10) NOT NULL DEFAULT '0',
  `type` enum('bg_deviation') COLLATE utf8_bin NOT NULL DEFAULT 'bg_deviation',
  `value` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`device_program_period_id`,`type`),
  CONSTRAINT `device_program_values_ibfk_1` FOREIGN KEY (`device_program_period_id`) REFERENCES `device_programs` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_programs`
--

DROP TABLE IF EXISTS `device_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_programs` (
  `seq` int(10) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `type` enum('basal_program','isf_program','ic_ratio_program','bg_target_program','bg_threshold_program','correction_program') COLLATE utf8_bin NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `period_start` time NOT NULL,
  `period_rate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`seq`),
  UNIQUE KEY `device_id` (`device_id`,`type`,`name`,`period_start`),
  CONSTRAINT `device_programs_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2003575 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_properties`
--

DROP TABLE IF EXISTS `device_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_properties` (
  `device_id` int(10) unsigned NOT NULL,
  `property` varchar(32) COLLATE utf8_bin NOT NULL,
  `value` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`device_id`,`property`),
  CONSTRAINT `device_properties_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_setting`
--

DROP TABLE IF EXISTS `device_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_setting` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `device_setting_group_id` int(11) NOT NULL,
  `setting_id` int(10) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_device_setting_device_setting_group1` (`device_setting_group_id`),
  CONSTRAINT `fk_device_setting_device_setting_group1` FOREIGN KEY (`device_setting_group_id`) REFERENCES `device_setting_group` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=201134 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_setting_group`
--

DROP TABLE IF EXISTS `device_setting_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_setting_group` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `transfer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_device_setting_group_devices1` (`device_id`),
  KEY `fk_device_setting_group_transfers1` (`transfer_id`),
  CONSTRAINT `fk_device_setting_group_devices1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_device_setting_group_transfers1` FOREIGN KEY (`transfer_id`) REFERENCES `transfers` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8351 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_settings`
--

DROP TABLE IF EXISTS `device_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_settings` (
  `device_id` int(10) unsigned NOT NULL,
  `setting_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`device_id`,`setting_id`),
  CONSTRAINT `device_settings_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serialnumber` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `devicetype` tinytext COLLATE utf8_bin,
  `deviceclass` enum('device_meter','device_pump','device_pen','device_pen_cap','device_phone','device_combo','device_user') COLLATE utf8_bin DEFAULT NULL,
  `serial_obfuscated` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`seq`),
  UNIQUE KEY `serialnumber` (`serialnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=109543 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dibs_transactions`
--

DROP TABLE IF EXISTS `dibs_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dibs_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `referenceno` varchar(128) COLLATE utf8_bin NOT NULL,
  `reply` varchar(128) COLLATE utf8_bin NOT NULL,
  `replytext` varchar(128) COLLATE utf8_bin NOT NULL,
  `verifyid` varchar(128) COLLATE utf8_bin NOT NULL,
  `sum` varchar(128) COLLATE utf8_bin NOT NULL,
  `currency` varchar(128) COLLATE utf8_bin NOT NULL,
  `arraydata` text COLLATE utf8_bin,
  `transactiontime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pcode` (`pcode`),
  KEY `referenceno` (`referenceno`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `errorlog`
--

DROP TABLE IF EXISTS `errorlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `errorlog` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `errorcode` int(11) DEFAULT NULL,
  `hw_nr` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `sw_nr` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `bl_nr` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `serial_terminal` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `serial_meter` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `metertype` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`seq`)
) ENGINE=MyISAM AUTO_INCREMENT=16179 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `food_collection`
--

DROP TABLE IF EXISTS `food_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_collection` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `last_edited` datetime DEFAULT NULL,
  `transferred_from_device` datetime DEFAULT NULL,
  `transferred_to_device` datetime DEFAULT NULL,
  `device_id` int(10) unsigned DEFAULT NULL,
  `is_backup` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `food_collection_owner_clinic`
--

DROP TABLE IF EXISTS `food_collection_owner_clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_collection_owner_clinic` (
  `food_collection_id` int(11) DEFAULT NULL,
  `admin` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  KEY `fk_food_collection_owner_clinic1` (`admin`),
  KEY `fk_food_collection_owner_clinic2` (`food_collection_id`),
  CONSTRAINT `fk_food_collection_owner_clinic1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_food_collection_owner_clinic2` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `food_collection_owner_language`
--

DROP TABLE IF EXISTS `food_collection_owner_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_collection_owner_language` (
  `food_collection_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(128) NOT NULL DEFAULT '',
  `supplier` varchar(128) DEFAULT NULL,
  `comment` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`food_collection_id`,`language`),
  CONSTRAINT `food_collection_owner_language_ibfk_1` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `food_collection_owner_patient`
--

DROP TABLE IF EXISTS `food_collection_owner_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_collection_owner_patient` (
  `food_collection_id` int(11) DEFAULT NULL,
  `pcode` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  KEY `fk_food_collection_owner_patient1` (`pcode`),
  KEY `fk_food_collection_owner_patient2` (`food_collection_id`),
  CONSTRAINT `fk_food_collection_owner_patient1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_food_collection_owner_patient2` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `food_item`
--

DROP TABLE IF EXISTS `food_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_item` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `food_collection_id` int(11) DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `servings` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `carbs` int(11) DEFAULT NULL,
  `fiber` int(11) DEFAULT NULL,
  `protein` int(11) DEFAULT NULL,
  `fat` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `food_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `fk_food_collection` (`food_collection_id`),
  CONSTRAINT `fk_food_collection` FOREIGN KEY (`food_collection_id`) REFERENCES `food_collection` (`seq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=159354 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hba1c`
--

DROP TABLE IF EXISTS `hba1c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hba1c` (
  `seq` int(10) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `value` varchar(4) COLLATE utf8_bin NOT NULL,
  `timestamp` datetime NOT NULL,
  `comment` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `pcode` (`pcode`),
  CONSTRAINT `hba1c_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `terminal_id` int(10) unsigned DEFAULT NULL,
  `device_id` int(10) unsigned DEFAULT NULL,
  `entry` tinytext COLLATE utf8_bin NOT NULL,
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `licensecodes`
--

DROP TABLE IF EXISTS `licensecodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licensecodes` (
  `licensecode` varchar(8) COLLATE utf8_bin NOT NULL,
  `distributor` varchar(128) COLLATE utf8_bin NOT NULL,
  `subscription_level` enum('full','limited') COLLATE utf8_bin DEFAULT 'full',
  `subscription_time` enum('30_days','60_days','3_months','6_months','12_months') COLLATE utf8_bin DEFAULT '30_days',
  `limitation` enum('no_limit','only_new','only_registered') COLLATE utf8_bin DEFAULT 'no_limit',
  `validity_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `validity_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `max_activations` varchar(8) COLLATE utf8_bin NOT NULL,
  `comment` text COLLATE utf8_bin,
  PRIMARY KEY (`licensecode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_activity`
--

DROP TABLE IF EXISTS `login_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_activity` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `username` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` blob,
  `event` enum('ok','archived','expired','failed','deleted') COLLATE utf8_bin NOT NULL,
  `account_type` enum('-','patient_normal','patient_srpa','clinic_admin','clinic_user','super_admin') COLLATE utf8_bin NOT NULL,
  `browser` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=381154 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lostpassword_keys`
--

DROP TABLE IF EXISTS `lostpassword_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lostpassword_keys` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `key` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `pcode` (`pcode`),
  CONSTRAINT `lostpassword_keys_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metadata`
--

DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata` (
  `key` varchar(24) COLLATE utf8_bin NOT NULL,
  `value` varchar(1024) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moblog`
--

DROP TABLE IF EXISTS `moblog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moblog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `key` enum('glucose','insuline','carbs','practice','starval','txt','login','logout','page') COLLATE utf8_bin NOT NULL,
  `value` text COLLATE utf8_bin NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pcode` (`pcode`),
  CONSTRAINT `moblog_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB AUTO_INCREMENT=16317 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(1) DEFAULT NULL,
  `criteria` int(10) unsigned DEFAULT '0',
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_text`
--

DROP TABLE IF EXISTS `notification_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_text` (
  `notification_id` int(10) unsigned NOT NULL,
  `language` varchar(32) NOT NULL DEFAULT '',
  `header` tinytext,
  `body` text,
  PRIMARY KEY (`notification_id`,`language`),
  CONSTRAINT `notification_text_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_view_log_clinic`
--

DROP TABLE IF EXISTS `notification_view_log_clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_view_log_clinic` (
  `admin` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notification_id` int(10) unsigned NOT NULL,
  `viewed` datetime DEFAULT NULL,
  PRIMARY KEY (`admin`,`notification_id`),
  KEY `notification_id` (`notification_id`),
  CONSTRAINT `notification_view_log_clinic_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`),
  CONSTRAINT `notification_view_log_clinic_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_view_log_clinic_user`
--

DROP TABLE IF EXISTS `notification_view_log_clinic_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_view_log_clinic_user` (
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notification_id` int(10) unsigned NOT NULL,
  `viewed` datetime DEFAULT NULL,
  PRIMARY KEY (`username`,`notification_id`),
  KEY `notification_id` (`notification_id`),
  CONSTRAINT `notification_view_log_clinic_user_ibfk_1` FOREIGN KEY (`username`) REFERENCES `clinic_users` (`username`),
  CONSTRAINT `notification_view_log_clinic_user_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_view_log_patient`
--

DROP TABLE IF EXISTS `notification_view_log_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_view_log_patient` (
  `pcode` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notification_id` int(10) unsigned NOT NULL,
  `viewed` datetime DEFAULT NULL,
  PRIMARY KEY (`pcode`,`notification_id`),
  KEY `notification_id` (`notification_id`),
  CONSTRAINT `notification_view_log_patient_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`),
  CONSTRAINT `notification_view_log_patient_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_client`
--

DROP TABLE IF EXISTS `oauth_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `hash` varchar(255) COLLATE utf8_bin NOT NULL,
  `salt` varchar(40) COLLATE utf8_bin NOT NULL,
  `active` tinyint(1) NOT NULL,
  `rate_limit_count` int(11) NOT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth_client_permission`
--

DROP TABLE IF EXISTS `oauth_client_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client_permission` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `oauth_client_id` int(11) NOT NULL,
  `account_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `resource_type` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdf_profiles`
--

DROP TABLE IF EXISTS `pdf_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdf_profiles` (
  `seq` int(10) NOT NULL AUTO_INCREMENT,
  `admin` varchar(10) COLLATE utf8_bin NOT NULL,
  `name` varchar(32) COLLATE utf8_bin NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `admin` (`admin`),
  CONSTRAINT `pdf_profiles_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registration_events`
--

DROP TABLE IF EXISTS `registration_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_events` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `event_type` enum('new_registration','subscription_renewal') COLLATE utf8_bin DEFAULT 'new_registration',
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_method` enum('trial','payment','licensecode') COLLATE utf8_bin DEFAULT 'trial',
  `payment_data` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `new_expire_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dibs_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`seq`),
  KEY `pcode` (`pcode`),
  KEY `dibs_id` (`dibs_id`),
  CONSTRAINT `registration_events_ibfk_1` FOREIGN KEY (`dibs_id`) REFERENCES `dibs_transactions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1181 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `super_admin_properties`
--

DROP TABLE IF EXISTS `super_admin_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `super_admin_properties` (
  `super_admin` varchar(30) COLLATE utf8_bin NOT NULL,
  `property` varchar(32) COLLATE utf8_bin NOT NULL,
  `value` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`super_admin`,`property`),
  CONSTRAINT `super_admin_properties_ibfk_1` FOREIGN KEY (`super_admin`) REFERENCES `super_admins` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `super_admins`
--

DROP TABLE IF EXISTS `super_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `super_admins` (
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `hash` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `salt` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `oldpassword` varchar(32) COLLATE utf8_bin DEFAULT '',
  `the_one_and_only` tinyint(1) DEFAULT '0',
  `comment` text COLLATE utf8_bin,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemeddata`
--

DROP TABLE IF EXISTS `telemeddata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemeddata` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `transfer_id` int(10) unsigned NOT NULL,
  `timestamp` datetime NOT NULL,
  `type` enum('ketones','glucose','pen_cap_off','insulin_basal','insulin_bolus','insulin_tdd','insulin_prime','insulin_alarm','insulin_bolus_basal','carbs','training','event') NOT NULL,
  `value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `device_id` (`device_id`,`timestamp`),
  KEY `transfer_id` (`transfer_id`),
  CONSTRAINT `telemeddata_ibfk_1` FOREIGN KEY (`transfer_id`) REFERENCES `transfers` (`seq`),
  CONSTRAINT `telemeddata_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=93725435 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemeddata_flags`
--

DROP TABLE IF EXISTS `telemeddata_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemeddata_flags` (
  `telemeddata_seq` int(10) unsigned NOT NULL DEFAULT '0',
  `flag` int(11) NOT NULL,
  PRIMARY KEY (`telemeddata_seq`,`flag`),
  CONSTRAINT `telemeddata_flags_ibfk_1` FOREIGN KEY (`telemeddata_seq`) REFERENCES `telemeddata` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telemeddata_values`
--

DROP TABLE IF EXISTS `telemeddata_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telemeddata_values` (
  `telemeddata_seq` int(10) unsigned NOT NULL DEFAULT '0',
  `type` enum('temperature','duration','insulin_basal_tdd','insulin_bolus_ext','insulin_bolus_suggested','insulin_bolus_prog_meal','insulin_bolus_prog_corr') NOT NULL DEFAULT 'temperature',
  `value` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`telemeddata_seq`,`type`),
  CONSTRAINT `telemeddata_values_ibfk_1` FOREIGN KEY (`telemeddata_seq`) REFERENCES `telemeddata` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal_locations`
--

DROP TABLE IF EXISTS `terminal_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal_locations` (
  `terminal_id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `database_server` int(11) NOT NULL,
  PRIMARY KEY (`terminal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal_permissions`
--

DROP TABLE IF EXISTS `terminal_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal_permissions` (
  `terminal_id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `permission` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`terminal_id`,`permission`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal_properties`
--

DROP TABLE IF EXISTS `terminal_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal_properties` (
  `terminal_id` int(10) unsigned NOT NULL,
  `property` varchar(32) COLLATE utf8_bin NOT NULL,
  `value` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`terminal_id`,`property`),
  CONSTRAINT `terminal_properties_ibfk_1` FOREIGN KEY (`terminal_id`) REFERENCES `terminals` (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal_software_versions`
--

DROP TABLE IF EXISTS `terminal_software_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal_software_versions` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) COLLATE utf8_bin NOT NULL,
  `readable_version` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`seq`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminals`
--

DROP TABLE IF EXISTS `terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminals` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `terminal_id` varchar(32) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `admin` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phone` tinytext COLLATE utf8_bin,
  `nr_transfers` int(11) NOT NULL DEFAULT '0',
  `amount_data` int(11) NOT NULL DEFAULT '0',
  `comment` tinytext COLLATE utf8_bin,
  `location` tinytext COLLATE utf8_bin,
  `terminalclass` enum('terminal_diasend','terminal_smartpix','terminal_phone','terminal_file_transfer','terminal_web_ui','terminal_mobile_ui','terminal_dda') COLLATE utf8_bin NOT NULL DEFAULT 'terminal_diasend',
  PRIMARY KEY (`seq`),
  UNIQUE KEY `terminal_id` (`terminal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6985 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transfers`
--

DROP TABLE IF EXISTS `transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transfers` (
  `seq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `terminal_id` int(10) unsigned NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`seq`),
  KEY `terminal_id` (`terminal_id`,`timestamp`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `transfers_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`seq`),
  CONSTRAINT `transfers_ibfk_2` FOREIGN KEY (`terminal_id`) REFERENCES `terminals` (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=366384 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trial`
--

DROP TABLE IF EXISTS `trial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trial` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `username` varchar(128) COLLATE utf8_bin NOT NULL,
  `hash` varchar(255) COLLATE utf8_bin NOT NULL,
  `salt` varchar(40) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_licensecodes`
--

DROP TABLE IF EXISTS `user_licensecodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_licensecodes` (
  `licensecode` varchar(8) COLLATE utf8_bin NOT NULL,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`licensecode`,`pcode`),
  KEY `user_licensecodes_ibfk_2` (`pcode`),
  CONSTRAINT `user_licensecodes_ibfk_1` FOREIGN KEY (`licensecode`) REFERENCES `licensecodes` (`licensecode`),
  CONSTRAINT `user_licensecodes_ibfk_2` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_ownership`
--

DROP TABLE IF EXISTS `user_ownership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_ownership` (
  `seq` int(10) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `admin` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `access_type` enum('access_normal','access_srpa','access_view') COLLATE utf8_bin DEFAULT 'access_normal',
  PRIMARY KEY (`seq`),
  KEY `admin` (`admin`),
  KEY `user_ownership_ibfk_1` (`pcode`),
  CONSTRAINT `user_ownership_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`),
  CONSTRAINT `user_ownership_ibfk_2` FOREIGN KEY (`admin`) REFERENCES `admins` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8995 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_properties`
--

DROP TABLE IF EXISTS `user_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_properties` (
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `property` varchar(32) COLLATE utf8_bin NOT NULL,
  `value` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pcode`,`property`),
  CONSTRAINT `user_properties_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_strings`
--

DROP TABLE IF EXISTS `user_strings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_strings` (
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `string` varchar(30) COLLATE utf8_bin NOT NULL,
  `value` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`pcode`,`string`),
  CONSTRAINT `user_strings_ibfk_1` FOREIGN KEY (`pcode`) REFERENCES `users` (`pcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `pcode` varchar(128) COLLATE utf8_bin NOT NULL,
  `hash` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `salt` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pnumber` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `oldpassword` varchar(32) COLLATE utf8_bin DEFAULT '',
  `firstname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `goalmin` decimal(4,2) DEFAULT '4.00',
  `goalmax` decimal(4,2) DEFAULT '10.00',
  `staticinterval` enum('Yes','No') COLLATE utf8_bin NOT NULL DEFAULT 'No',
  `interval_time_b` smallint(4) DEFAULT NULL,
  `interval_time_a` smallint(4) DEFAULT NULL,
  `int-br-wd` smallint(4) NOT NULL DEFAULT '0',
  `int-br-we` smallint(4) NOT NULL DEFAULT '0',
  `int-lu-wd` smallint(4) NOT NULL DEFAULT '0',
  `int-lu-we` smallint(4) NOT NULL DEFAULT '0',
  `int-di-wd` smallint(4) NOT NULL DEFAULT '0',
  `int-di-we` smallint(4) NOT NULL DEFAULT '0',
  `int-ev-wd` smallint(4) NOT NULL DEFAULT '0',
  `int-ev-we` smallint(4) NOT NULL DEFAULT '0',
  `archived` enum('Yes','No','Deleted') COLLATE utf8_bin NOT NULL DEFAULT 'No',
  `diabetes_type` enum('general','type_1','type_2','gestational','other') COLLATE utf8_bin DEFAULT 'general',
  PRIMARY KEY (`pcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `metadata` WRITE;
/*!40000 ALTER TABLE `metadata` DISABLE KEYS */;
INSERT INTO `metadata` VALUES ('db_version','R3a'),('old_terminal_version','R5c');
/*!40000 ALTER TABLE `metadata` ENABLE KEYS */;
UNLOCK TABLES;
--
LOCK TABLES `super_admins` WRITE;
/*!40000 ALTER TABLE `super_admins` DISABLE KEYS */;
INSERT INTO `super_admins` VALUES ('diadmin','IrXDVd8Y1jn5b7HgbOcDQitjhhAaMifYG1SSPsULVjw4rh+YyN9mfe6+aQ4hW7priQCAQW4hXzfJ63U8aOTQng==','57op4ykeen0gw4c0084owkog4gsks4w','b0ttenf1sket',1,'');
/*!40000 ALTER TABLE `super_admins` ENABLE KEYS */;
UNLOCK TABLES;
--
LOCK TABLES `terminal_permissions` WRITE;
/*!40000 ALTER TABLE `terminal_permissions` DISABLE KEYS */;
INSERT INTO `terminal_permissions` VALUES ('S6019999999991','device_pump'),('S6019999999991','device_user');
/*!40000 ALTER TABLE `terminal_permissions` ENABLE KEYS */;
UNLOCK TABLES;
--
LOCK TABLES `terminals` WRITE;
/*!40000 ALTER TABLE `terminals` DISABLE KEYS */;
INSERT INTO `terminals` VALUES (1,'S6019999999991','',NULL,0,0,NULL,'clinic','terminal_diasend');
/*!40000 ALTER TABLE `terminals` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-13 13:30:46
