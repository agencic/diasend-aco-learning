<?php

require_once('password.php');

$host = 'localhost';
$db = '';
$user = '';
$pw = '';

function update($host, $db, $user, $pw, $table, $name)
{
	echo "Update table ".$table."\n";

	$link = mysqli_connect($host, $user, $pw, $db) or die ('unable to connect to db');
	mysqli_set_charset($link, 'utf8');

	$query = 'SELECT '.$name.' AS name, oldpassword FROM '.$table;
	$result = mysqli_query($link, $query);

	if (!is_bool($result) && mysqli_num_rows($result) > 0)
	{
		while ($row = mysqli_fetch_array($result, MYSQL_ASSOC))
		{
			$salt = password_create_salt();
			$hash = password_create($row['oldpassword'], $salt);
			$query = 'UPDATE '.$table.' SET hash="'.$hash.'", salt="'.$salt.
				'" WHERE '.$name.'="'.mysqli_real_escape_string($link, $row['name']).'"';
			if (!mysqli_query($link, $query))
			{
				echo "Failed to update : ".$query."\n";	
			}
		}	
	}

	mysqli_close($link);
}

update($host, $db, $user, $pw, 'admins', 'name');
update($host, $db, $user, $pw, 'clinic_users', 'username');
update($host, $db, $user, $pw, 'users', 'pcode');
update($host, $db, $user, $pw, 'super_admins', 'name');

?>
