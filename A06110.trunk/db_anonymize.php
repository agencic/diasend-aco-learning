<?php

/* vim: set expandtab tabstop=8 shiftwidth=4 softtabstop=8: */

/**
 * This file generates a sql script for anonymizing a diasend database
 *
 * Run the file by the following command:
 * php db_anynomize.php hostname username password databasename locale prefix
 *  
 *  hostname     - Hostname where mysql database is located
 *  username     - User in mysql
 *  password     - Password for user in mysql
 *  databasename - Name of database in mysql
 *  locale       - (optional) Locale to use when faking names and other information
 *  prefix       - (optional) Prefix before admins.name, pcode etc. 
 * 
 * The generated script can be used with database by following command:
 *  mysql -u <dbuser> -p < <filename created by this file>
 *
 * If anonymize script already used, running it may cause errors. The use the prefix to add a prefix to admins.name, pcode etc. 
 * running the script again will work. Else use the -f option for mysql which forces all statements in file to be executed.
 *
 * PHP version 5
 *
 */

// Autoload dependencies, make sure that composer install has been runned
require_once('vendor/autoload.php');

define('TMP_TOKEN', 'tmp123');

if ( $argc < 5 ) {
	echo "Script should be runned with following options:\n";
	echo $argv[0]." hostname username password databasename [locale] [prefix]\n";
	exit(0);
}
$host = $argv[1]; //"localhost";
$username = $argv[2]; // "root";
$password = $argv[3];// "developerdb";
//$dbname = "alex_db_test3";
$username_prefix = "int_";
$dbname = $argv[4]; //"diasend_se";
if ( $argc == 6 ) {
	$locale = $argv[5];
} else {
	$locale = "en_US";
}

if ( $locale == "sv_SE" ) {
	$username_prefix = "se_";
}

if ( $argc == 7 ) {
	$username_prefix = $argv[6].$username_prefix;
}

$con = mysqli_connect($host, $username, $password, $dbname);
mysqli_set_charset($con, "utf8");

output("SET NAMES 'utf8';");
output("SET character_set_client = utf8;");
output("SET foreign_key_checks = 0;");
output("SET autocommit = 0;");
output("SET unique_checks=0;");
output("USE " . $dbname . ";");
output("START TRANSACTION;");

output("DELETE FROM user_properties WHERE pcode NOT IN (SELECT pcode FROM users);");
output("DELETE FROM user_strings WHERE pcode NOT IN (SELECT pcode FROM users);");
output("DELETE FROM deletion_log WHERE pcode NOT IN (SELECT pcode FROM users);");

processCommonTable($con, $dbname, "user_strings", "string");
processCommonTable($con, $dbname, "user_properties", "property");
processCommonTable($con, $dbname, "admin_properties", "property", "value", "admin");


processGeneralTable($con, "communication", "id", "id", array("comment"), array("sentence"));
processGeneralTable($con, "deletion_log", "pcode", "pcode", array("ip"), array("ip"));
processFieldName($con, "device_program", "seq", "name", "program_value");
processFieldName($con, "device_programs", "seq", "name", "program_value");
processGeneralTable($con, "device_setting", "seq", "seq", array("value"), array("setting_value"));
processGeneralTable($con, "device_settings", "device_id", "setting_id", array("value"), array("setting_value"));
processGeneralTable($con, "account", "seq", "seq",  array("username"), array("email"));
processGeneralTable($con, "emr_destination", "seq", "seq", array("description"), array("hospital_name"));
processGeneralTable($con, "food_collection", "seq", "seq", array("name"), array("name_or_word"));
processGeneralTable($con, "activity", "seq", "seq", array("external_activity_id"), array("hash"));
processGeneralTable($con, "terminals", "seq", "seq", array("phone", "comment"), array("phoneNumber", "sentence"));
processGeneralTable($con, "login_activity", "seq", "seq", array("ip", "username"), array("ip", "email"));
processGeneralTable($con, "lostpassword_keys", "seq", "seq", array("ip", "pcode", "email"), array("ip", "email", "email"));

clearTableFields("api_log", array('request_data', 'response_body', 'api_resource_name')); 
clearTableFields("audit_log", array('parent_clinic', 'actor', 'target')); 
clearTableFields("audit_log_field_change", array('`from`', '`to`')); 
clearTableFields("dibs_transactions", array('pcode', 'arraydata')); 
clearTableFields("errorlog", array('data')); 
clearTableFields("database_servers", array('passwd')); 

processUsers($con, $dbname);
processAdmins($con, $dbname);
processClinicUsers($con, $dbname);
processSuperAdmins($con, $dbname);

removeTmpToken("admins","name");
removeTmpToken("admin_properties", "admin");
removeTmpToken("clinic_users", "admin");
removeTmpToken("food_collection_owner_clinic", "admin");
removeTmpToken("notification_view_log_clinic", "admin");
removeTmpToken("oauth_client_owner_patient_history", "admin");
removeTmpToken("pdf_profiles", "admin");
removeTmpToken("terminals", "admin");
removeTmpToken("user_ownership", "admin");
removeTmpToken("pdf_generated", "clinic");

removeTmpToken("clinic_users", "username");

removeTmpToken("users", "pcode");
removeTmpToken("deletion_log", "pcode");
removeTmpToken("access_token_owner_patient", "pcode");
removeTmpToken("communication", "pcode");
removeTmpToken("device_ownership", "pcode");
removeTmpToken("dibs_transactions", "pcode");
removeTmpToken("food_collection_owner_patient", "pcode");
removeTmpToken("hba1c", "pcode");
removeTmpToken("lostpassword_keys", "pcode");
removeTmpToken("moblog", "pcode");
removeTmpToken("notification_view_log_patient", "pcode");
removeTmpToken("oauth_client_owner_patient", "pcode");
removeTmpToken("oauth_client_owner_patient_history", "pcode");
removeTmpToken("registration_events", "pcode");
removeTmpToken("user_licensecodes", "pcode");
removeTmpToken("user_ownership", "pcode");
removeTmpToken("user_properties", "pcode");
removeTmpToken("user_strings", "pcode");
removeTmpToken("pdf_generated", "patient");

removeTmpToken("super_admins", "name");
removeTmpToken("admins", "super_admin");
removeTmpToken("super_admin_properties", "super_admin");

//output("/* ROLLBACK; */");
output("SET unique_checks=1;");
output("SET autocommit = 1;");
output("SET foreign_key_checks = 1;");
output("COMMIT;");
output("SHOW WARNINGS;");

function fakeHospitalName($faker) {
	global $locale;
	if ( $locale == "sv_SE" ) {
		return addslashes($faker->randomElement($array = array($faker->name, $faker->city))." ".$faker->randomElement($array = array('Barnsjukhus', 'Sjukhus', 'Regionsjukhus', 'Lassaret')));
	} else {
		return addslashes($faker->randomElement($array = array($faker->city,$faker->streetName." ".$faker->company)).$faker->randomElement($array = array(' Clinic', ' Hospital', '', ' Unit', ' Center', ' Medical Center', ' Children\'s Hospital')));
	}
	return "";
}

function output($str) {
	echo $str."\n";
}

function updateTable($table, $column, $oldValue, $newValue) {
	return sprintf("UPDATE %s SET %s='%s' WHERE %s='%s';", $table, $column, $newValue, $column, $oldValue);
}

function deleteTable($table, $column, $search, $not = false) {
	return sprintf("DELETE FROM %s WHERE %s ".($not?" NOT ":"")." LIKE '%s';", $table, $column, $search);
}

function processAdmins($con, $dbname)
{
	global $username_prefix;
	global $locale;
	$sql = "SELECT name, salt, email, phone, support FROM admins";
	$result = mysqli_query($con, $sql);

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row["name"]) {
			$oldname = mysqli_real_escape_string($con, $row["name"]);
			$newname = $username_prefix."admin".$i.TMP_TOKEN;
			$hash = password_create("diatest13", $row["salt"]);
			$faker = Faker\Factory::create($locale);
			$email = mysqli_real_escape_string($con, $row["email"]);
			$phone = mysqli_real_escape_string($con, $row["phone"]);
			$support = mysqli_real_escape_string($con, $row["support"]);
			$title = fakeHospitalName($faker);

			if ( strlen($email) > 0 ) {
				$email = addslashes($faker->email);
			}
			if ( strlen($phone) > 0 ) {
				$phone = addslashes($faker->phoneNumber);
			}
			if ( strlen($support) > 0 ) {
				$support = addslashes($faker->randomElement($array = array($faker->email, $faker->name)));
			}
			output(sprintf("UPDATE admins SET name='%s', title='%s', hash='%s', email='%s', phone='%s', support='%s' WHERE name='%s';", $newname, $title, $hash, $email, $phone, $support, $oldname));
			output(updateTable("admin_properties", "admin", $oldname, $newname));
			output(updateTable("clinic_users", "admin", $oldname, $newname));
			output(updateTable("food_collection_owner_clinic", "admin", $oldname, $newname));
			output(updateTable("notification_view_log_clinic", "admin", $oldname, $newname));
			output(updateTable("oauth_client_owner_patient_history", "admin", $oldname, $newname));
			output(updateTable("pdf_profiles", "admin", $oldname, $newname));
			output(updateTable("terminals", "admin", $oldname, $newname));
			output(updateTable("user_ownership", "admin", $oldname, $newname));
			output(updateTable("pdf_generated", "clinic", $oldname, $newname));

			$i++;
		}
	}
}

function processSuperAdmins($con, $dbname)
{
	global $username_prefix;
	global $locale;
	$sql = "SELECT name, salt FROM super_admins";
	$result = mysqli_query($con, $sql);

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row["name"]) {
			$oldname = mysqli_real_escape_string($con, $row["name"]);
			$newname = $username_prefix."super_admin".$i.TMP_TOKEN;
			$hash = password_create("diatest13", $row["salt"]);
			$faker = Faker\Factory::create($locale);
			$comment = addslashes($faker->realText());
			output(sprintf("UPDATE super_admins SET name='%s', hash='%s', comment='%s' WHERE name='%s';", $newname, $hash, $comment, $oldname));
			output(updateTable("admins", "super_admin", $oldname, $newname));
			output(updateTable("super_admin_properties", "super_admin", $oldname, $newname));

			$i++;
		}
	}
}


function processUsers($con, $dbname)
{
	global $username_prefix;
	global $locale;
	$sql = "SELECT pcode, salt, dateofbirth, pnumber FROM users";
	$result = mysqli_query($con, $sql);

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row["pcode"]) {
			$faker = Faker\Factory::create($locale);
			$firstname = addslashes($faker->firstName);
			$lastname = addslashes($faker->lastName);
			$oldpcode = mysqli_real_escape_string($con, $row["pcode"]);
			$newpcode = $username_prefix."user".$i.TMP_TOKEN;
			$hash = password_create("diatest13", $row["salt"]);
			if ( mysqli_real_escape_string($con, $row["dateofbirth"]) != "" ) {
				$birthdate = $faker->date($format = 'Y-m-d', $max = 'now');
				$date = DateTime::createFromFormat('Y-m-d', $birthdate);
			} else {
				$birthdate = "";
				$date = DateTime::createFromFormat('Y-m-d', $faker->date($format = 'Y-m-d', $max = 'now'));
			}
			$pnr = mysqli_real_escape_string($con, $row["pnumber"]);
			if ( strlen($pnr) > 0 ) {
				try {
					$pnr = $faker->personalIdentityNumber($date);
				} catch(Exception $e) {
					$pnr = $faker->randomNumber(7);
				}
			} 
		
			output(sprintf("UPDATE users SET pcode='%s', firstname='%s', lastname='%s', hash='%s', pnumber='%s', dateofbirth='%s' WHERE pcode='%s';", $newpcode, $firstname, $lastname, $hash, $pnr, $birthdate, $oldpcode));
			output(updateTable("deletion_log", "pcode", $oldpcode, $newpcode));
			output(updateTable("access_token_owner_patient", "pcode", $oldpcode, $newpcode));
			output(updateTable("communication", "pcode", $oldpcode, $newpcode));
			output(updateTable("device_ownership", "pcode", $oldpcode, $newpcode));
			output(updateTable("dibs_transactions", "pcode", $oldpcode, $newpcode));
			output(updateTable("food_collection_owner_patient", "pcode", $oldpcode, $newpcode));
			output(updateTable("hba1c", "pcode", $oldpcode, $newpcode));
			output(updateTable("lostpassword_keys", "pcode", $oldpcode, $newpcode));
			output(updateTable("moblog", "pcode", $oldpcode, $newpcode));
			output(updateTable("notification_view_log_patient", "pcode", $oldpcode, $newpcode));
			output(updateTable("oauth_client_owner_patient", "pcode", $oldpcode, $newpcode));
			output(updateTable("oauth_client_owner_patient_history", "pcode", $oldpcode, $newpcode));
			output(updateTable("registration_events", "pcode", $oldpcode, $newpcode));
			output(updateTable("user_licensecodes", "pcode", $oldpcode, $newpcode));
			output(updateTable("user_ownership", "pcode", $oldpcode, $newpcode));
			output(updateTable("user_properties", "pcode", $oldpcode, $newpcode));
			output(updateTable("user_strings", "pcode", $oldpcode, $newpcode));
			output(updateTable("pdf_generated", "patient", $oldpcode, $newpcode));

			$i++;
		}
	}
	output(deleteTable("users", "pcode", $username_prefix."user%", true));
	output(deleteTable("deletion_log", "pcode", $username_prefix."user%", true));
	output(deleteTable("access_token_owner_patient", "pcode", $username_prefix."user%", true));
	output(deleteTable("communication", "pcode", $username_prefix."user%", true));
	output(deleteTable("device_ownership", "pcode", $username_prefix."user%", true));
	output(deleteTable("dibs_transactions", "pcode", $username_prefix."user%", true));
	output(deleteTable("food_collection_owner_patient", "pcode", $username_prefix."user%", true));
	output(deleteTable("hba1c", "pcode", $username_prefix."user%", true));
	output(deleteTable("lostpassword_keys", "pcode", $username_prefix."user%", true));
	output(deleteTable("moblog", "pcode", $username_prefix."user%", true));
	output(deleteTable("notification_view_log_patient", "pcode", $username_prefix."user%", true));
	output(deleteTable("oauth_client_owner_patient", "pcode", $username_prefix."user%", true));
	output(deleteTable("oauth_client_owner_patient_history", "pcode", $username_prefix."user%", true));
	output(deleteTable("registration_events", "pcode", $username_prefix."user%", true));
	output(deleteTable("user_licensecodes", "pcode", $username_prefix."user%", true));
	output(deleteTable("user_ownership", "pcode", $username_prefix."user%", true));
	output(deleteTable("user_properties", "pcode", $username_prefix."user%", true));
	output(deleteTable("user_strings", "pcode", $username_prefix."user%", true));
	output(deleteTable("pdf_generated", "patient", $username_prefix."user%", true));
}

function fakeData($faker, $type, $value) {
	if ( $value == "" ) {
		return "";
	}
	switch ( $type ) {
		case "phoneNumber":
			return addslashes($faker->phoneNumber);
			break;

		case "sentence":
			return addslashes($faker->sentence(20));
			break;

		case "email":
			return addslashes($faker->email);
			break;

		case "name_or_word":
			return addslashes($faker->randomElement($array = array($faker->word, $faker->firstName)));
			break;

		case "ip":
			return addslashes(sprintf("%u",ip2long($faker->ipv4)));
			break;

		case "setting_value":
			if ( strlen($value) > 3 && !is_numeric($value) && strtolower($value) != "mg/dl" && strtolower($value) != "mmol/l" && 
			      strtolower($value) != "english" && !is_numeric(str_replace($value, ":", "")) ) {
				return addslashes($faker->randomElement($array = array($faker->word, $faker->randomNumber(4))));
			}
			return $value;
			break;
	
		case "program_value":
                	if ( strlen($value) > 3 && !stristr($value, 'week') && !stristr($value, 'day') && 
			    !stristr($value, 'period') && !stristr($value, 'work') && !stristr($value, 'school') && 
			    !stristr($value, 'profile') ) {
				return addslashes($faker->randomElement($array = array($faker->word, $faker->randomNumber(4), $faker->firstName)));
			}
			return $value;
			break;

		case "hospital_name":
			return addslashes(fakeHospitalName($faker));
			break;

		case "hash":
			return addslashes(base64_encode($faker->text(12)));
			break;
		default:
			throw new Exception("No data for fake type $type found.");
			break;
	}
	throw new Exception("No data for fake type $type found.");
	return "";
}

function processFieldName($con, $table, $primary_key, $field, $faker_type) {

	global $locale;
	$sql = "SELECT $field, group_concat($primary_key SEPARATOR ',') AS primary_keys FROM $table WHERE $primary_key IS NOT NULL AND $primary_key != '' GROUP BY $field";
	$result = mysqli_query($con, $sql);

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row[$field]) {
			$faker = Faker\Factory::create($locale);
                        $value = mysqli_real_escape_string($con, $row[$field]);
                        $newname = fakeData($faker, $faker_type, $value);
                        if ( $newname != $value ) {
				output("UPDATE $table SET $field ='".$newname."' WHERE $primary_key IN (".$row['primary_keys'].");");
			}
		}
	}
}

function processGeneralTable($con, $table, $primary_key1, $primary_key2, $field_array, $faker_array)
{
	global $locale;
	$sql = "SELECT $primary_key1, $primary_key2, ".implode(", ", $field_array)." FROM $table";
	$result = mysqli_query($con, $sql);

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row[$primary_key1]) {
			$faker = Faker\Factory::create($locale);
			$fields = array();
			$changed_fields = 0;
			foreach ( array_combine($field_array, $faker_array) as $field => $faker_type ) {	
				$value = mysqli_real_escape_string($con, $row[$field]);
				$newvalue = fakeData($faker, $faker_type, $value);
				if ( $newvalue != $value ) {
					$fields[] = $field."='".$newvalue."'";
					$changed_fields++;
				}
			}

			if ( $changed_fields > 0 ) {
				output(sprintf("UPDATE $table SET ".implode(", ", $fields)." WHERE $primary_key1='%s' AND $primary_key2='%s';", $row[$primary_key1], $row[$primary_key2]));
			}
			$i++;
		}
	}
}

function processCommonTable($con, $dbname, $table, $keyitem, $valueitem="value", $seqitem="pcode") {
	global $username_prefix;
	global $locale;
	$sql = "SELECT $seqitem, $keyitem, $valueitem FROM $table";
	$result = mysqli_query($con, $sql);

	while ($row = mysqli_fetch_assoc($result)) {
		if ($row[$seqitem]) {	
			$faker = Faker\Factory::create($locale);
			$value = mysqli_real_escape_string($con, $row[$valueitem]);
			if ( $value == "" ) {
				continue;
			}
			$pcode = mysqli_real_escape_string($con, $row[$seqitem]);
			$string = mysqli_real_escape_string($con, $row[$keyitem]);
			$oldvalue = $value;
			switch ($string) {
				case "adress":
					$value = addslashes($faker->streetAddress);
					break;

				case "city":
					$value = addslashes($faker->city);
					break;

				case "postal_code":
					$value = addslashes($faker->postcode);
					break;

				case "email":
					$value = addslashes($faker->email);
					break;

				case "tm_cell":
				case "mobile_tel":
					$value = addslashes($faker->phoneNumber);
					break;

				case "home_tel": 
				case "work_tel":
					$value = addslashes($faker->phoneNumber);
					break;
	
				case "state":
					if ( $locale == "en_US") {
						$value = addslashes($faker->stateAbbr);
					} else {
						$value = "";
					}
					break;

				case "length":
					$value = addslashes($faker->randomNumber(4));
					break;

				case "weight":
					$value = addslashes($faker->randomNumber(6));
					break;
	
				case "gender":
					$value = addslashes($faker->randomElement($array = array('0', '1', '2')));
					break;

				case "ip":
					$value = addslashes(sprintf("%u",ip2long($faker->ipv4)));
					break;

				default:
					continue 2;
					break;

			}
			if ( $value != $oldvalue ) {
				output(sprintf("UPDATE %s SET %s='%s' WHERE %s='%s' AND %s='%s';", $table, $valueitem, $value, $seqitem, $pcode, $keyitem, $string));
			}
		}
	}

}

function processClinicUsers($con, $dbname)
{
	global $username_prefix;
	global $locale;
	$sql = "SELECT username, salt, comment FROM clinic_users";
	$result = mysqli_query($con, $sql);

	$i = 1;
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row["username"]) {
                        $faker = Faker\Factory::create($locale);
			$firstname = addslashes($faker->firstName);
			$lastname = addslashes($faker->lastName);
			$oldpcode = mysqli_real_escape_string($con, $row["username"]);
			$newpcode = $username_prefix."clinicuser" . $i.TMP_TOKEN;
			$hash = password_create("diatest13", $row["salt"]);
			$comment = "";
			if ( mysqli_real_escape_string($con, $row["comment"]) != "" ) {
				$comment = addslashes($faker->realText($maxNbChars = 20));
			}
			output(sprintf("UPDATE clinic_users SET username='%s', firstname='%s', lastname='%s', hash='%s', comment='%s' WHERE username='%s';", $newpcode, $firstname, $lastname, $hash, $comment, $oldpcode));

			$i++;
		}
	}
}

function clearTableFields($table, $fields) {
	output("UPDATE $table SET ".implode("='', ", $fields)."='';");
}

function removeTmpToken($table, $field) {
	$token_size = strlen(TMP_TOKEN);
	output("UPDATE $table SET $field=REPLACE($field,'".TMP_TOKEN."', '');");
}

function password_create($raw, $salt)
{
	/* Merge unencrypted password and salt. */
	$salted = $raw.'{'.$salt.'}';

	$digest = hash('sha512', $salted, true);

	/* Stretch hash. */
	for ($i = 1; $i < 5000; $i++)
	{
		$digest = hash('sha512', $digest.$salted, true);
	}

	return base64_encode($digest);
}

?>
