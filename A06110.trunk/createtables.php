#!/usr/bin/php

<?php
// 24 Renamed history to transfers, using id:s instead of serial numbers in the table (07-10-05 roric)
// 24 Introduced telemeddata_sec_values, moved insulin_basal_tdd into it, added temperature to it, and removed insulin_pen_cap_data
//    since telemeddata now fulfils the requirements for pen cap data (07-10-05 roric)
// 24 Moved insulin_pump_settings into device_settings (07-10-05 roric)
// 24 Merged insulin_pump_basal_programs and insulin_pump_isf_programs into device_programs (07-10-05 roric)
// 24 Removed insulin_pump_data_flags (07-10-05 roric)
// 24 Merged insulin_pump_data and telemeddata into telemeddata (07-10-05 roric)
// 24 Switched to InnoDB (07-10-05 roric)
// 23 Added insulin_pump_isf_programs, which contains insulin sensitivity factor sequences (07-07 tjn)
// 22 Added device_ownership table which maps users to devices
// 22 Removed flag_id and made the other two columns to a key in insulin_pump_data_flaga and telemeddata_flags
// 22 Added timestamp and device_id indexes in telemeddata and insulin_pump_data
// 22 telemeddata now have type and value, rather then two value fields.(07-05-09 roric)
// 22 Pointed from insulin_pump_data and telemeddata to devices.(07-05-09 roric)
// 22 Added seq and devicetype to meters, renamed meters to devices. Removed insulin pumps. (07-05-09 roric)
// 22 Added seq to terminals, used in telemeddata and insulin_pump_data, pointed to from telmeddata and insulin_pump_data (07-05-09 roric)
// 22 removed flags, mealflag and mealcomment from telemeddata (07-05-09 roric)
// 22 Added metadata table, containing metadata, for instance version info (07-05-09 roric)
// 21 Removed unused latesttransmission and metertype from meters (07-05-03 roric)
// 20 Added insulin_pen_cap_data and admin_hsa_ids (07-05-02 roric)
// 19 Added interval_time_a and interval_time_b in users (07-03-28 Joakim)
// 18 Added glucoseunit til admins (07-01-14 David)
// 17 Removed type from history (07-01-07 David)
// 16 terminals added comment, location (07-01-04 David)
// 15 history type added (07-01-04 David)
// 14 table insulin_pumps added (06-12-28 David)
// 13 default charset utf-8 (06-12-25 David)
// 12 terminal_id unique for table terminals
// 11 added columns to table terminals
// 10 changed errorlog seq to auto_increment
//  9 added table terminals
//  8 renamed columns in errorlog
//  7 changed terminal-id to terminal_id in meters and telemeddata
//  6 Removed column serialnumber from table users
//  5 Values in telemeddata now default to NULL
//  4 Added table meters
//  3 Added staticinterval for table users
//  2 Added columns goalmin och goalmax for table users
//  1 Removed columns int-night-wd amd int-night-we from table users

$host = "localhost";
$user = "root";
$pass = "";
$db = "diasend_apa";

if (isset($_SERVER["argv"][1]))
{
        $arg = $_SERVER["argv"][1];
}
else
{
        $arg = "";
}

// List of known arguments, more arguments are added by later on...
$args = array("all");

function recreate_table($arg, $link, $table, $structure)
{
    if ($arg == $table || $arg == "all")
    {
        $query = "DROP TABLE IF EXISTS `" . $table . "`";
        mysql_query($query, $link) or die('Query failed: ' . mysql_error());

        $query = "
            CREATE TABLE `" . $table . "` ( ".
            $structure
            .") ENGINE=InnoDB DEFAULT CHARSET=utf8
        ";

        mysql_query($query, $link) or die('Query failed: ' . mysql_error());
    }
    
    return $table;
}

$link = mysql_connect($host, $user, $pass) or die('Could not connect: ' . mysql_error());
mysql_select_db($db, $link) or die('Could not select database');

$args[] = recreate_table($arg, $link, "admins",
 "`name` varchar(10) NOT NULL default '',
  `password` varchar(10) NOT NULL default '',
  `title` text NOT NULL,
  `email` varchar(30) NOT NULL default 'diasend@breneman.se',
  `phone` text NOT NULL,
  `hours` text NOT NULL,
  `support` text NOT NULL,
  `misc` text NOT NULL,
  `glucoseunit` int(2) NOT NULL default '0',
  PRIMARY KEY  (`name`)");


$args[] = recreate_table($arg, $link, "users",
 "`pcode` varchar(32) NOT NULL default '0000000000',
  `pnumber` varchar(32) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `admin` varchar(32) NOT NULL default '',
  `firstname` varchar(255) default NULL,
  `lastname` varchar(255) default NULL,
  `address` varchar(32) NOT NULL default '',
  `postcode` varchar(16) NOT NULL default '',
  `city` varchar(32) NOT NULL default '',
  `hometel` varchar(32) NOT NULL default '',
  `mobiletel` varchar(32) NOT NULL default '',
  `worktel` varchar(32) NOT NULL default '',
  `email` varchar(32) NOT NULL default '',
  `goalmin` tinyint(4) NOT NULL default '4',
  `goalmax` tinyint(4) NOT NULL default '12',
  `staticinterval` enum('Yes','No') NOT NULL default 'No',
  `interval_time_b` smallint(4) default NULL,
  `interval_time_a` smallint(4) default NULL,
  `int-br-wd` smallint(4) NOT NULL default '0',
  `int-br-we` smallint(4) NOT NULL default '0',
  `int-lu-wd` smallint(4) NOT NULL default '0',
  `int-lu-we` smallint(4) NOT NULL default '0',
  `int-di-wd` smallint(4) NOT NULL default '0',
  `int-di-we` smallint(4) NOT NULL default '0',
  `int-ev-wd` smallint(4) NOT NULL default '0',
  `int-ev-we` smallint(4) NOT NULL default '0',
  `archived` enum('Yes','No') NOT NULL default 'No',
  PRIMARY KEY  (`pcode`)");

$args[] = recreate_table($arg, $link, "communication",
 "`id` int(11) NOT NULL auto_increment,
  `pcode` varchar(30) NOT NULL default '',
  `is_admin` tinyint(1) NOT NULL default '0',
  `comment` text NOT NULL,
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)");

$args[] = recreate_table($arg, $link, "devices",
 "`seq` int unsigned NOT NULL auto_increment,
  `serialnumber` varchar(30) NOT NULL,
  `devicetype` tinytext,
  `pcode` varchar(32) default NULL,
  PRIMARY KEY (`seq`),
  UNIQUE KEY (`serialnumber`)");

$args[] = recreate_table($arg, $link, "terminals",
 "`seq` int unsigned NOT NULL auto_increment,
  `terminal_id` varchar(32) NOT NULL default '',
  `admin` varchar(32) NOT NULL default '',
  `hw_nr` varchar(32) default NULL,
  `sw_nr` varchar(32) default NULL,
  `bl_nr` varchar(32) default NULL,
  `phone` varchar(32) default NULL,
  `balance` int(11) default NULL,
  `nr_transfers` int(11) default NULL,
  `amount_data` int(11) default NULL,
  `total_amount_data` int(11) default NULL,
  `comment` tinytext,
  `location` tinytext,
  PRIMARY KEY (`seq`),
  UNIQUE KEY `terminal_id` (`terminal_id`)");

$args[] = recreate_table($arg, $link, "telemeddata",
 "`seq` int(11) NOT NULL auto_increment,
  `device_id` int unsigned NOT NULL,
  `terminal_id` int unsigned default NULL,
  `timestamp` datetime NOT NULL,
  `type` ENUM('ketones', 'glucose', 'insulin_basal', 'insulin_bolus', 'insulin_bolus_basal', 'insulin_tdd', 'insulin_prime', 'alarm', 'pen_cap_data') NOT NULL,
  `value` int unsigned default NULL,
  PRIMARY KEY  (`seq`),
  INDEX(`timestamp`),
  INDEX(`device_id`),
  FOREIGN KEY (`device_id`) REFERENCES devices(`seq`),
  FOREIGN KEY (`terminal_id`) REFERENCES terminals(`seq`)
  ");

$args[] = recreate_table($arg, $link, "telemeddata_sec_values",
 "`telemeddata_seq` int(11) NOT NULL auto_increment,
  `type` ENUM('temperature', 'insulin_basal_tdd', 'duration') NOT NULL,
  `value` int unsigned default NULL,
  PRIMARY KEY  (`telemeddata_seq`, `type`),
  FOREIGN KEY (`telemeddata_seq`) REFERENCES telemeddata(`seq`)
  ");

$args[] = recreate_table($arg, $link, "telemeddata_flags",
 "`telemeddata_seq` int(11),
  `flag`     int(11)      NOT NULL,
  PRIMARY KEY (`telemeddata_seq`, `flag`),
  FOREIGN KEY (`telemeddata_seq`) REFERENCES telemeddata(`seq`)
  ");

// Of some reason mysql fails to crea
$args[] = recreate_table($arg, $link, "transfers",
 "`seq` int(11) NOT NULL auto_increment,
  `ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `terminal_id` int default NULL,
  `device_id` int default NULL,
  `result` int default 0,
  `entry` tinytext NOT NULL,  
  PRIMARY KEY  (`seq`)
  ");

$args[] = recreate_table($arg, $link, "device_settings",
 "`device_id` int unsigned NOT NULL,
  `setting` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY  (`device_id`, `setting`),
  FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
  ");

$args[] = recreate_table($arg, $link, "device_programs",
 "`device_id` int unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` ENUM('insulin_basal_program', 'insulin_isf_program') default NULL,
  `time` time NOT NULL,
  `value` int NOT NULL,
   PRIMARY KEY  (`device_id`, `name`, `type`, `time`),
   FOREIGN KEY (`device_id`) REFERENCES devices(`seq`)
   ");



$args[] = recreate_table($arg, $link, "errorlog",
 "`seq` int(11) NOT NULL auto_increment,
  `ts` timestamp, 
  `errorcode` int(11) default NULL,
  `hw_nr` varchar(32) default NULL,
  `sw_nr` varchar(32) default NULL,
  `bl_nr` varchar(32) default NULL,
  `serial_terminal` varchar(32) default NULL,
  `serial_meter` varchar(255) default NULL,
  `metertype` varchar(64) default NULL,
  `data` text,
  PRIMARY KEY  (`seq`)");



$args[] = recreate_table($arg, $link, "terminal_permissions",
 "`seq` int(11) NOT NULL auto_increment,
  `terminal_id` varchar(32) NOT NULL,
  `permission` tinytext NOT NULL,
  PRIMARY KEY  (`seq`)");

$args[] = recreate_table($arg, $link, "terminal_locations",
 "`terminal_id` varchar(32) NOT NULL,
  `database_server` int(11) NOT NULL,
  PRIMARY KEY  (`terminal_id`)");

$args[] = recreate_table($arg, $link, "database_servers",
 "`server_id` int(11) NOT NULL auto_increment,
  `hostname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `passwd`   varchar(255) NOT NULL,
  `port`     int(11)      NOT NULL,
  `db`       varchar(255) NOT NULL,
  PRIMARY KEY  (`server_id`)");

$args[] = recreate_table($arg, $link, "admin_hsa_ids",
 "`admin` varchar(10) NOT NULL,
  `id` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)");

// Table for containing metadata, for instance version information
$args[] = recreate_table($arg, $link, "metadata",
 "`key` varchar(24) NOT NULL,
  `value` varchar(24) NOT NULL,
  PRIMARY KEY  (`key`)");

$args[] = recreate_table($arg, $link, "device_ownership",
 "`pcode` varchar(32) NOT NULL,
  `device_id` int unsigned NOT NULL,
  PRIMARY KEY  (`pcode`, `device_id`)");


// Check if the given argument $arg is one of the known argument, if not, show the help text
if (!in_array($arg, $args))
{
    $first = FALSE;
    print "\nPlease use on of the following arguments: ";
    foreach($args as &$a)
    {
        if (!$first)
        {
            print ", ";
        }
        else
        {
            $first = FALSE;
        }

        print "'" . $a . "'";
    }
    
    print ".\nThe chosen table will be deleted and recreated.\nAll data in the table will be lost!\n\n";
}

print "Done\n";
mysql_close($link);

?>
