<?php
// -----------------------------------------------------------------------------
// Copyright (C) 2012 Aidera AB, Sweden, http://www.aidera.se
// Developed by Endian Technologies AB, Sweden, http://endian.se
// -----------------------------------------------------------------------------

/**
 * Create salt. 
 *
 * @return
 *  Salt.
 */
function password_create_salt()
{
	return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
}

/**
 * Create password using the same algorithm as the new Intutiy web. 
 * 
 * @param string $raw
 *  Unencrypted password.
 *
 * @param string $salt
 *  Salt.
 *
 * @return
 *   Password as string.
 */
function password_create($raw, $salt)
{
	/* Merge unencrypted password and salt. */
	$salted = $raw.'{'.$salt.'}';

	$digest = hash('sha512', $salted, true);

	/* Stretch hash. */
	for ($i = 1; $i < 5000; $i++) 
	{
		$digest = hash('sha512', $digest.$salted, true);
	}

	return base64_encode($digest);
}

/**
 * Compares two passwords.
 *
 * This method implements a constant-time algorithm to compare passwords to
 * avoid (remote) timing attacks.
 *
 * @param string $password1 
 *  The first password
 *
 * @param string $password2 
 *  The second password
 *
 * @return True if the two passwords are the same.
 */
function password_compare($password1, $password2)
{
	if (strlen($password1) !== strlen($password2))
   	{
		return false;
	}

	$result = 0;
	for ($i = 0; $i < strlen($password1); $i++) 
	{
		$result |= ord($password1[$i]) ^ ord($password2[$i]);
	}

	return 0 === $result;
}

/**
 * Validate password using the same algorithm as in the new Intuity web. 
 *
 * @param string $raw
 *  Password from client. 
 * 
 * @param string $hash
 *  Hash from database.
 *
 * @param string $salt
 *  Salt from database. 
 * 
 * @return 
 *  True if the password is ok.
 */
function password_validate($raw, $hash, $salt)
{	
	$digest = password_create($raw, $salt); 

	return password_compare($digest, $hash);
}

?>
