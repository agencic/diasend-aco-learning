package com.purple.cc.androidonmipod;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnSocketSend;
    TextView txtInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSocketSend = (Button)findViewById(R.id.btnSocketSend);
        txtInfo = (TextView)findViewById(R.id.txtInfo);



        btnSocketSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtInfo.setText("Starting socket comm..");

                BackendCommunication bComm = new BackendCommunication("192.168.1.113",55715, txtInfo);
                bComm.execute();


            }
        });
    }
}
