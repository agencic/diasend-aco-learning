# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2016 Diasend AB, http://diasend.com
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE Glucocard SM
# 
import re
import itertools
import Common
import struct
from datetime import datetime
from collections import OrderedDict

from Generic import *
from Defines import *

class GlucocardSMAnalyser(object):

    def __init__(self, data):

        self.result = []

        self.inc_nr_results = 0

        self.result.append({ELEM_DEVICE_MODEL: 'Menarini GLUCOCARD SM'})

        # List index n tag, index n+1 tag payload
        items = re.split(r'TAG(\d{3})!', data)[1:]

        # Unordered dictionary - key tag, value payload
        d = dict(zip(*[iter(items)]*2))

        # Sort function that makes sure that the serial record is first.
        def tag_sort_index(item):
            if item[0] == '003':
                return 0
            return 1000 + int(item[0])

        sorted_d = OrderedDict(sorted(d.items(), key=tag_sort_index))

        callbacks = {
            '000': self.extract_meter_setting,
            '003': self.extract_serial,
            '100': self.extract_number_of_results,
            '102': self.extract_results
        }

        for tag, payload in sorted_d.items():
            if tag in callbacks:
                records = callbacks[tag](payload)
                if isinstance(records, list):
                    self.result.extend(records)
                else:
                    self.result.append(records)

    def extract_meter_setting(self, payload):
        bits = int(payload.split('|')[0], 16)

        record = {
            SETTINGS_LIST: {
                SETTING_GLUCOCARD_SM_BEEPER_VOLUME   : (bits & 0x0003), 
                SETTING_GLUCOCARD_SM_HYPO            : (bits & 0x0004) >> 2, 
                SETTING_DATE_FORMAT                  : (bits & 0x0008) >> 3,
                SETTING_BG_UNIT                      : (bits & 0x0010) >> 4, 
                SETTING_TIME_FORMAT                  : (bits & 0x0020) >> 5, 
                SETTING_GLUCOCARD_SM_DST             : (bits & 0x0040) >> 6, 
                SETTING_GLUCOCARD_SM_HYPER           : (bits & 0x0080) >> 7, 
                SETTING_GLUCOCARD_SM_AVERAGE_DISPLAY : (bits & 0x0700) >> 8, 
                SETTING_GLUCOCARD_SM_AVERAGE_FLAG    : (bits & 0x1800) >> 11, 
                SETTING_GLUCOCARD_SM_BLE             : (bits & 0x2000) >> 13
            }
        }        

        self.inc_nr_results += 1

        return record

    def extract_serial(self, payload):
        return {ELEM_DEVICE_SERIAL: payload.split('|')[0].rstrip()}

    def extract_number_of_results(self, payload):
        try:
            nr_results = int(payload.split('|')[0])
            nr_results += self.inc_nr_results
        except ValueError, e:
            return {}

        return {ELEM_NR_RESULTS: nr_results}

    def extract_results(self, payload):

        results = []

        records = payload.split('|')[:-1]

        for record in records:

            try:
                value = int(record[0:4]) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                dt    = datetime.strptime(record[4:14], '%y%m%d%H%M')
                bits  = int(record[14:16], 16)

                flag_definition = {
                    0:FLAG_RESULT_CTRL,
                    1:FLAG_RESULT_DELETED,
                    2:FLAG_RESULT_OUTSIDE_TEMP,
                    3:FLAG_BEFORE_MEAL,
                    4:FLAG_AFTER_MEAL,
                    5:FLAG_RESULT_BELOW_HYPO,
                    6:FLAG_RESULT_ABOVE_HYPER,
                    7:FLAG_BEFORE_SLEEP
                }

                flags = []

                for bit in range(8):
                    if bits & 2**bit:
                        flags.append(flag_definition[bit])

                # Note : device has a setting so the actual LO value can be
                # either 10 or 20 mg/dL. But the documentation states that 
                # US devices must use 20 so lets use that.
                if value < 20:
                    flags.append(FLAG_RESULT_LOW)
                elif value > 600:
                    flags.append(FLAG_RESULT_HIGH)

                results.append({
                    ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE,
                    ELEM_TIMESTAMP : dt,
                    ELEM_FLAG_LIST : flags,
                    ELEM_VAL       : value})

            except ValueError, e:
                pass

        return results

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalGlucocardSMSerialRecord(line):
    return line if ELEM_DEVICE_SERIAL in line else {}

def EvalGlucocardSMUnitRecord(line):
    return { ELEM_DEVICE_UNIT:"mg/dL" }
    
def EvalGlucocardSMModelRecord(line):
    return line if ELEM_DEVICE_MODEL in line else {}

def EvalGlucocardSMResultRecord(line):
    if any(k in line for k in (SETTINGS_LIST, ELEM_VAL)):
        return line
    return {}

def EvalGlucocardSMChecksumRecord(line, record):
    return True

def EvalGlucocardSMChecksumFile(line):
    return True

def EvalGlucocardSMNrResultsRecord(line):
    return line if ELEM_NR_RESULTS in line else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterGlucocardSM(inList):
    """
    Detect if data comes from a Glucocard SM.
    """
    return DetectDevice('Glucocard SM', inList, DEVICE_METER);

def AnalyseMeterGlucocardSM(inData):
    """
    Analyse data from Glucocard SM.
    """

    d = {}

    d[ "eval_device_model_record" ]  = EvalGlucocardSMModelRecord
    d[ "eval_serial_record" ]        = EvalGlucocardSMSerialRecord
    d[ "eval_unit"]                  = EvalGlucocardSMUnitRecord
    d[ "eval_result_record" ]        = EvalGlucocardSMResultRecord
    d[ "eval_checksum_record" ]      = EvalGlucocardSMChecksumRecord
    d[ "eval_checksum_file" ]        = EvalGlucocardSMChecksumFile
    d[ "eval_nr_results" ]           = EvalGlucocardSMNrResultsRecord

    analyser = GlucocardSMAnalyser(inData)

    resList = AnalyseGenericMeter(analyser.result, d);
    
    return resList

if __name__ == "__main__":

    testfiles = [
        'EAM2-0000083.log', 
        'EAM2-0000087.log', 
        'EAM2-0000088.log', 
        'EAM2-0000092.log', 
        'EAM2-0000093.log']

    for testfile in testfiles:
        with open('test/testcases/test_data/GlucocardSM/%s' % (testfile), 'r') as f:
            d = f.read()
            results = AnalyseMeterGlucocardSM(d)
