# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Error codes
# -----------------------------------------------------------------------------
ERROR_CODE_NONE                     = 0
ERROR_CODE_NO_HEADER_FOUND          = 1
ERROR_CODE_VALUE_ERROR              = 2
ERROR_CODE_UNIT_ERROR               = 3
ERROR_CODE_DATE_TIME                = 4
ERROR_CODE_CHECKSUM                 = 5
ERROR_CODE_NR_RESULTS               = 6
ERROR_CODE_UNKNOWN_METER            = 7
ERROR_CODE_DATABASE                 = 8
ERROR_CODE_UPDATE_AMOUNT_DATA       = 9
ERROR_CODE_TERMINAL_SERIAL_MISSING  = 10
ERROR_CODE_SUBSCRIPTION             = 11
ERROR_CODE_CHECKSUM_FILE            = 12 
ERROR_CODE_NO_MODEL_FOUND           = 13
ERROR_CODE_HEADER_NOT_COMPLETE      = 14
ERROR_CODE_TOO_OLD_SW_VER           = 15
ERROR_CODE_PREPROCESSING_FAILED     = 16
ERROR_CODE_SERIAL_MISSING_IN_RESULT = 17
ERROR_CODE_DUPLICATED_DEVICE        = 18
ERROR_CODE_INVALID_TERMINAL_SWID    = 19   # Terminal SWID (e.g. A10024) is blacklisted and not allowed to insert data into
                                            # database.    
ERROR_CODE_INVALID_TERMINAL_SERNO   = 20   # Terminal SERNO (serial number) is blacklisted.
ERROR_CODE_INVALID_USER             = 21   # The user is not found in the database. 
ERROR_CODE_INVALID_PASSWORD         = 22   # The password is not correct. 
ERROR_CODE_USER_REQUIRED            = 23   # User information is required. 
ERROR_CODE_GENERIC                  = 24
ERROR_CODE_TOO_MANY_PUMPS_REGISTERED = 25
ERROR_CODE_SERIAL_NUMBER_ALREADY_REGISTERED = 26 # Serial number already registered on device with different device-type.
ERROR_CODE_SUBSCRIPTION_DEVICE_RULE = 27 # Rejection according to subscription device rule
ERROR_CODE_BLINDED_DEVICE = 28 # Dexcom G4/G5 blinded device


# --- --- TABLE OF transmitter articles 
valid_transmitters = (\
                       "A06102",
                       "A07013",
                       "A10024",
                       "A14030",
                       "XA0001-SenseonicsApp"
                      )

# --- --- TABLE OF software transmitter articles and serial prefixes
sw_transmitters = {
                    "A10024": ["DDA-DIAS-", "terminal_dda"],
                    "A14030": ["MOB-DIAS-", "terminal_mobile_ui"],
                    "XA0001-SenseonicsApp": ["EXT-SENS-", "terminal_mobile_ui"],
                  }

sw_transmitters_blacklist = (\
                            )

# --- --- TABLE OF mapping between location and database servers location->server_id
database_servers =  {
                     "SE": 1,    # internal
                     "US": 2,    # international
                     "OTHER": 2, # international
                    }

# --- --- DEFINES OF DEVICE TYPES
DEVICE_METER    = "device_meter"
DEVICE_PUMP     = "device_pump"
DEVICE_PEN      = "device_pen"
DEVICE_PEN_CAP  = "device_pen_cap"
DEVICE_PHONE    = "device_phone"
DEVICE_USER     = "device_user"
DEVICE_CLOUD    = "device_cloud"
DEVICE_TOO_EARLY= "device_not_known_yet"    # This is a response if the detect function can not decide
                                            # For instance smartpix supports both pumps and meters
                                            # So the detect function can not tell the device type
                                            # it must be found out by the analyser

# --- --- DEVICE SPECIFIC TAGS
PREFIX_DEVICE_MODEL_NAME_WILDCARD = "*"     # Used by analysers where we have no control over the device name 
                                            # (for example SmartPix), and thereby needs to be 'wildcarded'
                                            # when comparing device models

# --- --- PROTOCOL DEFINES

# Defines for responses 
RSP_OK   = "!OK"
RSP_NAK  = "!NAK"
RSP_LOAD = "!LOAD"
RSP_RDY  = "!READY"


# --- --- DATABASE DEFINES

# Required db version
REQ_DB_VERSION           = 117 # R1r


# Value types defines
VALUE_TYPE_GLUCOSE       = "glucose"
VALUE_TYPE_KETONES       = "ketones"
VALUE_TYPE_INS_BASAL     = "insulin_basal"
VALUE_TYPE_INS_BOLUS     = "insulin_bolus"        # Ordinary bolus, use for extended bolus, but set duration and flag
VALUE_TYPE_INS_BOLUS_EXT = "insulin_bolus_ext"    # Extended part of combo bolus, long term bolus
VALUE_TYPE_INS_BOLUS_BASAL= "insulin_bolus_basal" # Long term bolus, typically given by a insulin pen. Stored in 1/10000 u.
VALUE_TYPE_INS_TDD       = "insulin_tdd"
VALUE_TYPE_INS_BASAL_TDD = "insulin_basal_tdd"
VALUE_TYPE_ALARM         = "insulin_alarm"
VALUE_TYPE_INS_PRIME     = "insulin_prime"
VALUE_TYPE_PEN_CAP_OFF   = "pen_cap_off"
VALUE_TYPE_TEMP          = "temperature"
VALUE_TYPE_DURATION      = "duration"  # Bolus duration expressed in minutes
VALUE_TYPE_EXERCISE      = "training"
VALUE_TYPE_CARBS         = "carbs"
VALUE_TYPE_EVENT         = "event"
VALUE_TYPE_INS_BOLUS_SUGGESTED       = "insulin_bolus_suggested"       # Normally considered the total of suggested meal, suggested correction and IOB
VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL  = "insulin_bolus_sugg_meal"       # Part of VALUE_TYPE_INS_BOLUS_SUGGESTED
VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR  = "insulin_bolus_sugg_corr"       # Part of VALUE_TYPE_INS_BOLUS_SUGGESTED
VALUE_TYPE_INS_BOLUS_IOB             = "insulin_bolus_iob"             # Part of VALUE_TYPE_INS_BOLUS_SUGGESTED
VALUE_TYPE_INS_BOLUS_PROGRAMMED      = "insulin_bolus_programmed"      # Normwally considered the total of programmed meal and programmed correction
VALUE_TYPE_INS_BOLUS_PROGRAMMED_MEAL = "insulin_bolus_prog_meal"       # Part of VALUE_TYPE_INS_BOLUS_PROGRAMMED
VALUE_TYPE_INS_BOLUS_PROGRAMMED_CORR = "insulin_bolus_prog_corr"       # Part of VALUE_TYPE_INS_BOLUS_PROGRAMMED
VALUE_TYPE_INS_BOLUS_OVERRIDE        = "insulin_bolus_override"        # Precalculated override (not normally used, only if separately stored)
VALUE_TYPE_INS_BASAL_PERCENTAGE      = "insulin_basal_percentage"      # Procentage of basal temporary change 
VALUE_TYPE_UUID         = "uuid"    # Unique ID of the item, can be useful if it needs to be referenced by the device
                                    # but also in situations when the timestamp can not be exactly determined and might
                                    # vary slightly between transfers, then the UUID can be used to avoid duplicate
                                    # values


# Multiplication factors for various types
VAL_FACTOR_GLUCOSE_MMOL = 10 # Actually resolution is 1000, but 100 is multiplied in Generic
VAL_FACTOR_GLUCOSE_MGDL = 1 # Actually resolution is 1000, but 1000 is multiplied in Generic
VAL_FACTOR_KETONES_MMOL = 10 # Actually resolution is 1000, but 100 is multiplied in Generic
VAL_FACTOR_KETONES_MGDL = 1 # Actually resolution is 1000, but 100 is multiplied in Generic
VAL_FACTOR_BOLUS        = 10000
VAL_FACTOR_BASAL        = 1000
VAL_FACTOR_TDD          = 10000
VAL_FACTOR_PRIME        = 100 # This is taken from Animas
VAL_FACTOR_CARBS        = 1
VAL_FACTOR_CONV_MMOL_TO_MGDL = 18.016

# Program type
PROGRAM_TYPE             = "device_program"
PROGRAM_TYPE_BASAL       = "basal_program" # In units
PROGRAM_TYPE_ISF         = "isf_program" # BG value, mmol/l * 1000
PROGRAM_TYPE_IC_RATIO    = "ic_ratio_program" # The values are in g per U
PROGRAM_TYPE_BG_TARGET   = "bg_target_program" # BG target values, can have secondary +- value
PROGRAM_TYPE_BG_THRESHOLD= "bg_threshold_program" # BG units * 1000 

PROGRAM_VALUE_BG_DEVIATION = "bg_deviation"
PROGRAM_VALUE_BG_THRESHOLD = "bg_threshold"

SETTINGS_LIST            = "device_settings"

PROGRAM_NAME             = "device_program_name"
PROGRAM_PERIODS          = "device_program_periods" # Each element is a tuple (datetime.time, units * 1000)

COMMENT_TYPE             = "comment"

BLACK_BOX_HISTORY_RECORD = "black_box_history"


# Elements of result records
ELEM_VAL_TYPE            = "device_value_type"
ELEM_VAL                 = "device_value"
ELEM_TIMESTAMP           = "device_date_time"
ELEM_VALUE_LIST          = "device_value_list"
ELEM_DEVICE_UNIT         = "meter_unit"
ELEM_FLAG_LIST           = "meter_flaglist"
ELEM_SW_VERSION          = "device_version"
ELEM_TERMINAL_VERSION    = "terminal_version"
ELEM_DEVICE_MODEL        = "device_model"
ELEM_DEVICE_SERIAL       = "meter_serial"
ELEM_CHECKSUM            = "meter_checksum"
ELEM_DEVICE_CLASS        = "device_class"
ELEM_NR_RESULTS          = "meter_nr_results"
ELEM_CGM_METER           = "meter_cgm"  # 1 if cgm meter

# Parameters to results
PARAM_TERMINAL_VERSION_RECOMMENDED = "terminal_version_recommended"
PARAM_DEVICE_VERSION_RECOMMENDED = "device_version_recommended"

# Flag defines
FLAG_RESULT_WORKING_HIGH = 100 # Result between the result_low_limit and the result_high_limit, taken when the temperature falls within the higher working region. 
FLAG_RESULT_WORKING_LOW  = 101 # Result between the result_low_limit and the result_high_limit, taken when the temperature falls within the lower working region. 
FLAG_RESULT_HIGH         = 102 # Result greater than the result_high_limit.
FLAG_RESULT_LOW          = 103 # Result less than the result_low_limit
FLAG_RESULT_OUTSIDE_TEMP = 104 # Result outside the normal temperature range but within the lockout limits.
FLAG_RESULT_ERRONEOUS    = 105 # Result marked by the meter or user as being “Erroneous”.
FLAG_RESULT_CTRL         = 106 # Result marked by the user as being that for a “Control” solution.
FLAG_RESULT_CTRL_2       = 107 # Result marked by the user as being that for a “Control” solution.
FLAG_RESULT_AVERAGE      = 108 # Used to determine the number of results used to calculate an average, ( e.g. if 10 results were averaged the status would be 128 + 10 = 138 ).
FLAG_RESULT_ALT_TESTSITE = 109
FLAG_REUSED_DRUM         = 110
FLAG_EXPIRED_DRUM        = 111
FLAG_STRIP_WARNING       = 112
FLAG_NO_DATE_SET         = 113
FLAG_RESULT_BELOW_HYPO   = 114
FLAG_GENERAL             = 115
FLAG_CTRL_NOT_IDENTIFIED = 116
FLAG_RESULT_BELOW_USER_RANGE = 117
FLAG_RESULT_ABOVE_USER_RANGE = 118
FLAG_PARITY_ERROR            = 119
FLAG_TIMESTAMP_IN_FUTURE     = 120 # If the timestamp of the record is in the future it is tagged with this flag
FLAG_RESULT_DELETED          = 121 # Value has been deleted, but still exists in the meter
FLAG_POSSIBLE_KETONES       = 122 # Flag indicating that there might be ketones present in the blood
FLAG_CONTINOUS_READING  = 123 # This flag indicates that the value has been read automatically and not by the user, for instance Abbott Navigator
FLAG_RESULT_HIGH_TEMP   = 124
FLAG_RESULT_LOW_TEMP    = 125
FLAG_MANUAL             = 126 # Value typed in manually by the user
FLAG_LOST_TIME          = 127 # Reading was taken during lost time
FLAG_LOW_BATTERY        = 128 # Reading was taken during low battery
FLAG_METER_REMOTE       = 129 # Value was taken from a Meter Remote
FLAG_CALIBRATION        = 130 # Calibration value
FLAG_USER_TRIGGERED     = 131 # Used to indicate user triggered CGM values.
FLAG_STRUCTURED_TESTING = 132 # If the value is a part of a structured testing, for instance Roche 3-day
FLAG_ADVICED            = 133 # The value was adviced
FLAG_POWER_ON           = 134
FLAG_POWER_OFF          = 135
FLAG_TIMESTAMP_IN_SERVER_TIMEZONE = 136 # If the timestamp is in the timezone of the server, not the device
FLAG_OUTSIDE_RANGE	= 137 # Reading outside the physical range..
FLAG_CALIBRATION_ERROR	= 138 # A calibration error has occured, reading is invalid
FLAG_RESULT_ABOVE_HYPER = 139

FLAG_ORIGIN_FILE_IMPORT = 198 # Indicates that the value is imported from a file, i.e "unsafe" value
FLAG_CUSTOM             = 199 # @TODO: Remove this?

#MEAL FLAGS
FLAG_BEFORE_MEAL       = 200
FLAG_AFTER_MEAL        = 201
FLAG_BEFORE_BREAKFAST  = 202
FLAG_AFTER_BREAKFAST   = 203
FLAG_BEFORE_LUNCH      = 204
FLAG_AFTER_LUNCH       = 205
FLAG_BEFORE_DINNER     = 206
FLAG_AFTER_DINNER      = 207
FLAG_NIGHT             = 208
FLAG_JUNK_FOOD         = 209
FLAG_FASTING           = 210
FLAG_SKIPPED_MEAL      = 211
FLAG_BEFORE_SLEEP      = 212
FLAG_BREAKFAST         = 213
FLAG_LUNCH             = 214
FLAG_DINNER            = 215
FLAG_SNACK             = 216
FLAG_MEAL_AMOUNT1      = 217
FLAG_MEAL_AMOUNT2      = 218
FLAG_MEAL_AMOUNT3      = 219
FLAG_MORNING           = 220
FLAG_EVENING           = 221
FLAG_MEAL              = 222
FLAG_EXTENDED_RANGE    = 223

#EXERCISE FLAGS
FLAG_BEFORE_EXERCISE   = 300
FLAG_DURING_EXERCISE   = 301
FLAG_AFTER_EXERCISE    = 302
FLAG_MILD_EXERCISE     = 303
FLAG_HARD_EXERCISE     = 304
FLAG_MEDIUM_EXERCISE   = 305
FLAG_EXERCISE1         = 306
FLAG_EXERCISE2         = 307

# REST OF THE FLAGS
FLAG_STRESS            = 400
FLAG_FEEL_HYPO         = 401
FLAG_ILLNESS           = 402
FLAG_MENSES            = 403
FLAG_VACATION          = 404
FLAG_NOT_ENOUGH_FOOD   = 405
FLAG_TOO_MUCH_FOOD     = 406
FLAG_MEDICATION        = 407
FLAG_LOGBOOK           = 408
FLAG_OTHER             = 409
FLAG_PARTY             = 410
FLAG_CHECK_MARK        = 411 # Added for GlucoMen LX PLUS - the user can add a check mark to a result
FLAG_PREMENSTRUAL      = 412
FLAG_EVENT             = 413 # Added for the Omnitest 5 (Infopia)

FLAG_TEMPERATURE_DRIFT = 430 # Abnormal temperature drift
FLAG_SUSPICIOUS        = 431 # For instance Senseonics uses this for calibration values which are strange
FLAG_INSULIN = 432
FLAG_SICK_DAY = 433
FLAG_DEVICE_GLUCOSE_ON_TARGET = 434
FLAG_DEVICE_GLUCOSE_BELOW_TARGET = 435
FLAG_DEVICE_GLUCOSE_ABOVE_TARGET = 436


# Flags for glucose values in combined devices
FLAG_MISSED_BOLUS      = 470
FLAG_CARB_GUESS        = 471
FLAG_BASAL_EVAL_START  = 472
FLAG_BASAL_EVAL_STOP   = 473

# Ketone flags
FLAG_KETONES_NEG       = 500
FLAG_KETONES_TRACE     = 501
FLAG_KETONES_SMALL     = 502
FLAG_KETONES_MODERATE  = 503
FLAG_KETONES_LARGE     = 504
FLAG_DEVICE_KETONE_IN_RANGE = 505
FLAG_DEVICE_KETONE_ELEVATED = 506
FLAG_DEVICE_KETONE_HIGH = 507


# PUMP RELATED FLAGS
FLAG_TEMP_MODIFIED     = 1000
FLAG_SUSPENDED         = 1001
# This flag should not be used anymore since, FLAG_BOLUS_TYPE_COMBO means the same
#FLAG_BOLUS_COMBO       = 1002
FLAG_BOLUS_TYPE_COMBO_EXT   = 1003
FLAG_BOLUS_TYPE_COMBO_NORMAL= 1004
FLAG_BOLUS_COMPLETED   = 1005
FLAG_CANCELED           = 1006
FLAG_BOLUS_ACTIVE      = 1007
FLAG_BOLUS_INACTIVE    = 1008
# This flag is depricated, a normal bolus is an unflagged bolus..
#FLAG_BOLUS_TYPE_NORMAL = 1009
FLAG_BOLUS_TYPE_AUDIO  = 1010
FLAG_BOLUS_TYPE_COMBO  = 1011
# Pump Prime flags
FLAG_NOT_PRIMED         = 1012
FLAG_PRIMED             = 1013
FLAG_CANNULA_BOLUS      = 1014
FLAG_TUBING             = 1015
FLAG_RESUME_TRIGGER_OTHER = 1016
FLAG_SUSPEND_TRIGGER_OTHER = 1017
FLAG_PROGRAM_CHANGE        = 1018
FLAG_TEMP_BASAL_CANCELLED  = 1019
FLAG_CARTRIDGE_EMPTY    = 1020 
FLAG_TIME_CHANGED       = 1021
FLAG_DELIVERY_STOPPED   = 1022
FLAG_TEMP_MODIFIED_PERCENTAGE  = 1023

# More bolus flags      
FLAG_BOLUS_TYPE_CORRECTION  = 1030
FLAG_BOLUS_TYPE_MEAL        = 1031
FLAG_BOLUS_TYPE_CARB        = 1032
FLAG_BOLUS_TYPE_EXT         = 1033
FLAG_BOLUS_TYPE_EZBG        = 1034
FLAG_BOLUS_TYPE_EZCARB      = 1035

# Remote features
FLAG_BOLUS_REMOTE_INITIATED = 1050
FLAG_BOLUS_REMOTE_CANCELLED = 1051
FLAG_BASAL_REMOTE_INITIATED = 1052

# Flag first/last glucose value in group (think session).
FLAG_FIRST_GLUCOSE_VALUE_IN_GROUP   = 1060
FLAG_LAST_GLUCOSE_VALUE_IN_GROUP    = 1061


# Impact of insulin (MDI devices, Navigator, pens etc etc)
FLAG_INSULIN_IMPACT_RAPID_ACTING     = 1100
FLAG_INSULIN_IMPACT_SHORT_ACTING     = 1101
FLAG_INSULIN_IMPACT_MIX              = 1102
FLAG_INSULIN_IMPACT_LONG             = 1103

FLAG_INSULIN_IMPACT_INTERMEDIATE     = 1110
FLAG_INSULIN_IMPACT_OTHER            = 1111

# Device specific flags

FLAG_ASANTE_STILL_PENDING                 = 10000
FLAG_ASANTE_USER_ACKED                    = 10001
FLAG_ASANTE_CRADLE_ATTACHED               = 10002
FLAG_ASANTE_PUMP_ATTACHED                 = 10003
FLAG_ASANTE_PUMP_ATTACHED_GOOD_BATT       = 10004
FLAG_ASANTE_PUMP_ALLOWS_FLASHLIGHT        = 10005
FLAG_ASANTE_PUMP_DETACHED                 = 10006
FLAG_ASANTE_U_I_START_PUMP                = 10007
FLAG_ASANTE_NO_POWER                      = 10008
FLAG_ASANTE_VERY_LOW_CART                 = 10009
FLAG_ASANTE_CARTRIDGE_EMPTY               = 10010
FLAG_ASANTE_EXITED_PRIME                  = 10011
FLAG_ASANTE_SET_TIME_DATE                 = 10012
FLAG_ASANTE_MAX_LEVEL                     = 10013
FLAG_ASANTE_STOP_REASON_DETACHED          = 10014
FLAG_ASANTE_STOP_REASON_STOP_BTN          = 10015
FLAG_ASANTE_STOP_REASON_ALARM             = 10016
FLAG_ASANTE_STOP_REASON_PRIME             = 10017
FLAG_ASANTE_STOP_REASON_SET_CLOCK         = 10018
FLAG_ASANTE_STOP_REASON_XCHNG_PUMP        = 10019
FLAG_ASANTE_STOP_REASON_SETTINGS          = 10020
FLAG_ASANTE_STOP_REASON_BATT_UNSAFE       = 10021
FLAG_ASANTE_COMPLETION_CODE_NORMAL        = 10022
FLAG_ASANTE_COMPLETION_CODE_ALARM         = 10023
FLAG_ASANTE_COMPLETION_CODE_USER_STOP     = 10024
FLAG_ASANTE_COMPLETION_CODE_DETACHED      = 10025
FLAG_ASANTE_COMPLETION_CODE_PRIME         = 10026
FLAG_ASANTE_COMPLETION_CODE_IN_PROGRESS   = 10027
FLAG_ASANTE_COMPLETION_CODE_CLEARED       = 10028
FLAG_ASANTE_COMPLETION_CODE_SYS_RESET     = 10029
FLAG_ASANTE_COMPLETION_CODE_BATT_NOT_SAFE = 10030
FLAG_ASANTE_COMPLETION_CODE_UNDEFINED     = 10031
FLAG_ASANTE_SUFFICIENT_POWER              = 10032
FLAG_ASANTE_TEMP_BASAL_ENDED              = 10033
FLAG_ASANTE_CARTRIDE_DATE_OK              = 10034
FLAG_ASANTE_PB_REMINDER_RESET             = 10035

FLAG_ABBOTT_FREESTYLE_LIBRE_RAPID_ACTING_INSULIN = 10040
FLAG_ABBOTT_FREESTYLE_LIBRE_TIME_CHANGE          = 10041
FLAG_ABBOTT_FREESTYLE_LIBRE_FIRST_SENSOR_POINT   = 10042

FLAG_CONTROL_SOLUTION_TYPE_LOW  = 11001
FLAG_CONTROL_SOLUTION_TYPE_MID  = 11002
FLAG_CONTROL_SOLUTION_TYPE_HIGH = 11003

FLAG_LINEARITY_SOLUTION_LEVEL_1 = 11011
FLAG_LINEARITY_SOLUTION_LEVEL_2 = 11012
FLAG_LINEARITY_SOLUTION_LEVEL_3 = 11013
FLAG_LINEARITY_SOLUTION_LEVEL_4 = 11014
FLAG_LINEARITY_SOLUTION_LEVEL_5 = 11015

# Device setting indexes
SETTING_AUDIO_BOLUS_ENABLE              = 1
SETTING_AUDIO_BOLUS_INCREMENT           = 2
SETTING_ADV_BOLUS_OPTIONS_ENABLE        = 3
SETTING_BOLUS_REMINDER_OPTIONS_ENABLE   = 4
SETTING_BOLUS_DELIVERY_SPEED            = 5 # 0 - normal, 1 - slow, 5 - moderate, 6 - very slow
SETTING_BASAL_PROGRAMS_IN_UI            = 6
SETTING_MAX_BASAL_RATE                  = 7 # Rate * 1000
SETTING_MAX_BOLUS                       = 8 # U * 1000
SETTING_MAX_TDD                         = 9 # U * 1000
SETTING_MAX_SETTINGS_LOCK               = 10
SETTING_LANG_SELECTION_IDX              = 11
SETTING_DISPLAY_TIMEOUT                 = 12 # Seconds
SETTING_AUTO_OFF_ENABLE                 = 13
SETTING_AUTO_OFF_TIMEOUT                = 14 # Hours
SETTING_CARTRIDGE_WARNING_LEVEL         = 15 # Duplicate of SETTING_LOW_INSULIN_LEVEL
SETTING_OCCLUSION_SENSITIVITY_LEVEL     = 16
SETTING_INSULIN_ON_BOARD_ENABLE         = 17
SETTING_INSULIN_ON_BOARD_DECAY_DURATION = 18 # Nipro raw value
SETTING_IC_RATIO_3                      = 19 # Only used on the IR1200. 
SETTING_IC_RATIO_0                      = 20
SETTING_IC_RATIO_1                      = 21
SETTING_IC_RATIO_2                      = 22
SETTING_ACTIVE_BASAL_PROGRAM_ID         = 23
SETTING_SICK_BG_OVER_LIMIT              = 24
SETTING_SICK_KETOONE_CHECK_TIME         = 25
SETTING_SICK_CHECK_BG_TIME              = 26
SETTING_BASAL_PROG1_NAME                = 27
SETTING_BASAL_PROG2_NAME                = 28
SETTING_BASAL_PROG3_NAME                = 29
SETTING_BASAL_PROG4_NAME                = 30
SETTING_BASAL_PROG5_NAME                = 31
SETTING_BASAL_PROG6_NAME                = 32
SETTING_BASAL_PROG7_NAME                = 33
SETTING_BASAL_PROG8_NAME                = 34
SETTING_BOLUS_IMMIDIATE_DURATION        = 35
SETTING_MEAL_BOLUS_UNITS                = 36
SETTING_CARB_RATIO                      = 37 # TODO: How is this stored in the cozmo
SETTING_BOLUS_INCREMENT                 = 38 # U * 1000
SETTING_CARB_INCREMENT                  = 39
SETTING_EXTENDED_BOLUS_ENABLE           = 40
SETTING_COMBINATION_BOLUS_ENABLE        = 41
SETTING_CORR_BOLUS_MAIN_MENU            = 42
SETTING_CORR_BOLUS_MEAL_BOLUS           = 43
SETTING_CORR_BOLUS_UNITS                = 44
SETTING_TARGET_BG_MGDL                  = 45 # x mg/dl
SETTING_TARGET_BG_MMOLL                 = 46
SETTING_CORR_BOLUS_FACTOR_MGDL          = 47
SETTING_CORR_BOLUS_FACTOR_MMOLL         = 48
SETTING_INSULIN_DURATION_OF_ACTION      = 49 # minutes
SETTING_TEMP_RATE_ENABLE                = 50
SETTING_TEMP_RATE_METHOD                = 51 # 1 = U/h, % = 2, or Off = 0
SETTING_TEMP_RATE_COMPLETE_ALARM_ENABLE = 52
SETTING_TEMP_RATE_ACTIVE_ALARM_ENABLE   = 53
SETTING_TEMP_RATE_ACTIVE_ALARM_INTERVAL = 54
SETTING_ALERT_VOLUME                    = 55
SETTING_ALERT_TYPE                      = 56
SETTING_ALARM_AUTO_OFF_ENABLE           = 57 # 0 - disable, 1 enable
SETTING_ALARM_AUTO_OFF_INTERVAL         = 58
SETTING_ALARM_AUTO_OFF_CUSTOM_TEXT_ENABLE   = 59
SETTING_ALARM_AUTO_OFF_CUSTOM_TEXT1     = 60
SETTING_ALARM_AUTO_OFF_CUSTOM_TEXT2     = 61
SETTING_ALARM_AUTO_OFF_CUSTOM_TEXT3     = 62
SETTING_RESVOL_LOW_AMOUNT               = 63
SETTING_SITE_CHANGE_REMINDER_ENABLE     = 64
SETTING_SITE_CHANGE_INTERVAL            = 65 # Days
SETTING_SITE_CHANGE_TIME                = 66
SETTING_DELIVERY_LIMIT_AMOUNT           = 67
# SETTING_DELIVERY_LIMIT_AMOUNT was copy and pasted twice
SETTING_GLUCOSE_REMINDER_ENABLE         = 69 # 0 - disable, 1 - enable
SETTING_GLUCOSE_REMINDER_INTERVAL       = 70
SETTING_REPORT_AVERAGE_DAYS             = 71
SETTING_REPORT_DELIVERY_SUMMARY_ENABLE  = 72
SETTING_REPORT_AVG_DELIVERY_SUMMARY_ENABLE  = 73
SETTING_REPORT_BASAL_PERCENT_TDD_ENABLE     = 74
SETTING_REPORT_AVG_BASAL_PERCENT_TDD_ENABLE = 75
SETTING_REPORT_BOLUS_HISTORY_ENABLE     = 76
SETTING_REPORT_COMPLETE_HISTORY_ENABLE  = 77
SETTING_DATE_FORMAT                     = 78
SETTING_TIME_FORMAT                     = 79 # 0 - 12h, 1 - 24h
SETTING_NUMERIC_FORMAT                  = 80
SETTING_CUSTOM_PUMP_LABEL_PRESENT       = 81
SETTING_PUMP_NAME                       = 82
SETTING_CUSTOM_HOME_TEXT_PRESENT        = 83
SETTING_CUSTOM_HOME_TEXT1               = 84
SETTING_CUSTOM_HOME_TEXT2               = 85
SETTING_PROGRAM_LOCK_ENABLE             = 86
SETTING_SYNC_CLOCK                      = 87
SETTING_DATE                            = 88
SETTING_TIME                            = 89
SETTING_AUDIO_BOLUS_CARB_INCREMENT      = 90
SETTING_HOME_IOB_SCREEN_ENABLE          = 91
SETTING_HOME_BG_SCREEN_ENABLE           = 92
SETTING_TEMP_RATE_SCREEN_ENABLE         = 93
SETTING_KEY_BEEP_ENABLE                 = 94
SETTING_HOME_SITE_REMINDER_SCREEN_ENABLE    = 95
SETTING_BG_LOW_ALERT_ENABLE             = 96
SETTING_BG_LOW_ALERT_INTERVAL           = 97
SETTING_BG_LOW_ALERT_LIMIT_MGDL         = 98
SETTING_BG_LOW_ALERT_LIMIT_MMOLL        = 99
SETTING_BG_HIGH_ALERT_ENABLE            = 100
SETTING_BG_HIGH_ALERT_INTERVAL          = 101
SETTING_BG_HIGH_ALERT_LIMIT_MGDL        = 102
SETTING_BG_HIGH_ALERT_LIMIT_MMOLL       = 103
SETTING_REPORT_AVERAGE_BG_DAYS          = 104
SETTING_REPORT_BG_HISTORY_ENABLE        = 105
SETTING_REPORT_AVG_BG_HISTORY_ENABLE    = 106

SETTING_BG_UNIT                         = 107 # "mg/dl", "mmol/l"
SETTING_BG_UNIT_MGDL                    = "mg/dl"
SETTING_BG_UNIT_MMOLL                   = "mmol/L"
SETTING_VOLUME                          = 108
SETTING_BUTTON_FEEDBACK                 = 109 # 0 - Off, 1 - On
SETTING_NOTIFICATION_MODE               = 110 # 0 - Audio, 1 - Vibration, 2 - Booth
SETTING_LCD_CONTRAST                    = 111 # 0 - Low, 1 - Medium, 2 - High
SETTING_LOW_INSULIN_LEVEL               = 112 # U
SETTING_INACTIVITY_TIMEOUT              = 113 # Timeout in minutes
SETTING_INACTIVITY_TIMEOUT_ENABLE       = 114 # 0 - disabled, 1 - enabled
SETTING_LOCKOUT                         = 115 # 0 - unlocked, 1 - Locked 
SETTING_EXT_BOLUS_MODES                 = 116 # 0 - disabled, 1 - enabled. Duplicate of SETTING_EXTENDED_BOLUS_ENABLE
SETTING_STEALTH_BOLUS_ENABLE            = 117 # 0 - disabled, 1 - enabled
SETTING_STEALTH_INCREMENT               = 118 # nipro raw value
SETTING_BOLUS_ESTIMATOR_ENABLE          = 119 # 0 - disabled, 1 - enabled

SETTING_REMINDER_0                      = 120 # 0xffffffff disabled, seconds since mid night
SETTING_REMINDER_1                      = 121
SETTING_REMINDER_2                      = 122
SETTING_REMINDER_3                      = 123
SETTING_REMINDER_4                      = 124
SETTING_REMINDER_5                      = 125
SETTING_REMINDER_6                      = 126
SETTING_REMINDER_7                      = 127
SETTING_REMINDER_8                      = 128
SETTING_REMINDER_9                      = 129

SETTING_INFUSION_FREQUENCY              = 130 # minutes
SETTING_SYRINGE_PRESSURE                = 131 # Nipro raw value
SETTING_MIN_OPTI_SENSOR_DELTA           = 132 # Nipro raw value
SETTING_BASAL_PROGRAMS                  = 133 # Number of programs
SETTING_INSULIN_SENSITIVITY             = 134 # 1U/x mg/dl

SETTING_PAIRED_DEVICE                   = 135 # Serial number of the paired device
SETTING_TUBING_PRIME_SIZE               = 136 # In 0.01 units
SETTING_PRIME_SIZE                      = 137 # In 0.01 units

SETTING_OCCLUSION_PRESSURE              = 140 # 0 - low, 1 - high
SETTING_OCCLUSION_LOW                   = 141 # Nipro raw value
SETTING_OCCLUSION_HIGH                  = 142 # Nipro raw value

SETTING_MAX_2HOURS                      = 143 # U * 1000

SETTING_INSULIN_INCREMENT               = 144 # U * 1000

SETTING_SOUND_ENABLED                   = 150 # 1 / 0
SETTING_VACUUM_ENABLED                  = 151 # 1 / 0

SETTING_CARB_UNIT                       = 152 # 0 - Grams, 1 - BE, 2 - CU, 3 - BW, 4 - CC, 5 - KE

SETTING_LANGUAGE                        = 160 # String with the language name

# BG Settings
SETTING_BG_GOAL_MIN                     = 200 # mmol/L * 1000
SETTING_BG_GOAL_UPPER                   = 201 # mmol/L * 1000

# Bolus suggestion settings
SETTING_MIN_BG_CALC                     = 301 # mmol/L * 1000
SETTING_REVERSE_BOLUS_CALC_ENABLE       = 302 # 0 - no, 1 - yes
SETTING_SUGGESTION_BOLUS_CALC_ENABLE    = 303 # 0 - no, 1 - yes

# ALARMS
SETTING_REMINDERS_ENABLE                = 401 # 0 - off, 1 - on
SETTING_CONFIDENCE_ALERT_ENABLE         = 402 # 0 - off, 1 - on

# Bolus
SETTING_EXT_BOLUS_TYPE                  = 500 # % = 1, units = 2, off = 0

SETTING_BOLUS_ADVICE_ENABLED            = 501 # 0 - false, 1 - true

# Insulet
SETTING_POD_EXPIRE_ALERT                = 1000 # minutes

# CGM Settings
SETTING_CGM_SOUND_TYPE_USER_HIGH_GLUCOSE_WARNING = 1100 # 0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High
SETTING_CGM_SOUND_TYPE_USER_LOW_GLUCOSE_WARNING  = 1101 # 0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High
SETTING_CGM_SOUND_TYPE_RISE_RATE_WARNING = 1102 # 0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High  
SETTING_CGM_SOUND_TYPE_FALL_RATE_WARNING = 1103 # 0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High
SETTING_CGM_SOUND_TYPE_TRM_OUT_OF_RANGE_WARNING = 1104 # Transmitter out of range warning; 0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High
SETTING_CGM_SOUND_TYPE_OTHER_WARNING = 1105 # All warnings not covered explicitly; 0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High
SETTING_CGM_HIGH_GLUCOSE_WARNING_LIMIT = 1106 # 120 – 400 mg/dL in mg/dL (the meter uses 20 mg/dL steps internally)
SETTING_CGM_LOW_GLUCOSE_WARNING_LIMIT = 1107 # 60 – 100 mg/dL in mg/dL (the meter uses 10 mg/dL steps internally)
SETTING_CGM_RISE_RATE_LIMIT = 1108 # 2 mg/dL or 3 mg/dL
SETTING_CGM_FALL_RATE_LIMIT = 1109 # 2 mg/dL or 3 mg/dL 
SETTING_CGM_SNOOZE_TIME_USER_LOW_GLUCOSE  = 1110 # 0 - 300 minutes expressed in minutes (meter uses 30 min steps internally)
SETTING_CGM_SNOOZE_TIME_USER_HIGH_GLUCOSE = 1111 # 0 - 300 minutes expressed in minutes (meter uses 30 min steps internally)
SETTING_CGM_SNOOZE_TIME_TRM_OUT_OF_RANGE  = 1112 # Transmitter out of range snooze. 0 - 300 minutes expressed in minutes 
                                                 # (meter uses 30 min steps internally)
SETTING_CGM_USER_LOW_GLUCOSE_WARNING  = 1113 # 0 = Disable, 1= Enable 
SETTING_CGM_USER_HIGH_GLUCOSE_WARNING = 1114 # 0 = Disable, 1= Enable
SETTING_CGM_RISE_RATE_WARNING         = 1115 # 0 = Disable, 1= Enable                                                 
SETTING_CGM_FALL_RATE_WARNING         = 1116 # 0 = Disable, 1= Enable
SETTING_CGM_TRM_OUT_OF_RANGE_WARNING  = 1117 # Transmitter out of range warning. 0 = Disable, 1= Enable
SETTING_CGM_TRANSMITTER_ID            = 1118 # Transmitter ID. Five ASCII characters.
SETTING_CGM_BLUETOOTH_ENABLED_STATUS  = 1119 # Bluetooth enabled status. One Byte. 

SETTING_DEXCOM_HIGH_GLUCOSE_ALERT_TYPE          = 1200 # 0 = Vibrate, 1 = VibrateAndBeep, 2=Silent, 3=Disabled
SETTING_DEXCOM_LOW_GLUCOSE_ALERT_TYPE           = 1201 # 0 = Vibrate, 1 = VibrateAndBeep, 2=Silent, 3=Disabled
SETTING_DEXCOM_UP_RATE_ALERT_TYPE               = 1202 # 0 = Vibrate, 1 = VibrateAndBeep, 2=Silent, 3=Disabled
SETTING_DEXCOM_UP_RATE_ALERT_TYPE               = 1203 # 0 = Vibrate, 1 = VibrateAndBeep, 2=Silent, 3=Disabled
SETTING_DEXCOM_OUT_OF_RANGE_ALERT_TYPE          = 1204 # 0 = Vibrate, 1 = VibrateAndBeep, 2=Silent, 3=Disabled
SETTING_DEXCOM_OTHER_ALERT_TYPE                 = 1205 # 0 = Vibrate, 1 = VibrateAndBeep, 2=Silent, 3=Disabled
SETTING_DEXCOM_BEEP_ON_RECEIVE                  = 1206 # ?
SETTING_DEXCOM_DISPLAY_MODE                     = 1207 # ?
SETTING_DEXCOM_BACKLIGHT_ENABLED                = 1208 # ?
SETTING_DEXCOM_RECORDS_IN_LAST_METER_TRANSFER   = 1209 # ?
SETTING_DEXCOM_METER_TYPE                       = 1210 # ?
SETTING_DEXCOM_LAST_METER_TRANSFER_TIME         = 1211 # ?
SETTING_DEXCOM_METER_SERIAL                     = 1212 # ?

SETTING_DEXCOM_CLOUD_ALERT_OUT_OF_RANGE        = 1220
SETTING_DEXCOM_CLOUD_ALERT_LOW                 = 1230
SETTING_DEXCOM_CLOUD_ALERT_HIGH                = 1240
SETTING_DEXCOM_CLOUD_ALERT_FALL                = 1250
SETTING_DEXCOM_CLOUD_ALERT_RISE                = 1260
SETTING_DEXCOM_CLOUD_ALERT_VALUE_OFFSET        = 0
SETTING_DEXCOM_CLOUD_ALERT_SNOOZE_VALUE_OFFSET = 1
SETTING_DEXCOM_CLOUD_ALERT_ENABLED_OFFSET      = 2 # 0 disabled - 1 enabled

SETTING_INSULIN_CALCULATOR           = 0x1000 # 0 = Off, 1 = Easy, 2 = Advanced
SETTING_INSULIN_MANUAL_LOG           = 0x1001 # Bit 0 -> 0 = Off, 1 = On; Bit 1 -> 0 long lasting insulin Off, 1 = On
SETTING_SMART_TAGS                   = 0x1002 # 0 = Off, 1 = On
SETTING_INSULIN_CALCULATOR_DOSE_UNIT = 0x1003 # 0 = 0.5 U, 1 = 1 U
SETTING_TYPE_OF_CORRECTION_TARGET    = 0x1004 # 0 = single target, 1 = range target
SETTING_INSULIN_CALCULATOR_FOOD_UNIT = 0x1005 # 0 = by Grams of carbs, 1 = Servings

# Correction Blood Glucose Single Target 
#
# Base 0x1010
#
# Bit 0-6
#  0 = No fixed time 
#  1 = Morning
#  2 = Midday
#  3 = Evening
#  4 = Night
#  
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET                = 0x1010 # Base setting for targets.
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_NO_FIXED_TIME  = 0x1010 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_MORNING        = 0x1011 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_MIDDAY         = 0x1012 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_EVENING        = 0x1013 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_NIGHT          = 0x1014 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_TIME_MODE      = 0x101F # 0 = No fixed time, 1 = Fixed time (morning/midday etc)

# Correction Blood Glucose Target ratio range
#
# Base 0x1020
#
# Bit 1-7
#  0 = No fixed time 
#  1 = Morning
#  2 = Midday
#  3 = Evening
#  4 = Night
#  
#  Bit 0
#   0 = Low
#   1 = High
#        
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET                    = 0x1020 # Base setting for targets.
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_NO_FIXED_TIME_LOW  = 0x1020 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_NO_FIXED_TIME_HIGH = 0x1021 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_MORNING_LOW        = 0x1022 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_MORNING_HIGH       = 0x1023 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_MIDDAY_LOW         = 0x1024 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_MIDDAY_HIGH        = 0x1025 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_EVENING_LOW        = 0x1026 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_EVENING_HIGH       = 0x1027 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_NIGHT_LOW          = 0x1028 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_NIGHT_HIGH         = 0x1029 # mg/dL
SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_TIME_MODE          = 0x102F # 0 = No fixed time, 1 = Fixed time (morning/midday etc)

# Blood Glucose drop (Correction Factor
#
# Base 0x1030
#
# Bit 0-6
#  0 = No fixed time 
#  1 = Morning
#  2 = Midday
#  3 = Evening
#  4 = Night
#  
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR               = 0x1030 # Base setting.
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_NO_FIXED_TIME = 0x1030 # mg/dL
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_MORNING       = 0x1031 # mg/dL
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_MIDDAY        = 0x1032 # mg/dL
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_EVENING       = 0x1033 # mg/dL
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_NIGHT         = 0x1034 # mg/dL
SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_TIME_MODE     = 0x103F # 0 = No fixed time, 1 = Fixed time (morning/midday etc)

# Carbohydrate insulin ratio.
#
# Base 0x1040
#
# Bit 0-6
#  0 = No fixed time 
#  1 = Morning
#  2 = Midday
#  3 = Evening
#  4 = Night
#  
SETTING_CARB_INSULIN_RATIO               = 0x1040 # Base setting.
SETTING_CARB_INSULIN_RATIO_NO_FIXED_TIME = 0x1040 # g
SETTING_CARB_INSULIN_RATIO_MORNING       = 0x1041 # g
SETTING_CARB_INSULIN_RATIO_MIDDAY        = 0x1042 # g
SETTING_CARB_INSULIN_RATIO_EVENING       = 0x1043 # g
SETTING_CARB_INSULIN_RATIO_NIGHT         = 0x1044 # g
SETTING_CARB_INSULIN_RATIO_TIME_MODE     = 0x104F # 0 = No fixed time, 1 = Fixed time (morning/midday etc)

# Carbohydrate Insulin Ratio per Serving.
#
# Base 0x1050
#
# Bit 0-6
#  0 = No fixed time 
#  1 = Morning
#  2 = Midday
#  3 = Evening
#  4 = Night
#  
SETTING_CARB_INSULIN_PER_SERVING_RATIO               = 0x1050 # Base setting.
SETTING_CARB_INSULIN_PER_SERVING_RATIO_NO_FIXED_TIME = 0x1050 # 0.5 U
SETTING_CARB_INSULIN_PER_SERVING_RATIO_MORNING       = 0x1051 # 0.5 U
SETTING_CARB_INSULIN_PER_SERVING_RATIO_MIDDAY        = 0x1052 # 0.5 U
SETTING_CARB_INSULIN_PER_SERVING_RATIO_EVENING       = 0x1053 # 0.5 U
SETTING_CARB_INSULIN_PER_SERVING_RATIO_NIGHT         = 0x1054 # 0.5 U
SETTING_CARB_INSULIN_PER_SERVING_RATIO_TIME_MODE     = 0x105F # 0 = No fixed time, 1 = Fixed time (morning/midday etc)

# Bolus advice timeblock
# 
# bit 4-7 timeblock index
# 
SETTING_BOLUS_ADVICE_TIMEBLOCK                                   = 0x1100 # HH:MM:SS
SETTING_BOLUS_ADVICE_TIMEBLOCK_END_TIME                          = 0x1101 # HH:MM:SS
SETTING_BOLUS_ADVICE_TIMEBLOCK_MIN_BG_VALUE                      = 0x1102 # mg/dl
SETTING_BOLUS_ADVICE_TIMEBLOCK_MAX_BG_VALUE                      = 0x1103 # mg/dl
SETTING_BOLUS_ADVICE_TIMEBLOCK_CARB_RATIO_INSULIN_VALUE          = 0x1104 # units (0.0001 divisions)
SETTING_BOLUS_ADVICE_TIMEBLOCK_CARB_RATIO_CARB_VALUE             = 0x1105 # g
SETTING_BOLUS_ADVICE_TIMEBLOCK_INSULIN_SENSITIVITY_INSULIN_VALUE = 0x1106 # units (0.0001 divisions)
SETTING_BOLUS_ADVICE_TIMEBLOCK_INSULIN_SENSITIVITY_BG_VALUE      = 0x1107 # mg/dl

# Health events
#
# All units + / - integer
SETTING_HEALTH_EVENT_VALUE              = 0x1200
SETTING_HEALTH_EVENT_EXCERCISE_1_VALUE  = 0x1200
SETTING_HEALTH_EVENT_EXCERCISE_2_VALUE  = 0x1201
SETTING_HEALTH_EVENT_STRESS_VALUE       = 0x1202
SETTING_HEALTH_EVENT_ILLNESS_VALUE      = 0x1203
SETTING_HEALTH_EVENT_PREMENSTRUAL_VALUE = 0x1204
SETTING_HEALTH_EVENT_CUSTOMIZED_1       = 0x1209
SETTING_HEALTH_EVENT_CUSTOMIZED_2       = 0x120a
SETTING_HEALTH_EVENT_CUSTOMIZED_3       = 0x120b


SETTINGS_BOLUS_ADVICE_OPTIONS_MEAL_EXCURSION = 0x1205  # mg/dl
SETTINGS_BOLUS_ADVICE_OPTIONS_SNACK_LIMIT    = 0x1206  # in g according to Roche 7005079 doc, but might be in "carb units" 
SETTINGS_BOLUS_ADVICE_OPTIONS_ACTIVE_TIMEOUT = 0x1207  # HH:MM:SS
SETTINGS_BOLUS_ADVICE_OPTIONS_OFFSET_TIMEOUT = 0x1208  # HH:MM:SS

SETTING_ASANTE_SMART_BOLUS_ENABLE      = 0x1300
SETTING_ASANTE_SMART_BOLUS_INITIALIZED = 0x1301
# SETTING_ASANTE_BG_UNITS_TYPE           = 0x1302
SETTING_ASANTE_INSULIN_ACTION          = 0x1303
SETTING_ASANTE_IOB_MODE                = 0x1304
SETTING_ASANTE_BOLUS_BUTTON_SELECT     = 0x1305
SETTING_ASANTE_COMBO_BOLUS_ENABLE      = 0x1306
SETTING_ASANTE_TIMED_BOLUS_ENABLE      = 0x1307
SETTING_ASANTE_BOLUS_REMINDER_ENABLE   = 0x1308
SETTING_ASANTE_BOLUS_STEP_SIZE         = 0x1309
SETTING_ASANTE_AUDIO_BOLUS_STEP_SIZE   = 0x130a
SETTING_ASANTE_BOLUS_LIMIT             = 0x130b
SETTING_ASANTE_ACTIVE_PROFILE          = 0x130c
SETTING_ASANTE_BASAL_LIMIT             = 0x130d
SETTING_ASANTE_TIME_FORMAT             = 0x130e
SETTING_ASANTE_BG_REMINDER_ENABLE      = 0x130f
SETTING_ASANTE_BG_REMINDER_TIME        = 0x1310
SETTING_ASANTE_LOW_INSULIN_ENABLE      = 0x1311
SETTING_ASANTE_LOW_INSULIN_LEVEL       = 0x1312
SETTING_ASANTE_NOTIFICATION_TIMING     = 0x1313
SETTING_ASANTE_DELIVERY_LIMIT          = 0x1314
SETTING_ASANTE_AUTO_OFF_ENABLE         = 0x1315
SETTING_ASANTE_AUTO_OFF_DURATION       = 0x1316
SETTING_ASANTE_PUMP_REMINDER_ENABLE    = 0x1317
SETTING_ASANTE_PUMP_REMINDER_HOURS     = 0x1318
SETTING_ASANTE_TARGET_BG_MIN           = 0x1319
SETTING_ASANTE_TARGET_BG_MAX           = 0x131a
SETTING_ASANTE_BEEP_VOLUME             = 0x131b
SETTING_ASANTE_BUTTON_GUARD_ENABLE     = 0x131c
SETTING_ASANTE_SPLASH_SCREEN_ENABLE    = 0x131d
SETTING_ASANTE_FLASHLIGHT_ENABLE       = 0x131e
SETTING_ASANTE_SCREEN_TIMEOUT          = 0x131f
SETTING_ASANTE_THEME_COLOR             = 0x1320

SETTING_LONG_ACTING_INSULIN_BASE_DOSE_MORNING    = 0x1400 # 0-99 U, 255 = disabled
SETTING_LONG_ACTING_INSULIN_BASE_DOSE_EVENING    = 0x1401 # 0-99 U, 255 = disabled
SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_BREAKFAST = 0x1402 # 0-99 U, 255 = disabled
SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_LUNCH     = 0x1403 # 0-99 U, 255 = disabled
SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_DINNER    = 0x1404 # 0-99 U, 255 = disabled

SETTING_FIRST_DAY_OF_WEEK = 0x1420 # 0 = Monday, 1 = Sunday

SETTING_TANDEM_QB_ACTIVE                = 0x1500 # 0 = disable, 1 = enable
SETTING_TANDEM_QB_INCREMENT_UNITS       = 0x1501 # In 0.001 U
SETTING_TANDEM_QB_INCREMENT_CARBS       = 0x1502 # In 0.001 grams
SETTING_TANDEM_QB_DATA_ENTRY_TYPE       = 0x1503 # 0 - insulin 1 - carbs
SETTING_TANDEM_BUTTON_ANNUN             = 0x1504 # 0 - Audio high, 1 = Audio medium, 2 = Audio Low, 3 = Vibe
SETTING_TANDEM_QUICK_BOLUS_ANNUN        = 0x1505 # 0 - Audio high, 1 = Audio medium, 2 = Audio Low, 3 = Vibe
SETTING_TANDEM_BOLUS_ANNUN              = 0x1506 # 0 - Audio high, 1 = Audio medium, 2 = Audio Low, 3 = Vibe
SETTING_TANDEM_REMINDER_ANNUN           = 0x1507 # 0 - Audio high, 1 = Audio medium, 2 = Audio Low, 3 = Vibe
SETTING_TANDEM_ALERT_ANNUN              = 0x1508 # 0 - Audio high, 1 = Audio medium, 2 = Audio Low, 3 = Vibe
SETTING_TANDEM_ALARM_ANNUN              = 0x1509 # 0 - Audio high, 1 = Audio medium, 2 = Audio Low, 3 = Vibe
SETTING_TANDEM_CANNULA_PRIME_SIZE       = 0x150a # In 0.01 units
SETTING_TANDEM_PUMP_LOCKED              = 0x150d # Pump lock enable, 0 - No, 1 - Yes

# Tandem has 9 different reminders (ends at 0x153d)
# The reminders are:
# 0 - Low BG
# 1 - High BG
# 2 - Site change
# 3 - Missed bolus 0
# 4 - Missed bolus 1
# 5 - Missed bolus 2
# 6 - Missed bolus 3
# 7 - BG reminder
# 8 - Additional Bolus reminder
#	NOTE: this (Additional Bolus reminder) was not mentioned in the spec,
#	but after we found inconsistencies we got the information that this
#	reminder existed
SETTING_TANDEM_REMINDER_BASE            = 0x1510
SETTING_TANDEM_REMINDER_FREQ_OFFSET         = 0 # Offset of the freqency (minutes)
SETTING_TANDEM_REMINDER_START_TIME_OFFSET   = 1 # Offset of the start time (minute since midnight)
SETTING_TANDEM_REMINDER_END_TIME_OFFSET     = 2 # Offset of the end time (minute since midnight)
SETTING_TANDEM_REMINDER_ACTIVE_DAYS_OFFSET  = 3 # Offset of active days (bitmask bit 0 - monday)
SETTING_TANDEM_REMINDER_ENABLED_OFFSET      = 4 # Offset of enable (1 - enabled)


SETTING_FREESTYLE_LIBRE_VOLUME = 0x1600 # 0 = Low, 1 = High


SETTING_STRUCTURED_TESTING_ACTIVE   = 0x1900 # 0 - Not active, 1 - active
SETTING_STRUCTURED_TESTING_START    = 0x1901 # String start time YYYY-MM-DD HH:MM
SETTING_STRUCTURED_TESTING_BREAKFAST= 0x1902 # String HH:MM
SETTING_STRUCTURED_TESTING_LUNCH    = 0x1903 # String HH:MM
SETTING_STRUCTURED_TESTING_DINNER   = 0x1904 # String HH:MM
SETTING_STRUCTURED_TESTING_BEDTIME  = 0x1905 # String HH:MM

SETTING_GLUCOCARD_SM_BEEPER_VOLUME   = 0x1A00 # 0 - mute, 1 - low, 2 - medium - 3 high
SETTING_GLUCOCARD_SM_HYPO            = 0x1A01 # 0 - off, 1 - on 
SETTING_GLUCOCARD_SM_HYPER           = 0x1A02 # 0 - off, 1 - on 
SETTING_GLUCOCARD_SM_DST             = 0x1A03 # 0 - off, 1 - on 
SETTING_GLUCOCARD_SM_AVERAGE_DISPLAY = 0x1A04 # 0 - 7, 1 - 14, 2 - 30, 3 - 60 and 4 - 90 days 
SETTING_GLUCOCARD_SM_AVERAGE_FLAG    = 0x1A05 # 0 - none, 1 - before meal, 2 - after meal
SETTING_GLUCOCARD_SM_BLE             = 0x1A06 # 0 - off, 1 -on 

# Alarms - starts at 100000 since animas uses lowest 16 bits
ALARM_POWER_OFF         = 100001
ALARM_POWER_ON          = 100002
ALARM_POWER_INTERRUPT   = 100003
ALARM_POWER_AUTO_OFF    = 100004
ALARM_POWER_AUTO_OFF_ADVISORY = 100005

ALARM_BATTERY_TYPE      = 100009
ALARM_BATTERY_OUTAGE    = 100010
ALARM_BATTERY_LOW       = 100011
ALARM_REPLACE_BATTERY   = 100012
ALARM_BATTERY_REMOVED   = 100013
ALARM_BATTERY_CRITICAL  = 100014

ALARM_SYSTEM_FAULT      = 100015
ALARM_SYSTEM_FAULT_RECOVERABLE= 100016
ALARM_CALL_SERVICE      = 100017
ALARM_INTERNAL_ERROR    = 100018

ALARM_CARTRIDGE_TYPE    = 100019
ALARM_CARTRIDGE_EMPTY   = 100020
ALARM_CARTRIDGE_VERY_LOW= 100021
ALARM_CARTRIDGE_LOW     = 100022
ALARM_CARTRIDGE_LOADED  = 100023
ALARM_CARTRIDGE_REMOVED = 100024
ALARM_CARTRIDGE         = 100025
ALARM_CARTRIDGE_REPLACE = 100026
ALARM_CARTRIDGE_NO_COUNT= 100027
ALARM_CARTRIDGE_CRITICAL= 100028
ALARM_CARTRIDGE_LOW_NIPRO = 100029 # Nipro's own text

ALARM_OCCLUSION_STARTED = 100030
ALARM_OCCLUSION_ENDED   = 100031
ALARM_OCCLUSION_1       = 100032 # Animas undocumented occlusion alarms
ALARM_OCCLUSION_2       = 100033 # Animas undocumented occlusion alarms
ALARM_OCCLUSION_3       = 100034 # Animas undocumented occlusion alarms
ALARM_OCCLUSION_4       = 100035 # Animas undocumented occlusion alarms
ALARM_OCCLUSION         = 100036 # Nipro's own text
ALARM_OCCLUSION_5       = 100037 # Animas undocumented occlusion alarms

ALARM_DATE_CHANGED      = 100040
ALARM_DATE_CHANGE_NEEDED= 100041
ALARM_ALARM_CLOCK       = 100042
ALARM_REMINDER          = 100043

ALARM_BAD_HISTORY_DATA  = 100050
ALARM_CHECK_ERROR       = 100051
ALARM_SHUTDOWN          = 100052
ALARM_DAILY_MAX_FULL    = 100053

ALARM_MECHANICAL_ERROR      = 100054
ALARM_ELECTRONICAL_ERROR    = 100055
ALARM_BLUETOOTH_ERROR       = 100056

#basal related
ALARM_BASAL_PROGRAM_CHANGE  = 100060
ALARM_REVIEW_BASAL          = 100061

#bolus related
ALARM_MISSED_MEAL_BOLUS    =  100080
ALARM_BOLUS_CANCELLED      =  100081

# PRime related
ALARM_NOT_PRIMED           =  100090

#Delivery
ALARM_DELIVERY_LIMIT_ATTENTION  = 100100
ALARM_DELIVERY_LIMIT_WARNING    = 100101
ALARM_DELIVERY_STARTED          = 100102
ALARM_DELIVERY_STARTED_REMOTE   = 100103
ALARM_DELIVERY_STOPPED          = 100104
ALARM_DELIVERY_STOPPED_REMOTE   = 100105
ALARM_DELIVERY_STOPPED_USER     = 100106
ALARM_DELIVERY_STOPPED_EXCEED_TDD   = 100107
ALARM_DELIVERY_STOPPED_EXCEED_BASAL = 100108
ALARM_DELIVERY_STOPPED_EXCEED_BOLUS = 100109

ALARM_DELIVERY_BASAL_SUSPENDED      = 100110
ALARM_DELIVERY_BASAL_SUSPENDED_15MIN= 100111
ALARM_DELIVERY_PRIME_NOT_DELIVERED  = 100112
ALARM_DELIVERY_BOLUS_SUSPENDED      = 100113
ALARM_DELIVERY_BOLUS_CANCELED       = 100114
ALARM_DELIVERY_EXT_BOLUS_CANCELED   = 100115
ALARM_DELIVERY_EXT_BOLUS_COMPLETE   = 100116
ALARM_DELIVERY_EXT_BOLUS_ACTIVE     = 100117
ALARM_DELIVERY_TEMP_BASAL_CANCELED  = 100118
ALARM_DELIVERY_TEMP_BASAL_COMPLETE  = 100119
ALARM_DELIVERY_TEMP_BASAL_ACTIVE    = 100120
ALARM_DELIVERY_SUSPENDED            = 100121
ALARM_RF_TIMEOUT                    = 100122
ALARM_DELIVERY_STOPPED_USER_METER   = 100123

# Glucose
ALARM_HIGH_BLOOD_GLUCOSE        = 100200
ALARM_LOW_BLOOD_GLUCOSE         = 100201
ALARM_PROJ_HIGH_BLOOD_GLUCOSE        = 100202 # projected high in future
ALARM_PROJ_LOW_BLOOD_GLUCOSE         = 100203 # projected low in future

ALARM_KEY_STUCK                 = 100250
ALARM_KEY_STUCK_NIPRO           = 100251 # Nipro's own text

ALARM_INACTIVITY_TIMEOUT        = 100270

ALARM_CALIBRATION_ERROR         = 100290
ALARM_STATUS_ERROR              = 100291

# CGM warnings
ALARM_CGM_EAW_BASE              = 110000 # Add this offset to Animas EAW to
                                         # get unique representation in backend
ALARM_CGM_EAW_CALL_SERVICE_NO_DELIVERY    = 110127 # CALL SERVICE No delivery. Remove battery to silence the alarm. 
ALARM_CGM_EAW_REPLACE_BATTERY_NO_DELIVERY = 110128 # REPLACE BATTERY No delivery. Remove battery to silence the alarm       
ALARM_CGM_EAW_EMPTY_CARTRIDGE_NO_DELIVERY = 110144 # EMPTY CARTRIDGE NO DELIVERY. Replace cartridge.
ALARM_CGM_EAW_OCCLUSION_DETECTED_145      = 110145 # OCCLUSION DETECTED NO DELIVERY 
ALARM_CGM_EAW_OCCLUSION_DETECTED_146      = 110146 # OCCLUSION DETECTED NO DELIVERY 
ALARM_CGM_EAW_OCCLUSION_DETECTED_147      = 110147 # OCCLUSION DETECTED NO DELIVERY 
ALARM_CGM_EAW_OCCLUSION_DETECTED_148      = 110148 # OCCLUSION DETECTED NO DELIVERY 
ALARM_CGM_EAW_OCCLUSION_DETECTED_149      = 110149 # OCCLUSION DETECTED NO DELIVERY 
ALARM_CGM_EAW_AUTO_OFF_NO_DELIVERY        = 110150 # AUTO-OFF NO DELIVERY   
ALARM_CGM_EAW_LOW_BATTERY                 = 110177 # Low battery.   
ALARM_CGM_EAW_LOW_CARTRIDGE               = 110178 # Low cartridge. 
ALARM_CGM_EAW_SENSOR_ERROR_0              = 110201 # CGM Sensor Error 0     
ALARM_CGM_EAW_SENSOR_ERROR_1              = 110202 # CGM Sensor Error 1     
ALARM_CGM_EAW_SENSOR_FAILURE              = 110203 # CGM Sensor Failure     
ALARM_CGM_EAW_BG_CALIBRATION_REQ          = 110205 # Enter BG - BG Calibration Required     
ALARM_CGM_EAW_TRANSMITTER_OUT_OF_RANGE    = 110206 # Transmitter Out of Range       
ALARM_CGM_EAW_GLUCOSE_LEVEL_BELOW_LOW_LIMIT    = 110208 # Glucose Level Below Low Glucose Alert Limit    
ALARM_CGM_EAW_SESSION_EXPIRES_IN_30_MINS       = 110211 # CGM Session will Expire in 30 mins     
ALARM_CGM_EAW_SENSOR_EXPIRED_CGM_SESSION_ENDED = 110212 # Sensor Expired - CGM Session Ended     
ALARM_CGM_EAW_GLUCOSE_LEVEL_ABOVE_HIGH_LIMIT   = 110213 # Glucose Level Above High Glucose Alert Limit   
ALARM_CGM_EAW_GLUCOSE_LEVEL_BELOW_55_MG_DL     = 110214 # Glucose Level is Below 55 mg/dL
ALARM_CGM_EAW_FAILURE                          = 110215 # CGM Failure    
ALARM_CGM_EAW_SESSION_STOPPED                  = 110216 # CGM Session Stopped    
ALARM_CGM_EAW_GLUCOSE_LEVEL_RISING_TOO_QUICKLY = 110217 # Glucose Level Rising Too Quickly       
ALARM_CGM_EAW_GLUCOSE_LEVEL_FALLING_TOO_QUICKLY = 110218 # Glucose Level Falling Too Quickly      
ALARM_CGM_EAW_CHECK_BG_BEFORE_ADJUSTMENTS = 110221 # Check BG Before Making any Therapeutic Adjustments     
ALARM_CGM_EAW_TRANSMITTER_LOW_BATTERY     = 110223 # Replace CGM Transmitter Low Battery 


# Insulet specific errors
ALARM_INSULET_EEPROM            = 200000
ALARM_INSULET_RAM               = 200001
ALARM_INSULET_SWTIMER           = 200002
ALARM_INSULET_ROM               = 200003
ALARM_INSULET_STACK             = 200004
ALARM_INSULET_CODE              = 200005
ALARM_INSULET_QUEUE             = 200006
ALARM_INSULET_RTC_FAIL          = 200007
ALARM_INSULET_REMOTE_HW         = 200008
ALARM_INSULET_ID                = 200009
ALARM_INSULET_CNFM_ERROR        = 200010
ALARM_INSULET_PUMP_FAIL         = 200011
ALARM_INSULET_COMM              = 200012
ALARM_INSULET_REMOTE            = 200013
ALARM_INSULET_EXPIRED           = 200014
ALARM_INSULET_OCCL              = 200015
ALARM_INSULET_ACTIVATE          = 200016
ALARM_INSULET_ALARM             = 200017
ALARM_INSULET_COMM              = 200018
ALARM_INSULET_OCCL_ADVISORY     = 200019
ALARM_INSULET_SUSPEND           = 200020
ALARM_INSULET_EXP1              = 200021
ALARM_INSULET_EXP2              = 200022
ALARM_INSULET_FLASH             = 200023
ALARM_INSULET_EXP_WARNING       = 200024

# Insulinx specific errors / events
ALARM_INSULINX_ERROR                             = 200100
ALARM_INSULINX_TIME_LOST                         = 200101
ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_1 = 200102
ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_2 = 200103
ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_3 = 200104
ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_4 = 200105
ALARM_INSULINX_RESET_TO_FACTORY_CONFIGURATION    = 200106
ALARM_INSULINX_RESULT_CLEARED                    = 200107
ALARM_INSULINX_REBOOT                            = 200108
ALARM_INSULINX_RECOVERY                          = 200109
ALARM_OPTIUM_NEO_BASAL_DOSE                      = 200110
ALARM_OPTIUM_NEO_INSULIN_MEAL_SETUP              = 200111
ALARM_OPTIUM_NEO_BASAL_TITRATION_SETUP           = 200112
ALARM_OPTIUM_NEO_MEAL_CORRECTION_SETUP           = 200113
ALARM_OPTIUM_NEO_SUCCESSFUL_ROM_CAL_READ         = 200114
ALARM_OPTIUM_NEO_NO_TITRATION_ADJUSTMENT         = 200115
ALARM_OPTIUM_NEO_TITRATION_ACCEPTED_OR_REJECTED  = 200116
ALARM_LIBRE_CLEAR_RESULT_AND_INSULIN_DATABASE    = 200117
ALARM_LIBRE_CLEAR_SCAN_DATABASE                  = 200118
ALARM_LIBRE_CLEAR_ACTIVATION_DATABASE            = 200119
ALARM_LIBRE_CLEAR_HISTORICAL_DATABASE            = 200120
ALARM_LIBRE_USER_TIME_CHANGE                     = 200121
ALARM_LIBRE_CLEAR_EVENT_LOG                      = 200122
ALARM_LIBRE_RECOVERY                             = 200123
ALARM_LIBRE_MICROCONTROLLER_RESET                = 200124
ALARM_LIBRE_MASKED_MODE_STATUS                   = 200125
ALARM_LIBRE_SENSOR_EXPIRED                       = 200126
ALARM_LIBRE_DATABASE_RECORD_NUMBER_WRAP          = 200127

# Asante specific errors / events
ALARM_ASANTE_DROPPED_PUMP               = 200200
# ALARM_ASANTE_NO_POWER                 = 200201
# ALARM_ASANTE_SET_CLOCK                = 200202
ALARM_ASANTE_AUTO_OFF                   = 200203
# ALARM_ASANTE_OCCLUSION                = 200204
# ALARM_ASANTE_CARTRIGE_EMPTY           = 200205
ALARM_ASANTE_ADAPTER                    = 200206
ALARM_ASANTE_PUMP_DRIVE                 = 200207
ALARM_ASANTE_WET_PUMP                   = 200208
ALARM_ASANTE_RESEAT_PUMP                = 200209
ALARM_ASANTE_DELIVERY_LIMIT             = 200210
ALARM_ASANTE_PUMP_DETACH                = 200211
ALARM_ASANTE_CHOOSE_SETTING             = 200212
ALARM_ASANTE_VRSN_MISMATCH              = 200213
ALARM_ASANTE_ALERT_KEY                  = 200214
ALARM_ASANTE_ALERT_POWER_VERY_LOW       = 200215
# ALARM_ASANTE_ALERT_CARTRIDGE_VERY_LOW = 200216
ALARM_ASANTE_ALERT_TEMP_BASAL_STATUS    = 200217
ALARM_ASANTE_ALERT_CARTRIDGE_LOW        = 200218
ALARM_ASANTE_ALERT_PUMP_REPLACE         = 200219
ALARM_ASANTE_ALERT_BG_REMINDER          = 200220
ALARM_ASANTE_ALERT_BOLUS_REMINDER       = 200221
ALARM_ASANTE_ALERT_DAILYREMINDER        = 200222
ALARM_ASANTE_ALERT_BOLUS_STOPPED        = 200223
ALARM_ASANTE_ALERT_PUMP_REMINDER        = 200224
ALARM_ASANTE_INFO_PUMP_CONNECTED        = 200225
ALARM_ASANTE_INFO_PUMP_DETACHED         = 200226
ALARM_ASANTE_INFO_PUMP_STOPPED          = 200227
ALARM_ASANTE_INFO_MISSED_BASAL          = 200228
ALARM_ASANTE_INFO_MISSED_BOLUS          = 200229
ALARM_ASANTE_INFO_DELIVERY_DONE         = 200230
ALARM_ASANTE_INFO_FLASHLIGHT_LIMIT      = 200231
ALARM_ASANTE_INFO_BATTERY_CHARGED       = 200232
ALARM_ASANTE_INFO_THERAPIES_CANCELED    = 200233
ALARM_ASANTE_NO_ACTIVE_MSG              = 200234
ALARM_ASANTE_PUMP_BODY_NOT_ACCEPTED     = 200235
ALARM_ASANTE_DELIVERY_CANCELLED         = 200236
ALARM_ASANTE_BOLUS_REMINDER_CANCELLED   = 200237
ALARM_ASANTE_CARTRIDGE_MAY_HAVE_EXPIRED = 200238
ALARM_ASANTE_ASANTESYNC_INIT            = 200239

# Events - Not regarded as alarms by certain devices
DIASEND_EVENT_REFILL                    = 500000
DIASEND_EVENT_BASAL_PROGRAM_CHANGE      = 500001
DIASEND_EVENT_DELIVERY_SUSPENDED        = 500002
DIASEND_EVENT_DELIVERY_RESUMED          = 500003
DIASEND_EVENT_POWER_ON                  = 500004
DIASEND_EVENT_DATETIME_CHANGE           = 500005
DIASEND_EVENT_TEMP_BASAL_ON             = 500006
DIASEND_EVENT_TEMP_BASAL_OFF            = 500007
DIASEND_EVENT_BASAL_PROGRAM_EDITED      = 500008
DIASEND_EVENT_PUMP_SETTINGS_EDITED      = 500009
DIASEND_EVENT_GLOBAL_SETTINGS_EDITED    = 500010
DIASEND_EVENT_REMINDER_SETTINGS_EDITED  = 500011
DIASEND_EVENT_PUMP_ACTIVATED            = 500012

# Manufacturer specific constants
LIFESCAN_BG_VALUE_LOW_MMOL      = 1.1
LIFESCAN_BG_VALUE_HIGH_MMOL     = 33.3

