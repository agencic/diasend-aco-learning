# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re
import os
import string
from Generic import *

from Defines import *
from Header import *

import EventLog

import Config
exec "from %s import InsertListIntoDatabase" % Config.settings["database-module"]
exec "from %s import EndSyncIntoDatabase" % Config.settings["database-module"]
exec "from %s import InsertErrorLogIntoDatabase" % Config.settings["database-module"]
exec "from %s import InsertHistoryIntoDatabase" % Config.settings["database-module"]
exec "from %s import CheckSubscriptionLevel" % Config.settings["database-module"]
exec "from %s import CheckLatestOldTransmitterVersion" % Config.settings["database-module"]
exec "from %s import IsTerminalBlacklisted" % Config.settings["database-module"]
exec "from %s import GetTerminalAdmin" % Config.settings["database-module"]
exec "from %s import HasClinicUnitedSupport" % Config.settings["database-module"]
exec "from %s import IsClinicBelongingToAnimasMiddleAdmins" % Config.settings["database-module"]
exec "from %s import IsDeviceCoveredByRule" % Config.settings["database-module"]

from UserManagement import AuthenticateUser

from MeterAbbottFreestyle               import AnalyseAbbottFreestyle
from MeterAbbottFreestyle               import DetectAbbottFreestyle
from MeterAbbottFreestyleInsulinx       import AnalyseAbbottFreestyleInsulinx
from MeterAbbottFreestyleInsulinx       import DetectAbbottFreestyleInsulinx
from MeterAbbottPrecisionXceed          import AnalyseAbbottPrecisionXceed
from MeterAbbottPrecisionXceed          import DetectAbbottPrecisionXceed
from MeterBayerAscensiaContour          import AnalyseBayerAscensiaContour
from MeterBayerAscensiaContour          import DetectBayerAscensiaContour
from MeterBayerContourUSB               import AnalyseBayerContourUSB
from MeterBayerContourUSB               import DetectBayerContourUSB
from MeterLifescanOnetouchUltra         import AnalyseLifescanOnetouchUltra
from MeterLifescanOnetouchUltra         import DetectLifescanOnetouchUltra
from MeterLifescanOnetouchUltra2        import AnalyseLifescanOnetouchUltra2
from MeterLifescanOnetouchUltra2        import DetectLifescanOnetouchUltra2
from MeterLifescanOnetouchUltraEasy     import AnalyseLifescanOneTouchUltraEasy
from MeterLifescanOnetouchUltraEasy     import DetectLifescanOneTouchUltraEasy
from MeterRocheAccuchekAviva            import AnalyseRocheAccuchekAviva
from MeterRocheAccuchekAviva            import DetectRocheAccuchekAviva
from MeterRocheAccuchekCompact          import AnalyseRocheAccuchekCompact
from MeterRocheAccuchekCompact          import DetectRocheAccuchekCompact
from MeterRocheAccuchekMobileLcm        import AnalyseAccuchekMobileLcm
from MeterRocheAccuchekMobileLcm        import DetectAccuchekMobileLcm
from MeterLifescanOnetouchUltraSmart    import AnalyseLifescanOnetouchUltraSmart
from MeterLifescanOnetouchUltraSmart    import DetectLifescanOnetouchUltraSmart
from MeterLifescanOnetouchPing          import AnalyseLifescanOnetouchPing
from MeterLifescanOnetouchPing          import DetectLifescanOnetouchPing
from MeterMenariniVisio                 import AnalyseMenariniVisio
from MeterMenariniVisio                 import DetectMenariniVisio
from MeterMenariniGlucoMenLX            import AnalyseMenariniGlucoMenLX
from MeterMenariniGlucoMenLX            import DetectMenariniGlucoMenLX
from MeterMenariniGlucoMenAreoNFC       import AnalyseMenariniGlucoMenAreoNFC
from MeterMenariniGlucoMenAreoNFC       import DetectMenariniGlucoMenAreoNFC
from MeterAgaMatrixKeyNote              import DetectAgaMatrixKeyNote
from MeterAgaMatrixKeyNote              import AnalyseAgaMatrixKeyNote
from MeterArkrayXMeter                  import DetectArkrayXMeter
from MeterArkrayXMeter                  import AnalyseArkrayXMeter
from MeterHaemedicGlucosure             import DetectHaemedicGlucosure
from MeterHaemedicGlucosure             import AnalyseHaemedicGlucosure
from MeterHaemedicGlucosureUSB          import DetectHaemedicGlucosureUSB
from MeterHaemedicGlucosureUSB          import AnalyseHaemedicGlucosureUSB
from MeterEverymedX3                    import DetectEverymedX3
from MeterEverymedX3                    import AnalyseEverymedX3
from MeterAbbottNavigator               import DetectAbbottNavigator
from MeterAbbottNavigator               import AnalyseAbbottNavigator
from MeterWavesenseJazz                 import DetectWavesenseJazz
from MeterWavesenseJazz                 import AnalyseWavesenseJazz
from MeterTerumoFinetouch               import DetectTerumoFinetouch
from MeterTerumoFinetouch               import AnalyseTerumoFinetouch
from MeterIntuityOnQPogo                import DetectIntuityOnQPogo
from MeterIntuityOnQPogo                import AnalyseIntuityOnQPogo
from MeterOnCall                        import DetectOnCall
from MeterOnCall                        import AnalyseOnCall
from MeterTrueResult                    import DetectTrueResult
from MeterTrueResult                    import AnalyseTrueResult
from MeterDexcomSevenPlus               import DetectDexcomSevenPlus
from MeterDexcomSevenPlus               import AnalyseDexcomSevenPlus
from MeterDexcomG4                      import DetectDexcomG4
from MeterDexcomG4                      import AnalyseDexcomG4
from MeterWellionCalla                  import DetectWellionCalla
from MeterWellionCalla                  import AnalyseWellionCalla
from MeterLifescanOnetouchVerioPro      import DetectLifescanOneTouchVerioPro
from MeterLifescanOnetouchVerioPro      import AnalyseLifescanOneTouchVerioPro
from MeterDiscreetAndCareSens           import DetectDiscreetAndCareSens
from MeterDiscreetAndCareSens           import AnalyseDiscreetAndCareSens
from MeterNovaMaxLink                   import DetectNovaMaxLink
from MeterNovaMaxLink                   import AnalyseNovaMaxLink
from MeterNexus                         import DetectMeterNexus
from MeterNexus                         import AnalyseMeterNexus
from MeterMicrodotPlus                  import DetectMeterMicrodotPlus
from MeterMicrodotPlus                  import AnalyseMeterMicrodotPlus
from MeterMylifeUnio                    import DetectMeterMylifeUnio
from MeterMylifeUnio                    import AnalyseMeterMylifeUnio
from MeterMylifePura                    import DetectMeterMylifePura
from MeterMylifePura                    import AnalyseMeterMylifePura
from MeterRocheAccuChekActive           import DetectMeterAccuChekActive
from MeterRocheAccuChekActive           import AnalyseMeterAccuChekActive
from MeterProdigyAutoCode               import AnalyseMeterProdigyAutoCode
from MeterProdigyAutoCode               import DetectMeterProdigyAutoCode
from MeterProdigyAutoCode               import AnalyseMeterPalmdoc
from MeterProdigyAutoCode               import DetectMeterPalmdoc
from MeterInfopiaElement                import AnalyseMeterInfopiaElement
from MeterInfopiaElement                import DetectMeterInfopiaElement
from MeterOneTouchBGMDLL                import AnalyseMeterOneTouchBGMDLL
from MeterOneTouchBGMDLL                import DetectMeterOneTouchBGMDLL
from MeterPharmasupplyAdvocate          import AnalyseMeterPharmasupplyAdvocate
from MeterPharmasupplyAdvocate          import DetectMeterPharmasupplyAdvocate
from MeteriSensCareSensSN3              import AnalyseiSensCareSensSN3
from MeteriSensCareSensSN3              import DetectiSensCareSensSN3
from MeterSenseonicsTransmitter         import AnalyseMeterSenseonicsTransmitter
from MeterSenseonicsTransmitter         import DetectMeterSenseonicsTransmitter
from MeterSenseonicsApp         	import AnalyseMeterSenseonicsApp
from MeterSenseonicsApp         	import DetectMeterSenseonicsApp
from MeterDexcomCloud                   import AnalyseMeterDexcomCloud
from MeterDexcomCloud                   import DetectMeterDexcomCloud
from MeterAbbottCloud                   import AnalyseMeterAbbottCloud
from MeterAbbottCloud                   import DetectMeterAbbottCloud
from MeterGlucocardSM                   import AnalyseMeterGlucocardSM
from MeterGlucocardSM                   import DetectMeterGlucocardSM
from MeterBeurerGL50evo                 import AnalyseMeterBeurerGL50evo
from MeterBeurerGL50evo                 import DetectMeterBeurerGL50evo
from MeterKeyaSmart                     import AnalyseMeterKeyaSmart
from MeterKeyaSmart                     import DetectMeterKeyaSmart

from PumpAnimasIR                       import AnalyseAnimasIR
from PumpAnimasIR                       import DetectAnimasIR
from PumpDeltecCozmo                    import AnalyseDeltecCozmo
from PumpDeltecCozmo                    import DetectDeltecCozmo
from PumpInsuletOmniPod                 import AnalyseInsuletOmniPod
from PumpInsuletOmniPod                 import DetectInsuletOmniPod
from PumpNiproAmigo                     import AnalyseNiproAmigo
from PumpNiproAmigo                     import DetectNiproAmigo
from PumpDanaDiabecare                  import AnalyseDanaDiabecare
from PumpDanaDiabecare                  import DetectDanaDiabecare
from PumpAsante                         import AnalyseAsanteSnap
from PumpAsante                         import DetectAsanteSnap
from PumpTandemTslim                    import AnalyseTandemTslim
from PumpTandemTslim                    import DetectTandemTslim

from PenDiamescoPendiq                  import AnalyseDiamescoPendiq
from PenDiamescoPendiq                  import DetectDiamescoPendiq

from DeviceRocheSmartpix                import AnalyseRocheSmartpix
from DeviceRocheSmartpix                import DetectRocheSmartpix
from DeviceDiasendGeneric               import AnalyseDiasendGeneric
from DeviceDiasendGeneric               import DetectDiasendGeneric
from DeviceDiasendGenericXML            import AnalyseDiasendGenericXML
from DeviceDiasendGenericXML            import DetectDiasendGenericXML
from DeviceSMS                          import AnalyseSMS
from DeviceSMS                          import DetectSMS
from DevicePHDC                         import DetectPHDC
from DevicePHDC                         import AnalysePHDC
 
from FileImportAbbottCoPilot            import AnalyseAbbottCoPilot
from FileImportAbbottCoPilot            import DetectAbbottCoPilot
from FileImportAnimasEzManagerMax       import AnalyseAnimasEzManagerMax
from FileImportAnimasEzManagerMax       import DetectAnimasEzManagerMax
from FileImportLifescanOneTouchDiabetesManagement   import AnalyseLifescanOneTouchDiabetesManagement
from FileImportLifescanOneTouchDiabetesManagement   import DetectLifescanOneTouchDiabetesManagement
from FileImportDexcomXML                            import AnalyseDexcomXML
from FileImportDexcomXML                            import DetectDexcomXML
from FileImportCSVGlooko                            import AnalyseCSVGlooko
from FileImportCSVGlooko                            import DetectCSVGlooko
from FileImportCSViBGStar                           import AnalyseCSViBGStar
from FileImportCSViBGStar                           import DetectCSViBGStar

# List of supported meters and pumps
analyseAndDetectFunctions = ( 
    { "analyse":AnalyseRocheAccuchekCompact,        "detect":DetectRocheAccuchekCompact }, 
    { "analyse":AnalyseRocheAccuchekAviva,          "detect":DetectRocheAccuchekAviva }, 
    { "analyse":AnalyseLifescanOneTouchUltraEasy,   "detect":DetectLifescanOneTouchUltraEasy }, 
    { "analyse":AnalyseLifescanOnetouchUltraSmart,  "detect":DetectLifescanOnetouchUltraSmart }, 
    { "analyse":AnalyseLifescanOnetouchUltra2,      "detect":DetectLifescanOnetouchUltra2 }, 
    { "analyse":AnalyseLifescanOnetouchUltra,       "detect":DetectLifescanOnetouchUltra }, 
    { "analyse":AnalyseLifescanOnetouchPing,        "detect":DetectLifescanOnetouchPing }, 
    { "analyse":AnalyseLifescanOneTouchVerioPro,    "detect":DetectLifescanOneTouchVerioPro }, 
    { "analyse":AnalyseBayerAscensiaContour,        "detect":DetectBayerAscensiaContour }, 
    { "analyse":AnalyseBayerContourUSB,             "detect":DetectBayerContourUSB }, 
    { "analyse":AnalyseAbbottFreestyle,             "detect":DetectAbbottFreestyle }, 
    { "analyse":AnalyseAbbottPrecisionXceed,        "detect":DetectAbbottPrecisionXceed }, 
    { "analyse":AnalyseAnimasIR,                    "detect":DetectAnimasIR }, 
    { "analyse":AnalyseDeltecCozmo,                 "detect":DetectDeltecCozmo }, 
    { "analyse":AnalyseInsuletOmniPod,              "detect":DetectInsuletOmniPod }, 
    { "analyse":AnalyseNiproAmigo,                  "detect":DetectNiproAmigo }, 
    { "analyse":AnalyseDanaDiabecare,               "detect":DetectDanaDiabecare }, 
    { "analyse":AnalyseMenariniVisio,               "detect":DetectMenariniVisio }, 
    { "analyse":AnalyseMenariniGlucoMenLX,          "detect":DetectMenariniGlucoMenLX }, 
    { "analyse":AnalyseMenariniGlucoMenAreoNFC,     "detect":DetectMenariniGlucoMenAreoNFC },
    { "analyse":AnalyseDiasendGenericXML,           "detect":DetectDiasendGenericXML },
    { "analyse":AnalyseDiasendGeneric,              "detect":DetectDiasendGeneric }, 
    { "analyse":AnalyseAgaMatrixKeyNote,            "detect":DetectAgaMatrixKeyNote }, 
    { "analyse":AnalyseHaemedicGlucosureUSB,        "detect":DetectHaemedicGlucosureUSB }, 
    { "analyse":AnalyseHaemedicGlucosure,           "detect":DetectHaemedicGlucosure }, 
    { "analyse":AnalyseEverymedX3,                  "detect":DetectEverymedX3 }, 
    { "analyse":AnalyseAbbottNavigator,             "detect":DetectAbbottNavigator }, 
    { "analyse":AnalyseRocheSmartpix,               "detect":DetectRocheSmartpix }, 
    { "analyse":AnalyseSMS,                         "detect":DetectSMS }, 
    { "analyse":AnalyseAbbottCoPilot,               "detect":DetectAbbottCoPilot }, 
    { "analyse":AnalyseAnimasEzManagerMax,          "detect":DetectAnimasEzManagerMax }, 
    { "analyse":AnalyseLifescanOneTouchDiabetesManagement,          "detect":DetectLifescanOneTouchDiabetesManagement }, 
    { "analyse":AnalyseDexcomXML,                   "detect":DetectDexcomXML }, 
    { "analyse":AnalyseCSVGlooko,                   "detect":DetectCSVGlooko }, 
    { "analyse":AnalyseCSViBGStar,                  "detect":DetectCSViBGStar }, 
    { "analyse":AnalyseWavesenseJazz,               "detect":DetectWavesenseJazz }, 
    { "analyse":AnalyseTerumoFinetouch,             "detect":DetectTerumoFinetouch }, 
    { "analyse":AnalyseTrueResult,                  "detect":DetectTrueResult }, 
    { "analyse":AnalyseArkrayXMeter,                "detect":DetectArkrayXMeter }, 
    { "analyse":AnalyseIntuityOnQPogo,              "detect":DetectIntuityOnQPogo }, 
    { "analyse":AnalyseOnCall,                      "detect":DetectOnCall }, 
    { "analyse":AnalyseDexcomSevenPlus,             "detect":DetectDexcomSevenPlus }, 
    { "analyse":AnalyseDexcomG4,                    "detect":DetectDexcomG4 }, 
    { "analyse":AnalyseWellionCalla,                "detect":DetectWellionCalla }, 
    { "analyse":AnalyseDiscreetAndCareSens,         "detect":DetectDiscreetAndCareSens }, 
    { "analyse":AnalyseAccuchekMobileLcm,           "detect":DetectAccuchekMobileLcm }, 
    { "analyse":AnalyseNovaMaxLink,                 "detect":DetectNovaMaxLink }, 
    { "analyse":AnalyseDiamescoPendiq,              "detect":DetectDiamescoPendiq }, 
    { "analyse":AnalyseAbbottFreestyleInsulinx,     "detect":DetectAbbottFreestyleInsulinx }, 
    { "analyse":AnalyseMeterNexus,                  "detect":DetectMeterNexus}, 
    { "analyse":AnalyseMeterMicrodotPlus,           "detect":DetectMeterMicrodotPlus}, 
    { "analyse":AnalyseMeterMylifeUnio,             "detect":DetectMeterMylifeUnio},
    { "analyse":AnalyseMeterMylifePura,             "detect":DetectMeterMylifePura},
    { "analyse":AnalyseMeterAccuChekActive,         "detect":DetectMeterAccuChekActive},
    { "analyse":AnalyseAsanteSnap,                  "detect":DetectAsanteSnap},
    { "analyse":AnalyseMeterProdigyAutoCode,        "detect":DetectMeterProdigyAutoCode},
    { "analyse":AnalyseMeterPalmdoc,                "detect":DetectMeterPalmdoc},
    { "analyse":AnalyseMeterInfopiaElement,         "detect":DetectMeterInfopiaElement},
    { "analyse":AnalyseTandemTslim,                 "detect":DetectTandemTslim},
    { "analyse":AnalysePHDC,                        "detect":DetectPHDC},
    { "analyse":AnalyseMeterOneTouchBGMDLL,         "detect":DetectMeterOneTouchBGMDLL},
    { "analyse":AnalyseMeterPharmasupplyAdvocate,   "detect":DetectMeterPharmasupplyAdvocate},
    { "analyse":AnalyseiSensCareSensSN3,            "detect":DetectiSensCareSensSN3},
    { "analyse":AnalyseMeterSenseonicsTransmitter,  "detect":DetectMeterSenseonicsTransmitter},
    { "analyse":AnalyseMeterSenseonicsApp,          "detect":DetectMeterSenseonicsApp},
    { "analyse":AnalyseMeterDexcomCloud,            "detect":DetectMeterDexcomCloud},
    { "analyse":AnalyseMeterAbbottCloud,            "detect":DetectMeterAbbottCloud},
    { "analyse":AnalyseMeterBeurerGL50evo,          "detect":DetectMeterBeurerGL50evo},
    { "analyse":AnalyseMeterKeyaSmart,              "detect":DetectMeterKeyaSmart},
    { "analyse":AnalyseMeterGlucocardSM,            "detect":DetectMeterGlucocardSM})

# -----------------------------------------------------------------------------
# Function definitions
# -----------------------------------------------------------------------------

def CheckForUpdate(sw_ver_str):
    """Check if the terminal needs a software update - if so return True, else False"""
    # Expect !HDR!SWID!A06102 XXX where XXX==version (e.g. P1b)
    # Latest version should be found in the file firmware/latest.txt.

    latest_ver_str = CheckLatestOldTransmitterVersion()
    if not latest_ver_str:
        EventLog.EventLogError('CheckForUpdate: Failed to check latest version')
        return False
    
    # check versions
    if sw_ver_str.upper() in latest_ver_str.upper():
        EventLog.EventLogInfo('CheckForUpdate: This Transmitter has latest software')
        return False
    else:
        EventLog.EventLogInfo('CheckForUpdate: Will order update from ver: ' + sw_ver_str + ' to: ' + latest_ver_str)
        return True

def AnalyseIncomingHeader( header ):
    """
    Function that checks an incoming header.

    Right now only the version is checked, and if it is too old !LOAD is set as response to 
    the complete transfer. If the Transmitter software the resonse to the header is set to !LOAD
    """
    res = {}
    # default is to ACK the header, if not LOAD or NAK it!
    res["rsp_header"] = RSP_OK

    bl_numeric_ver = header.getBootloaderNumericVersion()
    
    # Can not load if the boot loader version is unknown
    if bl_numeric_ver == None:
        return res

    # Bootloader R2a and earlier does not implement !LOAD correctly
    if bl_numeric_ver > 200:
        # The boot loader seem to support !LOAD

        sw_ver = header.GetSoftwareVersion()
        prot_ver = header.getProtocolVersion()
    
        if sw_ver and prot_ver:
            if CheckForUpdate( sw_ver ):
            
                if sw_ver == 'trunk':
                    # This is a development version, do not send !LOAD
                    EventLog.EventLogInfo('AnalyseIncomingHeader: Development version detected, will not issue ' + RSP_LOAD)
                    return res

                # The software on the Transmitter is not the latest version
                if prot_ver["major"] > 2 or (prot_ver["major"] == 2 and prot_ver["minor"] > 0):
                    # Only prot version 2.1 and upwards !LOAD as response to the header
                    res["rsp_header"] = RSP_LOAD
                else:
                    res["rsp_transfer"] = RSP_LOAD            
        else:
            # Failed to extract software version or protocol version, send !LOAD
            if not sw_ver:
                EventLog.EventLogError('AnalyseIncomingHeader: Failed to extract software version')
            if not prot_ver:
                EventLog.EventLogError('AnalyseIncomingHeader: Failed to extract protocol version')
            res["rsp_header"] = RSP_LOAD

    # Sanity check on the header
    if res.has_key("rsp_header") and res["rsp_header"] == RSP_LOAD:
        pass
    elif not header.areMandatoryTagsSet():
        resList = { \
            "results" : [{ "error_code":ERROR_CODE_HEADER_NOT_COMPLETE, "fault_data":header.getBuffer() }], \
            "header" : header.toDictionary() }
        SetLastAnalyseResult( resList )
        InsertErrorLogIntoDatabase( resList )
        InsertHistoryIntoDatabase( str(header.getTerminalSerial()), 'unknown', 'Failed : Incomplete header %s' % header.getConnectionInfo() )
        
        # Code above did not force a !LOAD, just !NAK, since header is corrupt
        return { "rsp_header": RSP_NAK }

    return res

def HandleAnalysedData(device_type, header, resList):
    """
    Takes care of the result list returned by an analyser
    """
   
    resList["header"].update(header.toDictionary())

    # prepare some variables for the history entry
    meter_serial = resList["header"]["meter_serial"]
    if not meter_serial:
        meter_serial = "0"

    # check if the analyse reported ok or nok
    if ResultIsError( resList["results"] ):
        SetLastAnalyseResult( resList )
        InsertErrorLogIntoDatabase( resList )
        InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, \
            'Failed : Analyse error (see errorlog for details) %s' % header.getConnectionInfo() )
        return False

    # If the transmitter uses protocol >=3.0 check that the SWID is valid
    protVer = header.getProtocolVersion()
    if (not protVer) or ((protVer["major"] >= 3) and not (header.getSoftwareID() in valid_transmitters)):
        print "Not a valid SWID."
        resList = { \
            "results" : [{ "error_code":ERROR_CODE_INVALID_TERMINAL_SWID }], \
            "header" : header.toDictionary() }
        SetLastAnalyseResult( resList )
        InsertErrorLogIntoDatabase( resList )
        # Meter serial is not know at this point
        InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, \
            'Failed : SWID was invalid or missing %s' % header.getConnectionInfo() )
        return False
         

    # Check that the terminal is not blacklisted
    if IsTerminalBlacklisted(header.getTerminalSerial()):
        # Terminal was blacklisted, insert an error
        resList = { \
            "results" : [{ "error_code":ERROR_CODE_INVALID_TERMINAL_SERNO }], \
            "header" : header.toDictionary() }
        SetLastAnalyseResult( resList )
        InsertErrorLogIntoDatabase( resList )
        # Meter serial is not know at this point
        InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, \
            'Failed : Terminal was blacklisted %s' % header.getConnectionInfo() )
        return False
        
    # Okay everything fine so far, if the detect function did not return any device type
    # Check so the analyser did and the customer is allowed to use that kind of device
    if device_type == DEVICE_TOO_EARLY:
        device_type = ResultDeviceClass(resList)

        # Check if the subscription allows the data to be inserted into the database
        if not header.isTerminalSerialUnknown() and not CheckSubscriptionLevel(header.getTerminalSerial(), device_type):
            # Subscription level was not OK, insert an error
            resList = { \
                "results" : [{ "error_code":ERROR_CODE_SUBSCRIPTION }], \
                "header" : header.toDictionary() }
            SetLastAnalyseResult( resList )
            InsertErrorLogIntoDatabase( resList )
            # Meter serial is not know at this point
            InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, \
                'Failed : Subscription level did not allow this transaction %s' % header.getConnectionInfo() )
            return False

    
    # If data is from a hardware Transmitter we should follow the rules as follows:
    # if (terminal class == Transmitter)
    #   if (clinic belongs to animas_middle_admins)
    #     if (device is in Rule B)
    #       reject
    
    rule_reject_reason = None
    if not header.isSoftwareTransmitter():
        clinic = GetTerminalAdmin(header.getTerminalSerial())
        meter_type = resList["header"]["meter_type"]
        if clinic and IsClinicBelongingToAnimasMiddleAdmins(clinic):
            if IsDeviceCoveredByRule(meter_type, "B"):
                # Animas clinic trying to upload devices according to rule B - reject!
                rule_reject_reason = "B"

    # If the transfer should be rejected, send result and store in database
    if rule_reject_reason:
        print "Transmitter device rejection: Rule ", rule_reject_reason
        resList = { \
            "results" : [{ "error_code":ERROR_CODE_SUBSCRIPTION_DEVICE_RULE }], \
            "header" : header.toDictionary() }
        SetLastAnalyseResult( resList )
        InsertErrorLogIntoDatabase( resList )
        InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, \
            'Failed : Subscription level (non-United) did not allow this transfer (%s), rejected Rule %s %s' % (meter_type, rule_reject_reason, header.getConnectionInfo()) )
        return False
    
    # Variable option is needed by API module since it stores cookie data
    option = None
    # If this is data from a software transmitter we require a Diasend SOFT account. 
    if header.isSoftwareTransmitter():

        # meter_serial already extracted above. 
        terminal_serial = resList["header"]["terminal_serial"]
        if not terminal_serial:
            terminal_serial = "0"
        meter_type = resList["header"]["meter_type"]
        if not meter_type:
            meter_type = "0"

        error, option = AuthenticateUser(header, meter_serial, meter_type, device_type)
        print "AuthenticateUser()", error, option

        if error != ERROR_CODE_NONE:
            resList = { \
                "results" : [{ "error_code":error, "error_option":option}], \
                "header" : header.toDictionary() }
            SetLastAnalyseResult( resList )
            InsertErrorLogIntoDatabase( resList )
            # Write the database history entry, but ignore error codes which already has written their own entry 
            if error != ERROR_CODE_SUBSCRIPTION_DEVICE_RULE:
                InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, \
                    'Failed : Software Transmitter Error Code %d %s' % (error, header.getConnectionInfo()) )
            return False
                    
    # If the device is a pump, duplicates within the transaction are allowed, for instance duplicated bolus
    # needs to be inserted into the database. In the future shall probably all devices allow dups within
    # the transaction, it is possible to speed up database insertion in that case.
    try: 
        insert_result, error_code = InsertListIntoDatabase( resList, device_type, (device_type == DEVICE_PUMP), option, header.getClinicProperty())
        if insert_result:
            EndSyncIntoDatabase(header.getOS().partition(' ')[0], header.getOS().partition(' ')[2], header.GetSoftwareVersion(), header.getTerminalSerial(), header.isSoftwareTransmitter(), option)
    except Exception as e:
        if len(e.args) > 0 and e.args[0] == ERROR_CODE_USER_REQUIRED:
            print "AuthenticationError"
            resList = { \
                "results" : [{ "error_code":ERROR_CODE_USER_REQUIRED, "error_option":None}], \
                "header" : header.toDictionary() }
            SetLastAnalyseResult( resList )
            InsertErrorLogIntoDatabase( resList )
            return False
        else:
            raise e

    if insert_result:
        info = ""
        
        # Check if the device header has a software version which is not recommended
        if resList["header"].has_key(PARAM_TERMINAL_VERSION_RECOMMENDED):
            if not resList["header"][PARAM_TERMINAL_VERSION_RECOMMENDED]:
                # Not recommended software
                info = "This terminal is running sw version: %s, NOT recommended" % resList["header"][ELEM_TERMINAL_VERSION]
                resList = { \
                    "results" : [{ "error_code":ERROR_CODE_TOO_OLD_SW_VER }], \
                    "header" : header.toDictionary() }
                SetLastAnalyseResult( resList )
                InsertErrorLogIntoDatabase( resList )
                InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, 'Failed : %s' % info )
                return False
        
        InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, 'Ok : %s' % info )
        return True
    else:
        resList["results"] = [ { "error_code":error_code, "fault_data":resList["results"] } ]
        SetLastAnalyseResult( resList )
        InsertErrorLogIntoDatabase( resList )
        InsertHistoryIntoDatabase( header.getTerminalSerial(), meter_serial, 'Failed : Unable to insert into database' )
        return False
        

def AnalyseIncomingData(inData, sizeOfIncomingData, header):
    """
    Analyse incoming data. Result is :

    * Stored in mysql database (if and only if new values have been reported;
      old values are not reentered)
    * Stored in error table in mysql database (if there is a fault in the
      transferred data; e.g. checksum error)
    """
    foundMeter = False
    result = True
   
    # Get the serial number of the terminal, it's needed to device which database 
    # that shall have the info
    terminal_serial = header.getTerminalSerial()


    # Detect all possible meters
    for functions in analyseAndDetectFunctions:

        # Try to detect meter, the function returns type of device
        device_type = functions["detect"]( header.getDeviceType() )

        if device_type:

            foundMeter = True
            
            if (device_type != DEVICE_TOO_EARLY) and not (header.isSoftwareTransmitter() and header.isTerminalSerialUnknown()):
                # Check if the subscription allows the data to be inserted into the database
                if not CheckSubscriptionLevel(header.getTerminalSerial(), device_type):
                    # Subscription level was not OK, insert an error
                    resList = { \
                        "results" : [{ "error_code":ERROR_CODE_SUBSCRIPTION }], \
                        "header" : header.toDictionary() }
                    SetLastAnalyseResult( resList )
                    InsertErrorLogIntoDatabase( resList )
                    # Meter serial is not know at this point
                    InsertHistoryIntoDatabase( header.getTerminalSerial(), "0", 'Failed : Subscription level did not allow this transaction %s' % header.getConnectionInfo() )
                    result = False
                    break
            
            # analyse incoming data
            resList = functions["analyse"]( inData )
            for res in resList:
                if result:
                    result = HandleAnalysedData(device_type, header, res)
            break

    if not foundMeter:
        resList = { \
            "results" : [{ "error_code":ERROR_CODE_UNKNOWN_METER, "fault_data":header.getBuffer() }], \
            "header" : header.toDictionary() }
        InsertErrorLogIntoDatabase( resList )
        InsertHistoryIntoDatabase( header.getTerminalSerial(), 'unknown', 'Failed : Unknown device %s' % header.getConnectionInfo() )
        result = False

    return result

# Error code from analyse (to be sent to a software transmitter). 
last_analyse_error_code = ERROR_CODE_GENERIC
last_analyse_error_option = None

def SetLastAnalyseResult(resList):
    """
    Set error code returned to the software transmitter. No point in returning to much 
    information about the implementation of the backend. 
    """
    global last_analyse_error_code
    global last_analyse_error_option

    if "error_code" in resList["results"][0]: 
        last_analyse_error_code = resList["results"][0]["error_code"]
    else:
        last_analyse_error_code = ERROR_CODE_GENERIC

    if "error_option" in resList["results"][0]:
        last_analyse_error_option = resList["results"][0]["error_option"]
    else:
        last_analyse_error_option = None
    
    print "SET LAST ERROR  %d" % (resList["results"][0]["error_code"])

def GetLastAnalyseResult():

    global last_analyse_error_code
    global last_analyse_error_option

    print "GET LAST ERROR %d" % (last_analyse_error_code), last_analyse_error_option

    code = last_analyse_error_code
    option = last_analyse_error_option

    # Reset not to repeat old errors
    last_analyse_error_code=ERROR_CODE_GENERIC
    last_analyse_error_option=None

    return code, option

def PrintData(data):
    print data
        
if __name__ == '__main__':
    l = []
    l.append('!HDR!DIASEND')
    l.append('!HDR!SWID!A06102 P1b')
    l.append('!HDR!METERTYPE!BayerAscensiaContour')
    l.append('!HDR!METERDATA')
    l.append('!HDR!SERNO!S6019999999991')
    l.append('')
    
    l = '!HDR!DIASEND\r\n!HDR!SWID!A06102 R3a\r\n!HDR!SERNO!S6019999999991\r\n'
    AnalyseIncomingData(l,1000,PrintData)
   

