# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE Accu-Chek Active USB
#
from pkgutil import get_data
import string

from Generic import *
from Defines import *
import Buffer

# Global variable holding extracted serial number. NEVER used in the actual extraction; 
# only used for error reporting. 
extracted_serial_number = 'Unknown'

class PHDC:

    AARQ_CHOSEN = 0xE200
    AARE_CHOSEN = 0xE300
    RLRQ_CHOSEN = 0xE400
    RLRE_CHOSEN = 0xE500
    ABRT_CHOSEN = 0xE600
    PRST_CHOSEN = 0xE700

    ROIV_CMIP_EVENT_REPORT_CHOSEN           = 0x0100
    ROIV_CMIP_CONFIRMED_EVENT_REPORT_CHOSEN = 0x0101
    ROIV_CMIP_GET_CHOSEN                    = 0x0103
    ROIV_CMIP_SET_CHOSEN                    = 0x0104
    ROIV_CMIP_CONFIRMED_SET_CHOSEN          = 0x0105
    ROIV_CMIP_ACTION_CHOSEN                 = 0x0106
    ROIV_CMIP_CONFIRMED_ACTION_CHOSEN       = 0x0107
    RORS_CMIP_CONFIRMED_EVENT_REPORT_CHOSEN = 0x0201
    RORS_CMIP_GET_CHOSEN                    = 0x0203
    RORS_CMIP_CONFIRMED_SET_CHOSEN          = 0x0205
    RORS_CMIP_CONFIRMED_ACTION_CHOSEN       = 0x0207
    ROER_CHOSEN                             = 0x0300
    RORJ_CHOSEN                             = 0x0400    

    MDC_NOTI_CONFIG = 0x0D1C
    MDC_ACT_SEG_GET_INFO = 0x0C0D
    MDC_ACT_SEG_TRIG_XFER = 0x0C1C
    MDC_NOTI_SEGMENT_DATA = 0x0D21
    MDC_MOC_VMO_PMSTORE = 0x003d
    MDC_ATTR_PM_SEG_MAP = 0x0A4E
    MDC_ATTR_ID_MODEL = 0x0928
    MDC_ATTR_ID_PROD_SPECN = 0x092D

    MDC_CONC_GLU_GEN = 28948
    MDC_CONC_GLU_CAPILLARY_WHOLEBLOOD = 29112
    MDC_CONC_GLU_CAPILLARY_PLASMA = 29116
    MDC_CONC_GLU_VENOUS_WHOLEBLOOD = 29120
    MDC_CONC_GLU_VENOUS_PLASMA = 29124
    MDC_CONC_GLU_ARTERIAL_WHOLEBLOOD = 29128
    MDC_CONC_GLU_ARTERIAL_PLASMA = 29132
    MDC_CONC_GLU_UNDETERMINED_WHOLEBLOOD = 29292
    MDC_CONC_GLU_UNDETERMINED_PLASMA = 29296
    MDC_CONC_GLU_CONTROL = 29136
    MDC_CONC_GLU_ISF = 29140
    MDC_CONC_HBA1C = 29148

    MDC_GLU_METER_DEV_STATUS = 29144
    MDC_CTXT_GLU_EXERCISE = 29152
    MDC_CTXT_GLU_CARB = 29156
    MDC_CTXT_GLU_CARB_BREAKFAST = 29160
    MDC_CTXT_GLU_CARB_LUNCH = 29164
    MDC_CTXT_GLU_CARB_DINNER = 29168
    MDC_CTXT_GLU_CARB_SNACK = 29172
    MDC_CTXT_GLU_CARB_DRINK = 29176
    MDC_CTXT_GLU_CARB_SUPPER = 29180
    MDC_CTXT_GLU_CARB_BRUNCH = 29184
    MDC_CTXT_MEDICATION = 29188
    MDC_CTXT_MEDICATION_RAPIDACTING = 29192
    MDC_CTXT_MEDICATION_SHORTACTING = 29196
    MDC_CTXT_MEDICATION_INTERMEDIATEACTING = 29200
    MDC_CTXT_MEDICATION_LONGACTING = 29204

    MDC_CTXT_GLU_HEALTH_MAJOR = 29220
    MDC_CTXT_GLU_HEALTH_MENSES = 29224
    MDC_CTXT_GLU_HEALTH_STRESS = 29228
    MDC_CTXT_GLU_HEALTH_NONE = 29232
    MDC_CTXT_GLU_SAMPLELOCATION = 29236
    MDC_CTXT_GLU_SAMPLELOCATION_FINGER = 29240
    MDC_CTXT_GLU_SAMPLELOCATION_AST = 29244
    MDC_CTXT_GLU_SAMPLELOCATION_EARLOBE = 29248
    MDC_CTXT_GLU_SAMPLELOCATION_CTRLSOLUTION = 29252
    MDC_CTXT_GLU_MEAL = 29256
    MDC_CTXT_GLU_MEAL_PREPRANDIAL = 29260
    MDC_CTXT_GLU_MEAL_POSTPRANDIAL = 29264
    MDC_CTXT_GLU_TESTER = 29276
    MDC_CTXT_GLU_TESTER_SELF = 29280
    MDC_CTXT_GLU_TESTER_HCP = 29284
    MDC_CTXT_GLU_TESTER_LAB = 29288

    GLU_METER_DEV_STATUS_DEVICE_BATTERY_LOW              = 2**15    # BITS-16 (0)
    GLU_METER_DEV_STATUS_SENSOR_MALFUNCTION              = 2**14    # BITS-16 (1)
    GLU_METER_DEV_STATUS_SENSOR_SAMPLE_SIZE_INSUFFICIENT = 2**13    # BITS-16 (2)
    GLU_METER_DEV_STATUS_SENSOR_STRIP_INSERTION          = 2**12    # BITS-16 (3)
    GLU_METER_DEV_STATUS_SENSOR_STRIP_TYPE_INCORRECT     = 2**11    # BITS-16 (4)
    GLU_METER_DEV_STATUS_SENSOR_RESULT_TOO_HIGH          = 2**10    # BITS-16 (5)
    GLU_METER_DEV_STATUS_SENSOR_RESULT_TOO_LOW           = 2**9     # BITS-16 (6)
    GLU_METER_DEV_STATUS_SENSOR_TEMP_TOO_HIGH            = 2**8     # BITS-16 (7)
    GLU_METER_DEV_STATUS_SENSOR_TEMP_TOO_LOW             = 2**7     # BITS-16 (8)
    GLU_METER_DEV_STATUS_SENSOR_READ_INTERRUPT           = 2**6     # BITS-16 (9)
    GLU_METER_DEV_STATUS_DEVICE_GEN_FAULT                = 2**5     # BITS-16 (10)


def parse_segment_info(buffer):
    '''
    Extract PM segment information. This is necessary since this APDU contains the
    segment type and the actual data is located in another APDU.
    '''

    segment_flags = {}
    (apdu, apdu_length, octet_length, invoke_id, message_id, message_len, obj_handle, 
        action_type, action_type_len, segment_count, segment_len) = buffer.get_struct('>HHHHHHHHHHH')
    if action_type == PHDC.MDC_ACT_SEG_GET_INFO:
        for _ in range(0, segment_count):
            (segment_instance_no, attribute_list_count, attribute_list_length) = buffer.get_struct('>HHH')
            for attribute_index in range(attribute_list_count):
                (attribute_id, attribute_len) = buffer.get_struct('>HH')
                if attribute_id == PHDC.MDC_ATTR_PM_SEG_MAP:
                    # We are only interested in the header - let's extract it and 
                    # consume the rest.
                    (segm_entry_header, seq_data_1, seq_data_2, 
                        seq_data_3, class_id, metric_type) = buffer.get_struct('>HHHHHH')
                    buffer.consume(attribute_len - 12) # 12 = length of already extracted header.
                    segment_flags[segment_instance_no] = metric_type
                else:
                    buffer.consume(attribute_len)
    else:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unexpected action type %x' % (action_type))

    return segment_flags


def extract_data(buffer, segment_flags, values_map):
    '''
    Time to extract data from an APDU with MDC_NOTI_SEGMENT_DATA content.
    '''
    (event_info_len, segment_instance, segm_evt_entry_idx, segm_evt_entry_count, 
        segm_evt_status, segm_data_evt_entries_len) = buffer.get_struct('>HHIIHH')
    segment_type = segment_flags[segment_instance]
    for _ in range(0, segm_evt_entry_count):
        (timestamp, value) = [hex(x)[2:] if idx is 0 else x for idx, x in enumerate(buffer.get_struct('>qH'))]

        if timestamp in values_map:
            result = values_map[timestamp]
        else:
            result = {
                ELEM_VAL_TYPE: VALUE_TYPE_GLUCOSE,
                ELEM_TIMESTAMP: datetime(
                    int(timestamp[0:4]),
                    int(timestamp[4:6]),
                    int(timestamp[6:8]),
                    int(timestamp[8:10]),
                    int(timestamp[10:12])), 
                ELEM_FLAG_LIST: [], 
                ELEM_VAL: None}
            values_map[timestamp] = result

        if segment_type == PHDC.MDC_CONC_GLU_UNDETERMINED_PLASMA:
            if result[ELEM_VAL] is not None:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Multiple glucose values with identical timestamp %s' % result[ELEM_TIMESTAMP])
            result[ELEM_VAL] = value * 18.0/VAL_FACTOR_CONV_MMOL_TO_MGDL
        elif segment_type == PHDC.MDC_CONC_GLU_CONTROL:
            result[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL)
        elif segment_type == PHDC.MDC_GLU_METER_DEV_STATUS:

            bits_to_flags = (
                {'bit': PHDC.GLU_METER_DEV_STATUS_DEVICE_BATTERY_LOW,              'flag': FLAG_LOW_BATTERY},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_MALFUNCTION,              'flag': FLAG_RESULT_ERRONEOUS},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_SAMPLE_SIZE_INSUFFICIENT, 'flag': FLAG_RESULT_ERRONEOUS},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_STRIP_INSERTION,          'flag': FLAG_STRIP_WARNING},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_STRIP_TYPE_INCORRECT,     'flag': FLAG_RESULT_ERRONEOUS},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_RESULT_TOO_HIGH,          'flag': FLAG_RESULT_HIGH},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_RESULT_TOO_LOW,           'flag': FLAG_RESULT_LOW},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_TEMP_TOO_HIGH,            'flag': FLAG_RESULT_HIGH_TEMP},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_TEMP_TOO_LOW,             'flag': FLAG_RESULT_LOW_TEMP},
                {'bit': PHDC.GLU_METER_DEV_STATUS_SENSOR_READ_INTERRUPT,           'flag': FLAG_RESULT_ERRONEOUS},
                {'bit': PHDC.GLU_METER_DEV_STATUS_DEVICE_GEN_FAULT,                'flag': FLAG_RESULT_ERRONEOUS})

            for bit_to_flag in bits_to_flags:
                if value & bit_to_flag['bit']:
                    result[ELEM_FLAG_LIST].append(bit_to_flag['flag'])

        elif segment_type == PHDC.MDC_CTXT_GLU_MEAL and value == PHDC.MDC_CTXT_GLU_MEAL_PREPRANDIAL:
            result[ELEM_FLAG_LIST].append(FLAG_BEFORE_MEAL)
        elif segment_type == PHDC.MDC_CTXT_GLU_MEAL and value == PHDC.MDC_CTXT_GLU_MEAL_POSTPRANDIAL:
            result[ELEM_FLAG_LIST].append(FLAG_AFTER_MEAL)


def amount_records(buffer, segment_flags):
    '''
    Time to loop through all the segments (each in a separate APDU). Only APDU's with 
    data is extracted - all other APDUs are ignored.
    '''
    # First we assemble our data from the PHCD blob received from the meter by using maps:
    values_map = {}

    while buffer.nr_bytes_left() > 0:

        (apdu, apdu_length, octet_length, invoke_id, message_id, 
            message_len, obj_handle) = buffer.get_struct('>HHHHHHH')

        if apdu == PHDC.PRST_CHOSEN and message_id == PHDC.ROIV_CMIP_CONFIRMED_EVENT_REPORT_CHOSEN:
            (event_time, event_type) = buffer.get_struct('>IH')
            if event_type == PHDC.MDC_NOTI_SEGMENT_DATA:
                extract_data(buffer, segment_flags, values_map)
            else:
                buffer.consume(apdu_length - 16) # 16 = length of already extracted header.                
        else:
            buffer.consume(apdu_length - 10) # 10 = length of already extracted header.

    values_list = values_map.values()
    for value in values_list:
        if value[ELEM_VAL] == None:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Found flags without glucose value')
    values_list.append({ELEM_NR_RESULTS: len(values_list)})
    return values_list


def extract_model_and_serial(buffer):
    '''
    Walk the MDS structure looking for model name and serial number. 
    '''

    result = []

    (apdu, apdu_len, oct_str_len, invoke_id, msg_id, msg_len, obj_handle, 
        attr_list_count, attr_list_len) = buffer.get_struct('>HHHHHHHHH')
    
    for attr_index in range(attr_list_count):
        (attr_id, attr_len) = buffer.get_struct('>HH')

        if attr_id == PHDC.MDC_ATTR_ID_MODEL:
            octet_str_len = buffer.get_struct('>H')[0]
            model_name    = buffer.get_struct('%ds' % (octet_str_len))[0]
            octet_str_len = buffer.get_struct('>H')[0]
            model_number  = buffer.get_struct('%ds' % (octet_str_len))[0]
            
            # In case of Roche, the name includes non ascii characters
            model_name = filter(lambda x: x in string.printable, model_name) + filter(lambda x: x in string.printable, model_number)

            if model_name == 'Roche2102':
                result.append({ELEM_DEVICE_MODEL: 'Accu-Chek Active USB'})
            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unknown model %s' % (model_name))

        elif attr_id == PHDC.MDC_ATTR_ID_PROD_SPECN:
            (prod_spec_count, prod_spec_len) = buffer.get_struct('>HH')
            for prod_spec_index in range(prod_spec_count):
                (spec_type, component_id, octet_str_len) = buffer.get_struct('>HHH')
                octet_str = buffer.get_struct('%ds' % (octet_str_len))[0]
                if spec_type == 1: # ProductSpec 1 = serial-number
                    remove_this = 'serial-number: '
                    serial = octet_str[len(remove_this):] if octet_str.startswith(remove_this) else octet_str   
                    result.append({ELEM_DEVICE_SERIAL: serial})
                    global extracted_serial_number
                    extracted_serial_number = serial

        else:
            buffer.consume(attr_len)

    return result


def get_generic_PHDC_structure(buffer, expected_apdu):
    '''
    All PHDC messages starts with an id (APDU) and a length. Extract a complete
    PHDC message and validate the APDU.
    '''
    buffer.set_mark()
    apdu = buffer.get_struct('>H')[0]
    if apdu != expected_apdu:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unexpected APDU')
    length = buffer.get_struct('>H')[0]
    buffer.consume(length)
    return buffer.get_slice_from_mark()

def parse_data(data):
    local_result = []
    buffer = Buffer.Buffer(data)

    association_request     = Buffer.Buffer(get_generic_PHDC_structure(buffer, PHDC.AARQ_CHOSEN))
    accepted_unknown_config = Buffer.Buffer(get_generic_PHDC_structure(buffer, PHDC.PRST_CHOSEN))
    mds_object_attributes   = Buffer.Buffer(get_generic_PHDC_structure(buffer, PHDC.PRST_CHOSEN))
    mds_segments            = Buffer.Buffer(get_generic_PHDC_structure(buffer, PHDC.PRST_CHOSEN))

    local_result.extend(extract_model_and_serial(mds_object_attributes))
    segment_info = parse_segment_info(mds_segments)
    local_result.extend(amount_records(buffer, segment_info))

    return local_result

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAccuChekActiveSerialRecord(record):
    return record if ELEM_DEVICE_SERIAL in record else {}

def EvalAccuChekActiveUnitRecord(line):
    return {ELEM_DEVICE_UNIT:"mg/dL"}

def EvalAccuChekActiveModelRecord(record):
    return record if ELEM_DEVICE_MODEL in record else {}

def EvalAccuChekActiveResultRecord(record):
    return record if ELEM_VAL in record else {}

def EvalAccuChekActiveChecksumRecord(line, record):
    return True

def EvalAccuChekActiveChecksumFile(inList):
    return True

def EvalAccuChekActiveNrResultsRecord(record):
    return record if ELEM_NR_RESULTS in record else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterAccuChekActive(inList):
    """
    Detect if data comes from a AccuChekActive.
    """

    return DetectDevice( 'AccuChekActiveUSB', inList, DEVICE_METER )

def AnalyseMeterAccuChekActive(inData):
    """
    Analyse data from AccuChekActive.
    """
    # Empty dictionary
    d = {"eval_device_model_record": EvalAccuChekActiveModelRecord,
         "eval_serial_record": EvalAccuChekActiveSerialRecord,
         "eval_unit": EvalAccuChekActiveUnitRecord,
         "eval_result_record": EvalAccuChekActiveResultRecord,
         "eval_checksum_record": EvalAccuChekActiveChecksumRecord,
         "eval_checksum_file": EvalAccuChekActiveChecksumFile,
         "eval_nr_results": EvalAccuChekActiveNrResultsRecord}

    global extracted_serial_number
    extracted_serial_number = 'Unknown'

    try:
        in_list = parse_data(inData)
        res_list = AnalyseGenericMeter( in_list, d )

    except Exception, e:

        import traceback
        traceback.print_exc()
        
        # The 'standard' way is to raise Exception with two parameters, diasend error code and 
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList('Accu-Chek Active USB', extracted_serial_number, e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList('Accu-Chek Active USB', extracted_serial_number, ERROR_CODE_PREPROCESSING_FAILED, 
                ''.join(map(str, e.args)))

        return error_response

    return res_list

if __name__ == "__main__":
    # test_file = open('test/testcases/test_data/RocheAccuChekActive/RocheAccuChekActive_260_mixed_flags.log')
    test_file = open('test/testcases/test_data/RocheAccuChekActive/RocheAccuChekActive_HI.log')
    testcase = test_file.read()
    test_file.close()

    results = AnalyseMeterAccuChekActive(testcase)
    print results[0]['header']
    for result in results[0]['results']:
        print result
