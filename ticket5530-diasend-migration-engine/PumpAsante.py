# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
#
#     ___                     __          _____ _   _____    ____ 
#    /   |  _________ _____  / /____     / ___// | / /   |  / __ \
#   / /| | / ___/ __ `/ __ \/ __/ _ \    \__ \/  |/ / /| | / /_/ /
#  / ___ |(__  ) /_/ / / / / /_/  __/   ___/ / /|  / ___ |/ ____/ 
# /_/  |_/____/\__,_/_/ /_/\__/\___/   /____/_/ |_/_/  |_/_/      
#
# -----------------------------------------------------------------------------

# @DEVICE Asante SNAP

import re
from datetime import datetime, timedelta
import bisect

import logging

import crcmod

from Generic import *
from Defines import *

from Record import BaseRecord, EnumRecord, EnumBitRecord
from Buffer import Buffer

ASANTE_TO_DIASEND_BOLUS_FACTOR = 500 
ASANTE_TO_DIASEND_BASAL_FACTOR = 50
ASANTE_TO_DIASEND_SMART_FACTOR = 100

DATE_TIME_1ST = 0  # record will be first in list (requirement of Generic.py)

class Checksum(object):
    @staticmethod
    def calculate(data):    
        return crcmod.predefined.mkCrcFun('crc-ccitt-false')(data)

def add_record_flag(record, flag):
    if not ELEM_FLAG_LIST in record:
        record[ELEM_FLAG_LIST] = []
    record[ELEM_FLAG_LIST].append(flag)

def add_record_value(record, value_type, value):
    if not ELEM_VALUE_LIST in record:
        record[ELEM_VALUE_LIST] = []
    record[ELEM_VALUE_LIST].append({ELEM_VAL_TYPE:value_type, ELEM_VAL:value})

def timedelta_total_seconds(td):
    '''
    datetime.timedelta.total_seconds only appeared in python 2.7. 
    '''
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

# --- enums

class EnumBolusTypes(EnumRecord):
    format = ('B',)
    enums = {
        'now'      : 0,
        'timed'    : 1,
        'combo'    : 2,
        'undefined': 3
    }

class EnumCompletionCode(EnumRecord):
    format = ('B',)
    enums = {
        'normal'       : 0, 
        'alarm'        : 1, 
        'user_stop'    : 2, 
        'detached'     : 3, 
        'prime'        : 4, 
        'in_progress'  : 5, 
        'cleared'      : 6, 
        'sys_reset'    : 7, 
        'batt_not_safe': 8, 
        'undefined'    : 9
    }
    map_enum_to_diasend = {
        'normal'       : FLAG_ASANTE_COMPLETION_CODE_NORMAL,
        'alarm'        : FLAG_ASANTE_COMPLETION_CODE_ALARM,
        'user_stop'    : FLAG_ASANTE_COMPLETION_CODE_USER_STOP,
        'detached'     : FLAG_ASANTE_COMPLETION_CODE_DETACHED,
        'prime'        : FLAG_ASANTE_COMPLETION_CODE_PRIME,
        'in_progress'  : FLAG_ASANTE_COMPLETION_CODE_IN_PROGRESS,
        'cleared'      : FLAG_ASANTE_COMPLETION_CODE_CLEARED,
        'sys_reset'    : FLAG_ASANTE_COMPLETION_CODE_SYS_RESET,
        'batt_not_safe': FLAG_ASANTE_COMPLETION_CODE_BATT_NOT_SAFE,
        'undefined'    : FLAG_ASANTE_COMPLETION_CODE_UNDEFINED
    }

class EnumEventType(EnumRecord):
    format = ('B',)
    enums  = {
        'profile_event':0, 
        'temp_basal'   :1, 
        'pump_stopped' :2
    }

class EnumProfileEvent(EnumRecord):
    format = ('B',)
    enums  = {
        'profile_edited'  : 0,
        'profile_selected': 1,
        'max_position'    : 2
    }
    map_enum_to_diasend = {
        'profile_edited'  : DIASEND_EVENT_BASAL_PROGRAM_CHANGE,
        'profile_selected': DIASEND_EVENT_BASAL_PROGRAM_EDITED,
        'max_position'    : DIASEND_EVENT_BASAL_PROGRAM_EDITED
    }


class EnumMessagesOldFormat(EnumRecord):
    format = ('B',)
    enums  = {
        'alarm_dropped_pump'      : 0,
        'alarm_no_power'          : 1,
        'alarm_set_clock'         : 2,
        'alarm_auto_off'          : 3,
        'alarm_occlusion'         : 4,
        'alarm_cartrige_empty'    : 5,
        'alarm_adapter'           : 6,
        'alarm_pump_drive'        : 7,
        'alarm_wet_pump'          : 8,
        'alarm_reseat_pump'       : 9,
        'alarm_delivery_limit'    : 10,
        'alarm_pump_detach'       : 11,
        'alarm_choose_setting'    : 12,
        'alarm_vrsn_mismatch'     : 13,
        'alert_key'               : 14,
        'alert_power_very_low'    : 15,
        'alert_cartridge_very_low': 16,
        'alert_temp_basal_status' : 17,
        'alert_cartridge_low'     : 18,
        'alert_pump_replace'      : 19,
        'alert_bg_reminder'       : 20,
        'alert_bolus_reminder'    : 21,
        'alert_dailyreminder'     : 22,
        'alert_bolus_stopped'     : 23,
        'alert_pump_reminder'     : 24,
        'info_pump_connected'     : 25,
        'info_pump_detached'      : 26,
        'info_pump_stopped'       : 27,
        'info_missed_basal'       : 28,
        'info_missed_bolus'       : 29,
        'info_delivery_done'      : 30,
        'info_flashlight_llimit'  : 31,
        'info_battery_charged'    : 32,
        'info_therapies_canceled' : 33,
        'no_active_msg'           : 34
    }
    map_enum_to_diasend = {
        'alarm_dropped_pump'      : ALARM_ASANTE_DROPPED_PUMP,
        'alarm_no_power'          : ALARM_POWER_OFF,
        'alarm_set_clock'         : ALARM_DATE_CHANGED,
        'alarm_auto_off'          : ALARM_ASANTE_AUTO_OFF,
        'alarm_occlusion'         : ALARM_OCCLUSION,
        'alarm_cartrige_empty'    : ALARM_CARTRIDGE_EMPTY,
        'alarm_adapter'           : ALARM_ASANTE_ADAPTER,
        'alarm_pump_drive'        : ALARM_ASANTE_PUMP_DRIVE,
        'alarm_wet_pump'          : ALARM_ASANTE_WET_PUMP,
        'alarm_reseat_pump'       : ALARM_ASANTE_RESEAT_PUMP,
        'alarm_delivery_limit'    : ALARM_ASANTE_DELIVERY_LIMIT,
        'alarm_pump_detach'       : ALARM_ASANTE_PUMP_DETACH,
        'alarm_choose_setting'    : ALARM_ASANTE_CHOOSE_SETTING,
        'alarm_vrsn_mismatch'     : ALARM_ASANTE_VRSN_MISMATCH,
        'alert_key'               : ALARM_ASANTE_ALERT_KEY,
        'alert_power_very_low'    : ALARM_ASANTE_ALERT_POWER_VERY_LOW,
        'alert_cartridge_very_low': ALARM_CARTRIDGE_VERY_LOW,
        'alert_temp_basal_status' : ALARM_ASANTE_ALERT_TEMP_BASAL_STATUS,
        'alert_cartridge_low'     : ALARM_ASANTE_ALERT_CARTRIDGE_LOW,
        'alert_pump_replace'      : ALARM_ASANTE_ALERT_PUMP_REPLACE,
        'alert_bg_reminder'       : ALARM_ASANTE_ALERT_BG_REMINDER,
        'alert_bolus_reminder'    : ALARM_ASANTE_ALERT_BOLUS_REMINDER,
        'alert_dailyreminder'     : ALARM_ASANTE_ALERT_DAILYREMINDER,
        'alert_bolus_stopped'     : ALARM_ASANTE_ALERT_BOLUS_STOPPED,
        'alert_pump_reminder'     : ALARM_ASANTE_ALERT_PUMP_REMINDER,
        'info_pump_connected'     : ALARM_ASANTE_INFO_PUMP_CONNECTED,
        'info_pump_detached'      : ALARM_ASANTE_INFO_PUMP_DETACHED,
        'info_pump_stopped'       : ALARM_ASANTE_INFO_PUMP_STOPPED,
        'info_missed_basal'       : ALARM_ASANTE_INFO_MISSED_BASAL,
        'info_missed_bolus'       : ALARM_ASANTE_INFO_MISSED_BOLUS,
        'info_delivery_done'      : ALARM_ASANTE_INFO_DELIVERY_DONE,
        'info_flashlight_llimit'  : ALARM_ASANTE_INFO_FLASHLIGHT_LIMIT,
        'info_battery_charged'    : ALARM_ASANTE_INFO_BATTERY_CHARGED,
        'info_therapies_canceled' : ALARM_ASANTE_INFO_THERAPIES_CANCELED,
        'no_active_msg'           : ALARM_ASANTE_NO_ACTIVE_MSG
    }

class EnumMessagesNewFormat(EnumRecord):
    format = ('B',)
    enums  = {
        'alarm_dropped_pump'             : 0,
        'alarm_no_power'                 : 1,
        'alarm_set_clock'                : 2,
        'alarm_auto_off'                 : 3,
        'alarm_occlusion'                : 4,
        'alarm_cartrige_empty'           : 5,
        'alarm_adapter'                  : 6,
        'alarm_pump_drive'               : 7,
        'alarm_wet_pump'                 : 8,
        'alarm_reseat_pump'              : 9,
        'alarm_delivery_limit'           : 10,
        'alarm_pump_detach'              : 11,
        'alarm_choose_setting'           : 12,
        'alarm_pump_body_not_accepted'   : 13, 
        'alarm_vrsn_mismatch'            : 14,
        'alert_key'                      : 15,
        'alert_power_very_low'           : 16,
        'alert_cartridge_very_low'       : 17,
        'alert_temp_basal_status'        : 18,
        'alert_cartridge_low'            : 19,
        'alert_pump_replace'             : 20,
        'alert_bg_reminder'              : 21,
        'alert_bolus_reminder'           : 22,
        'alert_bolus_stopped'            : 23,
        'alert_pump_reminder'            : 24,
        'info_pump_connected'            : 25,
        'info_pump_detached'             : 26,
        'info_pump_stopped'              : 27,
        'info_missed_basal'              : 28,
        'info_missed_bolus'              : 29,
        'info_delivery_done'             : 30,
        'info_delivery_cancelled'        : 31,
        'info_bolus_reminder_cancelled'  : 32, 
        'info_cartridge_may_have_expired': 33,
        'info_asantesync_init'           : 34,
        'no_active_msg'                  : 35
    }

    map_enum_to_diasend = {
        'alarm_dropped_pump'             : ALARM_ASANTE_DROPPED_PUMP,
        'alarm_no_power'                 : ALARM_POWER_OFF,
        'alarm_set_clock'                : ALARM_DATE_CHANGED,
        'alarm_auto_off'                 : ALARM_ASANTE_AUTO_OFF,
        'alarm_occlusion'                : ALARM_OCCLUSION,
        'alarm_cartrige_empty'           : ALARM_CARTRIDGE_EMPTY,
        'alarm_adapter'                  : ALARM_ASANTE_ADAPTER,
        'alarm_pump_drive'               : ALARM_ASANTE_PUMP_DRIVE,
        'alarm_wet_pump'                 : ALARM_ASANTE_WET_PUMP,
        'alarm_reseat_pump'              : ALARM_ASANTE_RESEAT_PUMP,
        'alarm_delivery_limit'           : ALARM_ASANTE_DELIVERY_LIMIT,
        'alarm_pump_detach'              : ALARM_ASANTE_PUMP_DETACH,
        'alarm_choose_setting'           : ALARM_ASANTE_CHOOSE_SETTING,
        'alarm_pump_body_not_accepted'   : ALARM_ASANTE_PUMP_BODY_NOT_ACCEPTED, 
        'alarm_vrsn_mismatch'            : ALARM_ASANTE_VRSN_MISMATCH,
        'alert_key'                      : ALARM_ASANTE_ALERT_KEY,
        'alert_power_very_low'           : ALARM_ASANTE_ALERT_POWER_VERY_LOW,
        'alert_cartridge_very_low'       : ALARM_CARTRIDGE_VERY_LOW,
        'alert_temp_basal_status'        : ALARM_ASANTE_ALERT_TEMP_BASAL_STATUS,
        'alert_cartridge_low'            : ALARM_ASANTE_ALERT_CARTRIDGE_LOW,
        'alert_pump_replace'             : ALARM_ASANTE_ALERT_PUMP_REPLACE,
        'alert_bg_reminder'              : ALARM_ASANTE_ALERT_BG_REMINDER,
        'alert_bolus_reminder'           : ALARM_ASANTE_ALERT_BOLUS_REMINDER,
        'alert_bolus_stopped'            : ALARM_ASANTE_ALERT_BOLUS_STOPPED,
        'alert_pump_reminder'            : ALARM_ASANTE_ALERT_PUMP_REMINDER,
        'info_pump_connected'            : ALARM_ASANTE_INFO_PUMP_CONNECTED,
        'info_pump_detached'             : ALARM_ASANTE_INFO_PUMP_DETACHED,
        'info_pump_stopped'              : ALARM_ASANTE_INFO_PUMP_STOPPED,
        'info_missed_basal'              : ALARM_ASANTE_INFO_MISSED_BASAL,
        'info_missed_bolus'              : ALARM_ASANTE_INFO_MISSED_BOLUS,
        'info_delivery_done'             : ALARM_ASANTE_INFO_DELIVERY_DONE,
        'info_delivery_cancelled'        : ALARM_ASANTE_DELIVERY_CANCELLED, 
        'info_bolus_reminder_cancelled'  : ALARM_ASANTE_BOLUS_REMINDER_CANCELLED, 
        'info_cartridge_may_have_expired': ALARM_ASANTE_CARTRIDGE_MAY_HAVE_EXPIRED, 
        'info_asantesync_init'           : ALARM_ASANTE_ASANTESYNC_INIT, 
        'no_active_msg'                  : ALARM_ASANTE_NO_ACTIVE_MSG
    }

class EnumClearConditionOldFormat(EnumRecord):
    format = ('B',)
    enums  = {
        'still_pending'          : 0,
        'user_acked'             : 1,
        'cradle_attached'        : 2,
        'pump_attached'          : 3,
        'pump_attached_good_batt': 4,
        'pump_allows_flashlight' : 5,
        'pump_detached'          : 6,
        'u_i_start_pump'         : 7,
        'no_power'               : 8,
        'very_low_cart'          : 9,
        'cartridge_empty'        : 10,
        'exited_prime'           : 11,
        'set_time_date'          : 12,
        'sufficient_power'       : 13,
        'temp_basal_ended'       : 14,
        'max_level'              : 15
    }
    map_enum_to_diasend = {
        'still_pending'          :FLAG_ASANTE_STILL_PENDING,
        'user_acked'             :FLAG_ASANTE_USER_ACKED,
        'cradle_attached'        :FLAG_ASANTE_CRADLE_ATTACHED,
        'pump_attached'          :FLAG_ASANTE_PUMP_ATTACHED,
        'pump_attached_good_batt':FLAG_ASANTE_PUMP_ATTACHED_GOOD_BATT,
        'pump_allows_flashlight' :FLAG_ASANTE_PUMP_ALLOWS_FLASHLIGHT,
        'pump_detached'          :FLAG_ASANTE_PUMP_DETACHED,
        'u_i_start_pump'         :FLAG_ASANTE_U_I_START_PUMP,
        'no_power'               :FLAG_ASANTE_NO_POWER,
        'very_low_cart'          :FLAG_ASANTE_VERY_LOW_CART,
        'cartridge_empty'        :FLAG_ASANTE_CARTRIDGE_EMPTY,
        'exited_prime'           :FLAG_ASANTE_EXITED_PRIME,
        'set_time_date'          :FLAG_ASANTE_SET_TIME_DATE,
        'sufficient_power'       :FLAG_ASANTE_SUFFICIENT_POWER,
        'temp_basal_ended'       :FLAG_ASANTE_TEMP_BASAL_ENDED,
        'max_level'              :FLAG_ASANTE_MAX_LEVEL 
    }

class EnumClearConditionNewFormat(EnumRecord):
    format = ('B',)
    enums  = {
        'still_pending'          : 0,
        'user_acked'             : 1,
        'cradle_attached'        : 2,
        'pump_attached'          : 3,
        'pump_attached_good_batt': 4,
        'pump_detached'          : 5,
        'u_i_start_pump'         : 6,
        'no_power'               : 7,
        'very_low_cart'          : 8,
        'cartridge_empty'        : 9,
        'exited_prime'           : 10,
        'set_time_date'          : 11,
        'cartridge_date_ok'      : 12,
        'sufficient_power'       : 13,
        'temp_basal_ended'       : 14,
        'pb_reminder_reset'      : 15,
        'max_level'              : 16
    }
    map_enum_to_diasend = {
        'still_pending'          :FLAG_ASANTE_STILL_PENDING,
        'user_acked'             :FLAG_ASANTE_USER_ACKED,
        'cradle_attached'        :FLAG_ASANTE_CRADLE_ATTACHED,
        'pump_attached'          :FLAG_ASANTE_PUMP_ATTACHED,
        'pump_attached_good_batt':FLAG_ASANTE_PUMP_ATTACHED_GOOD_BATT,
        'pump_detached'          :FLAG_ASANTE_PUMP_DETACHED,
        'u_i_start_pump'         :FLAG_ASANTE_U_I_START_PUMP,
        'no_power'               :FLAG_ASANTE_NO_POWER,
        'very_low_cart'          :FLAG_ASANTE_VERY_LOW_CART,
        'cartridge_empty'        :FLAG_ASANTE_CARTRIDGE_EMPTY,
        'exited_prime'           :FLAG_ASANTE_EXITED_PRIME,
        'set_time_date'          :FLAG_ASANTE_SET_TIME_DATE,
        'cartridge_date_ok'      :FLAG_ASANTE_CARTRIDE_DATE_OK,
        'sufficient_power'       :FLAG_ASANTE_SUFFICIENT_POWER,
        'temp_basal_ended'       :FLAG_ASANTE_TEMP_BASAL_ENDED,
        'pb_reminder_reset'      :FLAG_ASANTE_PB_REMINDER_RESET,
        'max_level'              :FLAG_ASANTE_MAX_LEVEL 
    }

class EnumPrimeTypes(EnumRecord):
    format = ('B',)
    enums  = {
        'tube'     : 0,
        'cannula'  : 1,
        'undefined': 2
    }

class EnumConnectionTypes(EnumRecord):
    format = ('B',)
    enums  = {
        'connect'       : 0,
        'disconnect'    : 1, 
        'cra_connect'   : 2, 
        'cra_disconnect': 3, 
        'uc_connect'    : 4, 
        'uc_disconnect' : 5, 
        'don_connect'   : 6, 
        'don_disconnect': 7, 
        'ill_connect'   : 8, 
        'ill_disconnect': 9
    }

class EnumClickResults(EnumRecord):
    format = ('B',)
    enums  = {
        'success'     : 0,
        'pump_stopped': 1,
        'mtr_err'     : 2
    }

class EnumBGUnitsTypes(EnumRecord):
    format = ('H',)
    enums  = {
        'mg_dl' : 0,
        'mmol_l': 1
    }

class EnumIOBModes(EnumRecord):
    format = ('H',)
    enums  = {
        'bg_only'    : 0,
        'entire_bolus': 1
    }

class EnumBolusButtonSelect(EnumRecord):
    format = ('H',)
    enums  = {
        'bolus_button_off'  :0,
        'audio_bolus_button':1,
        'smart_bolus_button':2
    }

class EnumRisingBGAlertRates(EnumRecord):
    format = ('H',)
    enums  = {
        'two_mg_dl_min'  : 0,
        'three_mg_dl_min': 1
    }

class EnumTimeFormats(EnumRecord):
    format = ('H',)
    enums  = {
        'twelve_hour'     : 0,
        'twenty_four_hour': 1,
    }

class EnumDailyAlertFrequencys(EnumRecord):
    format = ('H',)
    enums  = {
        'undefined':0,
        'everyday' :1,
        'weekdays' :2,
        'weekends' :3
    }

class EnumBeepVolumes(EnumRecord):
    format = ('H',)
    enums  = {
        'volume1':0,
        'volume2':1,
        'volume3':2,
        'volume4':3,
        'volume5':4
    }

class EnumScreenTimeouts(EnumRecord):
    format = ('H',)
    enums  = {
        'slow_timeout':0,
        'fast_timeout':1
    }

class EnumClickStates(EnumRecord):
    format = ('H',)
    enums  = {
        'done'   :0,
        'queued' :1,
        'running':2,
    }

class EnumThemeColors(EnumRecord):
    format = ('H',)
    enums  = {
        'grey'  : 0,
        'green' : 1,
        'blue'  : 2,
        'aqua'  : 3,
        'yellow': 4,
        'red'   : 5,
        'pink'  : 6,
        'purple': 7
    }

class EnumReasonForStop(EnumBitRecord):
    format = ('B',)
    enums  = {
        'detached'   : 1,
        'stop_btn'   : 2,
        'alarm'      : 4,
        'prime'      : 8,
        'set_clock'  : 16,
        'xchng_pump' : 32,
        'settings'   : 64,
        'batt_unsafe': 128
    }
    map_enum_to_diasend = {
        'detached'   : FLAG_ASANTE_STOP_REASON_DETACHED,
        'stop_btn'   : FLAG_ASANTE_STOP_REASON_STOP_BTN,
        'alarm'      : FLAG_ASANTE_STOP_REASON_ALARM,
        'prime'      : FLAG_ASANTE_STOP_REASON_PRIME,
        'set_clock'  : FLAG_ASANTE_STOP_REASON_SET_CLOCK,
        'xchng_pump' : FLAG_ASANTE_STOP_REASON_XCHNG_PUMP,
        'settings'   : FLAG_ASANTE_STOP_REASON_SETTINGS,
        'batt_unsafe': FLAG_ASANTE_STOP_REASON_BATT_UNSAFE
    }
    ignore_not_defined_bits = True

class EnumTimeEditLogFlagBits(EnumBitRecord):
    format = ('B',)
    enums  = {
        'user_time_flag':1,
        'edit_cause'    :2
    }
    ignore_not_defined_bits = True

# --- records

class AsanteRecord(BaseRecord):
    '''
    Base class for all Asante records. 

    This class holds states used during the interpretation of the 
    raw buffer from the terminal; including the record database 
    of all records extracted up to this point. 

    This class also contains a factory method for creating 
    instances of the sub-classes (depending on the currently extracted
    tag).

    '''

    # Most records has a timestamp (named date_time) but some (e.g. user settings)
    # don't. Use this value in that case.
    date_time = 1

    # There can exist a max number of records of a certain type. This is the 
    # fallback number. 
    max_nr_of_instances = 0

    # Shall this record be skipped if no time edit has been found before this?
    skip_if_no_time_edit = True

    # Setting for record - is this record a new format record?
    new_format = False

    # Global setting - should we use the new format or the old?
    device_uses_new_format = False

    def __lt__(self, other):
        return self.date_time < other.date_time

    @classmethod
    def create(cls, state_object, tag, buffer):
        '''
        Factory : look at the class attribute tag of all 
        sub-classes and create an instance of the sub-class 
        that matches. 
        '''

        def create_record(buffer):
            record = sub_class(buffer)
            record.state_object = state_object
            state_object.add_record(record)
            return record

        # If this is a pump with the new data format we try to find 
        # new format record types. 
        if AsanteRecord.device_uses_new_format:
            for sub_class in cls.__subclasses__():
                if sub_class.tag == tag and sub_class.new_format:
                    return create_record(buffer)

        # If this is a pump with the old data format _or_ we didn't
        # find a new format record type - let's look for the old
        # type instead.
        for sub_class in cls.__subclasses__():
            if sub_class.tag == tag and not sub_class.new_format:
                return create_record(buffer)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'AsanteRecord : unknown tag %s' % (tag))

    @staticmethod
    def use_new_format():
        AsanteRecord.device_uses_new_format = True

    @staticmethod
    def use_old_format():
        AsanteRecord.device_uses_new_format = False

    def convert_bg(self, value):
        '''
        Convert from Asante BG value to diasend BG value. 
        '''
        return float(value) / 10.0 * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

    def update_datetime(self):
        '''
        Set my own personal date time. 
        '''
        self.my_date_time = self.convert_rtc_to_datetime(self.date_time)

    def convert_rtc_to_datetime(self, rtc):
        '''
        Convert an Asante timestamp to a python datetime object.

        NOTE : The way the Asante pump works the usertime_n and rtc_n variables from the 
        state object will change over time (if the user has edited the settings). So this
        function must be called in sequence (i.e. with the correct usertime_n and 
        rtc_n set). 
        '''
        if self.state_object.usertime_n == None or self.state_object.rtc_n == None:
            dt = datetime(2008,1,1)
        else:
            dt = datetime(2008,1,1) + timedelta(seconds = rtc + self.state_object.usertime_n - self.state_object.rtc_n)

        return dt

    def convert_to_diasend(self):
        '''
        Abstract method for record types that should not be converted to diasend 
        format.
        '''
        return None

    def get_raw_buffer(self):
        '''
        Return the raw buffer of the message (string). Need a function because we have 
        sub-classes that needs special handling (think union).
        '''
        return self.raw_buffer

class RecordBolus(AsanteRecord):
    max_nr_of_instances = 450
    tag = 'SECTION00'
    format = ('HHIIHHHHI',EnumBolusTypes, EnumCompletionCode,'BBBB',)
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'bolus_id', 'clicks_delivered', 'now_clicks_requested', 
        'timed_clicks_requested', 'end_time', 'type', 'completion_code', 'duration_15_min_units', 
        'smart_bolus', 'smart_total_override', 'pad') 

    def convert_to_diasend(self):

        record  = {}

        # Only store completed boluses. It seems like data can be extracted from the Asante pump
        # while it is working (the controller unit is detached).
        if self.completion_code == EnumCompletionCode.in_progress:
            return record

        # This completion codes indicates that the bolus was canceled.
        if self.completion_code in (EnumCompletionCode.alarm, EnumCompletionCode.user_stop, EnumCompletionCode.detached, 
            EnumCompletionCode.sys_reset, EnumCompletionCode.batt_not_safe):
            add_record_flag(record, FLAG_CANCELED)

        start_datetime = self.my_date_time
        end_datetime   = self.convert_rtc_to_datetime(self.end_time)

        record[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BOLUS
        record[ELEM_TIMESTAMP] = start_datetime

        add_record_flag(record, self.completion_code.to_diasend())

        # If a bolus is stopped (user action, pump restart etc) the actually delivered
        # number of clicks will differ from the requested number of clicks. In that case we will
        # do an educated guess of how the bolus shape looks.        
        if self.clicks_delivered != (self.now_clicks_requested + self.timed_clicks_requested):

            if self.clicks_delivered > self.now_clicks_requested:
                spike    = self.now_clicks_requested
                extended = self.clicks_delivered - self.now_clicks_requested
                duration = int(timedelta_total_seconds(end_datetime - start_datetime) / 60)
            else:
                spike    = self.clicks_delivered
                extended = 0
                duration = 0

        else:
            spike    = self.now_clicks_requested
            extended = self.timed_clicks_requested
            duration = int(timedelta_total_seconds(end_datetime - start_datetime) / 60)

        # See #759 for diasend definition of bolus types.
        
        # Note : spike = 0 is ok; this is an extended bolus without
        # an immediate bolus.
        record[ELEM_VAL] = spike * ASANTE_TO_DIASEND_BOLUS_FACTOR

        if extended != 0:
            add_record_value(record, VALUE_TYPE_INS_BOLUS_EXT, extended * ASANTE_TO_DIASEND_BOLUS_FACTOR)
            add_record_value(record, VALUE_TYPE_DURATION, duration)

        # Smart bolus = Got result from Insulin calculator.
        if self.smart_bolus == 1:

            # Look up the corresponding log smart entry.
            smart_bolus_records = self.state_object.search_instance_with_key_value(RecordSmart, 'bolus_id', self.bolus_id)

            if len(smart_bolus_records) == 1:

                # Note : variables net_carb_insulin, net_bg_insulin and carb_insulin_percent are ignored by diasend. 
                # These variables records the actual insulin delivered as a result of using the insulin calculator; we
                # are only interested in the values from the insulin calculator.

                # Note : variable bolus_delivered is ignored. We loop through all bolus records and only use smart records
                # that are referenced from a bolus record.

                # Note : variable total_override is ignored. If suggested != delivered the web will indicate that the 
                # value is overridden. 

                smart_bolus_record = smart_bolus_records[0]

                # Calculated insulin used to cover food carbs.
                if smart_bolus_record.gross_carb_insulin != 0:
                    add_record_value(record, VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL, 
                        smart_bolus_record.gross_carb_insulin * ASANTE_TO_DIASEND_SMART_FACTOR)

                # Calculated insulin used to reduce BG.
                if smart_bolus_record.gross_bg_insulin != 0:
                    add_record_value(record, VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR, 
                        smart_bolus_record.gross_bg_insulin * ASANTE_TO_DIASEND_SMART_FACTOR)

                # Insulin on board.
                if smart_bolus_record.iob != 0:
                    add_record_value(record, VALUE_TYPE_INS_BOLUS_IOB, 
                        abs(smart_bolus_record.iob * ASANTE_TO_DIASEND_SMART_FACTOR))

                # Total smart bolus amount calculated.
                if smart_bolus_record.total_insulin != 0:
                    add_record_value(record, VALUE_TYPE_INS_BOLUS_SUGGESTED, 
                        smart_bolus_record.total_insulin * ASANTE_TO_DIASEND_SMART_FACTOR)

            else:
                # If the bolus record indicates that we have a smart bolus record _but_
                # we are unable to find it - simply ignore the entire record (the data
                # is not conclusive).
                record = {}

        return record

class RecordSmart(AsanteRecord):
    max_nr_of_instances = 450
    tag = 'SECTION01'
    format = ('HHIIHhhihhhhhhhbb',)
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'bolus_id', 'current_bg', 'food_carbs', 'iob', 
        'iob_mode', 'total_insulin', 'gross_bg_insulin', 'gross_carb_insulin', 'net_bg_insulin', 
        'net_carb_insulin', 'carb_insulin_percent', 'bolus_delivered', 'total_override') 
  
    def convert_to_diasend(self):

        records = []

        dt = self.my_date_time

        # User added BG in wizard calculator.
        if self.current_bg != 0:
            record = {
                ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE, 
                ELEM_TIMESTAMP : dt,
                ELEM_VAL       : self.convert_bg(self.current_bg)
            }
            add_record_flag(record, FLAG_MANUAL)
            records.append(record)

        # User added carb in wizard calculator.
        if self.food_carbs != 0:
            record = {
                ELEM_VAL_TYPE  : VALUE_TYPE_CARBS, 
                ELEM_TIMESTAMP : dt,
                ELEM_VAL       : self.food_carbs
            }
            records.append(record)

        return records

class RecordBasal(AsanteRecord):
    max_nr_of_instances = 2232
    tag = 'SECTION02'
    format = ('HHIIBb')
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'clicks_delivered', 'pad')

    def convert_to_diasend(self):

        records = []

        # We need two data points to draw the basal graph (to be able to 
        # calculate the mean basal rate during the segment).
        if self.state_object.previous_basal_record:

            dt_prev   = self.state_object.previous_basal_record.my_date_time
            time_diff = self.my_date_time - dt_prev

            # Loop all missed basal records between the previous and this
            # basal record. Subtract the duration of each missed basal period to get 
            # the active duration.
            for mbr in self.state_object.stored_missed_basal_records:
                time_diff = time_diff - (mbr[1] - mbr[0])

            if timedelta_total_seconds(time_diff) > 0:

                # Calculate the mean basal rate by using the actually delivered amount of basal 
                # and the active duration.
                mean_basal_rate = ASANTE_TO_DIASEND_BASAL_FACTOR * (
                    3600.0 * float(self.state_object.previous_basal_record.clicks_delivered) / float(timedelta_total_seconds(time_diff)))

                # Add a basal record at start of segment (previous basal record).
                record = {}
                record[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BASAL
                record[ELEM_TIMESTAMP] = self.state_object.previous_basal_record.my_date_time
                record[ELEM_VAL]       = mean_basal_rate
                if self.state_object.current_temp_basal_end_datetime:
                    add_record_flag(record, FLAG_TEMP_MODIFIED)
                records.append(record)

                # Now update graph using the missed basal start / stop during the segment.
                for mbr in self.state_object.stored_missed_basal_records:

                    dt_mbr_start = mbr[0]
                    dt_mbr_end   = mbr[1]

                    record = {}
                    record[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BASAL
                    record[ELEM_TIMESTAMP] = dt_mbr_start
                    record[ELEM_VAL]       = 0
                    if self.state_object.current_temp_basal_end_datetime:
                        add_record_flag(record, FLAG_TEMP_MODIFIED)
                    records.append(record)

                    # The right side of the dip (positive flank) may occur
                    # very close to the next data point (the current basal record).
                    # If the basal rate changes this will result in an ugly spike
                    # in the graph. This is simple filter to require at least one 
                    # minute between the right hand side of the dip and the next 
                    # data point. 
                    if timedelta_total_seconds(self.my_date_time - dt_mbr_end) > 60:
                        record = {}
                        record[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BASAL
                        record[ELEM_TIMESTAMP] = dt_mbr_end
                        record[ELEM_VAL]       = mean_basal_rate
                        if self.state_object.current_temp_basal_end_datetime:
                            add_record_flag(record, FLAG_TEMP_MODIFIED)
                        records.append(record)

        self.state_object.stored_missed_basal_records = []
        self.state_object.previous_basal_record = self

        # Check if temp basal is active and check if it expired during the current segment.
        if self.state_object.current_temp_basal_end_datetime and (self.state_object.current_temp_basal_end_datetime <= self.my_date_time):
            self.state_object.current_temp_basal_end_datetime = None

        return records

class RecordBasalConfigEventProfile(AsanteRecord):
    '''
    Union struct class for RecordBasalConfig. 
    '''
    tag = ''
    format = ('Hh', EnumProfileEvent, '8sb')
    variables = ('active_profile', 'total24_hour', 'profile_event', 'name', 'profile_number')

class RecordBasalConfigEventBasal(AsanteRecord):
    '''
    Union struct class for RecordBasalConfig. 
    '''
    tag = ''
    format = ('HHH', EnumCompletionCode, 'b', '6s')
    variables = ('percentage', 'duration_programmed', 'duration_final', 'completion_code', 'pad3', '_union_pad')
   
class RecordBasalConfigEventPumpStopped(AsanteRecord):
    '''
    Union struct class for RecordBasalConfig. 
    '''
    tag = ''
    format = ('I', EnumCompletionCode, 'b', '8s')
    variables = ('restart_time', 'cause', 'pad4', '_union_pad')

class RecordBasalConfig(AsanteRecord):
    '''
    The basal config record contains a union. We store the union part in 
    RecordBasalConfig.event.
    '''
    max_nr_of_instances = 400
    tag = 'SECTION03'
    format = ('HHII', EnumEventType, 'B')
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'event_type', 'pad1')

    def __init__(self, buffer):
        super(RecordBasalConfig, self).__init__(buffer)

        if self.event_type.value == EnumEventType.profile_event:
            self.event = RecordBasalConfigEventProfile(buffer)

        elif self.event_type.value == EnumEventType.temp_basal:
            self.event = RecordBasalConfigEventBasal(buffer)

        elif self.event_type.value == EnumEventType.pump_stopped:
            self.event = RecordBasalConfigEventPumpStopped(buffer)

        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Basal config - unknown event type %d' % (self.event_type.value))

    def convert_to_diasend(self):
        dt = self.my_date_time

        if self.event_type.value == EnumEventType.temp_basal:
            logging.debug('BASAL CONFIG TEMP BASAL %s percent %d duration %d', dt, self.event.percentage, self.event.duration_programmed)

            self.state_object.current_temp_basal_start_datetime = dt
            self.state_object.current_temp_basal_end_datetime   = dt + timedelta(seconds = 60 * self.event.duration_final)

        elif self.event_type.value == EnumEventType.profile_event:

            logging.debug('BASAL CONFIG PROFILE EVENT %s event %s', dt, self.event.profile_event.to_text())

            record = {}

            record[ELEM_VAL_TYPE]  = VALUE_TYPE_EVENT
            record[ELEM_TIMESTAMP] = dt
            record[ELEM_VAL]       = self.event.profile_event.to_diasend()

            return record

        elif self.event_type.value == EnumEventType.pump_stopped:

            logging.debug('BASAL CONFIG PUMP STOPPED %s restart time %s cause %s', dt, 
                self.convert_rtc_to_datetime(self.event.restart_time), 
                self.event.cause.to_text())
            
        return None

    def get_raw_buffer(self):
        '''
        The union complicates things a bit - this function returns the raw
        data which is header + union.
        '''
        return self.raw_buffer + self.event.raw_buffer

class RecordAlarmAlertOldFormat(AsanteRecord):
    max_nr_of_instances = 401
    tag = 'SECTION04'
    format = ('HHIIIhh', EnumMessagesOldFormat, EnumClearConditionOldFormat)
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'ack_time', 'qualifier1', 'qualifier2', 'event', 'ack_cause')

    # Note : we do not store qualifier1 and qualifier2.     
    # Note : we do not store ack_time (only creation time).

    def convert_to_diasend(self):
        dt = self.my_date_time

        logging.debug('ALARM %s message %s clear condition %s', dt, self.event.to_text(), self.ack_cause.to_text())

        record = {}

        record[ELEM_VAL_TYPE]  = VALUE_TYPE_ALARM
        record[ELEM_TIMESTAMP] = dt
        record[ELEM_VAL]       = self.event.to_diasend()

        add_record_flag(record, self.ack_cause.to_diasend())

        return record

class RecordAlarmAlertNewFormat(AsanteRecord):
    max_nr_of_instances = 401
    tag = 'SECTION04'
    new_format = True
    format = ('HHIIIhh', EnumMessagesNewFormat, EnumClearConditionNewFormat)
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'ack_time', 'qualifier1', 'qualifier2', 'event', 'ack_cause')

    # Note : we do not store qualifier1 and qualifier2.     
    # Note : we do not store ack_time (only creation time).

    def convert_to_diasend(self):
        dt = self.my_date_time

        logging.debug('ALARM %s message %s clear condition %s', dt, self.event.to_text(), self.ack_cause.to_text())

        record = {}

        record[ELEM_VAL_TYPE]  = VALUE_TYPE_ALARM
        record[ELEM_TIMESTAMP] = dt
        record[ELEM_VAL]       = self.event.to_diasend()

        add_record_flag(record, self.ack_cause.to_diasend())

        return record

class RecordPrime(AsanteRecord):
    max_nr_of_instances = 128
    tag = 'SECTION05'
    format = ('HHIIHH', EnumCompletionCode, EnumPrimeTypes)
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'clicks_requested', 'clicks_delivered', 'completion_code', 'type')

    def convert_to_diasend(self):

        record = {}

        if self.completion_code == EnumCompletionCode.in_progress:
            return record

        dt = self.my_date_time

        logging.debug('PRIME %s clicks delivered %d completion %s prime type %s', dt, 
            self.clicks_delivered, 
            self.completion_code.to_text(), 
            self.type.to_text())

        prime_cancelled = self.completion_code in (
            EnumCompletionCode.alarm, 
            EnumCompletionCode.user_stop, 
            EnumCompletionCode.detached, 
            EnumCompletionCode.sys_reset, 
            EnumCompletionCode.batt_not_safe)

        if not prime_cancelled:
            
            record[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_PRIME
            record[ELEM_TIMESTAMP] = dt
            record[ELEM_VAL]       = self.clicks_delivered * ASANTE_TO_DIASEND_BOLUS_FACTOR

            if self.type == EnumPrimeTypes.tube:
                add_record_flag(record, FLAG_PRIMED)
            elif self.type == EnumPrimeTypes.cannula:
                add_record_flag(record, FLAG_CANNULA_BOLUS)

        return record

class RecordPump(AsanteRecord):
    max_nr_of_instances = 512
    tag = 'SECTION06'
    format = ('HHIIIIh', EnumConnectionTypes, 'b')
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'pb_ser_num_cm', 'pb_ser_num_rtc', 'insulin_volume', 'connection_type', 'pad')

    # Review : more data we should have in diasend?

class RecordMissedBasal(AsanteRecord):
    max_nr_of_instances = 256
    tag = 'SECTION07'
    format = ('HHIIIH', EnumReasonForStop, 'b')
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'start_of_suspension', 'clicks_missed', 'reasons_for_stopping', 'pad')

    def convert_to_diasend(self):

        # Note : we use the missed basal record to record suspend and resume events. It seems like the 
        # Basal Config Pump Stopped event could be used instead - it holds the same information and 
        # a record of each type is created in the test data I have seen so far. 

        self.state_object.stored_missed_basal_records.append((
            self.convert_rtc_to_datetime(self.start_of_suspension), 
            self.my_date_time))

        records = []

        record = {}

        record[ELEM_VAL_TYPE]  = VALUE_TYPE_EVENT
        record[ELEM_TIMESTAMP] = self.convert_rtc_to_datetime(self.start_of_suspension)
        record[ELEM_VAL]       = DIASEND_EVENT_DELIVERY_SUSPENDED

        # Convert bit value to list of flags.
        map(lambda flag:add_record_flag(record, flag), self.reasons_for_stopping.to_diasend_list())

        records.append(record)

        record = {}

        record[ELEM_VAL_TYPE]  = VALUE_TYPE_EVENT
        record[ELEM_TIMESTAMP] = self.my_date_time
        record[ELEM_VAL]       = DIASEND_EVENT_DELIVERY_RESUMED

        records.append(record)

        logging.debug('BASAL MISSED start %s end %s clicks missed %d', 
            self.convert_rtc_to_datetime(self.start_of_suspension),
            self.my_date_time,
            self.clicks_missed)

        return records

class RecordTimeEdits(AsanteRecord):
    max_nr_of_instances = 64
    skip_if_no_time_edit = False
    tag = 'SECTION08'
    format = ('HHIII', EnumTimeEditLogFlagBits, 'b')
    variables = ('section', 'crc', 'date_time', 'seq_nmbr', 'user_set_time', 'flags', 'pad')

    def convert_to_diasend(self):

        record = {}

        if self.state_object.usertime_n != None and self.state_object.rtc_n != None:
            record[ELEM_VAL_TYPE]  = VALUE_TYPE_EVENT
            record[ELEM_TIMESTAMP] = self.convert_rtc_to_datetime(self.date_time)
            record[ELEM_VAL]       = DIASEND_EVENT_DATETIME_CHANGE
    
        self.state_object.rtc_n      = self.date_time
        self.state_object.usertime_n = self.user_set_time

        return record

class RecordUserSettings(object):

    def convert_to_diasend(self):

        records = []

        # I:C ratio
        
        periods  = []
        for segment in range(8):

            minutes_since_midnight = self.__dict__['food_profile_start_time_%d' % (segment)]
            carb_ratio             = float(self.__dict__['food_profile_carb_ratio_%d' % (segment)]) * 0.1

            if minutes_since_midnight >= 0:
                periods.append((datetime(2008,1,1) + timedelta(seconds = minutes_since_midnight * 60), carb_ratio))

        if len(periods) > 0:
            program = {}
            program[PROGRAM_TYPE]    = PROGRAM_TYPE_IC_RATIO
            program[PROGRAM_NAME]    = '1'
            program[PROGRAM_PERIODS] = periods
            records.append(program)
                
        # Basal profile
        for profile in range(4):

            periods = []

            segment_count = self.__dict__['basal_profile_%d_segment_count' % (profile)]

            for segment in range(segment_count):
                minutes_since_midnight = self.__dict__['basal_profile_%d_segment_%d_start_time' % (profile, segment)]
                amount                 = float(self.__dict__['basal_profile_%d_segment_%d_amount' % (profile, segment)]) * 10.0
                periods.append((datetime(2008,1,1) + timedelta(seconds = minutes_since_midnight * 60), amount))

            if len(periods) > 0:
                program = {}
                program[PROGRAM_TYPE]    = PROGRAM_TYPE_BASAL
                program[PROGRAM_NAME]    = str(profile + 1)
                program[PROGRAM_PERIODS] = periods
                records.append(program)

        # BG target program
        periods = []

        for segment in range(3):
            minutes_since_midnight = self.__dict__['target_bg_start_time_%d' % (segment)]
            min_bg                 = self.__dict__['target_bg_min_bg_%d' % (segment)]
            max_bg                 = self.__dict__['target_bg_max_bg_%d' % (segment)]

            # Programs are not converted by Generic.py.
            min_bg = min_bg * 100 / VAL_FACTOR_CONV_MMOL_TO_MGDL
            max_bg = max_bg * 100 / VAL_FACTOR_CONV_MMOL_TO_MGDL

            if minutes_since_midnight >= 0:
                mean_bg   = ((min_bg + max_bg) / 2.0)
                deviation = ((max_bg - min_bg) / 2.0)
                periods.append((datetime(2008,1,1) + timedelta(seconds = minutes_since_midnight * 60), mean_bg, ((PROGRAM_VALUE_BG_DEVIATION, deviation),)))

        if len(periods) > 0:
            program = {}
            program[PROGRAM_TYPE]    = PROGRAM_TYPE_BG_TARGET
            program[PROGRAM_NAME]    = '1'
            program[PROGRAM_PERIODS] = periods
            records.append(program)

        # ISF
        periods = []

        for segment in range(3):

            minutes_since_midnight = self.__dict__['bg_profile_start_time_%d' % (segment)]
            bg_ratio               = self.__dict__['bg_profile_bg_ratio_%d' % (segment)] * 100 / VAL_FACTOR_CONV_MMOL_TO_MGDL

            if minutes_since_midnight >= 0:
                periods.append((datetime(2008,1,1) + timedelta(seconds = minutes_since_midnight * 60), bg_ratio))

        if len(periods) > 0:
            program = {}
            program[PROGRAM_TYPE]    = PROGRAM_TYPE_ISF
            program[PROGRAM_NAME]    = '1'
            program[PROGRAM_PERIODS] = periods

            records.append(program)

        # One-off settings
        map_setting_to_diasend = [ 
            ('smart_bolus_enable',      SETTING_ASANTE_SMART_BOLUS_ENABLE, 1), 
            ('smart_bolus_initialized', SETTING_ASANTE_SMART_BOLUS_INITIALIZED, 1), 
            ('bg_units_type',           SETTING_BG_UNIT, {0:'mg/dl', 1:'mmol/L'}), 
            ('insulin_action',          SETTING_ASANTE_INSULIN_ACTION, 1), 
            ('iob_mode',                SETTING_ASANTE_IOB_MODE, 1), 
            ('bolus_button_select',     SETTING_ASANTE_BOLUS_BUTTON_SELECT, 1), 
            ('combo_bolus_enable',      SETTING_ASANTE_COMBO_BOLUS_ENABLE, 1), 
            ('timed_bolus_enable',      SETTING_ASANTE_TIMED_BOLUS_ENABLE, 1), 
            ('bolus_reminder_enable',   SETTING_ASANTE_BOLUS_REMINDER_ENABLE, 1), 
            ('bolus_step_size',         SETTING_ASANTE_BOLUS_STEP_SIZE, 1), 
            ('audio_bolus_step_size',   SETTING_ASANTE_AUDIO_BOLUS_STEP_SIZE, 1), 
            ('bolus_limit',             SETTING_ASANTE_BOLUS_LIMIT, 1), 
            ('active_profile',          SETTING_ASANTE_ACTIVE_PROFILE, 1), 
            ('basal_limit',             SETTING_ASANTE_BASAL_LIMIT, 1), 
            ('time_format',             SETTING_ASANTE_TIME_FORMAT, 1), 
            ('bg_reminder_enable',      SETTING_ASANTE_BG_REMINDER_ENABLE, 1), 
            ('bg_reminder_time',        SETTING_ASANTE_BG_REMINDER_TIME, 1), 
            ('low_insulin_enable',      SETTING_ASANTE_LOW_INSULIN_ENABLE, 1), 
            ('low_insulin_level',       SETTING_ASANTE_LOW_INSULIN_LEVEL, 1), 
            ('notification_timing',     SETTING_ASANTE_NOTIFICATION_TIMING, 1), 
            ('delivery_limit',          SETTING_ASANTE_DELIVERY_LIMIT, 1), 
            ('auto_off_enable',         SETTING_ASANTE_AUTO_OFF_ENABLE, 1), 
            ('auto_off_duration',       SETTING_ASANTE_AUTO_OFF_DURATION, 1), 
            ('pump_reminder_enable',    SETTING_ASANTE_PUMP_REMINDER_ENABLE, 1), 
            ('pump_reminder_hours',     SETTING_ASANTE_PUMP_REMINDER_HOURS, 1), 
            ('target_bg_min',           SETTING_ASANTE_TARGET_BG_MIN, 1), 
            ('target_bg_max',           SETTING_ASANTE_TARGET_BG_MAX, 1), 
            ('beep_volume',             SETTING_ASANTE_BEEP_VOLUME, 1), 
            ('button_guard_enable',     SETTING_ASANTE_BUTTON_GUARD_ENABLE, 1), 
            ('splash_screen_enable',    SETTING_ASANTE_SPLASH_SCREEN_ENABLE, 1), 
            ('flashlight_enable',       SETTING_ASANTE_FLASHLIGHT_ENABLE, 1), 
            ('screen_timeout' ,         SETTING_ASANTE_SCREEN_TIMEOUT, 1)
        ]

        # Add new format settings.
        if hasattr(self, 'theme_color'):
            map_setting_to_diasend.append(('theme_color', SETTING_ASANTE_THEME_COLOR, 1))

        def get_setting_dict_value(key, setting_dict_or_conversion_factor, default_value):
            if isinstance(setting_dict_or_conversion_factor, dict):
                if key in setting_dict_or_conversion_factor:
                    return setting_dict_or_conversion_factor[key]
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 
                        'Unable to find setting key %s in for setting %s' % (key, setting_dict_or_conversion_factor))
            return default_value * setting_dict_or_conversion_factor

        for setting in map_setting_to_diasend:

            var = self.__dict__[setting[0]]
            # This is an enum.
            if isinstance(var, BaseRecord):
                value = get_setting_dict_value(var.value, setting[2], var.value)
            else:
                # Not an enum - but it might be a dictionary that holds a 
                # translation of the setting.
                value = get_setting_dict_value(var, setting[2], var)

            records.append({SETTINGS_LIST: {setting[1]: value}})

        return records

class RecordUserSettingsOldFormat(RecordUserSettings, AsanteRecord):
    max_nr_of_instances = 1
    skip_if_no_time_edit = False
    tag = 'SECTION09'
    format = ('HH2h', EnumBGUnitsTypes, 
        # food profile
        '16h', 
        # bg profile
        '6h', 
        # target bg profile
        '9h', 'h', EnumIOBModes, EnumBolusButtonSelect, '7h', 
        # basal profile
        '8s22h', '8s22h', '8s22h', '8s22h', 
        'h', EnumTimeFormats, '6h', 
        '2h', EnumDailyAlertFrequencys, '30s', 
        '2h', EnumDailyAlertFrequencys, '30s', 
        '2h', EnumDailyAlertFrequencys, '30s', 
        '2h', EnumDailyAlertFrequencys, '30s', 
        '2h', EnumDailyAlertFrequencys, '30s', 
        '2h', '2h', '2h', EnumBeepVolumes, '3h', EnumScreenTimeouts, '30s30s30s')

    variables = ('section', 'crc', 'smart_bolus_enable','smart_bolus_initialized', 'bg_units_type',
        'food_profile_start_time_0', 'food_profile_carb_ratio_0', 
        'food_profile_start_time_1', 'food_profile_carb_ratio_1', 
        'food_profile_start_time_2', 'food_profile_carb_ratio_2', 
        'food_profile_start_time_3', 'food_profile_carb_ratio_3', 
        'food_profile_start_time_4', 'food_profile_carb_ratio_4', 
        'food_profile_start_time_5', 'food_profile_carb_ratio_5', 
        'food_profile_start_time_6', 'food_profile_carb_ratio_6', 
        'food_profile_start_time_7', 'food_profile_carb_ratio_7', 
        'bg_profile_start_time_0', 'bg_profile_bg_ratio_0',  
        'bg_profile_start_time_1', 'bg_profile_bg_ratio_1', 
        'bg_profile_start_time_2', 'bg_profile_bg_ratio_2', 
        'target_bg_start_time_0', 'target_bg_min_bg_0', 
        'target_bg_max_bg_0', 'target_bg_start_time_1', 
        'target_bg_min_bg_1', 'target_bg_max_bg_1', 
        'target_bg_start_time_2', 'target_bg_min_bg_2', 
        'target_bg_max_bg_2', 
        'insulin_action', 'iob_mode', 'bolus_button_select', 
        'combo_bolus_enable', 'timed_bolus_enable', 'bolus_reminder_enable', 
        'bolus_step_size', 'audio_bolus_step_size', 'bolus_limit', 'active_profile', 
        'basal_profile_0_name', 'basal_profile_0_segment_count', 'basal_profile_0_total24_hour', 
        'basal_profile_0_segment_0_start_time', 'basal_profile_0_segment_0_amount', 'basal_profile_0_segment_1_start_time', 
        'basal_profile_0_segment_1_amount', 'basal_profile_0_segment_2_start_time', 'basal_profile_0_segment_2_amount', 
        'basal_profile_0_segment_3_start_time', 'basal_profile_0_segment_3_amount', 'basal_profile_0_segment_4_start_time', 
        'basal_profile_0_segment_4_amount', 'basal_profile_0_segment_5_start_time', 'basal_profile_0_segment_5_amount', 
        'basal_profile_0_segment_6_start_time', 'basal_profile_0_segment_6_amount', 'basal_profile_0_segment_7_start_time', 
        'basal_profile_0_segment_7_amount', 'basal_profile_0_segment_8_start_time', 'basal_profile_0_segment_8_amount', 
        'basal_profile_0_segment_9_start_time', 'basal_profile_0_segment_9_amount', 
        'basal_profile_1_name', 'basal_profile_1_segment_count', 'basal_profile_1_total24_hour', 
        'basal_profile_1_segment_0_start_time', 'basal_profile_1_segment_0_amount', 'basal_profile_1_segment_1_start_time', 
        'basal_profile_1_segment_1_amount', 'basal_profile_1_segment_2_start_time', 'basal_profile_1_segment_2_amount', 
        'basal_profile_1_segment_3_start_time', 'basal_profile_1_segment_3_amount', 'basal_profile_1_segment_4_start_time', 
        'basal_profile_1_segment_4_amount', 'basal_profile_1_segment_5_start_time', 'basal_profile_1_segment_5_amount', 
        'basal_profile_1_segment_6_start_time', 'basal_profile_1_segment_6_amount', 'basal_profile_1_segment_7_start_time', 
        'basal_profile_1_segment_7_amount', 'basal_profile_1_segment_8_start_time', 'basal_profile_1_segment_8_amount', 
        'basal_profile_1_segment_9_start_time', 'basal_profile_1_segment_9_amount', 
        'basal_profile_2_name', 'basal_profile_2_segment_count', 'basal_profile_2_total24_hour', 
        'basal_profile_2_segment_0_start_time', 'basal_profile_2_segment_0_amount', 'basal_profile_2_segment_1_start_time', 
        'basal_profile_2_segment_1_amount', 'basal_profile_2_segment_2_start_time', 'basal_profile_2_segment_2_amount', 
        'basal_profile_2_segment_3_start_time', 'basal_profile_2_segment_3_amount', 'basal_profile_2_segment_4_start_time', 
        'basal_profile_2_segment_4_amount', 'basal_profile_2_segment_5_start_time', 'basal_profile_2_segment_5_amount', 
        'basal_profile_2_segment_6_start_time', 'basal_profile_2_segment_6_amount', 'basal_profile_2_segment_7_start_time', 
        'basal_profile_2_segment_7_amount', 'basal_profile_2_segment_8_start_time', 'basal_profile_2_segment_8_amount', 
        'basal_profile_2_segment_9_start_time', 'basal_profile_2_segment_9_amount', 
        'basal_profile_3_name', 'basal_profile_3_segment_count', 'basal_profile_3_total24_hour', 
        'basal_profile_3_segment_0_start_time', 'basal_profile_3_segment_0_amount', 'basal_profile_3_segment_1_start_time', 
        'basal_profile_3_segment_1_amount', 'basal_profile_3_segment_2_start_time', 'basal_profile_3_segment_2_amount', 
        'basal_profile_3_segment_3_start_time', 'basal_profile_3_segment_3_amount', 'basal_profile_3_segment_4_start_time', 
        'basal_profile_3_segment_4_amount', 'basal_profile_3_segment_5_start_time', 'basal_profile_3_segment_5_amount', 
        'basal_profile_3_segment_6_start_time', 'basal_profile_3_segment_6_amount', 'basal_profile_3_segment_7_start_time', 
        'basal_profile_3_segment_7_amount', 'basal_profile_3_segment_8_start_time', 'basal_profile_3_segment_8_amount', 
        'basal_profile_3_segment_9_start_time', 'basal_profile_3_segment_9_amount', 
        'basal_limit', 'time_format', 'bg_reminder_enable', 'bg_reminder_time', 
        'low_insulin_enable', 'low_insulin_level', 'notification_timing', 'delivery_limit', 
        'daily_alert_0_enable', 'daily_alert_0_time', 'daily_alert_0_frequency', 'daily_alert_0_text', 
        'daily_alert_1_enable', 'daily_alert_1_time', 'daily_alert_1_frequency', 'daily_alert_1_text', 
        'daily_alert_2_enable', 'daily_alert_2_time', 'daily_alert_2_frequency', 'daily_alert_2_text', 
        'daily_alert_3_enable', 'daily_alert_3_time', 'daily_alert_3_frequency', 'daily_alert_3_text', 
        'daily_alert_4_enable', 'daily_alert_4_time', 'daily_alert_4_frequency', 'daily_alert_4_text', 
        'auto_off_enable', 'auto_off_duration', 'pump_reminder_enable', 'pump_reminder_hours', 
        'target_bg_min', 'target_bg_max', 'beep_volume', 'button_guard_enable', 
        'splash_screen_enable', 'flashlight_enable', 'screen_timeout', 
        'splash_screen_text_line_1', 'splash_screen_text_line_2', 'splash_screen_text_line_3')

class RecordUserSettingsNewFormat(RecordUserSettings, AsanteRecord):
    max_nr_of_instances = 1
    skip_if_no_time_edit = False
    tag = 'SECTION09'
    new_format = True
    format = ('HH2h', EnumBGUnitsTypes, 
        # food profile
        '16h', 
        # bg profile
        '6h', 
        # target bg profile
        '9h', 'h', EnumIOBModes, EnumBolusButtonSelect, '7h', 
        # basal profile
        '8s22h', '8s22h', '8s22h', '8s22h', 
        'h', EnumTimeFormats, '6h', 
        '2h', '2h', '2h', EnumBeepVolumes, EnumThemeColors, '3h', 'h', EnumScreenTimeouts, '32s32s32s')

    variables = ('section', 'crc', 'smart_bolus_enable','smart_bolus_initialized', 'bg_units_type',
        'food_profile_start_time_0', 'food_profile_carb_ratio_0', 
        'food_profile_start_time_1', 'food_profile_carb_ratio_1', 
        'food_profile_start_time_2', 'food_profile_carb_ratio_2', 
        'food_profile_start_time_3', 'food_profile_carb_ratio_3', 
        'food_profile_start_time_4', 'food_profile_carb_ratio_4', 
        'food_profile_start_time_5', 'food_profile_carb_ratio_5', 
        'food_profile_start_time_6', 'food_profile_carb_ratio_6', 
        'food_profile_start_time_7', 'food_profile_carb_ratio_7', 
        'bg_profile_start_time_0', 'bg_profile_bg_ratio_0',  
        'bg_profile_start_time_1', 'bg_profile_bg_ratio_1', 
        'bg_profile_start_time_2', 'bg_profile_bg_ratio_2', 
        'target_bg_start_time_0', 'target_bg_min_bg_0', 
        'target_bg_max_bg_0', 'target_bg_start_time_1', 
        'target_bg_min_bg_1', 'target_bg_max_bg_1', 
        'target_bg_start_time_2', 'target_bg_min_bg_2', 
        'target_bg_max_bg_2', 
        'insulin_action', 'iob_mode', 'bolus_button_select', 
        'combo_bolus_enable', 'timed_bolus_enable', 'bolus_reminder_enable', 
        'bolus_step_size', 'audio_bolus_step_size', 'bolus_limit', 'active_profile', 
        'basal_profile_0_name', 'basal_profile_0_segment_count', 'basal_profile_0_total24_hour', 
        'basal_profile_0_segment_0_start_time', 'basal_profile_0_segment_0_amount', 'basal_profile_0_segment_1_start_time', 
        'basal_profile_0_segment_1_amount', 'basal_profile_0_segment_2_start_time', 'basal_profile_0_segment_2_amount', 
        'basal_profile_0_segment_3_start_time', 'basal_profile_0_segment_3_amount', 'basal_profile_0_segment_4_start_time', 
        'basal_profile_0_segment_4_amount', 'basal_profile_0_segment_5_start_time', 'basal_profile_0_segment_5_amount', 
        'basal_profile_0_segment_6_start_time', 'basal_profile_0_segment_6_amount', 'basal_profile_0_segment_7_start_time', 
        'basal_profile_0_segment_7_amount', 'basal_profile_0_segment_8_start_time', 'basal_profile_0_segment_8_amount', 
        'basal_profile_0_segment_9_start_time', 'basal_profile_0_segment_9_amount', 
        'basal_profile_1_name', 'basal_profile_1_segment_count', 'basal_profile_1_total24_hour', 
        'basal_profile_1_segment_0_start_time', 'basal_profile_1_segment_0_amount', 'basal_profile_1_segment_1_start_time', 
        'basal_profile_1_segment_1_amount', 'basal_profile_1_segment_2_start_time', 'basal_profile_1_segment_2_amount', 
        'basal_profile_1_segment_3_start_time', 'basal_profile_1_segment_3_amount', 'basal_profile_1_segment_4_start_time', 
        'basal_profile_1_segment_4_amount', 'basal_profile_1_segment_5_start_time', 'basal_profile_1_segment_5_amount', 
        'basal_profile_1_segment_6_start_time', 'basal_profile_1_segment_6_amount', 'basal_profile_1_segment_7_start_time', 
        'basal_profile_1_segment_7_amount', 'basal_profile_1_segment_8_start_time', 'basal_profile_1_segment_8_amount', 
        'basal_profile_1_segment_9_start_time', 'basal_profile_1_segment_9_amount', 
        'basal_profile_2_name', 'basal_profile_2_segment_count', 'basal_profile_2_total24_hour', 
        'basal_profile_2_segment_0_start_time', 'basal_profile_2_segment_0_amount', 'basal_profile_2_segment_1_start_time', 
        'basal_profile_2_segment_1_amount', 'basal_profile_2_segment_2_start_time', 'basal_profile_2_segment_2_amount', 
        'basal_profile_2_segment_3_start_time', 'basal_profile_2_segment_3_amount', 'basal_profile_2_segment_4_start_time', 
        'basal_profile_2_segment_4_amount', 'basal_profile_2_segment_5_start_time', 'basal_profile_2_segment_5_amount', 
        'basal_profile_2_segment_6_start_time', 'basal_profile_2_segment_6_amount', 'basal_profile_2_segment_7_start_time', 
        'basal_profile_2_segment_7_amount', 'basal_profile_2_segment_8_start_time', 'basal_profile_2_segment_8_amount', 
        'basal_profile_2_segment_9_start_time', 'basal_profile_2_segment_9_amount', 
        'basal_profile_3_name', 'basal_profile_3_segment_count', 'basal_profile_3_total24_hour', 
        'basal_profile_3_segment_0_start_time', 'basal_profile_3_segment_0_amount', 'basal_profile_3_segment_1_start_time', 
        'basal_profile_3_segment_1_amount', 'basal_profile_3_segment_2_start_time', 'basal_profile_3_segment_2_amount', 
        'basal_profile_3_segment_3_start_time', 'basal_profile_3_segment_3_amount', 'basal_profile_3_segment_4_start_time', 
        'basal_profile_3_segment_4_amount', 'basal_profile_3_segment_5_start_time', 'basal_profile_3_segment_5_amount', 
        'basal_profile_3_segment_6_start_time', 'basal_profile_3_segment_6_amount', 'basal_profile_3_segment_7_start_time', 
        'basal_profile_3_segment_7_amount', 'basal_profile_3_segment_8_start_time', 'basal_profile_3_segment_8_amount', 
        'basal_profile_3_segment_9_start_time', 'basal_profile_3_segment_9_amount', 
        'basal_limit', 'time_format', 'bg_reminder_enable', 'bg_reminder_time', 
        'low_insulin_enable', 'low_insulin_level', 'notification_timing', 'delivery_limit', 
        'auto_off_enable', 'auto_off_duration', 'pump_reminder_enable', 'pump_reminder_hours', 
        'target_bg_min', 'target_bg_max', 'beep_volume', 'theme_color', 'button_guard_enable', 
        'splash_screen_enable', 'flashlight_enable', 'temp_basal_button_enable', 'screen_timeout', 
        'splash_screen_text_line_1', 'splash_screen_text_line_2', 'splash_screen_text_line_3')

class RecordTimeManagerData(AsanteRecord):
    max_nr_of_instances = 1
    skip_if_no_time_edit = False
    tag = 'SECTION10'
    format = ('HHIIH',)
    variables = ('section', 'crc', 'rtc_at_set_time', 'user_set_time', 'user_time_flag')

class RecordDeviceInfo(AsanteRecord):
    max_nr_of_instances = 1
    skip_if_no_time_edit = False
    tag = 'SERIAL'
    format = ('4sB11sBHB',)
    variables = ('model', 'sep1', 'serial', 'sep2', 'pump_record_version', 'sep3')
    date_time = DATE_TIME_1ST # This record must come first (Generic.py)

    def convert_to_diasend(self):
        return {ELEM_DEVICE_SERIAL: self.serial}

# --- controller 

class AsanteDataExtractor(object):

    def __init__(self, data):
        self.db                              = []   # 'Database' of all records.
        self.usertime_n                      = None # User time from last user time set event.
        self.rtc_n                           = None # RTC from last user time set event.
        self.previous_basal_record           = None
        self.stored_missed_basal_records     = []
        self.current_temp_basal_end_datetime = None # End date-time of 'currently' active temporary basal change.
                                                    # Set to None if no temporary basal change active.
        self.data                            = data
        self.ref_last_item_of_type           = {}   # Reference to last item of type

    def add_record(self, record):
        '''
        Add record to database. Records are always sorted according to 
        timestamp.
        '''
        if record.date_time == DATE_TIME_1ST:
            self.db.insert(0, record)
        else:
            self.db.insert(bisect.bisect_left(self.db, record), record)

    def extract_last_record_of_types(self):
        '''
        Store reference to the last (date) record of different types. 
        '''
        for db_index, db_record in enumerate(reversed(self.db)):
            if not type(db_record) in self.ref_last_item_of_type:
                self.ref_last_item_of_type[type(db_record)] = db_record

    def am_i_last_of_my_type(self, record):
        '''
        Is record the last instance of that type stored in the database?
        '''
        if type(record) in self.ref_last_item_of_type:
            return record == self.ref_last_item_of_type[type(record)]
        return False

    def print_record(self, r, indent=4):
        print '%s%s' % (' ' * indent, type(r))
        indent += 4
        for key, value in sorted(r.__dict__.items()):
            if key == 'raw_buffer':
                continue
            if isinstance(value, BaseRecord):
                print_record(value, indent+4)
            else:
                print '%s%s = %s' % (' ' * indent, key, repr(value))

    def search_instance(self, cls):
        '''
        Extract a list of all instances in the record database
        of the specified class.
        '''
        result = []
        for record in self.db:
            if isinstance(record, cls):
                result.append(record)
        return result

    def search_instance_with_key_value(self, cls, key, value):
        '''
        Extract a list of all instances in the record database
        that is of the specified class _and_ which as a member 
        variable named 'key' with value 'value'.
        '''
        result = []
        for record in self.db:
            if isinstance(record, cls):
                if key in record.__dict__:
                    if record.__dict__[key] == value:
                        result.append(record)
        return result

    def check_max_nr_of_instances(self):
        '''
        Validate the number of extraced records. Each record type has a maximum number
        allowed record instances.
        '''
        for sub_class in AsanteRecord.__subclasses__():
            nr_of_instances = len([ins for ins in self.db if isinstance(ins, sub_class)])
            if nr_of_instances > sub_class.max_nr_of_instances:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Max number of instances exceeded for class %s' % (str(sub_class)))

    def split(self):

        # Assume old format and switch to new if we get an updated pump revision. 
        AsanteRecord.use_old_format()

        # The buffer looks something like this. 
        # 
        # 00000000  21 54 41 47 21 53 45 52  49 41 4c 21 7e 01 14 00  |!TAG!SERIAL!~...|
        # 00000010  53 4e 41 50 00 43 4d 31  33 30 31 37 30 30 31 33  |SNAP.CM130170013|
        # 00000020  00 00 42 00 14 22 21 54  41 47 21 53 45 43 54 49  |..B.."!TAG!SECTI|
        # 00000030  4f 4e 30 30 21 7e 08 1e  00 00 00 81 77 86 22 a3  |ON00!~......w.".|
        # ...
        # 000000d0  0a af 02 00 00 06 00 10  00 10 00 00 00 f3 44 a3  |..............D.|
        # 000000e0  0a 00 00 00 00 00 00 ea  fc 21 54 41 47 21 53 45  |.........!TAG!SE|
        # 000000f0  43 54 49 4f 4e 30 31 21  7e 08 26 00 01 00 45 2b  |CTION01!~.&...E+|
        # 00000100  74 35 a3 0a e9 01 00 00  03 00 b0 04 02 00 40 ff  |t5............@.|
        # 
        # Start by splitting the in-data using the tag-header '!TAG!'. The likelihood to 
        # find this string in the payload is at least 256 ^ 5 = 1.0995116278×10^12 = 1 TB. 
        # 
        sections = self.data.split('!TAG!')

        # Since the data shall start with a !TAG! the first split result should be 
        # empty. 
        if len(sections) == 0 or len(sections[0]) > 0:
            len_sections_str = '0' if len(sections) == 0 else '%d - %d' % (len(sections), len(sections[0]))
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Split data - Incorrect number of sections (%s)' % (len_sections_str))
        
        for section in sections[1:]:

            # The section looks something like this. 
            # 
            # 00000000  53 45 52 49 41 4c 21 7e  01 14 00 53 4e 41 50 00  |SERIAL!~...SNAP.|
            # 00000010  43 4d 31 33 30 31 37 30  30 31 33 00 00 42 00 14  |CM130170013..B..|
            # 
            # Look for the section title by splitting the buffer by the first '!'.
            # 
            tag, body = section.split('!', 1)

            buffer = Buffer(body)

            # Header 4 bytes + CRC 2 bytes
            
            # This loop will deconstruct the Asante frame format (e.g. checksum calculation)
            # and create an AsanteRecord instance for the payload in each frame (every frame
            # contains a single Asante record).
            # 
            # Asante frame header 4 bytes
            # Asante crc tail 2 bytes
            while buffer.nr_bytes_left() >= 6:

                # Set the buffer mark - this is used to extract the complete frame later
                # for crc calculation.
                buffer.set_mark()

                # Extract the Asante frame header (SOF 0x7E, descriptor, frame length)
                header = buffer.get_struct('<BBH')

                if header[0] != 0x7e:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Split data - frame does not start with SOF')
                # 0x01 is a response to the device info command (serial number) and 0x08 is
                # a response to the request individual record command. 
                if not header[1] in [0x01, 0x08]:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Split data - unknown descriptor 0x%02x' % (header[1]))
                data_len = header[2]

                # Create the AsanteRecord instance. The result is stored in the AsanteRecord
                # db (static variable in the AsanteRecord class) for later retrieval. 
                # An exception will be raised if validation fails (e.g. unknown enum value or
                # too little data).
                record = AsanteRecord.create(self, tag, buffer)

                if tag == "SERIAL":
                    if record.pump_record_version >= 0x4800:
                        AsanteRecord.use_new_format()

                # Some records (responses to request individual command) includes a second
                # checksum. Let's validate that as well.
                if 'crc' in record.__dict__:
                    # Magic number 4 - skip section and crc at the beginning of the 
                    # message.
                    calculated_crc = Checksum.calculate(record.get_raw_buffer()[4:])
                    if calculated_crc != record.crc:
                        raise Exception(ERROR_CODE_CHECKSUM, 'Split data - record (not frame) checksum failed (0x%x != 0x%x) in tag %s' % (calculated_crc, record.crc, record.tag))

                # Note that the create function in the AsanteRecord class has consumed the 
                # payload of the message. The following command now give as the message from 
                # SOF up to (but not including) the checksum.
                buffer_for_crc = buffer.get_slice_from_mark()

                # Now extract the checksum from the buffer.
                crc = buffer.get_struct('<H')[0]

                if Checksum.calculate(buffer_for_crc) != crc:
                    raise Exception(ERROR_CODE_CHECKSUM, 'Split data - frame checksum failed (0x%x != 0x%x) in tag %s' % (Checksum.calculate(buffer_for_crc), crc, record.tag))

        if buffer.nr_bytes_left() != 0:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Split data - left over data %d bytes' % (buffer.nr_bytes_left()))

        # Build cache of last (date) record of different types.
        self.extract_last_record_of_types()

        # Check maximum number of allowed instances per record type.
        self.check_max_nr_of_instances()

        tdd_dict = {} # TDD dict - key date - value list [basal, bolus]

        # Now it is time to convert all records to the format known by Database.py. This is hard to
        # do on the fly (since some records depends on others).
        diasend_records = []
        for record in self.db:

            # Some records must be skipped if we haven't got a time edit record yet (without)
            # the time edit record we can't reproduce the user time. 
            if (self.usertime_n == None or self.rtc_n == None) and record.skip_if_no_time_edit:
                continue

            record.update_datetime()
            record_or_list = record.convert_to_diasend()

            # Since the pump doesn't have any TDD records we have to collect this manually. 
            if isinstance(record, RecordBasal):
                if not record.my_date_time.date() in tdd_dict:
                    tdd_dict[record.my_date_time.date()] = [0, 0]
                tdd_dict[record.my_date_time.date()][0] += record.clicks_delivered
            elif isinstance(record, RecordBolus):
                if not record.my_date_time.date() in tdd_dict:
                    tdd_dict[record.my_date_time.date()] = [0, 0]
                tdd_dict[record.my_date_time.date()][1] += record.clicks_delivered

            # Some records are for Asante internal use only and is not inserted into the diasend 
            # database.
            if record_or_list != None:
                if type(record_or_list) is list:
                    [diasend_records.append(record) for record in record_or_list]
                else:
                    diasend_records.append(record_or_list)

        # Time to create the TDD records
        for day, tdd in tdd_dict.items():

            record = {}

            basal = tdd[0] * ASANTE_TO_DIASEND_BASAL_FACTOR * (VAL_FACTOR_TDD / VAL_FACTOR_BASAL)
            bolus = tdd[1] * ASANTE_TO_DIASEND_BOLUS_FACTOR * (VAL_FACTOR_TDD / VAL_FACTOR_BOLUS)

            record[ELEM_VAL_TYPE]   = VALUE_TYPE_INS_TDD
            record[ELEM_TIMESTAMP]  = datetime.combine(day, datetime.min.time())
            record[ELEM_VAL]        = basal + bolus

            add_record_value(record, VALUE_TYPE_INS_BASAL_TDD, basal)

            diasend_records.append(record)

        return diasend_records

    # --- generic api

    @staticmethod
    def eval_serial_record(record):
        if ELEM_DEVICE_SERIAL in record:
            return record

        return {}

    @staticmethod
    def eval_unit_record(record):
        return {ELEM_DEVICE_UNIT : 'mg/dl'}

    @staticmethod
    def eval_result_record(record):
        if any(key in record for key in (ELEM_VAL_TYPE, PROGRAM_TYPE, SETTINGS_LIST)):
            return record

        return {}

    @staticmethod
    def eval_checksum_record(row, record):
        return True

    @staticmethod
    def eval_nr_results_record(record):
        return {}

    @staticmethod
    def eval_device_model_record(record):
        return {ELEM_DEVICE_MODEL:'Asante Snap'}

# --- external api

def DetectAsanteSnap(in_list):
    """
    Detect if data comes from a Asante Snap.
    """
    return DetectDevice( 'AsanteSnap', in_list, DEVICE_PUMP  );

def AnalyseAsanteSnap(data):

    callbacks = {}
    callbacks["eval_serial_record"]       = AsanteDataExtractor.eval_serial_record
    callbacks["eval_unit"]                = AsanteDataExtractor.eval_unit_record
    callbacks["eval_result_record"]       = AsanteDataExtractor.eval_result_record
    callbacks["eval_checksum_record"]     = AsanteDataExtractor.eval_checksum_record
#    callbacks["eval_nr_results"]          = AsanteDataExtractor.eval_nr_results_record
    callbacks["eval_device_model_record"] = AsanteDataExtractor.eval_device_model_record

    try:
        extractor = AsanteDataExtractor(data)
        diasend_records = extractor.split()

    except Exception, e:

        import traceback
        traceback.print_exc()
        
        serial_records = extractor.search_instance(RecordDeviceInfo)
        if len(serial_records) == 1:
            serial_number = serial_records[0].serial
        else:
            serial_number = 'UNKNOWN'

        # The 'standard' way is to raise Exception with two parameters, diasend error code and 
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList('Asante Snap', serial_number, e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList('Asante Snap', serial_number, ERROR_CODE_PREPROCESSING_FAILED, 
                ''.join(map(str, e.args)))

        return error_response

    result = AnalyseGenericMeter(diasend_records, callbacks)

    return result

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    #data = open('test/testcases/test_data/AsanteSnap/CM122670019.log', 'r').read()
    #data = open('test/testcases/test_data/AsanteSnap/CM132670018.log', 'r').read()
    data = open('test/testcases/test_data/AsanteSnap/CM133010022.log', 'r').read()

    results = AnalyseAsanteSnap(data)

    for result in results[0]['results']:
        try:
            print result
        except Exception, e:
            pass

    #print result
