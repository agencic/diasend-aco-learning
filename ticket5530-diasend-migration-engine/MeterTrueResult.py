# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011-2015 Diasend AB, Sweden, http://www.diasend.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------

# @DEVICE Nipro Diagnostics TRUEresult
# @DEVICE Nipro Diagnostics TRUEyou
# @DEVICE Nipro Diagnostics TRUEyou mini
# @DEVICE Nipro Diagnostics TRUE Metrix
# @DEVICE Nipro Diagnostics TRUE Metrix Air

import re
from Generic import *
from Defines import *
from datetime import datetime

# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------

g_overall_checksum = 0

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def CalcFrameChecksum(frame):
    """
    Compute the checksum by totaling up the ASCII value of each character in the frame,
    converting to hex and taking the last two characters. The frame starts with '('
    and ends with ')' followed by the checksum (2 characters).

    This functions performs the checksum calculation but does _not_ truncate the 
    result (it is returned as an integer).
    """
    checksum = 0
    for char in frame:
        checksum += ord(char)
    return checksum

def SplitData( inData ):
    """
    Split incoming data into separate records.
    """
    return inData.split("\r\n")

def EvalTrueResultSerialRecord(line):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}

    if line.startswith("(z1"):
        res[ELEM_DEVICE_SERIAL] = line.strip("(z1").split(")")[0]

    return res

def EvalTrueResultModelRecord(line):
    """
    Is this line a model record. If it is return the model name. 
    """
    res = {}
    if line.startswith("(iMAR)9A"):
        res[ELEM_DEVICE_MODEL] = "TRUEresult/TRUEyou"
    elif line.startswith("(iMIN)9E"):
        res[ELEM_DEVICE_MODEL] = "TRUEyou mini"
    elif line.startswith("(iBLU)9D"):
        res[ELEM_DEVICE_MODEL] = "TRUE METRIX AIR"
    elif line.startswith("(iMR2)8B"):
        res[ELEM_DEVICE_MODEL] = "TRUE METRIX"

    return res

def EvalTrueResultTestResultCommon(line):
    """
        Parses the common parts of the test result records,
        refer to the comment per function below
    """
    res = {}

    try:

        def ConvertH2I(s):
            """
            Convert a hex string (e.g. 8B7) to a four byte 
            integer string. 
            """
            return "%04d" % (int(s, 16))

        month           = int(line[0:1], 16)
        # Convert a hex string to an integer string and 
        # split it, e.g. 89F -> 2207 -> day = 22 year = 07. 
        day             = int(ConvertH2I(line[1:4])[0:2])
        year            = int(ConvertH2I(line[1:4])[2:4])
        hour            = int(ConvertH2I(line[4:7])[0:2])
        minute          = int(ConvertH2I(line[4:7])[2:4])
        glucose_r       = int(line[7:10], 16)
        glucose         = glucose_r & 0x7ff;
        alt_site_flag   = (glucose_r & 0x800) != 0

        # Check limits defined by specification.
        if (0 <= year <= 99) and \
           (1 <= month <= 12) and \
           (1 <= day <= 31) and \
           (0 <= hour <= 23) and \
           (0 <= minute <= 59) and \
           (0 <= glucose <= 2047):

            res[ELEM_TIMESTAMP]     = datetime(2000 + year, month, day, hour, minute)
            res[ELEM_VAL]           = glucose
            res[ELEM_VAL_TYPE]      = VALUE_TYPE_GLUCOSE
            res[ELEM_DEVICE_UNIT]   = "mg/dL"

            res[ELEM_FLAG_LIST] = []

            if glucose < 20:
                res[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
            elif glucose > 600:
                res[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)

            if alt_site_flag:
                res[ELEM_FLAG_LIST].append(FLAG_RESULT_ALT_TESTSITE)
        else:
            res = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":""}

    except IndexError:
        res = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":""}

    return res


def EvalTrueResultTestResultWithEventTagsRecord(line):
    """
    Convert a TrueResult record with event tags to the analyzer format. 

    The response will be in the format “zgndddddddddd” where n+1 ASCII bytes
    indicates the number of ASCII hex data bytes in the frame. Glucose test
    results are stored in a 6 byte packed data frame which is represented by the
    following example
        (zg5MMMMQQQQQQQQQQQQTTTTTTTTTTTTAGGGGGGGGGGGEEEEEEEE)xx

        Byte 0: MMMM QQQQ
        Byte 1: QQQQ QQQQ
        Byte 2: TTTT TTTT
        Byte 3: TTTT AGGG
        Byte 4: GGGG GGGG
        Byte 5: EEEE EEEE

        M - Month 4-bits 1-12 (0x0C max) 
        Q - Packed day and year 3 nibbles 0101-3199 (H'C7F max)
            First 2 digits represent day of month 01-31
            Second 2 digits represent year 00-99 (2000-2099) 
        T - Time 3 nibbles 0000-2359 (H'937 max) 
        A - Is a 1 bit flag to indicate if a result is a Morning Average value for TRUEtrack, 
            TRUEread and TRUEbalance, or if the result is an Alternate Site value for TRUEresult. 
            In either case this bit must be stripped out prior to converting to a glucose value. 
        G - Glucose value, maximum value is 2047 mg/dL i
        E - Event marker/control indication
                0x00 No event marker; indicates Blood test result
                0x01 Pre-meal event marker; indicates a Blood test result
                0x02 Post-meal event marker; indicates a Blood test result
                0x03 Exercise event marker; indicates a Blood test result
                0x04 Sick event marker; indicates a Blood test result
                0x05 Medication event marker; indicates a Blood test result
                0x06 Other event marker; indicates a Blood test result
                0x80 Indicates the a Glucose Control Test Result

    Notes: Conversion of day & year - convert to decimal and prepend 0 if less than 3 characters, 
           then first 2 characters = day and last 2 characters + 2000 = year
           Conversion of hours & minutes - convert to decimal and prepend 0 if less than 3 
           characters, first two characters are hours in military time and last two characters are minutes.
    """

    res = EvalTrueResultTestResultCommon(line[4:14])

    try:
        event_marker = int(line[14:16], 16)

        # Check limits defined by specification.
        if (event_marker & ~0x80) < 7:
            if event_marker & 0x80:
                res[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL)

            if event_marker == 1:
                res[ELEM_FLAG_LIST].append(FLAG_BEFORE_MEAL)
            elif event_marker == 2:
                res[ELEM_FLAG_LIST].append(FLAG_AFTER_MEAL)
            elif event_marker == 3:
                res[ELEM_FLAG_LIST].append(FLAG_DURING_EXERCISE)
            elif event_marker == 4:
                res[ELEM_FLAG_LIST].append(FLAG_ILLNESS)
            elif event_marker == 5:
                res[ELEM_FLAG_LIST].append(FLAG_MEDICATION)
            elif event_marker == 6:
                res[ELEM_FLAG_LIST].append(FLAG_OTHER)
        else:
            res = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":""}

    except IndexError:
        res = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":""}

    return res

def EvalTrueResultTestResultRecord(line):
    """
    Convert a TrueResult record to the analyzer format. 

    The response will be in the format "gnddd...ddd" where n+1 ASCII bytes indicates the
    number of ASCII hex data bytes in the frame. Glucose test results are stored in a 6 
    byte packed data frame which is represented by the following example 

        (g5MMMMQQQQQQQQQQQQTTTTTTTTTTTTAGGGGGGGGGGGSSSSSSSS)xx

        Byte 0: MMMM QQQQ
        Byte 1: QQQQ QQQQ
        Byte 2: TTTT TTTT
        Byte 3: TTTT AGGG
        Byte 4: GGGG GGGG
        Byte 5: SSSS SSSS

        M - Month 4-bits 1-12 (0x0C max) 
        Q - Packed day and year 3 nibbles 0101-3199 (H'C7F max)
            First 2 digits represent day of month 01-31
            Second 2 digits represent year 00-99 (2000-2099) 
        T - Time 3 nibbles 0000-2359 (H'937 max) 
        A - Is a 1 bit flag to indicate if a result is a Morning Average value for TRUEtrack, 
            TRUEread and TRUEbalance, or if the result is an Alternate Site value for TRUEresult. 
            In either case this bit must be stripped out prior to converting to a glucose value. 
        G - Glucose value, maximum value is 2047 mg/dL i
        S - 30h (ASCII 0) indicates the test is a Blood Test Result
          - 32h (ASCII 2) indicates the test is a Glucose Control Test Result

    Notes: Conversion of day & year - convert to decimal and prepend 0 if less than 3 characters, 
           then first 2 characters = day and last 2 characters + 2000 = year
           Conversion of hours & minutes - convert to decimal and prepend 0 if less than 3 
           characters, first two characters are hours in military time and last two characters are minutes.
    """

    res = EvalTrueResultTestResultCommon(line[3:13])

    try:
        glucose_type = int(line[13:15], 16)

        # Check limits defined by specification.
        if glucose_type in [0x30, 0x32]:
            if glucose_type == 0x32:
                res[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL)
        else:
            res = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":""}

    except IndexError:
        res = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":""}

    return res

def EvalTrueResultRecord(line):
    """
        There are two result records available, the one starting with g5
        which is an ordinary result record from the older devices.
        There is a new variant also containing event tags which starts with zg5
    """
    if line.startswith("(g5"):
        return EvalTrueResultTestResultRecord(line)
    if line.startswith("(zg5"):
        return EvalTrueResultTestResultWithEventTagsRecord(line)
    else:
        return {}

def EvalTrueResultChecksumFile(records):
    """
    This function calculates checksums on all (not only glucose) records. It also 
    validates the overall checksum for all glucose records. 
    """
    global g_overall_checksum

    ret = False

    for r in records:

        if len(r) == 0:
            continue

        # A wider re-expression to handle all frames.
        #
        # m group 1 -> entire frame except checksum (for single frame crc check)
        # m group 2 -> frame crc
        m = re.match(r'(\(.*\))([0-9a-fA-F]{2,2})', r, re.IGNORECASE)
        if m:
            # Here we check the checksum for a single frame. 
            # The checksum is converted to hex and truncated to 2 character!
            checksum_string = ("%02x" % (CalcFrameChecksum(m.group(1))))[-2:]
            if checksum_string.upper() != m.group(2).upper():
                break
        else:
            # An invalid record!
            break

        if r.startswith("(x1"):
            # Here we check the checksum for all frames.
            # This checksum is converted to hex and truncated to 4 characters!
            overall_checksum_string = ("%04x" % (g_overall_checksum))[-4:]
            overall_checksum_from_meter = r[3:7]
            if overall_checksum_string.upper() == overall_checksum_from_meter.upper():
                ret = True
            break

    return ret 

def EvalTrueResultChecksumRecord(line, record):
    """
    Add checksum for the glucose records to the global overall checksum.
    """
    global g_overall_checksum

    # Only match glucose records. This function should only receive glucose 
    # records so a non-match here is an error. 
    #
    # m group 1 -> payload (for overall crc check).
    # m group 2 -> frame crc
    m = re.match(r'\(z{0,1}g5([0-9a-fA-F]{12,12})\)([0-9a-fA-F]{2,2})', line, re.IGNORECASE)
    if m:
        # Calculate overall checksum on payload only. 
        g_overall_checksum += CalcFrameChecksum(m.group(1))
    else:
        return False

    return True

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectTrueResult(inList):
    """
    Detect if data comes from a TrueResult.
    """
    return DetectDevice( 'TrueResult', inList, DEVICE_METER );

def AnalyseTrueResult(inData):
    """
    Analyse TrueResult.
    """

    global g_overall_checksum
    g_overall_checksum = 0

    # Empty dictionary
    d = {}

    d[ "eval_serial_record" ]        = EvalTrueResultSerialRecord
    d[ "eval_device_model_record" ]  = EvalTrueResultModelRecord
    d[ "eval_result_record" ]        = EvalTrueResultRecord
    d[ "eval_checksum_record" ]      = EvalTrueResultChecksumRecord
    d[ "eval_checksum_file" ]        = EvalTrueResultChecksumFile

    inList = SplitData(inData)
    resList = AnalyseGenericMeter( inList, d );

    return resList

if __name__ == "__main__":

#    testcase = open('test/testcases/test_data/TrueResult/TrueyouMini.log')
#    testcase = open('test/testcases/test_data/TrueResult/TrueMetrix.log')
#    testcase = open('test/testcases/test_data/TrueResult/TrueMetrixAir.log')
    testcase = open('test/testcases/test_data/TrueResult/TrueMetrixAir-empty.log')

    ret = AnalyseTrueResult(testcase.read())
    print ret
