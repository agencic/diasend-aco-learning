# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, Sweden, http://diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
#
#_/  |______    ____    __| _/____   _____   _/  |_  /\   _____|  | |__| _____
#\   __\__  \  /    \  / __ |/ __ \ /     \  \   __\ \/  /  ___/  | |  |/     \
# |  |  / __ \|   |  \/ /_/ \  ___/|  Y Y  \  |  |   /\  \___ \|  |_|  |  Y Y  \
# |__| (____  /___|  /\____ |\___  >__|_|  /  |__|   \/ /____  >____/__|__|_|  /
#           \/     \/      \/    \/      \/                  \/              \/
#
# -----------------------------------------------------------------------------

# @DEVICE Tandem t:slim
# @DEVICE Tandem t:slim X2

import logging

from Generic import *
from Defines import *

from Buffer import Buffer
from Record import BaseRecord, EnumRecord, EnumBitRecord
from datetime import time

DEVICE_MODEL_TSLIM          = "Tandem t:slim"
DEVICE_MODEL_TFLEX          = "Tandem t:flex"
DEVICE_MODEL_TSLIM_G4       = "Tandem t:slim G4"
DEVICE_MODEL_TSLIM_X2       = "Tandem t:slim X2"

HEADER_SIZE = 3
START_OF_FRAME  = 0x55

def timedelta_total_seconds(delta):
    '''
    datetime.timedelta.total_seconds only appeared in python 2.7.
    '''
    return (delta.microseconds + (delta.seconds + delta.days * 24 * 3600) * 10**6) / 10**6

class EnumEnableDisable(EnumRecord):
    format = ('B',)
    enums  = {
        'disable'   : 0,
        'enable'    : 1
    }

    map_enum_to_diasend = {'disable': 0, 'enable': 1}

class EnumYesNo(EnumRecord):
    format = ('B',)
    enums  = {
        'no'    : 0,
        'yes'   : 1
    }

    map_enum_to_diasend = {'no': 0, 'yes': 1}

class EnumBolusEntryType(EnumRecord):
    format = ('B',)
    enums  = {
        'insulin'   : 0,
        'carbs'     : 1
    }

    map_enum_to_diasend = {'insulin': 0, 'carbs': 1}

class EnumQuickBolusStatus(EnumBitRecord):
    format = ('B',)
    enums  = {
        'active'            : 1,
        'increment_carbs'   : 2,
        'increment_units'   : 4,
        'data_entry_type'   : 8
    }

class EnumAnnun(EnumRecord):
    format = ('B',)
    enums  = {
        'audio_high'  : 0,
        'audio_med'   : 1,
        'audio_low'   : 2,
        'vibe'        : 3
    }

class EnumBolusType(EnumRecord):
    format = ('B',)
    enums  = {
        'standard_bolus'    : 0,
        'extended_bolus'    : 1,
        'quick_bolus'       : 2
    }

class EnumPumpStatus(EnumBitRecord):
    format = ('H',)
    enums  = {
        'low_insulin_thshld'    : 1,
        'auto_shutdown_enabled' : 2,
        'auto_shutdown_duration': 4,
        'cannula_prime_size'    : 8,
        'is_pump_locked'        : 16,
        'reserved0'             : 32,
        'oled_timeout'          : 64,
        'reserved1'             : 128,
        'reserved2'             : 256
    }

class EnumDays(EnumBitRecord):
    format = ('B',)
    enums  = {
        'monday'    : 1,
        'tuesday'   : 2,
        'wednesday' : 4,
        'thursday'  : 8,
        'friday'    : 16,
        'saturday'  : 32,
        'sunday'    : 64
    }

class EnumReminderStatus(EnumBitRecord):
    format = ('B',)
    enums  = {
        'frequency_minutes' : 1,
        'enabled'           : 2,
        'start_time'        : 4,
        'end_time'          : 8,
        'active_days'       : 16
    }

class EnumRemindSettingsStatus(EnumBitRecord):
    format = ('B',)
    enums  = {
        'low_bg_thshld'     : 1,
        'high_bg_thshld'    : 2,
        'site_change_days'  : 4
    }

class EnumIDPSegmentStatus(EnumBitRecord):
    format = ('B',)
    enums  = {
        'basal_rate_availability'   : 1,
        'carb_ratio_availability'   : 2,
        'target_bg_availability'    : 4,
        'isf_availability'          : 8
    }
    # If a segment is unused all its data might be just 0xff and so the status
    ignore_not_defined_bits = True

class EnumBolusSettingStatus(EnumBitRecord):
    format = ('B',)
    enums  = {
        'insulin_duration'      : 1,
        'max_bolus'             : 2,
        'bolus_enter_method'    : 4
    }

class EnumBasalChangeType(EnumBitRecord):
    format = ('B',)
    enums  = {
        'timed_segment'      : 1,
        'new_profile'        : 2,
        'temp_rate_start'    : 4,
        'temp_rate_end'      : 8,
        'pump_suspended'     : 16,
        'pump_resumed'       : 32,
        'pump_shutdown'      : 64
    }
    map_enum_to_diasend = {'timed_segment': None, 'new_profile': None,
        'temp_rate_start': FLAG_TEMP_MODIFIED, 'temp_rate_end': None,
        'pump_suspended': FLAG_SUSPENDED, 'pump_resumed': None,
        'pump_shutdown': None
    }

# Table 5 from the spec
class StructIDPSegment(BaseRecord):
    format = ('2HI2H', EnumIDPSegmentStatus)
    variables = ('start_time', 'basal_rate', 'carb_ratio', 'target_bg', 'isf',
        'status')

# Table 6 from the spec
class StructBolusSetting(BaseRecord):
    format = ('2H', EnumBolusEntryType, EnumBolusSettingStatus)
    variables = ('insulin_duration', 'max_bolus', 'carb_entry', 'status')

# Table 7 from the spec
class StructQuickBolusSetting(BaseRecord):
    format = (EnumEnableDisable, '2H', EnumBolusEntryType, EnumQuickBolusStatus)
    variables = ('active', 'increment_units', 'increment_carbs',
        'data_entry_type', 'status')

# Table 8 from the spec
class StructReminder(BaseRecord):
    format = ('I2H', EnumDays, EnumEnableDisable, EnumReminderStatus)
    variables = ('frequency', 'start_time', 'end_time', 'active_days',
        'enabled', 'status')

class StructLogEntryHeader(BaseRecord):
    format = ('I2H3I')
    variables = ('index', 'reserved', 'id', 'timestamp', 'seq_num', 'spare')

class TandemRecord(BaseRecord):
    '''
    Base class for all Tandem records.

    This class holds states used during the interpretation of the
    raw buffer from the terminal; including the record database
    of all records extracted up to this point.

    This class also contains a factory method for creating
    instances of the sub-classes (depending on the currently extracted
    message type).

    '''
    @classmethod
    def create(cls, msg_type, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        for sub_class in cls.__subclasses__():
            if sub_class.msg_type == msg_type:
                create_op = getattr(sub_class, "create_obj", None)
                if callable(create_op):
                    return create_op(buf, payload_len)
                else:
                    return sub_class(buf, payload_len)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'TandemRecord : unknown message type %s' % (msg_type))

    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'TandemRecord : Subclass did not implement convert %s')

class RecordVersion(TandemRecord):
    msg_type = 81
    format = ('16s16s7I8s2I8s16s16s16sH2IH',)
    variables = ('arm_sw_ver', 'msp_sw_ver', 'reserved0', 'reserved1',
        'reserved2', 'reserved3', 'reserved5', 'pump_sn', 'pump_part_num',
        'pump_rev', 'pcba_sn', 'spare', 'pcba_rev', 'sparea', 'spareb',
        'sparec', 'reserved6', 'model_number', 'timestamp', 'checksum')

    def __init__(self, buf, length):
        # Add 6 for the timestamp and CRC which is provided only for this record
        # from the transmitter..
        super(RecordVersion, self).__init__(buf, length + 6)

    def convert_to_diasend(self):
        models = {
            4628    : DEVICE_MODEL_TSLIM,
            4628001 : DEVICE_MODEL_TSLIM,
            4628004 : DEVICE_MODEL_TSLIM,
            5448    : DEVICE_MODEL_TSLIM,
            5448001 : DEVICE_MODEL_TSLIM,
            5448004 : DEVICE_MODEL_TSLIM,
            4628005 : DEVICE_MODEL_TFLEX,
            5448005 : DEVICE_MODEL_TFLEX,
            4628003 : DEVICE_MODEL_TSLIM_G4,
            5448003 : DEVICE_MODEL_TSLIM_G4,
            1000096 : DEVICE_MODEL_TSLIM_X2,
            1000354 : DEVICE_MODEL_TSLIM_X2,
        }

        ret = [{ELEM_DEVICE_SERIAL: self.pump_sn}]
        if self.model_number in models:
            ret.append({ELEM_DEVICE_MODEL: models[self.model_number]})

        return ret

class RecordGlobals(TandemRecord):
    msg_type = 179
    format = (StructQuickBolusSetting, EnumAnnun, EnumAnnun, EnumAnnun,
        EnumAnnun, EnumAnnun, EnumAnnun)
    variables = ('quick_bolus_settings', 'button_annun', 'quick_bolus_annun',
        'bolus_annun', 'reminder_annun', 'alert_annun', 'alarm_annun')

    def convert_to_diasend(self):
        ret = {}
        qb = self.quick_bolus_settings

        # The 'status' field of the quick bolus settings only means
        # 'changed from the default value', so we'll use the contents
        # of the settings to decide storage logic
        if qb.active.to_text() == 'enable':
            ret[SETTING_TANDEM_QB_ACTIVE] = qb.active.to_diasend()

            if qb.data_entry_type.to_text() == 'insulin':
                ret[SETTING_TANDEM_QB_INCREMENT_UNITS] = qb.increment_units
            else:
                ret[SETTING_TANDEM_QB_INCREMENT_CARBS] = qb.increment_carbs

            ret[SETTING_TANDEM_QB_DATA_ENTRY_TYPE] = qb.data_entry_type.to_diasend()

        ret[SETTING_TANDEM_BUTTON_ANNUN]        = self.button_annun.value
        ret[SETTING_TANDEM_QUICK_BOLUS_ANNUN]   = self.quick_bolus_annun.value
        ret[SETTING_TANDEM_BOLUS_ANNUN]         = self.bolus_annun.value
        ret[SETTING_TANDEM_REMINDER_ANNUN]      = self.reminder_annun.value
        ret[SETTING_TANDEM_ALERT_ANNUN]         = self.alert_annun.value
        ret[SETTING_TANDEM_ALARM_ANNUN]         = self.alarm_annun.value

        return {
            SETTINGS_LIST: ret
        }

class RecordPumpSettings(TandemRecord):
    msg_type = 182
    format = ('3B4H', EnumEnableDisable, '3B11sB',
        EnumPumpStatus)
    variables = ('low_insulin_tshld', 'cannula_prime_size',
        'auto_shutdown_enabled', 'auto_shutdown_duration', 'reserved0',
        'bolus_number', 'temp_rate_number', 'is_pump_locked', 'reserved1',
        'oled_timeout', 'reserved2', 'reserved3', 'reserved4', 'status')

    def convert_to_diasend(self):
        ret = {}

        # Note, the status bits only means "changed from default value", and need not to be used
        bits = self.status.bits

        # Save all the values from the settings
        ret[SETTING_LOW_INSULIN_LEVEL] = self.low_insulin_tshld
        ret[SETTING_TANDEM_CANNULA_PRIME_SIZE] = self.cannula_prime_size
        ret[SETTING_AUTO_OFF_ENABLE] = self.auto_shutdown_enabled
        ret[SETTING_AUTO_OFF_TIMEOUT] = self.auto_shutdown_duration

        ret[SETTING_TANDEM_PUMP_LOCKED] = self.is_pump_locked.to_diasend()

        ret[SETTING_DISPLAY_TIMEOUT] = self.oled_timeout
            
        # Note: Most-recent bolus and temp rate number settings are considered internal data (not user-set), so are not stored.

        return {
            SETTINGS_LIST: ret
        }

class RecordReminderSettings(TandemRecord):
    msg_type = 185
    format = (StructReminder, StructReminder, StructReminder, StructReminder,
        StructReminder, StructReminder, StructReminder, StructReminder,
        StructReminder, '2HB', EnumRemindSettingsStatus)

    variables = ('low_bg_reminder', 'high_bg_reminder', 'site_change_reminder',
        'missed_bolus_reminder_0', 'missed_bolus_reminder_1',
        'missed_bolus_reminder_2', 'missed_bolus_reminder_3', 'bg_reminder',
        'additional_bolus_reminder', 'low_bg_thshld', 'high_bg_thshld',
        'site_change_days', 'status')

    def convert_to_diasend(self):
        ret = {}

        reminders = [self.low_bg_reminder, self.high_bg_reminder,
            self.site_change_reminder, self.missed_bolus_reminder_0,
            self.missed_bolus_reminder_1, self.missed_bolus_reminder_2,
            self.missed_bolus_reminder_3, self.bg_reminder,
            self.additional_bolus_reminder]

        setting = SETTING_TANDEM_REMINDER_BASE
        for reminder in reminders:
            bits = reminder.status.bits
            if bits['frequency_minutes']:
                ret[setting] = reminder.frequency
            setting += 1
            if bits['start_time']:
                ret[setting] = reminder.start_time
            setting += 1
            if bits['end_time']:
                ret[setting] = reminder.end_time
            setting += 1
            if bits['active_days']:
                ret[setting] = reminder.active_days.value
            setting += 1
            if bits['enabled']:
                ret[setting] = reminder.enabled.to_diasend()
            setting += 1

        bits = self.status.bits

        if bits['site_change_days']:
            ret[SETTING_SITE_CHANGE_INTERVAL] = self.site_change_days

        if bits['low_bg_thshld']:
            ret[SETTING_BG_LOW_ALERT_LIMIT_MGDL] = self.low_bg_thshld

        if bits['high_bg_thshld']:
            ret[SETTING_BG_HIGH_ALERT_LIMIT_MGDL] = self.high_bg_thshld

        if ret:
            return {
                SETTINGS_LIST: ret
            }
        else:
            return None

class RecordIDPList(TandemRecord):
    msg_type = 174
    format = ('7B')
    variables = ('num_of_profile', 'slot1', 'slot2', 'slot3', 'slot4', 'slot5',
        'slot6')

    def convert_to_diasend(self):
        slots = [self.slot1, self.slot2, self.slot3, self.slot4, self.slot5,
            self.slot6]

        #the first slot is always containing the active program
        return {
            SETTINGS_LIST: {
                SETTING_BASAL_PROGRAMS_IN_UI    : self.num_of_profile
            }
        }

class RecordIDP(TandemRecord):
    msg_type = 176
    format = ('B17sB', StructIDPSegment, StructIDPSegment, StructIDPSegment,
        StructIDPSegment, StructIDPSegment, StructIDPSegment, StructIDPSegment,
        StructIDPSegment, StructIDPSegment, StructIDPSegment, StructIDPSegment,
        StructIDPSegment, StructIDPSegment, StructIDPSegment, StructIDPSegment,
        StructIDPSegment, StructBolusSetting)
    variables = ('idp', 'name', 'num_segments', 'seg0', 'seg1', 'seg2', 'seg3',
        'seg4', 'seg5', 'seg6', 'seg7', 'seg8', 'seg9', 'seg10', 'seg11',
        'seg12', 'seg13', 'seg14', 'seg15', 'bolus_settings')

    def get_name(self):
        """
            Get the name, and zap any null chars in the end
        """
        return self.name.rstrip('\0')

    def convert_to_diasend(self):
        segments = [self.seg0, self.seg1, self.seg2, self.seg3, self.seg4,
                     self.seg5, self.seg6, self.seg7, self.seg8, self.seg9,
                     self.seg10, self.seg11, self.seg12, self.seg13, self.seg14,
                     self.seg15]
        basal_periods = []
        ic_periods = []
        target_bg_periods = []
        isf_periods = []

        for i in range(self.num_segments):
            seg = segments[i]
            start_time = time(seg.start_time / 60, seg.start_time % 60)

            if seg.status.bits['isf_availability']:
                isf_periods.append((start_time, round(seg.isf * 1000.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1)))
            if seg.status.bits['basal_rate_availability']:
                basal_periods.append((start_time, seg.basal_rate))
            if seg.status.bits['target_bg_availability']:
                target_bg_periods.append((start_time, round(seg.target_bg * 1000.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1)))
            if seg.status.bits['carb_ratio_availability']:
                ic_periods.append((start_time, seg.carb_ratio * 0.001))

        result = [ {
                PROGRAM_TYPE    : PROGRAM_TYPE_BASAL,
                PROGRAM_NAME    : self.get_name(),
                PROGRAM_PERIODS : basal_periods
            }, {
                PROGRAM_TYPE    : PROGRAM_TYPE_IC_RATIO,
                PROGRAM_NAME    : self.get_name(),
                PROGRAM_PERIODS : ic_periods
            }, {
                PROGRAM_TYPE    : PROGRAM_TYPE_BG_TARGET,
                PROGRAM_NAME    : self.get_name(),
                PROGRAM_PERIODS : target_bg_periods
            }, {
                PROGRAM_TYPE    : PROGRAM_TYPE_ISF,
                PROGRAM_NAME    : self.get_name(),
                PROGRAM_PERIODS : isf_periods
            } ]

        return result

# This record is missing in the SPEC from Tandem. We receive it as response
# to a IDP list if the IDP list is not configured, so we can silently ignore it
class RecordCommandAck(TandemRecord):
    msg_type = 123
    format = ('H')
    variables = ('unknown')

    # This record is seen received as response to IDP programs that are not
    # programmed, so just return empty..
    def convert_to_diasend(self):
        return None

class RecordLogSize(TandemRecord):
    msg_type = 169
    format = ('3I')
    variables = ('num_entries', 'first_seq_num', 'last_seq_num')

    # Not much to do with this, we parse it but since we don't always
    # download all records we can not double check against it...
    def convert_to_diasend(self):
        return None

UNDOCUMENTED_LOG_ENTRIES = [9, 10, 17, 19, 22, 23, 24, 25, 29, 31, 32, 34, 35,
    41, 42, 43, 44, 52, 53, 58, 72, 88, 92, 98, 99, 100,
    134, 136, 146, 147, 148, 149, 150]
UNHANDLED_LOG_ENTRIES = [4, 5, 8, 26, 27, 28, 36, 37, 50, 67, 74]
SECONDS_2008 = timedelta_total_seconds(datetime(2008, 1, 1) - datetime(1970, 1, 1))

STATS = None

class RecordLogEntry(TandemRecord):
    msg_type = 125

    def __init__(self, buf, length):
        super(RecordLogEntry, self).__init__(buf, length)
        self.__header = None
        self.__dt = None

    def set_header(self, header):
        self.__header = header

    def get_timestamp(self):
        if not self.__dt and self.__header:
            self.__dt = datetime.utcfromtimestamp(SECONDS_2008 + \
                self.__header.timestamp)
        return self.__dt

    @classmethod
    def create_obj(cls, buf, payload_len):
        """
            Creates a instance of a RecordLogEntry subclass
        """
        global STATS
        buf.set_mark()
        header = StructLogEntryHeader(buf)
        payload_left = payload_len - buf.get_distance_to_mark()
        obj = None
        for sub_class in cls.__subclasses__():
            if sub_class.log_id == header.id:
                obj = sub_class(buf, payload_left)
                break

        # A lot of log types are undocumented...
        if not obj:
            if not header.id in UNDOCUMENTED_LOG_ENTRIES and \
                not header.id in UNHANDLED_LOG_ENTRIES:
                logging.debug("Unhandled record type: %d" % header.id)
            obj = RecordLogEntryUnparsed(buf, payload_left)

        if STATS != None:
            if not header.id in STATS:
                STATS[header.id] = 1
            else:
                STATS[header.id] += 1

        obj.set_header(header)

        return obj

class RecordCGMData(RecordLogEntry):
    log_id = 151
    format = ('2HI2H4B')
    variables = ('glucose_value_status', 'reserved0', 'timestamp', 'reserved1', 
        'currect_glucose_display_value', 'reserved2', 'reserved3', 'reserved4')

    def diasend_bg(self):
        return float(self.currect_glucose_display_value) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

    def convert_to_diasend(self):
        glucose_value_status_flags = {
            0: [FLAG_CONTINOUS_READING], 
            1: [FLAG_CONTINOUS_READING, FLAG_RESULT_HIGH], 
            2: [FLAG_CONTINOUS_READING, FLAG_RESULT_LOW], 
            3: [FLAG_CONTINOUS_READING, FLAG_CALIBRATION], 
            4: [FLAG_CONTINOUS_READING, FLAG_CALIBRATION, FLAG_RESULT_HIGH], 
            5: [FLAG_CONTINOUS_READING, FLAG_CALIBRATION, FLAG_RESULT_LOW]}

        if self.glucose_value_status in glucose_value_status_flags:
            return {
                ELEM_VAL_TYPE   : VALUE_TYPE_GLUCOSE,
                ELEM_VAL        : self.diasend_bg(),
                ELEM_FLAG_LIST  : glucose_value_status_flags[self.glucose_value_status],
                ELEM_TIMESTAMP  : self.get_timestamp()
            }

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Unknown glucose value status %d', self.glucose_value_status)

class RecordLogEntryUnparsed(RecordLogEntry):
    log_id = -1
    format = ('4I')
    variables = ('entry0', 'entry1', 'entry2', 'entry3')
    def convert_to_diasend(self):
        return None

class RecordLogErased(RecordLogEntry):
    log_id = 0
    format = ('4I')
    variables = ('num_erased', 'reserved0', 'reserved1', 'reserved2')

    def convert_to_diasend(self):
        return None

class RecordTempRateActivated(RecordLogEntry):
    log_id = 2
    format = ('2f2HI')
    variables = ('percent', 'duration', 'reserved0', 'temp_rate_id',
        'reserved1')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_VAL        : DIASEND_EVENT_TEMP_BASAL_ON,
            ELEM_TIMESTAMP  : self.get_timestamp(),
        }

class RecordBasalRateChange(RecordLogEntry):
    log_id = 3
    format = ('3fH', EnumBasalChangeType, 'B')
    variables = ('commanded_basal_rate', 'base_basal_rate', 'max_basal_rate',
        'idp', 'change_type', 'reserved')

    def convert_to_diasend(self):
        # We can detect shutdown here, but its no explicit alarm so
        # according to Daniel we dont signal an alarm

        ret = {
            ELEM_VAL_TYPE   : VALUE_TYPE_INS_BASAL,
            ELEM_VAL        : round(self.commanded_basal_rate * VAL_FACTOR_BASAL),
            ELEM_FLAG_LIST  : self.change_type.to_diasend_list(),
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

        if self.change_type.bits['new_profile']:
            ret = [
                ret, {
                    ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
                    ELEM_VAL        : DIASEND_EVENT_BASAL_PROGRAM_CHANGE,
                    ELEM_TIMESTAMP  : self.get_timestamp()
                }
            ]

        return ret

class RecordPumpingSuspended(RecordLogEntry):
    log_id = 11
    format = ('I2H2I')
    variables = ('reserved0', 'insulin_amount', 'reserved1', 'reserved2',
        'reserved3')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_VAL        : DIASEND_EVENT_DELIVERY_SUSPENDED,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordPumpingResumed(RecordLogEntry):
    log_id = 12
    format = ('I2H2I')
    variables = ('reserved0', 'insulin_amount', 'reserved1', 'reserved2',
        'reserved3')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_VAL        : DIASEND_EVENT_DELIVERY_RESUMED,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordTimeChange(RecordLogEntry):
    log_id = 13
    format = ('4I')
    variables = ('time_prior', 'time_after', 'reserved0', 'reserved1')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_VAL        : DIASEND_EVENT_DATETIME_CHANGE,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordDateChange(RecordLogEntry):
    log_id = 14
    format = ('4I')
    variables = ('date_prior', 'date_after', 'reserved0', 'reserved1')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_VAL        : DIASEND_EVENT_DATETIME_CHANGE,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class TempRateCompleted(RecordLogEntry):
    log_id = 15
    format = ('2H3I')
    variables = ('reserved0', 'temp_rate_id', 'time_left', 'reserved1',
        'reserved2')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_VAL        : DIASEND_EVENT_TEMP_BASAL_OFF,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordBgReadingTaken(RecordLogEntry):
    log_id = 16
    format = ('H2Bf2HI')
    variables = ('bg', 'reserved0', 'reserved1', 'iob', 'target_bg', 'isf',
        'reserved2')

    def diasend_bg(self):
        return float(self.bg) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_GLUCOSE,
            ELEM_VAL        : self.diasend_bg(),
            ELEM_FLAG_LIST  : [FLAG_MANUAL],
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordBolusCompleted(RecordLogEntry):
    log_id = 20
    format = ('2H3f')
    variables = ('reserved', 'bolus_id', 'iob', 'insulin_delivered',
        'insulin_requested')

    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')


class RecordBolexCompleted(RecordLogEntry):
    log_id = 21
    format = ('2H3f')
    variables = ('reserved', 'bolus_id', 'iob', 'insulin_delivered',
        'insulin_requested')
    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordCartridgeFilled(RecordLogEntry):
    log_id = 33
    format = ('If2I')
    variables = ('insulin_volume', 'v2_volume', 'reserved0', 'reserved1')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_ALARM,
            ELEM_VAL        : ALARM_CARTRIDGE_LOADED,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordCarbEntered(RecordLogEntry):
    log_id = 48
    format = ('f3I')
    variables = ('carbs', 'reserved0', 'reserved1', 'reserved2')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_CARBS,
            ELEM_VAL        : round(self.carbs * VAL_FACTOR_CARBS),
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordBolusActivated(RecordLogEntry):
    log_id = 55
    format = ('2H3f')
    variables = ('bolus_id', 'reserved0', 'iob', 'bolus_size', 'reserved1')
    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordIDPMsg2(RecordLogEntry):
    log_id = 57
    format = ('2BHI8s')
    variables = ('idp', 'reserved0', 'reserved1', 'reserved2', 'name')
    def convert_to_diasend(self):
        # Silently ignore, the first message will result in an event...
        return None

class RecordBolexActivated(RecordLogEntry):
    log_id = 59
    format = ('2H2fI')
    variables = ('bolus_id', 'reserved0', 'iob', 'bolex_size', 'reserved1')
    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordDataLogCorruption(RecordLogEntry):
    log_id = 60
    format = ('4I')
    variables = ('reserved0', 'reserved1', 'reserved2', 'reserved3')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_ALARM,
            ELEM_VAL        : ALARM_BAD_HISTORY_DATA,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordCannulaFilled(RecordLogEntry):
    log_id = 61
    format = ('f3I')
    variables = ('prime_size', 'reserved0', 'reserved1', 'reserved2')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_INS_PRIME,
            ELEM_VAL        : round(self.prime_size * VAL_FACTOR_PRIME),
            ELEM_FLAG_LIST  : [FLAG_CANNULA_BOLUS],
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordTubingFilled(RecordLogEntry):
    log_id = 63
    format = ('f3I')
    variables = ('prime_size', 'reserved0', 'reserved1', 'reserved2')

    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_INS_PRIME,
            ELEM_VAL        : round(self.prime_size * VAL_FACTOR_PRIME),
            ELEM_FLAG_LIST  : [FLAG_TUBING],
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordBolusRequestMsg1(RecordLogEntry):
    log_id = 64
    format = ('H', EnumBolusEntryType, EnumYesNo, '2HfI')
    variables = ('bolus_id', 'bolus_type', 'correction_bolus_included',
        'carb_amount', 'bg', 'iob', 'carb_ratio')
    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordBolusRequestMsg2(RecordLogEntry):
    log_id = 65
    format = ('H', EnumBolusType, 'B4H', EnumYesNo, EnumYesNo, 'H')
    variables = ('bolus_id', 'options', 'standard_percent', 'duration', 'reserved0',
        'isf', 'target_bg', 'user_override', 'declined_correction', 'reserved1')
    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordBolusRequestMsg3(RecordLogEntry):
    log_id = 66
    format = ('2H3f')
    variables = ('bolus_id', 'reserved', 'food_bolus_size', 'correction_bolus_size',
        'total_bolus_size')
    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordIDPTDSeg(RecordLogEntry):
    log_id = 68
    format = ('4B4HI')
    variables = ('idp', 'status', 'segment_index', 'modification_type',
        'stat_time', 'basal_rate', 'isf', 'target_bg', 'carb_ratio')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_BASAL_PROGRAM_EDITED
        }

class RecordIDPChange(RecordLogEntry):
    log_id = 69
    format = ('4BI8s')
    variables = ('idp', 'status', 'source_idp', 'reserved0', 'reserved1', 'name')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_BASAL_PROGRAM_EDITED
        }

class RecordIDPBolusChange(RecordLogEntry):
    log_id = 70
    format = ('4B2H2BHI')
    variables = ('idp', 'modification', 'bolus_status', 'reserved0',
        'insulin_duration', 'max_bolus_size', 'bolus_data_entry_type',
        'reserved1', 'reserved2', 'reserved3')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_BASAL_PROGRAM_EDITED
        }

class RecordIDPListChange(RecordLogEntry):
    log_id = 71
    format = ('2BH6BHI')
    variables = ('num_of_profiles', 'reserved0', 'reserved1', 'gui_slot1',
        'gui_slot2', 'gui_slot3', 'gui_slot4', 'gui_slot5', 'gui_slot6',
        'reserved2', 'reserved3')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_BASAL_PROGRAM_EDITED
        }

class RecordPumpSettingsChanged(RecordLogEntry):
    log_id = 73
    format = ('4I')
    variables = ('todo0', 'todo1', 'todo2', 'todo3')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_PUMP_SETTINGS_EDITED
        }

class RecordGlobalSettingsChanged(RecordLogEntry):
    log_id = 74
    format = ('4I')
    variables = ('todo0', 'todo1', 'todo2', 'todo3')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_GLOBAL_SETTINGS_EDITED
        }

class RecordDailyBasal(RecordLogEntry):
    log_id = 81
    format = ('3f2BH')
    variables = ('daily_total_basal', 'last_basal_rate', 'iob', 'reserved',
        'abc', 'lip_mv')
    def convert_to_diasend(self):
        # Only the basal part returned, this is not a complete record
        return {
            ELEM_VAL_TYPE: VALUE_TYPE_INS_BASAL_TDD,
            ELEM_VAL: round(self.daily_total_basal * VAL_FACTOR_TDD)
        }

class RecordFactoryReset(RecordLogEntry):
    log_id = 82
    format = ('4I')
    variables = ('reserved0', 'reserved1', 'reserved2', 'reserved3')
    def convert_to_diasend(self):
        # TODO: Should it be renamed?
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_ALARM,
            ELEM_VAL        : ALARM_INSULINX_RESET_TO_FACTORY_CONFIGURATION,
            ELEM_TIMESTAMP  : self.get_timestamp()
        }

class RecordNewDay(RecordLogEntry):
    log_id = 90
    format = ('f3I')
    variables = ('commanded_basal_rate', 'reserved0', 'reserved1', 'reserved2')
    def convert_to_diasend(self):
        return None

class RecordCorrectionDeclined(RecordLogEntry):
    log_id = 93
    format = ('2Hf2H2BH')
    variables = ('bg', 'bolus_id', 'iob', 'target_bg', 'isf', 'reserved0',
        'reserved1', 'reserved2')
    def convert_to_diasend(self):
        # Since it's related to a bolus this will be handled the bolus class
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'Separate bolus object should not be converted')

class RecordReminderChanged(RecordLogEntry):
    log_id = 96
    format = ('4I')
    variables = ('todo0', 'todo1', 'todo2', 'todo3')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_REMINDER_SETTINGS_EDITED
        }

class RecordReminderSettingsChanged(RecordLogEntry):
    log_id = 97
    format = ('4I')
    variables = ('todo0', 'todo1', 'todo2', 'todo3')
    def convert_to_diasend(self):
        return {
            ELEM_VAL_TYPE   : VALUE_TYPE_EVENT,
            ELEM_TIMESTAMP  : self.get_timestamp(),
            ELEM_VAL        : DIASEND_EVENT_REMINDER_SETTINGS_EDITED
        }

class Bolus:
    """
        A bolus contains of many messages, so an instance of this class acts
        as a container for the bolus records until finished, then its
        flatten out to diasend records...
    """
    def __init__(self, first_record):
        # We only allow one message per type, to double check parsing
        self.__records = {first_record.__class__.__name__: first_record}
        self.__declined = []

    def add_record(self, record):
        if record.__class__.__name__ == "RecordCorrectionDeclined":
            # Of some reason multiple Correction declined might be generated
            self.__declined.append(record.get_timestamp())
        elif self.has_record_of_class(record.__class__):
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                "Parsing - multiple records of same type in bolus: %r", record.__class__)
        else:
            self.__records[record.__class__.__name__] = record

    def has_record_of_class(self, cls):
        """
            Check if a record of a certain class is added to the bolus
        """
        return cls.__name__ in self.__records

    def get_record(self, cls):
        return self.__records[cls.__name__]

    def has_record_of_all_classes(self, classes):
        """
            Check if a bolus has a record of all given classes
        """
        for cls in classes:
            if not self.has_record_of_class(cls):
                return False

        return True

    def print_bolus(self):
        """
            Print information regarding the bolus
        """
        # We might have parts of the log so the first bolus might not
        # be completed..
        any_msg = self.__records.values()[0]
        bolus_id = None
        if hasattr(any_msg, 'bolus_id'):
            bolus_id = any_msg.bolus_id
        else:
            bolus_id = any_msg.bolex_id

        if not self.validate():
            logging.debug("Bolus incomplete")
            return

        timed = {}
        for r in self.__records:
            rec = self.__records[r]
            if not rec.get_timestamp() in timed:
                timed[rec.get_timestamp()] = []

            timed[rec.get_timestamp()].append(rec)

        keys = timed.keys()
        keys.sort()

        for ts in keys:
            recs = timed[ts]
            for rec in recs:
                logging.debug(rec.__class__.__name__, ts)
        for dec in self.__declined:
            logging.debug(dec)

        msg1 = self.get_record(RecordBolusRequestMsg1)
        msg2 = self.get_record(RecordBolusRequestMsg2)
        msg3 = self.get_record(RecordBolusRequestMsg3)
        if self.has_record_of_class(RecordBolusActivated):
            act = self.get_record(RecordBolusActivated)
        else:
            act = None
        if self.has_record_of_class(RecordBolusCompleted):
            complete = self.get_record(RecordBolusCompleted)
        else:
            # Not yet completed
            complete = None

        if self.has_record_of_class(RecordBolexCompleted):
            bolex_complete = self.get_record(RecordBolexCompleted)
            bolex_act = self.get_record(RecordBolexActivated)
        else:
            # Not yet completed
            bolex_complete = None
            bolex_act = None

        logging.debug("type:", msg1.bolus_type.to_text(), \
            "overrid:", msg2.user_override.to_text(), "has corr", \
            msg1.correction_bolus_included.to_text(), "declined corr", \
            msg2.declined_correction.to_text())
        logging.debug("carbs: ", msg1.carb_amount, "bg:", msg1.bg, "ratio:", msg1.carb_ratio)

        logging.debug(msg2.options.to_text(), "% now", msg2.standard_percent, "dur:", \
            msg2.duration, "isf:", msg2.isf, "target:", msg2.target_bg)

        logging.debug("food size:", msg3.food_bolus_size, "corr size:", msg3.correction_bolus_size, \
            "tot:", msg3.total_bolus_size, "iob:", msg1.iob)

        if msg2.user_override.to_text() == 'yes':
            override_size = msg3.total_bolus_size - msg3.food_bolus_size - \
                msg3.correction_bolus_size
            logging.debug("Override size:", override_size)

        if act:
            logging.debug("Activated size:", act.bolus_size)
        else:
            logging.debug("NOT ACTIVATED")

        if bolex_act:
            logging.debug("Extended Activated size:", bolex_act.bolex_size)

        if not complete:
            logging.debug("Bolus was not completed")
        else:
            logging.debug("Completed size:", complete.insulin_delivered, "req:", complete.insulin_requested)

            if round(complete.insulin_delivered,2) != round(complete.insulin_requested,2):
                logging.debug("size mismatch")

        if bolex_complete:
            logging.debug("Extended complete size:", bolex_complete.insulin_delivered, "req:", bolex_complete.insulin_requested)

    def validate(self):
        """
            Validate if the bolus is OK, it must contain a set of records
            and especially a completed in the end.
            It might now have an activate record if the bolus was canceled

            A bolus must contain:
                * the first 3 start record
                * the complete record

            Optionally it can contain (has to have both):
                * the activate record
                * the bolex activate record
                * the bolex complete record
        """
        if not self.has_record_of_all_classes([RecordBolusRequestMsg1,
                RecordBolusRequestMsg2, RecordBolusRequestMsg3,
                RecordBolusCompleted]):
            return False

        msg2 = self.get_record(RecordBolusRequestMsg2)
        has_extended = msg2.duration > 0

        if has_extended:
            return self.has_record_of_class(RecordBolexActivated) and \
                self.has_record_of_class(RecordBolexCompleted)
        else:
            return not self.has_record_of_class(RecordBolexActivated) and \
                not self.has_record_of_class(RecordBolexCompleted)

    def get_timestamp(self):
        """
            Get the timestamp from a bolus
            If its activated that event timestamp will be returned,
            if not then the complete timestamp will be returned
        """
        if not self.validate():
            return None
        elif self.has_record_of_class(RecordBolusActivated):
            return self.get_record(RecordBolusActivated).get_timestamp()
        else:
            return self.get_record(RecordBolusCompleted).get_timestamp()

    def get_immediate_delivered(self):
        """
            Returns the immediate part along with a timestamp

            If nothing was delivered None is returned
        """
        if self.validate():
            bolus = self.get_record(RecordBolusCompleted)
            if bolus:
                return (bolus.insulin_delivered, bolus.get_timestamp())

        return None

    def get_extended_delivered(self):
        """
            Returns the extended (bolex) part along with a timestamp

            If nothing was delivered None is returned
        """
        if self.validate():
            if self.get_record(RecordBolusRequestMsg2).duration > 0:
                bolex = self.get_record(RecordBolexCompleted)
                if bolex:
                    return (bolex.insulin_delivered, bolex.get_timestamp())

        return None

    def convert_to_diasend(self):
        """
            Flattern the different bolus records to a diasend record
        """
        if not self.validate():
            # We could think of throwing exceptions here.. but the log we get
            # is capped so there is a risk that we dont get the start messages
            # for instance but only the completion, or if its still ongoing
            # while downloading huh
            return None

        # Activate is optional, depending on if the bolus is started or not
        if self.has_record_of_class(RecordBolusActivated):
            activate = self.get_record(RecordBolusActivated)
        else:
            activate = None

        complete = self.get_record(RecordBolusCompleted)
        msg1 = self.get_record(RecordBolusRequestMsg1)
        msg2 = self.get_record(RecordBolusRequestMsg2)
        msg3 = self.get_record(RecordBolusRequestMsg3)

        flags = []

        ret = {}
        ret[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS
        ret[ELEM_TIMESTAMP] = self.get_timestamp()
        if not activate:
            flags.append(FLAG_CANCELED)

        ret[ELEM_VAL] = round(complete.insulin_delivered * VAL_FACTOR_BOLUS)

        values = []

        if round(complete.insulin_delivered, 2) != \
            round(complete.insulin_requested, 2):
            # Failed to deliver everything
            if not FLAG_CANCELED in flags:
                flags.append(FLAG_CANCELED)

        # Things to care about:
        # * carb/insulin
        # * Standard/extended/quick
        # * FoodBolusSize
        # * CorrectionBolusSize
        # * user override
        # * declined correction
        # * target bg
        # * isf
        if msg1.bolus_type.to_text() == 'carbs':
# TODO: Secondary cargs is not allowed
#            values.append({ELEM_VAL_TYPE: VALUE_TYPE_CARBS,
#                ELEM_VAL: round(msg1.carb_amount * VAL_FACTOR_CARBS)})

            # TODO: Should we flag like EZ carg in the animas case

# TODO: Secondary BG values are not allowed
#        if msg1.bg > 0:
#            values.append({ELEM_VAL_TYPE: VALUE_TYPE_GLUCOSE,
#                ELEM_VAL: float(msg1.bg) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL,
#                ELEM_FLAG_LIST: [FLAG_MANUAL]})
            pass

        if msg3.food_bolus_size > 0:
            values.append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL,
                ELEM_VAL: round(msg3.food_bolus_size * VAL_FACTOR_BOLUS)})

        if msg2.user_override.to_text() == 'yes':
            # We can calculate the override size

            # it can look like this: here the user obviosl threw in 7.5
            # type: carbs overrid:  yes has corr no declined corr no
            # carbs:  0 bg: 111 ratio: 6000
            # standard_bolus % now 100 dur: 0 isf: 50 target: 120
            # food size: 0.0 corr size: 0.0 tot: 7.5
            # Activated size: 7.5
            # Completed size: 7.5 req: 7.5

            # One with food and corr and is overriddens
            # type: carbs overrid:  yes has corr yes declined corr no
            # carbs:  23 bg: 201 ratio: 6000
            # standard_bolus % now 100 dur: 0 isf: 50 target: 150
            # food size: 3.82999992371 corr size: 1.01999998093 tot: 5.84999990463
            # Activated size: 5.84999990463
            # Completed size: 5.8499994278 req: 5.84999990463

            # Here is another where the user decreased the dose on 0.26
            # type: carbs overrid:  yes has corr yes declined corr no
            # carbs:  0 bg: 363 ratio: 6000
            # standard_bolus % now 100 dur: 0 isf: 50 target: 150
            # food size: 0.0 corr size: 4.26000022888 tot: 4.0
            # Activated size: 4.0
            # Completed size: 3.99999952316 req: 4.0

            # Sum the suggested size since total will contained the overide..
            suggested_size = msg3.food_bolus_size + \
                msg3.correction_bolus_size
        else:
            suggested_size = msg3.total_bolus_size

        values.append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_SUGGESTED,
            ELEM_VAL:  round(suggested_size * VAL_FACTOR_BOLUS) })

        # Only store programmed if cancelled
        if FLAG_CANCELED in flags:
            values.append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_PROGRAMMED,
                ELEM_VAL:  round(msg3.total_bolus_size * VAL_FACTOR_BOLUS) })

        # We use the IOB value when we activate
        if activate:
            iob = round(activate.iob * VAL_FACTOR_BOLUS)
        else:
            iob = round(complete.iob * VAL_FACTOR_BOLUS)

        values.append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_IOB, ELEM_VAL: iob})

        if msg1.correction_bolus_included.to_text() == 'yes':
            values.append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR,
                ELEM_VAL: round(msg3.correction_bolus_size * VAL_FACTOR_BOLUS)})

        if msg2.duration > 0:
            # An extended part...
            bolex_complete = self.get_record(RecordBolexCompleted)

            values.append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_EXT,
                ELEM_VAL: round(bolex_complete.insulin_delivered * VAL_FACTOR_BOLUS)})

            values.append({ELEM_VAL_TYPE: VALUE_TYPE_DURATION,
                ELEM_VAL: msg2.duration})

            flags.append(FLAG_BOLUS_TYPE_COMBO)

        ret[ELEM_VALUE_LIST] = values
        ret[ELEM_FLAG_LIST] = flags

        return ret

# --- controller

class TandemDataExtractor(object):

    def __init__(self, data):
        self.__data   = data
        self.__version = None

    @staticmethod
    def convert_record(record, diasend_records):
        """
            Convert a single record and append any results to the list
        """
        record_or_list = record.convert_to_diasend()

        if record_or_list != None:
            if type(record_or_list) is list:
                diasend_records.extend(record_or_list)
            else:
                diasend_records.append(record_or_list)

    @staticmethod
    def convert_tdd(day, tdd, diasend_records):
        """
            The TDD needs special treatment since the pump only returns
            the basal part, while the bolus part is added during extraction
        """
        total_amount = tdd["bolus"]

        if "basal" in tdd:
            rec = tdd["basal"]
            total_amount += rec.daily_total_basal
            basal = rec.convert_to_diasend()
            timestamp = rec.get_timestamp()
        else:
            basal = None
            timestamp = datetime.datetime(day.year, day.month, day.day, 23, 55)

        record = {
            ELEM_VAL_TYPE   : VALUE_TYPE_INS_TDD,
            ELEM_VAL        : round(total_amount * VAL_FACTOR_TDD),
            ELEM_TIMESTAMP  : timestamp
        }

        if basal:
            record[ELEM_VALUE_LIST] = [basal]

        diasend_records.append(record)

    def get_version(self):
        return self.__version

    def split(self):
        # The buffer contains of tandem packets received from the device.
        # All but the first record has the timestamp and CRC thrown away
        # The CRC is validated on the device and the timestamp is just
        # interesting once if we want to check the datetime of the device
        # this saves us 6 bytes * the number of records which are up to 15.000
        buf = Buffer(self.__data)
        # We dont need the data anymore, can potentially save RAM
        self.__data = None

        # We will pickup the IDP list since it keeps the active
        # profile, so we can set the name of the active profile
        # when the profiles are parsed
        idp_list = None

        # the pump generates several daily basal every day so we need to
        #    only evaluate the latest
        # We also need to add the bolus sum to the TDD since the TDD
        # from the pump is only containing the sum of the basal part
        tdds = {}
        # The pump generates several events per bolus, so we create
        # aggregate objects encapsulating all events
        boluses = {}

        # The output records in diasend format...
        diasend_records = []

        # The header is 3 bytes
        while buf.nr_bytes_left() >= HEADER_SIZE:
            # Extract the Tandem t:slum frame header (SOF msg type, cargo length)
            header = buf.get_struct('3B')

            if header[0] != START_OF_FRAME:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                    "Split data - frame does not start with SOF 0x%02x 0x%02x 0x%02x" % (header[0], header[1], header[2]))

            cargo_len = header[2]

            # Create the TandemRecord instance. The result is stored in the TandemRecord
            # db for later retrieval.
            # An exception will be raised if validation fails (e.g. unknown enum value or
            # too little data).
            record = TandemRecord.create(header[1], buf, cargo_len)
            if isinstance(record, RecordDailyBasal):
                # Keep the latest TDD for each day, then when we only have
                # the interesting ones, we convert them
                day = record.get_timestamp().date()
                if not day in tdds:
                    tdds[day] = {"bolus": 0.0}

                if not "basal" in tdds[day] or tdds[day]["basal"].get_timestamp() < record.get_timestamp():
                    tdds[day]["basal"] = record
            elif hasattr(record, 'bolus_id'):
                if not record.bolus_id in boluses:
                    boluses[record.bolus_id] = Bolus(record)
                else:
                    try:
                        boluses[record.bolus_id].add_record(record)
                    except Exception,e:
                        # Wow the same bolus ID occures again...
                        # There is a known bug in the pump where this can happen
                        # Info from tandem (p624:#3916)
                        # The duplicate Bolus IDs #380 found in your dataset is
                        # caused by a bug that was found after we released the
                        # SW that is on your current pumps.
                        #
                        # The duplicate Bolus IDs occur when a user requests a
                        # bolus over the absolute max bolus size and gets an
                        # additional bolus reminder from the pump.
                        #
                        # The next Bolus that the user requests will use the
                        # previous Bolus ID, in this case #380. This should not
                        # happen and each Bolus request will have a unique
                        # Bolus ID.
                        #
                        # This bug will be fixed when your pumps get updated to
                        # the latest SW.
                        if boluses[record.bolus_id].validate():
                            # The previous Bolus is complete...
                            # Give it some fake index and add the new one
                            # at the ID
                            new_id = "dupe%d" % record.bolus_id
                            while new_id in boluses:
                                new_id += "X"

                            boluses[new_id] = boluses[record.bolus_id]
                            boluses[record.bolus_id] = Bolus(record)
                        else:
                            raise e
            else:
                if isinstance(record, RecordIDPList):
                        idp_list = record
                elif isinstance(record, RecordIDP) and idp_list.slot1 == record.idp:
                        diasend_records.append({
                                SETTINGS_LIST: {
                                    SETTING_ACTIVE_BASAL_PROGRAM_ID : record.get_name()
                                }
                        })

                TandemDataExtractor.convert_record(record, diasend_records)
                if not self.__version and isinstance(record, RecordVersion):
                    self.__version = record

        if buf.nr_bytes_left() != 0:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'Split data - left over data %d bytes' % (buf.nr_bytes_left()))

        # Add the boluses
        for record in boluses.values():
            bolus = record.get_immediate_delivered()
            bolex = record.get_extended_delivered()
            self.add_to_tdd(tdds, bolus)
            self.add_to_tdd(tdds, bolex)

            TandemDataExtractor.convert_record(record, diasend_records)
            if STATS != None:
                record.print_bolus()

        # Add the latest TDD per day
        for day in tdds:
            TandemDataExtractor.convert_tdd(day, tdds[day], diasend_records)

        return diasend_records

    @staticmethod
    def add_to_tdd(tdds, bolus):
        """
            Adds a bolus with timestamp to a TDD, also checks for None
        """
        if bolus:
            (amount, timestamp) = bolus
            day = timestamp.date()
            if not day in tdds:
                tdds[day] = {"bolus": 0.0}
            tdds[day]["bolus"] += amount

    @staticmethod
    def eval_serial_record(record):
        if ELEM_DEVICE_SERIAL in record:
            return record

        return {}

    @staticmethod
    def eval_unit_record(record):
        return {ELEM_DEVICE_UNIT : 'mg/dl'}

    @staticmethod
    def eval_result_record(record):
        if any(key in record for key in (ELEM_VAL_TYPE, PROGRAM_TYPE, SETTINGS_LIST)):
            return record

        return {}

    @staticmethod
    def eval_checksum_record(row, record):
        return True

    @staticmethod
    def eval_device_model_record(record):
        if ELEM_DEVICE_MODEL in record:
            return record

        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectTandemTslim( inList ):
    """
    Detect if data comes from an Tandem Pump.
    """
    return DetectDevice( "TandemTslim", inList, DEVICE_PUMP )

def AnalyseTandemTslim( data ):
    """
    Analyse Tandem t:slim Pump
    """
    callbacks = {}
    callbacks["eval_serial_record"]       = TandemDataExtractor.eval_serial_record
    callbacks["eval_unit"]                = TandemDataExtractor.eval_unit_record
    callbacks["eval_result_record"]       = TandemDataExtractor.eval_result_record
    callbacks["eval_checksum_record"]     = TandemDataExtractor.eval_checksum_record
    callbacks["eval_device_model_record"] = TandemDataExtractor.eval_device_model_record

    try:
        extractor = TandemDataExtractor(data)
        diasend_records = extractor.split()

    except Exception, e:

        import traceback
        traceback.print_exc()

        version_record = extractor.get_version()
        if version_record:
            serial_number = version_record.pump_sn
        else:
            serial_number = 'UNKNOWN'

        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(DEVICE_MODEL_TSLIM, serial_number, e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(DEVICE_MODEL_TSLIM, serial_number, ERROR_CODE_PREPROCESSING_FAILED,
                ''.join(map(str, e.args)))

        return error_response

    result = AnalyseGenericMeter(diasend_records, callbacks)

    return result

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    test_files = [
        'TandemTslim/TandemTflex-half.bin',
        'TandemTslim/TandemTslim-full.bin',
        'TandemTslim/TandemTslim-half.bin',
        'TandemTslim/TandemTslim-multiple-records-same-type-Z25891.bin',
        'TandemTslim/TandemTslim-multiple-records-same-type-Z37191.bin',
        'TandemTslim/TandemTslim-real.bin',
        'TandemTslim/TandemTslim-tandem1.bin',
        'TandemTslim/TandemTslim-tandem2.bin',
        'TandemTslim/TandemTslim-tandem3.bin',
        'TandemTslim/TandemTslim-tandem4.bin',
        'TandemTslim/TandemTslim-unchanged-pump-settings-Z25595.bin',
        'TandemTslim/TandemTslim-with_settings.bin',
        'TandemTslim/TandemTslim-with_settings2.bin',
        'TandemTslim/TandemTslim-with_settings3.bin',
        'TandemTslim/TandemTslimG4-Z45568.bin',
        'TandemTslim/TandemTslimG4-from-tandem.bin',
        'TandemTslim/TandemTslimG4-SN480912.bin',
        'TandemTslim/TandemTslimG4-SN479853.bin',
        'TandemTslim/TandemTslimX2-SN491153.bin']
        
    for test_file in test_files:
        with open('test/testcases/test_data/%s' % (test_file), 'rb') as f:
            raw_data = f.read()
            res = AnalyseTandemTslim(raw_data)
