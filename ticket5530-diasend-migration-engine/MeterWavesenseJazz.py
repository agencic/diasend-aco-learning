# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2009 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE AgaMatrix WaveSense JAZZ
# @DEVICE AgaMatrix WaveSense JAZZ Wireless
# @DEVICE Sanofi BGStar
# @DEVICE Sanofi MyStar Extra

import re
import Common
import struct
from Generic import *
from Defines import *
from datetime import datetime
from datetime import timedelta


# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

class WavesendJazzAnalyser(object):
    
    def __init__(self):
        self.model_name = None
        self.items = []

    def eval_serial_record(self, line):
        """
        Evaluate a Jazz serial nr record. Extract groups : 
        group 1 : serial number
        
        Serial number is a string upto 16 characters. 
       
        example
        !TAG!SERNO!200 serial CWDD29Z8600122
        """
        m = re.match( r'^!TAG!SERNO!200 serial (\w{4,16})', line, re.IGNORECASE )
        return m
    
    def eval_binary_result_record(self, line):
        """
        Evaluate a Jazz result record. Extract groups : 
        time
        glucose
        timeblock
        ctrl
        
        """
        if len(line) != 6:
            return None
        
        res = {}
        data = struct.unpack("<LH", line)
        res["time"]      = data[0]
        res["glucose"]   = data[1] & 0x03FF
        res["timeblock"] = (data[1] & 0x1C00) >> 10
        res["ctrl"]      = (data[1] & 0x2000) >> 13
    
        return res
        
    def eval_ascii_result_record(self, line):
        """
        Evaluate a Jazz ASCII result record. Extract groups : 
        group 1 : ctrl
        group 2 : glucose
        group 3 : timeblock
        group 4 : year
        group 5 : month
        group 6 : day
        group 7 : hour
        group 8 : minute
        group 9 : second
        
        However, in order to keep most code intact when adding this result type, the results are then converted to same format as EvalBinaryResultRecord()
        
        example
        200 glurec 2 0 315 2 2009 7 1 2 5 0
        
    
        """
        
        m = re.match( r'^200 glurec \d+ (\d) (\d{1,3}) (\d) (\d{4}) (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2})', line, re.IGNORECASE )
        if m:
            res = {}
            
            # Time should be in seconds from 2000-01-01 to match the binary representation
            td = (datetime(int(m.group(4)), int(m.group(5)), int(m.group(6)), int(m.group(7)), int(m.group(8)), int(m.group(9))) - datetime(2000, 1, 1))
            res["time"]      = td.days*24*3600 + td.seconds
            
            res["glucose"]   = int(m.group(2))
            res["timeblock"] = int(m.group(3))
            res["ctrl"]      = int(m.group(1))
            return res
            
        else:
            return None
            
    def eval_ascii_strip_error_record(self, line):
        """
        Check if this is a strip error record.
        """
        m = re.match( r'^200 glurec \d+ (\d) E([A-Fa-f0-9]{2}) (\d) (\d{4}) (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2})', line, re.IGNORECASE )
        if m:
            return True
        return False
    
    def eval_nr_result_record(self, line):
        """
        Evaluate a Jazz nr of result record. Extract groups : 
        group 1 : Number of results
    
        example
        !TAG!RESULTS!200 glucount 934
        """
    
        m = re.match( r'^!TAG!RESULTS!200 glucount (\d{1,4})', line, re.IGNORECASE )
        return m
    
    def eval_model_record(self, line):
        """
        Evaluate a Jazz model record. Extracted groups :
        group 1 : Model
    
        example
        !TAG!MODEL!200 hello JAZZ
        """
    
        m = re.match( r'^!TAG!MODEL!200 hello (.*)', line)
        return m
    
    
    def split_data(self, in_data):
        """
        Split the incoming data packet into multiple lines
        
        Because of support for different versions of the protocol, two versions of the records exist:
        ASCII-only or ASCII-mixed-with-binary
        """
        
        if "!TAG!BINARY!" in in_data:
            out_list = self.split_mixture_data(in_data)
        else:
            out_list = self.split_ascii_data(in_data)
        
        cbs = [self.eval_wavesense_jazz_serial_record,
            self.eval_wavesense_jazz_model_record,
            self.eval_wavesense_jazz_result_record,
            self.eval_wavesense_jazz_nr_results_record]
        
        for line in out_list:
            for cb in cbs:
                res = cb(line)
                if res != {}:
                    self.items.append(res)
                    continue
                    
        if self.model_name is not None:
            self.items.insert(0, {ELEM_DEVICE_MODEL: self.model_name})
        
        return self.items
    
    def split_ascii_data(self, in_data):
        """
        Split the incoming ASCII-only data packet into multiple lines
        """
        return in_data.split('\r\n')
    
    
    def split_mixture_data(self, in_data):
        """
        Split the incoming mixed data packet into multiple lines
        
        The data for newer versions of the Jazz is a mixture of ASCII and binary data. This is to be
        prepared for the Wireless Jazz, where straight ASCII download is not recommended
        in the documentation. It is also much faster to download.
        First, some ASCII lines then a binary blob of the structs
        * one fs_glurecbin_header_t record (Size of data)
        * zero or more fs_gfsrecord_t records (Data) , size = 12 bytes
        * one fs_glurecbin_footer_t record (CRC)
        """
    
        # Separate the data into ASCII and binary
        in_list = in_data.split("!TAG!BINARY!")
        
        # Create the ASCII records
        out_list = self.split_ascii_data(in_list[0])
        
        # Split the binary chunks (6 bytes each, length first)
        bin_data = in_list[1]
        size = struct.unpack("<H", bin_data[0:2])[0]
        dataList = Common.SplitCount(bin_data[2:size-2], 6)
    
        # Assemble the ASCII and binary lists
        for line in dataList:
            if line:
                out_list.append(line)
        
        return out_list
    
    def eval_wavesense_jazz_serial_record(self, line):
        """
        Is this line a serial record. If so, return a dictionary with serial
        number.
        """
        res = {}
        m = self.eval_serial_record( line )
        if m:
            res[ELEM_DEVICE_SERIAL] = m.group(1)
            if self.model_name == "MODEL_JAZZESC_DECIDE_LATER":
                if res[ELEM_DEVICE_SERIAL][0] == 'J':
                    self.model_name = "Sanofi BGStar"
                else:
                    self.model_name = "WaveSense JAZZ"
        return res
        
    def eval_wavesense_jazz_model_record(self, line):
        """
        Extract the model name (if found). Always returns empty dict - the model
        record has to be added later when the serial number is known as well.
        """
        m = self.eval_model_record(line)
        if m:
            if m.group(1) == "JAZZ":
                self.model_name = "WaveSense JAZZ"
            elif m.group(1) == "JAZZM":
                self.model_name = "WaveSense JAZZ Wireless"
            elif m.group(1) == "FPGM":
                self.model_name = "Sanofi MyStar Extra"
            else:
                # BGStar responds with 'JAZZESC-' followed by a two letter character code per ISO 639-1.
                r = re.match( r'^JAZZESC-([A-Z]{2})$', m.group(1) )
                if r:
                    self.model_name = "MODEL_JAZZESC_DECIDE_LATER"
                else:
                    print "Unknown model:", m.group(1)
        
        # Require serial number in certain cases to determine model. 
        return {}
    
    def eval_wavesense_jazz_result_record(self, line):
        """
        Is this a result record? If so, return a dictonary with keys >
        
        date_time   > date in yyyy-mm-dd hh:mm:ss format
        value       > value (float) 
        unit        > unit if present (otherwise require headerunit)
        flags       > list of flags (int) if present
        """
        res = {}
        m = self.eval_binary_result_record(line)
        if not m:
            m = self.eval_ascii_result_record(line)
    
            # We want to support Ascii from Sanofi MyStar Extra
            if self.model_name == "Sanofi MyStar Extra":
                pass
    
            # Currently, we don't want old comm devices to be transferred...
            # Might enable this in the future. Then just remove those two lines.
            elif m:
                return { "error_code":ERROR_CODE_TOO_OLD_SW_VER }
    
        if not m:
            if self.eval_ascii_strip_error_record(line):
                return {"null_result": True}
        
        if m:
            # Date since 2000
            try:
                res[ ELEM_TIMESTAMP ] = datetime(2000, 1, 1, 0, 0, 0) + timedelta(seconds=m["time"])
            except:
                return None
                    
                    
            _flags = []
            res[ ELEM_FLAG_LIST ] = _flags
    
            # Fetch the timeblock
            if m["timeblock"] == 1:
                _flags.append(FLAG_BREAKFAST)
            elif m["timeblock"] == 2:
                _flags.append(FLAG_AFTER_BREAKFAST)
            elif m["timeblock"] == 3:
                _flags.append(FLAG_LUNCH)
            elif m["timeblock"] == 4:
                _flags.append(FLAG_AFTER_LUNCH)
            elif m["timeblock"] == 5:
                _flags.append(FLAG_DINNER)
            elif m["timeblock"] == 6:
                _flags.append(FLAG_AFTER_DINNER)
            elif m["timeblock"] == 7:
                _flags.append(FLAG_NIGHT)
                
            # Control solution check
            if m["ctrl"]:
                _flags.append(FLAG_RESULT_CTRL)
    
            # Set the value
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
            
            try:
                res[ ELEM_VAL ] = float( m["glucose"] )
                
                # g is the glucose level in mg/dL [0-767]
                #  * less than 20 is a Lo reading 
                #  * greater than 600 is a Hi reading
                #  * value above 767 indicates error in value
    
                if res[ ELEM_VAL ] > 767.0:
                    return None
                
                if res[ ELEM_VAL ] < 20.0:
                    _flags.append(FLAG_RESULT_LOW)
                elif res[ ELEM_VAL ] > 600.0:
                    _flags.append(FLAG_RESULT_HIGH)
                    
            except ValueError:
                return None
    
        return res
        
    def eval_wavesense_jazz_nr_results_record(self, line):
        """
        Is this line a nr results. If so, return a dictionary with nr results.
        """
        res = {}
        m = self.eval_nr_result_record(line)
        if m:
            try:
                res[ "meter_nr_results" ] = int( m.group(1), 10 )
            except ValueError:
                res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
        return res

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalWavesenseJazzSerialRecord(line):
    return line if ELEM_DEVICE_SERIAL in line else {}

def EvalWavesenseJazzUnitRecord(line):
    return { ELEM_DEVICE_UNIT:"mg/dL" }
    
def EvalWavesenseJazzModelRecord(line):
    return line if ELEM_DEVICE_MODEL in line else {}

def EvalWavesenseJazzResultRecord(line):
    return line if ELEM_VAL in line or "null_result" in line or "error_code" in line else {}

def EvalWavesenseJazzChecksumRecord(line, record):
    return True

def EvalWavesenseJazzChecksumFile(line):
    return True

def EvalWavesenseJazzNrResultsRecord(line):
    return line if ELEM_NR_RESULTS in line else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectWavesenseJazz(in_list):
    """
    Detect if data comes from a Jazz.
    """
    return DetectDevice( 'WavesenseJazz', in_list, DEVICE_METER )

def AnalyseWavesenseJazz(in_data):
    """
    Analyse Jazz
    """

    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ]  = EvalWavesenseJazzModelRecord
    d[ "eval_serial_record" ]        = EvalWavesenseJazzSerialRecord
    d[ "eval_unit"]                  = EvalWavesenseJazzUnitRecord
    d[ "eval_result_record" ]        = EvalWavesenseJazzResultRecord
    d[ "eval_checksum_record" ]      = EvalWavesenseJazzChecksumRecord
    d[ "eval_checksum_file" ]        = EvalWavesenseJazzChecksumFile
    d[ "eval_nr_results" ]           = EvalWavesenseJazzNrResultsRecord

    analyser = WavesendJazzAnalyser()
    items = analyser.split_data(in_data)

    res_list = AnalyseGenericMeter(items, d)
    return res_list


if __name__ == "__main__":

    test_files = ['test/testcases/test_data/BGStar/Jazz_CUBC247F405243.log',
        'test/testcases/test_data/BGStar/BGStar-ticket2193.bin',
        'test/testcases/test_data/BGStar/BGStar_JZBC14ZA400140.log',
        'test/testcases/test_data/BGStar/BGStar-ticket2464.bin',
        'test/testcases/test_data/BGStar/BGStar_JZBC16ZA203688.log',
        'test/testcases/test_data/BGStar/BGStar_JZBC14ZA300932.log',
        'test/testcases/test_data/BGStar/StarExtra-ticket4833.log']

    for test_file in test_files:
        with open(test_file, 'rb') as f:
            data = f.read()
            print AnalyseWavesenseJazz(data)

