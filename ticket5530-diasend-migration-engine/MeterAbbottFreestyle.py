# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE Abbott FreeStyle Mini
# @DEVICE Abbott FreeStyle Flash
# @DEVICE Abbott FreeStyle Freedom
# @DEVICE Abbott FreeStyle Freedom Lite
# @DEVICE Abbott FreeStyle Lite
# @DEVICE Abbott Precision Xceed (new version)

import re
import string
from Generic import *
from Defines import *
from datetime import *

# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------
monthConvertTable = { "jan":1, "feb":2, "mar":3, "apr":4, "may":5, "june":6, \
                      "july":7, "aug":8, "sep":9, "oct":10, "nov":11, "dec":12 } 

BIT_FREESTYLE_CONTROL           = 0x01
BIT_FREESTYLE_TEMP_OUT_OF_RANGE = 0x02
BIT_FREESTYLE_LOST_TIME         = 0x04
BIT_FREESTYLE_LOW_BATTERY       = 0x08
BIT_XCEED_CONTROL               = 0x01
BIT_XCEED_LOST_TIME             = 0x02

def SplitData( inData ):
    """
    Split up the data into correct records
    """
    # First, remove strange characters which for some reason can appear in the transfer
    # @TODO: Understand why they appear
    inData = filter(lambda i: i not in ['\xbe', '\xc6'], inData)

    outList = inData.split('\n')
        
    return outList

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalChecksumRecord( line ):
    """
    Evaluate a Freestyle Mini checksum record. Extract groups : 
    group 1 : checksum
    
    Note : This is a very basic test. Also use row number to be sure.
    """
    m = re.match( r'0x([\d|a-f]{4,4})  END', line, re.IGNORECASE )
    return m

def EvalSerialRecord( line ):
    """
    Evaluate a Freestyle Mini serial nr record. Extract groups : 
    group 1 : serial number
    
    Note : This is a very basic test. Also use row number to be sure.
    """
    
    # The old freestyle flash has of some reason different order on \r and \n in the
    # beginning and in the end of the header, so we allow \r infront of the serial number.
    m = re.match( r'\r*([\w-]{12,})', line, re.IGNORECASE )
    return m

def EvalSwVersionRecord( line ):
    """
    Evaluate a Freestyle Mini software version record. Extract groups : 
    group 1 : serial number
    """
    m = re.match( r'(\d*\.[\d\s]*-P)', line, re.IGNORECASE )
    return m
    
def EvalNrResultsRecord( line ):
    """
    Evaluate a Freestyle Mini number results record. Extract groups : 
    group 1 : number results
    """
    
    # On the older freestyle flash the formatting of this line is xxx\n\r\n
    # On the newer ones the formatting is xxx\r\n\n
    # So we allow arbritrary number of \r in the end of the string.
    m = re.match( r'(\d{3,3}|Log Empty)\r*$', line, re.IGNORECASE )
    return m

def EvalResultRecord( line ):
    """
    Evaluate a Freestyle Mini (or new Xceed) result record. Extract groups : 
    group 1 : value or HI|LO
    group 2 : month
    group 3 : date (DD)
    group 4 : year (YYYY)
    group 5 : hour (HH)
    group 6 : minute (MM)
    group 7 : Freestyle:Cal code Xceed: K or G
    group 8 : status flags 
    """
    m = re.match( r"""
        ([\d|HI|LO]+)\s+
        (jan|feb|mar|apr|may|june|july|aug|sep|oct|nov|dec|tst|XXXX)\s+
        (\d{2,2})\s+(\d{4,4})\s+
        (\d{2,2}):(\d{2,2})\s+
        (\d{2}|K|G)\s+
        0x([0-9a-fA-F]{2})
        """, line, re.IGNORECASE + re.VERBOSE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAbbottFreestyleSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(1)
    return res

def EvalAbbottFreestyleDeviceModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with model
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        if m.group(1)[0] == "X":
            res[ "device_model" ] = "Precision Xceed"
        else:
            res[ "device_model" ] = "Freestyle"
    return res



def EvalAbbottFreestyleNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results. 
    """
    res = {}
    m = EvalNrResultsRecord( line )
    if m:
        try:
            # Adding result record for bg unit setting
            res[ "meter_nr_results" ] = string.atoi( m.group(1), 10 ) + 1
        except ValueError:
            if ('Log Empty' in m.group(1)):
                res[ "meter_nr_results" ] = 0
            else:
                res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }  
    return res
    
def EvalAbbottFreestyleUnitRecord( line ):
    """
    Always return mg/dl 
    """
    res = { "meter_unit":"mg/dL" }
    return res

def EvalAbbottFreestyleResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        if m.group(1)[2] == "M":
            res[ SETTING_BG_UNIT] = SETTING_BG_UNIT_MMOLL
        else:
            res[ SETTING_BG_UNIT ] = SETTING_BG_UNIT_MGDL
        return { SETTINGS_LIST: res }

    m = EvalResultRecord( line )
    if m:
        # A value that contains a invalid time/date field (month is XXXX) should be ignored
        # Note - this probably corresponds to having the "LOST_TIME" flag set as well
        if (m.group(2) == "XXXX"):
            return {"null_result":True}
            
        try:
            try:
                res[ ELEM_TIMESTAMP ] = datetime(int(m.group(4)), monthConvertTable[m.group(2).lower()], int(m.group( 3 )), int(m.group(5)), int(m.group(6)) )
            except:
                res[ ELEM_TIMESTAMP ] = None
                
            _flags = []
            res["meter_flaglist"] = _flags
            record_flag = int(m.group(8), 16)

            # The new Xceeds (with Freestyle protocol) has a different coding for fields 7 & 8
            if m.group(7) in ["K", "G"]:
                if m.group(7) == "K":
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_KETONES
                else:
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                
                # Flags for (new) Xceed
                if record_flag & BIT_XCEED_CONTROL:
                    _flags.append(FLAG_RESULT_CTRL)
                if record_flag & BIT_XCEED_LOST_TIME:
                    _flags.append(FLAG_LOST_TIME)
            
            else:
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                
                # Flags for Freestyle
                if record_flag & BIT_FREESTYLE_CONTROL:
                    _flags.append(FLAG_RESULT_CTRL)
                if record_flag & BIT_FREESTYLE_TEMP_OUT_OF_RANGE:
                    _flags.append(FLAG_RESULT_OUTSIDE_TEMP)
                if record_flag & BIT_FREESTYLE_LOST_TIME:
                    _flags.append(FLAG_LOST_TIME)
                if record_flag & BIT_FREESTYLE_LOW_BATTERY:
                    _flags.append(FLAG_LOW_BATTERY)
            
            try:
                res[ ELEM_VAL ] = float( m.group(1) )
            except ValueError:
                res[ ELEM_VAL ] = 0
                if ('HI' in m.group(1)):
                    _flags.append(FLAG_RESULT_HIGH)
                elif ('LO' in m.group(1)):
                    _flags.append(FLAG_RESULT_LOW)
                else:
                    res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }
                    
        except KeyError:
                res = { "error_code":ERROR_CODE_DATE_TIME, "fault_data":line }

    return res

def EvalAbbottFreestyleChecksumRecord( line, record ):
    """
    Evaluate checksum of result record. Not applicable for this meter.
    """
    return True

def EvalAbbottFreestyleChecksumFile( inList ):
    """
    Evaluate checksum of result record.
    """
    # calculate checksum
    startIndex = -1
    endIndex = -1
    checkSumInFile = -1

    # find serial record
    for i in range(len(inList)):
        m = EvalSerialRecord(inList[i])
        if m != None:
            startIndex = i - 1
            break

    if startIndex < 0:
        return False

    # find number result record
    # This is needed since no checksum is sent if the meter
    # didn't send any result, so in that case return true.
    for i in range(len(inList)):
        m = EvalAbbottFreestyleNrResultsRecord(inList[i])
        if m.has_key("meter_nr_results"):
            if m[ "meter_nr_results" ] == 0:
                return True
            else:
                break

    # find checksum
    for i in range(len(inList)):
        m = EvalChecksumRecord(inList[i])
        if m:
            endIndex = i
            checkSumInFile = string.atoi( m.group(1), 16 )
            break

    if endIndex < 0:
        return False

    # calculate checksum
    checkSum = 0
    for i in range( startIndex, endIndex ):
        for c in inList[i]:
            checkSum = checkSum + ord(c)
        checkSum = checkSum + 0x0a   # must add eol

    # only 16-bit in checksum
    checkSum = checkSum & 0xffff
        
    if checkSum <> checkSumInFile:
        return False
    else:
        return True

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectAbbottFreestyle( inList ):
    """
    Detect if data comes from a Freestyle.
    """
    return DetectDevice( 'AbbottFreestyle', inList, DEVICE_METER );

def AnalyseAbbottFreestyle( inData ):
    """
    Analyse Freestyle Mini
    """

    inList = SplitData(inData)
    
    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ]    = EvalAbbottFreestyleDeviceModelRecord
    d[ "eval_serial_record" ]   = EvalAbbottFreestyleSerialRecord
    d[ "eval_unit"]             = EvalAbbottFreestyleUnitRecord
    d[ "eval_result_record" ]   = EvalAbbottFreestyleResultRecord
    d[ "eval_checksum_record" ] = EvalAbbottFreestyleChecksumRecord
    d[ "eval_checksum_file" ]   = EvalAbbottFreestyleChecksumFile
    d[ "eval_nr_results" ]      = EvalAbbottFreestyleNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    #testfile = open('test/testcases/test_data/AbbottFreeStyle/AbbottFreeStyleXceed-new-version.log')
    testfile = open('test/testcases/test_data/AbbottFreeStyle/AbbottFreeStyle-ticket2745-z5201.log')
    
    testcase = testfile.read()
    testfile.close()

    print AnalyseAbbottFreestyle(testcase)
