# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2008 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
import re

def GetNumericVersion( version_string ):
    """
    Converts a string version to a numeric version
    
    For instance R2c -> 202
    """
    m = re.match(r'[R|P](\d)([a-z])', version_string, re.IGNORECASE)
    if m:
        return int(m.group(1)) * 100 + ord(m.group(2)) - ord('a')
    else:
        return None

def SplitCount(s, count):
    """
    Splits a string s into a list of strings, each chunk has count length.
    """
    return [''.join(x) for x in zip(*[list(s[z::count]) for z in range(count)])]

def RemoveNullTermination(string):
    """
    Remove trailing nulls from incoming string
    """
    # The serial is null terminated, cut it at the first null
    null_idx = string.find("\x00")
    if (null_idx >= 0):
        return string[:null_idx]
    else:
        return string
