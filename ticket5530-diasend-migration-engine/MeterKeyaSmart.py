# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2016 Diasend AB, http://diasend.com
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE KEYA Smart
# 
import re
import itertools
import Common
import struct
import datetime
import csv
import decimal

from Generic import *
from Defines import *

class KeyaSmartAnalyser(object):

    def __init__(self, data):

        self.results = []

        self.results.append({ELEM_DEVICE_MODEL: 'Inside Biometrics KEYA Smart'})

        # 051221-035,29/02/2016,21:00:00,2.5 mmol/L,0.5 mmol/L,Fasting,Below Target,In Range

        reader = csv.reader(data.splitlines())

        # CSV file header
        header_line = reader.next()

        if header_line != ['Serial Number', 'Date', 'Time', 'Glucose', 'Ketone', 'User Tag', 'Glucose Target', 'Ketone Target']:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid CSV format - incorrect header')

        found_serial = False

        for line in reader:

            result_glucose = {}
            result_glucose[ELEM_VAL_TYPE]  = VALUE_TYPE_GLUCOSE
            result_glucose[ELEM_FLAG_LIST] = []
        
            result_ketone = {}
            result_ketone[ELEM_VAL_TYPE]  = VALUE_TYPE_KETONES
            result_ketone[ELEM_FLAG_LIST] = []

            # ['051221-035', '14/08/2015', '01:00:00', '2.2 mmol/L', '0.2 mmol/L', 'Pre Meal', 'Below Target', 'In Range']
            if not found_serial:
                found_serial = True
                self.results.append({ELEM_DEVICE_SERIAL: line[0]})

            try:
                dt = datetime.strptime('%s %s' % (line[1], line[2]), '%d/%m/%Y %H:%M:%S')
            except ValueError, e:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid date/time %s %s' % (line[1], line[2]))

            result_glucose[ELEM_TIMESTAMP] = dt
            result_ketone[ELEM_TIMESTAMP]  = dt

            def cb_mmol(result, s):
                result[ELEM_VAL] = int(decimal.Decimal(s) * VAL_FACTOR_GLUCOSE_MMOL)
                result[ELEM_DEVICE_UNIT] = "mmol/L"                

            def cb_mg(result, s):
                value = int(decimal.Decimal(s))
                result[ELEM_VAL] = value * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                result[ELEM_DEVICE_UNIT] = "mg/dL"

            def cb_high(result, s):
                result[ELEM_VAL] = 0
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                result[ELEM_DEVICE_UNIT] = "mmol/L"

            def cb_low(result, s):
                result[ELEM_VAL] = 0
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                result[ELEM_DEVICE_UNIT] = "mmol/L"

            regex_glucose_cbs = (
                (r'(\d{1,3}\.\d{1}) mmol/L', cb_mmol),
                (r'(\d{1,4}) mg/dL', cb_mg),
                (r'(HIGH)', cb_high),
                (r'(LOW)', cb_low))

            found = False
            for regex_cb in regex_glucose_cbs:
                m = re.match(regex_cb[0], line[3])
                if m:
                    found = True
                    regex_cb[1](result_glucose, m.group(1))

            if not found:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid glucose %s' % (line[3]))

            user_tag_flags = []

            if len(line[5]) > 0:

                user_tags = (
                    ('Pre Meal', FLAG_BEFORE_MEAL),
                    ('Post Meal', FLAG_AFTER_MEAL),
                    ('Fasting', FLAG_FASTING),
                    ('Insulin', FLAG_INSULIN),
                    ('Sick Day', FLAG_SICK_DAY),
                    ('Exercise', FLAG_BEFORE_EXERCISE),
                    ('Control Solution', FLAG_RESULT_CTRL))

                for user_tag in user_tags:
                    if line[5] == user_tag[0]:
                        user_tag_flags.append(user_tag[1])
                        break

                if len(user_tag_flags) == 0:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid flag %s' % (line[5]))

            result_glucose[ELEM_FLAG_LIST].extend(user_tag_flags)

            if len(line[6]) > 0:

                glucose_targets = (
                    ('On Target', FLAG_DEVICE_GLUCOSE_ON_TARGET),
                    ('Below Target', FLAG_DEVICE_GLUCOSE_BELOW_TARGET),
                    ('Above Target', FLAG_DEVICE_GLUCOSE_ABOVE_TARGET))

                found = False
                for glucose_target in glucose_targets:
                    if line[6] == glucose_target[0]:
                        found = True
                        result_glucose[ELEM_FLAG_LIST].append(glucose_target[1])
                        break

                if not found:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid glucose target %s' % (line[6]))

            self.results.append(result_glucose)

            if len(line[4]) > 0:
                regex_ketone_cbs = (
                    (r'(\d{1,3}\.\d{1}) mmol/L', cb_mmol),
                    (r'(HIGH)', cb_high))

                found = False
                for regex_cb in regex_ketone_cbs:
                    m = re.match(regex_cb[0], line[4])
                    if m:
                        found = True
                        regex_cb[1](result_ketone, m.group(1))

                if not found:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid ketone %s' % (line[4]))

                result_ketone[ELEM_FLAG_LIST].extend(user_tag_flags)

                if len(line[7]) > 0:

                    ketone_targets = (
                            ('In Range', FLAG_DEVICE_KETONE_IN_RANGE),
                            ('Elevated Ketone', FLAG_DEVICE_KETONE_ELEVATED),
                            ('High Ketone', FLAG_DEVICE_KETONE_HIGH))

                    found = False
                    for ketone_target in ketone_targets:
                        if line[7] == ketone_target[0]:
                            found = True
                            result_ketone[ELEM_FLAG_LIST].append(ketone_target[1])
                            break

                    if not found:
                        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid ketone target %s' % (line[7]))

                self.results.append(result_ketone)

        self.results.append({ELEM_NR_RESULTS: len(self.results)-2})

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalKeyaSmartSerialRecord(line):
    return line if ELEM_DEVICE_SERIAL in line else {}
    
def EvalKeyaSmartModelRecord(line):
    return line if ELEM_DEVICE_MODEL in line else {}

def EvalKeyaSmartResultRecord(line):
    if any(k in line for k in (SETTINGS_LIST, ELEM_VAL)):
        return line
    return {}

def EvalKeyaSmartChecksumRecord(line, record):
    return True

def EvalKeyaSmartChecksumFile(line):
    return True

def EvalKeyaSmartNrResultsRecord(line):
    return line if ELEM_NR_RESULTS in line else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterKeyaSmart(inList):
    """
    Detect if data comes from a Keya Smart.
    """
    return DetectDevice('keya_smart', inList, DEVICE_METER);

def AnalyseMeterKeyaSmart(inData):
    """
    Analyse data from Keya Smart.
    """

    model_serial_list = ['', '']
    
    res_list = []

    try:

        d = {}

        d[ "eval_device_model_record" ]  = EvalKeyaSmartModelRecord
        d[ "eval_serial_record" ]        = EvalKeyaSmartSerialRecord
        d[ "eval_result_record" ]        = EvalKeyaSmartResultRecord
        d[ "eval_checksum_record" ]      = EvalKeyaSmartChecksumRecord
        d[ "eval_checksum_file" ]        = EvalKeyaSmartChecksumFile
        d[ "eval_nr_results" ]           = EvalKeyaSmartNrResultsRecord

        analyser = KeyaSmartAnalyser(inData)

        res_list = AnalyseGenericMeter(analyser.results, d);

    except Exception as e:

        # Uncomment for developement. 
        import traceback
        traceback.print_exc()
        
        # The 'standard' way is to raise Exception with two parameters, diasend error code and 
        # description.
        if len(e.args) == 2:
            res_list = CreateErrorResponseList(model_serial_list[1], model_serial_list[0], e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            res_list = CreateErrorResponseList(model_serial_list[1], model_serial_list[0], ERROR_CODE_PREPROCESSING_FAILED, 
                ''.join(map(str, e.args)))

    return res_list

if __name__ == "__main__":

    testfiles = (
        'fail/051221-035.log',
        'fail/051221-004.log',
        'KeyaSmart-true-header.log')

    for testfile in testfiles:
        with open('test/testcases/test_data/KeyaSmart/%s' % (testfile), 'r') as f:
            d = f.read()
            results = AnalyseMeterKeyaSmart(d)
            print results[0]['header']
            #for r in results[0]['results']:
            #    print r
