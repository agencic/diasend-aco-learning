# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re
import Buffer

from Defines import *
from Generic import *

MODEL_ABBOTT_FREESTYLE_LIBRE = 'Abbott FreeStyle Libre'

BINARY_DATA_LENGTH_BIT     = 0x80
BINARY_DATA_LENGTH_MASK    = 0x7f
BINARY_COMMAND_GETDBASE    = 0x31  # Binary command to get Database Records
BINARY_COMMAND_GETCFGDATA  = 0x51  # Binary command to get Configuration data
BINARY_COMMAND_GETDATETIME = 0x41  # Binary command to get Date and Time
BINARY_COMMAND_SETDATETIME = 0x42  # Binary command to set Date and Time
BINARY_COMMAND_COMPRESS    = 0x60  # Binary command to set Compress mode
BINARY_RESPONSE_GETDBASE   = 0x35  # Binary response on zero compression Database Records
BINARY_COMMAND_GETSCHEMA   = 0x34  # Binary command to get Database Schema

BINARY_SINGLE_RECORD_COMMANDS = [ BINARY_COMMAND_GETDATETIME, BINARY_COMMAND_SETDATETIME, BINARY_COMMAND_COMPRESS, BINARY_COMMAND_GETCFGDATA ]
BINARY_MULTI_RECORD_COMMANDS = [ BINARY_COMMAND_GETDBASE, BINARY_RESPONSE_GETDBASE, BINARY_COMMAND_GETSCHEMA ]
BINARY_COMMANDS_WITH_PARAM = [ BINARY_COMMAND_GETDBASE, BINARY_RESPONSE_GETDBASE, BINARY_COMMAND_GETSCHEMA, BINARY_COMMAND_GETCFGDATA ]

class AnalyseLibre(object):

    @staticmethod
    def is_of_model(commands):
        for command in commands:
            if len(AnalyseLibre.convert_command_sn(command)) > 0:
                return True
        return False
        
    @staticmethod
    def convert_result_to_diasend(command):
        command_to_extract_dict = {
            'sn'       : AnalyseLibre.convert_command_sn, 
            'event'    : AnalyseLibre.convert_command_event, 
            'arresult' : AnalyseLibre.convert_command_arresult, 
            'hrfmt'    : AnalyseLibre.convert_command_hrfmt, 
            'btsound'  : AnalyseLibre.convert_command_btsound, 
            'ntsound'  : AnalyseLibre.convert_command_ntsound,
            'lang'     : AnalyseLibre.convert_command_lang,
            'gtarget'  : AnalyseLibre.convert_command_gtarget,
            'history'  : AnalyseLibre.convert_command_history,
        }

        if command.get_name() in command_to_extract_dict:
            return command_to_extract_dict[command.get_name()](command)

        return []

    @staticmethod
    def convert_command_sn(command):

        result = []

        if command.get_name() != 'sn':
            return result

        if command.nr_of_records() != 1:
            return result

        m = re.match(r'^(.{1,13})', command.get_records()[0], re.IGNORECASE)
        if m is not None:
            if m.group(1).startswith('JCG') or m.group(1).startswith('JCM') or m.group(1).startswith('JGG') or m.group(1).startswith('JGM'):
                result.append({ELEM_DEVICE_SERIAL:m.group(1)})
                result.append({ELEM_DEVICE_MODEL: MODEL_ABBOTT_FREESTYLE_LIBRE})

        return result

    @staticmethod
    def convert_command_event(command):

        result = []

        if command.get_name() != 'event':
            return result

        # Libre RN,VR,RT,M,D,Y,H,MI,S,V,*
        # Each event has additional data that is currently ignored.

        for record in command.get_records():

            m = re.match(r'^(\d+),([12]),(\d+),(\d{1,2}),(\d{1,2}),(\d{1,2}),(\d{1,2}),(\d{1,2}),(\d{1,2}),([01])(.*)', record, re.IGNORECASE)
            if m is not None:
                keys   = ('rn', 'vr', 'rt', 'm', 'd', 'y', 'h', 'mi', 's', 'v')
                values = map(int, m.groups()[:10])
                items  = dict(zip(keys, values))
                if items['vr'] == 1:
                    event_map = {
                        #32:ALARM_INSULINX_ERROR, 
                        33:ALARM_BATTERY_LOW, 
                        34:ALARM_BATTERY_OUTAGE, 
                        35:ALARM_DATE_CHANGED, 
                        36:ALARM_INSULINX_TIME_LOST, 
                        37:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_1, 
                        38:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_2, 
                        39:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_3, 
                        40:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_4, 
                        #42:ALARM_LIBRE_CLEAR_RESULT_AND_INSULIN_DATABASE,
                        #43:ALARM_LIBRE_CLEAR_SCAN_DATABASE,
                        #44:ALARM_LIBRE_CLEAR_ACTIVATION_DATABASE,
                        #45:ALARM_LIBRE_CLEAR_HISTORICAL_DATABASE,
                        46:ALARM_LIBRE_USER_TIME_CHANGE,
                        47:ALARM_LIBRE_CLEAR_EVENT_LOG,
                        #48:ALARM_LIBRE_RECOVERY,
                        #49:ALARM_LIBRE_MICROCONTROLLER_RESET,
                        #50:ALARM_LIBRE_MASKED_MODE_STATUS,
                        51:ALARM_LIBRE_SENSOR_EXPIRED,
                        #52:ALARM_LIBRE_DATABASE_RECORD_NUMBER_WRAP
                        }
                        
                    if items['rt'] in event_map:
                        dt = datetime(2000+items['y'], items['m'], items['d'], items['h'], items['mi'], items['s'])

                        value = {
                            ELEM_TIMESTAMP : dt,
                            ELEM_VAL_TYPE  : VALUE_TYPE_ALARM,
                            ELEM_VAL       : event_map[items['rt']],
                            ELEM_FLAG_LIST : []
                        }

                        # Check Date-Time-Valid flag
                        if items['v'] == 0:
                            # Time reading is not reliable (often occurs during removal of battery etc)
                            value[ELEM_FLAG_LIST].append(FLAG_LOST_TIME)

                        result.append(value)
                    else:
                        # As certain events should be ignored, we should not fail if not found.
                        pass
                else:
                    # 'vr' flag means event is marked as invalid. We should just ignore such records.
                    pass

        return result

    @staticmethod
    def convert_command_arresult(command):

        result = []

        if command.get_name() != 'arresult':
            return result

        def extract_record_type_012(record_type, body, dt, external_flags):

            def _fuzzy_to_int(value):
                try:
                    return int(value)
                except ValueError:
                    return value

            m1 = re.match(ur'^([012]),([0123]),([0123]),(\d+),([01]),([012345]),([01]),([01]),'
                    ur'([01]),([01]),(\d+),([01]),(\d+),([012345]),(\d+),([01]),([01]),(\d+),(\d+),'
                    ur'(\d+),"([\u0020-\ufeff]*)","([\u0020-\ufeff]*)","([\u0020-\ufeff]*)","([\u0020-\ufeff]*)","([\u0020-\ufeff]*)","([\u0020-\ufeff]*)"(?:,(.*))?', body, re.IGNORECASE)
            if m1 is not None:
                keys = ('k','t','rstat','rslt','act','trnd','ex','md','rair',
                    'lair','stag','rid','rip','ga','la','ft','fdr','f1','f2',
                    'dq','tag0','tag1','tag2','tag3','tag4','tag5');
                values = map(_fuzzy_to_int, m1.groups()[:26])
                items  = dict(zip(keys, values))

                value = {
                    ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE if items['k'] in [0,2] else VALUE_TYPE_KETONES,
                    ELEM_TIMESTAMP : dt,
                    ELEM_FLAG_LIST : list(external_flags)
                }
                result.append(value)
                
                # glucose strip
                if items['k'] == 0:
                    if items['rslt'] < 20:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                    elif items['rslt'] > 500:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                # ketone strip
                elif items['k'] == 1:
                    if items['rslt'] > 144:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                # glucose scan - manual
                elif items['k'] == 2:
                    if items['rslt'] < 40:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                        value[ELEM_FLAG_LIST].append(FLAG_EXTENDED_RANGE)
                    elif items['rslt'] > 500:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                        value[ELEM_FLAG_LIST].append(FLAG_EXTENDED_RANGE)
                    value[ELEM_FLAG_LIST].append(FLAG_CONTINOUS_READING)
                    value[ELEM_FLAG_LIST].append(FLAG_USER_TRIGGERED)

                value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW_TEMP)     if items['t'] == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH_TEMP)    if items['t'] == 2 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_OUTSIDE_TEMP) if items['t'] == 3 else None

                value[ELEM_FLAG_LIST].append(FLAG_MEDIUM_EXERCISE)  if items['ex'] == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_MEDICATION)       if items['md'] == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_MEAL)             if items['ft'] == 1 else None

                # Adjust mg -> mmol factor.
                value[ELEM_VAL] = items['rslt'] * 18.0 / 18.016
                
                # We might have carbs?
                if items['fdr'] == 1:

                    value = {
                        ELEM_VAL_TYPE  : VALUE_TYPE_CARBS,
                        ELEM_TIMESTAMP : dt,
                        ELEM_FLAG_LIST : list(external_flags)
                    }
                    result.append(value)

                    if record_type == 0: 
                        # Food by servings. 
                        # F1 - number of servings in 0.5 step
                        # F2 - carbs per serving in 0.5 g
                        value[ELEM_VAL] = int(round(items['f1'] * items['f2'] * 0.5 * 0.5))

                    elif record_type == 1:
                        # Food by meal type. 
                        # F1 - carbs in g
                        # F2 - meal type 
                        value[ELEM_VAL] = items['f1']
                        value[ELEM_FLAG_LIST].append(FLAG_BREAKFAST) if items['f2'] == 1 else None 
                        value[ELEM_FLAG_LIST].append(FLAG_LUNCH)     if items['f2'] == 2 else None 
                        value[ELEM_FLAG_LIST].append(FLAG_DINNER)    if items['f2'] == 3 else None 

                    elif record_type == 2:
                        # Food by grams of carb
                        value[ELEM_VAL] = items['f1']

                # We might have long-acting insulin?
                if items['la'] > 0:

                    value = {
                        ELEM_VAL_TYPE  : VALUE_TYPE_INS_BOLUS_BASAL,
                        ELEM_TIMESTAMP : dt,
                        ELEM_VAL       : items['la'] * 0.5 * VAL_FACTOR_BOLUS, 
                        ELEM_FLAG_LIST : list(external_flags)
                    }                    

                    value[ELEM_FLAG_LIST].append(FLAG_INSULIN_IMPACT_LONG)
                    value[ELEM_FLAG_LIST].append(FLAG_MANUAL)

                    result.append(value)

                # We might have rapid-acting insulin?
                if items['rid'] == 1:

                    m2 = re.match(r'^(\d),.*', m1.group(27), re.IGNORECASE)

                    if m2 is not None:
                        irt = int(m2.group(1))

                        # Manual rapid-acting insulin.
                        if irt != 6:
                            m2 = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),([01]),(\d+)', m1.group(27), re.IGNORECASE)
                            if m2 is not None:
                                keys = ('irt','imm','idd','iyy','ihh','imi','iss','idtv','mrslt')
                                values = map(int, m2.groups())
                                items  = dict(zip(keys, values))

                                try:
                                    dt2 = datetime(2000+items['iyy'], items['imm'], items['idd'], items['ihh'], items['imi'], items['iss'])
                                except ValueError:
                                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (row))
                                
                                flags = [FLAG_LOST_TIME] if items['idtv'] == 0 else []

                                value = {
                                    ELEM_VAL_TYPE  : VALUE_TYPE_INS_BOLUS,
                                    ELEM_TIMESTAMP : dt2,
                                    ELEM_VAL       : items['mrslt'] * 0.5 * VAL_FACTOR_BOLUS, 
                                    ELEM_FLAG_LIST : flags
                                }                    

                                value[ELEM_FLAG_LIST].append(FLAG_MANUAL)

                                result.append(value)

                        # Calculator rapid-acting insulin
                        elif irt == 6:

                            m2 = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),([01]),(\d+),(-?\d+),(-?\d+),(\d+),(\d+),(\d+),(\d+)', m1.group(27), re.IGNORECASE)
                            if m2 is not None:
                                keys = ('irt','imm','idd','iyy','ihh','imi','iss','idtv','irslt','ior','ic','iob','im','ito','urai')
                                values = map(int, m2.groups())
                                items  = dict(zip(keys, values))

                                try:
                                    dt2 = datetime(2000+items['iyy'], items['imm'], items['idd'], items['ihh'], items['imi'], items['iss'])
                                except ValueError:
                                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (row))

                                secondary = []

                                flags = [FLAG_LOST_TIME] if items['idtv'] == 0 else []

                                value = {
                                    ELEM_TIMESTAMP  : dt2,
                                    ELEM_VAL_TYPE   : VALUE_TYPE_INS_BOLUS,
                                    ELEM_VAL        : items['irslt'] * 0.5 * VAL_FACTOR_BOLUS,
                                    ELEM_FLAG_LIST  : flags,
                                    ELEM_VALUE_LIST : secondary
                                }

                                if items['im'] > 0:
                                    secondary.append({ELEM_VAL : items['im'] * 0.5 * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL})
                                    
                                if items['ic'] != 0:
                                    secondary.append({ELEM_VAL : items['ic'] * 0.5 * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR})

                                if items['iob'] > 0:
                                    secondary.append({ELEM_VAL : items['iob'] * 0.5 * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_IOB})

                                if items['ior'] != 0:
                                    secondary.append({ELEM_VAL : items['ior'] * 0.5 * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_OVERRIDE})
                                
                                # Considered as a manual record
                                flags.append(FLAG_MANUAL)

                                # TODO : unlogged insulin

                                result.append(value)

            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Record type %d (glucose) - invalid body %s' % (record_type, body))

        def extract_record_type_3(record_type, body, dt, external_flags):
            pass
            #
            # TODO : Unlogged insulin currently not supported.
            # 
            # m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),([01]).*$', body, re.IGNORECASE)
            # if m is not None:
            #     keys = ('urai','emm','edd','eyy','ehh','emi','ess','ev')
            #     values = map(int, m.groups())
            #     items  = dict(zip(keys, values))
            #     # TODO - ev
            #     try:
            #         edt = datetime(2000+items['eyy'], items['emm'], items['edd'], items['ehh'], items['emi'], items['ess'])
            #     except ValueError:
            #         raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (row))

        def extract_record_type_4(record_type, body, dt, external_flags):
            m = re.match('r^([012]),([0123]),([0123]),(\d+).*$', body, re.IGNORECASE)
            if m is not None:
                keys   = ('k','t','rstat','rslt')
                values = map(_fuzzy_to_int, m1.groups()[:26])
                items  = dict(zip(keys, values))

                value = {
                    ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE if items['k'] in [0,2] else VALUE_TYPE_KETONES,
                    ELEM_TIMESTAMP : dt,
                    ELEM_FLAG_LIST : list(external_flags)
                }
                result.append(value)
                
                # glucose strip
                if items['k'] == 0:
                    if items['rslt'] < 20:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                    elif items['rslt'] > 500:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                # ketone strip
                elif items['k'] == 1:
                    if items['rslt'] > 144:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                # glucose scan - manual
                elif items['k'] == 2:
                    if items['rslt'] < 40:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                        value[ELEM_FLAG_LIST].append(FLAG_EXTENDED_RANGE)
                    elif items['rslt'] > 500:
                        value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                        value[ELEM_FLAG_LIST].append(FLAG_EXTENDED_RANGE)
                    value[ELEM_FLAG_LIST].append(FLAG_CONTINOUS_READING)
                    value[ELEM_FLAG_LIST].append(FLAG_USER_TRIGGERED)

                value[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL)

                value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW_TEMP)     if items['t'] == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH_TEMP)    if items['t'] == 2 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_OUTSIDE_TEMP) if items['t'] == 3 else None

                # Adjust mg -> mmol factor.
                value[ELEM_VAL] = items['rslt'] * 18.0 / 18.016

        total_number_records = 0

        for record in command.get_records():

            # Look for result record (only header now - look deeper in ExtractRecordTypeX functions).
            m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(.*)$', record, re.IGNORECASE)
            if m is not None:
                keys   = ('rn','rt','mm','dd','yy','hh','mi','ss','dtv')
                values = map(int, m.groups()[:9])
                items  = dict(zip(keys, values))

                total_number_records += 1

                flags = [FLAG_LOST_TIME] if items['dtv'] == 0 else []

                try:
                    dt = datetime(2000+items['yy'], items['mm'], items['dd'], items['hh'], items['mi'], items['ss'])
                except ValueError:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (row))

                record_types = {
                    0 : extract_record_type_012,
                    1 : extract_record_type_012,
                    2 : extract_record_type_012, 
                    # 3 : extract_record_type_3, # Unlogged insulin - not supported at the moment
                    4 : extract_record_type_4
                }

                if items['rt'] in record_types:
                    record_types[items['rt']](items['rt'], m.group(10), dt, flags)

            # Not a record - maybe there are no result records in this device.
            if m is None:
                m = re.match( r'^(Log Empty)', record, re.IGNORECASE)
                if m is not None:
                    pass

            # Or it could be a number of results record.
            if m is None:
                m = re.match(r'^(\d+),([\da-fA-F]{8,8})', record, re.IGNORECASE)
                if m is not None:
                    # Internal check - we are able to validate the number of data points which 
                    # is different from total number of records (which includes events etc).
                    if int(m.group(1)) != total_number_records:
                        raise Exception(ERROR_CODE_NR_RESULTS, 'Incorrect number of results %d != %d' % (int(m.group(1)), total_number_records))

        return result

    @staticmethod
    def convert_command_history(command):
        result = []

        if command.get_name() != 'history':
            return result

        for record in command.get_records():

            m = re.match(r'^(\d+),(12),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),([01]),([01]),([01]),([01]),([01]),(\d+),(\d+),(\d+).*$', record, re.IGNORECASE)
            if m is not None:
                keys   = ('rn','tt','mm','dd','yy','hh','mi','ss','dv','ra','fd','tc','fs','gv','lc','dq')
                values = map(int, m.groups())
                items  = dict(zip(keys, values))

                flags = [FLAG_LOST_TIME] if items['dv'] == 0 else []

                try:
                    dt = datetime(2000+items['yy'], items['mm'], items['dd'], items['hh'], items['mi'], items['ss'])
                except ValueError:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (record))

                value = {
                    ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE,
                    ELEM_TIMESTAMP : dt,
                    ELEM_FLAG_LIST : flags
                }
                result.append(value)
                
                if items['gv'] < 40:
                    value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                    value[ELEM_FLAG_LIST].append(FLAG_EXTENDED_RANGE)
                elif items['gv'] > 500:
                    value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                    value[ELEM_FLAG_LIST].append(FLAG_EXTENDED_RANGE)
                value[ELEM_FLAG_LIST].append(FLAG_CONTINOUS_READING)

                value[ELEM_FLAG_LIST].append(FLAG_ABBOTT_FREESTYLE_LIBRE_RAPID_ACTING_INSULIN) if items['ra'] == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_MEAL) if items['fd']                                        == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_ABBOTT_FREESTYLE_LIBRE_TIME_CHANGE) if items['tc']          == 1 else None
                value[ELEM_FLAG_LIST].append(FLAG_ABBOTT_FREESTYLE_LIBRE_FIRST_SENSOR_POINT) if items['fs']   == 1 else None

                # Adjust mg -> mmol factor.
                value[ELEM_VAL] = items['gv'] * 18.0 / 18.016

        return result

    @staticmethod
    def extract_setting(command, name, setting_list=None):
        '''
        Validate response and if setting_list != None create 
        a setting list (diasend format). If setting_list == None
        return raw extracted list of values.
        '''

        result = []

        if command.get_name() != name:
            return result

        if command.nr_of_records() != 1:
            return result

        values = command.get_records()[0].strip().split(',')
        values = map(int, values)

        if (setting_list is not None) and (len(setting_list) == len(values)):
            for idx, value in enumerate(values):
                if value in setting_list[idx][1]:
                    result.append({SETTINGS_LIST: {setting_list[idx][0]: setting_list[idx][1][value]}})
        else:
            return values

        return result

    @staticmethod
    def convert_command_hrfmt(command):
        return AnalyseLibre.extract_setting(command, 'hrfmt', ((SETTING_TIME_FORMAT, {0:1, 1:0}),))
 
    @staticmethod
    def convert_command_btsound(command):
        return AnalyseLibre.extract_setting(command, 'btsound', ((SETTING_SOUND_ENABLED, {0:0, 1:1}), (SETTING_FREESTYLE_LIBRE_VOLUME, {0:0, 1:1})))

    @staticmethod
    def convert_command_ntsound(command):
        result = []
        values = AnalyseLibre.extract_setting(command, 'ntsound')
        # SETTING_NOTIFICATION_MODE 0 - Audio, 1 - Vibration, 2 - Both
        if values == [1,0]:
            result.append({SETTINGS_LIST: {SETTING_NOTIFICATION_MODE: 0}})
        elif values == [0,1]:
            result.append({SETTINGS_LIST: {SETTING_NOTIFICATION_MODE: 1}})
        elif values == [1,1]:
            result.append({SETTINGS_LIST: {SETTING_NOTIFICATION_MODE: 2}})

        return result

    @staticmethod
    def convert_command_gtarget(command):
        result = []
        values = AnalyseLibre.extract_setting(command, 'gtarget')

        if len(values) == 2:
            result.append({SETTINGS_LIST: {
                SETTING_BG_GOAL_MIN: (values[1] * 1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 
                SETTING_BG_GOAL_UPPER: (values[0] * 1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL
                }})

        return result

    @staticmethod
    def convert_command_lang(command):

        lang_d = {
            0: 'Portuguese',
            1: 'EU Spanish',
            2: 'US English',
            3: 'German',
            4: 'EU French',
            5: 'Italian',
            6: 'UK English',
            7: 'Canadian French',
            8: 'Simplified Chinese',
            9: 'Japanese',
            10: 'Dutch',
            11: 'Swedish',
            12: 'US Spanish',
            13: 'Norwegian',
            14: 'Danish',
            15: 'Finnish',
            16: 'Greek',
            17: 'Polish',
            18: 'EU Portuguese',
            19: 'Russian',
            20: 'Turkish',
            21: 'Arabic',
            22: 'Hebrew'}

        return AnalyseLibre.extract_setting(command, 'lang', ((SETTING_LANGUAGE, lang_d),))


# ---

# Convert binary blocks in file to text format
# Replaces all binary blocks in data with text formatted data
class ConvertBinaryToText(object):
    binary_marker = "%binary\r\n"

    # Give the data to convert to pure text format
    # @param inData 
    def __init__(self, inData):
        # Time conversion offset
        self.time_offset = 0
        # Insulin records extracted from binary data
        self.insulin_records = dict()
        self.buffer = Buffer.Buffer(inData)

    # Data record header of the database tables 0, 1, 6 and 7
    # See DOC33891_rev-B attachment 1
    #   Worksheet RecordHeader
    # @return Tuple of (record number, type, valid, reader time, user time offset, year, month, day, hour, minute, second)
    def record_header(self):
         # Extract record number, type, valid flag, reader time and user time offset
         (id0, id8, id7, id9, id10 )  = self.buffer.get_struct("<HBBIi")
         id7 = id7 >> 7
         # The time is calculated by using time conversion given from Factory config
         # Reader time + User time offset + Time conversion
         unixtime = id9 + id10 + self.time_offset
         dd = int(datetime.utcfromtimestamp(unixtime).strftime('%d'))
         mm = int(datetime.utcfromtimestamp(unixtime).strftime('%m'))
         yy = int(datetime.utcfromtimestamp(unixtime).strftime('%y'))
         hh = int(datetime.utcfromtimestamp(unixtime).strftime('%H'))
         mi = int(datetime.utcfromtimestamp(unixtime).strftime('%M'))
         ss = int(datetime.utcfromtimestamp(unixtime).strftime('%S'))
         return (id0, id8, id7, id9, id10, yy, mm, dd, hh, mi, ss)
    
    # Commands as the are in text format
    # @param command Binary command byte
    # @param param Binary parameter
    # @return Text command for given binary command and parameter
    def get_command_str(self, command_byte, param):
        if command_byte == BINARY_COMMAND_GETDBASE:
            if param == 0:
                return "$arresult?\r\n"
            if param == 6:
                return "$history?\r\n"
            if param == 7:
                return "$event?\r\n"
        return ""

    # Fabricate a checksum for data block
    # @param inData Data to create checksum for
    # @return Calculated checksum number
    def fabricate_checksum(self, inData):
       checksum = 0
       records = inData.splitlines(True)
       for record in records:
          for c in record:
              checksum += ord(c)
       return checksum
 
    # Decode Factory Configurations
    # See DOC33891_rev-B attachment 2
    #   worksheet FactoryConfig
    # @return Empty string
    def decode_command51_param1(self):
        self.buffer.consume(156)
        # We need the Time conversion value to calculate correct time
        (id1072,) = self.buffer.get_struct("<I")
        self.time_offset = id1072
        self.stop = True
        return ""

    # Decode Rapid Action insulin records
    # See DOC33891_rev-B attachment 1
    #   Worksheet Insulin_Calc, Manual_RAI and Insulin_Wrap
    # @return Empty string
    def decode_command31_param1(self):
        (id0, id8, id7, id9, id10, yy, mm, dd, hh, mi, ss) = self.record_header()
        id78 = 0
        id70 = 0
        id71 = 0
        id72 = 0
        id73 = 0
        id74 = 0
        id75 = 0
        id76 = 0
        # Insulin_Calc
        if id8 == 6:
            (id70, id71, id72, id73, id74, id75) = self.buffer.get_struct("<HHhHHH")
            id76 = (id75 >> 7) & 0x1f
            id75 = (id75 << 2) | (id74 >> 14)
            id74 = id74 & 0x1f
        # Manual_RAI
        if id8 == 7:
            (id78,) = self.buffer.get_struct("<H")
        # Insulin_Wrap
        if id8 == 14:
            (id79,) = self.buffer.get_struct("<I")
        # Save the insulin records to be used when create the arresult data
        self.insulin_records[id0] = {'id8': id8, 'mm': mm, 'dd': dd, 'yy': yy, 'hh': hh, 'mi': mi, 'ss': ss, 
                                     'id7': id7, 'id9': id9, 'id78': id78, 'id70': id70, 'id71': id71, 'id72': id72, 
                                     'id73': id73, 'id74': id74, 'id75': id75, 'id76': id76}
        return ""

    # Decode Result records
    # See DOC33891_rev-B attachment 1
    #   Worksheet Glucose_Ketone_Serving, Glucose_Ketone_Meal, Glucose_Ketone_Carbs, Unlogged_Insulin, Control_Sol_Test, 
    #             Time_Change_result, ResultRecordWrap
    # @return Text decoded to same format as in DOC33891_rev-B chapter 8.5.1 $arresult?
    def decode_command31_param0(self):
        output = ""
        (id0, id8, id7, id9, id10, yy, mm, dd, hh, mi, ss) = self.record_header()
        # Glucose_Ketone_Serving, Glucose_Ketone_Meal, Glucose_Ketone_Carbs
        if id8 in (0,1,2):
            (id20, id24, id32, id34, id39, id57, id49) = self.buffer.get_struct("<HHHHHHH")
            id50 = str(self.buffer.get_slice(14)).rstrip('\0') 
            id51 = str(self.buffer.get_slice(14)).rstrip('\0') 
            id52 = str(self.buffer.get_slice(14)).rstrip('\0') 
            id53 = str(self.buffer.get_slice(14)).rstrip('\0') 
            id54 = str(self.buffer.get_slice(14)).rstrip('\0') 
            id55 = str(self.buffer.get_slice(14)).rstrip('\0') 
    
            id61 = (id20 >> 10) & 0x3
            id23 = (id20 >> 14) & 0x3
            id20 = id20 & 0x3ff
    
            id30 = (id24 >> 12) & 0x7
            id29 = (id24 >> 11) & 0x1
            id28 = (id24 >> 10) & 0x1
            id26 = (id24 >> 9) & 0x1
            id25 = (id24 >> 8) & 0x1
            id24 = id24 & 0x3f

            id33 = (id32 >> 15) & 0x1
            id32 = id32 & 0x1fff
        
            id35 = (id34 >> 13) & 0x7
            id34 = id34 & 0x1ff
        
            id41 = (id39 >> 15) & 0x1
            id40 = (id39 >> 14) & 0x1
            id39 = id39 & 0xfff
            id36 = id39 & 0x3f
            id37 = (id39 >> 6) & 0x3f
            id38 = (id39 >> 10) & 0x03
            id39 = id39 & 0x3ff

            # Glucose_Ketone_Serving
            if id8 == 0:
                f1 = id37
                f2 = id36
            # Glucose_Ketone_Meal
            if id8 == 1:
                f1 = id39
                f2 = id38
            # Glucose_Ketone_Carbs
            if id8 == 2:
                f1 = id39
                f2 = 0
    
            output = output + "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"" % \
                              (id0, id8, mm, dd, yy, hh, mi, ss, id7, id23, 0, id61, id20, 1, id30, id29, id28, id26, id25, id24, \
                               id33, id32, id35, id34, id41, id40, f1, f2, id57, id50, id51, id52, id53, id54, id55)
            
            # If there are insulin connected to this record, add the data from insulin records dictonary
            if id26 == 1:
                if id32 in self.insulin_records.keys():
                    # Collect the record
                    in_rec = self.insulin_records[id32]
                    # Manual_RAI -> see decode_command31_param1
                    if in_rec['id8'] != 6:
                        output = output + ",%d,%d,%d,%d,%d,%d,%d,%d,%d" % \
                                          (in_rec['id8'], in_rec['mm'], in_rec['dd'], in_rec['yy'], in_rec['hh'], in_rec['mi'], in_rec['ss'], in_rec['id7'], in_rec['id78'])
                    else:
                        # Insulin_Calc -> see decode_command31_param1
                        output = output + ",%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d" % \
                                          (in_rec['id8'], in_rec['mm'], in_rec['dd'], in_rec['yy'], in_rec['hh'], in_rec['mi'], in_rec['ss'], in_rec['id7'], \
                                           in_rec['id70'], in_rec['id71'], in_rec['id72'], in_rec['id73'], in_rec['id74'], in_rec['id76'], in_rec['id75'])
                else:
                    print "No insulin record %d found" % (id32)
            output = output + "\r\n"
        # Control_Sol_Test
        if id8 == 4:
            (id20, ) = self.buffer.get_struct("<H")

            id61 = (id20 >> 10) & 0x03
            id23 = (id20 >> 14) & 0x01
            id20 = id20 & 0x3ff

            output = output + "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n" % (id0, id8, mm, dd, yy, hh, mi, ss, id7, id23, 0, id61, id20)
        # Time_Change_result
        if id8 == 5:
            (id42, id43, id44) = self.buffer.get_struct("<IIH")
            unixtime = id42 + id43 + self.time_offset 
            odd = int(datetime.utcfromtimestamp(unixtime).strftime('%d'))
            omm = int(datetime.utcfromtimestamp(unixtime).strftime('%m'))
            oyy = int(datetime.utcfromtimestamp(unixtime).strftime('%y'))
            ohh = int(datetime.utcfromtimestamp(unixtime).strftime('%H'))
            omi = int(datetime.utcfromtimestamp(unixtime).strftime('%M'))
            oss = int(datetime.utcfromtimestamp(unixtime).strftime('%S'))
            output = output + "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n" % \
                              (id0, id8, mm, dd, yy, hh, mi, ss, id7, omm, odd, oyy, ohh, omi, oss, id44, id9, id10, id42, id43)
        # ResultRecordWrap
        if id8 == 13:
            (id60,) = self.buffer.get_struct("<I")
            output = output + "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n" % (id0, id8, mm, dd, yy, hh, mi, ss, id7, id60)
        return output

    # Decode history records
    # See DOC33891_rev-B attachment 1
    #   Worksheet Historical_Data and Historical_Wrap
    # @return Text decoded to same format as in DOC33891_rev-B chapter 8.5.5 $history?
    def decode_command31_param6(self):
        output = ""
        (id0, id8, id7, id9, id10, yy, mm, dd, hh, mi, ss) = self.record_header()
        # Historical_Data
        if id8 == 12:
            (id150, id157, id156) = self.buffer.get_struct("<HHH")
               
            id151 = (id150 >> 12) & 0x1
            id152 = (id150 >> 13) & 0x1
            id153 = (id150 >> 14) & 0x1
            id154 = (id150 >> 15) & 0x1
            id150 = id150 & 0x3ff
            output = output + "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n" % \
                              (id0, id8, mm, dd, yy, hh, mi, ss, id7, id154, id153, id152, id151, id150, id157, id156)
        # Historical_Wrap
        if id8 == 17:
            (id158, dummy, id155) = self.buffer.get_struct("<IHH")
            output = output + "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n" % (id0, id8, mm, dd, yy, hh, mi, ss, id7, id158)
        return output

    # Decode event records
    # See DOC33891_rev-B attachment 1
    #   Worksheet Error_Record, Low_Battery, Dead_Battery, Time_Change_event, Lost_Time, Insulin_Calc_Setup1, Insulin_Calc_Setup2, 
    #             Insulin_Calc_Setup3, Insulin_Calc_Setup4, Restore_Config, User_Time_Change, Clear_Scan_Database, 
    #             Clear_Patch_Activiation, Clear_Historical_Database, User_Time_Change, Clear_Event_Database, Recovery_Event,
    #             MicroController_Reset, MaskModeChange, Sensor_Expired, Event_Data_Wrap
    # @return Text decoded to same format as in DOC33891_rev-B chapter 8.5.4 $event?
    def decode_command31_param7(self):
        output = ""
        (id0, id8, id7, id9, id10, yy, mm, dd, hh, mi, ss) = self.record_header()
        output = output + "%d,1,%d,%d,%d,%d,%d,%d,%d,%d" % (id0, id8, mm, dd, yy, hh, mi, ss, id7)
        # Error_Record
        if id8 == 32:
           (id160,) = self.buffer.get_struct("<H")
           (id161_data) = self.buffer.get_struct("<BBBBBBBB")
           output = output + ",%d" % (id160)
           for id161_byte in id161_data:
               output = output + ",%d" % (id161_byte)
        # Low_Battery, Dead_Battery, Clear_Historical_Database, Clear_Event_Database
        if id8 in (33, 34, 45, 47):
            pass
        # Time_Change_event
        if id8 == 35:
            (id165, id166, id167) = self.buffer.get_struct("<IIH")
            id167 = id167 & 0x1
            output = output + ",%d,%d,%d" % (id165, id166, id167)
        # Lost_Time
        if id8 == 36:
            (id168) = self.buffer.get_struct("<I")
            output = output + ",%d" % (id168)
        # Insulin_Calc_Setup1
        if id8 == 37:
            (id204, id203, id237, id205, id206, id207, id208) = self.buffer.get_struct("<BBBBHHH")
            id237 = (id237 >> 8) & 0x1
            id205 = id205 & 0x7f 
            id206 = id206 & 0x1ff
            id207 = id207 & 0x1ff
            id208 = id208 & 0x1ff
            output = output + ",%d,%d,%d,%d,%d,%d,%d" % (id203, id204, id205, id237, id206, id207, id208)
        # Insulin_Calc_Setup2
        if id8 == 38:
            (id210, id209, id212, id211, id214, id213, id215, id216, id217) = self.buffer.get_struct("<BBBBBBBBH")
            id220 = (id217 >> 15) & 0x1
            id219 = (id217 >> 14) & 0x1
            id218 = (id217 >> 13) & 0x1
            id217 = id217 & 0x3f  
            output = output + ",%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d" % \
                              (id209, id210, id211, id212, id213, id214, id215, id216, id220, id219, id218, id217)
        # Insulin_Calc_Setup3
        if id8 == 39:
            (id222, id221, id224, id223, id226, id225, id228, id227, id229) = self.buffer.get_struct("<BBBBBBBBH")
            id229 = (id229 >> 15) & 0x1
            output = output + ",%d,%d,%d,%d,%d,%d,%d,%d,%d" % (id221, id222, id223, id224, id225, id226, id227, id228, id229)
        # Insulin_Calc_Setup4
        if id8 == 40:
            (id231, id230, id233, id232, id234) = self.buffer.get_struct("<BBBBH")
            id236 = (id234 >> 15) & 0x1
            id235 = (id234 >> 14) & 0x1
            id234 = id234 & 0x3f
            output = output + ",%d,%d,%d,%d,%d,%d,%d" % (id230, id231, id232, id233, id234, id235, id236)
        # Restore_Config
        if id8 == 41:
            (id170,) = self.buffer.get_struct("<H")
            id172 = (id170 >> 2) & 0x1
            id171 = (id170 >> 1) & 0x1
            id170 = id170 & 0x1
            output = output + ",%d,%d,%d" % (id170, id171, id172)
        # Clear_Result_RAI_Database, Clear_Scan_Database, Clear_Patch_Activation, Event_Data_Wrap
        if id8 in (42, 43, 44, 52):
            (id239,) = self.buffer.get_struct("<I")
            output = output + ",%d" % (id239)
        # User_Time_Change
        if id8 == 46:
            (id166,) = self.buffer.get_struct("<i")
            output = output + ",%d,%d" % (id166, id10)
        # Recovery_Event
        if id8 == 48:
            (id175,) = self.buffer.get_struct("<H")
            output = output + ",%d" % (id175)
        # Microcontroller_Reset
        if id8 == 49:
            (bits,) = self.buffer.get_struct("<H")
            output = output + ",%d" % (bits)
        # MaskModeChange, Sensor_Expired
        if id8 in (50, 51):
            (id193, id194) = self.buffer.get_struct("<BB")
            output = output + ",%d,%d" % (id193, id194)
        output = output + "\r\n"
        return output

    # Decodes binary data from device command
    # Reads buffer and decodes the binary data found to text format
    # @param command Command given to device to extract data
    # @param param Parameter given to device to extract data
    # @return Decoded text data
    def decode_binary_command_result(self, command, param):
        self.stop = False
        data_var = self.buffer.get_slice(1) 
        result = ""
        # Read binary data until stop indicated
        while not self.stop:
            # Decode AAP frame header
            data_len = 0
            # The first 3 bytes can contain data length, if no data length
            if ord(data_var) & BINARY_DATA_LENGTH_BIT:
               data_len = ord(data_var) & BINARY_DATA_LENGTH_MASK
               data_var = self.buffer.get_slice(1)
               if ord(data_var) & BINARY_DATA_LENGTH_BIT:
                  data_len = data_len + (( ord(data_var) & BINARY_DATA_LENGTH_MASK ) << 7)
                  data_var = self.buffer.get_slice(1)
                  if ord(data_var) & BINARY_DATA_LENGTH_BIT:
                     data_len = data_len + (( ord(data_var) & BINARY_DATA_LENGTH_MASK ) << 14 )
                     data_var = self.buffer.get_slice(1)
            # After length (if there are any) is the command byte
            data_command = ord(data_var)

            # From here comes the data, let's mark it to be able to consume later
            self.buffer.set_mark()
            if data_len == 0:
                break

            # For commands with parameters,  read the parameter which is part of the data
            if data_command in BINARY_COMMANDS_WITH_PARAM:
                databasenr = ord(self.buffer.get_slice(1))
            
            if data_command != command:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'The binary data is corrupt')

            # Decode the binary data based on command and parameter
            if data_command == BINARY_COMMAND_GETCFGDATA and databasenr == 1:
                result = result + self.decode_command51_param1()
            elif data_command == BINARY_COMMAND_GETDBASE:
                if databasenr == 1:
                    result = result + self.decode_command31_param1()
                elif databasenr == 0:
                    result = result + self.decode_command31_param0()
                elif databasenr == 6:
                    result = result + self.decode_command31_param6()
                elif databasenr == 7:
                    result = result + self.decode_command31_param7()
                else:
                    self.stop = True
            else:
                self.stop = True

            # Since we have read all the data needed, skip the rest (if there is any left)
            self.buffer.consume(data_len - self.buffer.get_distance_to_mark())

            # For multi record commands, check the next byte
            if data_command in (BINARY_MULTI_RECORD_COMMANDS):
                data_var = self.buffer.get_slice(1)
                # If the next byte is the command, there are no more records
                if ord(data_var)  == command: 
                    self.stop = True

            # If command only has one record, stop
            if data_command in BINARY_SINGLE_RECORD_COMMANDS:
                self.stop = True

        if len(result) > 0:
            result = result + "CKSM:%08X\r\nCMD OK\r\n" % (self.fabricate_checksum(result))
        return result 

    # Decode binary block found in text file
    # Checks and decodes binary data for current block in buffer
    # @return Decoded binary block
    def decode_binary_block(self):
        # First the command, check first byte if no data length bit then its a single command
        first_byte = self.buffer.get_slice(1)
        if ord(first_byte) & BINARY_DATA_LENGTH_BIT:
            param_len = ord(first_byte) & BINARY_DATA_LENGTH_MASK 
            command_byte = ord(self.buffer.get_slice(1))
            param = ord(self.buffer.get_slice(param_len))
        else:
            command_byte = ord(first_byte)
            param = ""

        command_result = self.decode_binary_command_result(command_byte, param)
        command_str = self.get_command_str(command_byte, param)
        # Only for commands which we should return results for
        if len(command_str) > 0:
            # Put the command string and result from the command togeather as it is in text format
            return command_str + command_result
        return ""
    
    # Decode the text file data given
    # Decode the binary blocks found and return all data as text format
    # @return Text and decoded binary data in text format
    def decode(self):
        decoded_data = ""

        # Extract binary data
        # Find binary marker, read ahead, until no more data. Replace binary commands with text
        found = self.buffer.find(ConvertBinaryToText.binary_marker)
        while found >= 0:
            decoded_data = decoded_data + self.buffer.get_slice(found)
            self.buffer.consume(len(ConvertBinaryToText.binary_marker))
            # Decode and add extracted data
            decoded_data = decoded_data + self.decode_binary_block()
            # Removing trailing \r\n
            if self.buffer.get_slice(2) != "\r\n":
               raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Problem decoding binary data in file') 
            found = self.buffer.find(ConvertBinaryToText.binary_marker)
        return decoded_data + self.buffer.get_slice(self.buffer.nr_bytes_left())
        
