# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import logging
import logging.handlers

import Config

_index = 0

def EventLogDebug(text):
    """log debug text to event logger"""
    logger.debug(text)

def EventLogInfo(text):
    """log info text to event logger"""
    logger.info(text)

def EventLogWarning(text):
    """log warning text to event logger"""
    logger.warning(text)

def EventLogError(text):
    """log error text to event logger"""
    logger.error(text)

def EventLogDebugWithIndex(text):
    """log debug text to event logger"""
    logger.debug('#%d:%s' % (_index, text))

def EventLogInfoWithIndex(text):
    """log info text to event logger"""
    logger.info('#%d:%s' % (_index, text))

def EventLogWarningWithIndex(text):
    """log warning text to event logger"""
    logger.warning('#%d:%s' % (_index, text))

def EventLogErrorWithIndex(text):
    """log error text to event logger"""
    logger.error('#%d:%s' % (_index, text))

def EventLogSetIndex(index):
    global _index
    _index = index

# setup circular log-files with backup

try:
    prefix = Config.settings["log-prefix"].rstrip()
except KeyError, e:
    prefix = ''

try:
    logpath = Config.settings["log-logpath"].rstrip()
except KeyError, e:
    logpath = '/tmp'

log_handler   = logging.handlers.RotatingFileHandler(logpath+'/'+prefix+'diasend.log', 'a', 10*1024*1024,50)
log_handler.setLevel(logging.DEBUG) 
log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
log_handler.setFormatter(log_formatter) 
logging.getLogger('diasend').addHandler(log_handler) 
logger = logging.getLogger('diasend')
logger.setLevel(logging.DEBUG)
