# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re

from Defines import *
from Generic import *

MODEL_ABBOTT_FREESTYLE_OPTIUM_NEO  = 'Abbott FreeStyle Optium Neo'

class AnalyseOptiumNeo(object):

    @staticmethod
    def is_of_model(commands):
        for command in commands:
            if len(AnalyseOptiumNeo.convert_command_serlnum(command)) > 0:
                return True
        return False

    @staticmethod
    def convert_result_to_diasend(command):
        command_to_extract_dict = {
            'serlnum'  : AnalyseOptiumNeo.convert_command_serlnum, 
            'event'    : AnalyseOptiumNeo.convert_command_event, 
            'result'   : AnalyseOptiumNeo.convert_command_result, 
            'resmtrcfg': AnalyseOptiumNeo.convert_command_resmtrcfg, 
        }

        if command.get_name() in command_to_extract_dict:
            return command_to_extract_dict[command.get_name()](command)

        return []

    @staticmethod
    def convert_command_serlnum(command):

        result = []

        if command.get_name() != 'serlnum':
            return result

        if command.nr_of_records() != 1:
            return result

        m = re.match(r'^(.{1,13})', command.get_records()[0], re.IGNORECASE)
        if m is not None:
            if m.group(1).startswith('LAG') or m.group(1).startswith('LAM'):
                result.append({ELEM_DEVICE_SERIAL:m.group(1)})
                result.append({ELEM_DEVICE_MODEL: MODEL_ABBOTT_FREESTYLE_OPTIUM_NEO})

            if m.group(1)[2] == 'G':
                result.append({SETTINGS_LIST: { SETTING_BG_UNIT: SETTING_BG_UNIT_MGDL }})
            elif m.group(1)[2] == 'M':
                result.append({SETTINGS_LIST: { SETTING_BG_UNIT: SETTING_BG_UNIT_MMOLL }})

        return result

    @staticmethod
    def convert_command_event(command):

        result = []

        if command.get_name() != 'event':
            return result

        # Optium Neo RN,VE,ET,MM,DD,YY,HH,MT,DTV,ERCD,ESD1,ESD2,ESD3

        for record in command.get_records():

            m = re.match(r'^(\d+),([12]),(\d+),(\d{1,2}),(\d{1,2}),(\d{2}),(\d{1,2}),(\d{1,2}),([01])(.*)', record, re.IGNORECASE)
            if not m:
                # Optium Neo has a new line Numevents: at the end of the dump of events. 
                # Let's ignore it for now.
                if not re.match(r'Numevents: \d+', record):
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse event - invalid format %s' % (record))

            else:
                # We skip the ERCD and ESD#.
                keys   = ('rn', 've', 'et', 'mm', 'dd', 'yy', 'hh', 'mt', 'dtv')
                values = map(int, m.groups()[:9])
                items  = dict(zip(keys, values))
                if items['ve'] == 1:
                    event_map = {
                        #32:ALARM_INSULINX_ERROR, 
                        33:ALARM_BATTERY_LOW, 
                        35:ALARM_DATE_CHANGED, 
                        36:ALARM_INSULINX_TIME_LOST, 
                        42:ALARM_INSULINX_RESET_TO_FACTORY_CONFIGURATION, 
                        43:ALARM_INSULINX_RESULT_CLEARED, 
                        #44:ALARM_INSULINX_REBOOT, 
                        46:ALARM_OPTIUM_NEO_BASAL_DOSE, 
                        47:ALARM_OPTIUM_NEO_INSULIN_MEAL_SETUP, 
                        48:ALARM_OPTIUM_NEO_BASAL_TITRATION_SETUP, 
                        49:ALARM_OPTIUM_NEO_MEAL_CORRECTION_SETUP, 
                        #50:ALARM_OPTIUM_NEO_SUCCESSFUL_ROM_CAL_READ, 
                        51:ALARM_OPTIUM_NEO_NO_TITRATION_ADJUSTMENT,
                        52:ALARM_OPTIUM_NEO_TITRATION_ACCEPTED_OR_REJECTED
                        }
                        
                    if items['et'] in event_map:
                        dt = datetime(2000+items['yy'], items['mm'], items['dd'], items['hh'], items['mt'])

                        value = {
                            ELEM_TIMESTAMP : dt,
                            ELEM_VAL_TYPE  : VALUE_TYPE_ALARM,
                            ELEM_VAL       : event_map[items['et']],
                            ELEM_FLAG_LIST : []
                        }

                        # Check Date-Time-Valid flag
                        if items['dtv'] == 0:
                            # Time reading is not reliable (often occurs during removal of battery etc)
                            value[ELEM_FLAG_LIST].append(FLAG_LOST_TIME)

                        result.append(value)
                    else:
                        # As certain events should be ignored, we should not fail if not found.
                        pass
                else:
                    # 've' flag means event is marked as invalid. We should just ignore such records.
                    pass
                    
        return result

    @staticmethod
    def convert_command_result(command):

        result = []

        if command.get_name() != 'result':
            return result

        def ExtractRecordType7(body, dt, external_flags):
            m = re.match(r'^(LO|HI|\d+),(\d),(\d),(\d),(\d),(\d+),(\d),(\d),(\d),(\d),(\d)', body, re.IGNORECASE)
            if m:
                keys = ('rslt', 'vn', 'cs', 'batl', 'qc', 'hct', 'fst', 'ft', 'pc', 'tic', 'ipa')

                try:
                    values = map(int, m.groups())
                except ValueError:
                    # Preserve first value which is either 'HI' or 'LO'.
                    values = list(m.groups(1))
                    values.extend(map(int, m.groups()[1:]))

                items  = dict(zip(keys, values))

                glucose_value = 19 if items['rslt'] == 'LO' else 501 if items['rslt'] == 'HI' else items['rslt']

                value = {
                    ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE,
                    ELEM_TIMESTAMP : dt,
                    ELEM_VAL       : glucose_value,
                    ELEM_FLAG_LIST : external_flags
                }
                
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)  if glucose_value < 20 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH) if glucose_value > 500 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL) if items['cs'] == 0 else None

                # Adjust mg -> mmol factor.
                value[ELEM_VAL] = value[ELEM_VAL] * 18.0 / 18.016
                
                result.append(value)

            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Record type 7 (glucose) - invalid body %s' % (body))

        def ExtractRecordType9(body, dt, external_flags):
            m = re.match(r'^(HI|\d+),(\d),(\d)', body, re.IGNORECASE)
            if m is not None:
                keys = ('rslt', 'cs', 'batl')
                try:
                    values = map(int, m.groups())            
                except ValueError:
                    # Preserve first value which is 'HI'.
                    values = list(m.groups(1))
                    values.extend(map(int, m.groups()[1:]))

                items  = dict(zip(keys, values))

                ketone_value = 145 if items['rslt'] == 'HI' else items['rslt']

                value = {
                    ELEM_VAL_TYPE  : VALUE_TYPE_KETONES,
                    ELEM_TIMESTAMP : dt,
                    ELEM_VAL       : ketone_value,
                    ELEM_FLAG_LIST : external_flags
                }
                
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH) if ketone_value > 144 else None
                value[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL) if items['cs'] == 0 else None
                value[ELEM_FLAG_LIST].append(FLAG_LOW_BATTERY) if items['batl'] == 1 else None

                # Adjust mg -> mmol factor.
                value[ELEM_VAL] = value[ELEM_VAL] * 18.0 / 18.016
                
                result.append(value)
            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Record type 9 (ketone) - invalid body %s' % (body))

        def ExtractRecordType10(body, dt, extra_flags):            
            MORNING_BASAL_DOSE = 0
            BREAKFAST_DOSE     = 1
            LUNCH_DOSE         = 2
            EVENING_DOSE       = 3
            DINNER_DOSE        = 4
            EXTRA_DOSE         = 5

            insulin_type_map = {
                MORNING_BASAL_DOSE : {'value_type': VALUE_TYPE_INS_BOLUS_BASAL, 'flag': FLAG_MORNING},
                BREAKFAST_DOSE     : {'value_type': VALUE_TYPE_INS_BOLUS, 'flag': FLAG_BREAKFAST},
                LUNCH_DOSE         : {'value_type': VALUE_TYPE_INS_BOLUS, 'flag': FLAG_LUNCH},
                EVENING_DOSE       : {'value_type': VALUE_TYPE_INS_BOLUS_BASAL, 'flag': FLAG_EVENING},
                DINNER_DOSE        : {'value_type': VALUE_TYPE_INS_BOLUS, 'flag': FLAG_DINNER},
                EXTRA_DOSE         : {'value_type': None, 'flag': None}
            }

            m = re.match(r'^(\d),(\d+),(-?\d+),(\d+),(\d)', body, re.IGNORECASE)
            if m:

                keys   = ('tp', 'ins', 'cci','si', 'ic')
                values = map(int, m.groups())
                items  = dict(zip(keys, values))

                if items['tp'] in insulin_type_map:
                    # Some insulin types shall be ignored.
                    # TBD : total count of values?
                    if insulin_type_map[items['tp']]['value_type'] is None:
                        return 
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unknown insulin type' % (items['tp']))
                
                value = {
                    ELEM_TIMESTAMP  : dt,
                    ELEM_VAL_TYPE   : insulin_type_map[items['tp']]['value_type'],
                    ELEM_VAL        : items['ins'] * VAL_FACTOR_BOLUS,
                    ELEM_FLAG_LIST  : [insulin_type_map[items['tp']]['flag']],
                    ELEM_VALUE_LIST : []
                }

                value[ELEM_FLAG_LIST].extend(extra_flags)
                value[ELEM_FLAG_LIST].append(FLAG_MANUAL)

                result.append(value)

            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Record type 10 (insulin) - invalid body %s' % (body))

        total_number_records = 0

        for record in command.get_records():

            # Look for result record (only header now - look deeper in ExtractRecordTypeX functions).
            m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(.*)$', record, re.IGNORECASE)
            if m is not None:
                keys   = ('rt', 'rn', 'mm', 'dd', 'yy', 'hh', 'mt', 'dtv')
                values = map(int, m.groups()[:8])
                items  = dict(zip(keys, values))

                total_number_records += 1

                flags = [FLAG_LOST_TIME] if items['dtv'] == 0 else []

                try:
                    dt = datetime(2000+items['yy'], items['mm'], items['dd'], items['hh'], items['mt'])
                except ValueError:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (row))

                record_types = {
                    7 : ExtractRecordType7,
                    9 : ExtractRecordType9,
                    10: ExtractRecordType10 
                }

                if items['rt'] in record_types:
                    record_types[items['rt']](m.group(9), dt, flags)

            # Not a record - maybe there are no result records in this device.
            if m is None:
                m = re.match( r'^(Log Empty)', record, re.IGNORECASE)
                if m is not None:
                    pass
                    # self.AddRecord("nr_results", {ELEM_NR_RESULTS:self.data_dump.number_of_records})

            # Or it could be a number of results record.
            if m is None:
                m = re.match(r'^(\d+),([\da-fA-F]{8,8})', record, re.IGNORECASE)
                if m is not None:
                    pass
                    # Internal check - we are able to validate the number of data points which 
                    # is different from total number of records (which includes events etc).
                    if int(m.group(1)) != total_number_records:
                        raise Exception(ERROR_CODE_NR_RESULTS, 'Incorrect number of results %d != %d' % (int(m.group(1)), total_number_records))
                    # And here we add the total number of records and let Generic to the validation.
                    #self.AddRecord("nr_results", {ELEM_NR_RESULTS:self.data_dump.number_of_records})

        return result

    @staticmethod
    def convert_command_resmtrcfg(command):

        result = []

        if command.get_name() != 'resmtrcfg':
            return result

        for record in command.get_records():

            m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+) (\d+) (\d+).*$', record, re.IGNORECASE)
            if m is not None:
                keys = (
                    'qc_test_time', 
                    'tag_result', 
                    'insulin_dose_reset_time', 
                    'evening_dose', 
                    'morning_dose', 
                    'breakfast_insulin', 
                    'lunch_insulin', 
                    'dinner_insulin', 
                    'titration_start_day', 
                    'titration_start_month', 
                    'titration_start_year', 
                    'insulin_titration_feature', 
                    'titration_range_bottom', 
                    'titration_range_top', 
                    'titration_period', 
                    'default_max_titrate_dose', 
                    'dose_factor', 
                    'titration_auto_shut_off', 
                    'correction', 
                    'hi_indicator_value', 
                    'lo_indicator_value', 
                    'indicator_pattern_time', 
                    'indicator_on_off', 
                    'insulin_reset_day', 
                    'insulin_reset_month', 
                    'insulin_reset_year', 
                    'pc_only_setting_defaults_changed', 
                    'insulin_setup_mode', 
                    'time_format', 
                    'date_format', 
                    'engineering_mode_inactivity_time', 
                    'engineering_mode_ad_hoc', 
                    'night_check_time', 
                    'night_check_min_bg'
                )
                values = map(int, m.groups())
                items  = dict(zip(keys, values))

                setting_list = {
                    SETTING_LONG_ACTING_INSULIN_BASE_DOSE_MORNING   : items['morning_dose'],
                    SETTING_LONG_ACTING_INSULIN_BASE_DOSE_EVENING   : items['evening_dose'],
                    SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_BREAKFAST: items['breakfast_insulin'],
                    SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_LUNCH    : items['lunch_insulin'],
                    SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_DINNER   : items['dinner_insulin'],
                    SETTING_FIRST_DAY_OF_WEEK                       : items['date_format'],
                    SETTING_TIME_FORMAT                             : 0 if items['time_format'] == 1 else 1 # See comment below
                }

                # Note SETTING_TIME_FORMAT : diasend 0 = 12h, 1 = 24h. Abbott 0 = 24h, 1 = 12h.

                result.append({SETTINGS_LIST: setting_list})

        return result
