# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2007 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Arkray X-meter
# @DEVICE Glucomen X-mini Plus
# @DEVICE Glucomen Gm
# @DEVICE Glucofix iD
# @DEVICE GLUCOCARD XM
# @DEVICE GLUCOCARD 01
# @DEVICE GLUCOCARD Vital
# @DEVICE ReliOn Confirm
# @DEVICE ReliOn Prime

import re
from Generic import *
from Defines import *
from datetime import *

# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------

METER_TYPE_GLUCOCARD_01 = "GlucoCard 01"
METER_TYPE_GLUCOCARD_VITAL = "GlucoCard Vital"
METER_TYPE_RELION_CONFIRM = "ReliOn Confirm"
METER_TYPE_RELION_PRIME = "ReliOn Prime"

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a X-Meter serial nr / model record. Extract groups : 
    group 1 : product code
    group 2 : serial number
    
    For the X-meter and x-mini Plus, the serial number is a string of 12 characters.
    The current instructions says 
     * X-meter: "Letters and numbers located below the barcode (7 characters)" 
       (for instance "1910-abcdefg")
     * X-mini Plus: "Letters and numbers after [SN] (12 characters including hyphen)" 
       (for instance "1960abcdefgh")
   
    Examples:
     * X-meter
    \x021H|\\^&||JFGAFP|GT-1910^1.50\\02-0^1910-M606800|||||||P|1|200710191215\r\x17FC\r
     * X-mini Plus
    \x021H|\\^&||POBHED|GT-1960^2.14\\10-0^1960RJ000339|||||||P|1|201209270927\r\x1733\r
     * GlucoMen Gm
    \x021H|\^&||OAAIEC|V1meter^0.01\00-0^SN 047004990|||||||P|1|201004061935
     * GlucoCard 01
    \x021H|\\^&||PEOJLH|GT-1920^7.02\\10-0^1920-4228507|||||||P|1|201610130055\r\x17FA\r
     * ReliOn Confirm
    \x021H|\\^&||PHLNOA|GT-1920^5.02\\10-0^1920-C554177|||||||P|1|201612051050\r\x170B\r
     * GlucoCard Vital
    \x021H|\\^&||JGGLFH|GT-1050^8.12\\00-0^1050KF548829|||||||P|1|201612190904\r\x1726\r
     * ReliOn Prime
    \x021H|\\^&||MPKEMB|GT-1050^8.02\\00-0^1050BE690205|||||||P|1|201610120954\r\x1713\r
    """

    # Try to match an X-Meter (GT-1910) or Glucocard X-mini Plus (GT-1960)
    m = re.match( r'^\x021H\|.*\|.*\|.*\|(GT-19\d0)\^\d+\.\d+\\.*\^(19\d0.*)\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\r', line, re.IGNORECASE )
    if not m:
        # Try to match a GlucoMen Gm
        m = re.match( r'^\x021H\|.*\|.*\|.*\|(V1meter)\^\d+\.\d+\\.*\^SN (.*)\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\r', line, re.IGNORECASE )
    if not m:
        # Try to match a Glucocard Vital or ReliOn Prime
        m = re.match( r'^\x021H\|.*\|.*\|.*\|(GT-10\d0)\^\d+\.\d+\\.*\^(10\d0.*)\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\r', line, re.IGNORECASE )    

    return m

def EvalResultRecord( line ):
    """
    Evaluate a X-Meter result record. Extract groups : 
    group 1 : Result lower
    group 2 : Result upper
    group 3 : minute
    group 4 : hour
    group 5 : Day
    group 6 : Month
    group 7 : Year
    group 8 : Flags 1
    group 9 : Flags 2
    group 10: After meal elapse time
    group 11: checksum

    example:
    '\x06D|8D00591618100700\x1719|\r'
    '\x06D|90010015060510002000\x17B5|'
        
    """
    m = re.match( r"^\x06D\|([\d|A-F]{2})([\d|A-F]{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})([\d|A-F]{2})([\d|A-F]{2}|)([\d|A-F]{2}|)\x17([\d|A-F]{2})\|\r", line, re.IGNORECASE )

    return m

def EvalNrResultRecord( line ):
    """
    Evaluate a X-Meter nr of result record. Extract groups : 
    group 1 : Number of results

    example
    'D|002|\r'
    """

    m = re.match( r'^D\|(\d+)\|\r', line, re.IGNORECASE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalArkrayXMeterSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        # Use different method depending on X-meter or X-mini plus
        serial = m.group(2)
        if serial[0:5] == "1910-":
            res[ "meter_serial" ] = serial[5:]
        else:
            res[ "meter_serial" ] = serial
            
    return res

def EvalArkrayXMeterDeviceModelRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )

    if m:
        if m.group(1) == "GT-1910":
            res[ ELEM_DEVICE_MODEL ] = "GlucoCard X-meter"
        elif m.group(1) == "GT-1960":
            # According to info from menarini:
            # GLUCOCARD MX is distinguished using S/N(Serial number). The S/N of MX is 1960B3xxxxxx.
            # - "B1" or "B3" of MX Serial Number "1960B1xxxxxx" OR "1960B3xxxxxx" are for MX.
            # - The serial number of other GLUCOCARD X-mini plus product's doesn't include "B1" or "B3".
            b_num = m.group(2)[4:6]
            if b_num == 'B1' or b_num == 'B3':
                res[ ELEM_DEVICE_MODEL ] = "GLUCOCARD MX"
            else:
                res[ ELEM_DEVICE_MODEL ] = "GlucoCard X-mini Plus"
        elif m.group(1) == "GT-1920":
            # ID of glucocard 01 and relion prime devices.
            # From Arkrai :
            #     Meter        Serial Number
            #  Glucocard 01    1920-4XXXXXX
            # ReliOn Confirm   1920-XXXXXX
            serial =  m.group(2)
            if serial[5] == '4':
                res[ ELEM_DEVICE_MODEL ] = METER_TYPE_GLUCOCARD_01
            else:
                res[ ELEM_DEVICE_MODEL ] = METER_TYPE_RELION_CONFIRM 
        elif m.group(1) == "GT-1050":
            # ID of glucocard vital and relion prime devices.
            # From Arkrai :
            #      Meter        Serial Number
            # Glucocard Vital   1050-XXXXXXX or 1050KFXXXXXX
            #  ReliOn Prime     1050XXXXXXXX
            serial =  m.group(2)
            if serial[4] == '-' or serial[4:6] == "KF":
                res[ ELEM_DEVICE_MODEL ] = METER_TYPE_GLUCOCARD_VITAL
            else:
                res[ ELEM_DEVICE_MODEL ] = METER_TYPE_RELION_PRIME
        elif m.group(1) == "V1meter":
			# According to info from Menarini:
			# iD - Serial numbers start with 048
			# Gm mg/dL - Serial numbers start with 046
			# Gm mmol/L - Serial numbers start with 047
            serial = m.group(2)
            if serial.startswith("048"):
                res[ ELEM_DEVICE_MODEL ] = "GLUCOFIX iD"
            elif serial.startswith("046") or serial.startswith("047"):
                res[ ELEM_DEVICE_MODEL ] = "GlucoMen Gm"
    
    return res
    
def EvalArkrayXMeterUnitRecord( line ):
    """
    Always return mg/dl 
    """
    res = { "meter_unit":"mg/dL" }
    return res

def EvalArkrayXMeterResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    res = {}
    m = EvalResultRecord( line )
     
    if m:
        try:
            res[ ELEM_TIMESTAMP ] = datetime(2000 + int(m.group(7)), int(m.group(6)), int(m.group(5)), int(m.group(4)), int(m.group(3)))
        except:
            res[ ELEM_TIMESTAMP ] = None

        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE

        low = int(m.group(1), 16)
        hi = int(m.group(2), 16) << 8

        try:
            res[ ELEM_VAL ] = float( low + hi )
        except ValueError:
            res[ ELEM_VAL ] = 0

                
        flags = []
        res["meter_flaglist"] = flags

        # Check status byte, according to spec
        # Bit 0x01 - High
        # Bit 0x02 - Low
        # Bit 0x04 - Control solution 
        # Bit 0x08 - Deleted
        # Bit 0x10 - Don't care
        # Bit 0x20 - Temperature out of range
        # Bit 0x40 - Don't care
        # Bit 0x80 - Don't care
        
        flags1_byte_value = int(m.group(8),16)
        flags2_byte_value = 0
        if m.group(9):
            flags2_byte_value = int(m.group(9),16)

        if flags1_byte_value & 0x01:
            flags.append(FLAG_RESULT_HIGH)    

        if flags1_byte_value & 0x02:
            flags.append(FLAG_RESULT_LOW)    

        if flags1_byte_value & 0x04:
            flags.append(FLAG_RESULT_CTRL)    

        if flags1_byte_value & 0x08:
            flags.append(FLAG_RESULT_DELETED)

        if flags1_byte_value & 0x20:
            flags.append(FLAG_RESULT_OUTSIDE_TEMP)

        if flags1_byte_value & 0x40:
            flags.append(FLAG_TEMPERATURE_DRIFT)

        if flags2_byte_value & 0x01:
            flags.append(FLAG_BEFORE_MEAL)
        
        if flags2_byte_value & 0x02:
            flags.append(FLAG_AFTER_MEAL)
        
        if flags2_byte_value & 0x04:
            flags.append(FLAG_MEAL_AMOUNT1)
        
        if flags2_byte_value & 0x08:
            flags.append(FLAG_MEAL_AMOUNT2)
        
        if flags2_byte_value & 0x10:
            flags.append(FLAG_MEAL_AMOUNT3)
        
        if flags2_byte_value & 0x20:
            flags.append(FLAG_AFTER_EXERCISE)
        
        if flags2_byte_value & 0x40:
            flags.append(FLAG_FEEL_HYPO)
        
        # Currently not used
        # if flags2_byte_value & 0x80:
        #    flags.append(FLAG_AFTER_MEAL_ELAPSE_TIME)

        
    return res

def EvalArkrayXMeterChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    
    csum = sum(map(ord,line[3:-4]))

    return int(line[-3:-2],16) == (csum & 0xf)

def EvalArkrayXMeterChecksumFile( inList ):
    """
    Evaluate checksum of result record.
    """
    return True

def EvalArkrayXMeterNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalNrResultRecord( line )
    if m:
        try:
            res[ "meter_nr_results" ] = int( m.group(1), 10 )
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
    return res


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectArkrayXMeter( inList ):
    """
    Detect if data comes from a X-Meter.
    """

    return DetectDevice( 'ArkrayXMeter', inList, DEVICE_METER );

def AnalyseArkrayXMeter( inData ):
    """
    Analyse X-Meter
    """

    inList = inData.split('\n')
    
    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ] = EvalArkrayXMeterDeviceModelRecord
    d[ "eval_serial_record" ]   = EvalArkrayXMeterSerialRecord
    d[ "eval_unit"]             = EvalArkrayXMeterUnitRecord
    d[ "eval_result_record" ]   = EvalArkrayXMeterResultRecord
    d[ "eval_checksum_record" ] = EvalArkrayXMeterChecksumRecord
    d[ "eval_checksum_file" ]   = EvalArkrayXMeterChecksumFile
    d[ "eval_nr_results" ]      = EvalArkrayXMeterNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );

    return resList

if __name__ == "__main__":

    import pprint

    testfiles = [
        'test/testcases/test_data/ArkrayXmeter/ArkrayXmeter.bin',
        'test/testcases/test_data/ArkrayXmeter/ArkrayXmeter-full.bin',
        'test/testcases/test_data/GlucocardXminiPlus/GlucocardXminiPlus-ticket2493.bin',
        'test/testcases/test_data/GlucocardXminiPlus/GlucocardXminiPlus.bin',
        'test/testcases/test_data/GlucomenGm/GlucomenGm.bin',
        'test/testcases/test_data/MenariniGlucofixID/GlucofixID-half.bin',
        'test/testcases/test_data/MenariniGlucocardMX/MenariniGlucocardMX-de0113.bin',
        'test/testcases/test_data/Glucocard01/1920-4227302.log',
        'test/testcases/test_data/Glucocard01/1920-4228501.log',
        'test/testcases/test_data/Glucocard01/1920-4228507.log',
        'test/testcases/test_data/RelionConfirm/1920-C554157.log',
        'test/testcases/test_data/RelionConfirm/1920-C554177.log',
        'test/testcases/test_data/RelionConfirm/1920-C554198.log',
        'test/testcases/test_data/GlucocardVital/GlucocardVital-DE0424.log',
        'test/testcases/test_data/GlucocardVital/GlucocardVital-DE0425.log',
        'test/testcases/test_data/GlucocardVital/GlucocardVital-DE0426.log',
        'test/testcases/test_data/RelionPrime/1050BE690205.log',
        'test/testcases/test_data/RelionPrime/1050BE690214.log',
        'test/testcases/test_data/RelionPrime/1050BE690215.log'
    ]
    for testfile in testfiles:
        with open(testfile, 'r') as fd:
            testcase = fd.read()
            res = AnalyseArkrayXMeter(testcase)
            #pprint.pprint(res)
