# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import Config
exec "from %s import *" % Config.settings["database-module"]
from Header import *
from Defines import *
import EventLog
import Password

def UsernameInAdmins(name):
    """
    Checks if the username exists among admins (clinics)
    """
    return FindInDatabase("name", "admins", "name", name, force_ci=True)

def UsernameInSuperAdmins(name):
    """
    Checks if the username exists among superadmins (diasend admins)
    """
    return FindInDatabase("name", "super_admins", "name", name, force_ci=True)

def UsernameInClinicUsers(name):
    """
    Checks if the username exists among clinic_users (caregivers@clinics)
    """
    return FindInDatabase("username", "clinic_users", "username", name, force_ci=True)

def UsernameInUsers(name):
    """
    Checks if the username exists among individual users
    """
    return FindInDatabase("pcode", "users", "pcode", name, force_ci=True)
    
def IsUserPasswordOk(pcode, password):
    """
    Checks if user and password matches
    """
    salt = FindInDatabase("salt", "users", "pcode", pcode);
    digest = FindInDatabase("hash", "users", "pcode", pcode);

    return Password.compare(password, digest[0], salt[0])

def IsAdminPasswordOk(name, password):
    """
    Checks if user and password matches
    """
    salt = FindInDatabase("salt", "admins", "name", name);
    digest = FindInDatabase("hash", "admins", "name", name);

    return Password.compare(password, digest[0], salt[0])

def IsClinicUserPasswordOk(name, password):
    """
    Checks if user and password matches
    """
    salt = FindInDatabase("salt", "clinic_users", "username", name);
    digest = FindInDatabase("hash", "clinic_users", "username", name);

    return Password.compare(password, digest[0], salt[0])

def CheckAndUpdateLocation(terminal_serial, location, terminalClass):
    """
    Makes sure that the terminal has the specified location set, and if needed 
    will create a new terminal on the new location.
    """

    # Update location (will only affect database if location has changed)
    if not UpdateTerminalServerLocation(terminal_serial, location):
        EventLog.EventLogDebugWithIndex("Couldn't update server location")
        return False

    # Now, if location has changed, the terminal may be missing
    # and if so it will be created.
    if not IsTerminalRegistered(terminal_serial, location):
        # From now on, we are running all operations on the database server, specified by the location!
        
        EventLog.EventLogDebugWithIndex("Terminal was not registered for location %s" % (location))
        if not InsertTerminalInDatabase(terminal_serial):
            return False
        if not SetTerminalClass(terminal_serial, terminalClass):
            return False
        if not AddTerminalPermission(terminal_serial, DEVICE_PUMP):
            return False
        if not SetTerminalLocationType(terminal_serial, "home"):
            return False
    
    return True

def AuthenticateUserCookie(header):
    try:
        # Terminal id not set, needs to be set
        newSerial = GetTerminalIdFromDatabase(header)
        # Get a new serial from Connector API
        if newSerial != header.getTerminalSerial():
            return ERROR_CODE_TERMINAL_SERIAL_MISSING, newSerial
    except Exception as e:
        if len(e.args) > 0 and e.args[0] == ERROR_CODE_USER_REQUIRED:
            print "Wrong auth"
            return ERROR_CODE_USER_REQUIRED, None
        else:
            raise e

    return ERROR_CODE_NONE, header.getUserCookie()

def AuthenticateUser(header, device_serial, device_type, device_class):

    article = header.getSoftwareID().split()[0]
    if article in sw_transmitters_blacklist:
        EventLog.EventLogDebugWithIndex("Software transmitter article is blacklisted (%s)." % article)
        return ERROR_CODE_INVALID_TERMINAL_SWID, None

    cookie = header.getUserCookie()
    if cookie:
        return AuthenticateUserCookie(header)

    if not header.getLocation() in database_servers:
        EventLog.EventLogDebugWithIndex("The location was not known")
        return ERROR_CODE_HEADER_NOT_COMPLETE, None

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return ERROR_CODE_DATABASE, None

    sw_settings = sw_transmitters[article]
    serialPrefix = sw_settings[0]
    terminalClass = sw_settings[1]

    if header.getTerminalSerial() == "0" or header.getTerminalSerial() == "":

        newSerial = InsertSoftTerminalInDatabase(serialPrefix, database_servers[header.getLocation()])
        if not newSerial:
            EventLog.EventLogDebugWithIndex("Database error: Couldn't generate new serial for soft transmitter")
            return ERROR_CODE_DATABASE, None

        if not SetTerminalClass(newSerial, terminalClass):
            EventLog.EventLogDebugWithIndex("Database error: Couldn't set terminal class for %s" % (newSerial))
            return ERROR_CODE_DATABASE, None

        if not AddTerminalPermission(newSerial, DEVICE_PUMP):
            EventLog.EventLogDebugWithIndex("Database error: Couldn't add terminal permission %s" % (newSerial))
            return ERROR_CODE_DATABASE, None
          
        if not header.getClinicProperty():
            # If not a clinic software transmitter, set location type home,
            # clinic location is set if successful clinic login from clinic
            # transmitter.
            if not SetTerminalLocationType(newSerial, "home"):
                return ERROR_CODE_DATABASE, None

        EventLog.EventLogDebugWithIndex("New serial for soft transmitter: %s" % (newSerial))
        return ERROR_CODE_TERMINAL_SERIAL_MISSING, newSerial

    else:
        # It is possible that the DDA user has changed location compared to previous transfers,
        # if so, we have to update the location
        if not CheckAndUpdateLocation(header.getTerminalSerial(), header.getLocation(), terminalClass):
            EventLog.EventLogDebugWithIndex("Database error: Couldn't update terminal location")
            return ERROR_CODE_DATABASE, None
        # We might now be using another database than previously - if the location for this terminal ID was changed

    # Find the device and terminal and transfer ID:s. Note that if terminal is not in 
    # the database it will be created. 
    terminal_id = GetTerminalIdFromDatabase(header.getTerminalSerial())

    if ((device_class is not DEVICE_USER) and HasDeviceTypeConflict(device_serial, device_type)):
        return ERROR_CODE_SERIAL_NUMBER_ALREADY_REGISTERED, None

    device_id   = GetDeviceIdFromDatabase(device_serial, device_type, device_class)

    action = ''

    if header.getUserInformationAvailable():
        username  = header.getUserPCode()
        password  = header.getUserPassword()
        firstname = header.getUserFirstName()
        lastname  = header.getUserLastName()
        action    = header.getUserAction()
           
        if "LOGIN" in action:
            EventLog.EventLogDebugWithIndex("User login action")
            admin = None 

            username_users        = UsernameInUsers(username)
            username_admins       = UsernameInAdmins(username)
            username_clinic_users = UsernameInClinicUsers(username)

            if username_users:
                username = username_users[0]
                if not IsUserPasswordOk(username, password):
                    EventLog.EventLogDebugWithIndex("Login user ok, but password wrong")
                    return ERROR_CODE_INVALID_PASSWORD, None
                else:
                    EventLog.EventLogDebugWithIndex("User login OK")
                    # The user might already have this access so do not check return value. 
                    SetFileUploadAccess(username)
                    
                    # If this is a non Animas-marked device in Rule B we need to check that the user does not have Animas support
                    if IsDeviceCoveredByRule(device_type, "B"):
                        if HasPatientAnimasSupport(username):
                            EventLog.EventLogDebugWithIndex("User is an Animas patient, rejecting according to Device Rule B")
                            InsertHistoryIntoDatabase( header.getTerminalSerial(), device_serial, \
                                'Failed : Subscription level (Patient Uploader, Animas) did not allow this transfer (%s), rejected Rule B %s' % (device_type, header.getConnectionInfo()) )
                            return ERROR_CODE_SUBSCRIPTION_DEVICE_RULE, None

                    # This is a new device so let's create it in the database. Note that some
                    # countries requires an encrypted serial number.
                    #
                    # Also note that we always create the device - even if it will be 
                    # rejected for other reasons later in the process.
                    if device_id == None:
                        obfuscated_serial = None
                        if UserRequiresEncryptedDeviceSerial(username, device_class): 
                            unencrypted_device_serial = device_serial
                            device_serial, obfuscated_serial = EncryptSerialNumber(device_serial)
                            EventLog.EventLogDebugWithIndex("User %s requires encrypted serial number (%s -> %s)" % (username, unencrypted_device_serial, device_serial))
                        device_id = InsertDeviceInDatabase(device_serial, obfuscated_serial, device_type, device_class)

                    if device_class == 'device_pump':
                        if (not DeviceRegisteredOnUser(username, device_id)) and (NumberOfRegisteredDevicesOnUser(username, 'device_pump') >= 5):
                            EventLog.EventLogDebugWithIndex("Too many pumps registered")
                            return ERROR_CODE_TOO_MANY_PUMPS_REGISTERED, None

                    # If this is a user that requires an encrypted serial number, i.e. a French SRPA, look if any
                    # other French SRPA has this device registered. If so, unregister it (from any previous 
                    # French SRPAs). 
                    if UserRequiresEncryptedDeviceSerial(username, device_class):
                        users = UsersRegisteredAsOwnerOfDevice(device_id)
                        for user in users:
                            if user != username and UserRequiresEncryptedDeviceSerial(user, device_class):
                                UnregisterDeviceOnUser(user, device_id, device_class)

                    if not RegisterDeviceOnUser(username, device_id, device_class):
                        EventLog.EventLogDebugWithIndex("Couldn't register device on user")
                        ret = ERROR_CODE_GENERIC, None

            elif username_admins:
                username = username_admins[0]
                if not IsAdminPasswordOk(username, password):
                    EventLog.EventLogDebugWithIndex("Login admin user ok, but password wrong")
                    return ERROR_CODE_INVALID_PASSWORD, None
                else:
                    EventLog.EventLogDebugWithIndex("Admin login OK")
                    admin = username

            elif username_clinic_users:
                username = username_clinic_users[0]
                if not IsClinicUserPasswordOk(username, password):
                    EventLog.EventLogDebugWithIndex("Login clinic user ok, but password wrong")
                    return ERROR_CODE_INVALID_PASSWORD, None
                else:
                    # Find out which clinic the user belongs to
                    # and register the terminal on that clinic
                    EventLog.EventLogDebugWithIndex("Clinic user login OK")
                    admin = GetClinicOfClinicUser(username)

            else:
                EventLog.EventLogDebugWithIndex("Login user not registered")
                return ERROR_CODE_INVALID_USER, None
            
            if admin and header.getClinicProperty():
                if not RegisterTerminalOnClinic( header.getTerminalSerial(), admin):
                    return ERROR_CODE_DATABASE, None
                if not SetTerminalLocationType(header.getTerminalSerial(), "clinic"):
                    return ERROR_CODE_DATABASE, None
                if not HasClinicDDAAccess(header.getTerminalSerial(), admin): 
                    EventLog.EventLogDebugWithIndex("Terminal is registered on a clinic with no DDA access")
                    return ERROR_CODE_INVALID_USER, None
            elif admin and not header.getClinicProperty():
                EventLog.EventLogDebugWithIndex("Login from clinic or clinic user, but clinic property is not set")
                return ERROR_CODE_INVALID_USER, None

        else:
            return ERROR_CODE_GENERIC, None

    if (header.getClinicProperty() == True):
        if IsTerminalRegisteredOnClinic( header.getTerminalSerial() ) != True:
            EventLog.EventLogDebugWithIndex("Clinic property was set, but terminal is not registered on a clinic.")
            return ERROR_CODE_USER_REQUIRED, None

        clinic = GetTerminalAdmin(header.getTerminalSerial())
        if not HasClinicDDAAccess(header.getTerminalSerial(), clinic): 
            EventLog.EventLogDebugWithIndex("Clinic property was set, but clinic has no DDA access.")
            return ERROR_CODE_USER_REQUIRED, None
        
        # If the Clinic belongs to the Animas middle admins list, but is trying to upload a non Animas-marked device we should reject
        if IsClinicBelongingToAnimasMiddleAdmins(clinic):
            if IsDeviceCoveredByRule(device_type, "B"):
                EventLog.EventLogDebugWithIndex("Animas-listed clinic, rejecting device according to Rule B")
                InsertHistoryIntoDatabase( header.getTerminalSerial(), device_serial, \
                    'Failed : Subscription level (Clinic Uploader, Animas) did not allow this transfer (%s), rejected Rule B %s' % (device_type, header.getConnectionInfo()) )
                return ERROR_CODE_SUBSCRIPTION_DEVICE_RULE, None
            
            
        # If this is a new device we need to create it in the database. 
        if device_id == None:
            obfuscated_serial = None
            if ClinicRequiresEncryptedDeviceSerial(header.getTerminalSerial(), clinic, device_class): 
                unencrypted_device_serial = device_serial
                device_serial, obfuscated_serial = EncryptSerialNumber(device_serial)
                EventLog.EventLogDebugWithIndex("Terminal %s requires encrypted serial number (%s -> %s)" % (header.getTerminalSerial(), unencrypted_device_serial, device_serial))
            device_id = InsertDeviceInDatabase(device_serial, obfuscated_serial, device_type, device_class)

    else:
        # This is a Patient Uploader
        result = False

        # We have no device - let's request a user login at once!
        if device_id == None:
            return ERROR_CODE_USER_REQUIRED, None

        # Primary pump flag is currently only used for pumps.
        if device_class == 'device_pump':
            at_least_one_primary_found = False
        else:
            at_least_one_primary_found = True

        # If this is a non-Animas-marked device in Rule B we need to check that it is registered on at least one non-Animas SRPA
        if IsDeviceCoveredByRule(device_type, "B"):
            classic_support = False
            for user in GetDeviceUsers(device_id):
                if not HasPatientAnimasSupport(user):
                    classic_support = True
            if not classic_support:
                EventLog.EventLogDebugWithIndex("Device not registered to any Diasend classic patients, needs logon credentials")
                return ERROR_CODE_USER_REQUIRED, None

        for user in GetDeviceUsers(device_id):
            EventLog.EventLogDebugWithIndex("Check user: %s" % (repr(user)))

            # If one of the registered users is a French SRPA we always require login
            # information. French SRPA is checked by looking at the user propery 'country'. 
            # This property is only set for an SRPA (a side effect more or less). 
            if (not "LOGIN" in action) and (UserRequiresEncryptedDeviceSerial(user, device_class)):
                EventLog.EventLogDebugWithIndex('French SRPA but no login information')
                return ERROR_CODE_USER_REQUIRED, None

            if not ((IsUserActive(user)) and ( HasUserFileUploadAccessByClinic(user) or 
                    (HasUserFileUploadAccess(user) and (not IsUserExpired(user))))):

                if not IsUserActive(user):
                    EventLog.EventLogDebugWithIndex("Clinic property was not set and user is archived")
                elif HasUserFileUploadAccess(user) != True:
                    EventLog.EventLogDebugWithIndex("Clinic property was not set, and user did not have Diasend SOFT access")
                elif IsUserExpired(user) and (not HasUserFileUploadAccessByClinic(user)):
                    EventLog.EventLogDebugWithIndex("The user is expired and has no upload access by clinic")
            else:
                # A valid user was found
                result = True
                if PumpIsPrimaryForUser(user, device_id):
                    EventLog.EventLogDebugWithIndex("Ok user and pump is primary")
                    at_least_one_primary_found = True
                else:
                    EventLog.EventLogDebugWithIndex("Ok user but pump is not primary")

        if not result or not at_least_one_primary_found:
            EventLog.EventLogDebugWithIndex("No valid user found")
            return ERROR_CODE_USER_REQUIRED, None
        
    return ERROR_CODE_NONE, None
