# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Roche Accu-Chek Aviva Nano
# @DEVICE Roche Accu-Chek Mobile
# @DEVICE Roche Accu-Chek Performa
# @DEVICE Roche Accu-Chek Aviva (generic)
# @DEVICE Roche Accu-Chek Aviva Combo
# @DEVICE Roche Accu-Chek Aviva Expert

import re
import pprint
from datetime import datetime
from datetime import time as datetime_time

from Generic import *
from Defines import *

SETTINGS_BOLUS_ADVICE_TIMEBLOCK = 'settings_bolus_advice_timeblock'
SETTINGS_BOLUS_ADVICE_OPTIONS = 'settings_bolus_advice_options'
SETTINGS_HEALTH_EVENT_VALUE = 'settings_health_event_value'

DEVICE_NAME_ACCU_CHEK_ACTIVE           = 'Accu-Chek Active'
DEVICE_NAME_ACCU_CHEK_ADVANTAGE        = 'Accu-Chek Advantage'
DEVICE_NAME_ACCU_CHEK_AVIVA            = 'Accu-Chek Aviva'
DEVICE_NAME_ACCU_CHEK_AVIVA_COMBO      = 'Accu-Chek Aviva Combo'
DEVICE_NAME_ACCU_CHEK_AVIVA_CONNECT    = 'Accu-Chek Aviva Connect'
DEVICE_NAME_ACCU_CHEK_AVIVA_EXPERT     = 'Accu-Chek Aviva Expert'
DEVICE_NAME_ACCU_CHEK_AVIVA_INSIGHT    = 'Accu-Chek Aviva Insight'
DEVICE_NAME_ACCU_CHEK_AVIVA_NANO       = 'Accu-Chek Aviva Nano'
DEVICE_NAME_ACCU_CHEK_COMPACT          = 'Accu-Chek Compact'
DEVICE_NAME_ACCU_CHEK_COMPACT_PLUS     = 'Accu-Chek Compact Plus'
DEVICE_NAME_ACCU_CHEK_DTRON            = 'Accu-Chek D-TRON'
DEVICE_NAME_ACCU_CHEK_GO               = 'Accu-Chek Go'
DEVICE_NAME_ACCU_CHEK_INSIGHT          = 'Accu Chek Insight'
DEVICE_NAME_ACCU_CHEK_MOBILE           = 'Accu-Chek Mobile'
DEVICE_NAME_ACCU_CHEK_NANO             = 'Accu-Chek Nano'
DEVICE_NAME_ACCU_CHEK_PERFORMA         = 'Accu-Chek Performa'
DEVICE_NAME_ACCU_CHEK_PERFORMA_COMBO   = 'Accu-Chek Performa Combo'
DEVICE_NAME_ACCU_CHEK_PERFORMA_CONNECT = 'Accu-Chek Performa Connect'
DEVICE_NAME_ACCU_CHEK_PERFORMA_EXPERT  = 'Accu-Chek Performa Expert'
DEVICE_NAME_ACCU_CHEK_PERFORMA_INSIGHT = 'Accu-Chek Performa Insight'
DEVICE_NAME_ACCU_CHEK_PERFORMA_NANO    = 'Accu-Chek Performa Nano'
DEVICE_NAME_ACCU_CHEK_POCKET_COMPASS   = 'Accu-Chek Pocket Compass'
DEVICE_NAME_ACCU_CHEK_SPIRIT           = 'Accu-Chek Spirit'
DEVICE_NAME_ACCU_CHEK_SPIRIT_COMBO     = 'Accu-Chek Spirit Combo'
DEVICE_NAME_ACCU_CHEK_VOICEMATE_PLUS   = 'Accu-Chek Voicemate Plus'

model_ids = (
#    { 
#        'name': DEVICE_NAME_ACCU_CHEK_DTRON,
#        'ids': (77,),
#        'concatenate_model_and_serial': True
#    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_ACTIVE,
        'ids': (2102, 2100, 2101),
        'concatenate_model_and_serial': False
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_SPIRIT,
        'ids': (79,),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_COMPACT_PLUS,
        'ids': (910,),
        'concatenate_model_and_serial': False
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_AVIVA_INSIGHT,
        'ids': (469, 480, 481, 482),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_AVIVA,
        'ids': (516, 517, 518, 519, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 555, 556, 557, 558, 559, 565, 566, 567, 568, 569, 571, 572, 573, 574, 575, 576, 577, 578, 579, 626, 627, 628, 629, 455, 457, 458, 459, 793, 794),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_PERFORMA_EXPERT,
        'ids': (649, 656, 657, 658, 659),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_COMPACT,
        'ids': (900,),
        'concatenate_model_and_serial': False
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_AVIVA_CONNECT,
        'ids': (483, 484, 497, 498, 499, 500, 502),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_PERFORMA_CONNECT,
        'ids': (479, 501, 503, 765),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_PERFORMA_NANO,
        'ids': (590, 591, 592, 593, 594, 595),
        'concatenate_model_and_serial': True
    },
#    { 
#        'name': DEVICE_NAME_ACCU_CHEK_ADVANTAGE,
#        'ids': (100,),
#        'concatenate_model_and_serial': True
#    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_AVIVA_COMBO,
        'ids': (668, 670, 671, 672, 673, 674, 675, 680, 681, 682),
        'concatenate_model_and_serial': True
    },
#    { 
#        'name': DEVICE_NAME_ACCU_CHEK_POCKET_COMPASS,
#        'ids': (2200,),
#        'concatenate_model_and_serial': True
#    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_PERFORMA_COMBO,
        'ids': (665, 667, 669, 676, 677, 678, 679),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_PERFORMA_INSIGHT,
        'ids': (466, 467, 468),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_INSIGHT,
        'ids': (82,),
        'concatenate_model_and_serial': True
    },
#    { 
#        'name': DEVICE_NAME_ACCU_CHEK_GO,
#        'ids': (920,),
#        'concatenate_model_and_serial': True
#    },
#    { 
#        'name': DEVICE_NAME_ACCU_CHEK_VOICEMATE_PLUS,
#        'ids': (2500,),
#        'concatenate_model_and_serial': True
#    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_MOBILE,
        'ids': (1205, 1206, 1207, 1208, 1209, 1200, 1201, 1202, 1203, 1204, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250),
        'concatenate_model_and_serial': False
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_AVIVA_NANO,
        'ids': (600, 601, 602, 603, 604, 605),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_NANO,
        'ids': (596, 597, 598, 599),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_AVIVA_EXPERT,
        'ids': (648, 650, 651, 652, 653, 654, 655, 660, 661),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_SPIRIT_COMBO,
        'ids': (80,),
        'concatenate_model_and_serial': True
    },
    { 
        'name': DEVICE_NAME_ACCU_CHEK_PERFORMA,
        'ids' : (686, 687, 688, 689, 795, 520, 521, 522, 523, 524, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 630, 631, 632, 633, 634),
        'concatenate_model_and_serial': True
    }
)

class AvivaAnalyzer(object):

    def __init__(self):
        self.model_number                 = 0
        self.model_name                   = ''
        self.concatenate_model_and_serial = True
        
    def eval_settings_record(self, line):
        m = re.match(r'!TAG!SET(\d{2,2})(_)?(\d)?!(.*)', line, re.IGNORECASE)
        if m:
            command     = m.group(1)
            message     = m.group(4)
            sub_command = m.group(3)

            if 'FAIL' in message:
                return None

            # Bolus advice time block
            if command == '78':
                m = re.match(r'(\d+)\t(\d{2,2})(\d{2,2})(\d{2,2})\t(\d+)\t(\d+)\t([\d\.]+)\t(\d*)\t([\d\.]+)\t(\d*)', message, re.IGNORECASE)
                if m:
                    setting = {}
                    timeblock = int(sub_command) << 4

                    end_time = datetime_time(int(m.group(2)), int(m.group(3)), int(m.group(4)))
                    setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_END_TIME + timeblock] = str(end_time)

                    setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_MIN_BG_VALUE + timeblock]             = int(m.group(5))
                    setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_MAX_BG_VALUE + timeblock]             = int(m.group(6))
                    setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_CARB_RATIO_INSULIN_VALUE + timeblock] = int(float(m.group(7)) * 10000)
                    if m.group(8):
                        setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_CARB_RATIO_CARB_VALUE + timeblock] = int(m.group(8))
                    setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_INSULIN_SENSITIVITY_INSULIN_VALUE + timeblock] = int(float(m.group(9)) * 10000)
                    if m.group(10):
                        setting[SETTING_BOLUS_ADVICE_TIMEBLOCK_INSULIN_SENSITIVITY_BG_VALUE + timeblock] = int(m.group(10))

                    return {SETTINGS_LIST: setting}

            elif command == '79':

                m = re.match(r'(\d+)\t([\d\s+-]{2,3})', message, re.IGNORECASE)
                if m:
                    setting = {}
                    setting[SETTING_HEALTH_EVENT_VALUE + int(sub_command)] = int(m.group(2))

                    return {SETTINGS_LIST: setting}

                return None

            elif command == '80':
                m = re.match(r'(\d+)\t(\d+)\t(\d{2,2})(\d{2,2})(\d{2,2})\t(\d{2,2})(\d{2,2})(\d{2,2})\t(\d+)\t', message, re.IGNORECASE)
                if m:
                    setting = {}
                    setting[SETTINGS_BOLUS_ADVICE_OPTIONS_MEAL_EXCURSION] = int(m.group(2))
                    setting[SETTINGS_BOLUS_ADVICE_OPTIONS_ACTIVE_TIMEOUT] = str(datetime_time(int(m.group(3)), int(m.group(4)), int(m.group(5))))
                    setting[SETTINGS_BOLUS_ADVICE_OPTIONS_OFFSET_TIMEOUT] = str(datetime_time(int(m.group(6)), int(m.group(7)), int(m.group(8))))
                    setting[SETTINGS_BOLUS_ADVICE_OPTIONS_SNACK_LIMIT] = int(m.group(9))

                    return {SETTINGS_LIST: setting}

            return None

    def eval_serial_record(self, line):
        """
        Evaluate a Roche Aviva Serial record. Extracted groups : 
        group 1 : serial number
        group 2 : checksum
        """
        m = re.match( r'!TAG!SERNO![\d|a-f]{2,2}\t(\w{1,15})\t([\d|a-f]{2,2})', line, re.IGNORECASE )
        return m

    def eval_unit_record(self, line):
        """
        Evaluate a Roche Aviva Unit record. Extracted groups : 
        group 1 : unit (mmol/l or mg/dl)
        """
        m = re.match( r'!TAG!UNIT!\d{2,2}\t(mmol/l|mg/dl)', line, re.IGNORECASE )
        return m

    def eval_device_name_record(self, line):
        """
        Evaluate a Roche Aviva Name record. Extracted groups : 
        group 1 : model
        group 2 : checksum
        """
        m = re.match( r'!TAG!NAME![\d|a-f]{2,2}\t([-\w\s]*)\t([\d|a-f]{2,2})', line, re.IGNORECASE )
        return m

    def eval_device_model_record(self, line):
        """
        Evaluate a Roche Aviva Model record. Extracted groups : 
        group 1 : model
        group 2 : checksum
        """
        m = re.match( r'!TAG!MODEL![\d|a-f]{2,2}\t([-\w\s]*)\t([\d|a-f]{2,2})', line, re.IGNORECASE )
        return m

    def eval_nr_results_record(self, line):
        """
        Evaluate a Roche Aviva Number Results record. Extracted groups : 
        group 1 : number of result records
        group 2 : checksum
        """
        m = re.match( r'!TAG!RESULTS![\d|a-f]{2,2}\t(\w{1,15})\t([\d|a-f]{2,2})', line, re.IGNORECASE )
        return m

    def eval_result_record(self, line):
        """
        Evaluate a Roche Aviva Result record. Extracted groups : 
        group 1 : glucose value (1-4)
        group 2 : time (4)
        group 3 : date (6)
        group 4 : flag (8)
        group 5 : DM-block (x)
        group 6 : checksum (2)
        """
        m = re.match( r'[\d|a-f]{2,2}\t(\d{1,4}|\B)\t(\d{4,4})\t(\d{6,6})\t([\d|a-f]{0,8})\t(.*)\t([\d|a-f]{2,2})$', line, re.IGNORECASE )
        return m
    
    def eval_dm_block(self, line):
        """
        Evaluate a Roche Aviva DM Block. 
        A block can contain a number of different data types. The following are defined for Aviva "DM":
        * Meal time info
        * Health event info
        * Carbs
        * Pen / Syringe Insulin Bolus
        * Pen / Syringe Insulin Basal
        """
        # The format is <num data><tab><dm block>
        m = re.match( r'([\d]{1,1})\t(.*)', line, re.IGNORECASE )
        if m:            
            # Add extra <tab> at end to simply decoding
            dm_data = m.group(2) + "\t"
            r = {}
            while dm_data:
                line = dm_data
                # Meal Time / Health Event Info
                m = re.match( r'37\t(\d{1,2})\t(\d{1,2}|\B)\t\t\t(.*)', line, re.IGNORECASE )
                if m:
                    r["meal_health_info1"] = m.group(1)
                    r["meal_health_info2"] = m.group(2)
                    dm_data = m.group(3)
                else:
                    # Carbs
                    m = re.match( r'12\t(\d{3,3})\t(.*)', line, re.IGNORECASE )
                    if m:
                        r["carbs"] = m.group(1)
                        dm_data = m.group(2)
                    else:
                        # Pen / Syringe Insulin Bolus
                        m = re.match( r'34\t1\t29\t([\d|\.]{4,4})\t(.*)', line, re.IGNORECASE )
                        if m:
                            r["bolus"] = m.group(1)
                            dm_data = m.group(2)
                        else:
                            # Pen / Syringe Insulin Basal
                            m = re.match( r'34\t2\t30\t([\d|\.]{4,4})\t(.*)', line, re.IGNORECASE )
                            if m:
                                r["basal"] = m.group(1)
                                dm_data = m.group(2)
                            else:
                                # Failure, could not match this block.  
                                r = None
                                break
            return r

    def eval_roche_accuchek_aviva_serial_record(self, line):
        """
        Is this line a serial record. If so, return a dictionary with serial
        number.
        """
        res = {}
        m = self.eval_device_model_record(line)
        if m:
            self.model_number = int(m.group(1))
        else:
            m = self.eval_serial_record(line)
            if m:
                if self.concatenate_model_and_serial:
                    res[ "meter_serial" ] = '%d%s' % (self.model_number, m.group(1))
                else:
                    res[ "meter_serial" ] = '%s' % (m.group(1))
        return res

    def eval_roche_accuchek_aviva_device_model_record(self, line):
        """
        Excerpt model by using NAME and MODEL tag. First tries to match the model id, 
        if not found try the name. 
        """
        res = {}
        m = self.eval_device_name_record(line)
        if m:
            if m.group(1) == "Mini 9":
               self.model_name = DEVICE_NAME_ACCU_CHEK_AVIVA_NANO
            elif m.group(1) == "Accu-Chek Mobile":
               self.model_name = DEVICE_NAME_ACCU_CHEK_MOBILE
            elif m.group(1) == "Performa ASIC":
               self.model_name = DEVICE_NAME_ACCU_CHEK_PERFORMA
        else:
            m = self.eval_device_model_record(line)            
            if m:
                self.model_number = int(m.group(1))
                for model in model_ids:
                    if self.model_number in model['ids']:
                        res[ELEM_DEVICE_MODEL] = model['name']
                        self.concatenate_model_and_serial = model['concatenate_model_and_serial']

                # If model id not found, pick up (if available) the 
                # model name (previously found) from the name record.
                if ELEM_DEVICE_MODEL not in res:
                    if self.model_name != '':
                        res[ELEM_DEVICE_MODEL] = self.model_name

        return res

    def eval_roche_accuchek_aviva_unit_record(self, line):
        """
        always return mg/dl. Note : the unit return by the meter is only
        display unit, values are always sent as mg/dl
        """
        res = { "meter_unit":"mg/dl" }
        return res

    def eval_roche_accuchek_aviva_result_record(self, line):
        """
        Is this a result record? If so, return a dictonary with keys >
        
        date_time   > date in yyyy-mm-dd hh:mm:ss format
        value       > value (float) 
        unit        > unit if present (otherwise require headerunit)
        flags       > list of flags (int) if present
        """
        
        results = []
        m = self.eval_unit_record( line )
        if m:
            res = {}
            print "eval_unit_record found"
            if m.group(1) in ("mg/dl", "mmol/l"):
                if m.group(1) == "mg/dl":
                    res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MGDL
                else:
                    res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MMOLL
                results.append({ SETTINGS_LIST: res })
         
        m = self.eval_result_record( line )

        # Glucose values == 9999 signals a time change event for the Mobile -> ignore. 
        if (m and not((m.group(1)) and int(m.group(1) == 9999))):
            try:
                timestamp = datetime( 2000 + int(m.group(3)[0:2]), int(m.group(3)[2:4]), int(m.group(3)[4:6]), int(m.group(2)[0:2]), int(m.group(2)[2:4]) )
            except:
                timestamp = None

            # Check if a DM block is available
            dm_res = None
            if m.group(5):
                dm_res = self.eval_dm_block(m.group(5))
                if dm_res:
                    # The DM Block was correctly decoded

                    # There are several different data types in the DM block but we only store some of them
                    if dm_res.has_key("carbs"):
                        res = { ELEM_TIMESTAMP:timestamp }
                        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_CARBS
                        res[ ELEM_VAL ] = float( dm_res["carbs"])
                        results.append(res)
                    
                    if dm_res.has_key("bolus"):
                        res = { ELEM_TIMESTAMP:timestamp }
                        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS
                        res[ ELEM_VAL ] = float( dm_res["bolus"] ) * VAL_FACTOR_BOLUS
                        flags = []
                        flags.append(FLAG_MANUAL)
                        res[ ELEM_FLAG_LIST ] = flags
                        results.append(res)
                    
                else:
                    # There was a DM Block but we failed to decode it
                    res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":m.group(5) }
                    results.append(res)

            # Check if a glucose value is available
            if m.group(1):
                res = {}
                res[ ELEM_TIMESTAMP ] = timestamp
                ret_flags = []
                try:
                    _flags = int(m.group(4), 16)
                    if _flags & 0x0002:
                        ret_flags.append(FLAG_STRIP_WARNING)
                    if _flags & 0x0004:
                        ret_flags.append(FLAG_RESULT_LOW)
                    if _flags & 0x0008:
                        ret_flags.append(FLAG_RESULT_HIGH)
                    if _flags & 0x0010:
                        ret_flags.append(FLAG_RESULT_CTRL_2)
                    if _flags & 0x0020:
                        ret_flags.append(FLAG_RESULT_CTRL)
                    if _flags & 0x0040:
                        ret_flags.append(FLAG_RESULT_OUTSIDE_TEMP)
                    if _flags & 0x0080:
                        ret_flags.append(FLAG_NO_DATE_SET)
                    if _flags & 0x0200:
                        ret_flags.append(FLAG_RESULT_BELOW_HYPO)
                    if _flags & 0x0800:
                        ret_flags.append(FLAG_RESULT_BELOW_USER_RANGE)
                    if _flags & 0x1000:
                        ret_flags.append(FLAG_RESULT_ABOVE_USER_RANGE)
                    if _flags & 0x4000:
                        ret_flags.append(FLAG_GENERAL)
                    if _flags & 0x8000:
                        ret_flags.append(FLAG_CTRL_NOT_IDENTIFIED)                
                    if _flags & 0x00040000:
                        ret_flags.append(FLAG_BEFORE_MEAL)                
                    if _flags & 0x00080000:
                        ret_flags.append(FLAG_AFTER_MEAL)                
                except:
                    pass
                  
                # Handle flags coming through the DM block
                if dm_res:
                    if dm_res.has_key("meal_health_info1"): 
                        for meal_health in (dm_res["meal_health_info1"], dm_res["meal_health_info2"]):
                            try:
                                _flag = int(meal_health)
                                if _flag == 1:
                                    ret_flags.append(FLAG_BEFORE_MEAL)
                                elif _flag == 2:
                                    ret_flags.append(FLAG_AFTER_MEAL)
                                elif _flag == 4:
                                    ret_flags.append(FLAG_NIGHT)
                                elif _flag == 74:
                                    ret_flags.append(FLAG_OTHER)
                                elif _flag == 75:
                                    ret_flags.append(FLAG_EXERCISE1)
                                elif _flag == 76:
                                    ret_flags.append(FLAG_EXERCISE2)
                                elif _flag == 29:
                                    ret_flags.append(FLAG_STRESS)
                                elif _flag == 73:
                                    ret_flags.append(FLAG_PREMENSTRUAL)
                                elif _flag == 20:
                                    ret_flags.append(FLAG_FASTING)
                                elif _flag == 31:
                                    ret_flags.append(FLAG_ILLNESS)
                                    
                            except:
                                pass
            
                res[ ELEM_FLAG_LIST ] = ret_flags
                try:
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                    res[ ELEM_VAL ] = float( m.group(1) )
                except ValueError:
                    res= { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }
                    
                results.append(res)

            # If the result line was decoded ok but no actual result was stored we need to signal this
            if not results:
                res= { "null_result":True }
                results.append(res)

        else:
            setting = self.eval_settings_record(line)
            if setting:
                results.append(setting)
                

        return results

    def eval_roche_accuchek_aviva_nr_results_record(self, line):
        """
        Is this line a nr results. If so, return a dictionary with nr results. 
        """
        res = {}
        m = self.eval_nr_results_record( line )
        if m:
            try:
                res[ ELEM_NR_RESULTS ] = int( m.group(1) )
            except ValueError:
                res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }  
        return res
    
    def eval_roche_accuchek_aviva_checksum_record(self, line, record):
        """
        Evaluate checksum of result record.
        """

        # Results are sent here without TAG (since the TAG holds number of results in
        # that case) but settings include the TAG. Remove it before calculating the 
        # checksum.
        mu = self.eval_unit_record( line )
        if mu:
            return True

        m = re.match(r'!TAG!SET\d{2}_?\d?!(.*)', line, re.IGNORECASE)
        if m:
            line = m.group(1)

        # calculate checksum
        checkSumma = 0x6E;
        for item in line[2:-2]:
            checkSumma = checkSumma ^ ord(item);
        try:
            if checkSumma == int(line[-2:], 16):
                return True
            else:
                return False
        except ValueError:
            return False

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectRocheAccuchekAviva( inList ):
    """
    Detect if data comes from a Roche Aviva. 
    """
    return DetectDevice( "RocheAccuchekAviva", inList, DEVICE_METER )

def AnalyseRocheAccuchekAviva( inData ):
    """
    Analyse Roche Aviva
    """

    # must make sure that the list is line-separated
    # remove 0x02 and 0x06, split using 0x03 and 0x04

    # Remove some uncessary rep
    line = inData.replace(chr(2),'')
    line = line.replace(chr(6),'')
    p = re.compile(r'[\x03|\x04]')
    splitList = p.split(line)

    analyzer = AvivaAnalyzer()

    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ] = analyzer.eval_roche_accuchek_aviva_device_model_record
    d[ "eval_serial_record" ]       = analyzer.eval_roche_accuchek_aviva_serial_record
    d[ "eval_unit"]                 = analyzer.eval_roche_accuchek_aviva_unit_record
    d[ "eval_result_record" ]       = analyzer.eval_roche_accuchek_aviva_result_record
    d[ "eval_checksum_record" ]     = analyzer.eval_roche_accuchek_aviva_checksum_record

    # Number of results is no longer used, since it has been shown that it is valid that the device sometimes returns less values than "Number of results". Will keep the record for completeness though.
    #d[ "eval_nr_results" ]      = eval_roche_accuchek_aviva_nr_results_record

    resList = AnalyseGenericMeter( splitList, d );
    return resList

if __name__ == "__main__":
    test_files = (
        'AccuCheckAvivaExpert-model654-ticket2644.bin',
        'AccuCheckAvivaExpert.bin',
        'AccuChekAvivaExpert-model660-ticket2726.bin',
        'AccuChekAvivaExpert-zero-health-events.bin',
        'AccuChekMobileWithTimeChangeEvents.bin',
        'AccuChekPerforma.bin',
        'AvivaCombo-model681-ticket2389.bin',
        'AvivaCombo-with-settings.log',
        'AccuChekPerformaCombo-model676.bin'
    )

    for test_file in test_files:
        with open('test/testcases/test_data/RocheAccuChekAviva/%s' % (test_file), 'r') as f:
            payload = f.read()

        res = AnalyseRocheAccuchekAviva(payload)
        print res[0]['header']
        #for r in res[0]['results']:
        #    print r
