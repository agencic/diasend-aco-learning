# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2009 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Insulet OmniPod
# @DEVICE myLife OmniPod Eros (EST400)

from Generic import *
from Defines import *
from subprocess import *
import datetime
import re
import random
import os

# -----------------------------------------------------------------------------
# Defines
# -----------------------------------------------------------------------------
PROGRAM_INDEX_START = 1

# -----------------------------------------------------------------------------
# Globals
# -----------------------------------------------------------------------------
PROGRAM_INDEX = PROGRAM_INDEX_START

glob_last_suggestion = None

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------

class Cache(object):
    def __init__(self):
        self._bg_target    = None
        self._bg_threshold = None
        self.valid         = True

    def invalidate(self):
        self.valid = False

    @property
    def bg_target(self):
        return self._bg_target if self.valid else None

    @bg_target.setter
    def bg_target(self, value):
        self._bg_target = value
    
    @property
    def bg_threshold(self):
        return self._bg_threshold if self.valid else None

    @bg_threshold.setter
    def bg_threshold(self, value):
        self._bg_threshold = value

def EvalSerialRecord( line ):
    """
    Main function for validating a SMS generic serial record
    
    'PDM UID: 20000094'
    """
    return re.match( r'PDM UID: (\d*)', line, re.IGNORECASE )

def InsuletSettingToDiasendSetting(setting, value_in):
    """

    User Setting	BG_MIN	70
    User Setting	BG_GOAL_LOW	100
    User Setting	BG_GOAL_UP	100
    User Setting	INSULIN_DURATION	8
    User Setting	BOL_CALCS_REVERSE	1
    User Setting	BOL_CALCS	0
    User Setting	LOW_VOL	1000
    User Setting	BG_REMINDER	0
    User Setting	EXPIRE_ALERT	4
    User Setting	AUTO_OFF	0
    User Setting	BOL_REMINDER	0
    User Setting	REMDR_ALERT	1
    User Setting	CONF_ALERT	1
    User Setting	BG_DISPLAY	0
    User Setting	BASAL_MAX	300
    User Setting	BOLUS_MAX	1000
    User Setting	EXT_BOL_TYPE	0
    User Setting	BG_SOUND	1
    User Setting	BOLUS_INCR	10
    User Setting	TEMP_BAS_TYPE	0

    User Setting    BG_MIN  57
    User Setting    BG_GOAL_LOW     90
    User Setting    BG_GOAL_UP      140
    User Setting    INSULIN_DURATION        12
    User Setting    BOL_CALCS_REVERSE       1
    User Setting    BOL_CALCS       1
    User Setting    LOW_VOL 5000
    User Setting    BG_REMINDER     1
    User Setting    EXPIRE_ALERT    1
    User Setting    AUTO_OFF        0
    User Setting    BOL_REMINDER    0
    User Setting    REMDR_ALERT     1
    User Setting    CONF_ALERT      0
    User Setting    BG_DISPLAY      0
    User Setting    BASAL_MAX       1000
    User Setting    BOLUS_MAX       2050
    User Setting    EXT_BOL_TYPE    0
    User Setting    BG_SOUND        0
    User Setting    BOLUS_INCR      50
    User Setting    TEMP_BAS_TYPE   0
    User Setting    LANGUAGE        English
    """
    try:
        # Most fields are integers, try to convert it
        val = int(value_in)
    except:
        # Text fields are not converted
        val = value_in

    if setting == 'BG_MIN':
        return (SETTING_MIN_BG_CALC, round(float(val * 1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1))
    elif setting == 'BG_GOAL_LOW':
        return (SETTING_BG_GOAL_MIN, round(float(val * 1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1))
    elif setting == 'BG_GOAL_UP':
        return (SETTING_BG_GOAL_UPPER, round(float(val * 1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1))
    elif setting == 'INSULIN_DURATION':
        # The setting is in 30 minutes units
        return (SETTING_INSULIN_DURATION_OF_ACTION,  val * 30)
    elif setting == 'BOL_CALCS_REVERSE':
        return (SETTING_REVERSE_BOLUS_CALC_ENABLE, val)
    elif setting == 'BOL_CALCS':
        return (SETTING_SUGGESTION_BOLUS_CALC_ENABLE, val)
    elif setting == 'LOW_VOL':
        return (SETTING_LOW_INSULIN_LEVEL, float(val) / 100)
    elif setting == 'BG_REMINDER':
        return (SETTING_GLUCOSE_REMINDER_ENABLE, val)
    elif setting == 'EXPIRE_ALERT':
        return (SETTING_POD_EXPIRE_ALERT, val * 60)
    elif setting == 'AUTO_OFF':
        return (SETTING_ALARM_AUTO_OFF_ENABLE, val)
    elif setting == 'BOL_REMINDER':
        return (SETTING_BOLUS_REMINDER_OPTIONS_ENABLE, val)
    elif setting == 'REMDR_ALERT':
        return (SETTING_REMINDERS_ENABLE, val)
    elif setting == 'CONF_ALERT':
        return (SETTING_CONFIDENCE_ALERT_ENABLE, val)
    elif setting == 'BG_DISPLAY':
        if val == 0:
            unit = "mg/dl"
        else:
            unit = "mmol/L"
        return (SETTING_BG_UNIT, unit)
    elif setting == 'BASAL_MAX':
        return (SETTING_MAX_BASAL_RATE, val * 10)
    elif setting == 'BOLUS_MAX':
        return (SETTING_MAX_BOLUS, val * 10)
    elif setting == 'EXT_BOL_TYPE':
        # % = 1, units = 2, off = 0
        return (SETTING_EXT_BOLUS_TYPE, val)
    elif setting == 'BG_SOUND':
        return (SETTING_SOUND_ENABLED, val)
    elif setting == 'BOLUS_INCR':
        return (SETTING_BOLUS_INCREMENT, val * 10)
    elif setting == 'TEMP_BAS_TYPE':
        # % = 1, U/h = 2, or Off = 0:
        # - which differs from the generic setting:
        # 1 = U/h, % = 2, or Off = 0
        # So we need to convert 1 to 2 and vice versa
        if val != 0:
            val = 3-val
        return (SETTING_TEMP_RATE_METHOD, val)
    elif setting == 'LANGUAGE':
        # English, German, Spanish, Italian, Unknown
        return (SETTING_LANGUAGE, val)

    return None

def InsuletAlarmToDiasendAlarm(alarm):
    if alarm == "AlrmHAZ_EEPROM":
        return ALARM_INSULET_EEPROM
    elif alarm == "AlrmHAZ_RAM":
        return ALARM_INSULET_RAM
    elif alarm == "AlrmHAZ_SWTIMER":
        return ALARM_INSULET_SWTIMER
    elif alarm == "AlrmHAZ_ROM":
        return ALARM_INSULET_ROM
    elif alarm == "AlrmHAZ_STACK":
        return ALARM_INSULET_STACK
    elif alarm == "AlrmHAZ_CODE":
        return ALARM_INSULET_CODE
    elif alarm == "AlrmHAZ_QUEUE":
        return ALARM_INSULET_QUEUE
    elif alarm == "AlrmHAZ_RTC_FAIL":
        return ALARM_INSULET_RTC_FAIL
    elif alarm == "AlrmHAZ_REMOTE_HW":
        return ALARM_INSULET_REMOTE_HW
    elif alarm == "AlrmHAZ_ID":
        return ALARM_INSULET_ID
    elif alarm == "AlrmHAZ_CNFM_ERROR":
        return ALARM_INSULET_CNFM_ERROR
    elif alarm == "AlrmHAZ_PUMP_FAIL":
        return ALARM_INSULET_PUMP_FAIL
    elif alarm == "AlrmHAZ_COMM":
        return ALARM_INSULET_COMM
    elif alarm == "AlrmHAZ_REMOTE":
        return ALARM_INSULET_REMOTE
    elif alarm == "AlrmHAZ_PUMP_VOL":
        return ALARM_CARTRIDGE_EMPTY
    elif alarm == "AlrmHAZ_PUMP_AUTO_OFF":
        return ALARM_POWER_AUTO_OFF
    elif alarm == "AlrmHAZ_PUMP_EXPIRED":
        return ALARM_INSULET_EXPIRED
    elif alarm == "AlrmHAZ_PUMP_OCCL" or alarm =="OCCLUSION":
        return ALARM_INSULET_OCCL
    elif alarm == "AlrmHAZ_PUMP_ACTIVATE":
        return ALARM_INSULET_ACTIVATE
    elif alarm == "AlrmHAZ_PUMP_ALARM":
        return ALARM_INSULET_ALARM
    elif alarm == "AlrmHAZ_PUMP_COMM":
        return ALARM_INSULET_COMM
    elif alarm == "AlrmHAZ_FLASH":
        return ALARM_INSULET_FLASH
    elif alarm == "AlrmADV_KEY":
        return ALARM_KEY_STUCK
    elif alarm == "AlrmADV_PUMP_OCCL":
        return ALARM_INSULET_OCCL_ADVISORY
    elif alarm == "AlrmADV_PUMP_VOL":
        return ALARM_CARTRIDGE_LOW
    elif alarm == "AlrmADV_PUMP_AUTO_OFF":
        return ALARM_POWER_AUTO_OFF_ADVISORY
    elif alarm == "AlrmADV_PUMP_SUSPEND":
        return ALARM_INSULET_SUSPEND
    elif alarm == "AlrmADV_PUMP_EXP1":
        return ALARM_INSULET_EXP1
    elif alarm == "AlrmADV_PUMP_EXP2":
        return ALARM_INSULET_EXP2
    elif alarm == "AlrmEXP_WARNING":
        return ALARM_INSULET_EXP_WARNING
    elif alarm == "AlrmHAZ_PDM_AUTO_OFF":
        return ALARM_POWER_AUTO_OFF
    else:
        return None

def EvalSettingsRecord(line):
    """
    'User Setting\tINSULIN_DURATION\t8'
    """
    # Match integers or a predefined set of language strings
    m = re.match( r'User Setting\t(\w*)\t([0-9]+|English|German|Spanish|Italian|Unknown)', line, re.IGNORECASE )
    if m:
        (s, v) = InsuletSettingToDiasendSetting(m.group(1), m.group(2))
        return {SETTINGS_LIST: {s : v}}
    else:
        return None

def EvalBasalProfileRecord(line):
    """
    These records will appear after the User Settings and will not have the header data.  After the Enabled field there will be 48 values, a tab before each.  These records are basal Program data, with the names of each stored basal programs, the program rates, and which basal program is the currently used program.

    Record Type     Name    Elapsed         Enabled     Value 1     Value 2 …   Value 48
    Basal Profile   string  # (see below)   E, blank    #           #           #

    Each basal program contains:

    An identifying name (allowable characters are alphanumeric upper and lower case, ‘@’, hyphen, period and space).
    The “Elapsed” value is the elapsed time since startup (in seconds) when the profile was set.
    The enabled basal profile is the profile that is currently being used.
    The values represent a series of non-zero basal rates (in 0.01 units per hour, in increments of 0.05 units) in half-hour increments that represents the full 24-hour period from midnight to midnight.
    Up to 24 time periods (contiguous segments with the same values) can be specified in a single basal
    
    'Basal Profile	basal 1	2254002	E	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5	5'
    """
    m = re.match(r'Basal Profile\t([^\t]+)\t\d+\t(E?)\t(.*)', line, re.IGNORECASE)
    if m:
        res = []
        global PROGRAM_INDEX
        
        if m.group(2) == "E":
            # This is the active program
            res.append({SETTINGS_LIST: {SETTING_ACTIVE_BASAL_PROGRAM_ID : PROGRAM_INDEX}})

        half_hours = 0
        periods = []
        for p in m.group(3).split('\t'):
            minutes = 0
            if half_hours % 2 == 1:
                minutes = 30
            start_time = datetime.time(half_hours /2, minutes)
            periods.append((start_time,int(p) * 10))
            half_hours+=1

        r = {PROGRAM_TYPE: PROGRAM_TYPE_BASAL, PROGRAM_NAME: str(PROGRAM_INDEX), PROGRAM_PERIODS: periods}
        res.append(r)
        PROGRAM_INDEX += 1
        return res

    return None

def EvalProfileRecord(line, cache):
    """
    These records will appear after the Basal Profiles and will not have the header data. After the Elapsed field there will be 48 values, a tab before each.

    Record Type     Name         Elapsed         Value 1      Value 2      ...     Value 48
    Profile         Correction   # (see below)   #            #                    # 
                    ICRatio
                    Target BG
                    BGThresh

    Each profile contains:

    * The identifying names can be:
      * Correction - each value is 1 - 300 mg/dL point drop per U insulin, specified in increments of 1 mg/dL
      * ICRatio - each value is 1 – 150 g carbohydrates per U insulin, in increments of 1
      * Target BG – each value describes the target blood glucose level, in 70 - 200 mg/dL, specified in increments of 1 mg/dL
      * BGThresh – each value is the blood glucose correction threshold, in mg/dL
    * The “Elapsed” value is the elapsed time since startup (in seconds) when the profile was set.
    * The values represent a time-varying series of values in half-hour increments that represent the full 24-hour period from midnight to midnight.

    'Profile	Correction	2253949	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0'
    'Profile	ICRatio	2253949	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0'
    'Profile	Target BG	2253949	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0'
    'Profile	BGThresh	2253949	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0'
    """
    m = re.match(r'Profile\t(Correction|ICRatio|Target BG|BGThresh)\t\d+\t(.*)', line, re.IGNORECASE)
    if m:
        # Get the profile type
        if m.group(1) == "Correction":
            type = PROGRAM_TYPE_ISF
        elif m.group(1) == "ICRatio":
            type = PROGRAM_TYPE_IC_RATIO
        elif m.group(1) == "Target BG":
            type = PROGRAM_TYPE_BG_TARGET
        else:
            type = PROGRAM_TYPE_BG_THRESHOLD 

        # Create the period list
        half_hours = 0
        periods = []
        for p in m.group(2).split('\t'):
            minutes = 0
            if half_hours % 2 == 1:
                minutes = 30
            start_time = datetime.time(half_hours /2, minutes)

            # All types but IC Ratio needs to be converted to mmol/l
            if type != PROGRAM_TYPE_IC_RATIO:
                p = round(float(int(p) * 1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1)

            # Now, store the values 
            periods.append((start_time,int(p)))
            half_hours+=1

        res = {PROGRAM_TYPE: type, PROGRAM_NAME: "1", PROGRAM_PERIODS: periods}

        # BG target and BG threshold will be combined in our database so let's 
        # cache them.
        if type == PROGRAM_TYPE_BG_TARGET:
            cache.bg_target = res 
        elif type == PROGRAM_TYPE_BG_THRESHOLD:
            cache.bg_threshold = res
        else:
            return res

        # If we have both BG target and BG threshold we are ready to create
        # our combined data type.
        if cache.bg_target is not None and cache.bg_threshold is not None:

            # Sanity check - periods should be in half-hour steps.
            if len(cache.bg_target[PROGRAM_PERIODS]) != 48 or len(cache.bg_threshold[PROGRAM_PERIODS]) != 48:
                return None

            res = {
                PROGRAM_TYPE: PROGRAM_TYPE_BG_TARGET, 
                PROGRAM_NAME: "1", 
                PROGRAM_PERIODS: []
            }
            # Create the combined type.
            for bg_target, bg_threshold in zip(cache.bg_target[PROGRAM_PERIODS], cache.bg_threshold[PROGRAM_PERIODS]):
                bg_target_with_deviation = (
                    bg_target[0],                               # Datetime
                    bg_target[1],                               # BG target
                    [
                        (
                            PROGRAM_VALUE_BG_THRESHOLD, 
                            bg_threshold[1]
                        )
                    ]
                )
                res[PROGRAM_PERIODS].append(bg_target_with_deviation)
                
            # The documentation is really unclear if there may exists more than one BG target / 
            # BG threshold pair but empirical tests (and the previous implementation) suggests that 
            # there can be only one (and exactly one) BG target/BG threshold pair. We enforce this
            # view in code by invalidating the cache when we have found the first pair.
            cache.invalidate()

            return res
    
    return None

def EvalHistoryCommon(tag, line):
    """
    """
    return re.match(r'' + tag + '\t(?:N|E|N E|\B)\t(?:U|\B)\t(?:C|\B)\t(?:P|\B)\t(.*)', line, re.IGNORECASE)

def EvalActivateRecord(line, timestamp):
    """
    'ACTIVATE\tN\t\t\t\t'
    """
    if EvalHistoryCommon("ACTIVATE", line):
        return {ELEM_VAL_TYPE: VALUE_TYPE_EVENT, ELEM_VAL: DIASEND_EVENT_PUMP_ACTIVATED, ELEM_TIMESTAMP: timestamp}

    return None

def EvalInvalidHistoryType(line):
    """
    This is a previously undocumented type that just should be ignored, according to Insulet
    """
    if EvalHistoryCommon("Invalid History Type", line):
        return {"null_result":True}

    return None

def EvalAlarmRecord(line):
    return EvalGenericAlarmRecord("ALARM", line)

def EvalGenericAlarmRecord(alarm_tag, line):
    """
    'ALARM\tE\t\t\t\tAlarm Date\t3/3/2009\tAlarm Time\t18:45:51\tDescription\tAlrmHAZ_EEPROM\t0\tFile\t250\tLine\t20\tError Code\t2'
    'REMOTE HAZ\t\t\t\t\tAlarm Date\t1/1/2007\tAlarm Time\t00:00:09\tDescription\tAlrmHAZ_REMOTE\t13\tFile\t420\tLine\t11\tError Code\t0'
    """
    m = EvalHistoryCommon(alarm_tag, line)
    if m:
        # Group 1 looks something like this:
        # 'Alarm Date\t3/2/2009\tAlarm Time\t17:20:17\tDescription\tAlrmHAZ_CODE\t5\tFile\t420\tLine\t20\tError Code\t1'
        m1 = re.match(r'Alarm Date\t(\d+)/(\d+)/(\d+)\tAlarm Time\t(\d+):(\d+):(\d+)\tDescription\t([^\t]*)\t(?:.*)', m.group(1), re.IGNORECASE)
        if m1:
            res = {ELEM_TIMESTAMP: datetime.datetime(int(m1.group(3)), int(m1.group(1)), int(m1.group(2)) ,int(m1.group(4)), int(m1.group(5)), int(m1.group(6)))}
            res[ELEM_VAL_TYPE]  = VALUE_TYPE_ALARM
            res[ELEM_VAL] = InsuletAlarmToDiasendAlarm(m1.group(7))
            if not res[ELEM_VAL]:
                return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m1.group(7)}
                
            return res
        else:
            return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1)}
    else:
        return None

def EvalDeactivateRecord(line):
    """
    'DEACTIVATE\t\t\t\t\t'
    """
    if EvalHistoryCommon("DEACTIVATE", line):
        return {"null_result":True}

    return None

def EvalDownloadRecord(line):
    """
    'DOWNLOAD\t\t\t\t\t'
    """
    if EvalHistoryCommon("DOWNLOAD", line):
        return {"null_result":True}

    return None
    
def EvalRemoteHazardRecord(line):
    """
    'REMOTE HAZ\t\t\t\t\tAlarm Date\t1/1/2007\tAlarm Time\t00:00:09\tDescription\tAlrmHAZ_REMOTE\t13\tFile\t420\tLine\t11\tError Code\t0'
    """
    return EvalGenericAlarmRecord("REMOTE HAZ", line)

def EvalTermBasalRecord(line):
    """
    """
    if EvalHistoryCommon("TERM BASAL", line):
        return {"null_result":True}

    return None

def EvalTermBolusRecord(line):
    """
    """
    if EvalHistoryCommon("TERM BOLUS", line):
        return {"null_result":True}

    return None

def EvalOcclusionRecord(line):
    """
    """
    if EvalHistoryCommon("OCCLUSION", line):
        return {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: InsuletAlarmToDiasendAlarm("OCCLUSION")}

    return None

def EvalSuspendRecord(line):
    """
    """
    if EvalHistoryCommon("SUSPEND", line):
        return {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: ALARM_DELIVERY_SUSPENDED}

    return None


def EvalResumeRecord(line):
    """
    """
    if EvalHistoryCommon("RESUME", line):
        return {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: ALARM_DELIVERY_STARTED}

    return None

def EvalDateChangeRecord(line):
    """
    'DATE CHNG\tE\t\t\t\tTo\t3/2/2009'
    """
    if EvalHistoryCommon("DATE CHNG", line):
        return {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: ALARM_DATE_CHANGED}

    return None


def EvalCarbRecord(line):
    """
    'CARB\t\t\t\t\tCarbs\t9\tIsPreset\tN'
    """
    m = EvalHistoryCommon("CARB", line)
    if m:
        m = re.match(r'Carbs\t(\d*)\tIsPreset\t([YN])', m.group(1), re.IGNORECASE)
        if m:
            return {ELEM_VAL_TYPE: VALUE_TYPE_CARBS, ELEM_VAL: int(m.group(1))* VAL_FACTOR_CARBS}

    return None


def EvalTimeAdjRecord(line):
    """
    'TIME ADJ\tN E\t\t\t\tTo\t21:03'
    """
    if EvalHistoryCommon("TIME ADJ", line):
        return {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: ALARM_DATE_CHANGED}

    return None


def EvalGlucoseRecord(line):
    """
    Reading                 #, High, Low, ---
    BG Flags                Manual, Temp Error, Other, blank
    In Range                Above Range, Below Range, blank
    Error Code              #
    Statistics              Y, N
    Averages                Y, N
    
    'BG\t\t\t\t\tReading\t144\tBG Flags\tManual\tIn Range\tAbove Range\tError Code\t0\tStatistics\tY\tAverages\tY\tTag1\t\tTag2\t'

    'BG\t\t\t\t\tReading\t115\tBG Flags\t\tIn Range\t\tError Code\t0\tStatistics\tY\tAverages\tY\tTag1\tPre-bedtime\tTag2\t'

    'BG\t\t\t\t\tReading\t122\tBG Flags\t\tIn Range\tAbove Range\tError Code\t0\tStatistics\tY\tAverages\tY\tTag1\tCarb guess\tTag2\t'

    'BG\t\t\t\t\tReading\t108\tBG Flags\tManual\tIn Range\t\tError Code\t0\tStatistics\tY\tAverages\tY\tTag1\tExercise (light)\tTag2\t'

    'BG\t\t\t\t\tReading\t108\tBG Flags\tManual\tIn Range\t\tError Code\t0\tStatistics\tY\tAverages\tY\tTag1\tBasal evaluation (start)\tTag2\tFasting'

    'BG\t\t\t\t\tReading\t113\tBG Flags\tTemp Error\tIn Range\t\tError Code\t0\tStatistics\tN\tAverages\tN\tTag1\t\tTag2\t
    
    'BG\t\t\t\t\tReading\t129\tBG Flags\t\tIn Range\t\tError Code\t0\tStatistics\tY\tAverages\tY\tTag1\tN\xfcchtern\tTag2\t'
    """
    m = EvalHistoryCommon("BG", line)
    if m:
        m = re.match(r'Reading\t(\d+|High|Low|---)\tBG Flags\t(Manual|Temp Error|Other|\B)\tIn Range\t(Above Range|Below Range|\B)\tError Code\t(\d+)\tStatistics\t(Y|N)\tAverages\t(Y|N)\tTag1\t(.*)\tTag2\t(.*)', m.group(1), re.IGNORECASE)
        if m:
            flags = []
            res = {ELEM_VAL_TYPE: VALUE_TYPE_GLUCOSE, ELEM_FLAG_LIST: flags}
            try:
                res[ELEM_VAL] = float(m.group(1)) * VAL_FACTOR_GLUCOSE_MGDL
            except:
                res[ELEM_VAL] = 0
                if (m.group(1) == "High") or (m.group(1) == "HIGH"):
                    flags.append(FLAG_RESULT_HIGH)
                elif (m.group(1) == "Low") or (m.group(1) == "LOW"):
                    flags.append(FLAG_RESULT_LOW)
                elif m.group(1) == "---":
                    return {"null_result":True}
                else:
                    return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1)}
                
            if m.group(2) == "Manual":
                flags.append(FLAG_MANUAL)
            elif m.group(2) == "Temp Error":
                flags.append(FLAG_RESULT_OUTSIDE_TEMP)
            elif m.group(2) == "Other":
                flags.append(FLAG_OTHER)
            elif len(m.group(2)) > 0:
                return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(2)}

            if m.group(3) == "Above Range":
                flags.append(FLAG_RESULT_ABOVE_USER_RANGE)
            elif m.group(3) == "Below Range":
                flags.append(FLAG_RESULT_BELOW_USER_RANGE)
            elif len(m.group(3)) > 0:
                return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(3)}

            if m.group(7) == "Control" or m.group(7) == "Kontrolle":
                flags.append(FLAG_RESULT_CTRL)
            elif m.group(7) == "Pre-meal" or m.group(7) == "Vor dem Essen":
                flags.append(FLAG_BEFORE_MEAL)
            elif m.group(7) == "Post-meal" or m.group(7) == "Nach dem Essen":
                flags.append(FLAG_AFTER_MEAL)
            elif m.group(7) == "Missed bolus" or m.group(7) == "Verpasster Bolus":
                flags.append(FLAG_MISSED_BOLUS)
            elif m.group(7) == "Carb guess" or m.group(7) == "KH-Sch\xe4tzung":
                flags.append(FLAG_CARB_GUESS)
            elif m.group(7) == "Basal evaluation (start)" or m.group(7) == "Basalbewertung (Start)":
                flags.append(FLAG_BASAL_EVAL_START)
            elif m.group(7) == "Basal evaluation (stop)":
                flags.append(FLAG_BASAL_EVAL_STOP)
            elif m.group(7) == "Skipped meal" or m.group(7) == "N\xfcchtern":
                flags.append(FLAG_SKIPPED_MEAL)
            elif m.group(7) == "Exercise (light)" or m.group(7) == "Anstrengung (leicht)":
                flags.append(FLAG_MILD_EXERCISE)
            elif m.group(7) == "Exercise (moderate)" or m.group(7) == "Anstrengung (m\xe4\xdfig)":
                flags.append(FLAG_MEDIUM_EXERCISE)
            elif m.group(7) == "Exercise (strenuous)" or m.group(7) == "Anstrengung (schwer)":
                flags.append(FLAG_HARD_EXERCISE)
            elif m.group(7) == "Fasting":
                flags.append(FLAG_FASTING)
            elif m.group(7) == "Sick day" or m.group(7) == "Krankheitstag":
                flags.append(FLAG_ILLNESS)
            elif m.group(7) == "Ketones (neg)" or m.group(7) == "Ketone (neg.)":
                flags.append(FLAG_KETONES_NEG)
            elif m.group(7) == "Ketones (trace)":
                flags.append(FLAG_KETONES_TRACE)
            elif m.group(7) == "Ketones (small)":
                flags.append(FLAG_KETONES_SMALL)
            elif m.group(7) == "Ketones (moderate)":
                flags.append(FLAG_KETONES_MODERATE)
            elif m.group(7) == "Ketones (large)":
                flags.append(FLAG_KETONES_LARGE)
            elif m.group(7) == "Pre-bedtime" or m.group(7) == "Vor dem Schlafengehen":
                flags.append(FLAG_BEFORE_SLEEP)
            elif len(m.group(7)) > 0:
                # This tag can also be the generic "string", and will be kept as a custom flag. The text itself is not stored.
                flags.append(FLAG_CUSTOM) 
                print "Found custom text '"+m.group(7)+"', setting FLAG_CUSTOM", repr(line)

            return res

    return None

def EvalBasalRecord(line):
    """
    'BASAL\t\t\t\t\tRate\t0.05\tType\t\tPercent\t\tDuration\t'

    'BASAL\tN\t\t\t\tRate\t0.05\tType\t\tPercent\t\tDuration\t'

    'BASAL\t\t\t\t\tRate\t2.85\tType\tPercent temp\tPercent\t-5%\tDuration\t01:00\r'
    """
    m = EvalHistoryCommon("BASAL", line)
    if m:
        m = re.match(r'Rate\t([\d\.]*|off)\tType\t(Fixed temp|Percent temp|\B)\tPercent\t(-{0,1}\d+\%|\B)\tDuration\t(\B|\d+\:\d+)', m.group(1), re.IGNORECASE)
        if m:
            flags = []
            res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_BASAL, ELEM_FLAG_LIST: flags}
            if m.group(1) == "off":
                res[ELEM_VAL] = 0
            else:
                res[ELEM_VAL] = float(m.group(1)) * VAL_FACTOR_BASAL

            if m.group(2) == "Fixed temp":
                flags.append(FLAG_TEMP_MODIFIED)
            elif m.group(2) == "Percent temp":
                flags.append(FLAG_TEMP_MODIFIED_PERCENTAGE)
            elif len(m.group(2)) > 0:
                return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(2)}
            
            if len(m.group(4)) > 0:
                times = m.group(4).split(":")
                duration = int(times[0]) * 60 + int(times[1])
                res[ELEM_VALUE_LIST] = [{ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]
		if len(m.group(3)) > 2:
                    percent = int(m.group(3).replace('%', ''))
                    res[ELEM_VALUE_LIST].append({ELEM_VAL: percent, ELEM_VAL_TYPE: VALUE_TYPE_INS_BASAL_PERCENTAGE})

            return res
        
    return None

def EvalBolusRecord(line, timestamp):
    """
    Volume 
    #  (Units)

    Immediate Duration
    # (seconds)

    Extended Duration
    hh:mm, blank

    'BOLUS\t\t\t\t\tVolume\t0.65\tImmediate Duration\t\tExtended Duration\t00:30\t'    
    
    'BOLUS\t\t\t\t\tVolume\t0.10\tImmediate Duration\t6\tExtended Duration\t\tSuggested Bolus\t0.00\tMeal\t\tCorrection\t(115 - 100) / 50 = 0.30\tCorrection Insulin on Board\t0.05\tMeal Insulin on Board\t1.10\tTotal\t0.00\tCorrect\t50\tBG\t115\tTarget BG\t100\tCorrection Above\t105\tReverse Correction\tOn\tIC Ratio\t\tCarbs\t\tProgrammed Correction\t0.10\tProgrammed Meal\t\tSuggested Calculation Record\t5753'

    'BOLUS\t\t\t\t\tVolume\t0.65\tImmediate Duration\t\tExtended Duration\t00:30\tSuggested Bolus\t1.70\tMeal\t(25 / 15) = 1.65\tCorrection\t(122 - 100) / 50 = 0.40\tCorrection Insulin on Board\t0.05\tMeal Insulin on Board\t0.30\tTotal\t1.70\tCorrect\t50\tBG\t122\tTarget BG\t100\tCorrection Above\t105\tReverse Correction\tOn\tIC Ratio\t15\tCarbs\t25\tProgrammed Correction\t0.05\tProgrammed Meal\t1.65\tSuggested Calculation Record\t5747'

    'BOLUS\t\t\t\t\tVolume\t0.30\tImmediate Duration\t\tExtended Duration\t01:00\tSuggested Bolus\t1.30\tMeal\t(20 / 15) = 1.30\tCorrection\t\tCorrection Insulin on Board\t\tMeal Insulin on Board\t\tTotal\t1.30\tCorrect\t50\tBG\t---\tTarget BG\t100\tCorrection Above\t105\tReverse Correction\tOn\tIC Ratio\t15\tCarbs\t20\tProgrammed Correction\t\tProgrammed Meal\t1.30\tSuggested Calculation Record\t5719'

    'BOLUS\t\t\t\t\tVolume\t0.00\tImmediate Duration\t\tExtended Duration\t\tSuggested Bolus\t0.00\tMeal\t\tCorrection\t-0.30 (BG <= 105)\tCorrection Insulin on Board\t0.00\tMeal Insulin on Board\t0.00\tTotal\t0.00\tCorrect\t50\tBG\t87\tTarget BG\t100\tCorrection Above\t105\tReverse Correction\tOn\tIC Ratio\t\tCarbs\t\tProgrammed Correction\t\tProgrammed Meal\t\tSuggested Calculation Record\t5715'
    """

    m = EvalHistoryCommon("BOLUS", line)
    if m:
        m = re.match(r'Volume\t([\d\.]+)\tImmediate Duration\t(\d*)\tExtended Duration\t([\d\:]*)\t(.*)', m.group(1), re.IGNORECASE)
        if m:
            if (float(m.group(1)) == 0.0):
                # No bolus was delivered, so don't store this one. The suggested bolus might still be present, but it has been decided that it is not interesting.
                return { "null_result" : True }

            flags = []
            secondary = []
            res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS, ELEM_FLAG_LIST: flags, ELEM_TIMESTAMP: timestamp}
            res[ELEM_VAL] = float(m.group(1)) * VAL_FACTOR_BOLUS
            res[ELEM_VALUE_LIST] = secondary
        
            if len(m.group(3)) > 0:
                flags.append(FLAG_BOLUS_TYPE_EXT)

                times = m.group(3).split(":")
                duration = int(times[0]) * 60 + int(times[1])
                secondary.append({ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION})

            global glob_last_suggestion

            if len(m.group(4)) > 0:
                # Linked to a suggestion bolus
                m1 = re.match(r'Suggested Bolus\t([\d\.]*)\tMeal\t([\d \-\/\=\.\(\)]*)\tCorrection\t([\d \-\/\=\.\(\)\<BG]*)\tCorrection Insulin on Board\t([\d\.]*)\tMeal Insulin on Board\t([\d\.]*)\tTotal\t([\d\.]*)\tCorrect\t([\d\.]*)\tBG\t([\d\.]+|---)\tTarget BG\t([\d\.]*)\tCorrection Above\t([\d\.]*)\tReverse Correction\t(On|Off)\tIC Ratio\t([\d\.]*)\tCarbs\t([\d\.]*)\tProgrammed Correction\t([\d\.]*)\tProgrammed Meal\t([\d\.]*)\tSuggested Calculation Record\t([\d\.]*)', m.group(4), re.IGNORECASE)
                if not m1:
                    return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(4)}

                # Handle the suggestion total:
                secondary.append({ELEM_VAL : float(m1.group(1)) * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED})

                # - Suggested parts:
                # -- Suggested meal
                if len(m1.group(2)) > 0:
                    suggested_meal = 0.0
                    # Try the case "(25 / 15) = 1.65"
                    m2 = re.match('\(\d+ / \d+\) = ([\d.]*)', m1.group(2), re.IGNORECASE)
                    if m2:
                        suggested_meal = float(m2.group(1))
                    
                    if suggested_meal != 0.0:
                        secondary.append({ELEM_VAL : suggested_meal * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL})

                # -- Suggested correction
                if len(m1.group(3)) > 0:
                    suggested_corr = 0.0
                    # Try the case "(115 - 100) / 50 = 0.30"
                    m2 = re.match('\(\d+ - \d+\) / \d+ = ([-\d.]*)', m1.group(3), re.IGNORECASE)
                    if m2:
                        suggested_corr = float(m2.group(1))
                    else:
                        # Try the case "-0.30 (BG <= 120)"
                        m2 = re.match('([-\d.]*) \(BG <= \d+\)', m1.group(3), re.IGNORECASE)
                        if m2:
                            suggested_corr = float(m2.group(1))

                    if suggested_corr != 0.0:
                        secondary.append({ELEM_VAL : suggested_corr * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR})
                
                # -- IOB 
                # --- Correction
                iob_corr = 0.0
                if len(m1.group(4)) > 0:
                    iob_corr = float(m1.group(4))
                    
                # --- Meal
                iob_meal = 0.0
                if len(m1.group(5)) > 0:
                    iob_meal = float(m1.group(5))
                    
                iob = iob_corr + iob_meal
                if (iob > 0.0):
                    secondary.append({ELEM_VAL : iob * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_IOB})

                # Programmed parts:
                if len(m1.group(15)) > 0:
                    secondary.append({ELEM_VAL : float(m1.group(15)) * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_PROGRAMMED_MEAL})
                
                if len(m1.group(14)) > 0:
                    secondary.append({ELEM_VAL : float(m1.group(14)) * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_PROGRAMMED_CORR})
                    
                if glob_last_suggestion and glob_last_suggestion[0] == m1.group(16):
                    # Linked to same suggestion record -> this is a combo bolus
                    (x,prev) = glob_last_suggestion

                    if FLAG_BOLUS_TYPE_EXT in prev[ELEM_FLAG_LIST]:
                        # Move the value into the secondary list
                        prev[ELEM_VALUE_LIST].append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_EXT, ELEM_VAL: prev[ELEM_VAL]})
                        prev[ELEM_VAL] = res[ELEM_VAL]
                        res = prev
                    elif FLAG_BOLUS_TYPE_EXT in res[ELEM_FLAG_LIST]:
                        # Move the value into the secondary list
                        res[ELEM_VALUE_LIST].append({ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS_EXT, ELEM_VAL: res[ELEM_VAL]})
                        res[ELEM_VAL] = prev[ELEM_VAL]

                    # Replace FLAG_BOLUS_TYPE_EXT with FLAG_BOLUS_TYPE_COMBO_EXT
                    res[ELEM_FLAG_LIST][res[ELEM_FLAG_LIST].index(FLAG_BOLUS_TYPE_EXT)] = FLAG_BOLUS_TYPE_COMBO_EXT
                    # The last element is "merged" into current and consumed
                    glob_last_suggestion = None
                elif glob_last_suggestion:
                    # Had a stored suggestion bolus, but it was not linked to this...
                    # Return last and store this...
                    (x, tmp) = glob_last_suggestion
                    glob_last_suggestion = (m1.group(16), res)
                    res = tmp
                else:
                    # No stored, store this
                    glob_last_suggestion = (m1.group(16), res)
                    res = {"null_result" : True}
            else:
                # This was not linked to a suggestion bolus, if there was a "stored one" to be linked
                # return it since this bolus wasn't linked
                if glob_last_suggestion:
                    (x,prev) = glob_last_suggestion
                    ret = [prev, res]
                    res = ret
                    # The stored one is now consumed
                    glob_last_suggestion = None

            return res

    return None

def EvalHistoryRecord(line):
    """
        'History	93	3/24/2009	16:43:28	2321819	DOWNLOAD'
    
        'History	69	1/1/2007	00:00:22	2253776	ALARM	E				Alarm Date	3/3/2009	Alarm Time	18:45:51	Description	AlrmHAZ_EEPROM	0	File	250	Line	20	Error Code	2'    

        Possible types: (marked '-' are not stored)
        - ACTIVATE,
        * ALARM,
        * BG, 
        * BOLUS, 
        * BASAL, 
        * CARB,
        * DATE CHNG,
        - DEACTIVATE,
        - DOWNLOAD,
        * OCCLUSION
        - REMOTE HAZ,
        * RESUME,
        * SUSPEND,
        - TERM BASAL,
        - TERM BOLUS,
        * TIME ADJ,
        - Invalid History Type [not documented, but detected in the field. Confirmed by Insulet]
    """
    m1 = None
    m = re.match(r'History\t\d+\t(\d+)\/(\d+)\/(\d+)\t(\d+):(\d+):(\d+)\t(\d*)\t(.*)', line, re.IGNORECASE)
    if m:
        timestamp = datetime.datetime(int(m.group(3)), int(m.group(1)), int(m.group(2)), \
            int(m.group(4)), int(m.group(5)), int(m.group(6)))
        
        # TODO these are not supported but we at least do the parsing
        m1 = EvalActivateRecord(m.group(8), timestamp)
        if not m1:
            m1 = EvalDeactivateRecord(m.group(8))
        if not m1:
            m1 = EvalDownloadRecord(m.group(8))
        if not m1:
            m1 = EvalTermBasalRecord(m.group(8))
        if not m1:
            m1 = EvalTermBolusRecord(m.group(8))
        if not m1:
            m1 = EvalInvalidHistoryType(m.group(8))
        if m1:
            return m1
        
        m1 = EvalSuspendRecord(m.group(8))
        if not m1:
            m1 = EvalResumeRecord(m.group(8))
        if not m1:
            m1 = EvalDateChangeRecord(m.group(8))
        if not m1:
            m1 = EvalTimeAdjRecord(m.group(8))
        if not m1:
            m1 = EvalCarbRecord(m.group(8))
        if not m1:
            m1 = EvalGlucoseRecord(m.group(8))
        if not m1:
            m1 = EvalBasalRecord(m.group(8))
        if not m1:
            m1 = EvalBolusRecord(m.group(8), timestamp)
        if not m1:
            m1 = EvalAlarmRecord(m.group(8))
        if not m1:
            m1 = EvalRemoteHazardRecord(m.group(8))
        if not m1:
            m1 = EvalOcclusionRecord(m.group(8))
        if not m1:
            return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(8)}

        if m1 and type(m1) != types.ListType and not m1.has_key(ELEM_TIMESTAMP):
            m1[ELEM_TIMESTAMP] = timestamp

    return m1


def EvalPumpAlarmRecord(line):
    """
        Pump Alarm	27	1/5/2011	03:39:13	6812275	PUMP HAZ					Description	AlrmHAZ_PUMP_EXPIRED	16	Error Code	0
        Pump Alarm	26	1/5/2011	02:40:13	6808735	PUMP ADVS					Description	AlrmADV_PUMP_EXP1	26	Error Code	4

        Possible types:
        * OCCLUSION (No additional data)
        * PUMP ADVS (Description maps to the alarm)
        * PUMP HAZ (Description maps to the alarm)
    """
    m1 = None
    m = re.match(r'Pump Alarm\t\d+\t(\d+)\/(\d+)\/(\d+)\t(\d+):(\d+):(\d+)\t(\d*)\t(.*)\t\t\t\t\tDescription\t(.*)\t(\d+)\tError Code\t(\d+)', line, re.IGNORECASE)
    if m:
        res = {ELEM_TIMESTAMP: datetime.datetime(int(m.group(3)), int(m.group(1)), int(m.group(2)) ,int(m.group(4)), int(m.group(5)), int(m.group(6)))}
        res[ELEM_VAL_TYPE]  = VALUE_TYPE_ALARM
        res[ELEM_VAL] = None
        if m.group(8) == "OCCLUSION":
            res[ELEM_VAL] = InsuletAlarmToDiasendAlarm(m.group(8))
        elif m.group(8) == "PUMP ADVS" or m.group(8) == "PUMP HAZ":
            res[ELEM_VAL] = InsuletAlarmToDiasendAlarm(m.group(9))

        if not res[ELEM_VAL]:
            return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m1.group(7)}
                
        return res
    else:
        return None


def EvalInsulinSummary(line):
    """
    'Insulin Summary\t4/29/2008\tTotal Basal\t15.00\tBasal Confirmed\tY\tPercent Basal\t70%\tTotal Bolus\t6.40\tBolus Confirmed\tY\tPercent Bolus\t30%\tTotal Insulin\t21.40\tError\t\tIn Progress\t'

    'Insulin Summary\t1/1/2008\tTotal Basal\t15.20\tBasal Confirmed\tY\tPercent Basal\t67%\tTotal Bolus\t7.60\tBolus Confirmed\tY\tPercent Bolus\t33%\tTotal Insulin\t22.80\tError\t\tIn Progress\t'    

    'Insulin Summary\t2/27/2009\tTotal Basal\t18.55\tBasal Confirmed\tN\tPercent Basal\t100%\tTotal Bolus\t\tBolus Confirmed\t\tPercent Bolus\t\tTotal Insulin\t18.55\tError\t\tIn Progress\t'

    'Insulin Summary\t1/1/2007\tTotal Basal\t\tBasal Confirmed\t\tPercent Basal\t\tTotal Bolus\t\tBolus Confirmed\t\tPercent Bolus\t\tTotal Insulin\t\tError\tRemote Alarm\tIn Progress\t' 
    """
    m = re.match(r'Insulin Summary\t(.*)', line, re.IGNORECASE)
    if m:
        line = m.group(1)
        m = re.match(r'(\d+)/(\d+)/(\d+)\tTotal Basal\t([\d\.]*)\tBasal Confirmed\t(Y|N|\B)\tPercent Basal\t(\d+\%|\B)\tTotal Bolus\t([\d\.]*)\tBolus Confirmed\t(Y|N|\B)\tPercent Bolus\t(\d+\%|\B)\tTotal Insulin\t([\d\.]+|\B)\tError\t([\w\s]*|\B)\tIn Progress\t', line, re.IGNORECASE)
        if not m:
            return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":line}
        
        if m.group(11):
            # Found error. @TODO: Set flag? 
            print "Insulin summary error found (and ignored):", m.group(11)
        
        if m.group(10):    
            # A value for Total Insuln exist
            res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_TDD, ELEM_VAL: float(m.group(10)) * VAL_FACTOR_TDD}

            if m.group(4):
                # A second result is the insulin_basal_tdd
                res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : float(m.group(4)) * VAL_FACTOR_TDD, ELEM_VAL_TYPE : VALUE_TYPE_INS_BASAL_TDD}]
            # @TODO: Do something when there is no insulin_basal_tdd?        

            res[ELEM_TIMESTAMP] = datetime.datetime(int(m.group(3)), int(m.group(1)), int(m.group(2)) ,23, 59)
            return res

    return None
    
# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalInsuletOmniPodModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
    line = line[0]
    return {ELEM_DEVICE_MODEL : "OmniPod"}


def EvalInsuletOmniPodSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    line = line[0]
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ELEM_DEVICE_SERIAL] = m.group(1)
    return res


def EvalInsuletOmniPodUnitRecord( line ):
    """
    This one always uses mg/dL
    """
    line = line[0]
    return { ELEM_DEVICE_UNIT:"mg/dL" }


def EvalInsuletOmniPodResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    (line, cache) = line
    m = EvalSettingsRecord(line)
    if not m:
        m = EvalBasalProfileRecord(line)
    if not m:
        m = EvalProfileRecord(line, cache)
    if not m:
        m = EvalHistoryRecord(line)
    if not m:
        m = EvalPumpAlarmRecord(line)
    if not m:
        m = EvalInsulinSummary(line)
    if m:
        return m
    else:
        #print "Unhandled:", line
        return {}


def EvalDiasendGenericNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        if (m["last_written"] == 0):
            res[ "meter_nr_results" ] = 0
        elif (m["last_written"] - 0x10000) >= 0:
            res[ "meter_nr_results" ] = (m["last_written"] - 0x10000) + 1
        else:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m["last_written"] }
    return res

def EvalDiasendGenericChecksumRecord( line, record ):
    """
    No checksums in the data, since blocks has checksums, just return true
    """
    return True
    
def InsuletOmniPodAllLinesEvaluated():
    """
    Called when all lines are evaluated

    We check if there is a "cached" bolus
    """
    global glob_last_suggestion
    if glob_last_suggestion:
        (x, tmp) = glob_last_suggestion
        glob_last_suggestion = None
        return tmp
    else:
        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectInsuletOmniPod( inList ):
    """
    Detect if data comes from a Omnipod.
    """
    return DetectDevice( 'InsuletOmnipod', inList, DEVICE_PUMP )

def AnalyseInsuletOmniPodTextFile(inData_txt):

    cache = Cache()

    # Empty dictionary
    d = { \
        "eval_device_model_record": EvalInsuletOmniPodModelRecord, \
        "eval_serial_record": EvalInsuletOmniPodSerialRecord, \
        "eval_unit": EvalInsuletOmniPodUnitRecord, \
        "eval_result_record": EvalInsuletOmniPodResultRecord, \
        "all_lines_evaluated": InsuletOmniPodAllLinesEvaluated \
        }
    
    # Setup global vars
    global PROGRAM_INDEX
    PROGRAM_INDEX = PROGRAM_INDEX_START 

    # Organize the file in lines
    inList = []
    inData_txt = inData_txt.replace("\r","")
    tmpList = inData_txt.split("\n")
    for s in tmpList:
        inList.append((s.lstrip(' ').rstrip(' '), cache))
    
    # Perform the actual analysis
    return AnalyseGenericMeter( inList, d )

def LineContainsError(line):
    return line.startswith('Stop\t')

def AnalyseInsuletOmniPod( inData ):
    """
    Analyse OmniPod
    """
    import insulet

    output = ""

    prob = insulet.HIM.SpecifyInputFile(inData)
    if prob == insulet.HIM.NoHeaderProblem:
        ibfRev = insulet.HIM.GetIBFVersionInfo()
        supportEnum = insulet.HIM.CheckRevSupport(ibfRev)
        if supportEnum == insulet.HIM.eSupported:
            output += insulet.HIM.GetHeaderLines()

            while 1:
                record = insulet.HIM.GetNextRecord()
                if LineContainsError(record):
                    error_response = CreateErrorResponseList('OmniPod', 'Unknown', ERROR_CODE_PREPROCESSING_FAILED, 'Insulet OmniPod binary parser returned %s' % record)
                    return error_response
                output += record
                if insulet.HIM.GetErrorStatus() == insulet.HIM.NoMoreOutput:
                    break
                if insulet.HIM.GetErrorStatus() == insulet.HIM.OutputTruncated:
                    print "Output truncated"

    insulet.HIM.CloseInputFile()

    return AnalyseInsuletOmniPodTextFile(output)

# -----------------------------------------------------------------------------
# SOME CODE THAT WILL RUN ON IMPORT
# -----------------------------------------------------------------------------

def RunIBFs(ibf_files):
    """
    Analyse the IBF files in the list, useful for unit testing
    """
    for ibf_file in ibf_files:
        f = open(ibf_file, "rb")
        databin = f.read()
        f.close()
        res = AnalyseInsuletOmniPod(databin) 
        for r in res[0]['results']:
            print r

if __name__ == "__main__":

    # Run a number of test files, supplied by Insulet
    ibfs = [
    "test/testcases/test_data/InsuletOmniPod/20024311-2011-02-14-20-51-35.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024321-2011-02-21-23-00-21.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024325-2011-02-14-21-05-39.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024327-2011-02-14-20-50-47.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024328-2011-02-14-21-04-38.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024333-2011-02-21-22-08-13.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024336-2007-01-01-00-00-22.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024341-2007-01-01-00-00-15.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024341-2011-02-14-20-43-27.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024342-2007-01-01-00-00-13.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024345-2011-02-21-22-10-20.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024354-2010-01-01-00-00-51.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024355-2011-02-14-21-17-09.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024361-2011-02-14-20-59-47.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024362-2011-02-14-21-02-51.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024363-2011-02-10-20-31-11.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024367-2010-01-01-00-00-18.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024370-2011-02-14-20-54-31.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024378-2010-01-01-00-00-17.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024381-2007-01-01-00-00-22.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024390-2007-01-01-00-00-22.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024391-2010-02-21-09-28-46.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024392-2011-02-14-21-02-32.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024435-2010-01-01-00-00-45.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024562-2011-02-14-21-20-00.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024574-2011-02-14-20-47-35.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024582-2010-02-14-21-29-58.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024594-2011-02-14-21-12-46.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024707-2007-01-01-00-00-22.ibf",
    "test/testcases/test_data/InsuletOmniPod/20024910-2011-03-02-15-37-09.ibf",
    "test/testcases/test_data/InsuletOmniPod/20025151-2007-01-01-00-00-25.ibf",
    "test/testcases/test_data/InsuletOmniPod/20025158-2011-02-21-22-02-37.ibf",
    "test/testcases/test_data/InsuletOmniPod/20025763-2010-01-01-00-00-19.ibf",
    "test/testcases/test_data/InsuletOmniPod/20025901-2007-01-01-00-00-23.ibf"
    ]
    
    RunIBFs(ibfs)

    #AnalyseInsuletOmniPod(data) 
    #print AnalyseInsuletOmniPodTextFile(data)
