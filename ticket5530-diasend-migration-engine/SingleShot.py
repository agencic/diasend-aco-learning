#
# Execute backend _and_ regression test of your choice in one 
# single command (why didn't I think about that before;)
# 

import sys
import os
import signal
import subprocess

import time

# Start backend
p_server = subprocess.Popen('python AsyncServer.py', shell=True, preexec_fn=os.setsid) 

time.sleep(1)

# Start regression test
p_client = subprocess.Popen('python RegressionTest.py -c test/testcases/%s' % (sys.argv[1]), shell=True)
p_client.wait()

os.killpg(p_server.pid, signal.SIGTERM) 
