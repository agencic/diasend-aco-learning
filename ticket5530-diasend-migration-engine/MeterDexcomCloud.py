# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Dexcom Cloud

import json
import aniso8601
import zlib
import base64
import logging

#logging.basicConfig(level=logging.DEBUG)

from Generic import *

DEVICE_MODEL = 'Dexcom Cloud'

class DexcomCloudAnalyser(object):

    def __init__(self):
        
        self.data_points = []
        self.events = []
        self.user_device_serial = 'UNKNOWN'
        self.unknown_events = {}

    def add_events(self, events):

        event_callbacks = {
            'AlertSetting'   : self.convert_event_alert_setting,
            'Calibration'    : self.convert_event_calibration,
            'CarbsLog'       : self.convert_event_carbs_log,
            'DeviceSettings' : self.convert_event_device_settings,
            'ExerciseLog'    : self.convert_event_exercise_log,
            'HealthLog'      : self.convert_event_health_log,
            'InsulinLog'     : self.convert_event_insulin_log,
        }

        for event in events:
            if not all (k in event for k in ('label',
                                             'time')):
                raise Exception('JSON format error (4)')

            if event['label'] in event_callbacks:
                event_callbacks[event['label']](event)
            else:
                if event['label'] not in self.unknown_events:
                    self.unknown_events[event['label']] = []
                self.unknown_events[event['label']].append(event)
                
        for event in self.unknown_events.keys():
            logging.debug('Found %d unknown events of type %s' % (
                len(self.unknown_events[event]), event))

    def split(self, data):

        try:
            data_d = json.loads(data)
        except ValueError as e:
            raise Exception('Failed to parse incoming JSON')

        self.user_device_serial = data_d['user_device_serial']
        self.data_points.append({ELEM_DEVICE_SERIAL: self.user_device_serial})

        z = zlib.decompress(base64.b64decode(data_d['payload']))
        d = json.loads(z)

        try:
            if 'receiver' in d:
                transmitters = d['receiver']['transmitterData']
                events       = d['receiver']['deviceEvents']
            elif 'phone' in d:
                transmitters = d['phone']['transmitterData']
                events       = d['phone']['deviceEvents']
            else:
                raise Exception('JSON format error (1)')    
        except KeyError:
            raise Exception('JSON format error (2)')    

        for transmitter in transmitters:

            for dp in transmitter['glucoseValues']:
                if 'magnitude' in dp:
                    self.data_points.append({
                        ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                        ELEM_TIMESTAMP: aniso8601.parse_datetime(dp['time']),
                        ELEM_VAL: int(round(float(dp['magnitude']))),
                        ELEM_FLAG_LIST: [FLAG_CONTINOUS_READING],
                    })
                elif int(dp['originalValue']) == 39:
                    self.data_points.append({
                        ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                        ELEM_TIMESTAMP: aniso8601.parse_datetime(dp['time']),
                        ELEM_VAL: int(round(float(dp['originalValue']))),
                        ELEM_FLAG_LIST: [FLAG_CONTINOUS_READING, FLAG_RESULT_LOW],
                    })
                elif int(dp['originalValue']) == 401:
                    self.data_points.append({
                        ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                        ELEM_TIMESTAMP: aniso8601.parse_datetime(dp['time']),
                        ELEM_VAL: int(round(float(dp['originalValue']))),
                        ELEM_FLAG_LIST: [FLAG_CONTINOUS_READING, FLAG_RESULT_HIGH],
                    })

            self.add_events(transmitter['events'])

        self.add_events(events)

    def convert_event_alert_setting(self, event):
        
        return
        
        setting_dict = {
            'OutOfRange': SETTING_DEXCOM_CLOUD_ALERT_OUT_OF_RANGE,
            'Low'       : SETTING_DEXCOM_CLOUD_ALERT_LOW,
            'High'      : SETTING_DEXCOM_CLOUD_ALERT_HIGH,
            'Fall'      : SETTING_DEXCOM_CLOUD_ALERT_FALL,
            'Rise'      : SETTING_DEXCOM_CLOUD_ALERT_RISE
        }

        if event['subLabel'] in setting_dict:
            sb = setting_dict[event['subLabel']]

            dp = {
                SETTINGS_LIST: {
                    sb + SETTING_DEXCOM_CLOUD_ALERT_VALUE_OFFSET       : int(event['data']['value']),
                    sb + SETTING_DEXCOM_CLOUD_ALERT_SNOOZE_VALUE_OFFSET: int(event['data']['snoozeValue']),
                    sb + SETTING_DEXCOM_CLOUD_ALERT_ENABLED_OFFSET     :
                        0 if str(event['data']['isEnabled']).lower() == 'false' else 1
                }
            }

            self.data_points.append(dp)

    def convert_event_calibration(self, event):
        dp = {
            ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
            ELEM_TIMESTAMP: aniso8601.parse_datetime(event['time']),
            ELEM_VAL: int(round(float(event['data']['value']))),
            ELEM_FLAG_LIST: [FLAG_CONTINOUS_READING, FLAG_CALIBRATION],
        }

        self.data_points.append(dp)

    def convert_event_carbs_log(self, event):
        dp = {
            ELEM_VAL_TYPE: VALUE_TYPE_CARBS,
            ELEM_VAL: int(round(float(event['data']['value']))),
            ELEM_TIMESTAMP : aniso8601.parse_datetime(event['time'])
        }

        self.data_points.append(dp)

    def convert_event_device_settings(self, event):
        pass

    def convert_event_exercise_log(self, event):
        flag_dict = {
            'Light' : FLAG_MILD_EXERCISE,
            'Medium': FLAG_MEDIUM_EXERCISE,
            'Heavy' : FLAG_HARD_EXERCISE
        }

        flag_list = []
        if 'subLabel' in event and event['subLabel'] in flag_dict:
            flag_list.append(flag_dict[event['subLabel']])

        value_list = []
        if event['data']['units'] == 'minutes':
            value_list.append({
                ELEM_VAL_TYPE: VALUE_TYPE_DURATION,
                ELEM_VAL     : int(round(float(event['data']['value'])))
            })

        dp = {
            ELEM_VAL_TYPE  : VALUE_TYPE_EXERCISE,
            ELEM_VAL       : 0,
            ELEM_FLAG_LIST : flag_list,
            ELEM_TIMESTAMP : aniso8601.parse_datetime(event['time']),
            ELEM_VALUE_LIST: value_list
        }

        self.data_points.append(dp)

    def convert_event_health_log(self, event):
        # Since health log events are separate from cgm readings we currently
        # ignore these events (in the diasend system those events are simply
        # flags associated glucose readings).
        pass

    def convert_event_insulin_log(self, event):
        dp = {
            ELEM_VAL_TYPE  : VALUE_TYPE_INS_BOLUS,
            ELEM_VAL       : int(round(float(event['data']['value']) * VAL_FACTOR_BOLUS)),
            ELEM_TIMESTAMP : aniso8601.parse_datetime(event['time']),
            ELEM_FLAG_LIST : [FLAG_MANUAL]
        }

        self.data_points.append(dp)

    def eval_serial(self, data):
        return data if ELEM_DEVICE_SERIAL in data else {}

    def eval_model(self, data):
        return {ELEM_DEVICE_MODEL: 'User'}
        
    def eval_class(self, data):
        return {ELEM_DEVICE_CLASS: DEVICE_USER}

    def eval_class(self, data):
        return {}

    def eval_unit(self, data):
        return {ELEM_DEVICE_UNIT: 'mg/dl'}

    def eval_result(self, data):
        if ELEM_VAL_TYPE in data or SETTINGS_LIST in data:
            return data
        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterDexcomCloud(data):
    '''
    Detect if data comes from Dexcom Cloud extractor.
    '''
    return DetectDevice('Dexcom Cloud', data, DEVICE_USER)

def AnalyseMeterDexcomCloud(data):
    '''
    Analyse data from Dexcom cloud.
    '''
    try:

        dexcom_cloud_analyser = DexcomCloudAnalyser()
        dexcom_cloud_analyser.split(data)

        callbacks = {
            'eval_serial_record'        : dexcom_cloud_analyser.eval_serial,
            'eval_device_model_record'  : dexcom_cloud_analyser.eval_model,
            'eval_device_class_record'  : dexcom_cloud_analyser.eval_class,
            'eval_unit'                 : dexcom_cloud_analyser.eval_unit,
            'eval_result_record'        : dexcom_cloud_analyser.eval_result,
            'eval_device_class_record'  : dexcom_cloud_analyser.eval_class,
        }

    except Exception, e:

        import traceback
        traceback.print_exc()

        serial_number = dexcom_cloud_analyser.user_device_serial
        
        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(DEVICE_MODEL,
                                                     serial_number, e.args[0],
                                                     e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(DEVICE_MODEL,
                                                     serial_number,
                                                     ERROR_CODE_PREPROCESSING_FAILED,
                                                     ''.join(map(str, e.args)))

        return error_response

    return AnalyseGenericMeter(dexcom_cloud_analyser.data_points, callbacks)

if __name__ == "__main__":

    test_files = [
        'daniel.log'
    ]

    for test_file in test_files:
        with open('test/testcases/test_data/DexcomCloud/%s' % (test_file), 'rb') as f:
            data = f.read()
            results = AnalyseMeterDexcomCloud(data)
            print results[0]['header']
            #for result in results[0]['results']:
            #   print result
