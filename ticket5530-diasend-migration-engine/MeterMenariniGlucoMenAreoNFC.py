# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# -----------------------------------------------------------------------------

# @DEVICE Menarini GlucoMen areo (via NFC)

from Generic import *
from Defines import *
from datetime import datetime
import struct
from MeterMenariniGlucoMenLX import MODEL_NAME_GLUCOMEN_AREO

# -----------------------------------------------------------------------------
# DEFINES
# -----------------------------------------------------------------------------
AREO_NFC_DIASEND_SEPARATOR       = "!TAG!SEPARATOR!"

AREO_NFC_COMMAND_BARCODE         = '\xdd\x0f'
AREO_NFC_COMMAND_FOUR_RECORDS    = '\xdd\x10'

AREO_NFC_POS_REPLY   = 0
AREO_NFC_POS_OPCODE  = 1
AREO_NFC_POS_PAYLOAD = 2

AREO_NFC_POS_CHECKSUM_BARCODE = 10
AREO_NFC_POS_CHECKSUM_FOUR_RECORDS = 28

AREO_NFC_LENGTH_SERIALNUMBER = 8
AREO_NFC_LENGTH_PAYLOAD_RECORD = 7



# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------

def SplitIndata(indata):
    """
    Splits incoming data into a list. 
    
    The data consists of records, all starting with a Diasend-specific separator
    """
    
    # Split according to tags
    inList = indata.split(AREO_NFC_DIASEND_SEPARATOR)
    
    # Remove any empty lines (i.e the first one)
    inList = filter(None, inList)
    
    return inList


def EvalHeaderRecord(line):
    """
    Evaluate if this is a GlucoMen areo NFC header record
    """

    m = {}
    if line.startswith(AREO_NFC_COMMAND_BARCODE):
        # Line is a barcode record
    
        # The serial number is written in reverse. 
        m[0] = line[AREO_NFC_POS_PAYLOAD : AREO_NFC_POS_PAYLOAD+AREO_NFC_LENGTH_SERIALNUMBER][::-1]
        checksum = ord(line[AREO_NFC_POS_CHECKSUM_BARCODE])

        # Calculate the checksum for this record (OP Code is part of it)
        calc_cs = []
        for b in line[AREO_NFC_POS_OPCODE : AREO_NFC_POS_PAYLOAD+AREO_NFC_LENGTH_SERIALNUMBER]:   
            calc_cs.append(ord(b))

        calc_cs = sum(calc_cs) & 0xff

        # Only set a serial number if valid checksum
        if checksum != calc_cs:
            return None

    return m


def EvalFourResultRecord(line):
    """
    Evaluate if this a four record block
    """
    m = []
    
    if line.startswith(AREO_NFC_COMMAND_FOUR_RECORDS):
        pl = line[AREO_NFC_POS_PAYLOAD:]

        # Four records for each block 
        for record_number in range(0, 4):

            num = record_number*AREO_NFC_LENGTH_PAYLOAD_RECORD
            # Create placeholder object
            o = type("Dummy", (object,), {})
            
            # Extract binary data according to the memory map
            data0 = ord(pl[num])
            data1 = ord(pl[num+1])
            data23 = struct.unpack("<H", pl[num+2:num+4])[0]
            data4 = ord(pl[num+4])
            data5 = ord(pl[num+5])
            data6 = ord(pl[num+6])
            
            # Blocks containing all 0xFFs should be thrown away (end of memory).
            empty_block = '\xff' * 7
            data_block = pl[num:num+7]
            if data_block == empty_block:
                continue

            # Unit
            if (data0 & 0x80):
                o.unit = "mg/dl"
            else:
                o.unit = "mmol/l"
                        
            # Flags 
            o.control  = data0 & 0x40
            o.exercise = data1 & 0x08
            o.fishbone = data1 & 0x04
            o.fish     = data1 & 0x02
            o.check    = data1 & 0x01

            # Date
            year  = data0 & 0x3f
            month = (data1 & 0xf0) >> 4
            day   = data23 & 0x001f
            minute = data23 >> 10
            hour   = (data23 & 0x03e0) >> 5

            # Value
            if data5 == 0x20:
                # Coding according to mg/dl
                value = data4 * 10 + data6
                
                # Check high/low flags
                o.low = value <= 19
                o.high = value >= 601
                
            elif data5 == 0x2e:
                # Coding according to mmol/l
                value = data4 + 0.1 * data6
                
                # Check high/low flags. Note: Normal range is 1.1 < value > 33.4 mmol(l). Multiply by 10 to avoid float compares
                o.low = int(value*10) <= int(1.1*10)
                o.high = int(value*10) >= int(33.4*10)
            else:
                # Incorrect coding
                raise Exception(ERROR_CODE_VALUE_ERROR, 'Incorrectly coded unit type, bug?')
                return False
                
            # The USB Glucomen LX analyser sets the values to zero if flags are present, so that needs to be done here as well.
            if o.low or o.high:
                value = 0
                
            if o.unit == "mg/dl":
                # Adjust the factor for the analyser. 
                # Note that no adjustment of mgdl calc (18.0/18.016 etc) is done because it is not done in the USB Glucomen LX analyser.
                value = value*VAL_FACTOR_GLUCOSE_MGDL
            else:
                value = value*VAL_FACTOR_GLUCOSE_MMOL

            o.year = year
            o.month = month
            o.day = day
            o.hour = hour
            o.minute = minute
            o.value = value

            m.append(o)

    return m


def EvalGlucoMenAreoNFCResultRecord(line):
    """
    Evaluate Glucomen areo result record.
    """
    
    resultList = []
    m = EvalFourResultRecord(line);

    for rec in m:
        # Convert the flags
        flags = []
        flags.append(FLAG_RESULT_HIGH)     if rec.high else None
        flags.append(FLAG_RESULT_LOW)      if rec.low else None
        flags.append(FLAG_RESULT_CTRL)     if rec.control else None
        flags.append(FLAG_CHECK_MARK)      if rec.check else None
        flags.append(FLAG_BEFORE_MEAL)     if rec.fish else None
        flags.append(FLAG_AFTER_MEAL)      if rec.fishbone else None
        flags.append(FLAG_DURING_EXERCISE) if rec.exercise else None

        # Create the finished value
        res = {
            ELEM_TIMESTAMP      : datetime(rec.year+2000, rec.month, rec.day, rec.hour, rec.minute),
            ELEM_VAL_TYPE       : VALUE_TYPE_GLUCOSE,
            ELEM_VAL            : rec.value,
            ELEM_DEVICE_UNIT    : rec.unit,
            ELEM_FLAG_LIST      : flags
        }

        resultList.append(res)

    return resultList

def EvalGlucoMenAreoNFCSerialRecord(line):
    """
    Evaluate Glucomen areo serial number.
    """
    res = {}
    m = EvalHeaderRecord(line)

    if m:
        serial_no = m[0];
        res[ELEM_DEVICE_SERIAL] = serial_no;

    return res


def EvalGlucoMenAreoNFCDeviceModelRecord(line):
    """
    The model is always Glucomen areo.
    """
    res = {}
    res[ELEM_DEVICE_MODEL] = MODEL_NAME_GLUCOMEN_AREO
    return res
    
def EvalGlucoMenAreoNFCChecksumRecord(line, record):
    """
    Evaluate checksum of result record.
    """
    if line.startswith(AREO_NFC_COMMAND_FOUR_RECORDS):
        pl = line[AREO_NFC_POS_PAYLOAD:]

        # Calculate checksum for four records at a time
        calc_cs = [ord(line[AREO_NFC_POS_OPCODE])]
        for num in range(0, 4*AREO_NFC_LENGTH_PAYLOAD_RECORD):
            calc_cs.append(ord(pl[num]))
        calc_cs = sum(calc_cs) & 0xff
        
        checksum = ord(pl[AREO_NFC_POS_CHECKSUM_FOUR_RECORDS])
        
        return checksum == calc_cs

    return True
    

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMenariniGlucoMenAreoNFC( inList ):
    """
    Detect if data comes from an Areo NFC.
    """
    return DetectDevice( 'MenariniGlucoMenAreoNFC', inList, DEVICE_METER );

def AnalyseMenariniGlucoMenAreoNFC(inData):
    """
    Analyse Menarini GlucoMen areo NFC
    """

    inList = SplitIndata(inData)

    d = {}

    d["eval_serial_record"]         = EvalGlucoMenAreoNFCSerialRecord
    d['eval_device_model_record']   = EvalGlucoMenAreoNFCDeviceModelRecord
    d['eval_result_record']         = EvalGlucoMenAreoNFCResultRecord
    d['eval_checksum_record']       = EvalGlucoMenAreoNFCChecksumRecord


    resList = AnalyseGenericMeter( inList, d );
    return resList


if __name__ == "__main__":
    testfile = open("test/testcases/test_data/GlucoMenAreo/NFC/GlucoMenAreoNFC_WW004720_600values.log")
    
    testcase = testfile.read()
    testfile.close()

    res = AnalyseMenariniGlucoMenAreoNFC(testcase)
    print(res)
