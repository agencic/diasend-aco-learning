import os
import sys
import ConfigParser

settings = None

class ParseException(Exception):
    """
    No configuration file could be read/parsed. 
    """
    def __init__(self, text):
        self.text = text
        
    def __str__(self):
        return self.text

def init():
    """
    Try to read and parse a backend configuration file, first in current working directory,
    then in /etc/diasend/ .

    Returns a dictionary of settings, or raises an exception if no configuration can be read.
    """

    global settings 

    if not settings:
        filenames = [os.getcwd()+"/diasend-backend.conf", "/etc/diasend/diasend-backend.conf"]
        try:
            for filename in filenames:
                config = ConfigParser.SafeConfigParser()
                file_found = config.read(filename)
                if file_found:
                    print "Config read successfully"
                    break

            if not file_found:
                raise ParseException("No configuration found.")
        except ConfigParser.Error, e:
            raise ParseException("Couldn't parse " + filename + ": " + e.message)

    print "Configuration was read from", filename

    # Concatenate section/option together to create and return
    # a dictionary of <section-option : value>
    settings = {}
    for section in config.sections():
        for item in config.items(section):
            settings[section+"-"+item[0]] = item[1]

    return settings


# If settings is not initialized when this module is sourced;
# initialize
if settings == None:
    init()


# Code below is for testings only
def main():
    print settings

if __name__ == "__main__":
    sys.exit(main())
