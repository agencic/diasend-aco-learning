# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Sanofi iBGStar

import re
from Generic import *
from Defines import *
from datetime import *

# -----------------------------------------------------------------------------
# GLOBALS
# -----------------------------------------------------------------------------
glob_date_format = None
glob_time_format = None

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord(line):
    """
    Evaluates the serial record (user device) - comes from the generic Email Import Handler format
    
    """
    return re.match( r'^!TAG!SERNO!(DVU[A-Za-z0-9@._]+)!TAG!BODY!', line, re.IGNORECASE )


def EvalUnitRecord(line):
    """
    Evaluates the unit record
    
    Glucose Unit,mg/dL
    
    """
    
    return re.match( r'Glucose Unit,(mg/dL|mmol/L)', line, re.IGNORECASE )


def EvalDateFormatRecord(line):
    """
    Evaluates the unit record
    
    Glucose Unit,mg/dL
    
    """
    return re.match( r'Date Format,(.*)', line, re.IGNORECASE )

def EvalTimeFormatRecord(line):
    """
    Evaluates the unit record
    
    Glucose Unit,mg/dL
    
    """
    return re.match( r'Time Format,(.*)', line, re.IGNORECASE )


def EvalResultRecord( line ):
    """
    Evaluate an iBGStar CSV file result record. Extracted groups : 
    group 1 : date
    group 2 : time
    group 3 : flags
    group 4 : BG value
    group 5 : carb value
    group 6 : insulin value
    group 7 : insulin type
    group 8 : notes
    
    28/09/2012,09:20 CEST,(No Tag),100,,,,Manuel^M
    28/09/2012,09:07 CEST,Post-Breakfast,234,,,,Raisin^M
    06/05/2012,20:52 GMT+02:00,(No Tag),104,,,,^M
    06/05/2012,13:58 GMT+02:00,(No Tag),118,,,,^M
    06/05/2012,12:41 GMT+02:00,(No Tag),112,,,,^M
    6/21/13,9:29 PM CDT,(No Tag),500,,,,
    6/21/13,9:27 PM CDT,Post-Dinner,250,58,8.0,Rapid Acting,
    """
    return re.match( r'(.*),(.*) .*,(.*),(\d+),(.*),(.*),(.*),(.*)', line, re.IGNORECASE )

def EvalFlags(flagtext):
    """
    Parsing list of flags into diasend flags
    """
    flags = []
    flags.append(FLAG_ORIGIN_FILE_IMPORT)
    
    _flag_map = {
        "Pre-Breakfast" : FLAG_BEFORE_BREAKFAST,
        "Post-Breakfast" : FLAG_AFTER_BREAKFAST,
        "Pre-Lunch" : FLAG_BEFORE_LUNCH,
        "Post-Lunch" : FLAG_AFTER_LUNCH,
        "Pre-Dinner" : FLAG_BEFORE_DINNER,
        "Post-Dinner" : FLAG_AFTER_DINNER,
        "Night" : FLAG_NIGHT
    }

    if flagtext in _flag_map:
        flags.append(_flag_map[flagtext])
        
    return flags

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvaliBGStarSerialRecord( line ):
    """
    """
    res = {}
    m = EvalSerialRecord(line)
    if m:
        res[ELEM_DEVICE_SERIAL] = m.group(1)

    return res

def EvaliBGStarUnitRecord( line ):
    """
    """
    res = {}
    m = EvalUnitRecord(line)
    if m:
        res[ELEM_DEVICE_UNIT] = m.group(1)

    return res
    
def EvaliBGStarResultRecord( line ):
    """
    Is this a result record? If so, return a dictionary
    
    """
    global glob_date_format
    global glob_time_format

    # Try to get the date and time formats and convert them to our conversion algorithms. @todo Handle nicer - in separate functionality
    m = EvalDateFormatRecord(line)
    if m:
        if m.group(1) == "dd/MM/yyyy":
            glob_date_format = "%d/%m/%Y"
        elif m.group(1) == "M/d/yy":
            glob_date_format = "%m/%d/%y"
    m = EvalTimeFormatRecord(line)
    if m:
        if m.group(1) == "HH:mm":
            glob_time_format = "%H:%M"
        elif m.group(1) == "h:mm a":
            glob_time_format = "%I:%M %p"

    # Analyse an actual record
    m = EvalResultRecord( line )
    results = []
    if m:
        if glob_date_format and glob_time_format:
            res = {}
            # Set the timestamp
            date_time = m.group(1)+", "+m.group(2)
            format = glob_date_format+", "+glob_time_format
            res[ELEM_TIMESTAMP] = datetime.strptime(date_time, format)

            # Set BG value
            res[ELEM_FLAG_LIST] = EvalFlags(m.group(3))
            res[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
            res[ELEM_VAL] = float(m.group(4))
            results.append(res)

            # Set carb value, if available
            try:
                carb_val = float(m.group(5))
            except:
                carb_val = -1
                
            if carb_val >= 0:
                res_carb = {}
                res_carb[ELEM_TIMESTAMP] = res[ELEM_TIMESTAMP]
                res_carb[ELEM_VAL_TYPE] = VALUE_TYPE_CARBS
                res_carb[ELEM_FLAG_LIST] = EvalFlags(None)
                res_carb[ELEM_VAL] = carb_val
                results.append(res_carb)
                
            # Set insulin value, if available
            try:
                insulin_val = float(m.group(6))
            except:
                insulin_val = -1
                
            # Only knows about rapid acting insulin for now. 
            # @todo Support more types
            if (insulin_val >= 0) and (m.group(7) == "Rapid Acting"):
                res_ins = {}
                res_ins[ELEM_TIMESTAMP] = res[ELEM_TIMESTAMP]
                res_ins[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS
                res_ins[ELEM_FLAG_LIST] = EvalFlags(None)
                res_ins[ELEM_VAL] = insulin_val * VAL_FACTOR_BOLUS
                results.append(res_ins)
                
        else:
            res = { "error_code":ERROR_CODE_DATE_TIME, "line":line, "fault_data":"" }
            results.append(res)
            
        
    return results


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectCSViBGStar( inList ):
    """
    Detect if data comes from a Freestyle.
    """
    return DetectDevice( 'CSViBGStar', inList, DEVICE_USER );

def AnalyseCSViBGStar( inData ):
    """
    Analyse iBGStar data
    """

    global glob_date_format
    global glob_time_format

    glob_date_format = None
    glob_time_format = None

    # Setup the handlers
    d = {}
    d[ "meter_type" ]           = "iBGStar CSV"
    d[ "eval_serial_record"]    = EvaliBGStarSerialRecord
    d[ "eval_unit"]             = EvaliBGStarUnitRecord
    d[ "eval_result_record" ]   = EvaliBGStarResultRecord

    # Since different platforms handle the carriage return differently - we will remove those characters, only leaving "new line".
    inData = inData.replace("\r", "")
    # Split up all rows on new lines.
    inList = inData.split('\n')

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    
    #testfile = open('test/testcases/test_data/FileCSViBGStar/ibgstar-date1.bin')
    testfile = open('test/testcases/test_data/FileCSViBGStar/ibgstar-date2.bin')
    testcase = testfile.read()
    testfile.close()
    
    print AnalyseCSViBGStar(testcase)
