# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import struct

from Defines import *

class BaseRecord(object):
    """
    Base class for all Dexcom records.
    """

    def __init__(self, buffer, payload_len = -1, consume_data=True):
        """
        Construct a record from the buffer provided (unserialize using the
        format string provided by the sub-class).

        @param buffer
            Buffer.Buffer instance - holds binary data.

        @param payload_len
            Length (extracted from header) of payload. Must match the length
            of the struct format string.

        @param consume_data
            If True consume the data in the buffer when unserializing.

        Function will raise Record.ConvertException if an error is detected.
        """
        # Retrieve the expected payload length from the class and compare with the
        # provided length.
        if payload_len != -1:
            my_payload_len = self.record_length()
            if my_payload_len != payload_len:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Payload length does not match record (payload %d record %d).' % (payload_len, my_payload_len))
        else:
            payload_len = self.record_length()

        index = 0

        # Save raw buffer for CRC check (later).
        self.raw_buffer = buffer.peek_slice(payload_len)

        # If the format is a string, do not loop over each character it breaks
        # for instance '8s' since the 8 will be handled as '<8' which isn't
        # valid, if its a string, create a list of strings so the string is
        # handled in one go
        if isinstance(self.format, str):
            formatting = [self.format]
        else:
            formatting = self.format

        # The format string can be divided into sub-string where each substring is either a
        # Python struct string _or_ a record class.
        for sub_format in formatting:
            # This is a Python struct string - unserialize.
            if isinstance(sub_format, str):
                if len(sub_format) > 0:

                    # We only support little endian so add the Python struct '<' constant.
                    format_string = BaseRecord.prefix_format_string(sub_format)
                    sub_format_len = struct.calcsize(format_string)
                    if consume_data:
                        index += self.unserialize(buffer.get_slice(sub_format_len), format_string, self.variables, index)
                    else:
                        index += self.unserialize(buffer.peek_slice(sub_format_len), format_string, self.variables, index)

            # This is is a class! Create an instance and store it in the provided variable name.
            # Recursion rocks!
            else:
                self.__dict__[self.variables[index]] = sub_format(buffer, sub_format.record_length())
                index += 1

        self.validate()

    def __str__(self):
        """
        Poor mans generic string of a class. If necessary this can be made to look really pretty
        using the format and variables strings.
        """
        return str(self.__dict__)

    def unserialize(self, data, format, variables, index=0):
        """
        Unserialize data in the form of a string (containing binary data) and map
        the data to instance variables.

        The function uses Python struct module to unserialize a C struct into
        a tuple. This tuple is then mapped into variables from the self.variables
        tuple (provided by the sub-class).

        If the sub-class has the following variables :

            self.format = "<BB"
            self.variables = ("byte1", "byte2")

        a call to unserialize('\x12\x34') will create variables byte1 and byte2
        in the sub-class and set byte1 == 0x12 and byte2 == 0x34.

        @param data
            The binary data to unpack.

        @param format
            The Python struct format string.

        @param variables
            A tuple with the variable names. Note : this can be a tuple with more variables then
            necessary - just use the index to indicate where the function should start
            mapping.

        @param index
            Index into the variables tuple - where to start mapping data to variables.

        @return Number of variables matched.
        """
        data = struct.unpack(format, data)
        my_variables = variables[index:index + len(data)]
        self.__dict__ = dict(self.__dict__, **dict(zip(my_variables, data)))

        return len(data)

    @classmethod
    def prefix_format_string(cls, string):
        """
            If a format string is lacking the byte order character '<' or '>'
            then lets add a little endian marker in the beginning...
        """
        if string and string[0] not in '<>':
            return '<' + string
        else:
            return string

    @classmethod
    def build_format_string(cls, standalone=True):
        """
        Build a Python struct format string by (if necessary recursively) walk
        through the format variable of the class.

        @param cls
            Class reference

        @param standalone
            Prefix the string with '<' (little endian) if True.
            And no other byte order formatting is already in place

        @return format string.
        """
        format_string = ''
        endian_prefixes = []
        for format in cls.format:
            if isinstance(format, str):
                next_format_string = format
            else:
                next_format_string = format.build_format_string(False)
            # Its not allowed to have a endian format in the middle
            # save them and add in the end.
            if next_format_string[0] in '<>':
                endian_prefixes.append(next_format_string[0])
                next_format_string = next_format_string[1:]
            format_string += next_format_string

        if endian_prefixes:
            chosen = None
            for prefix in endian_prefixes:
                if not chosen:
                    chosen = prefix
                elif chosen != prefix:
                    raise Exception(ERROR_CODE_VALUE_ERROR, 'Multiple endian prefixes used')
            format_string = chosen + format_string

        if standalone:
            return BaseRecord.prefix_format_string(format_string)
        else:
            return format_string

    @classmethod
    def record_length(cls):
        """
        Return length (as reported by struct.calcsize) for a record.
        """
        format = cls.build_format_string()
        return struct.calcsize(format)

    def convert_to_diasend_format(self):
        """
        Convert record to Diasend format.

        Returns dictionary - empty if no useful data.
        """
        return {}

    def validate(self):
        return True

class RecordConsumeData(BaseRecord):
    """
    Generic record that simply consumes the number of bytes
    provided in the constructor.
    """
    format = ('',)
    variables = ('',)

    def __init__(self, buffer, payload_len, consume_data=True):
        """
        Create struct format with a single parameter.
        """
        if payload_len > 0:
            RecordConsumeData.format = ('%ds' % (payload_len),)
            RecordConsumeData.variables = ('all'),
        else:
            RecordConsumeData.format = ('',)
            RecordConsumeData.variables = ('',)

        super(RecordConsumeData, self).__init__(buffer, payload_len, consume_data)

class EnumMetaClass(type):
    '''
    EnumRecord sub-classes store enum definitions in a dictionary named
    'enums'. This meta-class makes it possible to access enum definitions 
    using a dot syntax instead of using the dictionary.

    Example (using class dictionary) : 

        EnumEventType.enums.pump_stopped

    Example (using this meta-class) : 

        EnumEventType.pump_stopped
    '''
    def __getattr__(cls, name):
        if name in cls.enums:
            return cls.enums[name]
        raise AttributeError

class EnumRecord(BaseRecord):
    '''
    Base class for all enums. 
    '''
    variables                 = ('value',)
    __metaclass__             = EnumMetaClass
    map_enum_to_diasend       = {}

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        return not self.value < other and not other < self.value

    def __ne__(self, other):
        return self.value < other or other < self.value

    def __gt__(self, other):
        return other < self.value

    def __ge__(self, other):
        return not self.value < other

    def __le__(self, other):
        return not other < self.value

    def validate(self):
        '''
        Is the (raw) value a defined enum? If not 
        raise an exception.
        '''
        if not self.value in self.enums.values():
            raise Exception(ERROR_CODE_VALUE_ERROR, 'Enum (%s) : enum value %d not defined' % (self.__class__, self.value))

    def to_text(self):
        for key, value in self.enums.items():
            if not key.startswith('__') and not key.endswith('__') and not key == 'value':
                if value == self.value:
                    return str(key)
        return '(unknown enum %d)' % (self.value)

    def to_diasend(self):
        for key, value in self.enums.items():
            if not key.startswith('__') and not key.endswith('__') and not key == 'value':
                if value == self.value:
                    if key in self.map_enum_to_diasend:
                        return self.map_enum_to_diasend[key]
        raise Exception(ERROR_CODE_VALUE_ERROR, 'Enum (%s) - failed to convert enum value %d to diasend format' % (self.__class__, self.value))

class EnumBitRecord(BaseRecord):
    '''
    Base class for all bit-bases enums. Will store the 
    individual bits in self.bits (dict) and the 
    raw value in value. 
    '''
    variables               = ('value',)
    __metaclass__           = EnumMetaClass
    map_enum_to_diasend     = {}
    ignore_not_defined_bits = False # Define as True in child-class to accept (and ignore) undefined bits set to 1.

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        return not self.value < other and not other < self.value

    def __ne__(self, other):
        return self.value < other or other < self.value

    def __gt__(self, other):
        return other < self.value

    def __ge__(self, other):
        return not self.value < other

    def __le__(self, other):
        return not other < self.value

    def validate(self):
        self.bits = {}
        sum = self.value
        for key, value in self.enums.items():
            if value & self.value:
                sum -= value
                self.bits[key] = 1
            else:
                self.bits[key] = 0
        if sum != 0 and not self.ignore_not_defined_bits:
            raise Exception(ERROR_CODE_VALUE_ERROR, 'Enum bit (%s) - enum bit value %d not (fully) defined' % (self.__class__, self.value))

    def to_diasend_list(self):
        flags = []
        for key, value in self.enums.items():
            if not key.startswith('__') and not key.endswith('__') and not key == 'value':
                if value & self.value:
                    if key in self.map_enum_to_diasend:
                        # The bit flag can be set to None to indicate no flag
                        if self.map_enum_to_diasend[key] != None:
                            flags.append(self.map_enum_to_diasend[key])
                    else:
                        raise Exception(ERROR_CODE_VALUE_ERROR, 'Enum (%s) - failed to convert enum bit value %d to diasend format' % (self.__class__, self.value))
        return flags
