# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Diamesco Pendiq

# Decoding is based on Diamesco Pendiq Communications Specification 2011-12-12.
# Note! Serial record response length deviates from the specifiction (19 
# instead of 18 bytes).

import re
from Generic import *
from Defines import *
from datetime import datetime


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------


def UpdateChecksum(initial_crc, byte):
    """
    Update the initial checksum with another checksum byte.
    The checksum is using XOR on the previous initial value/
    checksum.
    """
    return initial_crc ^ byte
    


# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Pendiq serial number record. Extracted groups : 
     1  Cartridge size
     2  Unit
     3  Cartridge product
     4  Serial number
     5  csum
    
    The serial number record looks like this before stripping off CR:
    '<ESC>30100B0601000035F<CR>'
    """

    # The length of the result records shall always be 19 - 1 bytes (stripped off \r)    
    if len(line) != 18:
        return None
    
    m = re.match(r'\x1B([023]{1,1})([01]{1,1})([\d]{1,1})([\w]{12,12})([\dA-F]{2,2})', line, re.IGNORECASE )

    return m

def EvalNoResultRecord(line):
    """
    Evaluate a Pendiq data count record. Extracted groups : 
     1  Count
     2  csum
    
    The serial number record looks like this before stripping off CR:
    '<ESC>D00005F<CR>'
    """

    # The length of the result records shall always be 9 - 1 bytes (stripped off \r)    
    if len(line) != 8:
        return None
    
    m = re.match(r'\x1B[D]{1,1}([\d]{4,4})([\dA-F]{2,2})', line, re.IGNORECASE )

    return m  



def EvalResultRecord( line ):
    """
    Evaluate a Pendiq result record. Extracted value : 

    1 Date - YYMMDD
    2 Time - HHmm
    3 Value - nnnn 0000 - 9999 representing n.nnn ml or nnn.n u
    

    Lines look like this before stripping off CR:
    '<ESC>D1108171035023057<CR>'
    """

    # The length of the result records shall always be 19 - 1 bytes (stripped off \r)
    if (len(line) != 18):
        return None
    
    return re.match( r'\x1BD([0-9]{6,6})([0-9]{4,4})([0-9]{4,4})([\dA-F]{2,2})', line, re.IGNORECASE )

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalDiamescoPendiqSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}

    m = EvalSerialRecord(line)
    if m:
        res[ ELEM_DEVICE_SERIAL ] = m.group(4)

    return res

def EvalDiamescoPendiqNrOfResultRecord(line):
    """
    Is this a number of results record? If so, return a dictionary with 
    the number of results. 
    """
    res = {}
    
    m = EvalNoResultRecord(line)

    if m:
        res["meter_nr_results"] = int(m.group(1))

    return res

def EvalDiamescoPendiqResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    

    m = EvalResultRecord(line)

    if not m:
        return {}

    res = []
    rec = {} 
    flags = []
        
    if m:

        year    = int(m.group(1)[:2]) + 2000
        month   = int(m.group(1)[2:4])
        day     = int(m.group(1)[4:6])
        
        hour    = int(m.group(2)[:2])
        min     = int(m.group(2)[2:4])
        
        try:
            rec[ ELEM_TIMESTAMP ] = datetime(year, month, day, hour, min)
        except:
            rec[ ELEM_TIMESTAMP ] = None



        rec[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS_BASAL
        # The Bolus Basal value is stored in the Diasend DB as 10000 * u
        # Pendiq represents the value in 1/10 u
        # Hence multiply with 10000/10
        value = int(m.group(3))*1000      
        rec[ ELEM_VAL ] = value
    
        res.append(rec)
        
    return res


def EvalDiamescoPendiqChecksumRecord( line, record ):
    """
    Evaluate checksum of result record. 
    
    The checksum is based on plain XOR summing 
    """
    cs = 0
    
    for c in line[:len(line) - 2]:
        cs = UpdateChecksum(cs, ord(c))

    given_cs = int(line[len(line) - 2: len(line)], 16)
    
    return cs == given_cs

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectDiamescoPendiq( inList ):
    """
    Detect if data comes from a Diamesco Pendiq Infusion Pen
    """
    return DetectDevice( 'Pendiq', inList, DEVICE_PEN )

def AnalyseDiamescoPendiq( inData ):
    """
    Analyse Diamesco Pendic Digital Infusion Pen
    """

    inList = inData.split('\r')
    
    # Empty dictionary
    d = {}

    d[ "meter_type" ]               = "Diamesco Pendiq"
    d[ "eval_serial_record" ]       = EvalDiamescoPendiqSerialRecord
    d[ "eval_result_record" ]       = EvalDiamescoPendiqResultRecord
    d[ "eval_nr_results" ]          = EvalDiamescoPendiqNrOfResultRecord
    d[ "eval_checksum_record" ]     = EvalDiamescoPendiqChecksumRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":


    #  testfile = open('test/testcases/test_data/DiamescoPendiq/Pendiq-00B060100003-1_value.bin')
    #testfile = open('test/testcases/test_data/DiamescoPendiq/Pendiq-00B060100010-143_values.bin')
    #testfile = open('test/testcases/test_data/DiamescoPendiq/Pendiq-00B060100010-196_values.bin')
    testfile = open('test/testcases/test_data/DiamescoPendiq/Pendiq-00B060100010-196_values_2_timezones.bin')   
    testcase = testfile.read()
    testfile.close()

    print AnalyseDiamescoPendiq(testcase)
