#!/bin/sh

openssl genrsa -out certs.pem 1024
openssl genrsa -out privkey.pem 1024
openssl req -new -key privkey.pem -out certreq.csr -config ssl-config
openssl x509 -req -days 3650 -in certreq.csr -signkey privkey.pem -out cert.pem

