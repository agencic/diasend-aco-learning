# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2008 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Animas IR1200
# @DEVICE Animas IR1250
# @DEVICE Animas 2020
# @DEVICE LifeScan OneTouch Ping
# @DEVICE Animas Vibe
# @DEVICE OneTouch Vibe Plus

import re
from Generic import *
from Defines import *
from datetime import datetime, timedelta
from datetime import time
import math
import struct
import Common
import MeterLifescanOnetouchUltraSmart
#import DexComUploader

# -----------------------------------------------------------------------------
# DEFINES the endian specific tagging of each packet received from the pump
# -----------------------------------------------------------------------------
TYPE_PUMP_SERIAL_NUMBER     = 1  # Unique pump serial number FACTORY SET 
TYPE_BASAL_PROGRAMS         = 2  # Basal Programs 0,1,2, and 3 
TYPE_ADV_SETTINGS           = 3  # User Advanced Setup settings1 
TYPE_BASAL_HISTORY          = 4  # Basal Rate Change event information, including rate, and time/datestamp 
TYPE_BOLUS_HISTORY          = 5  # Bolus delivery event information, including amounts and  time/datestamp 
TYPE_TDD_HISTORY            = 6  # Total Daily Delivery event information, including  amounts and datestamp 
TYPE_NR_RESULTS             = 7  # Total number of results sent from terminal to server, Aidera specific
TYPE_ALARM_HISTORY          = 8  # Pump Alarm history event
TYPE_PRIME_HISTORY          = 9  # Pump Prime History event
TYPE_ACTIVE_BASAL_PROGRAM   = 10 # Pump Active basal program
TYPE_ADV_SETTINGS_2         = 11 # User Advanced Setup settings2
TYPE_ADV_SETTINGS_3         = 12 # User Advanced Setup settings3
TYPE_SW_VERSION             = 13 # Software Version record
TYPE_BG_DISPLAY_MODE        = 14 # BG Display Mode record
TYPE_BG_HISTORY             = 15 # BG History
TYPE_REMOTE_NAME            = 16 # Name of the remote meter
TYPE_BOLUS2_HISTORY         = 17 # The Bolus2 History (carbs etc)
TYPE_CGM_SETTINGS           = 18 # CGM Settings
TYPE_CGM_WARNING            = 19 # CGM Warning history
TYPE_CGM_SESSIONS           = 20 # CGM Session history
TYPE_CGM_GLUCOSE            = 21 # CGM Glucose history
TYPE_CGM_DEBUG              = 22 # CGM Glucose Engine Debug data
TYPE_CGM_TIME_ADJUST        = 23 # CGM Time adjust history
TYPE_SUSPEND_RESUME_HISTORY = 24 # Suspend/Resume events history
TYPE_CLOCK_MODE             = 25 # Clock Display Mode record
TYPE_BLACK_BOX_HISTORY      = 29 # Black box history record
TYPE_PACK_FLAG              = 0x80 # This item is packed

# Special CGM glucose record types according to Unity CGM Records Software Design Rev 02
TYPE_CGM_GLUCOSE_ADJ_TIME_BEFORE_TIME_LOST   = 0x400  # Accumulated Adjust Time record just before Time Lost
TYPE_CGM_GLUCOSE_BG_CALIBRATION_RECORD       = 0x800  # BG Calibration Record
TYPE_CGM_GLUCOSE_TIME_CHANGED                = 0x1000 # Display time was changed
TYPE_CGM_GLUCOSE_TIME_LOST                   = 0x2000 # RTC Time was lost
TYPE_CGM_GLUCOSE_STOP_SESSION                = 0x4000 # Session was stopped 
TYPE_CGM_GLUCOSE_START_SESSION               = 0x8000 # Session was started
TYPE_H_CGM_GLUCOSE_START_SESSION             = 0x8400 # Same records as above _but_ for Vibe model >= H1a
TYPE_H_CGM_GLUCOSE_STOP_SESSION              = 0x8800
TYPE_H_CGM_GLUCOSE_TIME_LOST                 = 0x8c00
TYPE_H_CGM_GLUCOSE_TIME_CHANGED              = 0x9000
TYPE_H_CGM_GLUCOSE_BG_CALIBRATION_RECORD     = 0x9400
TYPE_H_CGM_GLUCOSE_ADJ_TIME_BEFORE_TIME_LOST = 0x9800
CGM_GLUCOSE_RECORD_TYPE_MASK                 = 0xFC00 # Mask for the Record type
CGM_GLUCOSE_VALUE_MASK                       = 0x01FF # Mask for the Value


ANIMAS_CTL_CHAR             = "\x7d"

BG_DISPLAY_MODE_MG      = 0xff00
BG_DISPLAY_MODE_MMOL    = 0xfe01

# Mask bits for Bolus1 type field
BOLUS1_TYPE_TRIG_MASK       = 0xC0
BOLUS1_TYPE_TRIG_EZBG       = 0x40
BOLUS1_TYPE_TRIG_EZCARB     = 0x80
BOLUS1_TYPE_REMOTE_CANCELLED= 0x20
BOLUS1_TYPE_REMOTE_INITIATED= 0x10

# Mask bits for Bolus2 configuration field
BOLUS2_CONFIGURATION_BGMODE_MMOL    = 0x02
BOLUS2_CONFIGURATION_IOB_ENABLED    = 0x04
BOLUS2_CONFIGURATION_EZCARB_BG_CORR = 0x08


DEVICE_MODEL_IR1200         = "Animas IR1200"
DEVICE_MODEL_IR1250         = "Animas IR1250"
DEVICE_MODEL_2020           = "Animas 2020"
DEVICE_MODEL_PING           = "OneTouch Ping"
DEVICE_MODEL_VIBE           = "Animas Vibe"
DEVICE_MODEL_VIBE_PLUS      = "OneTouch Vibe Plus"

# This is a global variable (coming from the bg display mode record) that 
# indicates which setting the display is set to. This also will shows the unit
# used for the ISF. 
_bg_display_mode_setting = None

# Temporary lists with values which need "two-step" processing
_records_needing_bg_recalc = []
g_records_needing_cgm_glucose_recalc = []

# Sum of all the display time changes carried by the CGM Time Adjust record
g_accumulated_time_adjust = 0

# global to indicate current model
device_model = None

# global to store timestamps, shared between Bolus and Bolus2 history
g_shared_bolus_timestamps = []

# global to store DexCom debug data. If present, will be sent at end of analysis
g_unity_dexcom_debug_data = []

# Save settings for mg/dL mmol/L recalulcation when all lines has been evaluated. 
g_saved_settings = {}

# Information used for unpacking records. 
# Dict-key      = Packed record tag
# Dict-value    = [Unpacked tag, Size of each record] 
packed_record_information = {
    TYPE_BASAL_HISTORY          + TYPE_PACK_FLAG : [TYPE_BASAL_HISTORY,             14],
    TYPE_TDD_HISTORY            + TYPE_PACK_FLAG : [TYPE_TDD_HISTORY,               14],
    TYPE_BG_HISTORY             + TYPE_PACK_FLAG : [TYPE_BG_HISTORY,                6],
    TYPE_ALARM_HISTORY          + TYPE_PACK_FLAG : [TYPE_ALARM_HISTORY,             14],
    TYPE_PRIME_HISTORY          + TYPE_PACK_FLAG : [TYPE_PRIME_HISTORY,             14],
    TYPE_BOLUS_HISTORY          + TYPE_PACK_FLAG : [TYPE_BOLUS_HISTORY,             14],
    TYPE_BOLUS2_HISTORY         + TYPE_PACK_FLAG : [TYPE_BOLUS2_HISTORY,            14],
    TYPE_CGM_WARNING            + TYPE_PACK_FLAG : [TYPE_CGM_WARNING,               14],
    TYPE_SUSPEND_RESUME_HISTORY + TYPE_PACK_FLAG : [TYPE_SUSPEND_RESUME_HISTORY,    14], 
    TYPE_BLACK_BOX_HISTORY      + TYPE_PACK_FLAG : [TYPE_BLACK_BOX_HISTORY,         128]
}

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------

def AddSettingsToLocalCache(settings):
    """
    Add settings to local cache. All settings are stored in a local cache and
    only added to the result when EvalAnimasAllLinesEvaluated is called. This
    makes it possible to do last-minute changes to the settings (at the moment
    the SETTING_SICK_BG_OVER_LIMIT value must be changed depending on the 
    current BG_UNIT set in the pump).
    """
    global g_saved_settings
    
    # Merge the dictionaries
    g_saved_settings = dict(g_saved_settings, **settings)

def UnEscape ( line ):
    """
    The animas serial protocol is frame based, where each frame has a known start and
    end octet. So the data is byte stuffed so there will be no occurence of the 
    start and end octet in the payload
    Defined octets are 0xc0 -> BOM, 0xc1 -> EOM, 0x7d -> CTL
    Those will be exchanged by CTL + byte xor 0x20
    """
    i = 0;
    length = len(line)

    while i < length:

        if line[i] == ANIMAS_CTL_CHAR:
            # found escaped char
            line = line[:i] + chr(ord(line[i+1]) ^ 0x20) + line[i+2:]
            length -= 1
        i += 1

    return line

def CalcFletcherChecksum( line ):
    """
    Function that calculates the fletcher checksum for a line, a two byte long string will be returned.
    """
    sum1 = 0
    sum2 = 0

    for b in line:
        sum1 = (sum1 + ord(b)) % 255
        sum2 = (sum2 + sum1)   % 255
        
    check1 = 255 - ((sum1 + sum2) % 255)
    check2 = 255 - ((sum1 + check1) % 255)
    
    return chr(check1) + chr(check2)

def NullPayloadInDIFrame( frame ):
    """
    Function that checks valid frame and returns True if the checksum matches 
    and the I-field is a DI message(payload) that contains only zeros
    """
    
    # First check the checksum, but do not include the checksum itself
    if CalcFletcherChecksum( frame[:-2] ) != frame[-2:]:
        return False

    #Check so this is a DI-message (data sent in response)
    if frame[2:4] != 'DI':
        return False
    
    #check all data bytes in the DI message, so those are 0.
    # Index 6 because: skip Addr field(1byte), Ctrl Field(1byte), Message type(2bytes), offset (2bytes)
    for i in frame[6:-2]:
        if i != '\0':
            return False

    return True
        
def SplitData( inData ):
    """
    Splits incoming data into a list. The data is a mixture of ascii,
    (the header) and binary result records.
    The result records are just fixed length binary chunks
    """

    global g_add_nr_of_items
    
    g_add_nr_of_items = 0

    # split the body on the known end octet
    bodyList = inData.split('\xc1')

    bodyList = map(UnEscape, bodyList)
    
    bodyListUnpacked = []
    
    # Time to search all items and see if we have any packed records. Packed records
    # must be unpacked. 
    for item in bodyList:
    
        # Does this item contain a packed record?
        if len(item) > 0 and ord(item[0]) in packed_record_information:
            
            # We will not add the packed item to the list.
            g_add_nr_of_items -= 1

            # Check CRC - we get one CRC for the entire packed record. 
            crc = CalcFletcherChecksum(item[2:-2])
            crc_ok = True
            # If the CRC fails don't report it here. We want to send a CRC error
            # back to the transmitter so we add a CRC error to the unpacked record.
            if crc != item[-2:]:
                crc_ok = False
            
            # Split the records. The first 8 bytes is the header of the records.
            if (ord(item[0]) & ~TYPE_PACK_FLAG) == TYPE_BLACK_BOX_HISTORY:
                record = item
                g_add_nr_of_items += 1
                bodyListUnpacked.append(record)
            else:
                records = Common.SplitCount(item[8:], packed_record_information[ord(item[0])][1])
        
                # Now we rebuild individual records and put them unpacked into the 
                # list. Note that we do not need to rebuild all parts of the record. 
                for record in records:
                
                    # Add header to record and assemble it like this:
                    # * depacked type
                    # * original header
                    # * the record payload 
                    # * a dummy checksum on the data level (this is currently not used since we only use the CRC on the frame level - added next)
                    record = "%c" % (packed_record_information[ord(item[0])][0]) + item[1:8] + record + "\x00\x00"
                    
                    # Create and append a CRC. 
                    crc = CalcFletcherChecksum(record[2:])
                    if crc_ok:
                        record = "%s%s" % (record, crc)
                    else:
                        if crc == 'AA':
                            record = "%sBB" % (record)
                        else:
                            record = "%sAA" % (record)
        
                    g_add_nr_of_items += 1
                
                    bodyListUnpacked.append(record)

        # An item that isn't packed.
        else:
        
            if len(item) > 0:
            
                # The CGM glucose packed record is already handled by the normal code. 
                # So we have to remove the packed flag from those items.
                if ord(item[0]) > TYPE_PACK_FLAG:
                    bodyListUnpacked.append("%c%s" % (ord(item[0]) & ~TYPE_PACK_FLAG, item[1:]))
                else:           
                    bodyListUnpacked.append(item)

    return bodyListUnpacked

def CalcDateTime( result ):
    """
    The result must be a dictionary containing "timestamp" and "datestamp"
    in Animas format. Will return a datetime object, or None if unsuccessful.
    """

    month = ord(result["datestamp"][0]) >> 4
    year  = ord(result["datestamp"][0]) & 0xf

    if device_model == DEVICE_MODEL_PING:
        year_base = 2007
    elif device_model in (DEVICE_MODEL_VIBE, DEVICE_MODEL_VIBE_PLUS):
        year_base = 2008
    else:
        year_base = 2000

    # Some values has the timestamp 00-00-00 00:00 -> invalid, so we return None
    try:
        retval = datetime(year_base + year, month + 1, ord(result["datestamp"][1]) , ord(result["timestamp"][0]), ord(result["timestamp"][1]) )
    except:
        return None       

    return retval


def InterpretBolusFlags(flag):

    _flags = []

    _type = flag & 0x3
    if _type == 1:
        # Normal, no flag
        pass
    elif _type == 2:
        _flags.append(FLAG_BOLUS_TYPE_AUDIO)
    elif _type == 3:
        _flags.append(FLAG_BOLUS_TYPE_COMBO)

    _status = (flag >> 2) & 0x3
    if _status == 0:
        _flags.append(FLAG_BOLUS_INACTIVE)
    elif _status == 1:
        _flags.append(FLAG_BOLUS_ACTIVE)
    elif _status == 2:
        _flags.append(FLAG_CANCELED)
    elif _status == 3:
        _flags.append(FLAG_BOLUS_COMPLETED)
        
    if (flag & BOLUS1_TYPE_TRIG_MASK) == BOLUS1_TYPE_TRIG_EZBG:
        _flags.append(FLAG_BOLUS_TYPE_EZBG)
    elif (flag & BOLUS1_TYPE_TRIG_EZCARB):
        _flags.append(FLAG_BOLUS_TYPE_EZCARB)

    # Ping specifics
    if device_model == DEVICE_MODEL_PING:
        if flag & BOLUS1_TYPE_REMOTE_INITIATED:
            _flags.append(FLAG_BOLUS_REMOTE_INITIATED)
            
        if FLAG_CANCELED in _flags:
            if flag & BOLUS1_TYPE_REMOTE_CANCELLED:
                _flags.append(FLAG_BOLUS_REMOTE_CANCELLED)

    return _flags
    
    
def GetAlarmCode(code):
    """
    Function to translate between Animas error codes and our internal
    """
    if code <= 127:
        return ALARM_CALL_SERVICE
    elif code == 128:
        return ALARM_REPLACE_BATTERY
    elif code == 144: 
        return ALARM_CARTRIDGE_REPLACE
    elif code == 145:
        return ALARM_OCCLUSION_1
    elif code == 146: 
        return ALARM_OCCLUSION_2
    elif code == 147:
        return ALARM_OCCLUSION_3
    elif code == 148:  
        return ALARM_OCCLUSION_4
    elif code == 149:  
        return ALARM_OCCLUSION_5
    elif code == 150:   
        return ALARM_POWER_AUTO_OFF
    elif code == 160:   
        return ALARM_DELIVERY_BASAL_SUSPENDED_15MIN
    elif code == 161:   
        return ALARM_DELIVERY_BOLUS_SUSPENDED
    elif code == 162:   
        return ALARM_DELIVERY_PRIME_NOT_DELIVERED
    elif code == 163:   
        return ALARM_CARTRIDGE_REMOVED
    elif code == 164:
        return ALARM_CARTRIDGE_EMPTY
    elif code == 165:   
        return ALARM_DELIVERY_STOPPED_EXCEED_TDD
    elif code == 166:   
        return ALARM_DELIVERY_STOPPED_EXCEED_BOLUS
    elif code == 167:   
        return ALARM_RF_TIMEOUT
    elif code == 171:   
        return ALARM_DELIVERY_STOPPED_EXCEED_BASAL
    elif code == 173:   
        return ALARM_CARTRIDGE_NO_COUNT
    elif code == 174:
        return ALARM_DELIVERY_BASAL_SUSPENDED
    elif code == 175:
        return ALARM_DELIVERY_STOPPED_USER
    elif code == 176:   
        return ALARM_DELIVERY_STOPPED_USER_METER
    elif code == 177:   
        return ALARM_BATTERY_LOW
    elif code == 178:   
        return ALARM_CARTRIDGE_LOW
    elif code == 179:   
        return ALARM_LOW_BLOOD_GLUCOSE
    elif code == 180:   
        return ALARM_HIGH_BLOOD_GLUCOSE
    else:
        return ALARM_CALL_SERVICE


def RecalcBGPeriods(periods, mmol):
    """
    The ISF BG values are stored in either 10th of mmol/l or in mg/dl
    So recalc the values depending on the unit
    """
    out_periods = []

    for period in periods:
        (start_time, value, values) = period
        outvalues = []
        if not mmol:
            value = round( float(value*1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1 )
            for (t,v) in values:
                if t == PROGRAM_VALUE_BG_DEVIATION:
                    v = round( float(v*1000) / VAL_FACTOR_CONV_MMOL_TO_MGDL, 1 )
                outvalues.append((t,v))
        else:
            value = value * 100
            for (t,v) in values:
                if t == PROGRAM_VALUE_BG_DEVIATION:
                    v = v * 100
                outvalues.append((t,v))
        out_periods.append((start_time, value, outvalues))
    
    return out_periods
    
def RoundAnimasStyle(value):
    """
    Algorithm according to section 5.1.2 of 'ezManager CGM Calculated Bolus Algorithms 9/4/2010'
    """
    # Round to nearest 0.05 units
    value = round(value/0.05)*0.05
    
    return value

def TruncateAnimasStyle(value):
    """
    Algorithm according to section 5.1.1 of 'ezManager CGM Calculated Bolus Algorithms 9/4/2010'
    """
    # Eliminate any possible floating point error (Note: python specific)
    # (i.e 1.16 could be stored as 1.1599999999999999, which otherwise gets cut to erroneous 1.15)
    # (&  -1.16 could be stored as -1.1599999999999999, which otherwise gets cut to erroneous -1.15)
    if value > 0:
        value += 0.000000001
    else:
        value -= 0.000000001

    # Keep two decimals, just cut the rest without rounding
    value = int(value*100)/100.0

    return value

    
def CalcBolusEzBG(res):
    """
    Algorithm according to section 5.2 of 'ezManager CGM Calculated Bolus Algorithms 9/4/2010'
    Note that this document version contains some typos (fixed in the algorithm below):
    * The last operation should save "rec_bol".  
    * The "rec_bol_bg=0" block is supposed to flow to the check for IOB_EN, not the final "Save" process.
    """
       
    bg_deviation = float(res["bg_actual"] - res["bg_target"])
    bg_delta = float(res["bg_delta"])
    isf = float(res["isf"])
    
    out = {}
        
    if res["config"] & BOLUS2_CONFIGURATION_IOB_ENABLED:
        iob = float(res["iob"])/100.0
        out["iob"] = iob
    else:
        iob = 0.0

    if bg_deviation <= bg_delta:
        rec_bol_bg = 0
    else:
        if isf == 0.0:
            print "WARNING: Pump bug, ISF is 0!"
            return None
        rec_bol_bg = TruncateAnimasStyle(bg_deviation / isf)
    
    rec_bol = rec_bol_bg - iob

    # For the EzBG case, BG is always defined. IOB depends, see above.
    out["rec_bol_total"] = rec_bol
    out["rec_bol_bg"]    = rec_bol_bg

    return out
    
def CalcBolusEzCarbWithoutBG(res):
    """
    Algorithm according to section 5.3 of 'ezManager CGM Calculated Bolus Algorithms 9/4/2010'
    """
    if res["ic_value"] == 0:
        print "WARNING: Pump bug, IC value is 0!"
        return None
    
    rec_bol_carbs = TruncateAnimasStyle(float(res["carbs"])/float(res["ic_value"]))
    
    # In the EzCarb without BG case, only the carbs are used. BG and IOB are undefined
    return {
        "rec_bol_total" : rec_bol_carbs, 
        "rec_bol_carbs" : rec_bol_carbs
    }


def CalcBolusEzCarbWithBG(res):
    """
    Algorithm according to section 5.4 of 'ezManager CGM Calculated Bolus Algorithms 9/4/2010'
    """
    if res["ic_value"] == 0:
        print "WARNING: Pump bug, IC value is 0!"
        return None

    rec_bol_carbs = TruncateAnimasStyle(float(res["carbs"])/float(res["ic_value"]))
    
    target_bg = float(res["bg_target"])
    target_high = float(res["bg_target"] + res["bg_delta"])
    target_low = float(res["bg_target"] - res["bg_delta"])
    actual_bg = float(res["bg_actual"])
    isf = float(res["isf"])
    
    out = {}
    
    if res["config"] & BOLUS2_CONFIGURATION_IOB_ENABLED:
        iob = TruncateAnimasStyle(float(res["iob"])/100.0)
        out["iob"] = iob
    else:
        iob = 0.0
    
    if ((actual_bg >= target_low) and (actual_bg <= target_high)):
        # Within target, BG is defined as 0. 
        rec_bol = rec_bol_carbs
        rec_bol_bg = 0.0
        # IOB is considered not defined, since the sum would look strange in presentation. Delete it if currently defined
        out.pop("iob", None)
    else:
        if isf == 0.0:
            print "WARNING: Pump bug, ISF is 0!"
            return None
        rec_bol_bg = TruncateAnimasStyle((actual_bg - target_bg) / isf)
        if actual_bg < target_low:
            rec_bol = rec_bol_bg + rec_bol_carbs - iob  # Note: In the Animas docs they redefine rec_bol_carbs as "rec_bol_carbs - iob", but that is wrong
            if rec_bol < 0:
                rec_bol = 0
        else:
            if rec_bol_bg <= iob:
                rec_bol = rec_bol_carbs
            else:
                rec_bol = rec_bol_bg - iob + rec_bol_carbs
    
    # In the EzCarb with BG case, BG and carbs are always defined. IOB depends, see above.
    out["rec_bol_total"] = rec_bol
    out["rec_bol_bg"]    = rec_bol_bg
    out["rec_bol_carbs"] = rec_bol_carbs
        
    return out

    
# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalBlackBoxHistoryRecord(line):
    """
    Evaluate black box history record. 
    """

    # No length check - black box history can have different length depending on 
    # device type (and the total number is not divisible by the size of the frame).

    if (ord(line[0]) & ~TYPE_PACK_FLAG) != (TYPE_BLACK_BOX_HISTORY):
        return None

    res = {}
    # Remove our tag
    res[BLACK_BOX_HISTORY_RECORD] = line[1:]

    return res

def EvalSerialRecord( line ):
    """
    Evaluate a Animas Serial record. Extracted elements: 
    serial
    checksum
    """
    if len(line) != 25:
        return None

    if ord(line[0]) != TYPE_PUMP_SERIAL_NUMBER:
        return None
        
    # This is a serial number row!
    res = {}
    
    res["checksum"] = line[-5:-3]
    res["serial"]   = line[8:-5]
    return res


def EvalBolusResultRecord( line ):
    """
    Evaluate a Animas Bolus Result record. Extracted groups : 
    index
    datestamp
    timestamp
    delivered
    required
    duration
    type
    checksum
    """
    
    global g_shared_bolus_timestamps

    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_BOLUS_HISTORY:
        return None

    res = {}
    
    res["index"]     = line[6:8]
    res["datestamp"] = line[8:10]
    res["timestamp"] = line[10:12]
    res["delivered"] = ord(line[12]) | ord(line[13]) << 8 | ord(line[14]) << 16 | ord(line[15]) << 24
    res["required"]  = ord(line[16]) | ord(line[17]) << 8
    res["duration"]  = ord(line[18]) | ord(line[19]) << 8
    res["type"]      = line[20]
    # Byte 21 is used as Synch, on IR1275+, unused on others
    res["synch"]     = line[21]
    res["checksum"]  = line[22:24]
    
    # Save the record, if needed by Bolus2 register decoding. 
    # Should be mapped 1 to 1 in order against the Bolus2 history
    g_shared_bolus_timestamps.append(res)

    return res

def EvalBasalResultRecord( line ):
    """
    Evaluate a Animas Basal Result record. Extracted groups : 
    index
    datestamp
    timestamp
    rate
    temp
    checksum
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_BASAL_HISTORY:
        return None

    res = {}

    res["index"]     = line[6:8]
    res["datestamp"] = line[8:10]
    res["timestamp"] = line[10:12]
    res["rate"]      = ord(line[12]) | ord(line[13]) << 8
    res["temp"]      = line[14]
    # Bytes 15 to 21 are unused
    res["checksum"]  = line[22:24]

    return res

def EvalTddResultRecord( line ):
    """
    Evaluate a Animas TDD Result record. Extracted groups : 
    index
    datestamp
    timestamp
    delivered
    basal_delivered
    flags
    checksum
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_TDD_HISTORY:
        return None

    res = {}

    res["index"]            = line[6:8]
    res["datestamp"]        = line[8:10]
    res["timestamp"]        = line[10:12]
    res["delivered"]        = ord(line[12]) | ord(line[13]) << 8 | ord(line[14]) << 16 | ord(line[15]) << 24
    res["basal_delivered"]  = ord(line[16]) | ord(line[17]) << 8 | ord(line[18]) << 16 | ord(line[19]) << 24
    res["flags"]            = line[20]

    # Byte 21 is unused
    res["checksum"]         = line[22:24]

    return res
        
def EvalBgDisplayMode( line ):
    """
    Evaluate an Animas BG Display Mode record. Extracted groups : 
    
    TBD
    
    """
    if len(line) == 0:  # TBD
        return None
        
    if ord(line[0]) != TYPE_BG_DISPLAY_MODE:
        return None
        
    res = {}
    
    res[ "bg_setting" ] = line[8:10]
    
    return res

def EvalClockDisplayMode( line ):
    """
    Evaluate an Animas Clock Display Mode record. Extracted groups : 
    
    clock_mode
    
    """
    if len(line) != 12:
        return None
        
    if ord(line[0]) != TYPE_CLOCK_MODE:
        return None
        
    res = {}
    
    res[ "clock_mode" ] = line[8:10]
    
    return res

def EvalSoftwareVersion( line ):
    """
    Evaluate an Animas Software version.

    """
    if len(line) != 22:
        return None

    if ord(line[0]) != TYPE_SW_VERSION:
        return None
  
    res = {}
   
    res[ "index" ]            = line[6:8]
    res[ "software_version" ] = line[8:16]
    return res

def EvalAdvancedSettings2( line ):
    """
    Evaluate an Animas Advanced Settings 2 record.

    Note : This function return "raw" Animas data. 
    """
    global device_model

    if len(line) != 74:
        return None

    if ord(line[0]) != TYPE_ADV_SETTINGS_2:
        return None

    # For some reason we get record 37 from 2020 as well. This should
    # be disregarded but we must return a null_result because otherwise
    # number of records will be incorrect. 
    if device_model not in [DEVICE_MODEL_IR1200, DEVICE_MODEL_IR1250]:
        return {"null_result":True}

    res = {}

    res["index"]                                = line[6:8]
    res["carb_bolus_insulin_to_carb_ratio"]     = line[8:14]
    res["insulin_sensitivy_factor"]             = line[14:26]
    res["bg_target"]                            = line[26:38]
    res["bg_target_delta"]                      = line[38:50]
    res["checksum"]                             = line[50:52]

    return res

def ConvertAdvancedSettings2(m):
    """
    Convert a "raw" advanced settings 2 record (from EvalAdvancedSettings2)
    to Diasend format.
    """

    # An advanced settings 2 record has been found but for an incorrect 
    # pump model. Disregard.
    if m.has_key("null_result"):
        return m

    programs = []

    # I:C
    program = {}
    program["meter_checksum"]   = m["checksum"]
    program[PROGRAM_NAME]       = "1"
    program[PROGRAM_TYPE]       = PROGRAM_TYPE_IC_RATIO
    program[PROGRAM_PERIODS]    = []

    for index in range(6):
        program[PROGRAM_PERIODS].append([time(4*index,0), \
                                         struct.unpack("B", m["carb_bolus_insulin_to_carb_ratio"][index])[0], \
                                         []])
    programs.append(program)

    # ISF
    program = {}
    program["meter_checksum"]   = m["checksum"]
    program[PROGRAM_NAME]       = "1"
    program[PROGRAM_TYPE]       = PROGRAM_TYPE_ISF
    program[PROGRAM_PERIODS]    = []

    for index in range(0, 12, 2):
        program[PROGRAM_PERIODS].append([time(2*index,0), \
                                         struct.unpack("<H", m["insulin_sensitivy_factor"][index:index+2])[0], \
                                         []])

    # 1250 always returns mg/dL.
    program[PROGRAM_PERIODS] = RecalcBGPeriods(program[PROGRAM_PERIODS], False)
    programs.append(program)

    # BG Target / Delta
    program = {}
    program["meter_checksum"]   = m["checksum"]
    program[PROGRAM_NAME]       = "1"
    program[PROGRAM_TYPE]       = PROGRAM_TYPE_BG_TARGET
    program[PROGRAM_PERIODS]    = []

    for index in range(12):
        program[PROGRAM_PERIODS].append([time(2*index,0), \
                                         struct.unpack("B", m["bg_target"][index])[0], \
                                         [(PROGRAM_VALUE_BG_DEVIATION, struct.unpack("B", m["bg_target_delta"][index])[0])]])

    # 1250 always returns mg/dL.
    program[PROGRAM_PERIODS] = RecalcBGPeriods(program[PROGRAM_PERIODS], False)
    programs.append(program)

    return programs

def EvalAdvancedSettings3( line ):
    """
    Evaluate an Animas Advanced Settings 3 record. Extracted groups : 
    
    TBD
    """
    if len(line) != 74:
        return None
        
    if ord(line[0]) != TYPE_ADV_SETTINGS_3:
        return None
                
    res = {}

    res["index"]                = line[6:8]
    res["sa3_index_code"]       = line[8]
    res["segments"]             = line[9]
    res["start_times"]          = Common.SplitCount(line[10:22], 1)
    res["segment_value_1"]      = Common.SplitCount(line[22:46], 2)
    res["segment_value_2"]      = Common.SplitCount(line[46:58], 1)

    res["checksum"]             = line[58:60]
    # there are a lot of unused in the end, and then a final checksum

    return res

def EvalAlarmResultRecord( line ):
    """
    Evaluate a Animas Alarm Result record. Extracted groups : 
    index
    datestamp
    timestamp
    code1
    code2
    checksum
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_ALARM_HISTORY:
        return None

    res = {}

    res["index"]        = line[6:8]
    res["datestamp"]    = line[8:10]
    res["timestamp"]    = line[10:12]
    res["code1"]        = ord(line[12])
    res["code2"]        = line[13:22]
    res["checksum"]     = line[22:24]

    return res

def EvalPrimeResultRecord( line ):
    """
    Evaluate a Animas Alarm Result record. Extracted groups : 
    index
    datestamp
    timestamp
    amount
    flags
    checksum
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_PRIME_HISTORY:
        return None

    res = {}

    res["index"]        = line[6:8]
    res["datestamp"]    = line[8:10]
    res["timestamp"]    = line[10:12]
    res["amount"]       = ord(line[12]) | ord(line[13]) << 8
    res["flags"]        = line[14]
    res["checksum"]     = line[22:24]

    return res

def EvalSuspendResumeRecord( line ):
    """
    Evaluate an Animas suspend/resume history record. Extracted groups : 
    index
    suspend datestamp
    suspend timestamp
    resume datestamp
    resume timestamp
    resume trigger
    suspend trigger
    checksum
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_SUSPEND_RESUME_HISTORY:
        return None

    res = {}

    res["index"]              = line[6:8]
    res["suspend_datestamp"]  = line[8:10]
    res["suspend_timestamp"]  = line[10:12]
    res["resume_datestamp"]   = line[12:14]
    res["resume_timestamp"]   = line[14:16]
    res["resume_trigger"]     = ((ord(line[16]) & 0x10)>>4)  #bit 4 
    res["suspend_trigger"]    = (ord(line[16]) & 0x01)      #bit 0
    res["checksum"]           = line[22:24]

    return res


def EvalBGResultRecord( line ):
    """
    Evaluate a Animas Bolus Result record. Extracted groups : 
    index
    datestamp
    timestamp
    bg_data
    checksum
    """

    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_BG_HISTORY:
        return None

    res = {}

    res["index"]     = line[6:8]
    res["datestamp"] = line[8:10]
    res["timestamp"] = line[10:12]
    # BG data, actually in ultrasmart format
    res["bg_data"]   = line[12:18]
    # Three unused bytes
    res["checksum"]  = line[22:24]

    return res

def EvalCGMBGCalibrationRecord( line ):
    """
    Evaluate CGM BG Calibration record. Extracted groups : 
    datestamp
    timestamp
    bg_data
    """
        
    if len(line) != 18:
        return None

    if ord(line[0]) != TYPE_BG_HISTORY:
        return None

    res = {}
    
    # Note this data is redundant as the same information is part of the glucose data
    res["index"]        = line[6:8]
    res["datestamp"]    = line[8:10]
    res["timestamp"]    = line[10:12]
    res["bg_data"]      = ord(line[12]) | ord(line[13]) << 8

    return res

def EvalRemoteNameRecord(line):
    """
    Evaluates the record from the Animas Ping which contains the pairing name
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_REMOTE_NAME:
        return None

    res = {}
    res["index"]     = line[6:8]
    res["remote_name"] = Common.RemoveNullTermination(line[8:-4])
    res["checksum"]  = line[22:24]

    return res

def EvalBolus2ResultRecord( line ):
    """
    Evaluate a Animas Bolus2 Result record. 

    """
    
    global g_shared_bolus_timestamps

    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_BOLUS2_HISTORY:
        return None

    res = {}
    
    res["index"]     = line[6:8]
    res["synch"]     = line[8]
    res["ic_value"]  = ord(line[9])
    res["carbs"]     = ord(line[10]) | ord(line[11]) << 8
    res["isf"]       = ord(line[12]) | ord(line[13]) << 8 
    res["bg_actual"] = ord(line[14]) | ord(line[15]) << 8 
    res["bg_target"] = ord(line[16]) | ord(line[17]) << 8 
    res["bg_delta"]  = ord(line[18])
    res["config"]    = ord(line[19])
    res["iob"]       = ord(line[20]) | ord(line[21]) << 8 
    res["checksum"]  = line[22:24]
    
    # Get the timestamp from the saved values
    bolus1reg = g_shared_bolus_timestamps.pop(0)
    
    # Verify the order
    if bolus1reg["synch"] != res["synch"]:
        print "WARNING: Sync error! (%s is not %s)" % (repr(bolus1reg["synch"]), repr(res["synch"]))
        return None
    
    # The synch matches, this is the correct timestamp
    res["datestamp"] = bolus1reg["datestamp"]
    res["timestamp"] = bolus1reg["timestamp"]
    res["bolus1_type"] = bolus1reg["type"]
    res["delivered"] = bolus1reg["delivered"]
    res["duration"] = bolus1reg["duration"]
    res["required"] = bolus1reg["required"]

    return res

def EvalCGMSettings( line ):
    """
    Evaluate an Animas CGM settings record. 
    Extracted SETTINGS_LIST with SETTING_CGM elements as 
    implemented in the code below.
    """    

    if device_model == DEVICE_MODEL_VIBE_PLUS:
        if len(line) != 42:
            return None
    elif len(line) != 40:
        return None

    if ord(line[0]) != TYPE_CGM_SETTINGS:
        return None
        
    resList = []
    res = {}

    res[SETTING_CGM_SOUND_TYPE_USER_HIGH_GLUCOSE_WARNING] = ("%d") % (ord(line[8]))
    res[SETTING_CGM_SOUND_TYPE_USER_LOW_GLUCOSE_WARNING] = ("%d") % (ord(line[9]))
    res[SETTING_CGM_SOUND_TYPE_RISE_RATE_WARNING] = ("%d") % (ord(line[10]))
    res[SETTING_CGM_SOUND_TYPE_FALL_RATE_WARNING] = ("%d") % (ord(line[11]))
    res[SETTING_CGM_SOUND_TYPE_TRM_OUT_OF_RANGE_WARNING] = ("%d") % (ord(line[12]))
    res[SETTING_CGM_SOUND_TYPE_OTHER_WARNING] = ("%d") % (ord(line[13]))
                                                                                                       
    # High Glucose Warning limit; 120 - 400 mg/dL in steps of 20 mg/dL 
    res[SETTING_CGM_HIGH_GLUCOSE_WARNING_LIMIT] = ("%d") % ((ord(line[14]) | (ord(line[15]) << 8)))
    
    # Low Glucose Warning limit; 60 - 100 mg/dL in steps of 10 mg/dL       
    res[SETTING_CGM_LOW_GLUCOSE_WARNING_LIMIT] = ("%d") % ((ord(line[16]) | (ord(line[17]) << 8)))
    
    res[SETTING_CGM_RISE_RATE_LIMIT] = ("%d") % (ord(line[18])) #2 mg/dL or 3 mg/dL
    res[SETTING_CGM_FALL_RATE_LIMIT] = ("%d") % (ord(line[19])) #2 mg/dL or 3 mg/dL

    # User High Glucose Snooze time 0 - 300 minutes in steps of 30 minutes
    res[SETTING_CGM_SNOOZE_TIME_USER_HIGH_GLUCOSE] = ("%d") % ((ord(line[20]) | (ord(line[21]) << 8)))

    # The User High Glucose Snooze time 0 - 300 minutes in steps of 30 minutes
    res[SETTING_CGM_SNOOZE_TIME_USER_LOW_GLUCOSE] = ("%d") % ((ord(line[22]) | (ord(line[23]) << 8)))
    
    # Transmitter Out of Range Snooze. 0 - 300 minutes in steps of 30 minutes
    res[SETTING_CGM_SNOOZE_TIME_TRM_OUT_OF_RANGE] = ("%d") % ((ord(line[24]) | (ord(line[25]) << 8)))    
    
    # Enabled/disabled warnings; 0=disabled, 1=enabled
    res[SETTING_CGM_USER_HIGH_GLUCOSE_WARNING] = ("%d") % (ord(line[26])) 

    # Enabled/disabled warnings; 0=disabled, 1=enabled
    res[SETTING_CGM_USER_LOW_GLUCOSE_WARNING] = ("%d") % (ord(line[27]))

    # Enabled/disabled warnings; 0=disabled, 1=enabled
    res[SETTING_CGM_RISE_RATE_WARNING] = ("%d") % (ord(line[28]))

    # Enabled/disabled warnings; 0=disabled, 1=enabled
    res[SETTING_CGM_FALL_RATE_WARNING] = ("%d") % (ord(line[29]))

    # Enabled/disabled warnings; 0=disabled, 1=enabled
    res[SETTING_CGM_TRM_OUT_OF_RANGE_WARNING] = ("%d") % (ord(line[30]))
    
    # CGM transmitter ID is five ASCII characters for model < H1a, 6 chars for >= H1a.
    # We also have an additional BT setting.
    if device_model == DEVICE_MODEL_VIBE_PLUS:
        res[SETTING_CGM_TRANSMITTER_ID]           = line[31:37]
        res[SETTING_CGM_BLUETOOTH_ENABLED_STATUS] = line[37]
    else:
        res[SETTING_CGM_TRANSMITTER_ID] = line[31:36]

    AddSettingsToLocalCache(res)

    return {"null_result":True}

def EvalCGMWarning( line ):
    """
    Evaluate CGM Warning record. Extracted groups :
    index
    datestamp
    timestamp
    code1 - EAW code + ALARM_CGM_EAW_BASE
    code2 - Engineering debug, 8 bytes
    checksum
    """
    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_CGM_WARNING:
        return None
        
    res = {}

    res["index"]     = line[6:8]  
    res["datestamp"] = line[8:10]
    res["timestamp"] = line[10:12]
    res["code1"] = ord(line[12]) + ALARM_CGM_EAW_BASE
    res["code2"] = line[13:22]
    res["checksum"]     = line[22:24]

    return res


def EvalCGMSessions( line ):
    """
    Evaluate a CGM Sessions record.
    """

    if len(line) < 1:
        return None

    if ord(line[0]) != TYPE_CGM_SESSIONS:
        return None
        
    # This record is currently not needed, so we don't decode all data
    res = {}
    
    res["index"] = line[6:8]

    return res


def EvalCGMGlucose( line ):      
    """
    EvalCGMGlucose will put non-empty CGM glucose records in list
    for later processing (allLinesEvaluated).
    """
    global g_records_needing_cgm_glucose_recalc
    
    if len(line) < 16:
        return None

    if ord(line[0]) != TYPE_CGM_GLUCOSE:
        return None

    # Calculate the number of records in this line by stripping out overhead
    recordsInLine = (len(line) - 10)/6

    # List of records, allows us to generate several results from one record
    for i in range(0, recordsInLine):
        record = line[8+(i*6):14+(i*6)]
        #ignore zeroed records
        if record != '\x00\x00\x00\x00\x00\x00':
            g_records_needing_cgm_glucose_recalc.append(record)

    # This record is currently not needed as is, so we don't decode all data
    res = {}
    res["index"]    = line[6:8]

    return res

def EvalCGMDebug( line ):
    """
    Handle the Unity CGM Debug message
    This one is special, since it is should not be stored in the database. Instead, it 
    will be stored globally and sent at the end of the result handling
    """

    global g_unity_dexcom_debug_data
    if len(line) < 1:
        return None

    if ord(line[0]) != TYPE_CGM_DEBUG:
        return None

    res = {}
    res["index"]    = line[6:8]
    res["data"]     = line[8:-2]
    
    # Add this data to the global buffer
    g_unity_dexcom_debug_data.append(res["data"])
    
    return res


def EvalCGMTimeAdjust( line ):
    """
    Evaluate CGM Time Adjust record. 
    Will update g_accumulated_time_adjust with the sum of all the display 
    time changes at time of transfer from the meter
    """
    global g_accumulated_time_adjust

    if len(line) != 26:
        return None

    if ord(line[0]) != TYPE_CGM_TIME_ADJUST:
        return None

    res = {}

    # The time is stored as a signed 32 bit integer
    g_accumulated_time_adjust = struct.unpack("<i", line[12:16])[0]

    res["index"]    = line[6:8]

    return res

    
def EvalBasalProgramResultRecord( line ):
    """
    Evaluate a Animas Basal Program Result record. Extracted groups : 
    index
    valid
    number_segments
    start_times
    rates
    segments
    checksum
    """
    if len(line) != 74:
        return None
    
    if ord(line[0]) != TYPE_BASAL_PROGRAMS:
        return None

    res = {}

    res["index"]        = line[6:8]
    res["valid"]        = line[8]
    res["segments"]     = line[9]
    res["start_times"]  = Common.SplitCount(line[10:22], 1)
    res["rates"]        = Common.SplitCount(line[22:46], 2)

    res["checksum"]     = line[-28:-26]
    # there are a lot of unused in the end, and then a final checksum
    
    return res

def EvalAdvancedSettingsResultRecord( line ):
    """
    Evaluate a Animas Advanced Settings Result record. Extracted groups : 
    
    These records differs between different models of the Animas pumps...
    why be backwards compatible? uh..
    
    index
    checksum
    LOADS of properties
    """
    global device_model
    
    if len(line) != 42:
        return None

    if ord(line[0]) != TYPE_ADV_SETTINGS:
        return None

    res = {}

    res[SETTING_AUDIO_BOLUS_ENABLE] = ("%d") % (ord(line[8]))
    res[SETTING_AUDIO_BOLUS_INCREMENT] = ("%d") % (ord(line[9]))
    res[SETTING_ADV_BOLUS_OPTIONS_ENABLE] = ("%d") % (ord(line[10]))
    res[SETTING_BOLUS_REMINDER_OPTIONS_ENABLE] = ("%d") % (ord(line[11]))
    res[SETTING_BOLUS_DELIVERY_SPEED] = ("%d") % (ord(line[12]))
    res[SETTING_BASAL_PROGRAMS_IN_UI] = ("%d") % (ord(line[13]))
    # Next are multiplied by 10 since we store them x 1000 in the db
    res[SETTING_MAX_BASAL_RATE] = ("%d") %  (((ord(line[14]) | (ord(line[15]) << 8))) * 10)
    res[SETTING_MAX_BOLUS] = ("%d") %  (((ord(line[16]) | (ord(line[17]) << 8))) * 10)
    res[SETTING_MAX_TDD] = ("%d") %  (((ord(line[18]) | (ord(line[19]) << 8))) * 10)
    # Of course Animas chosed to put a new settings in the middle rather than adding it last...
    pos = 20
    if device_model in (DEVICE_MODEL_PING, DEVICE_MODEL_VIBE, DEVICE_MODEL_VIBE_PLUS):
        res[SETTING_MAX_2HOURS] = ("%d") %  (((ord(line[20]) | (ord(line[21]) << 8))) * 10)
        pos += 2

    res[SETTING_MAX_SETTINGS_LOCK] = ("%d") % (ord(line[pos]))
    pos+=1
    res[SETTING_LANG_SELECTION_IDX] = ("%d") % (ord(line[pos]))
    pos+=1
    res[SETTING_DISPLAY_TIMEOUT] = ("%d") % (ord(line[pos]))
    pos+=1
    res[SETTING_AUTO_OFF_ENABLE] = ("%d") % (ord(line[pos]))
    pos+=1
    res[SETTING_AUTO_OFF_TIMEOUT] = ("%d") % (ord(line[pos]))
    pos+=1
    res[SETTING_CARTRIDGE_WARNING_LEVEL] = ("%d") % (ord(line[pos]))
    pos+=1
    res[SETTING_OCCLUSION_SENSITIVITY_LEVEL] = ("%d") % (ord(line[pos])) 
    pos+=1
    res[SETTING_INSULIN_ON_BOARD_ENABLE] = ("%d") % (ord(line[pos])) 
    pos+=1
    res[SETTING_INSULIN_ON_BOARD_DECAY_DURATION] = ("%d") % (ord(line[pos]))
    pos+=1
    if device_model == DEVICE_MODEL_IR1200:
        # Only the IR1200 uses fixed I:C Ratio fields. 
        # TODO: Convert this to I:C-fields like used in the other Animas pumps?
        res[SETTING_IC_RATIO_3]   = ("%d") % (ord(line[pos]))
        pos+=1
        res[SETTING_IC_RATIO_0]   = ("%d") % (ord(line[pos]))
        pos+=1
        res[SETTING_IC_RATIO_1]   = ("%d") % (ord(line[pos]))
        pos+=1
        res[SETTING_IC_RATIO_2]   = ("%d") % (ord(line[pos]))
        pos+=1
    elif device_model in (DEVICE_MODEL_IR1250, DEVICE_MODEL_2020, DEVICE_MODEL_PING, 
            DEVICE_MODEL_VIBE, DEVICE_MODEL_VIBE_PLUS):
        # One padding byte follows, to keep internal word alignment
        pos+=1
        res[SETTING_SICK_BG_OVER_LIMIT] = ((ord(line[pos]) | (ord(line[pos + 1]) << 8)))
        pos+=2
        res[SETTING_SICK_KETOONE_CHECK_TIME] = ("%d") % (ord(line[pos]))
        pos+=1
        res[SETTING_SICK_CHECK_BG_TIME] = ("%d") % (ord(line[pos]))
        pos+=1

    AddSettingsToLocalCache(res)
    
    return {"null_result":True}

def EvalActiveBasalProgramRecord( line ):
    """
    Evaluate a Animas IR active basal setting record. Extract groups : 
    index
    active_prograam
    """
    if len(line) != 12:
        return None

    if ord(line[0]) != TYPE_ACTIVE_BASAL_PROGRAM:
        return None

    res = {}

    res["index"]          = line[6:8]
    res["active_program"] = line[8:10]

    return res
    

def EvalNrResultsRecord( line ):
    """
    Evaluate a Animas IR number results record. Extract groups : 
    number_results
    checksum
    """
    if len(line) != 6:
        return None

    if ord(line[0]) != TYPE_NR_RESULTS:
        return None

    m = {}
    
    m["number_results"] = (ord(line[2]) << 8) | (ord(line[3])) 
    m["checksum"]       = line[4:6]

    return m
    

def EvalHistoryResultRecord( line ):
    """
    Is this a history record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    _flags = []

    res = None
    # List of records, allows us to generate several results from one record
    record_items = []

    m = EvalBasalResultRecord(line)    
    if m:
        res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_BASAL, ELEM_VAL: m["rate"]}

        if ord(m["temp"]) == 1:
            _flags.append(FLAG_TEMP_MODIFIED)
    if not m:
        m = EvalBolusResultRecord(line)

        if m:
            if device_model in (DEVICE_MODEL_2020, DEVICE_MODEL_PING, DEVICE_MODEL_VIBE, DEVICE_MODEL_VIBE_PLUS):
                # 2020/Ping/Unity boluses are handled separately, when the Bolus2 register appears.
                res = {"null_result":True}
            else:
                # Delivered 0.0 - 35.0000, value sent is multiplied by 10.000
                res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS, ELEM_VAL: m["delivered"]}
                _flags = InterpretBolusFlags(ord(m["type"]))
                
                # Handle the duration part as well, if this is a Combo. Resolution is 6 minutes
                if FLAG_BOLUS_TYPE_COMBO in _flags:
                    res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : m["duration"]*6, ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]
                    
                # If the bolus was cancelled we store the programmed("required") part as well (it is stored with as 0.001U instead of delivered 0.0001U)
                if FLAG_CANCELED in _flags:
                    res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : m["required"]*10, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_PROGRAMMED}]
    

    if not m:
        m = EvalTddResultRecord(line)
        if m:
            # Delivered 0.0 - 600.0000, value sent is multiplied by 10.000
            res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_TDD, ELEM_VAL: m["delivered"]}

            # A second result is the insulin_basal_tdd
            # Basal Delivered 0.0 - 600.0000, value sent is multiplied by 10.000
            res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : m["basal_delivered"], ELEM_VAL_TYPE : VALUE_TYPE_INS_BASAL_TDD}]

            _flag = ord(m["flags"])
            if _flag & 0x1:
                _flags.append(FLAG_TEMP_MODIFIED)
            if _flag & 0x2:
                _flags.append(FLAG_SUSPENDED)
    if not m:
        m = EvalAlarmResultRecord(line)
        if m:
            res = {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: GetAlarmCode(m["code1"])}
    if not m:
        m = EvalPrimeResultRecord(line)
        if m:
            res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_PRIME, ELEM_VAL: m["amount"]}

            _flag = ord(m["flags"])
            if _flag == 1:
                _flags.append(FLAG_NOT_PRIMED)
            elif _flag == 2:
                _flags.append(FLAG_PRIMED)
            elif _flag == 3:
                _flags.append(FLAG_CANNULA_BOLUS)
    if not m:
        m = EvalBGResultRecord(line)
        if m:
            # Fake complete ultrasmart record
            usmart_data = m["bg_data"] + "\x00\x00\x00"
            res = MeterLifescanOnetouchUltraSmart.EvalResultRecord(usmart_data)
            res["meter_flaglist"].append(FLAG_METER_REMOTE)


    # CGM handling

    if not m:
        m = EvalCGMBGCalibrationRecord(line) 
        if m:
            # We don't need this record
            res = {"null_result":True}
    if not m:
        m = EvalCGMGlucose( line ) 
        if m:
            # The actual line is not used as a result - handled separately
            res = {"null_result":True}
    if not m:
        m = EvalCGMWarning( line ) 
        if m:
            res = {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: m["code1"]}
    if not m:
        m = EvalCGMSessions( line ) 
        if m:
            # We don't need this record
            res = {"null_result":True}
    if not m:
        m = EvalCGMTimeAdjust( line ) 
        if m:
            # The actual line is not used as a result - handled separately
            res = {"null_result":True}
    if not m:
        m = EvalCGMDebug( line ) 
        if m:
            # The actual line is not used as a result - handled separately
            res = {"null_result":True}

    # == All results above are single-results, so if there exists a result, store it in the list ==
    if res:
        record_item = {"res":res, "flags":_flags}
        record_items.append(record_item)

    # Check Bolus 2 register, which might need crossreferences to Bolus 1
    if not m:
        m = EvalBolus2ResultRecord(line)
        if m:
            # Carbs and manual glucose are handled as a special case, since this single record can generate several records.
            bolus1_type = ord(m["bolus1_type"])

            # We are only interested in the carbs value, if a valid one
            carbflags = []
            if (m["carbs"] == 0) or (bolus1_type & BOLUS1_TYPE_TRIG_EZCARB == 0):
                res = {"null_result":True}
            else:
                carbflags.append(FLAG_BOLUS_TYPE_EZCARB)
                res = {ELEM_VAL_TYPE: VALUE_TYPE_CARBS, ELEM_VAL: m["carbs"]}
            # Add this carb record to the list
            record_item = {"res":res, "flags":carbflags}
            record_items.append(record_item)
               
            # Extract manual BG values from 2020, Ping and Unity
            # Due to a bug in the 2020, some ezCarb BGs will not be shown (where correction flag is incorrectly not set) 
            if (device_model in (DEVICE_MODEL_2020, DEVICE_MODEL_PING, DEVICE_MODEL_VIBE, DEVICE_MODEL_VIBE_PLUS)) and \
                (m["bg_actual"] > 0) and \
                (((bolus1_type & BOLUS1_TYPE_TRIG_MASK) == BOLUS1_TYPE_TRIG_EZBG) or
                (bolus1_type & BOLUS1_TYPE_TRIG_EZCARB) and (m["config"] & BOLUS2_CONFIGURATION_EZCARB_BG_CORR)):
                                
                bg = m["bg_actual"]

                if (m["config"] & BOLUS2_CONFIGURATION_BGMODE_MMOL):
                    # Unit is set to mg/dl in this file so we need to convert it to mg
                    bg = float(bg/10.0 * VAL_FACTOR_CONV_MMOL_TO_MGDL)

                res = {ELEM_VAL_TYPE: VALUE_TYPE_GLUCOSE, ELEM_VAL: bg}
                _flags.append(FLAG_MANUAL)
                
                # If the Ping remote initiated this bolus, flag this BG as a meter remote value
                if (device_model == DEVICE_MODEL_PING) and (FLAG_BOLUS_REMOTE_INITIATED in InterpretBolusFlags(bolus1_type)):
                    _flags.append(FLAG_METER_REMOTE)

                # Add this record to the list
                record_item = {"res":res, "flags":_flags}
                record_items.append(record_item)
                
            # Now check if we need to store the bolus. This only goes for 2020, Ping and Unity1, since other devices are
            # handled by the regular EvalBolusResultRecord() above
            if device_model in (DEVICE_MODEL_2020, DEVICE_MODEL_PING, DEVICE_MODEL_VIBE, DEVICE_MODEL_VIBE_PLUS):
                
                # Store the delivered bolus amount
                res = {ELEM_VAL_TYPE: VALUE_TYPE_INS_BOLUS, ELEM_VAL: m["delivered"]}

                # Setup the secondary values
                secondary = []
                res[ELEM_VALUE_LIST] = secondary
                _flags = InterpretBolusFlags(bolus1_type)
                
                # If the bolus was cancelled we store the programmed("required") part as well (it is stored with as 0.001U instead of delivered 0.0001U)
                if FLAG_CANCELED in _flags:
                    secondary.append({ELEM_VAL : m["required"]*10, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_PROGRAMMED})
                
                # Calculate the suggested bolus, if not a 2020 combo bolus (because of pump bug)
                if not ((device_model == DEVICE_MODEL_2020) and (FLAG_BOLUS_TYPE_COMBO in _flags)):
                    calc_bolus = None
                    if (bolus1_type & BOLUS1_TYPE_TRIG_EZCARB):
                        if (m["bg_actual"] > 0) and (m["config"] & BOLUS2_CONFIGURATION_EZCARB_BG_CORR):
                            calc_bolus = CalcBolusEzCarbWithBG(m)
                        else:
                            calc_bolus = CalcBolusEzCarbWithoutBG(m)
                    elif (bolus1_type & BOLUS1_TYPE_TRIG_MASK) == BOLUS1_TYPE_TRIG_EZBG:
                        calc_bolus = CalcBolusEzBG(m)
                    
                    # If the calculated bolus is valid, store it
                    if calc_bolus != None:
                        bolus_suggested = RoundAnimasStyle(calc_bolus["rec_bol_total"])*VAL_FACTOR_BOLUS
                        # Negative boluses means "no bolus"
                        if bolus_suggested < 0.0:
                            bolus_suggested = 0.0
                        secondary.append({ELEM_VAL : bolus_suggested, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED})
                        
                        # Store the separate parts as well
                        if "rec_bol_bg" in calc_bolus:
                            secondary.append({
                                ELEM_VAL : calc_bolus["rec_bol_bg"]*VAL_FACTOR_BOLUS, 
                                ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR})

                        if "rec_bol_carbs" in calc_bolus:
                            secondary.append({
                                ELEM_VAL : calc_bolus["rec_bol_carbs"]*VAL_FACTOR_BOLUS, 
                                ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL})

                        if "iob" in calc_bolus:
                            secondary.append({
                                ELEM_VAL : calc_bolus["iob"]*VAL_FACTOR_BOLUS, 
                                ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_IOB})
                                
                
                # Handle the duration part as well, if this is a Combo. Resolution is 6 minutes
                if FLAG_BOLUS_TYPE_COMBO in _flags:
                    secondary.append({ELEM_VAL : m["duration"]*6, ELEM_VAL_TYPE : VALUE_TYPE_DURATION})

                # Add this record to the list
                record_item = {"res":res, "flags":_flags}
                record_items.append(record_item)

                
    if not m:
        m = EvalSuspendResumeRecord(line)
        if m:
            suspend_record_item = {ELEM_VAL_TYPE : VALUE_TYPE_EVENT}
            resume_record_item = {ELEM_VAL_TYPE : VALUE_TYPE_EVENT}
            suspend_record_item[ELEM_VAL] = DIASEND_EVENT_DELIVERY_SUSPENDED
            resume_record_item[ELEM_VAL] = DIASEND_EVENT_DELIVERY_RESUMED
            # Extract the two date/time-stamps
            suspenddatetime = {"datestamp":m["suspend_datestamp"], "timestamp":m["suspend_timestamp"]}
            resumedatetime  = {"datestamp":m["resume_datestamp"], "timestamp":m["resume_timestamp"]}
            
            suspend_record_item[ELEM_TIMESTAMP] = CalcDateTime(suspenddatetime)
            resume_record_item[ELEM_TIMESTAMP] = CalcDateTime(resumedatetime)

            sr_flags = []

            if m["resume_trigger"] == 1:
                sr_flags.append(FLAG_RESUME_TRIGGER_OTHER)
            if m["suspend_trigger"] == 1:
                sr_flags.append(FLAG_SUSPEND_TRIGGER_OTHER)

            if suspend_record_item[ELEM_TIMESTAMP] == None:
                record_items.append({"res":{"null_result":True}})
            else:
                record_items.append({"res":suspend_record_item, "flags":sr_flags})

            if resume_record_item[ELEM_TIMESTAMP] == None:
                record_items.append({"res":{"null_result":True}})
            else:
                record_items.append({"res":resume_record_item, "flags":sr_flags})
            
    results = []
    for record_item in record_items:
        
        # Special case, if the pump isn't entry the whole payload of the frame is sent zeroised.
        # Even the checksum, which won't match in that case, as it should be 0xffff
        # So if the checksum is \x0000, check if the frame was empty
        if m.has_key("checksum") and m["checksum"] == "\0\0":
            # The frame data starts at index 2 (after our identifier and the SOF (0xc0))
            if NullPayloadInDIFrame(line[2:]):
                return {"null_result":True}


        # Get the single record contents
        res = record_item["res"]
        
        # If this is a correct record, it should be handled
        if not res.has_key("null_result") :
            _flags = record_item["flags"]
        
            res[ELEM_CHECKSUM]  = m["checksum"]

            if not res.has_key(ELEM_FLAG_LIST):
                    res[ELEM_FLAG_LIST] = list(_flags)

            if not res.has_key(ELEM_TIMESTAMP):
                # Add the generic data values
                res[ELEM_TIMESTAMP] = CalcDateTime(m)

        results.append(res)
    return results

def EvalConfigResultRecord( line ):
    """
    Is this a config record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    global _bg_display_mode_setting
    global _records_needing_bg_recalc
    
    res = {}
    
    # Check if this some basal program
    m = EvalBasalProgramResultRecord(line)
    if m:
        # This was a basal program
        if ord(m["valid"]) == 1:
            # And it was valid, the program number is the index + 1
            periods = []
            for i in range(ord(m["segments"])):
                start = ord(m["start_times"][i])
                # 0x30 is defined as blank segment
                if start != 0x30:
                    # start is in 0,5 hours since midnight
                    minutes = 0;
                    if start % 2 == 1:
                        minutes = 30;
                    hours = start /2;
                    start_time = time(hours, minutes)
                    rate = ord(m["rates"][i][0]) | ord(m["rates"][i][1]) << 8
                    periods.append((start_time, rate))

            res[PROGRAM_TYPE] = PROGRAM_TYPE_BASAL
            res[PROGRAM_NAME] = str(ord(m["index"][0]) + 1)
            res[PROGRAM_PERIODS] = periods
            res[ "meter_checksum" ]  = m["checksum"]
    if not m:
        m = EvalAdvancedSettingsResultRecord(line)
        if m:
            res = {"null_result":True}
    
    if not m:
        m = EvalActiveBasalProgramRecord(line)
        if m:
            # This record is a special record that does not use fletcher but second
            # half of the program ID contains an inverted byte
            if (~ord(m["active_program"][1]) & 0xff) == ord(m["active_program"][0]):
                _program = ord(m["active_program"][0])                
                AddSettingsToLocalCache({SETTING_ACTIVE_BASAL_PROGRAM_ID : _program})
                res = {"null_result":True}
    if not m:
        # Check if this advanced settings 3
        m = EvalAdvancedSettings3(line)
        if m:
            # This was advanced settings 3
            periods = []
            for i in range(ord(m["segments"])):
                start = ord(m["start_times"][i])
                # 0x30 is defined as blank segment
                if start != 0x30:
                    # start is in 0,5 hours since midnight
                    minutes = 0;
                    if start % 2 == 1:
                        minutes = 30;
                    hours = start /2;
                    start_time = time(hours, minutes)
                    value1 = struct.unpack("<H", m["segment_value_1"][i])[0]
                    values = []
                    if ord(m["sa3_index_code"]) == 3:
                        value2 = ord(m["segment_value_2"][i])
                        values.append((PROGRAM_VALUE_BG_DEVIATION, value2))

                    # NOTE: As per ticket #152, use 0.001 mmol/l resolution
                    periods.append((start_time, value1, values))
            
            # sa3_index_code 1=I:C, 2=ISF, 3=BG target/delta

            res[ "meter_checksum" ]  = m["checksum"]    
            res[PROGRAM_NAME] = "1"
            res[PROGRAM_PERIODS] = periods

            if ord(m["sa3_index_code"]) == 1:
                res[PROGRAM_TYPE] = PROGRAM_TYPE_IC_RATIO
            else:
                if ord(m["sa3_index_code"]) == 2:
                    res[PROGRAM_TYPE] = PROGRAM_TYPE_ISF
                elif ord(m["sa3_index_code"]) == 3:
                    # TODO: support the deviation +- segment_value_2
                    res[PROGRAM_TYPE] = PROGRAM_TYPE_BG_TARGET

                # Check if we can recalculate mg/dL -> mmol/L
                if _bg_display_mode_setting != None:
                    # We have the display mode BG setting, we can recalc the values
                    res[PROGRAM_PERIODS] = RecalcBGPeriods(periods, _bg_display_mode_setting != BG_DISPLAY_MODE_MG)
                else:
                    # We need to recalc those BG values later, store and return a null value so far
                    _records_needing_bg_recalc.append(res)
                    res = {"null_result":True}

    if not m:
        # Check bd display 
        m = EvalBgDisplayMode( line )
        if m:
            bg_setting = ord(m["bg_setting"][0]) | ord(m["bg_setting"][1]) << 8

            # setup global function
            if bg_setting == BG_DISPLAY_MODE_MG:
                _bg_display_mode_setting = BG_DISPLAY_MODE_MG
                setting = {SETTING_BG_UNIT : "mg/dl"}
            else:
                _bg_display_mode_setting = BG_DISPLAY_MODE_MMOL
                setting = {SETTING_BG_UNIT : "mmol/L"}
            
            AddSettingsToLocalCache(setting)
            
            res = {"null_result":True}

            if len(_records_needing_bg_recalc) > 0:
                reslist = []
                reslist.append(res)
                for record in _records_needing_bg_recalc:
                    # Only care for records that has periods...
                    if record.has_key(PROGRAM_PERIODS):
                        record[PROGRAM_PERIODS] = RecalcBGPeriods(record[PROGRAM_PERIODS], _bg_display_mode_setting != BG_DISPLAY_MODE_MG)
                        reslist.append(record)

                _records_needing_bg_recalc = []
                
                res = reslist
    if not m:
        m = EvalClockDisplayMode( line )
        if m:
            setting = {SETTING_TIME_FORMAT : ord(m["clock_mode"][0])} #0 = 12hr, 1=24 hr
            AddSettingsToLocalCache(setting)
            res = {"null_result":True}

    if not m:
        m = EvalAdvancedSettings2( line )
        if m:
            res = ConvertAdvancedSettings2(m)
    if not m:
        m = EvalSoftwareVersion( line )
        if m:
            # We do not support Software version
            res = {"null_result":True}
    if not m:
        m = EvalRemoteNameRecord(line)
        if m:
            # Don't care, but it is a result...
            res = {"null_result":True}
            
    if not m:
        m = EvalCGMSettings(line)
        if m:
            # CGM Settings not implemented yet
            res = m

    return res

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAnimasSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        _serial = m["serial"]

        # Now modify the serial to look like the one on the sticker on the pump
         
        # Last two characters contains the production date
        _serial = _serial[:-2]
        
        # From the spec:
        # ASCII                                  How it is shown on the Rear Pump Label 
        # 19702-1050  Model=10=IR1200            50-19702-10
        # Display on Status Screen #6=19702-10   {Fields: Check-Seed-Model}
        _serial = _serial[-2:] + '-' + _serial[:-2]
        
        res[ "meter_serial" ] = _serial

    return res
    
def EvalAnimasDeviceModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
    global device_model
    
    res = {}
    
    if device_model == "use_sw_ver":
        return EvalAnimasDeviceModelRecordNew( line )
    
    m = EvalSerialRecord( line )
    if m:        
        serial = m["serial"]

        # From the spec:
        # ASCII                                  How it is shown on the Rear Pump Label 
        # 19702-1050  Model=10=IR1200            50-19702-10
        # Display on Status Screen #6=19702-10   {Fields: Check-Seed-Model}
        
        #So now find the model:
        model = serial[6:8]
        if model == "10":
            device_model = DEVICE_MODEL_IR1200
        elif model == "11":
            device_model = DEVICE_MODEL_IR1250
        elif model == "12":
            device_model = DEVICE_MODEL_2020
        else:
            # Don't trust the model from the serial if not an IR1200/1250/2020
            # Instead, use the SW_version (info from Animas)
            # We can't always rely on SW version, since old Transmitters might not send this value
            device_model = "use_sw_ver"
            
        if device_model != "use_sw_ver":
            res[ "device_model" ] = device_model
            
    return res

def EvalAnimasDeviceModelRecordNew( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
    global device_model

    res = {}
    m = EvalSoftwareVersion( line )
    if m:        
        sw_version = m["software_version"]

        # The model is in the first character of the SW version (according to the spec)
        # "A" = IR1200
        # "B" = IR1250, IR1200+
        # "C" = RESERVED
        # "D" = IR1275
        # "E" = IR1285
        # "F" = Unity1
        model = sw_version[0]
        
        if model == "A":
            device_model = DEVICE_MODEL_IR1200
        elif model == "B":
            device_model = DEVICE_MODEL_IR1250
        elif model == "D":
            device_model = DEVICE_MODEL_2020
        elif model == "E":
            device_model = DEVICE_MODEL_PING
        elif model == "F":
            device_model = DEVICE_MODEL_VIBE
        elif model == "G":
            device_model = DEVICE_MODEL_VIBE
        elif model == "H":
            device_model = DEVICE_MODEL_VIBE_PLUS
        else:
            device_model = None
            
        if device_model != None:
            res[ "device_model" ] = device_model

    return res
    
def EvalAnimasUnitRecord( line ):
    """
    Return mg/dl that is what the remote uses, which basically is a modified Lifescan Ultrasmart
    """
    res = { "meter_unit":"mg/dl" }
    return res

def EvalAnimasResultRecord( line ):
    """
    Is this a result record? If so, return a dictionary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    res = {}

    m = EvalBlackBoxHistoryRecord(line)
    if not m:
        m = EvalHistoryResultRecord(line)
        if not m:
            m = EvalConfigResultRecord(line)
        
    if m:
        res = m
        
    return res
    
def EvalAnimasChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    #check the checksum on frame level (all messages from the pump contains the checksum, sweet)
    # First check the checksum, but do not include the checksum itself
    # Also skip the identifier and 0xc0 in the beginning
    return CalcFletcherChecksum( line[2:-2] ) == line[-2:]
    

def EvalAnimasNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results. 
    """
    global g_add_nr_of_items
    
    res = {}
    m = EvalNrResultsRecord( line )
    if m:
        res[ "meter_nr_results" ] = m["number_results"] + g_add_nr_of_items

    return res
    
def EvalAnimasAllLinesEvaluated():
    """
    Process updated records (timestamp) on Unity
    The implementation is done according to Animas Unity CGM flowchart "Processing of CGM Glucose Records".
    This flowchart was not entirely correct but after comments from Animas the implementation could be updated.
    """

    def is_of_record_type(record, record_type):
        '''
        Is this [record] of the required [record_type]? Has support for both Vibe model < H1a
        and Vibe model >= H1a.
        '''

        record_type_lookup = {
            TYPE_CGM_GLUCOSE_START_SESSION : (TYPE_CGM_GLUCOSE_START_SESSION,TYPE_H_CGM_GLUCOSE_START_SESSION),
            TYPE_CGM_GLUCOSE_STOP_SESSION : (TYPE_CGM_GLUCOSE_STOP_SESSION, TYPE_H_CGM_GLUCOSE_STOP_SESSION),
            TYPE_CGM_GLUCOSE_TIME_LOST : (TYPE_CGM_GLUCOSE_TIME_LOST, TYPE_H_CGM_GLUCOSE_TIME_LOST),
            TYPE_CGM_GLUCOSE_TIME_CHANGED : (TYPE_CGM_GLUCOSE_TIME_CHANGED, TYPE_H_CGM_GLUCOSE_TIME_CHANGED),
            TYPE_CGM_GLUCOSE_BG_CALIBRATION_RECORD : (TYPE_CGM_GLUCOSE_BG_CALIBRATION_RECORD,TYPE_H_CGM_GLUCOSE_BG_CALIBRATION_RECORD),
            TYPE_CGM_GLUCOSE_ADJ_TIME_BEFORE_TIME_LOST : (TYPE_CGM_GLUCOSE_ADJ_TIME_BEFORE_TIME_LOST, TYPE_H_CGM_GLUCOSE_ADJ_TIME_BEFORE_TIME_LOST)        
        }

        if record_type in record_type_lookup:
            if device_model == DEVICE_MODEL_VIBE_PLUS:
                return record == record_type_lookup[record_type][1]
            else:
                return record == record_type_lookup[record_type][0]

        return False
   
    global g_saved_settings
    global g_records_needing_cgm_glucose_recalc
    global g_accumulated_time_adjust
   
    if len(g_saved_settings) == 0 and len(g_records_needing_cgm_glucose_recalc) == 0:
        return { "null_result":True }

    tmpRes = {}
    res = []

    if g_saved_settings.has_key(SETTING_BG_UNIT) and g_saved_settings.has_key(SETTING_SICK_BG_OVER_LIMIT):
        # Let's make the SETTING_SICK_BG_OVER_LIMIT adjustment. The pump will send this setting in mmol/l * 10
        # or mg/dl depending on the current BG_UNIT setting.
        if "mmol" in g_saved_settings[SETTING_BG_UNIT]:
            # The pump sends mmol/l * 10 (scaled).
            g_saved_settings[SETTING_SICK_BG_OVER_LIMIT] = g_saved_settings[SETTING_SICK_BG_OVER_LIMIT] * 100
        else:
            # The pump sends mg/dl (unscaled).
            # The web already uses 18.016 when converting this value so we need to do that to. 
            g_saved_settings[SETTING_SICK_BG_OVER_LIMIT] = round((g_saved_settings[SETTING_SICK_BG_OVER_LIMIT] * 1000) / 18.016)

    if len(g_saved_settings) > 0:
        res.append({SETTINGS_LIST:g_saved_settings})

    refToLastGlucoseValue = None
    markNextGlucoseValueWithStopSession = False

    for cgmRecord in g_records_needing_cgm_glucose_recalc:
        _flags = []
        tmpRes = {}
        tmpRes[ ELEM_FLAG_LIST ] = _flags

        # Decode the timestamp (a signed 32 bit value)
        timeStamp = struct.unpack("<i", cgmRecord[2:6])[0]
        
        # Decode the value & record
        valueWithRecord = struct.unpack("<H", cgmRecord[0:2])[0]
        
        # Mask out the record type
        recordType = valueWithRecord & CGM_GLUCOSE_RECORD_TYPE_MASK
        # Mask out the value
        value = valueWithRecord & CGM_GLUCOSE_VALUE_MASK
        # Calculate time from meter base time
        BaseTimeStamp = datetime(2008, 1, 1, 0, 0) 
        
        # The main analyser
        if is_of_record_type(recordType, TYPE_CGM_GLUCOSE_ADJ_TIME_BEFORE_TIME_LOST) :
            g_accumulated_time_adjust = timeStamp
        elif is_of_record_type(recordType, TYPE_CGM_GLUCOSE_BG_CALIBRATION_RECORD) :
            newTimeStamp = BaseTimeStamp + timedelta(seconds=(timeStamp + g_accumulated_time_adjust))
            # Add the value, if valid
            # The actual compared values comes from the Unity EEPROM specification
            #if 1:
            if ((value >= 39 ) and (value <= 401)):
                _flags.append(FLAG_CALIBRATION)
                _flags.append(FLAG_CONTINOUS_READING)
                tmpRes[ ELEM_TIMESTAMP ] = newTimeStamp
                tmpRes[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                tmpRes[ ELEM_VAL ] = value
                if markNextGlucoseValueWithStopSession:
                    markNextGlucoseValueWithStopSession = False
                    if not FLAG_LAST_GLUCOSE_VALUE_IN_GROUP in _flags:
                        _flags.append(FLAG_LAST_GLUCOSE_VALUE_IN_GROUP)
                res.append(tmpRes)
                refToLastGlucoseValue = tmpRes
        elif is_of_record_type(recordType, TYPE_CGM_GLUCOSE_TIME_CHANGED) :
            g_accumulated_time_adjust -= timeStamp
        elif is_of_record_type(recordType, TYPE_CGM_GLUCOSE_TIME_LOST) :
            pass
        elif is_of_record_type(recordType, TYPE_CGM_GLUCOSE_STOP_SESSION):
            markNextGlucoseValueWithStopSession = True
            
        elif is_of_record_type(recordType, TYPE_CGM_GLUCOSE_START_SESSION):
            if refToLastGlucoseValue:
                if not FLAG_FIRST_GLUCOSE_VALUE_IN_GROUP in refToLastGlucoseValue[ ELEM_FLAG_LIST ]:
                    refToLastGlucoseValue[ ELEM_FLAG_LIST ].append(FLAG_FIRST_GLUCOSE_VALUE_IN_GROUP)
            
        elif recordType == 0:
            newTimeStamp = BaseTimeStamp + timedelta(seconds=(timeStamp + g_accumulated_time_adjust))
            # Add the value, if valid
            # The actual compared values comes from the Unity EEPROM specification
            if ((value >= 39 ) and (value <= 401)):
                _flags.append(FLAG_CONTINOUS_READING)
                tmpRes[ ELEM_TIMESTAMP ] = newTimeStamp
                tmpRes[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                tmpRes[ ELEM_VAL ] = value
                res.append(tmpRes)
                if markNextGlucoseValueWithStopSession:
                    markNextGlucoseValueWithStopSession = False
                    if not FLAG_LAST_GLUCOSE_VALUE_IN_GROUP in _flags:
                        _flags.append(FLAG_LAST_GLUCOSE_VALUE_IN_GROUP)
                refToLastGlucoseValue = tmpRes
        else:
            newTimeStamp = BaseTimeStamp + timedelta(seconds=(timeStamp + g_accumulated_time_adjust))
            if device_model != DEVICE_MODEL_VIBE_PLUS:
                print "Unhandled Record Type: ", recordType
                print "Timestamp: ", newTimeStamp, " Value: ", value
                print repr(tmpRes)

    return res
    
        
def QueueDexComDataTransfer(serial):
    """
    If DexCom debug data exists, send it to the DexCom server
    """
    global g_unity_dexcom_debug_data
    
    # Debug serial, if needed
    #serial = "ABC123"
   
    if len(g_unity_dexcom_debug_data) > 0:
        # Concatenate all binary chunks into one big one
        data = ''.join(g_unity_dexcom_debug_data)

        # Post the actual job. 
        # @TODO: Change the fixed "terminal ID" to the actual terminal ID,
        # when logic for this exists somewhere in the generic engine
        DexComUploader.PostJob(data, "0", serial)

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectAnimasIR( inList ):
    """
    Detect if data comes from an Animas Pump. 
    """
    return DetectDevice( "AnimasIR", inList, DEVICE_PUMP )

def AnalyseAnimasIR( inData ):
    """
    Analyse Animas Pump
    """
    global _bg_display_mode_setting
    global _records_needing_bg_recalc
    global g_records_needing_cgm_glucose_recalc
    global g_shared_bolus_timestamps
    global g_accumulated_time_adjust
    global device_model
    global g_unity_dexcom_debug_data
    global g_saved_settings
    
    _bg_display_mode_setting = None
    _records_needing_bg_recalc = []
    g_records_needing_cgm_glucose_recalc = []
    g_shared_bolus_timestamps = []
    device_model = None
    g_accumulated_time_adjust = 0
    g_unity_dexcom_debug_data = []
    g_saved_settings = {}
    
    inList = SplitData (inData)
    
    # Empty dictionary
    d = { \
        "eval_serial_record" : EvalAnimasSerialRecord, \
        "eval_device_model_record" : EvalAnimasDeviceModelRecord, \
        "eval_unit" : EvalAnimasUnitRecord, \
        "eval_result_record" : EvalAnimasResultRecord, \
        "eval_checksum_record" : EvalAnimasChecksumRecord, \
        "eval_nr_results" : EvalAnimasNrResultsRecord, \
        "all_lines_evaluated": EvalAnimasAllLinesEvaluated, \
    }

    resList = AnalyseGenericMeter( inList, d );
    
    # Perform post-analyse step
    # @TODO: Maybe add methods in the generic engine to do these kinds of things. 
    res = resList[0]
    if not(("results" in res) and ResultIsError(res["results"])):
        # Successfully analysed data
        if len(g_unity_dexcom_debug_data):
            # DexCom data is available
            if ("header" in res) and (ELEM_DEVICE_SERIAL in res["header"]):
                # Serial exists as well, queue the transfer!
                # QueueDexComDataTransfer(res["header"][ELEM_DEVICE_SERIAL])
                # @TODO: Re-enable support when needed.
                pass
    
    return resList

if __name__ == "__main__":

    test_files = (
        'Animas2020/Animas2020-Daniel-Bolus-breakdown.bin',
        'Animas2020/Animas2020.bin',
        'Animas2020/ep020.bin',
        'AnimasBlackBoxHistory/Animas2020BlackBoxHistory.bin',
        'AnimasBlackBoxHistory/AnimasVibeBlackBoxHistory.bin',
        'AnimasIR1200/ep021.bin',
        'AnimasIR1250/AnimasIR1250_advanced_settings.bin',
        'AnimasIR1250/reg55.bin',
        'AnimasIRPing/AnimasIRPing-ticket2211.bin',
        'AnimasIRPing/AnimasIRPing-ticket2217.bin',
        'AnimasIRPing/AnimasIRPing-ticket2289.bin',
        'AnimasIRPing/AnimasIRPing-ticket2323.bin',
        'AnimasIRPing/reg38.bin',
        'AnimasIRUnity/ArabicVibe-04-52731-16.bin',
        'AnimasIRUnity/ep001.bin',
        'AnimasIRUnity/ep002.bin',
        'AnimasIRUnity/ep003.bin',
        'AnimasIRUnity/ep005.bin',
        'AnimasIRUnity/Vibe-ticket2983-ZD10706.bin',
        'AnimasPacked/Animas2020Packed.bin',
        'AnimasPacked/AnimasPingPacked.bin',
        'AnimasPacked/AnimasVibePacked.bin',
        'AnimasPacked/AnimasVibeH.bin',
    )

    for test_file in test_files:
        with open('test/testcases/test_data/%s' % (test_file), 'r') as f:
            payload = f.read()

        res = AnalyseAnimasIR(payload)
        print res[0]['header']
