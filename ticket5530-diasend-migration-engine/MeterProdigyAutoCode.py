# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE Prodigy AutoCode
#
import traceback
import re
from decimal import Decimal

from Generic import *
from Defines import *

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

class AnalyserProdigyPalmdoc(object):

    MODEL_TYPE_PRODIGY_AUTOCODE = "PRODIGY_AUTOCODE"
    MODEL_TYPE_PALMDOC = "PALMDOC"

    def __init__(self, model_type):
        self.model_type = model_type
        self.serial = ''

    def split(self, in_data):

        result = []

        try:
            idx1 = in_data.index('TAG_SERIAL')
            idx2 = in_data.index('TAG_GLUCOSE')

        except ValueError:
            return result

        serial_data = in_data[idx1:idx2]
        result.append(serial_data) # We let the tag remain...

        result.extend(in_data[idx2:].split('\r\n'))

        return result

    def EvalSerialRecord(self, line):

        result = {}

        if self.model_type == AnalyserProdigyPalmdoc.MODEL_TYPE_PRODIGY_AUTOCODE:
            m = re.match(r'^TAG_SERIAL.(\d{5}-\d{7})(.{1})$', line)
            if m is None:
                return result
            calc_checksum = sum(map(ord, line[11:-1])) & 0xff
        elif self.model_type == AnalyserProdigyPalmdoc.MODEL_TYPE_PALMDOC:
            m = re.match(r'^TAG_SERIAL(\d{4}-\d{7}).{3}(.{1})', line)
            if m is None:
                return result            
            calc_checksum = sum(map(ord, line[10:-1])) & 0xff

        checksum = ord(m.group(2))

        if checksum == calc_checksum:
            result = {ELEM_DEVICE_SERIAL: m.group(1)}

        self.serial = m.group(1)

        return result

    def EvalModelRecord(self, record):
        if self.model_type == AnalyserProdigyPalmdoc.MODEL_TYPE_PRODIGY_AUTOCODE:
            return {ELEM_DEVICE_MODEL: 'Prodigy AutoCode'}
        elif self.model_type == AnalyserProdigyPalmdoc.MODEL_TYPE_PALMDOC:
            if self.serial.startswith('7861-1'):
                return {ELEM_DEVICE_MODEL: 'iCare PalmDoc I'}
            elif self.serial.startswith('7869-2'):
                return {ELEM_DEVICE_MODEL: 'iCare PalmDoc II'}
        return {}

    def EvalResultRecord(self, line):

        def _fuzzy_to_int(value):
            try:
                return int(value)
            except ValueError:
                return value    

        m = re.match(r'^\x02?(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(Hi\.|Lo\.|[\d\.]+),(mg_dL|mmol_L)$', line)
        if m:
            keys   = ('index', 'year', 'month', 'day', 'hour', 'minute', 'value', 'unit')
            values = map(_fuzzy_to_int, m.groups()[:9])
            items  = dict(zip(keys, values))

            result = {
                ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                ELEM_TIMESTAMP: datetime(items['year'], items['month'], items['day'], items['hour'], items['minute']),
                ELEM_FLAG_LIST: [], 
                ELEM_DEVICE_UNIT: 'mg/dL'
                }

            if items['value'] == 'Hi.':
                result[ELEM_VAL] = 600 * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
            elif items['value'] == 'Lo.':
                result[ELEM_VAL] = 20 * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
            else:
                if items['unit'] == 'mg_dL':
                    result[ELEM_VAL] = items['value'] * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                    result[ELEM_DEVICE_UNIT] = 'mg/dL'                    
                else:
                    result[ELEM_VAL]         = int(Decimal(items['value']) * 10)
                    result[ELEM_DEVICE_UNIT] = 'mmol/L'

            return result

        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterProdigyAutoCode(in_list):
    return DetectDevice( 'Prodigy_AutoCode', in_list, DEVICE_METER )

def AnalyseMeterProdigyAutoCode(in_data):
    analyser = AnalyserProdigyPalmdoc(AnalyserProdigyPalmdoc.MODEL_TYPE_PRODIGY_AUTOCODE)
    return AnalyseMeter(analyser, in_data)

def DetectMeterPalmdoc(in_list):
    return DetectDevice( 'palmdoc', in_list, DEVICE_METER )

def AnalyseMeterPalmdoc(in_data):
    analyser = AnalyserProdigyPalmdoc(AnalyserProdigyPalmdoc.MODEL_TYPE_PALMDOC)
    return AnalyseMeter(analyser, in_data)

def AnalyseMeter(analyser, in_data):
    """
    Analyse data from Prodigy AutoCode.
    """

    d = {
         "eval_device_model_record": analyser.EvalModelRecord,
         "eval_serial_record"      : analyser.EvalSerialRecord,
         "eval_result_record"      : analyser.EvalResultRecord}

    lines = analyser.split(in_data)
    res_list = AnalyseGenericMeter(lines, d)
    
    return res_list

if __name__ == "__main__":

    testfiles = (
        'ProdigyAutoCode_51850-2217180.log',
        'ProdigyAutoCode_51850-2363309.log')

    for testfile in testfiles:
        with open('test/testcases/test_data/ProdigyAutoCode/%s' % (testfile,), 'r') as f:
            data = f.read()
            results = AnalyseMeterProdigyAutoCode(data)
            #for result in results[0]['results']:
            #    print result
            print results[0]['header']

    testfiles = (
        '78611001014.log',
        '78611001015.log',
        '78611001013.log',
        '78692001014.log',
        '78692001013.log')

    for testfile in testfiles:
        with open('test/testcases/test_data/palmdoc/%s' % (testfile,), 'r') as f:
            data = f.read()
            results = AnalyseMeterPalmdoc(data)
            print results[0]['header']
            #for result in results[0]['results']:
            #    print result
 