# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Abbott FreeStyle InsuLinx
# @DEVICE Abbott FreeStyle Optium Neo
# @Device Abbott FreeStyle Flash/Libre

import re
import string
import datetime

from Generic import *
from Defines import *

from SupportAbbottFreestyleOptiumNeo import AnalyseOptiumNeo
from SupportAbbottFreestyleLibre import AnalyseLibre
from SupportAbbottFreestyleLibre import ConvertBinaryToText
from SupportAbbottFreestyleInsulinx import AnalyseInsulinx

class Command(object):
    '''
    A typical communication with the device looks like this. 

            $carbratio?
            0,0
            1,0
            2,0
            3,0
            4,0
            0
            CKSM:00000380
            CMD OK

    The first line is the command sent; followed by the result. This
    class holds this information in the following way.

        name    Name of command (carbratio above).
        records List of all rows in the response up to but not including the checksum.
    '''

    def __init__(self, name):
        self.name    = name
        self.records = []

    def add_record(self, record):
        self.records.append(record)

    def get_records(self):
        return self.records

    def get_name(self):
        return self.name

    def nr_of_records(self):
        return len(self.records)

class Analyser(object):

    def __init__(self):
        self.commands          = []
        self.number_of_records = 0
        self.diasend_records   = []

    def find_serial(self):
        for record in self.diasend_records:
            if 'record_type' in record and record['record_type'] == 'serial':
                return record['record_value']['meter_serial']

        return 'UNKNOWN'

    def extract_command(self, row):
        m = re.match(r'^.*\$([a-z]+)[,?](\d*)\r\n$', row, re.IGNORECASE)
        if m is not None:
            return m.group(1)
        return None

    def extract_checksum(self, row):
        m = re.match(r'^CKSM:([\da-fA-F]{8})', row, re.IGNORECASE)
        if m is not None:
            return int(m.group(1), 16)
        return None

    def validate_checksum(self, records, result_checksum):
        checksum = 0
        for record in records:
            for c in record:
                checksum += ord(c)

        if checksum != result_checksum:
            raise Exception(ERROR_CODE_CHECKSUM, 'Checksum failure - 0x%x != 0x%x' % (checksum, result_checksum))

    def is_command_result_ok(self, row):
        m = re.match(r'^(CMD OK)', row, re.IGNORECASE)
        return m != None

    def is_command_result_fail(self, row):
        print row
        m = re.match(r'^(CMD Fail!)', row, re.IGNORECASE)
        return m != None

    def run_state_machine(self, data):
        '''
        Data is a raw dump of the communication with the device. Each 
        set of records begins with the command sent to the device followed
        by multiple lines where each line = one record. Last comes a 
        checksum and an overall command result (normally CMD OK).

        Example : 

            $carbratio?
            0,0
            1,0
            2,0
            3,0
            4,0
            0
            CKSM:00000380
            CMD OK

        This function collects each command with corresponding results into 
        a Command instance and validates the checksum.
        '''

        # If the device is brand new or has gone out of power, for instance
        # out of battery, the date/time is reset and requests for their values
        # will fail. This is kind of a normal case so allow it.
        commands_allowed_to_fail = ['time', 'date']

        rows  = data.splitlines(True)
        state = 'command'

        for row in rows:
            if state == 'command':
                command_str = self.extract_command(row)
                if command_str is not None:
                    command = Command(command_str)
                    state = 'record'
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Statemachine error - unexpected row of data')

            elif state == 'record':
                checksum = self.extract_checksum(row)
                if checksum is not None:
                    self.validate_checksum(command.get_records(), checksum)
                    state = 'result'
                else:
                    command.add_record(row)

            elif state == 'result':
                if not self.is_command_result_ok(row):
                    print self.is_command_result_fail(row), command.get_name(), \
                        commands_allowed_to_fail, command.get_name() in commands_allowed_to_fail

                    # Something else than a "real fail" or its a fail and the
                    # command must not fail -> throw exception!
                    if not self.is_command_result_fail(row) or \
                       not command.get_name() in commands_allowed_to_fail:
                        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                                        'Statemachine error - unexpected row of data')

                    print self.is_command_result_fail(row), command.get_name()
                else:
                    self.commands.append(command)

                state = 'command'

    def build_diasend_records(self):
        '''
        Convert the Abbott formatted records in self.commands and convert them to 
        diasend format. 
        '''

        # Determine type of device
        
        analysers = [AnalyseOptiumNeo, AnalyseLibre, AnalyseInsulinx]

        for analyser in analysers:
            if analyser.is_of_model(self.commands):

                self.diasend_records = []

                for command in self.commands:
                    self.diasend_records.extend(analyser.convert_result_to_diasend(command))

                break

        # Data doesn't store number of results - let's generate it. 
        number_of_records = sum(1 for item in self.diasend_records if (ELEM_VAL in item) or (SETTINGS_LIST in item))
        self.diasend_records.append({ELEM_NR_RESULTS: number_of_records})

def EvalResultRecord(line):
    if ELEM_VAL in line or SETTINGS_LIST in line:
        return line
    return {}

def EvalUnitRecord(line):
    return { "meter_unit":"mg/dL" }

def EvalChecksumRecord(line, record):
    return True

def EvalNrResultsRecord(line):
    if ELEM_NR_RESULTS in line:
        return line
    return {}

def EvalSerialRecord(line):
    if ELEM_DEVICE_SERIAL in line:
        return line
    return {}

def EvalDeviceModelRecord( line ):
    if ELEM_DEVICE_MODEL in line:
        return line
    return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

model_string = 'AbbottFreeStyleInsuLinx'

def DetectAbbottFreestyleInsulinx(inList):
    """
    Detect if data comes from a Freestyle Insulinx.
    """
    return DetectDevice( model_string, inList, DEVICE_METER );

def AnalyseAbbottFreestyleInsulinx(inData):

    analyser = Analyser()
    decoder = ConvertBinaryToText(inData)

    try:
        inData = decoder.decode()
        analyser.run_state_machine(inData)
        analyser.build_diasend_records()

    except Exception, e:

        import traceback
        traceback.print_exc()
        
        serial_number = analyser.find_serial()

        # The 'standard' way is to raise Exception with two parameters, diasend error code and 
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(model_string, serial_number, e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(model_string, serial_number, ERROR_CODE_PREPROCESSING_FAILED, 
                ''.join(map(str, e.args)))

        return error_response

    d = {}
    d[ "eval_serial_record" ]       = EvalSerialRecord
    d[ "eval_unit"]                 = EvalUnitRecord
    d[ "eval_result_record" ]       = EvalResultRecord
    d[ "eval_checksum_record" ]     = EvalChecksumRecord
    d[ "eval_nr_results" ]          = EvalNrResultsRecord
    d[ "eval_device_model_record" ] = EvalDeviceModelRecord
 
    resList = AnalyseGenericMeter(analyser.diasend_records, d)

    return resList

# Local test.

if __name__ == '__main__':

    files = ('test/testcases/test_data/AbbottFreeStyleInsuLinx/JAMR202-K2030-with-insulin.bin',
        'test/testcases/test_data/AbbottFreeStyleInsuLinx/JAGS185-U3998-with-settings.bin',
        'test/testcases/test_data/AbbottFreeStyleInsuLinx/JAMR202-K1944-settings-setup.bin',
        'test/testcases/test_data/AbbottFreeStyleInsuLinx/JAGS185-U4159-time-change.bin',
        'test/testcases/test_data/AbbottFreeStyleInsuLinx/InsuLinx-DTV0events.log',
        'test/testcases/test_data/AbbottFreeStyleInsuLinx/InsuLinx-Z21737.bin',
        'test/testcases/test_data/AbbottFreeStyleOptiumNeo/Abbott_Optium_Neo_KetoneResults_DataSet_365Days.log',
        'test/testcases/test_data/AbbottFreeStyleOptiumNeo/Abbott_Optium_Neo_KetoneResults_DataSet_365Days.log',
        'test/testcases/test_data/AbbottFreeStyleOptiumNeo/Abbott_Optium_Neo_E0210.log',
        'test/testcases/test_data/AbbottFreeStyleOptiumNeo/Abbott_Optium_Neo_E0210-high-low-ketones.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9008_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/Fail/AbbottLibre_ApolRCR9008_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9009_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9009_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9010_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9010_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9011_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9016_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9012_RAR.log', 
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-Z50202-BE-carbs.log', 
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_Clinical_dataset_SYSTEM_A.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-JG-serial.log',
        'test/testcases/test_data/AbbottFreeStyleInsuLinx/InsuLinx-no-datetime.bin',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-unicode-comment-Z30112.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-special-comment-Z26195.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9008_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9009_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9009_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9010_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9010_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9011_LAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9012_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre_ApolRCR9016_RAR.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/X0157.txt.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/X0157.bin.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/T0115.txt.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/T0115.bin.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/T0163.txt.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/T0163.bin.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-exercising-id23-variable.bin.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-exercising-id23-variable-2.bin.log',
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-exercising-id38-variable.bin.log',        
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-error-16-statemachine-error-bin.log', 
        'test/testcases/test_data/AbbottFreeStyleLibre/AbbottLibre-error-16-statemachine-error2-bin.log')

    for filename in files:
        print filename
        with open(filename, 'r') as f:

            d = f.read()
            f.close()

            res = AnalyseAbbottFreestyleInsulinx(d)
            for r in res[0]['results']:
                pass
