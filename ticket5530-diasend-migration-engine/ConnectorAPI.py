import json
import requests 
import datetime
import Defines

DEFAULT_DEVICE_UNIT                 = None
CGM_DEVICE_TYPES                    = ("Abbott FreeStyle Libre", 
                                       "Dexcom G4", 
                                       "Dexcom G5", 
                                       "Dexcom Seven Plus", 
                                       "FreeStyle Navigator", 
                                       "FreeStyle Navigator II", 
                                       "Navigator", 
                                       "Senseonics Transmitter")

SOURCE_TYPE_METER                   = "meter"
SOURCE_TYPE_CGM                     = "cgm_device"
SOURCE_TYPE_PUMP                    = "pump"
SOURCE_TYPE_DEVICE                  = "device"
SOURCE_TYPE_UNKNOWN                 = "unknown"

SOURCE_TYPE_MAP                     = {SOURCE_TYPE_METER: "bg_meter",
                                       SOURCE_TYPE_CGM: "cgm_device",
                                       SOURCE_TYPE_PUMP: "pump",
                                       SOURCE_TYPE_DEVICE: "device"}

JSON_TYPE_MAP                       = {SOURCE_TYPE_METER: "meter",
                                       SOURCE_TYPE_CGM: "cgmDevice",
                                       SOURCE_TYPE_PUMP: "pump",
                                       SOURCE_TYPE_DEVICE: "device"}

DEVICE_UNIT_MMOLL                   = "mmoll"
DEVICE_UNIT_MGDL                    = "mgdl"

SUPPORTED_SOURCE_TYPES              = (SOURCE_TYPE_METER, SOURCE_TYPE_CGM, SOURCE_TYPE_PUMP, SOURCE_TYPE_DEVICE)

RESPONSE_CODE_OK                    = (200, 201, 204)
RESPONSE_CODE_REAUTH                = 421

SOFTWARE_UPLOADER                   = "uploader"
SOFTWARE_TRANSMITTER                = "transmitter"

VALUE_TYPE_SETTING                  = "device_setting"

ENDPOINT_CLINIC                     = "clinic"

"""
  Sends data to the connector API
"""
class ConnectorAPI:
    endpoint = ""
    sync_timestamp = ""
    values = []
    settings = []
    source_type = ""
    device_unit = DEFAULT_DEVICE_UNIT
    device_guid = ""
    programs = []

    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.values = []
        self.value_list = None
        self.settings = []
        self.programs = []
        self.source_type = ""
        self.sync_timestamp = ""
        self.device_guid = ""
  
    def send_data(self, connector, data_type, data, cookie, device_guid, sync_timestamp = None):
        """
        Generic function to send data to connector API
        Parameters:
        connector - base uri for the calls (i.e api)
        data_type - name of type of data to send (i.e. meter)
        data - data dictonary to send to connector API
        cookie - a authentication cookie used on call to connector API
        device_guid - GUID for the device 
        """
        if data_type:
            request_body = {data_type: data}
        else:
            request_body = data

        if device_guid:
            request_body["deviceGuid"] = device_guid 
         
        if sync_timestamp:
            request_body["syncTimestamp"] = sync_timestamp

        payload = json.dumps(request_body)
        headers = {"Content-Type": "application/json", "Accept-Charset": "UTF-8", "Cookie": cookie}
        endpoint = self.endpoint
        if connector: 
            endpoint += "/"+connector
        if data_type:
            endpoint += "/"+data_type
        print endpoint
        print payload

        r = requests.post(endpoint, data=payload, headers=headers)
        print "Response code", r.status_code
        if r.status_code == RESPONSE_CODE_REAUTH:
            print "Cookie needs to be reauthenticated"
            raise Exception(Defines.ERROR_CODE_USER_REQUIRED)
        elif r.status_code not in RESPONSE_CODE_OK:
            print "Something has gone wrong..."
            raise Exception()
        try:
            return (r.status_code, r.json())
        except ValueError, e:
            print "No json"
            print r.text
            return (r.status_code, {})

    def get_terminal_id(self, cookie, software_id, software_version, terminal_serial, operating_system, source_type):
        """
        Try to get/retrieve a terminal id for terminal in sync.
        """
        if not software_version:
            software_version = "N/A"

        request_body = {"softwareId": software_id, "softwareVersion": software_version, "operatingSystem": operating_system, "sourceType": source_type}

        # Only include terminal_serial if it"s set
        if terminal_serial != "0" and terminal_serial != "":
            request_body["serialNumber"] = terminal_serial

        (result, response) = self.send_data(None, "terminal", request_body, cookie, None)
        return response["serial_number"] if response.has_key("serial_number") else ""

    def start_sync(self, device_serial, device_type, device_class, cookie):
        """
        Start device sync/upload to connector API.
        """
        self.device_guid = self.get_device_id(device_serial, device_type, device_class, cookie)
        if not self.device_guid:
            print "Could not get device guid"
            return False

        # Start sync of data
        self.sync_timestamp = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%I:%S")

        if not self.send_device_data(cookie):
            print "Could not send device data"
            return False

        return True

    def end_sync(self, operating_system, operating_system_version, software_version, terminal_serial, application_type, cookie):
        """
        Send end sync information to connector API.
        This wraps up the synchronization of data.
        """
        if not software_version:
            software_version = "1.0.0"

        request_body = {"syncEvents": 
                           [{"sourceType": SOURCE_TYPE_MAP[self.source_type], 
                             "source": self.device_guid, 
                             "syncTimestamp": self.sync_timestamp, 
                             "deviceInformation": { 
                                 "applicationType": application_type, 
                                 "os": operating_system.lower(), 
                                 "osVersion": operating_system_version, 
                                 "applicationVersion": software_version, 
                                 "serialNumber": terminal_serial 
                                 }  
                           }] 
                       }

        (result, result_response) = self.send_data("sync/completed", None, request_body, cookie, None)

        self.sync_timestamp = ""
        self.source_type = ""
        self.device_guid = ""

        return True

    def get_device_id(self, device_serial, device_type, device_class, cookie):
        """ 
          Send data about device and retrieve the device_guid
        """
        request_body = {}
        device_data = {"serialNumber": device_serial, "deviceType": device_type}

        # Set meter units if existing
        if self.device_unit:
            device_data["units"] = self.device_unit

        if device_class == "device_pump":
            self.source_type = SOURCE_TYPE_PUMP
        elif device_type in CGM_DEVICE_TYPES:
            self.source_type = SOURCE_TYPE_CGM
        elif device_class == "device_meter":
            self.source_type = SOURCE_TYPE_METER
        elif device_class == "device":
            self.source_type = SOURCE_TYPE_DEVICE
        else:
            self.source_type = SOURCE_TYPE_UNKNOWN
 
        if self.source_type not in SUPPORTED_SOURCE_TYPES:
            print "Only meters and cgms supported this is a ", self.source_type
            raise Exception()

        request_body[JSON_TYPE_MAP[self.source_type]] = device_data
        (result, device_response) = self.send_data(self.source_type, None, request_body, cookie, None)

        return device_response["guid"] if device_response.has_key("guid") else None

    def add_device_setting(self, key, value):
        """ 
        Add device setting to internal list.
        If there is a setting about meter units, save the information.
        """
        # Store away device unit if found for use by inserting device
        if key == Defines.SETTING_BG_UNIT:
            if value == 0 or value == "mg/dl":
                self.device_unit = DEVICE_UNIT_MGDL
            if value == 1 or value == "mmol/L":
                self.device_unit = DEVICE_UNIT_MMOLL

        self.settings.append({"type": VALUE_TYPE_SETTING, "key": key, "value": value})
        return True

    def add_value_list(self, value, value_type):
        if not self.value_list:
            self.value_list = []
        self.value_list.append({"value": value, "type": value_type})
        return True

    def add_value(self, value, timestamp, flags, value_type = "glucose"):
        """ 
          Add value to send to connector API
        """ 
        value_dict = {"value": value, "timestamp": timestamp, "flags": flags, "type": value_type}
        if self.value_list:
            value_dict["valueList"] = self.value_list
        self.values.append(value_dict)
        self.value_list = None
        return True

    def send_device_data(self, cookie):
        """
          Send added values to the connector API
        """
        payload = self.programs + self.settings + self.values
        (result, value_response) = self.send_data(self.source_type, "data", payload, cookie, self.device_guid, self.sync_timestamp)
        self.values = []
        self.settings = []
        self.programs = []
        return True

    def add_device_program(self, program_type, program_name, period_start, period_rate):
        program = {"type": program_type, "name": program_name, "periodStart": period_start, "periodRate": period_rate}
        self.programs.append(program)
        return True

    def add_device_program_value(self, value_type, value):
        if len(self.programs) == 0:
            return False
        if not "valueList" in self.programs[-1]:
            self.programs[-1]["valueList"] = []
        self.programs[-1]["valueList"].append({"type": value_type, "value": value})

        return True

if __name__ == "__main__":
    print "Running main"
    capi = ConnectorAPI("https://localhost/clinic")

    capi.device_guid = "test-guid"
    try:
        capi.get_device_id("test-serial", "device_meter", "device", "test-cookie")
    except:
        pass
    capi.sync_timestamp =  datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%I:%S")
    capi.source_type = SOURCE_TYPE_DEVICE
    capi.add_device_program("basal_program", "1", "00:00:00", "4100")
    capi.add_device_program_value("bg_deviation", "555")
    capi.add_device_program_value("bg_deviation", "255")
    capi.add_device_program_value("bg_deviation", "355")
    capi.add_device_setting(123, "test")
    capi.add_device_setting(124, "test")
    capi.add_value(1000, "2016-12-12 12:31:21", [])
    capi.add_value(4000, "2016-12-12 12:35:21", [106,])
    capi.add_value(5000, "2016-12-12 12:41:21", [])
    try:
        capi.send_device_data("test")
    except:
        pass
    try:
        capi.end_sync("OS-test", "OS-ver", "SW-ver", "Term-serial", "transmitter", "test-cookie")
    except:
        pass



