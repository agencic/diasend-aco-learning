# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, Sweden, http://diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
#
# _________  ___ ___________  _________
# \______   \/   |   \______ \ \_   ___ \
#  |     ___/    ~    \    |  \/    \  \/
#  |    |   \    Y    /    `   \     \____
#  |____|    \___|_  /_______  /\______  /
#                  \/        \/        \/
#
# -----------------------------------------------------------------------------

# @DEVICE PHDC


import logging

from Generic import *
from Defines import *

from Buffer import Buffer
from Record import BaseRecord, EnumRecord, EnumBitRecord

from datetime import timedelta

MDC_MOC_VMO_METRIC = 4
MDC_MOC_VMO_METRIC_ENUM = 5
MDC_MOC_VMO_METRIC_NU = 6
MDC_MOC_VMO_METRIC_SA_RT = 9
MDC_MOC_SCAN = 16
MDC_MOC_SCAN_CFG = 17
MDC_MOC_SCAN_CFG_EPI = 18
MDC_MOC_SCAN_CFG_PERI = 19
MDC_MOC_VMS_MDS_SIMP = 37
MDC_MOC_VMO_PMSTORE = 61
MDC_MOC_PM_SEGMENT = 62

MDC_ATTR_ID_INSTNO                  = 2338
MDC_ATTR_ID_MODEL                   = 2344
MDC_ATTR_ID_PROD_SPECN              = 2349
MDC_ATTR_ID_TYPE                    = 2351
MDC_ATTR_METRIC_STORE_CAPAC_CNT     = 2369
MDC_ATTR_METRIC_STORE_SAMPLE_ALG    = 2371
MDC_ATTR_METRIC_STORE_USAGE_CNT     = 2372
MDC_ATTR_MSMT_STAT                  = 2375
MDC_ATTR_NUM_SEG                    = 2385
MDC_ATTR_SEG_USAGE_CNT              = 2427
MDC_ATTR_SYS_ID                     = 2436
MDC_ATTR_TIME_ABS                   = 2439
MDC_ATTR_TIME_END_SEG               = 2442
MDC_ATTR_TIME_REL                   = 2447
MDC_ATTR_TIME_START_SEG             = 2450
MDC_ATTR_UNIT_CODE                  = 2454
MDC_ATTR_PM_SEG_MAP                 = 2638
MDC_ATTR_OP_STAT                    = 2387
MDC_ATTR_DEV_CONFIG_ID              = 2628
MDC_ATTR_MDS_TIME_INFO              = 2629
MDC_ATTR_METRIC_SPEC_SMALL          = 2630
MDC_ATTR_ENUM_OBS_VAL_SIMP_OID      = 2633
MDC_ATTR_REG_CERT_DATA_LIST         = 2635
MDC_ATTR_NU_VAL_OBS_BASIC           = 2636
MDC_ATTR_PM_STORE_CAPAB             = 2637
MDC_ATTR_ATTRIBUTE_VAL_MAP          = 2645
MDC_ATTR_NU_VAL_OBS_SIMP            = 2646
MDC_ATTR_PM_STORE_LABEL_STRING      = 2647
MDC_ATTR_PM_SEG_LABEL_STRING        = 2648
MDC_ATTR_SYS_TYPE_SPEC_LIST         = 2650
MDC_ATTR_TIME_ABS_ADJUST            = 2658
MDC_ATTR_CLEAR_TIMEOUT              = 2659
MDC_ATTR_TRANSFER_TIMEOUT           = 2660
MDC_ATTR_ENUM_OBS_VAL_BASIC_BIT_STR = 2662

MDC_ACT_SEG_GET_INFO    = 3085
MDC_ACT_SEG_TRIG_XFER   = 3100

MDC_NOTI_CONFIG         = 3356
MDC_NOTI_SEGMENT_DATA   = 3361


# Partitions
MDC_PART_OBJ        = 1     # Object Infrastr.
MDC_PART_SCADA      = 2     # SCADA (Physio IDs)
MDC_PART_DIM        = 4     # Dimension
MDC_PART_INFRA      = 8     # Infrastructure
MDC_PART_PHD_DM     = 128   # Disease Mgmt
MDC_PART_PHD_HF     = 129   # Health and Fitness
MDC_PART_PHD_AI     = 130   # Aging Independently
MDC_PART_RET_CODE   = 255   # Return Codes
MDC_PART_EXT_NOM    = 256   # Ext. Nomenclature

# from the SCADA partition
MDC_TEMP_BODY                       = 19292 # TEMPbody
MDC_CONC_GLU_GEN                    = 28948
MDC_CONC_GLU_CAPILLARY_WHOLEBLOOD   = 29112
MDC_CONC_GLU_CAPILLARY_PLASMA       = 29116
MDC_CONC_GLU_VENOUS_WHOLEBLOOD      = 29120
MDC_CONC_GLU_VENOUS_PLASMA          = 29124
MDC_CONC_GLU_ARTERIAL_WHOLEBLOOD    = 29128
MDC_CONC_GLU_ARTERIAL_PLASMA        = 29132
MDC_CONC_GLU_CONTROL                = 29136
MDC_CONC_GLU_ISF                    = 29140
MDC_CONC_HBA1C                      = 29148
MDC_CONC_GLU_UNDETERMINED_WHOLEBLOOD= 29292
MDC_CONC_GLU_UNDETERMINED_PLASMA    = 29296
MDC_MASS_BODY_ACTUAL                = 57664
MDC_BODY_FAT                        = 57676

# From DM Partition
MDC_GLU_METER_DEV_STATUS    = 29144
MDC_CTXT_GLU_EXERCISE       = 29152
MDC_CTXT_GLU_CARB           = 29156
MDC_CTXT_GLU_CARB_BREAKFAST = 29160
MDC_CTXT_GLU_CARB_LUNCH     = 29164
MDC_CTXT_GLU_CARB_DINNER    = 29168
MDC_CTXT_GLU_CARB_SNACK     = 29172
MDC_CTXT_GLU_CARB_DRINK     = 29176
MDC_CTXT_GLU_CARB_SUPPER    = 29180
MDC_CTXT_GLU_CARB_BRUNCH    = 29184
MDC_CTXT_MEDICATION         = 29188
MDC_CTXT_MEDICATION_RAPIDACTING         = 29192
MDC_CTXT_MEDICATION_SHORTACTING         = 29196
MDC_CTXT_MEDICATION_INTERMEDIATEACTING  = 29200
MDC_CTXT_MEDICATION_LONGACTING          = 29204
MDC_CTXT_MEDICATION_PREMIX              = 29208
MDC_CTXT_GLU_HEALTH                     = 29212
MDC_CTXT_GLU_HEALTH_MINOR               = 29216
MDC_CTXT_GLU_HEALTH_MAJOR               = 29220
MDC_CTXT_GLU_HEALTH_MENSES              = 29224
MDC_CTXT_GLU_HEALTH_STRESS              = 29228
MDC_CTXT_GLU_HEALTH_NONE                = 29232
MDC_CTXT_GLU_SAMPLELOCATION             = 29236
MDC_CTXT_GLU_SAMPLELOCATION_FINGER      = 29240
MDC_CTXT_GLU_SAMPLELOCATION_AST         = 29244
MDC_CTXT_GLU_SAMPLELOCATION_EARLOBE     = 29248
MDC_CTXT_GLU_SAMPLELOCATION_CTRLSOLUTION= 29252
MDC_CTXT_GLU_MEAL                       = 29256
MDC_CTXT_GLU_MEAL_PREPRANDIAL           = 29260
MDC_CTXT_GLU_MEAL_POSTPRANDIAL          = 29264
MDC_CTXT_GLU_MEAL_FASTING               = 29268
MDC_CTXT_GLU_MEAL_BEDTIME               = 29300
MDC_CTXT_GLU_MEAL_CASUAL                = 29272
MDC_CTXT_GLU_TESTER                     = 29276
MDC_CTXT_GLU_TESTER_SELF                = 29280
MDC_CTXT_GLU_TESTER_HCP                 = 29284
MDC_CTXT_GLU_TESTER_LAB                 = 29288

# from the AI partition
MDC_AI_MED_DISPENSED_FIXED      = 13312 # Fixed-dosage dispensed
MDC_AI_MED_DISPENSED_VARIABLE   = 13313 # Variable-dosage dispensed
MDC_AI_MED_STATUS               = 13314 # Medication monitor status
MDC_AI_MED_FEEDBACK             = 13315 # User feedback
MDC_AI_MED_UF_LOCATION          = 13316 # User feedback - location
MDC_AI_MED_UF_RESPONSE          = 13317 # User feedback - response
MDC_AI_MED_UF_TYPE_YESNO        = 13318 # User feedback type – yes/no
MDC_AI_MED_UF_TYPE_1_5          = 13319 # User feedback type – interval 1-5
MDC_AI_MED_UF_TYPE_1_100        = 13320 #User feedback type – interval 1-100

# from The Dimension partition
MDC_DIM_MILLI_L         = 1618 # ml
MDC_DIM_G               = 1728 # g
MDC_DIM_MILLI_G         = 1746 # mg
MDC_DIM_MILLI_G_PER_DL  = 2130 # mg dL-1
MDC_DIM_MILLI_MOLE_PER_L= 4722 # mmol L-1
MDC_DIM_X_INTL_UNIT     = 5472 # i.u.

# from the Communication partition
MDC_DEV_SPEC_PROFILE_PULS_OXIM      = 4100
MDC_DEV_SPEC_PROFILE_BP             = 4103
MDC_DEV_SPEC_PROFILE_TEMP           = 4104
MDC_DEV_SPEC_PROFILE_SCALE          = 4111
MDC_DEV_SPEC_PROFILE_GLUCOSE        = 4113
MDC_DEV_SPEC_PROFILE_HF_CARDIO      = 4137
MDC_DEV_SPEC_PROFILE_HF_STRENGTH    = 4138
MDC_DEV_SPEC_PROFILE_AI_ACTIVITY_HUB= 4167
MDC_DEV_SPEC_PROFILE_AI_MED_MINDER  = 4168

# from objects partition
MDC_MOC_VMO_METRIC      = 4
MDC_MOC_VMO_METRIC_ENUM = 5
MDC_MOC_VMO_METRIC_NU   = 6
MDC_MOC_VMO_METRIC_SA_RT= 9
MDC_MOC_SCAN            = 16
MDC_MOC_SCAN_CFG        = 17
MDC_MOC_SCAN_CFG_EPI    = 18
MDC_MOC_SCAN_CFG_PERI   = 19
MDC_MOC_VMS_MDS_SIMP    = 37
MDC_MOC_VMO_PMSTORE     = 61
MDC_MOC_PM_SEGMENT      = 62

# TrigSegmXferRsp
TSXR_SUCCESSFUL             = 0
TSXR_FAIL_NO_SUCH_SEGMENT   = 1
TSXR_FAIL_SEGM_TRY_LATER    = 2
TSXR_FAIL_SEGM_EMPTY        = 3
TSXR_FAIL_OTHER             = 512

# SegmEvtStatus
SEVTSTA_FIRST_ENTRY         = 0x8000
SEVTSTA_LAST_ENTRY          = 0x4000
SEVTSTA_AGENT_ABORT         = 0x0800
SEVTSTA_MANAGER_CONFIRM     = 0x0080
SEVTSTA_MANAGER_ABORT       = 0x0008


# ProdSpecEntry - spec-type
UNSPECIFIED                 = 0
SERIAL_NUMBER               = 1
PART_NUMBER                 = 2
HW_REVISION                 = 3
SW_REVISION                 = 4
FW_REVISION                 = 5
PROTOCOL_REVISION           = 6
PROD_SPEC_GMDN              = 7

AARQ_CHOSEN                 = 0xE200
AARE_CHOSEN                 = 0xE300
RLRQ_CHOSEN                 = 0xE400
RLRE_CHOSEN                 = 0xE500
ABRT_CHOSEN                 = 0xE600
PRST_CHOSEN                 = 0xE700

class MeasurementStatus(EnumBitRecord):
    """
        MeasurementStatus ::= BITS-16 {
        invalid(0),
        questionable(1),
        not-available(2),
        calibration-ongoing(3),
        test-data(4),
        demo-data(5),
        validated-data(8),
        -- relevant, e.g., in an archive
        early-indication(9),
        -- early estimate of value
        msmt-ongoing(10)
        -- indicates a new measurement is just being taken
        -- (episodic)
    """
    format = ('H',)
    enums  = {
        'invalid'               : 0x8000,
        'questionable'          : 0x4000,
        'not_available'         : 0x2000,
        'calibration_ongoing'   : 0x1000,
        'test_data'             : 0x0800,
        'demo_data'             : 0x0400,
        'validated_data'        : 0x0200,
        'early_indication'      : 0x0100,
        'msmt_ongoing'          : 0x0080,
    }
    map_enum_to_diasend = {'invalid': FLAG_RESULT_ERRONEOUS, 'questionable': None,
        'not_available': None, 'calibration_ongoing': FLAG_CALIBRATION,
        'test_data': None, 'demo_data': None, 'validated_data': None,
        'early_indication': None, 'msmt_ongoing': None
    }


class DataProto(BaseRecord):
    format = ('>HHIHIIIH')
    variables = ('proto_id', 'proto_length', 'version', 'encoding_rules', 'nomenclature_version',
        'functional_units', 'system_type', 'system_id_length')

    def __init__(self, buf):
        super(DataProto, self).__init__(buf)
        self.system_id = buf.get_slice(self.system_id_length)
        (self.dev_config_id,) = buf.get_struct('>H')
        (self.data_req_mode_capab,) = buf.get_struct('>I')
        # Option list... just skip it for now
        (count, length) = buf.get_struct('>HH')
        buf.consume(length)

class Attribute(BaseRecord):
    @classmethod
    def create(cls, buf):
        '''
        Factory : look at the MDC attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        (attr,size) = buf.get_struct('>HH')
        for sub_class in cls.__subclasses__():
            if sub_class.attribute == attr:
                create_op = getattr(sub_class, "create_obj", None)
                if callable(create_op):
                    return create_op(buf, size)
                else:
                    return sub_class(buf, size)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'PHDCRecord : unknown attribute: %d of len: %d' % (attr, size))

class InstNumber(Attribute):
    attribute = MDC_ATTR_ID_INSTNO
    format = ('>H')
    variables = ('instance',)

    def __init__(self, buf, size):
        super(InstNumber, self).__init__(buf)

def parse_float(buf):
    """
        Parses an 11073 float

        The FLOAT-Type data type is defined to represent numeric values that are not integer in type. The
        FLOAT-Type is defined as a 32-bit value with 24-bit mantissa and 8-bit exponent. See F.7 for full
        definition of this data type. This data type is defined as follows:
        --
        -- 32-bit float type; the integer type is a placeholder only
        --
        FLOAT-Type ::= INT-U32
        The 32 bits contain an 8-bit signed exponent to base 10, followed by a 24-bit signed integer (mantissa).
        Special values are assigned to express the following:
                ⎯ NaN (not a number) [exponent 0, mantissa +(2**23 –1) -> 0x007FFFFF]
                ⎯ NRes (not at this resolution) [exponent 0, mantissa –(2**23) -> 0x00800000]
                ⎯ + INFINITY [exponent 0, mantissa +(2**23 –2) -> 0x007FFFFE]
                ⎯ – INFINITY [exponent 0, mantissa –(2**23 –2) -> 0x00800002]
                ⎯ Reserved for future use [exponent 0, mantissa –(2**23–1) -> 0x00800001]
    """
    import struct
    (data,) = buf.get_struct(">4s")
    # First handle the special cases, we map all of them to 0...
    if data == "\x00\x7f\xff\xfe" or \
       data == "\x00\x7f\xff\xff" or \
       data == "\x00\x80\x00\x00" or \
       data == "\x00\x80\x00\x01" or \
       data == "\x00\x80\x00\x02":
        return 0
    else:
        (exponent, ) = struct.unpack("b", data[0])
        (mantissa, ) = struct.unpack(">I", data)
        mantissa = mantissa & 0xffffff

        return mantissa * (10 ** exponent)

def twos_comp(val, bits):
    """compute the 2's compliment of int value val"""
    if val & (1 << (bits - 1)):
        val = val - (1 << bits)
    return val
    
def parse_short_float(buf):
    """
        A.2.10 Short floating point type (SFLOAT-Type) data type
        The short floating point type SFLOAT-Type data type is defined to represent numeric values that are not
        integer in type and have limited resolution. The SFLOAT-Type is defined as a 16-bit value with 12-bit
        mantissa and 4-bit exponent. See Annex F.7 for full definition of this data type. This data type is defined as
        follows:
        --
        -- 16-bit float type; the integer type is a placeholder only
        --
        SFLOAT-Type ::= INT-U16
        The 16–bit value contains a 4-bit exponent to base 10, followed by a 12-bit mantissa. Each is in twos-
        complement form.
        Special values are assigned to express the following:

        * NaN [exponent 0, mantissa +(2**11 –1) -> 0x07FF]
        * NRes [exponent 0, mantissa –(2**11) -> 0x0800]
        * + INFINITY [exponent 0, mantissa +(2**11 –2) -> 0x07FE]
        * – INFINITY [exponent 0, mantissa –(2**11 –2) -> 0x0802]
        * Reserved for future use [exponent 0, mantissa –(2**11 –1) -> 0x0801]
    """
    import struct
    (data,) = buf.get_struct(">2s")
    # First handle the special cases, we map all of them to 0...
    if data == "\x07\xfe" or \
       data == "\x07\xff" or \
       data == "\x08\x00" or \
       data == "\x08\x01" or \
       data == "\x08\x02":
        return 0
    else:
        exponent = twos_comp(ord(data[0]) >> 4, 4)
        (mantissa, ) = struct.unpack(">H", data)
        mantissa = twos_comp(mantissa & 0xfff, 12)
        return mantissa * (10 ** exponent)

class AttrValMapEntry(BaseRecord):
    format = ('>HH')
    variables = ('attr_id', 'attrib_length')

    def parse_buffer(self, buf):
        """
            Parses its value from a buffer
        """
        if self.attr_id == MDC_ATTR_NU_VAL_OBS_SIMP:
            val = parse_float(buf)
        elif self.attr_id == MDC_ATTR_ENUM_OBS_VAL_BASIC_BIT_STR:
            (val,) = buf.get_struct(">H")
        elif self.attr_id == MDC_ATTR_NU_VAL_OBS_BASIC:
            # Sfloat...
            val = parse_short_float(buf)
        elif self.attr_id == MDC_ATTR_MSMT_STAT:
            val = MeasurementStatus(buf)
        elif self.attr_id == MDC_ATTR_ENUM_OBS_VAL_SIMP_OID:
            """
                -- OID type as defined in nomenclature
                -- (do not confuse with ASN.1 OID)
                --
                OID-Type ::= INT-U16 -- 16-bit integer type
            """
            (val,) = buf.get_struct(">H")
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'AttrValMapEntry : Unsupported attrib_id: %d' % self.attr_id)

        return (self.attr_id, val)

class MetricType(BaseRecord):
    format = ('>HH')
    variables = ('partition', 'value')


class SegmEntryElem(BaseRecord):
    format = ('>H', MetricType, '>HHH')
    variables = ('class_id', 'metric_type', 'handle', 'count', 'length')
    def __init__(self, buf):
        super(SegmEntryElem, self).__init__(buf)
        self.attribs = []
        for i in range(self.count):
            self.attribs.append(AttrValMapEntry(buf))

    def parse_buffer(self, buf):
        """
            Parses its value from a buffer
        """
        # For now support one and only one attribute
        values = []
        for attrib in self.attribs:
            values.append(attrib.parse_buffer(buf))
        return values

class ProdSpecnEntry(BaseRecord):
    format= ('>HHH')
    variables = ('type', 'component_id', 'length')

    def __init__(self, buf):
        super(ProdSpecnEntry, self).__init__(buf)
        (self.spec,) = buf.get_struct("%ds" % self.length)

class IdModel(Attribute):
    attribute = MDC_ATTR_ID_MODEL
    format= ('>H')
    variables = ('manufacturer_length',)
    def __init__(self, buf, size):
        super(IdModel, self).__init__(buf)
        (manufacturer, self.model_length) = \
            buf.get_struct('>%dsH' % self.manufacturer_length)

        (model,) = buf.get_struct('%ds' % self.model_length)
        # Strip any null chars or spaces in the end
        self.model = model.rstrip('\x00').rstrip(' ')
        self.manufacturer = manufacturer.rstrip('\x00').rstrip(' ')

class ProdSpecn(Attribute):
    attribute = MDC_ATTR_ID_PROD_SPECN
    format= ('>HH')
    variables = ('count', 'length')
    def __init__(self, buf, size):
        super(ProdSpecn, self).__init__(buf)
        self.entries = []
        for i in range(self.count):
            self.entries.append(ProdSpecnEntry(buf))

    def get_spec_by_type(self, spec_type):
        """
            Find a spec by its type
        """
        for entry in self.entries:
            if entry.type == spec_type:
                return entry.spec.rstrip("\x00")
        return None

    def get_serial(self):
        """
            Get the serial number if available
        """
        # 1 is defined as serial number according to ISO/IEEE 11073-20601:2010(E)
        # A.3.1 MDS attributes
        return self.get_spec_by_type(SERIAL_NUMBER)

    def get_sw_version(self):
        """
            Get the software if available
        """
        # 4 is defined as software version according to ISO/IEEE 11073-20601:2010(E)
        # A.3.1 MDS attributes
        return self.get_spec_by_type(SW_REVISION)

    def get_fw_version(self):
        """
            Get the firmware version if available
        """
        # 5 is defined as firmware version according to ISO/IEEE 11073-20601:2010(E)
        # A.3.1 MDS attributes
        return self.get_spec_by_type(FW_REVISION)

class IdType(Attribute):
    attribute = MDC_ATTR_ID_TYPE
    format = ('>HH')
    variables = ('partition','type')

class SegmEntryHeader(EnumBitRecord):
    format = ('>H',)
    enums  = {
        'absolute_time'         : 0x8000,
        'relative_time'         : 0x4000,
        'hires_relative_time'   : 0x2000
    }

def convert_med_status_to_flags(med_status):
    """
        According to the continua interoperability guidelines the med status
        should be encoded as this: 
        (Table VIII-35 – Adherence monitor encoding – part 1)

        <0 or 1>^medication-not-dispensed-as-expected(0)
        <0 or 1>^medication-dispensed-unexpectedly(1)
        <0 or 1>^medication-unfit(2)
        <0 or 1>^medication-expiration(3)
        <0 or 1>^medication-course-complete(4)
        <0 or 1>^medication-taken-incorrectly(5)
        <0 or 1>^medication-course-reloaded(6)
        <0 or 1>^monitor-tamper(7)
        <0 or 1>^monitor-environmental-exceeded-high(8)
        <0 or 1>^monitor-environmental-exceeded-low(9)
        <0 or 1>^monitor-inoperable(10)
        <0 or 1>^consumer-non-compliant-yellow(11)
        <0 or 1>^consumer-non-compliant-red(12)
    """
    if med_status & (0x8000 >> 0):
        return [FLAG_CANCELED]
    elif not (med_status & (0x8000 >> 4)):
        return [FLAG_CANCELED]
    else:
        return None    

def convert_dev_status_to_flags(dev_status):
    """
        GlucoseDevStat::= BITS-16 {
            device-battery-low(0),
            sensor-malfunction(1),
            sensor-sample-size-insufficient(2),
            sensor-strip-insertion(3),
            sensor-strip-type-incorrect(4),
            sensor-result-too-high(5),
            sensor-result-too-low(6),
            sensor-temp-too-high(7),
            sensor-temp-too-low(8),
            sensor-read-interrupt(9),
            device-gen-fault(10)
        }
    """
    flags = []
    if dev_status & 0x8000:
        flags.append(FLAG_LOW_BATTERY)

    if dev_status & 0x4000:
        flags.append(FLAG_RESULT_ERRONEOUS)

    if dev_status & 0x2000:
        flags.append(FLAG_RESULT_ERRONEOUS)

    if dev_status & 0x1000:
        flags.append(FLAG_STRIP_WARNING)

    if dev_status & 0x0800:
        flags.append(FLAG_STRIP_WARNING)

    if dev_status & 0x0400:
        flags.append(FLAG_RESULT_HIGH)

    if dev_status & 0x0200:
        flags.append(FLAG_RESULT_LOW)

    if dev_status & 0x0100:
        flags.append(FLAG_RESULT_HIGH_TEMP)

    if dev_status & 0x0080:
        flags.append(FLAG_RESULT_LOW_TEMP)

    if dev_status & 0x0040:
        flags.append(FLAG_CANCELED)

    if dev_status & 0x0020:
        flags.append(FLAG_GENERAL)

    if dev_status & 0x001f:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'convert_dev_status_to_flags : dev_status: 0x%05x' % dev_status)

    return flags

class PmSegmentEntryMap(Attribute):
    attribute = MDC_ATTR_PM_SEG_MAP
    format = (SegmEntryHeader, '>HH')
    variables = ('header', 'count', 'length')

    def __init__(self, buf, size):
        super(PmSegmentEntryMap, self).__init__(buf)
        self.entries = []
        for i in range(self.count):
            self.entries.append(SegmEntryElem(buf))

    def print_entries(self):
        for entry in self.entries:
            print repr(entry)
            if isinstance(entry, SegmEntryElem):
                print entry.metric_type

    def get_metric_type(self):
        for entry in self.entries:
            if isinstance(entry, SegmEntryElem):
                return entry.metric_type
        return None

    def has_metric_type(self, id_type):
        m_type = self.get_metric_type()
        if m_type:
            return m_type.partition == id_type.partition and \
                   m_type.value == id_type.type

        return False

    def parse_buffer(self, timestamper, buf):
        """
            Parse a buffer given its definition.
        """
        ret = {}

        (ret[ELEM_TIMESTAMP], uuid, flags) = timestamper.parse_timestamp(self.header, buf)

        if uuid:
            ret[ELEM_VALUE_LIST] = [{ELEM_VAL_TYPE: VALUE_TYPE_UUID,
                ELEM_VAL: uuid}]

        for entry in self.entries:
            mt = entry.metric_type
            values = entry.parse_buffer(buf)
            for (val_type, val) in values:
                if mt.partition == MDC_PART_PHD_AI:
                    if mt.value == MDC_AI_MED_DISPENSED_VARIABLE:
                        ret[ELEM_VAL] = val
                    elif mt.value == MDC_AI_MED_STATUS:
                        status_flags = convert_med_status_to_flags(val)
                        if status_flags:
                            flags.extend(status_flags)
                    elif mt.value >= 0xf000:
                        # private value,
                        # from ISO/IEEE 11073-20601:2010(E), 6.4
                        # Another extension available is to use private (e.g., manufacturer-specific) object attributes and/or methods
                        # for the objects defined in this standard. Private attributes shall be identified by assigning nomenclature
                        # codes from the private numbering space (0xF000 – 0xFFFF) within the corresponding partition as defined
                        # in ISO/IEEE 11073-10101 [B12].
                        pass
                    else:
                        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                            'PmSegmentEntryMap : Unknown value type: %d' % mt.value)
                elif mt.partition == MDC_PART_SCADA:
                    if mt.value == MDC_CONC_GLU_UNDETERMINED_PLASMA or \
                       mt.value == MDC_CONC_GLU_CAPILLARY_PLASMA:
                        if val_type in (MDC_ATTR_NU_VAL_OBS_BASIC, MDC_ATTR_NU_VAL_OBS_SIMP):
                            ret[ELEM_VAL] = val
                    elif mt.value == MDC_CONC_GLU_CONTROL:
                        if val_type in (MDC_ATTR_NU_VAL_OBS_BASIC, MDC_ATTR_NU_VAL_OBS_SIMP):
                            ret[ELEM_VAL] = val
                            flags.append(FLAG_RESULT_CTRL)
                    else:
                        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                            'PmSegmentEntryMap : Unknown value type: %d' % mt.value)
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'PmSegmentEntryMap : Unknown partition: %d' % mt.partition)

        if ELEM_VAL not in ret:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PmSegmentEntryMap : No numeric value found')

        ret[ELEM_FLAG_LIST] = flags
        return ret

    def parse_buffer_flags(self, record_map, timestamper, buf):
        """
            Parse flags from a buffer given its definition.
        """
        ret = {}

        (timestamp, _, _) = timestamper.parse_timestamp(self.header, buf)

        if not timestamp in record_map:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PmSegmentEntryMap : Record missing for flag: %r' % timestamp)
        else:
            record = record_map[timestamp]

        if not ELEM_FLAG_LIST in record:
            record[ELEM_FLAG_LIST] = []

        for entry in self.entries:
            mt = entry.metric_type
            values = entry.parse_buffer(buf)
            for (val_type, val) in values:
                if mt.partition == MDC_PART_PHD_DM:
                    if mt.value == MDC_CTXT_GLU_MEAL:
                        if val_type == MDC_ATTR_ENUM_OBS_VAL_SIMP_OID:
                            if val == MDC_CTXT_GLU_MEAL_PREPRANDIAL:
                                record[ELEM_FLAG_LIST].append(FLAG_BEFORE_MEAL)
                            elif val == MDC_CTXT_GLU_MEAL_POSTPRANDIAL:
                                record[ELEM_FLAG_LIST].append(FLAG_AFTER_MEAL)
                            elif val == MDC_CTXT_GLU_MEAL_FASTING:
                                record[ELEM_FLAG_LIST].append(FLAG_FASTING)
                            elif val == MDC_CTXT_GLU_MEAL_BEDTIME:
                                record[ELEM_FLAG_LIST].append(FLAG_BEFORE_SLEEP)
#                            elif val == MDC_CTXT_GLU_MEAL_CASUAL:
                                # TODO: Unhandled
#                                pass
                        else:
                            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                                'PmSegmentEntryMap : Unknown value type: %d' % val_type)
                    elif mt.value == MDC_GLU_METER_DEV_STATUS:
                        if val_type == MDC_ATTR_ENUM_OBS_VAL_BASIC_BIT_STR:
                            record[ELEM_FLAG_LIST].extend(convert_dev_status_to_flags(val))
                        else:
                            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                                'PmSegmentEntryMap : Unknown value type: %d' % val_type)
                    else:
                        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                            'PmSegmentEntryMap : Unknown value type: %d' % mt.value)
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'PmSegmentEntryMap : Unknown partition: %d' % mt.partition)

class OperationalState(Attribute):
    attribute = MDC_ATTR_OP_STAT
    format = ('>H')
    variables = ('state',)

class AttributeValueMap(Attribute):
    attribute = MDC_ATTR_ATTRIBUTE_VAL_MAP
    format = ('>HH')
    variables = ('count', 'size')
    def __init__(self, buf, size):
        super(AttributeValueMap, self).__init__(buf)
        self.attribs = []
        for i in range(self.count):
            self.attribs.append(AttrValMapEntry(buf))

class NuValObsSimple(Attribute):
    attribute = MDC_ATTR_NU_VAL_OBS_SIMP
    format = ('>f')
    variables = ('value',)

class PmStoreLabelString(Attribute):
    attribute = MDC_ATTR_PM_STORE_LABEL_STRING
    format = ('>H')
    variables = ('length',)

    def __init__(self, buf, size):
        super(PmStoreLabelString, self).__init__(buf)
        (self.string,) = buf.get_struct('%ds' % self.length)

class PmSegLabelString(Attribute):
    attribute = MDC_ATTR_PM_SEG_LABEL_STRING
    format = ('>H')
    variables = ('length',)

    def __init__(self, buf, size):
        super(PmSegLabelString, self).__init__(buf)
        (self.string,) = buf.get_struct('%ds' % self.length)

class TypeVer(BaseRecord):
    format = ('>HH')
    variables = ('type', 'version')

class SysTypeSpecList(Attribute):
    attribute = MDC_ATTR_SYS_TYPE_SPEC_LIST
    format = ('>HH')
    variables = ('count', 'length')

    def __init__(self, buf, size):
        super(SysTypeSpecList, self).__init__(buf)
        self.types = []
        for i in range(self.count):
            self.types.append(TypeVer(buf))

    def supports(self, spec, version):
        """
            Checks if a given spec is supported
        """
        for spec_type in self.types:
            if spec_type.type == spec and spec_type.version == version:
                return True
        return False

    def get_specs(self):
        specs = []
        for spec_type in self.types:
            specs.append(spec_type.type)
        return specs

class AbsoluteTimeAdjust(Attribute):
    attribute = MDC_ATTR_TIME_ABS_ADJUST
    format = ('>6s')
    variables = ('value',)


class PmNumSegments(Attribute):
    attribute = MDC_ATTR_NUM_SEG
    format = ('>H')
    variables = ('count',)

class SystemId(Attribute):
    attribute = MDC_ATTR_SYS_ID
    format = ('>H')
    variables = ('length',)

    def __init__(self, buf, size):
        super(SystemId, self).__init__(buf)
        (self.data,) = buf.get_struct('%ds' % self.length)

class TimeAbsolute(Attribute):
    attribute = MDC_ATTR_TIME_ABS
    format = ('>BBBBBBBB')
    variables = ('century', 'year', 'month', 'day', 'hour', 'minute', 'second',
                 'sec_fractions')

    def get_datetime(self):
        """
        A.2.13 Absolute time data type
        The absolute time data type specifies the time of day with a resolution of 1/100 of a second. The hour field
        shall be reported in 24-hr time notion (i.e., from 0 to 23). The values in the structure shall be encoded using
        binary coded decimal (i.e., 4-bit nibbles). For example, the year 1996 shall be represented by the
        hexadecimal value 0x19 in the century field and the hexadecimal value 0x96 in the year field. This format
        is easily converted to character- or integer-based representations. The absolute time data type is defined as
        follows:
        """
        time_str = "%02x%02x-%02x-%02x %02x:%02x:%02x.%02x0000" % \
            (self.century, self.year, self.month, self.day, self.hour,
             self.minute, self.second, self.sec_fractions)

        return datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S.%f")


class TimeEndSeg(Attribute):
    attribute = MDC_ATTR_TIME_END_SEG
    format = ('>BBBBBBBB')
    variables = ('century', 'year', 'month', 'day', 'hour', 'minute', 'second',
                 'sec-fractions')

class TimeRelative(Attribute):
    attribute = MDC_ATTR_TIME_REL
    format = ('>I')
    variables = ('time',)

class TimeStartSeg(Attribute):
    attribute = MDC_ATTR_TIME_START_SEG
    format = ('>BBBBBBBB')
    variables = ('century', 'year', 'month', 'day', 'hour', 'minute', 'second',
                 'sec-fractions')

class PmSegUsageCount(Attribute):
    attribute = MDC_ATTR_SEG_USAGE_CNT
    format = ('>I')
    variables = ('count',)

class UnitCode(Attribute):
    attribute = MDC_ATTR_UNIT_CODE
    format = ('>H')
    variables = ('code',)

class ClearTimeout(Attribute):
    attribute = MDC_ATTR_CLEAR_TIMEOUT
    format = ('>I')
    variables = ('timeout',)

class TransferTimeout(Attribute):
    attribute = MDC_ATTR_TRANSFER_TIMEOUT
    format = ('>I')
    variables = ('timeout',)

class DevConfigId(Attribute):
    attribute = MDC_ATTR_DEV_CONFIG_ID
    format = ('>H')
    variables = ('config_id',)

class MdsTimeCapState(EnumBitRecord):
    format = ('>H',)
    enums  = {
        'real_time_clock'               : 0x8000,
        'set_clock'                     : 0x4000,
        'relative_time'                 : 0x2000,
        'high_res_relative_time'        : 0x1000,
        'sync_abs_time'                 : 0x0800,
        'sync_rel_time'                 : 0x0400,
        'sync_hi_res_relative_time'     : 0x0200,
        'abs_time_synced'               : 0x0100,
        'rel_time_synced'               : 0x0080,
        'hi_res_relative_time_synced'   : 0x0040,
        'set_time'                      : 0x0020
    }

class MdsTimeInfo(Attribute):
    attribute = MDC_ATTR_MDS_TIME_INFO
    format = (MdsTimeCapState, '>HIHHI')
    variables = ('cap_state', 'sync_protocol', 'sync_accuracy',
        'resolution_abs', 'resolution_rel', 'resolution_highres')

class MetricSpecSmall(Attribute):
    attribute = MDC_ATTR_METRIC_SPEC_SMALL
    format = ('>H')
    variables = ('spec',)

class RegCert(BaseRecord):
    format = ('>BBH')
    variables = ('auth_body', 'auth_struct_type', 'length')

    def __init__(self, buf):
        super(RegCert, self).__init__(buf)
        (self.data,) = buf.get_struct("%ds" % self.length)
    

class RegCertDataList(Attribute):
    attribute = MDC_ATTR_REG_CERT_DATA_LIST
    format = ('>HH')
    variables = ('count', 'length')

    def __init__(self, buf, size):
        super(RegCertDataList, self).__init__(buf)
        self.certs = []
        for i in range(self.count):
            self.certs.append(RegCert(buf))

class PmStoreCapabilities(Attribute):
    attribute = MDC_ATTR_PM_STORE_CAPAB
    format = ('>H')
    variables = ('capabilities',)

class MetricStoreSampleAlg(Attribute):
    attribute = MDC_ATTR_METRIC_STORE_SAMPLE_ALG
    format = ('>H')
    variables = ('algorithm',)

class MetricStoreUsageCount(Attribute):
    attribute = MDC_ATTR_METRIC_STORE_USAGE_CNT
    format = ('>I')
    variables = ('count',)

class MetricStoreCapacity(Attribute):
    attribute = MDC_ATTR_METRIC_STORE_CAPAC_CNT
    format = ('>I')
    variables = ('capacity',)

class SegmentInfo(BaseRecord):
    format = ('>HHH')
    variables = ('instance', 'count', 'length')

    def __init__(self, buf):
        super(SegmentInfo, self).__init__(buf)
        self.attribs = []
        for i in range(self.count):
            self.attribs.append(Attribute.create(buf))

    def get_usage_count(self):
        count = self.get_attribute_by_class(PmSegUsageCount)
        if count:
            return count.count
        else:
            return 0

    def get_label(self):
        label = self.get_attribute_by_class(PmSegLabelString)
        if label:
            return label.string
        else:
            return None

    def get_attribute_by_class(self, klass):
        """
            Find a given attribute by its ID
        """
        for attrib in self.attribs:
            if isinstance(attrib, klass):
                return attrib
        return None

    def print_attribs(self):
        for attrib in self.attribs:
            print repr(attrib)
            if isinstance(attrib, PmSegLabelString):
                print attrib.string
            elif isinstance(attrib, InstNumber):
                print attrib.instance

class PHDCRecord(BaseRecord):
    '''
    Base class for all PHDC records.

    This class also contains a factory method for creating
    instances of the sub-classes (depending on the currently extracted
    message type).

    '''
    @classmethod
    def create(cls, buf):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        (choice,length) = buf.get_struct('>HH')
        body = buf.get_sub_buffer(length)
        for sub_class in cls.__subclasses__():
            if sub_class.choice == choice:
                create_op = getattr(sub_class, "create_obj", None)
                if callable(create_op):
                    return create_op(body)
                else:
                    return sub_class(body)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'PHDCRecord : unknown message choice 0x%04x' % (choice))

    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'PHDCRecord : Subclass did not implement convert')

class AarqApdu(PHDCRecord):
    choice = AARQ_CHOSEN
    format = ('>I',)
    variables = ('assoc_version')

    def __init__(self, buf):
        super(AarqApdu, self).__init__(buf)
        self.data_proto_list = []
        (count, length) = buf.get_struct('>HH')
        for i in range(count):
            self.data_proto_list.append(DataProto(buf))

class RlreApdu(PHDCRecord):
    choice = RLRE_CHOSEN
    format = ('>H',)
    variables = ('reason')

class RlrqApdu(PHDCRecord):
    choice = RLRQ_CHOSEN
    format = ('>H',)
    variables = ('reason')

class PrstApdu(PHDCRecord):
    choice = PRST_CHOSEN

    @classmethod
    def create_obj(cls, buf):
        """
            Creates a instance of a RecordLogEntry subclass
        """
        (length, invoke_id, choice) = buf.get_struct(">HHH")

        for sub_class in cls.__subclasses__():
            if sub_class.choice == choice:
                return sub_class(buf)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'PrstApdu : Unknown choice: 0x%04x' % choice)

class Event(BaseRecord):
    @classmethod
    def create(cls, buf):
        '''
        Factory : look at the MDC attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        (event, size) = buf.get_struct('>HH')
        for sub_class in cls.__subclasses__():
            if sub_class.event == event:
                create_op = getattr(sub_class, "create_obj", None)
                if callable(create_op):
                    return create_op(buf, size)
                else:
                    return sub_class(buf, size)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'PHDCRecord : unknown event: %d' % (event))

class ConfigObject(BaseRecord):
    format = ('>HHHH')
    variables = ('obj_class', 'obj_handle', 'attrib_count', 'attrib_len')

    def __init__(self, buf):
        super(ConfigObject, self).__init__(buf)
        self.attribs = []
        for i in range(self.attrib_count):
            self.attribs.append(Attribute.create(buf))

    def get_attribute_by_class(self, klass):
        """
            Find a given attribute by its ID
        """
        for attrib in self.attribs:
            if isinstance(attrib, klass):
                return attrib
        return None

    def print_attribs(self):
        for attrib in self.attribs:
            print repr(attrib)

class ConfigReport(Event):
    event = MDC_NOTI_CONFIG
    format = ('>HHH')
    variables = ('config_id', 'config_count', 'config_length')

    def __init__(self, buf, size):
        super(ConfigReport, self).__init__(buf)
        self.objects = []
        for i in range(self.config_count):
            self.objects.append(ConfigObject(buf))

    def get_object_by_class(self, klass):
        """
            Find a config object for a given class
        """
        for obj in self.objects:
            if obj.obj_class == klass:
                return obj
        return None

    def get_object_by_handle(self, handle):
        """
            Find a config object given handle
        """
        for obj in self.objects:
            if obj.obj_handle == handle:
                return obj
        return None

    def get_object_by_metric_type(self, partition, metric_type):
        """
            Find a config object by its metric type
        """
        for obj in self.objects:
            id_type = obj.get_attribute_by_class(IdType)
            if id_type and id_type.partition == partition and \
               id_type.type == metric_type:
                return obj
        return None

    def print_objects(self):
        """
            Debug print objects
        """
        for obj in self.objects:
            print "Object, handle: %d, class: %d" % (obj.obj_handle, obj.obj_class)

class SegmentDataResult(Event):
    event = MDC_NOTI_SEGMENT_DATA
    format = ('>HIIHH')
    variables = ('instance', 'index', 'count', 'status', 'length')

    def __init__(self, buf, size):
        super(SegmentDataResult, self).__init__(buf)
        self.data = buf.get_sub_buffer(self.length)

class RoivCmipConfirmedEventReport(PrstApdu):
    choice = 0x0101
    format = ('>HHI',)
    variables = ('length', 'obj_handle', 'event_time')

    def __init__(self, buf):
        super(RoivCmipConfirmedEventReport, self).__init__(buf)
        self.event = Event.create(buf)


class RorsCmipGet(PrstApdu):
    choice = 0x0203
    format = (">HHHH")
    variables = ('length', 'obj_handle', 'attrib_count', 'attrib_len')

    def __init__(self, buf):
        super(RorsCmipGet, self).__init__(buf)
        self.attribs = []
        for i in range(self.attrib_count):
            self.attribs.append(Attribute.create(buf))

    def get_attribute_by_class(self, klass):
        for attrib in self.attribs:
            if isinstance(attrib, klass):
                return attrib
        return None

    def print_attribs(self):
        for attrib in self.attribs:
            print repr(attrib)

class Action(BaseRecord):
    @classmethod
    def create(cls, buf):
        '''
        Factory : look at the MDC attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        (action,size) = buf.get_struct('>HH')
        for sub_class in cls.__subclasses__():
            if sub_class.action == action:
                create_op = getattr(sub_class, "create_obj", None)
                if callable(create_op):
                    return create_op(buf, size)
                else:
                    return sub_class(buf, size)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'PHDCRecord : unknown action: %d' % (action))

class SegmentGetInfoRsp(Action):
    action = MDC_ACT_SEG_GET_INFO
    format = ('>HH')
    variables = ('count', 'length')

    def __init__(self, buf, size):
        super(SegmentGetInfoRsp, self).__init__(buf)
        self.segments = []
        for i in range(self.count):
            self.segments.append(SegmentInfo(buf))

class TrigXferRsp(Action):
    action = MDC_ACT_SEG_TRIG_XFER
    format = ('>HH')
    variables = ('instance', 'status')

class RorsCmipConfirmedAction(PrstApdu):
    choice = 0x0207
    format = (">HH")
    variables = ('length', 'obj_handle')

    def __init__(self, buf):
        super(RorsCmipConfirmedAction, self).__init__(buf)
        self.action = Action.create(buf)

# --- helper to handle time conversion for segment data

class Timestamper(object):
    def __init__(self, time_info):
        self.__current_segment_time = None
        # The bit fields are defined in this strange opposite order
        if time_info.cap_state.bits['relative_time']:
            self.__relative_time = True
            self.__125us_per_tick = time_info.resolution_rel
            self.__now_dt = datetime.now()
        elif time_info.cap_state.bits['real_time_clock']:
            self.__relative_time = False
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : Unsupported timestamping %r' % \
                time_info.cap_state.bits)

    def supports_relative(self):
        return self.__relative_time

    def set_current_segment(self, report):
        """
            Indicate which segment being currently handled, this is needed
            to calculate relative timestamps for instance... 
        """
        self.__current_segment_time = report.event_time

    def parse_timestamp(self, segment_header, buf):

        relative_time = -1
        for key in segment_header.bits:
            if segment_header.bits[key]:
                if key == 'relative_time' and self.supports_relative():
                    return self.parse_relative_timestamp(buf)
                elif key == 'absolute_time' and not self.supports_relative():
                    return self.parse_absolute_timestamp(buf)
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'PmSegmentEntryMap : Unsupported timestamp: %s' % key)

    def parse_relative_timestamp(self, buf):

        (timestamp,) = buf.get_struct(">I")

        diff_rel = self.__current_segment_time - timestamp
        delta = timedelta(microseconds = diff_rel * self.__125us_per_tick * 125)

        # Returning timestamp, uuid, flags
        return (self.__now_dt - delta, timestamp, [FLAG_TIMESTAMP_IN_SERVER_TIMEZONE])

    def parse_absolute_timestamp(self, buf):
        timestamp = TimeAbsolute(buf)

        # Returning timestamp, uuid, flags
        return (timestamp.get_datetime(), None, [])

# --- controller

# Well standards are standards, which can be interpreted differently...
# obviously some manufacturers handles various things differently, to cope
# with this we have a quirks structure
def quirk_accu_chek_connect_get_serial(id_model, prod_spec):
    """
        Roche's serial number is a combination of the model number and serial
        number. Also their serial number entry is prepended by a
        "serial-number: "-string
    """
    return id_model.model + prod_spec.get_serial().split(' ')[1]

def quirk_accu_check_active_get_serial(id_model, prod_spec):
    """
        Roche's serial number is a combination of the model number and serial
        number. Also their serial number entry is prepended by a
        "serial-number: "-string
        Also the serial number contains one more 0 than on the device

        But we return the number with the additional 0
    """
    return prod_spec.get_serial().split(' ')[1]

def quirk_accu_check_mobile_get_serial(id_model, prod_spec):
    """
        Roche's serial number is a combination of the model number and serial
        number. Also their serial number entry is prepended by a
        "serial-number: "-string
        Also the serial number contains one more 0 than on the device
    """
    return "U1" + prod_spec.get_serial().split(' ')[1][1:]

QUIRK_ACCU_CHEK_AVIVA_CONNECT = {
    "model": "Accu-Chek Aviva Connect",
    "get_serial" : quirk_accu_chek_connect_get_serial
}

QUIRK_ACCU_CHEK_PERFORMA_CONNECT = {
    "model": "Accu-Chek Performa Connect",
    "get_serial" : quirk_accu_chek_connect_get_serial
}

# In theory we support any device implementing medication monitor or
# the glucose specialisation, but we need to reject the data until verified
# working, so this table contains supported devices..
# The device might have a quirks dict making it possible to have
# give special treatment to some devices 
DEVICE_TABLE = {
    "Roche": {
        # The Accu-Check Aviva Connect
        "483": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "484": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "497": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "498": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "499": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "500": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "502": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        "685": QUIRK_ACCU_CHEK_AVIVA_CONNECT,
        # The Accu-Check Performa Connect (Same family as Aviva Connect)
        "479": QUIRK_ACCU_CHEK_PERFORMA_CONNECT,
        "501": QUIRK_ACCU_CHEK_PERFORMA_CONNECT,
        "503": QUIRK_ACCU_CHEK_PERFORMA_CONNECT,
        "765": QUIRK_ACCU_CHEK_PERFORMA_CONNECT,
        # The Accu-Check Mobile
        "1205": {
            "model": "Accu-Chek Mobile U1",
            "get_serial": quirk_accu_check_mobile_get_serial
        },
        # The Accu-Check Active
        "2102": {
            "model": "Accu-Chek Active USB",
            "get_serial" : quirk_accu_check_active_get_serial
        },
    },
    "Novo Nordisk A/S": {
        #Novopen 5
        "NP5 LCM": {
            "model": "NovoPen 5"
        },
        # NovoPen Echo
        "NP ECHO LCM": {
            "model": "NovoPen Echo"
        },
        # Suddenly they changed name, unclear which model it reflects...
        "NovoPen": {
        },
    }
}

class PHDCDataExtractor(object):
    def __init__(self, data):
        self.__data   = data
        self.__prod_spec = None
        self.__id_model = None
        self.__type = None
        self.__quirks = None

    def get_spec(self):
        return self.__prod_spec

    def get_serial(self):
        if self.__quirks and "get_serial" in self.__quirks and \
           self.__id_model and self.get_spec():
            return self.__quirks["get_serial"](self.__id_model, self.get_spec())
        elif self.get_spec():
            return self.get_spec().get_serial()
        else:
            return "UNKNOWN"

    def get_model(self):
        if self.__quirks and "model" in self.__quirks:
            return self.__quirks["model"]
        elif self.__id_model:
            return self.__id_model.model
        else:
            return "UNKNOWN PHDC"

    def get_type(self):
        return self.__type

    def set_id_model(self, id_model):
        """
            Save the IdModel for future use and also initialise the
            quirks depending on the manufacturer/model pair
        """
        self.__id_model = id_model
        # We need to check if the device is in the device table
        # the quirk might be None so we need an additional variable..
        found_in_device_table = False
        if id_model.manufacturer in DEVICE_TABLE:
            if id_model.model in DEVICE_TABLE[id_model.manufacturer]:
                found_in_device_table = True
                self.__quirks = DEVICE_TABLE[id_model.manufacturer][id_model.model]

        if not found_in_device_table:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : unsupported Manufacturer/model: %s, type: %s' % \
                (id_model.manufacturer, id_model.model))

    @staticmethod
    def get_config_report(apdus):
        # Find the config report among the apdus
        for apdu in apdus:
            if isinstance(apdu, RoivCmipConfirmedEventReport):
                if isinstance(apdu.event, ConfigReport):
                    return apdu.event
        return None

    @staticmethod
    def get_segment_info_rsp(apdus, handle):
        # Find the config report among the apdus
        for apdu in apdus:
            if isinstance(apdu, RorsCmipConfirmedAction) and \
               apdu.obj_handle == handle and \
               isinstance(apdu.action, SegmentGetInfoRsp):
                return apdu.action
        return None

    @staticmethod
    def get_trig_xfer_rsp(apdus, handle, instance):
        # Find the config report among the apdus
        for apdu in apdus:
            if isinstance(apdu, RorsCmipConfirmedAction) and \
               apdu.obj_handle == handle and \
               isinstance(apdu.action, TrigXferRsp) and \
               apdu.action.instance == instance:
                return apdu.action
        return None

    @staticmethod
    def get_apdu_by_class(apdus, klass):
        for apdu in apdus:
            if isinstance(apdu, klass):
                return apdu
        return None

    @staticmethod
    def get_segment_data_report(apdus, obj_handle, instance, index):
        """
            Get a segment instance for a given object
        """
        for apdu in apdus:
            if isinstance(apdu, RoivCmipConfirmedEventReport) and \
               apdu.obj_handle == obj_handle and \
               isinstance(apdu.event, SegmentDataResult):
                result = apdu.event
                if result.instance == instance and result.index == index:
                    return apdu
        return None

    def split(self):
        # The buffer contains of IEEE-11073 packets received from the device.
        # All but the first record has the timestamp and CRC thrown away
        # The CRC is validated on the device and the timestamp is just
        # interesting once if we want to check the datetime of the device
        # this saves us 6 bytes * the number of records which are up to 15.000
        buf = Buffer(self.__data)

        # First parse the 11073 pdus into objects
        apdus = []

        while buf.nr_bytes_left() > 0:
            record = PHDCRecord.create(buf)
            apdus.append(record)

        # Check the MDS (Medical Device System)
        mds = self.get_apdu_by_class(apdus, RorsCmipGet)
        timestamper = Timestamper(mds.get_attribute_by_class(MdsTimeInfo))
        # According to ISO/IEEE 11073-10472:2012(E)
        # Table 2 —MDS object attributes
        # The object handle for the MDS is 0
        if mds.obj_handle != 0:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : Invalid object handle: %d' % mds.obj_handle)

        spec = mds.get_attribute_by_class(SysTypeSpecList)
        if spec.supports(MDC_DEV_SPEC_PROFILE_AI_MED_MINDER, 1):
            self.__type = MDC_DEV_SPEC_PROFILE_AI_MED_MINDER
        elif spec.supports(MDC_DEV_SPEC_PROFILE_GLUCOSE, 2):
            self.__type = MDC_DEV_SPEC_PROFILE_GLUCOSE
        elif spec.supports(MDC_DEV_SPEC_PROFILE_GLUCOSE, 1):
            self.__type = MDC_DEV_SPEC_PROFILE_GLUCOSE
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : No known profile is unsupported %r' % \
                spec.get_specs())

        diasend_records = []

        self.set_id_model(mds.get_attribute_by_class(IdModel))

        self.__prod_spec = mds.get_attribute_by_class(ProdSpecn)

        diasend_records.append({ELEM_DEVICE_SERIAL: self.get_serial()})
        # For now only pens and meters are suppported...
        if self.get_type() == MDC_DEV_SPEC_PROFILE_AI_MED_MINDER:
            diasend_records.append({ELEM_DEVICE_CLASS: DEVICE_PEN})
        elif self.get_type() == MDC_DEV_SPEC_PROFILE_GLUCOSE:
            diasend_records.append({ELEM_DEVICE_CLASS: DEVICE_METER})
        else:
            # Should not happen, guarded above
            pass

        diasend_records.append({ELEM_DEVICE_MODEL: self.get_model()})

        sw_version = self.get_spec().get_sw_version()
        if not sw_version:
            sw_version = self.get_spec().get_fw_version()
        diasend_records.append({ELEM_SW_VERSION: sw_version})

        if self.get_type() == MDC_DEV_SPEC_PROFILE_AI_MED_MINDER:
            diasend_records.extend(self.handle_pen(apdus, timestamper))
        elif self.get_type() == MDC_DEV_SPEC_PROFILE_GLUCOSE:
            diasend_records.extend(self.handle_meter(apdus, timestamper))

        return diasend_records

    def handle_meter(self, apdus, timestamper):
        diasend_records = []
        settings = []

        # Ok we got the easy parts, now its time to decode the actual data...
        config_report = self.get_config_report(apdus)
        # According to: IEEE Std 11073-10417-2011
        # Table 7 —Blood glucose numeric object attributes has object ID 1
        glucose_numeric = config_report.get_object_by_handle(1)

        id_type = glucose_numeric.get_attribute_by_class(IdType)
        metric_type = None
        if id_type.partition == MDC_PART_SCADA:
            if id_type.type == MDC_CONC_GLU_CAPILLARY_PLASMA or \
               id_type.type == MDC_CONC_GLU_UNDETERMINED_PLASMA:
                metric_type = id_type

        if not metric_type:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : unsupported partition: %d, type: %d' % \
                (id_type.partition, id_type.type))

        unit_code = glucose_numeric.get_attribute_by_class(UnitCode)

        if unit_code.code == MDC_DIM_MILLI_G_PER_DL:
            diasend_records.append({ ELEM_DEVICE_UNIT: "mg/dL" })
            settings = {SETTINGS_LIST: { SETTING_BG_UNIT: SETTING_BG_UNIT_MMOLL } }
            val_factor = VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL		
        elif unit_code.code == MDC_DIM_MILLI_MOLE_PER_L:
            diasend_records.append({ ELEM_DEVICE_UNIT: "mmol/l" })
            settings = {SETTINGS_LIST: { SETTING_BG_UNIT: SETTING_BG_UNIT_MMOLL } }
            val_factor = VAL_FACTOR_GLUCOSE_MMOL
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : unsupported unit: %d' % unit_code.code)

        total_count, gluc_records = self.create_records(apdus, metric_type,
            timestamper, VALUE_TYPE_GLUCOSE, val_factor)

        # Find control records...
        ctrl = config_report.get_object_by_metric_type(MDC_PART_SCADA, MDC_CONC_GLU_CONTROL)
        if ctrl:
            id_type = ctrl.get_attribute_by_class(IdType)
            ctrl_count, ctrl_records = self.create_records(apdus, id_type,
                timestamper, VALUE_TYPE_GLUCOSE, val_factor)
            gluc_records.extend(ctrl_records)
            total_count += ctrl_count

        record_map = {}
        for record in gluc_records:
            record_map[record[ELEM_TIMESTAMP]] = record

        # Add meal flags
        meal = config_report.get_object_by_metric_type(MDC_PART_PHD_DM, MDC_CTXT_GLU_MEAL)
        if meal:
            id_type = meal.get_attribute_by_class(IdType)
            self.add_flags(apdus, record_map, id_type, timestamper)

        # Add status flags
        status = config_report.get_object_by_metric_type(MDC_PART_PHD_DM, MDC_GLU_METER_DEV_STATUS)
        if status:
            id_type = status.get_attribute_by_class(IdType)
            self.add_flags(apdus, record_map, id_type, timestamper)

        if len(settings) > 0:
            gluc_records.append(settings)
            total_count = total_count + 1
  
        diasend_records.extend(gluc_records)
        diasend_records.append({ELEM_NR_RESULTS: total_count})

        return diasend_records

    def handle_pen(self, apdus, timestamper):
        diasend_records = []

        # Ok we got the easy parts, now its time to decode the actual data...
        config_report = self.get_config_report(apdus)
        # According to: ISO/IEEE 11073-10472:2012(E)
        # Table 7 —Variable-dosage medication dispensed numeric object attribute
        # has object handle 2
        variable_dispensed = config_report.get_object_by_handle(2)

        id_type = variable_dispensed.get_attribute_by_class(IdType)

        if id_type.partition != MDC_PART_PHD_AI or \
           id_type.type != MDC_AI_MED_DISPENSED_VARIABLE:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : unsupported partition: %d, type: %d' % \
                (id_type.partition, id_type.type))

        unit_code = variable_dispensed.get_attribute_by_class(UnitCode)

        if unit_code.code == MDC_DIM_X_INTL_UNIT:
            diasend_records.append({ ELEM_DEVICE_UNIT: "U" })
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : unsupported unit: %d' % unit_code.code)

        segment_count, records = self.create_records(apdus, id_type,
            timestamper, VALUE_TYPE_INS_BOLUS_BASAL, VAL_FACTOR_BOLUS)
    
        diasend_records.extend(records)
        diasend_records.append({ELEM_NR_RESULTS: segment_count})

        return diasend_records

    def print_pm_store(self, apdus):
        print "PM STORE:"
        config_report = self.get_config_report(apdus)
        pm_store_cfg = config_report.get_object_by_class(MDC_MOC_VMO_PMSTORE)

        pm_store = self.get_segment_info_rsp(apdus, pm_store_cfg.obj_handle)
        for seg in pm_store.segments:
            seg_map = seg.get_attribute_by_class(PmSegmentEntryMap)

            m_type = seg_map.get_metric_type()
            print "Segment: %s: %d, type: %s,%d" % (seg.get_label(),
                seg.get_usage_count(), m_type.partition, m_type.value)

    def find_segment(self, apdus, metric_type):
        """
            Find a segment in the PM store by its metric type
        """
        config_report = self.get_config_report(apdus)
        pm_store_cfg = config_report.get_object_by_class(MDC_MOC_VMO_PMSTORE)
        pm_store = self.get_segment_info_rsp(apdus, pm_store_cfg.obj_handle)
        for seg in pm_store.segments:
            seg_map = seg.get_attribute_by_class(PmSegmentEntryMap)
            if seg_map.has_metric_type(metric_type):
                return seg

        return None

    def create_records(self, apdus, metric_type, timestamper, val_type, val_factor = 1):
        diasend_records = []
        segment = self.find_segment(apdus, metric_type)

        config_report = self.get_config_report(apdus)
        pm_store_cfg = config_report.get_object_by_class(MDC_MOC_VMO_PMSTORE)

        xfer_rsp = self.get_trig_xfer_rsp(apdus, pm_store_cfg.obj_handle,
                                          segment.instance)
        if xfer_rsp.status == TSXR_SUCCESSFUL:
            seg_map = segment.get_attribute_by_class(PmSegmentEntryMap)
            # Get the actual segment
            next_index = 0
            while next_index >= 0:
                report = self.get_segment_data_report(apdus, pm_store_cfg.obj_handle, segment.instance, next_index)
                timestamper.set_current_segment(report)

                segment_data = report.event

                buf = segment_data.data
                for i in range(segment_data.count):
                    rec = seg_map.parse_buffer(timestamper, buf)
                    if rec:
                        rec[ELEM_VAL_TYPE] = val_type
                        rec[ELEM_VAL] = rec[ELEM_VAL] * val_factor
                        diasend_records.append(rec)
                # sevtsta-last-entry(1),
                if segment_data.status & TSXR_FAIL_SEGM_EMPTY or \
                   segment_data.status & SEVTSTA_MANAGER_ABORT:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'PHDCDataExtractor : Invalid segment status: 0x%04x' % \
                        segment_data.status)
                elif not (segment_data.status & SEVTSTA_LAST_ENTRY):
                    next_index += segment_data.count
                else:
                    # terminate
                    next_index = -1
        elif xfer_rsp.status != TSXR_FAIL_SEGM_EMPTY:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : Xfer failed: %d' % xfer_rsp.status)

        return (segment.get_usage_count(), diasend_records)

    def add_flags(self, apdus, record_map, metric_type, timestamper):
        """
            Add flags from a given object in the metric store...
        """
        segment = self.find_segment(apdus, metric_type)

        config_report = self.get_config_report(apdus)
        pm_store_cfg = config_report.get_object_by_class(MDC_MOC_VMO_PMSTORE)
        xfer_rsp = self.get_trig_xfer_rsp(apdus, pm_store_cfg.obj_handle,
                                          segment.instance)

        if xfer_rsp.status == TSXR_SUCCESSFUL:
            seg_map = segment.get_attribute_by_class(PmSegmentEntryMap)

            next_index = 0
            while next_index >= 0:
                report = self.get_segment_data_report(apdus,
                    pm_store_cfg.obj_handle, segment.instance, next_index)

                timestamper.set_current_segment(report)

                segment_data = report.event

                for i in range(segment_data.count):
                    seg_map.parse_buffer_flags(record_map, timestamper, segment_data.data)

                # sevtsta-last-entry(1),
                if segment_data.status & TSXR_FAIL_SEGM_EMPTY or \
                   segment_data.status & SEVTSTA_MANAGER_ABORT:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'PHDCDataExtractor : Invalid segment status: 0x%04x' % \
                        segment_data.status)
                elif not (segment_data.status & SEVTSTA_LAST_ENTRY):
                    next_index += segment_data.count
                else:
                    # terminate
                    next_index = -1
        elif xfer_rsp.status != TSXR_FAIL_SEGM_EMPTY:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'PHDCDataExtractor : Xfer failed: %d' % xfer_rsp.status)

    @staticmethod
    def eval_record(record, key):
        if key in record:
            return record

        return {}


    @staticmethod
    def eval_serial_record(record):
        return PHDCDataExtractor.eval_record(record, ELEM_DEVICE_SERIAL)

    @staticmethod
    def eval_device_class_record(record):
        return PHDCDataExtractor.eval_record(record, ELEM_DEVICE_CLASS)

    @staticmethod
    def eval_device_model_record(record):
        return PHDCDataExtractor.eval_record(record, ELEM_DEVICE_MODEL)

    @staticmethod
    def eval_device_version(record):
        return PHDCDataExtractor.eval_record(record, ELEM_SW_VERSION)

    @staticmethod
    def eval_nr_results(record):
        return PHDCDataExtractor.eval_record(record, ELEM_NR_RESULTS)

    @staticmethod
    def eval_result_record(record):
        settings = PHDCDataExtractor.eval_record(record, SETTINGS_LIST)
        values = PHDCDataExtractor.eval_record(record, ELEM_VAL_TYPE)
        if len(settings) > 0:
            values.update(settings)
        return values

    @staticmethod
    def eval_unit_record(record):
        return PHDCDataExtractor.eval_record(record, ELEM_DEVICE_UNIT)

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectPHDC( inList ):
    """
    Detect if data comes from a PHDC comptatible device.
    """
    return DetectDevice( "PHDC", inList, DEVICE_TOO_EARLY )

def AnalysePHDC( data ):
    """
    Analyse PHDC comptatible device
    """
    try:
        extractor = PHDCDataExtractor(data)
        diasend_records = extractor.split()

    except Exception, e:

        import traceback
        traceback.print_exc()

        serial_number = extractor.get_serial()
        model = extractor.get_model()
    
        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(model, serial_number, e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(model, serial_number, ERROR_CODE_PREPROCESSING_FAILED,
                ''.join(map(str, e.args)))

        return error_response

    callbacks = {}
    callbacks["eval_serial_record"]       = PHDCDataExtractor.eval_serial_record
    callbacks["eval_device_class_record"] = PHDCDataExtractor.eval_device_class_record
    callbacks["eval_device_model_record"] = PHDCDataExtractor.eval_device_model_record
    callbacks["eval_device_version"]      = PHDCDataExtractor.eval_device_version
    callbacks["eval_nr_results"]          = PHDCDataExtractor.eval_nr_results
    callbacks["eval_result_record"]       = PHDCDataExtractor.eval_result_record
    callbacks["eval_unit"]                = PHDCDataExtractor.eval_unit_record

    result = AnalyseGenericMeter(diasend_records, callbacks)

    return result


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    test_files = ['test/testcases/test_data/PHDC/Roche/AccuChekActive/active-ctrl.log',
        'test/testcases/test_data/PHDC/Roche/AccuChekActive/active.log',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-483.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-484.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-498-empty.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-500-empty.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-500-full.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-502.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-model-685.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekMobile/mobile.log',
        'test/testcases/test_data/PHDC/Roche/AccuChekActive/PHDC-Unhandled-meal-flag-ticket4693.bin',
        'test/testcases/test_data/PHDC/Roche/AccuChekConnect/AccuChekAvivaConnect-MeasurementStatus-Control.bin']

    for test_file in test_files:
        
        print test_file
        
        with open(test_file, 'rb') as f:
            data = f.read()
        
        res = AnalysePHDC(data)

        meter_type = res[0]['header']['meter_type']
        meter_serial = res[0]['header']['meter_serial']
        nr_results = len(res[0]['results'])
        is_ok = False
        if nr_results > 0:
            print res[0]['results'][0]
            is_ok = 'device_value' in res[0]['results'][0] or 'device_settings' in res[0]['results'][0]
        else:
            is_ok = True

        f_desc = ''
        f_code = 0
        if not is_ok:
            f_desc = res[0]['results'][0]['fault_data']
            f_code = res[0]['results'][0]['error_code']
            
        if is_ok:
            print '%s : %s - OK - number of results %d' % (meter_type, meter_serial, nr_results)
        else:
            print '%s : %s - FAIL - code %d - description %s' % (meter_type, meter_serial, f_code, f_desc)
