# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2008 Diasend AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE LifeScan OneTouch VerioPro
# @DEVICE LifeScan OneTouch VerioIQ
# @DEVICE LifeScan OneTouch VerioSync

import re
from Generic import *
from Defines import *
from struct import unpack
import time
import datetime
import Buffer


# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------

CHAR_STX = 0x02
CHAR_ETX = 0x03

EASY_HI_VALUE = 0xffff
EASY_LO_VALUE = 0xfffe

# Meter response codes
DEVICE_RSP_OK                   = 0x06
DEVICE_RSP_NOT_AUTHORIZED       = 0x07
DEVICE_RSP_NOT_SUPPORTED        = 0x08
DEVICE_RSP_INVALID_PARAMETER    = 0x09
DEVICE_RSP_FAILED               = 0x0f

SERVICE_ID = 0x04
BG_RSP_LENGTH = 18
ALERT_RSP_LENGTH = 17

map_model_to_market_name = {
    'VerioIQ'   : 'OneTouch VerioIQ',
    'VerioPro'  : 'OneTouch VerioPro',
    'VerioSync' : 'OneTouch VerioSync'
}

class Parameters(object):
    """
    Ok, I admit it. It's a global variable in the disguise. But a little 
    bit better (will be recreated each time we execute the analyser).
    """
    def __init__(self):
        self.model = ''

def SplitData( inData, parameters ):
    """
    Splits incoming data into a list. 
    
    The data contains of binary records prefixed by STX, suffixed by ETX + CRC
    """

    inList  = []
    data = inData

    tag = re.compile(r'(!TAG![A-Z]+!)(.*)', re.DOTALL)
    start_of_frame = re.compile(r'\x02(.)\x00(.)(.)', re.DOTALL)
    current_tag = ''

    while(len(data) > 2):

        match = tag.match(data)
        if match:
            current_tag = match.group(1)
            data = match.group(2)
            continue

        match = start_of_frame.match(data)
        if match is None:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,  'No STX in the beginning')
        
        length        = ord(match.group(1))
        service_id    = ord(match.group(2))
        response_code = ord(match.group(3))

        if len(data) < length:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Not enough bytes in the buffer (%d < %d)' % (len(data), length))
            
        # Check service id and response code
        if service_id != SERVICE_ID or response_code != DEVICE_RSP_OK:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 
                'ServiceID (%d) or response error (%d) found in frame' % (service_id, response_code))

        # Previous implementation sometimes used length to determine 
        # record type. Let's return a tuple where position 0 holds the 
        # tag type (e.g. '!TAG!RESULTS!') and position 1 the communication
        # frame. 
        insert_item = (current_tag, data[:length], parameters)

        # The generic analyze function requires serial to be
        # the first record.
        if "!TAG!SERNO!" in current_tag:
            inList.insert(0, insert_item)
        else:
            inList.append(insert_item)
        
        data = data[length:]

    return inList


def RemoveNullTermination(string):
    """
    Remove trailing nulls from incoming string
    """

    res = string

    # The serial is null terminated, cut it at the first null
    null_idx = res.find("\x00")
    if (null_idx >= 0):
        res = res[:null_idx]

    return res


def calc_crc(data):
    """
    Calculating the CRC:  

    The CCITT-CRC16 is employed: X16 + X12 + X5 + X1 where the algorithm seed is 0xffff and input is the string to 
    be transmitted from the first character up to but not including the CRC bytes.  

    The following C function will return the CRC when: initial_crc set to 0xffff, the buffer pointer set to the start of 
    the string to be transmitted and the length set to the number of bytes in the string not including the two bytes 
    for the CRC. 
    
    """
    
    crc = 0xffff
    for char in data:
        crc = ((crc >> 8) & 0xff) | ((crc << 8) & 0xffff)
        #(unsigned short)((unsigned char)(crc >> 8) | (unsigned short)(crc << 8)); 
        crc ^= ord(char)
        #crc ^= buffer [index]; 
        crc ^= (crc & 0xff) >> 4; 
        
        crc ^= (crc << 12) & 0xffff; 
        crc ^= ((crc & 0xff) << 5) & 0xffff; 

    return crc
    

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalLifescanOneTouchVerioProSoftwareVersion( line ):
    """
    Is this line a software version record. If so, return a dictionary with
    software version.
    """
    
    res = {}

    if not "!TAG!SWVER!" in line[0]:
        return res

    res[ ELEM_SW_VERSION ] = RemoveNullTermination(line[1][6:-3])

    return res

def EvalLifescanOneTouchVerioProSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    
    res = {}

    if not "!TAG!SERNO!" in line[0]:
        return res

    res[ ELEM_DEVICE_SERIAL ] = RemoveNullTermination(line[1][5:-3])

    return res
    

def EvalLifescanOneTouchVerioProModelRecord( line ):
    """
    Detect the Lifescan model by looking at the serial record
    """
    res = {}

    if not "!TAG!MODEL!" in line[0]:
        return res

    model = RemoveNullTermination(line[1][5:-3])
    if model in map_model_to_market_name:
        res[ELEM_DEVICE_MODEL] = map_model_to_market_name[model]
    else:
        raise Exception(ERROR_CODE_UNKNOWN_METER, 'Unknown model name %s' % (model))

    parameters = line[2]
    parameters.model = res[ELEM_DEVICE_MODEL]

    return res


def EvalLifescanOneTouchVerioProUnitRecord( line ):
    """
    Always return mg/dl 
    """
    res = {ELEM_DEVICE_UNIT : "mg/dl"}

    return res


def EvalLifescanOneTouchVerioProResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    ELEM_TIMESTAMP  > date in datetime object
    ELEM_VAL        > value (float) 
    ELEM_VAL_TYPE   > type of value (VALUE_TYPE_GLUCOSE)
    ELEM_FLAG_LIST > list of flags (int) if present
    """
    res = {}

    if not "!TAG!RESULTS!" in line[0]:
        return res

    data       = line[1]
    parameters = line[2]

    if parameters.model == map_model_to_market_name['VerioSync']:

        buffer = Buffer.Buffer(data)

        try:
            # First read out the generic header
            (stx, length, control, service_id, response_code) = buffer.get_struct('<BBBBB')
            if stx != 0x02:
                return res

            (record_type, first_header, next_header, record_count, record_size) = buffer.get_struct('<BHHBB')

            if record_size != 9 or record_count != 1:
                return res

            (seconds_since_2000, timezone, value, record_id) = buffer.get_struct('<IbHH')

            # Timezone is in steps of 0.25H from GMT
            timezone_diff_in_seconds = timezone*15*60

            # Corruption flag
            if not value & 0x8000:

                dt = datetime.datetime(2000,1,1) + datetime.timedelta(seconds=seconds_since_2000+timezone_diff_in_seconds)

                # This conversion is only made for VerioSync.
                glucose = (value & 0x03ff) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

                res = {
                    ELEM_TIMESTAMP: dt,
                    ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                    ELEM_VAL      : glucose
                }

                res[ELEM_FLAG_LIST] = [FLAG_RESULT_CTRL] if value & 0x4000 else []

        except Buffer.ExtractException:
            pass # Empty result passed to generic - number of results error.

    else:
        if ord(data[0]) != CHAR_STX:
            return res
         
        # Check length to differentiate from alert records
        if ord(data[1]) != BG_RSP_LENGTH:
            return res    
         
        date_time = data[5:9]
        glucose_value = data[9:11]
        control_flag = ord(data[11])
        meal_flag = ord(data[12])
        user_flag = ord(data[13])
        corrupt   = ord(data[14])

        # For UltraEasy there were handling of daylight savings and UTC time,
        # but according to VerioPro documentation and its menu system there
        # are no such settings.
        dt  = unpack("<I", date_time)[0]
     
        dt_object = datetime.datetime(2000,1,1) + datetime.timedelta(0, dt) 

        res[ ELEM_TIMESTAMP ] = dt_object

        res [ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        res [ ELEM_VAL ] = unpack("<H", glucose_value)[0]

        flag_list = []
        res[ELEM_FLAG_LIST] = flag_list
        
        # Add flags for HIGH / LOW values. These is a special case
        # where we get a glucose value (i.e no HI/LO flag) _but_ the meter itself
        # reports HI/LO in the display.
        if res [ ELEM_VAL ] > (VAL_FACTOR_CONV_MMOL_TO_MGDL * LIFESCAN_BG_VALUE_HIGH_MMOL):
            flag_list.append(FLAG_RESULT_HIGH)
        elif res [ ELEM_VAL ] < (VAL_FACTOR_CONV_MMOL_TO_MGDL * LIFESCAN_BG_VALUE_LOW_MMOL):
            flag_list.append(FLAG_RESULT_LOW)

        # Handle regular flags
        if (control_flag == 0x01):
            flag_list.append(FLAG_RESULT_CTRL)

        if (meal_flag == 0x01):
            flag_list.append(FLAG_BEFORE_MEAL)
        elif (meal_flag == 0x02):
            flag_list.append(FLAG_AFTER_MEAL)
        elif (meal_flag == 0x03):
            flag_list.append(FLAG_FASTING)
        elif (meal_flag == 0x04):
            flag_list.append(FLAG_BEFORE_SLEEP)
            
        if (user_flag == 0x01):
            flag_list.append(FLAG_NOT_ENOUGH_FOOD)
        elif (user_flag == 0x02):
            flag_list.append(FLAG_TOO_MUCH_FOOD)
        elif (user_flag == 0x03):
            flag_list.append(FLAG_MILD_EXERCISE)
        elif (user_flag == 0x04):
            flag_list.append(FLAG_HARD_EXERCISE)
        elif (user_flag == 0x05):
            flag_list.append(FLAG_MEDICATION)
        elif (user_flag == 0x06):
            flag_list.append(FLAG_STRESS)
        elif (user_flag == 0x07):
            flag_list.append(FLAG_ILLNESS)
        elif (user_flag == 0x08):
            flag_list.append(FLAG_FEEL_HYPO)
        elif (user_flag == 0x09):
            flag_list.append(FLAG_MENSES)
        elif (user_flag == 0x0a):
            flag_list.append(FLAG_VACATION)
        elif (user_flag == 0x0b):
            flag_list.append(FLAG_OTHER)

        if (corrupt == 0x01):
            flag_list.append(FLAG_RESULT_ERRONEOUS)

    return res

def EvalLifescanOneTouchVerioProChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    Each line of the record has a checksum
    """

    return True

    # We need to strip away any tags first.
    startOfFrame = re.compile('!TAG![A-Z]+!|') # \x02=CHAR_STX
    start_index = startOfFrame.match(line).end()

    crc = calc_crc(line[start_index:-2])
    
    if crc & 0xff == ord(line[-2]) and (crc >> 8) & 0xff == ord(line[-1]):
        return True
        
    return False


def EvalLifescanOneTouchVerioProNoResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}

    if not "!TAG!NORESULTS!" in line[0]:
        return res

    res[ ELEM_NR_RESULTS ] = unpack("<H", line[1][5:7])[0]

    return res


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectLifescanOneTouchVerioPro( inList ):
    """
    Detect if data comes from a Lifescan OneTouch VerioPro.
    """
    return DetectDevice( 'Onetouch VerioPro', inList, DEVICE_METER );

def AnalyseLifescanOneTouchVerioPro( inData ):
    """
    Analyse Lifescan OneTouch VerioPro
    """

    try:
        parameters = Parameters()

        inList = SplitData(inData, parameters)
        # Empty dictionary
        d = {}

        d[ "eval_device_version" ]  = EvalLifescanOneTouchVerioProSoftwareVersion
        d[ "eval_serial_record" ]   = EvalLifescanOneTouchVerioProSerialRecord
        d[ "eval_device_model_record" ] = EvalLifescanOneTouchVerioProModelRecord
        d[ "eval_unit"]             = EvalLifescanOneTouchVerioProUnitRecord
        d[ "eval_result_record" ]   = EvalLifescanOneTouchVerioProResultRecord
        d[ "eval_checksum_record" ] = EvalLifescanOneTouchVerioProChecksumRecord
        d[ "eval_nr_results" ]      = EvalLifescanOneTouchVerioProNoResultsRecord

        resList = AnalyseGenericMeter( inList, d );

    except Exception as e:

        # Uncomment for developement. 
        #import traceback
        #traceback.print_exc()
        
        # The 'standard' way is to raise Exception with two parameters, diasend error code and 
        # description.
        #
        # Note : this extractor uses late extraction; i.e. we don't have access to device
        # model and serial.
        #
        if len(e.args) == 2:
            resList = CreateErrorResponseList('OneTouch Verio IQ/Pro/Sync', '', e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            resList = CreateErrorResponseList('OneTouch Verio IQ/Pro/Sync', '', ERROR_CODE_PREPROCESSING_FAILED, 
                ''.join(map(str, e.args)))

    return resList

if __name__ == "__main__":

    import glob
    import pprint

    directories = [
        'test/testcases/test_data/LifescanOnetouchVerioSync', 
        'test/testcases/test_data/OneTouchVerioPro', 
        'test/testcases/test_data/OneTouchVerioIQ']

    test_files = []

    for directory in directories:
        test_files.extend(glob.glob('%s/*.log' % (directory)))
        test_files.extend(glob.glob('%s/*.bin' % (directory)))

    for test_file in test_files:
        with open(test_file, 'r') as f:    
            test_case_data = f.read()    
            result = AnalyseLifescanOneTouchVerioPro(test_case_data)
            #pprint.pprint(result)
