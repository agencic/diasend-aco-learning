# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Pharmasupply Advocate Redi-Code+

from Generic import *
from datetime import datetime
from struct import unpack
import crcmod.predefined


PHARMASUPPLY_ADVOCATE_SERIAL_LENGTH = 13

PHARMASUPPLY_ADVOCATE_MSG_LEN_SHORT     = 8
PHARMASUPPLY_ADVOCATE_MSG_LEN_NORMAL    = 16
PHARMASUPPLY_ADVOCATE_MSG_LEN_LONG      = 20
PHARMASUPPLY_ADVOCATE_MSG_LEN_HUGE      = 64

def PharmasupplyAdvocateFrameLen(start):
    """
        Given a frame the function glances at the first byte
        and returns its length
    """
    if start == 0xaa:
        return PHARMASUPPLY_ADVOCATE_MSG_LEN_SHORT
    elif start == 0xcc:
        return PHARMASUPPLY_ADVOCATE_MSG_LEN_NORMAL
    elif start == 0xce:
        return PHARMASUPPLY_ADVOCATE_MSG_LEN_LONG
    elif start == 0xee:
        # Undocumented long frame
        return PHARMASUPPLY_ADVOCATE_MSG_LEN_HUGE
    else:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            "Invalid start byte: %s" % hex(start))

def PharmasupplyAdvocateFrameHasSingleResponse(frame):
    """
        The protocol lacks a way to detect which request a response relates to
        so we can check if a request requires a single reponse, then we
        know that the next frame is a response... this
        function returns True if the request has a single response
    """
    # skip the startbyte:
    data = frame[1:]
    return data.startswith("IDREAD\n") or data.startswith("Hello\n")

def ForaDiamondParseFrame(frame, offset = 0):
    """
        Parse a frame into two 16 bit values
    """
    val1 = ord(frame[offset + FORA_DATA0_POS]) | \
        (ord(frame[offset + FORA_DATA1_POS]) << 8)

    val2 = ord(frame[offset + FORA_DATA2_POS]) | \
        (ord(frame[offset + FORA_DATA3_POS]) << 8)

    return (val1, val2)

def ForaDiamondValidateFrameChecksum(frame, offset = 0):
    """
        Parse a frame into two 16 bit values
    """
    sum = 0
    for i in range(FORA_DIAMOND_FRAME_LENGTH - 1):
        sum = (sum + ord(frame[offset + i])) & 0xff

    return sum == ord(frame[offset + FORA_CRC_POS])

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalPharmasupplyAdvocateSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}

    data = line[1:]
    if data.startswith("IDREAD"):
        # This is the ID frame, packed with the repsonse...
        req_len = PharmasupplyAdvocateFrameLen(ord(line[0]))

        res[ELEM_DEVICE_SERIAL] = line[req_len + 1 : \
            req_len + 1 + PHARMASUPPLY_ADVOCATE_SERIAL_LENGTH]

    return res

def EvalPharmasupplyAdvocateModelRecord( line ):
    """
    We only know about the Redi-Code+
    """
    return { ELEM_DEVICE_MODEL : "ADVOCATE Redi-Code+" }

def EvalPharmasupplyAdvocateUnitRecord( line ):
    """
    It seems like the device always return results in mg/dL
    """
    return { ELEM_DEVICE_UNIT: "mg/dL" }

def EvalPharmasupplyAdvocateResultRecord( line ):
    """
    """
    res = {}
    if len(line) == PHARMASUPPLY_ADVOCATE_MSG_LEN_LONG:
        # Substract start + stop + 2 bytes CRC
        data_len = len(line) - 2 - 2

        (_, status, result, year, month, day, hour, minute) = \
            unpack("HHHHHHHH", line[1:1 + data_len])

        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        res[ELEM_TIMESTAMP] = datetime (2000 + year, month, day, hour, minute)
        res[ELEM_VAL] = float(result) * VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

        flags = []
        if status == 0x1:
            flags.append(FLAG_BEFORE_MEAL)
        elif status == 0x2:
            flags.append(FLAG_AFTER_MEAL)
        elif status == 0x3:
            flags.append(FLAG_GENERAL)
        elif status == 0x4:
            flags.append(FLAG_RESULT_CTRL)
        elif status == 0x5:
            flags.append(FLAG_OTHER)

        if flags:
            res[ELEM_FLAG_LIST] = flags

    return res

def EvalPharmasupplyAdvocateChecksumRecord( line, record ):
    """
        Evaluate checksum of result record.
    """
    # Remove start/stop + two bytes crc
    data_len = len(line) - 2 - 2

    crc_func = crcmod.predefined.mkCrcFun('modbus')

    calc_crc = crc_func(line[1:1 + data_len])
    (crc,) = unpack(">H", line[1 + data_len:1 + data_len + 2])

    return calc_crc == crc

def EvalPharmasupplyAdvocateTerminationRecord( line ):
    """
    Check if this is the termination record.
    This is the short reply containing the OK string

    """
    return len(line) == PHARMASUPPLY_ADVOCATE_MSG_LEN_SHORT and \
        line[1:].startswith("OK")

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterPharmasupplyAdvocate( inList ):
    """
    Detect if data comes from a Pharmasupply Advocate Redi-Code+.
    """
    return DetectDevice( 'Pharmasupply_Advocate', inList, DEVICE_METER )

def AnalyseMeterPharmasupplyAdvocate( inData ):
    """
    Analyse Pharmasupply Advocate Redi-Code+
    """
    inList = []
    index = 0
    stillCombining = True
    

    # There is a "problem" that the responses from the device does not contain
    # any reference to the request, its just pure data, so for the initial
    # messages pair the request with the response
    while index < len(inData):
        length = PharmasupplyAdvocateFrameLen(ord(inData[index]))
        frame = inData[index:index + length]
        index += length
        if stillCombining:
            stillCombining = PharmasupplyAdvocateFrameHasSingleResponse(frame)
            if stillCombining:
                length = PharmasupplyAdvocateFrameLen(ord(inData[index]))
                frame += inData[index:index + length]
                index += length

        inList.append(frame)

    # Empty dictionary
    d = {}

    d[ "eval_serial_record" ]   = EvalPharmasupplyAdvocateSerialRecord;
    d[ "eval_device_model_record" ] = EvalPharmasupplyAdvocateModelRecord;
    d[ "eval_unit"]             = EvalPharmasupplyAdvocateUnitRecord;
    d[ "eval_result_record" ]   = EvalPharmasupplyAdvocateResultRecord;
    d[ "eval_checksum_record" ] = EvalPharmasupplyAdvocateChecksumRecord
    d[ "eval_termination_record" ]  = EvalPharmasupplyAdvocateTerminationRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":

    test_files = (
        'test/testcases/test_data/PharmasupplyAdvocate/RediCodePlus-empty.bin',
        'test/testcases/test_data/PharmasupplyAdvocate/RediCodePlus-full.bin',
        'test/testcases/test_data/PharmasupplyAdvocate/RediCodePlus-halffull.bin',
        'test/testcases/test_data/PharmasupplyAdvocate/RediCodePlus-with-flags.log'
    )

    for test_file in test_files:
        with open(test_file, 'r') as f:
            payload = f.read()            

        results = AnalyseMeterPharmasupplyAdvocate(payload)
        print results[0]['header']
        for result in results[0]['results']:
            print result
