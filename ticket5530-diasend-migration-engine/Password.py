# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import hashlib
import base64

def create(raw, salt):
    """Create encrypted password using same algorithm as the web."""

    salted = "%s{%s}" % (raw, salt)
    digest = hashlib.sha512(salted)

    for i in range(4999):
        digest = hashlib.sha512(digest.digest() + salted)

    return base64.b64encode(digest.digest())

def compare(password, digest, salt):
    """Validate the password."""
    my_digest = create(password, salt)

    if len(digest) != len(my_digest):
        return False

    result = 0
    for index in range(len(digest)):
        result |= ord(digest[index]) ^ ord(my_digest[index])

    return result == 0
	
