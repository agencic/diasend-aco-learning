# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re
from Generic import *
from Defines import *
from datetime import datetime, timedelta
from datetime import time
import EventLog
import string

try:
    from elementtree import ElementTree
except:
    # Python 2.5
    from xml.etree import ElementTree

from xml.parsers.expat import ExpatError


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
def InitiateGlobals():
    """
    Initiate globally used variables
    """
    global serialNumber
    serialNumber = None


def FlattenTree( tree ):
    """
    Returns a list of the leaves in a tree
    """
    retList = []

    if len(tree.getchildren()) == 0:
        # This is a leaf
        retList.append(tree)
    else:
        for elem in tree.getchildren():
            retList += FlattenTree(elem)

    return retList

def SplitData( inData ):
    """
    Splits incoming data into a list. 
    The serial number is pre-decoded, since it is only available in the header
    """
    
    inList = []
    serial = None
    
    try:
        tree = ElementTree.fromstring( inData )
        # First, get the serial number from the header
        serial = tree.get("ReceiverNumber");
        
        # Get a list containing the leaves
        inList = FlattenTree(tree)
    except ExpatError, v:
        print "ERROR: No real XML"
        pass
                
    return (inList, serial)

def ParseDateTime(timestring):
    """
    Parses the string 2010-11-02 21:57:06.550
    Parses time of form: 13:02
    And date of form: 2007-07-23

    into a datetime
    """
    m = re.match( r"(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}).(\d{2}).(\d{3})", timestring, re.IGNORECASE)

    if not m:
        return None

    # Create the exact time, and then round to nearest second by adding half a second.
    true_time = datetime(int(m.group(1)), int(m.group(2)), int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)), 1000*int(m.group(7)))
    rounding_delta = timedelta(microseconds=500*1000)
    return true_time+rounding_delta


def EvalSensorMeterRecord( line ):
    """
    Evaluates a Meter or a Sensor XML element
    <Meter DisplayTime="2010-11-02 21:57:06.550" InternalTime="2010-11-02 20:43:41.260" Value="14,32" />
    <Sensor DisplayTime="2010-08-28 11:17:59.880" InternalTime="2010-08-28 09:03:24.010" Value="10,82" /> 

    <Meter DisplayTime="2010-11-02 21:57:06.550" InternalTime="2010-11-02 20:43:41.260" Value="14,32" />
    <Sensor DisplayTime="2010-08-28 11:17:59.880" InternalTime="2010-08-28 09:03:24.010" Value="10,82" /> 
    """
    res = {}
    
    # This is a blood glucose value 
    res[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
    
    # If it comes from a sensor, it is a CGM value
    flags = []
    if line.tag == "Sensor":
        flags.append(FLAG_CONTINOUS_READING)
    res[ELEM_FLAG_LIST] = flags

    # Get the value, but replace the comma character to create a real float
    value = line.get("Value")
    
    # The difference between a mmol/l value and a mg/dl value is that the mg/dl
    # has no decimal, where the mmol/l has two. We do a regexp on the value so
    # that we are certain no erroneous values are getting through. 
    m = re.match( r'(\d+,\d{2})$', value, re.IGNORECASE )
    if m:
        # This is a mmol/l value. Replace the decimal point to create a true
        # float value
        val = float(m.group(1).replace(",", "."))
        res[ELEM_VAL] = val*VAL_FACTOR_GLUCOSE_MMOL
        res[ELEM_DEVICE_UNIT]  = "mmol/l"
    else:
        m = re.match( r'(\d+)$', value, re.IGNORECASE )
        if m:
            # This is a mg/dl value
            val = float(m.group(1))
            # @TODO: Replace this "odd" calculation, when the mg/dl divisor is
            # correct in the generic code (18.016 instead of 18.0)
            res[ELEM_VAL] = val*VAL_FACTOR_GLUCOSE_MGDL*18.0/18.016
            res[ELEM_DEVICE_UNIT]  = "mg/dl"
        else:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }

    # Set the time & date
    res[ELEM_TIMESTAMP] = ParseDateTime(str(line.get("DisplayTime")))
    return res
    
    
def EvalEventRecord( line ):
    """
    Evaluates an Event XML element
        <Event DisplayTime="2010-09-22 10:59:09.600" InternalTime="2010-09-22 08:45:31.730" EventType="Carbs" Decription="25" />
        <Event DisplayTime="2010-11-04 10:07:23.190" InternalTime="2010-11-04 08:53:57.900" EventType="Carbs" Decription="50" />
        <Event DisplayTime="2010-11-04 10:07:54.930" InternalTime="2010-11-04 08:54:29.640" EventType="Insulin" Decription="350" />
        <Event DisplayTime="2010-11-04 10:08:27.130" InternalTime="2010-11-04 08:55:01.840" EventType="Health Stress" Decription="" />
    """
    res = {}
    
    eventType = line.get("EventType")
    description = line.get("Decription")
    flags = []
    
    if eventType == "Carbs":
        # This is a carbo hydrate value 
        res[ELEM_VAL_TYPE] = VALUE_TYPE_CARBS
        res[ELEM_VAL] = float(description)
    elif eventType == "Insulin":
        # This is a manual bolus
        res[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS
        res[ELEM_VAL] = float(description)*VAL_FACTOR_BOLUS/100
        flags.append(FLAG_MANUAL)
        
    # If a valid result, get the time & date as well
    if ELEM_VAL_TYPE in res:
        res[ELEM_FLAG_LIST] = flags
        res[ELEM_TIMESTAMP] = ParseDateTime(str(line.get("DisplayTime")))
        
    return res

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalDexcomXMLSerialRecord( line ):
    """
    There is no real 'serial record' in the XML files, so if we have found a serial
    number already, return this.
    """
    res = {}
    
    global serialNumber
    
    if serialNumber != None:
        res[ ELEM_DEVICE_SERIAL ] = serialNumber

    return res


def EvalDexcomXMLDeviceModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """

    return {ELEM_DEVICE_MODEL : "Dexcom XML File"}


def EvalDexcomXMLUnitRecord( line ):
    """
    Unit is always decided by each value. 
    """

    return {ELEM_DEVICE_UNIT : ""}


def EvalDexcomXMLResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    res = {}

    if line.tag == "Meter":
        res = EvalSensorMeterRecord (line)
    elif line.tag == "Sensor":
        res = EvalSensorMeterRecord (line)
    elif line.tag == "Event":
        res = EvalEventRecord (line)

    return res

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectDexcomXML( inList ):
    """
    Detect if data comes from a device using the generic Diasend XML format.
    """
    return DetectDevice( "DexcomXML", inList, DEVICE_METER )

def AnalyseDexcomXML( inData ):
    """
    Analyse generic Diasend XML format. 
    """
    
    global serialNumber;
    
    InitiateGlobals()

    # Get the data, with a predecoded serial number
    (inList, serialNumber) = SplitData (inData)

    # Empty dictionary
    d = {}

    d[ "eval_serial_record" ]       = EvalDexcomXMLSerialRecord
    d[ "eval_device_model_record" ] = EvalDexcomXMLDeviceModelRecord
    d[ "eval_unit"]                 = EvalDexcomXMLUnitRecord
    d[ "eval_result_record" ]       = EvalDexcomXMLResultRecord

    resList = AnalyseGenericMeter( inList, d );

    return resList

if __name__ == "__main__":

    # Some test data, truncated from a real transfer. Most values are in mmol/l but some mg/dl values are inserted manually
    dataDexcom = """<Patient Id="{86804BC3-1970-4896-92E0-23CD74B34739}" FirstName="Pia" LastName="Hanås" MiddleName="" Initials="PH" DisplayName="" PatientNumber="0" ReceiverNumber="48904" Gender="Unknown" DOB="" Doctor="" Email="" PatientIdentifier="" PhoneNumber="" PhoneExtension="" Comments="">
    <MeterReadings>
        <Meter DisplayTime="2010-08-28 10:23:35.000" InternalTime="2010-08-28 08:09:57.310" Value="11,42" />
        <Meter DisplayTime="2010-08-28 10:24:51.940" InternalTime="2010-08-28 08:11:14.070" Value="12,82" />
        <Meter DisplayTime="2010-08-28 11:24:51.940" InternalTime="2010-08-28 08:11:14.070" Value="304" />
        <Meter DisplayTime="2010-08-28 12:24:51.940" InternalTime="2010-08-28 08:11:14.070" Value="100" />
        <Meter DisplayTime="2010-08-28 14:11:06.940" InternalTime="2010-08-28 11:57:29.070" Value="10,88" />
        <Meter DisplayTime="2010-08-29 02:30:06.190" InternalTime="2010-08-29 00:16:28.320" Value="8,21" />
        <Meter DisplayTime="2010-09-23 22:44:16.940" InternalTime="2010-09-23 20:30:39.070" Value="7,33" />
        <Meter DisplayTime="2010-09-24 07:37:40.150" InternalTime="2010-09-24 05:24:02.280" Value="9,71" />
        <Meter DisplayTime="2010-09-24 17:21:02.950" InternalTime="2010-09-24 15:07:25.080" Value="6,83" />
        <Meter DisplayTime="2010-10-10 21:25:04.940" InternalTime="2010-10-10 19:11:27.070" Value="6,83" />
        <Meter DisplayTime="2010-10-11 06:07:30.940" InternalTime="2010-10-11 03:53:53.070" Value="7,38" />
        <Meter DisplayTime="2010-10-11 20:53:22.950" InternalTime="2010-10-11 18:39:45.080" Value="5,72" />
        <Meter DisplayTime="2010-10-12 07:20:10.160" InternalTime="2010-10-12 05:06:32.290" Value="7,71" />
        <Meter DisplayTime="2010-10-16 07:50:12.950" InternalTime="2010-10-16 05:36:35.080" Value="6,49" />
        <Meter DisplayTime="2010-10-16 17:08:08.940" InternalTime="2010-10-16 14:54:31.070" Value="3,88" />
        <Meter DisplayTime="2010-10-17 07:27:58.940" InternalTime="2010-10-17 05:14:21.070" Value="7,88" />
        <Meter DisplayTime="2010-10-17 14:23:05.120" InternalTime="2010-10-17 12:09:27.250" Value="7,88" />
        <Meter DisplayTime="2010-11-03 07:15:54.370" InternalTime="2010-11-03 06:02:29.080" Value="5,83" />
        <Meter DisplayTime="2010-11-03 21:53:15.370" InternalTime="2010-11-03 20:39:50.080" Value="7,60" />
        <Meter DisplayTime="2010-11-04 05:55:16.360" InternalTime="2010-11-04 04:41:51.070" Value="6,83" />
    </MeterReadings>
    <SensorReadings>
        <Sensor DisplayTime="2010-08-28 11:17:59.880" InternalTime="2010-08-28 09:03:24.010" Value="10,82" />
        <Sensor DisplayTime="2010-08-28 11:22:01.880" InternalTime="2010-08-28 09:08:24.010" Value="10,82" />
        <Sensor DisplayTime="2010-08-28 11:27:01.880" InternalTime="2010-08-28 09:13:24.010" Value="10,32" />
        <Sensor DisplayTime="2010-08-28 11:32:01.880" InternalTime="2010-08-28 09:18:24.010" Value="10,49" />
        <Sensor DisplayTime="2010-08-28 11:34:01.280" InternalTime="2010-08-28 09:13:24.010" Value="200" />
        <Sensor DisplayTime="2010-08-28 11:36:59.981" InternalTime="2010-08-28 09:18:24.010" Value="113" />
        <Sensor DisplayTime="2010-09-22 11:59:27.880" InternalTime="2010-09-22 09:45:50.010" Value="9,88" />
        <Sensor DisplayTime="2010-09-22 12:04:27.880" InternalTime="2010-09-22 09:50:50.010" Value="10,04" />
        <Sensor DisplayTime="2010-09-22 12:19:27.890" InternalTime="2010-09-22 10:05:50.020" Value="10,32" />
        <Sensor DisplayTime="2010-09-22 12:24:27.880" InternalTime="2010-09-22 10:10:50.010" Value="10,27" />
        <Sensor DisplayTime="2010-10-04 03:53:17.880" InternalTime="2010-10-04 01:39:40.010" Value="9,93" />
        <Sensor DisplayTime="2010-10-04 03:58:17.880" InternalTime="2010-10-04 01:44:40.010" Value="9,88" />
        <Sensor DisplayTime="2010-10-04 04:03:17.880" InternalTime="2010-10-04 01:49:40.010" Value="11,32" />
        <Sensor DisplayTime="2010-10-04 04:08:17.880" InternalTime="2010-10-04 01:54:40.010" Value="10,27" />
        <Sensor DisplayTime="2010-10-04 04:13:17.880" InternalTime="2010-10-04 01:59:40.010" Value="9,93" />
        <Sensor DisplayTime="2010-10-04 04:18:17.880" InternalTime="2010-10-04 02:04:40.010" Value="9,27" />
        <Sensor DisplayTime="2010-10-04 04:23:17.880" InternalTime="2010-10-04 02:09:40.010" Value="8,66" />
        <Sensor DisplayTime="2010-10-16 00:22:05.880" InternalTime="2010-10-15 22:08:28.010" Value="11,65" />
        <Sensor DisplayTime="2010-10-16 00:27:05.880" InternalTime="2010-10-15 22:13:28.010" Value="10,77" />
        <Sensor DisplayTime="2010-10-16 00:32:05.880" InternalTime="2010-10-15 22:18:28.010" Value="10,99" />
        <Sensor DisplayTime="2010-10-16 00:37:05.880" InternalTime="2010-10-15 22:23:28.010" Value="9,88" />
        <Sensor DisplayTime="2010-10-16 00:42:05.880" InternalTime="2010-10-15 22:28:28.010" Value="8,60" />
        <Sensor DisplayTime="2010-10-16 00:47:05.880" InternalTime="2010-10-15 22:33:28.010" Value="7,66" />
        <Sensor DisplayTime="2010-10-16 00:52:05.880" InternalTime="2010-10-15 22:38:28.010" Value="7,27" />
        <Sensor DisplayTime="2010-11-04 09:54:55.300" InternalTime="2010-11-04 08:41:30.010" Value="9,66" />
        <Sensor DisplayTime="2010-11-04 09:59:55.300" InternalTime="2010-11-04 08:46:30.010" Value="9,66" />
        <Sensor DisplayTime="2010-11-04 10:04:55.300" InternalTime="2010-11-04 08:51:30.010" Value="9,49" />
        <Sensor DisplayTime="2010-11-04 10:09:56.140" InternalTime="2010-11-04 08:56:30.850" Value="9,49" />
    </SensorReadings>
    <EventMarkers>
        <Event DisplayTime="2010-09-22 10:59:09.600" InternalTime="2010-09-22 08:45:31.730" EventType="Carbs" Decription="25" />
        <Event DisplayTime="2010-11-04 10:07:23.190" InternalTime="2010-11-04 08:53:57.900" EventType="Carbs" Decription="50" />
        <Event DisplayTime="2010-11-04 10:07:54.930" InternalTime="2010-11-04 08:54:29.640" EventType="Insulin" Decription="350" />
        <Event DisplayTime="2010-11-04 10:08:27.130" InternalTime="2010-11-04 08:55:01.840" EventType="Health Stress" Decription="" />
    </EventMarkers>
    <Settings>
        <Setting DisplayTime="2010-06-14 13:27:06.240" InternalTime="2010-06-14 13:27:06.240" DisplayMode="Unblinded" />
    </Settings>
</Patient>"""

    res = AnalyseDexcomXML(dataDexcom)

    print res
