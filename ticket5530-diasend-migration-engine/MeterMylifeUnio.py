# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE mylife Unio
# @DEVICE BIOSEVEN lineaD ORO
# 
import re
import Common
import struct
from datetime import datetime

from Generic import *
from Defines import *

class MylifeUnioRecord(object):

    # Limits for LO/HI values - set by the status record.
    min_bg_value = 0
    max_bg_value = 0

    def __init__(self, data):
        self.dictionary = {}

        # Checksum is a simple 8-bit sum of all bytes. 
        try:
            calculated_checksum = sum(map(ord, data)[:-2]) & 0xff
            checksum = int(data[-2:], 16)
            self.checksum_ok = calculated_checksum == checksum
        except ValueError:
            self.checksum_ok = False

    # Create an instance a sub-class, which one depends on the
    # prefix of the record (e.g. '{MN' is model name).
    @classmethod
    def create(cls, data):
        for sub_class in cls.__subclasses__():
            if data.startswith(sub_class.response_prefix):
                return sub_class(data)

        return None

    def get_dictionary(self):
        return self.dictionary

    def is_checksum_ok(self):
        return self.checksum_ok

    def is_model(self):
        return False

    def is_serial(self):
        return False

    def is_result(self):
        return False

    def is_number_of_results(self):
        return False

class MylifeUnioDeviceModelRecord(MylifeUnioRecord):

    # Prefix for the MylifeUnioRecord create method.
    response_prefix = '{MN'

    models = {
        'GM720':'mylife Unio',
        'GM721':'BIOSEVEN lineaD ORO'
        }
    
    def __init__(self, data):

        super(MylifeUnioDeviceModelRecord, self).__init__(data)

        # Note : model records are not checked for checksum errors
        # by Generic.py - let's just keep it empty. 
        if not self.is_checksum_ok():
            return 

        # Model name is {MN...CC, example {MNGM72043.
        model_name = data[3:-2]

        if model_name in MylifeUnioDeviceModelRecord.models:
            self.dictionary[ELEM_DEVICE_MODEL] = MylifeUnioDeviceModelRecord.models[model_name]            

    def is_model(self):
        return True

class MylifeUnioEnableStatusRecord(MylifeUnioRecord):

    # Prefix for the MylifeUnioRecord create method.
    response_prefix = '{EN'

    def __init__(self, data):
        super(MylifeUnioEnableStatusRecord, self).__init__(data)

class MylifeUnioStatusRecord(MylifeUnioRecord):

    # Prefix for the MylifeUnioRecord create method.
    response_prefix = '{RD'

    def __init__(self, data):
        super(MylifeUnioStatusRecord, self).__init__(data)

        # bit7 ==> 0:24H mode 1:12H mode
        # bit6 ==> 0:mgdl 1:mmol
        # bit5 ==> 0:(10~600mgdl) 1:(20~600mgdl)
        # bit4,bit3 ==> 00:(Mute) 
        #               01:(Volume_Level_H)
        #               10:(Volume_Level_M) 
        #               11:(Volume_Level_L) 
        # bit2 ==> 0:mm/dd/yyyy 1:dd/mm/yyyy
        # bit1,bit0 ==> 00:English 01:Deutsch 10:Francais 11:Italian0
        if data.startswith('{RD0069') and self.is_checksum_ok():
            status = int(data[7:9], 16)

            if status & 0x20:
                MylifeUnioRecord.min_bg_value = 20
                MylifeUnioRecord.max_bg_value = 600
            else:
                MylifeUnioRecord.min_bg_value = 10
                MylifeUnioRecord.max_bg_value = 600

class MylifeUnioSerialRecord(MylifeUnioRecord):

    # Prefix for the MylifeUnioRecord create method.
    response_prefix = '{ID'

    def __init__(self, data):

        super(MylifeUnioSerialRecord, self).__init__(data)

        # Note : serial records are not checked for checksum errors
        # by Generic.py - let's just keep it empty. 
        if not self.is_checksum_ok():
            return 

        # Remove prefix and checksum.
        self.dictionary[ELEM_DEVICE_SERIAL] = data[3:-2]

    def is_serial(self):
        return True

class MylifeUnioNoResultRecord(MylifeUnioRecord):

    # Prefix for the MylifeUnioRecord create method.
    response_prefix = 'No Memory Record...'

    def __init__(self, data):

        super(MylifeUnioNoResultRecord, self).__init__(data)

        # This response contains to checksum! 
        self.checksum_ok = True

        self.dictionary[ELEM_NR_RESULTS] = 0

    def is_number_of_results(self):
        return True

class MylifeUnioResultRecord(MylifeUnioRecord):

    # Prefix for the MylifeUnioRecord create method.
    response_prefix = '{RG'

    def __init__(self, data):

        super(MylifeUnioResultRecord, self).__init__(data)

        # This regular expression matches record index 0 which 
        # holds the total number of results.
        m = re.match(r'\{RG([0-9A-Z]{4})/[0-9A-Z]{6}', data)
        if m:
            # Number of results is not checked by the Generic
            # checksum control.
            if not self.is_checksum_ok():
                return
            self.dictionary[ELEM_NR_RESULTS] = int(m.group(1), 16)
        else:
            # So this is a glucose records instead.
            m = re.match(r'\{RG([0-9A-Z]{4})([0-9A-Z]{2})_([0-9A-Z]{2})/([0-9A-Z]{2})/([0-9A-Z]{2})_([0-9A-Z]{2}):([0-9A-Z]{2}):([0-9A-Z]{4})mg/dL([0-9A-Z]{2})', data)

            if m:
                index  = int(m.group(1), 16)
                status = int(m.group(2), 16)
                dt     = datetime(2000 + int(m.group(3), 16), int(m.group(4), 16), int(m.group(5), 16), int(m.group(6), 16), int(m.group(7), 16))
                value  = int(m.group(8), 16) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

                flags = []

                if MylifeUnioRecord.min_bg_value == 0 or MylifeUnioRecord.max_bg_value == 0:
                    return

                if value < MylifeUnioRecord.min_bg_value:
                    flags.append(FLAG_RESULT_LOW)
                if value > MylifeUnioRecord.max_bg_value:
                    flags.append(FLAG_RESULT_HIGH)

                # bit7 : fasting , 1 : set marker 0 : clr marker 
                # bit6 : sport, 1 : set marker 0 : clr marker 
                # bit5 : ill, 1 : set marker 0 : clr marker
                # bit4 : special, 1 : set marker 0 : clr marker 
                # bit3 : no use , don’t care
                # bit2 : CS, 1 : control test record 0 : blood test record 
                # bit1 : nofasting, 1 : set marker 0 : clr marker
                # bit0 : no use , don’t care
                
                if status & 0x80:
                    flags.append(FLAG_AFTER_MEAL)
                if status & 0x40:
                    flags.append(FLAG_MEDIUM_EXERCISE)
                if status & 0x20:
                    flags.append(FLAG_ILLNESS)
                if status & 0x04:
                    flags.append(FLAG_RESULT_CTRL)
                if status & 0x02:
                    flags.append(FLAG_BEFORE_MEAL)

                self.dictionary[ELEM_VAL_TYPE]   = VALUE_TYPE_GLUCOSE
                self.dictionary[ELEM_TIMESTAMP]  = dt
                self.dictionary[ELEM_FLAG_LIST]  = flags
                self.dictionary[ELEM_VAL]        = value

    def is_number_of_results(self):
        return ELEM_NR_RESULTS in self.dictionary

    def is_result(self):
        return ELEM_VAL_TYPE in self.dictionary

def SplitData(data):

    result = []

    index = 0

    while index < len(data):
        if data[index] != '?':
            break

        packet_len = ord(data[index+1])
        packet = data[index+2:index+2+packet_len]

        record = MylifeUnioRecord.create(packet)
        if record != None:
            result.append(record)

        index += packet_len + 2

    return result

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalMylifeUnioSerialRecord(record):
    """
    Check for serial record.
    """
    if record.is_serial():
        return record.get_dictionary()
    return {}
    
def EvalMylifeUnioUnitRecord(line):
    """
    Check for device unit - always return mg/dl 
    """
    res = {ELEM_DEVICE_UNIT:"mg/dL"}
    return res
    
def EvalMylifeUnioModelRecord(record):
    """
    Check for device model. 
    """
    if record.is_model():
        return record.get_dictionary()
    return {}

def EvalMylifeUnioResultRecord(record):
    """
    Is this a result record? If so, return a dictionary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    if record.is_result():
        return record.get_dictionary()
    return {}

def EvalMylifeUnioChecksumRecord(line, record):
    """
    Evaluate checksum of result record. Not applicable for this meter.
    """
    return line.is_checksum_ok()

def EvalMylifeUnioChecksumFile(inList):
    """
    Evaluate checksum of result record.
    """
    return True

def EvalMylifeUnioNrResultsRecord(record):
    """
    Is this record a nr results. If so, return a dictionary with nr results.
    """
    if record.is_number_of_results():
        return record.get_dictionary()
    return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterMylifeUnio(inList):
    """
    Detect if data comes from a mylife Undio.
    """
    return DetectDevice( 'MylifeUnio', inList, DEVICE_METER );

def AnalyseMeterMylifeUnio(inData):
    """
    Analyse data from mylife Unio.
    """

    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ]  = EvalMylifeUnioModelRecord
    d[ "eval_serial_record" ]        = EvalMylifeUnioSerialRecord
    d[ "eval_unit"]                  = EvalMylifeUnioUnitRecord
    d[ "eval_result_record" ]        = EvalMylifeUnioResultRecord
    d[ "eval_checksum_record" ]      = EvalMylifeUnioChecksumRecord
    d[ "eval_checksum_file" ]        = EvalMylifeUnioChecksumFile
    d[ "eval_nr_results" ]           = EvalMylifeUnioNrResultsRecord

    inList = SplitData(inData)

    resList = AnalyseGenericMeter( inList, d );
    
    return resList

if __name__ == "__main__":

    #testfile = open('test/testcases/test_data/MylifeUnio/MylifeUnio_1000.log')
    #testfile = open('test/testcases/test_data/MylifeUnio/MylifeUnio_Empty_1.log')
    #testfile = open('test/testcases/test_data/MylifeUnio/Fail/MylifeUnio_checksum.log')
    #testfile = open('test/testcases/test_data/MylifeUnio/Fail/MylifeUnio_too_few_results.log')
    #testfile = open('test/testcases/test_data/BiosevenLineaDOro/bioseven-oro-empty.bin')
    testfile = open('test/testcases/test_data/BiosevenLineaDOro/bioseven-oro-half.bin')
    testcase = testfile.read()
    testfile.close()
    
    results = AnalyseMeterMylifeUnio(testcase)

    for result in results[0]['results']:
        print result

