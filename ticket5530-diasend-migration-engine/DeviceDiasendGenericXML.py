# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Generic Diasend XML File

import re
from Generic import *
from Defines import *
from datetime import datetime
from datetime import time
import EventLog
import string

import Config
exec "import %s" % Config.settings["database-module"]

try:
    from elementtree import ElementTree
except:
    # Python 2.5
    from xml.etree import ElementTree

from xml.parsers.expat import ExpatError


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
def InitiateGlobals():
    """
    Initiate globally used variables
    """
    global _global_bg_factor
    _global_bg_factor = None


def FlattenTree( tree ):
    """
    Returns a list of the leaves in a tree
    """
    retList = []

    if len(tree.getchildren()) == 0:
        # This is a leaf
        retList.append(tree)
    else:
        for elem in tree.getchildren():
            retList += FlattenTree(elem)

    return retList

def SplitData( inData ):
    """
    Splits incoming data into a list
    """

    inList = []

    try:
        tree = ElementTree.fromstring( inData )
        
        # Get a list containing the leaves
        inList = FlattenTree(tree)
        inList = tree.getchildren()
    except ExpatError, v:
        print "ERROR: No real XML"
        pass                
                
    return inList

def ParseDateTime(date, time):
    """
    Parses time of form: 13:02
    And date of form: 2007-07-23

    into a datetime
    """
    d_m = re.match( r"(\d{4,4})-(\d{2,2})-(\d{2,2})", date, re.IGNORECASE)
    t_m = re.match( r"(\d{2,2}):(\d{2,2})", time, re.IGNORECASE)

    if not d_m or not t_m:
        return None

    return datetime(int(d_m.group(1)), int(d_m.group(2)), int(d_m.group(3)), int(t_m.group(1)), int(t_m.group(2)))


    
def ParseFlags( flagstring ):

    TextFlagToDiasendFlag = {
                        "M" : FLAG_MANUAL
                        }

    # Decode flags
    flags = []

    if flagstring:
        # The flags are separated with "," - like "M, X, Y"
        inflags = string.split(flagstring, ",")
        for flag in inflags: 
            _diasend_flag = TextFlagToDiasendFlag[ string.strip(flag) ]

            flags.append(_diasend_flag)
    
    return flags

def EvalBloodGlucoseRecord( line ):
    """
    Evaluates a BG XML element
    <BG Date="2010-06-16" Time="17:55" Value="12.6" Flags="M"/>
    """
    global _global_bg_factor    
    res = {}
    
    # This is a blood glucose value 
    res[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
    
    try:
        flags = ParseFlags(line.get("Flags"))
    except:
        return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":inflags}
    
    res[ELEM_FLAG_LIST] = flags
    val = line.get("Value")

    if _global_bg_factor:
        float_val = float(val) * _global_bg_factor
    else:
        return { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":"No BG factor yet" }

    res[ELEM_VAL] = round(float_val)
            
    res[ELEM_TIMESTAMP] = ParseDateTime(str(line.get("Date")), str(line.get("Time")))
    
    return res
    
def EvalBolusRecord( line ):
    """
    Evaluates a BOLUS XML element
    <BOLUS Date="2010-06-16" Time="17:55" Value="12.6" Flags="M"/>
    """
    global _global_bg_factor    
    res = {}
    
    # This is a insulin value 
    res[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS
    
    try:
        flags = ParseFlags(line.get("Flags"))
    except:
        return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":inflags}
    
    res[ELEM_FLAG_LIST] = flags
    res[ELEM_VAL] = round(float(line.get("Value"))* VAL_FACTOR_BOLUS)
            
    res[ELEM_TIMESTAMP] = ParseDateTime(str(line.get("Date")), str(line.get("Time")))
    
    return res
    
def EvalCarbsRecord( line ):
    """
    Evaluates a Carbs XML element
    <BOLUS Date="2010-06-16" Time="17:55" Value="12.6" Flags="M"/>
    """
    global _global_bg_factor    
    res = {}
    
    # This is a insulin value 
    res[ELEM_VAL_TYPE] = VALUE_TYPE_CARBS
    
    try:
        flags = ParseFlags(line.get("Flags"))
    except:
        return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":inflags}
    
    res[ELEM_FLAG_LIST] = flags
    res[ELEM_VAL] = round(float(line.get("Value")))
            
    res[ELEM_TIMESTAMP] = ParseDateTime(str(line.get("Date")), str(line.get("Time")))
    
    return res

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalDiasendGenericXMLSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    
    Also check the Smartpix version
    """
    res = {}
    
    if line.tag == "DEVICE":
        if line.get("Serial"):
            res[ "meter_serial" ] = line.get("Serial")
        elif line.get("User"):
            res[ "meter_serial" ] = Database.GetUserDeviceFromUsername(line.get("User"))

    return res
    

def EvalDiasendGenericXMLDeviceModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
    res = {}
    if line.tag == "DEVICE" or line.tag == "IP":
        res[ "device_model" ] = str(line.get("Name"))

    return res


def EvalDiasendGenericXMLDeviceClassRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
    res = {}
    if line.tag == "DEVICE":
        if line.get("User"):
            res[ "device_class" ] = DEVICE_USER
        else:
            print "ERROR: Device class not implemented"

    return res


def EvalDiasendGenericXMLUnitRecord( line ):
    """
    Check the unit used by this device
    """
    global _global_bg_factor 
    res = {}
    if line.tag == "DEVICE":
        res[ "meter_unit" ] = line.get("BGUnit")
        
        # Setup the factor (number of zeros added) depending on the unit
        if res["meter_unit"] == "mmol/l":
            _global_bg_factor = VAL_FACTOR_GLUCOSE_MMOL
        elif res["meter_unit"] == "mg/dl":
            _global_bg_factor = VAL_FACTOR_GLUCOSE_MGDL
        else:
            res = {}

    return res


def EvalDiasendGenericXMLResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    if line.tag == "RECORDS":
        resList = []
        for child in line.getchildren():
            if child.tag == "BG":
                resList.append(EvalBloodGlucoseRecord ( child ))
            elif child.tag == "BOLUS":
                resList.append(EvalBolusRecord ( child ))
            elif child.tag == "CARBS":
                resList.append(EvalCarbsRecord ( child ))
        return resList
    else:
        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectDiasendGenericXML( inList ):
    """
    Detect if data comes from a device using the generic Diasend XML format.
    """
    return DetectDevice( "DiasendGenericXML", inList, DEVICE_TOO_EARLY )

def AnalyseDiasendGenericXML( inData ):
    """
    Analyse generic Diasend XML format. 
    """
    
    InitiateGlobals()
    inList = SplitData (inData)
    print repr(inList)
    
    # Empty dictionary
    d = {}

    d[ "eval_serial_record" ]       = EvalDiasendGenericXMLSerialRecord
    d[ "eval_device_model_record" ] = EvalDiasendGenericXMLDeviceModelRecord
    d[ "eval_unit"]                 = EvalDiasendGenericXMLUnitRecord
    d[ "eval_result_record" ]       = EvalDiasendGenericXMLResultRecord
    d[ "eval_device_class_record" ] = EvalDiasendGenericXMLDeviceClassRecord

    resList = AnalyseGenericMeter( inList, d );

    print resList;

    return resList

if __name__ == "__main__":
    
    dataSerial = """<?xml version="1.0" encoding="UTF-8" ?>
<DATA>
<DEVICE Type="Hej" Name="Hopp" Model="Hepp" Serial="12345" BGUnit="mmol/l"/>
<TRANSFER Date="2010-06-16" Time="18:00"/>
<RECORDS>
<BG Date="2010-06-16" Time="17:55" Value="12.6" Flags="M"/>
<BOLUS Date="2010-06-13" Time="16:15" Value="10.2" Flags="M"/>
<CARBS Date="2010-06-15" Time="18:55" Value="10.2" Flags="M"/>
</RECORDS>
</DATA>"""

    dataUser = """<?xml version="1.0" encoding="UTF-8" ?>
<DATA>
<DEVICE Type="Hej" Name="Hopp" Model="Hepp" User="thomas" BGUnit="mmol/l"/>
<TRANSFER Date="2010-06-16" Time="18:00"/>
<RECORDS>
<BG Date="2010-06-16" Time="17:55" Value="12.6" Flags="M"/>
<BOLUS Date="2010-06-13" Time="16:15" Value="10.2" Flags="M"/>
<CARBS Date="2010-06-15" Time="18:55" Value="10.2" Flags="M"/>
</RECORDS>
</DATA>"""


    res = AnalyseDiasendGenericXML(dataUser)

    print res
