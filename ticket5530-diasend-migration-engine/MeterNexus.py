# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE GlucoRx Nexus
# @DEVICE GlucoRx Nexus Mini
# @DEVICE GlucoRx Nexus Voice
# @DEVICE FORA Diamond PRIMA
# @DEVICE FORA Diamond MINI
# @DEVICE GlucoRx HCT & Ketone
 
import re
import Common
import struct
from datetime import datetime

from Generic import *
from Defines import *

RECORD_TYPE_PING=0x22
RECORD_TYPE_DEVICE_CLOCK_TIME=0x23
RECORD_TYPE_DEVICE_MODEL=0x24
RECORD_TYPE_DATA_TIME=0x25
RECORD_TYPE_DATA_RESULT=0x26
RECORD_TYPE_DEVICE_SERIAL_START=0x27
RECORD_TYPE_DEVICE_SERIAL_END=0x28
RECORD_TYPE_DEVICE_NR_OF_DATA=0x2B
RECORD_TYPE_DEVICE_TURN_OFF=0x50

FRAME_START_BYTE=0x51
FRAME_STOP_BYTE=0xA5

class NexusAnalyser(object):

    def __init__(self):
        self.results         = []
        self.model           = ''
        self.MAX_NR_RESULTS  = 1000
        self.skipped_records = 0
        self.nr_results      = 0

    def split_data(self, data):

        # None means pair, False means ignore
        nexus_cmd_cb = {
            RECORD_TYPE_PING                :False, # Ping
            RECORD_TYPE_DEVICE_CLOCK_TIME   :False, # Read time
            RECORD_TYPE_DEVICE_MODEL        :self.parse_model_record,
            RECORD_TYPE_DATA_TIME           :None,
            RECORD_TYPE_DATA_RESULT         :self.parse_result_record,
            RECORD_TYPE_DEVICE_SERIAL_START :None,
            RECORD_TYPE_DEVICE_SERIAL_END   :self.parse_serial_record,
            RECORD_TYPE_DEVICE_NR_OF_DATA   :self.parse_number_of_results_record,
            RECORD_TYPE_DEVICE_TURN_OFF     :False # Turn off
        }

        # In the Nexus world all records are 8 bytes long. 
        if (len(data) % 8) != 0:
            return []
        
        responses = [data[i:i+8] for i in range(0, len(data), 8)]
        previous_response_list = None

        cnt = 0
        checksum_ok = True
        for response in responses:
            response_list = [ord(i) for i in response]

            if response_list[0] != FRAME_START_BYTE or response_list[6] != FRAME_STOP_BYTE:
                break

            if (sum(response_list[:-1]) & 0xff) != response_list[7]:
                checksum_ok = False

            cmd = response_list[1]

            if cmd in nexus_cmd_cb:
                if cmd == RECORD_TYPE_DATA_RESULT:
                   cnt += 1
                # Dont allow more than MAX data results
                if cnt > self.MAX_NR_RESULTS and cmd == RECORD_TYPE_DATA_RESULT:
                   break
                if nexus_cmd_cb[cmd]:
                    result = nexus_cmd_cb[cmd](response_list, previous_response_list, checksum_ok)
                    previous_response_list = None
                    checksum_ok = True
                elif nexus_cmd_cb[cmd] == None:
                    previous_response_list = response_list[:]
            else:
                # Unknown command - let's break (this shouldn't happen since we control
                # on the other end which commands that get requested).
                break

        self.results_append({ELEM_NR_RESULTS: self.nr_results - self.skipped_records}, True)

        return self.results

    def results_append(self, result, checksum_ok):
        self.results.append((result, checksum_ok))

    def parse_model_record(self, response, previous_response, checksum_ok):

        models = {
            0x4277:'Nexus', 
            0x4287:'Nexus Mini', 
            0x4280:'Nexus Voice',
            0x4281:'Diamond PRIMA',
            0x4283:'Diamond MINI',
            #0x4278:'evercare genius'
            0x4279:'GlucoRx HCT & Ketone'
            }

        # This is a single record - if we have a pair something is
        # wrong.
        if previous_response != None:
            return

        # Note : model records are not checked for checksum errors
        # by Generic.py - let's just keep it empty. 
        if not checksum_ok:
            return

        model_id = response[2] + response[3] * 0x100
        if model_id in models:
            self.model = models[model_id]
            self.results_append({ELEM_DEVICE_MODEL: models[model_id]}, checksum_ok)

            if self.model == 'GlucoRx HCT & Ketone':
                self.MAX_NR_RESULTS = 2000

    def parse_serial_record(self, response, previous_response, checksum_ok):

        # We need two records to build the serial number.
        if previous_response == None:
            return

        # Note : serial records are not checked for checksum errors
        # by Generic.py - let's just keep it empty. 
        if not checksum_ok:
            return

        if response[1] != RECORD_TYPE_DEVICE_SERIAL_END or previous_response[1] != RECORD_TYPE_DEVICE_SERIAL_START:
            return

        self.results_append({ELEM_DEVICE_SERIAL: '%02x%02x%02x%02x%02x%02x%02x%02x' % (
                response[5], response[4], response[3], response[2], 
                previous_response[5], previous_response[4], 
                previous_response[3], previous_response[2])}, checksum_ok)

    def parse_result_record(self, response, previous_response, checksum_ok):

        type_1_flags = {
            1:FLAG_BEFORE_MEAL, 
            2:FLAG_AFTER_MEAL, 
            3:FLAG_RESULT_CTRL}

        if previous_response == None:
            return

        if response[1] != RECORD_TYPE_DATA_RESULT or previous_response[1] != RECORD_TYPE_DATA_TIME:
            return

        year        = 2000 + (previous_response[3] >> 1)
        month       = ((previous_response[3] & 0x01) << 3) | (previous_response[2] >> 5)
        day         = previous_response[2] & 0x1f
        hour        = previous_response[5] & 0x1f
        minute      = previous_response[4] & 0x3f
        glucose     = response[2] + response[3] * 0x100
        type_1_flag = (response[5] >> 6) & 0x03
        type_2_flag = (response[5] >> 2) & 0x0f
        code        = response[5] & 0x3f
        temperature = response[4]
        
        if self.model == 'GlucoRx HCT & Ketone' and type_2_flag == 0x06:
            # Skip Hematocrit records 
            self.skipped_records += 1
            return

        try:
            dt = datetime(year, month, day, hour, minute)
        except ValueError, e:
            return

        result = {}

        # Ketone value
        if self.model == 'GlucoRx HCT & Ketone' and type_2_flag == 0x07:

            flags = []

            if type_1_flag in type_1_flags:
                flags.append(type_1_flags[type_1_flag])
            elif type_1_flag:
                # Uknown flag or bogus value 
                return
        
            result[ELEM_VAL_TYPE]    = VALUE_TYPE_KETONES
            result[ELEM_TIMESTAMP]   = dt
            result[ELEM_FLAG_LIST]   = flags
            result[ELEM_VAL]         = VAL_FACTOR_GLUCOSE_MMOL * glucose / 30.0
            result[ELEM_DEVICE_UNIT] = "mmol/L"

        # Glucose value
        else:

            glucose = glucose * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
            flags   = []

            if type_1_flag in type_1_flags:
                flags.append(type_1_flags[type_1_flag])
            elif type_1_flag:
                # Uknown flag or bogus value 
                return

            if glucose < 20:
                flags.append(FLAG_RESULT_LOW)
            elif glucose > 600:
                flags.append(FLAG_RESULT_HIGH)

            result[ELEM_VAL_TYPE]    = VALUE_TYPE_GLUCOSE
            result[ELEM_TIMESTAMP]   = dt
            result[ELEM_FLAG_LIST]   = flags
            result[ELEM_VAL]         = glucose
            result[ELEM_DEVICE_UNIT] = "mg/dL"

        self.results_append(result, checksum_ok)

    def parse_number_of_results_record(self, response, previous_response, checksum_ok):

        # This is a single record - if we have a pair something is
        # wrong.
        if previous_response != None:
            return

        # Note : number of results records are not checked for checksum 
        # errors by Generic.py - let's just keep it empty. 
        if not checksum_ok:
            return

        # Refer to p624:#4423 - There are devices that reports 1001 records
        # But the 1001th is just bogus, so ignore that.
        self.nr_results = response[2] + response[3] * 0x100
        if self.nr_results > self.MAX_NR_RESULTS:
            self.nr_results = self.MAX_NR_RESULTS

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalNexusSerialRecord(line):
    """
    Check for serial record.
    """
    if ELEM_DEVICE_SERIAL in line[0]:
        return line[0]
    return {}
    
def EvalNexusModelRecord(line):
    """
    Check for device model. 
    """
    if ELEM_DEVICE_MODEL in line[0]:
        return line[0]
    return {}

def EvalNexusResultRecord(line):
    """
    Is this a result record? If so, return a dictionary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    if ELEM_VAL in line[0]:
        return line[0]
    return {}

def EvalNexusChecksumRecord(line, record):
    """
    Evaluate checksum of result record. Not applicable for this meter.
    """
    return line[1]

def EvalNexusChecksumFile(inList):
    """
    Evaluate checksum of result record.
    """
    return True

def EvalNexusNrResultsRecord(line):
    """
    Is this record a nr results. If so, return a dictionary with nr results.
    """
    if ELEM_NR_RESULTS in line[0]:
        return line[0]
    return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterNexus(inList):
    """
    Detect if data comes from a Nexus.
    """
    return DetectDevice( 'Nexus', inList, DEVICE_METER );

def AnalyseMeterNexus(inData):
    """
    Analyse data from Nexus.
    """

    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ]  = EvalNexusModelRecord
    d[ "eval_serial_record" ]        = EvalNexusSerialRecord
    d[ "eval_result_record" ]        = EvalNexusResultRecord
    d[ "eval_checksum_record" ]      = EvalNexusChecksumRecord
    d[ "eval_checksum_file" ]        = EvalNexusChecksumFile
    d[ "eval_nr_results" ]           = EvalNexusNrResultsRecord

    analyser = NexusAnalyser()

    inList = analyser.split_data(inData)

    resList = AnalyseGenericMeter( inList, d );
    
    return resList

if __name__ == "__main__":

    testfiles = (
        'test/testcases/test_data/GlucoRxHCTKetone/4279316130000015.log',
        'test/testcases/test_data/GlucoRxHCTKetone/4279316130000026.log',
        'test/testcases/test_data/NexusGlucoRx/NexusGlucoRx_1000.log',
        'test/testcases/test_data/NexusGlucoRx/NexusGlucoRx_1001_ZD47191.log',
        'test/testcases/test_data/NexusGlucoRx/NexusGlucoRx_1001_ZD47630.log',
        'test/testcases/test_data/NexusGlucoRx/NexusGlucoRx_1001_ticket4423.log',
        'test/testcases/test_data/NexusGlucoRx/NexusGlucoRx_500.log',
        'test/testcases/test_data/NexusGlucoRx/NexusGlucoRx_Empty.log',
        'test/testcases/test_data/NexusMini/NexusMini_1000.log',
        'test/testcases/test_data/NexusMini/NexusMini_500.log',
        'test/testcases/test_data/NexusMini/NexusMini_Empty.log',
        'test/testcases/test_data/NexusVoice/NexusVoice_225.log',
        'test/testcases/test_data/NexusVoice/NexusVoice_450.log',
        'test/testcases/test_data/NexusVoice/NexusVoice_Empty.log',
        'test/testcases/test_data/ForaDiamond/fora-diamond-mini-de0201-full.bin',
        'test/testcases/test_data/ForaDiamond/fora-diamond-mini-de0202-empty.bin',
        'test/testcases/test_data/ForaDiamond/fora-diamond-mini-de0203-halffull.bin',
        'test/testcases/test_data/ForaDiamond/fora-diamond-mini-de0204-full.bin',
        'test/testcases/test_data/ForaDiamond/fora-diamond-prima-empty.bin',
        'test/testcases/test_data/ForaDiamond/fora-diamond-prima-full.bin',
        'test/testcases/test_data/ForaDiamond/fora-diamond-prima-halffull.bin',
    )

    for testfile in testfiles:
        with open(testfile) as f:
            data = f.read()
            results = AnalyseMeterNexus(data)
            print results[0]['header']
            #for result in results[0]['results']:
            #   print result
