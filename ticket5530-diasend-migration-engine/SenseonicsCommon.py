# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------


from Generic import *
from datetime import datetime, timedelta, MINYEAR
from struct import unpack
import crcmod.predefined
from Buffer import Buffer
from Record import BaseRecord, EnumRecord, EnumBitRecord
from collections import OrderedDict

DEVICE_MODEL_TRANSMITTER          = "Senseonics Transmitter"

ST_START_BYTE = 0x55

ST_RSP_BIT        = 0x80
ST_CMD_PING       = 0x01
ST_CMD_READ_LOG_SENSOR_GLUCOSE_MEASUREMENTS_IN_RECORD_NO_RANGE_LONG_FORMAT = 0x0c
ST_CMD_READ_LOG_SENSOR_GLUCOSE_ALERT_IN_RECORD_NO_RANGE = 0x13
ST_CMD_READ_LOG_OF_BLOOD_GLUCOSE_DATA_IN_RECORD_NO_RANGE_ONE_DATETIME = 0x18
ST_CMD_READ_LOG_OF_PATIENT_EVENTS_IN_RECORD_NO_RANGE = 0x1e
ST_CMD_READ_BLOCK = 0x29
ST_CMD_READ32     = 0x2e

ADDR_BLOOD_GLUC_DATA_LOG_START  = 0x050000
ADDR_BLOOD_GLUC_DATA_LOG_END    = 0x05FFFF

ADDR_PATIENT_EVENT_LOG_START    = 0x060000
ADDR_PATIENT_EVENT_LOG_END      = 0x06FFFF

ADDR_MISC_EVENT_LOG_START       = 0x078000
ADDR_MISC_EVENT_LOG_END         = 0x0A7FFF

ADDR_SENSOR_GLUC_DATA_LOG_START = 0x0B0000
ADDR_SENSOR_GLUC_DATA_LOG_END   = 0x3FFFFF

UUID_MASK_SENSOR_VALUE          = 0x01000000
UUID_MASK_BG_VALUE              = 0x02000000
UUID_MASK_PATIENT_EVENT_VALUE   = 0x04000000

# Seems like the flash page size is 4k
ST_PAGE_SIZE = 0x1000

class EnumTimezoneSign(EnumRecord):
    format = ('B',)
    enums  = {
        'positive' : 0,
        # There is also an undocumented negative (0x55)
        'negative' : 0x55,
        'negative1': 0xff,
    }

class ProductionModeRegisters(BaseRecord):
    address = 0x0000
    format = ('H2I4sI144s4sH',)
    variables = ('production_mode_enabled', 'serial',
                 'model', 'fw_version',
                 'comm_prot_version', 'unparsed', 'fw_version_extension',
                 'post_failure_override')

    def get_version(self):
        """
            Combines the fw version and its extension
        """
        return self.fw_version + self.fw_version_extension

class PassThroughRegisters(BaseRecord):
    address = 0x0404
    format = ('HBHBIHI100s',)
    variables = ('battery_voltage', 'battery_voltage_indicator',
                 'sensor_field_current', 'ready_for_calibration',
                 'date_of_most_recent_sensor_glucose_reading',
                 'most_recent_sensor_glucose_reading',
                 'most_recent_glucose_value',
                 'sensor_id_for_most_recent_sensor_glucose_value', 'unparsed')

class PatientSettingRegisters(BaseRecord):
    address = 0x0800
    format = ('32s32s8sH60s6s2IH', EnumTimezoneSign, '696s',)
    variables = ('last_name', 'first_middle_name', 'date_of_birth',
                 'identifier_len',
                 'identifier_text', 'language', 'linked_sensor_id',
                 'sensor_insertion_date', 'timezone_offset',
                 'timezone_offset_sign', 'unparsed')

    def get_last_name(self):
        return self.last_name.rstrip("\x00")

    def get_first_name(self):
        return self.first_middle_name.rstrip("\x00")

    def get_identifier_text(self):
        return self.identifier_text[self.identifier_len]

class GlucoseAlarmAlertRegisters(BaseRecord):
    address = 0x1000
    format = ('270s',)
    variables = ('unparsed')

class MathParameters(BaseRecord):
    address = 0x1200
    format = ('H',)
    variables = ('start')

class LogRegisters(BaseRecord):
    address = 0x2010
    format = ('2I4H2I4H2I4H2I4H2I4H16s2I4H2I4H2I4H',)
    variables = ('math_model_parameters_start', 'math_model_parameters_end',
                 'math_model_parameters_lowest_record_num', 'pad1',
                 'math_model_parameters_highest_record_num', 'pad2',
                 'sensor_glucose_alert_log_start', 'sensor_glucose_alert_log_end',
                 'sensor_glucose_alert_log_lowest_record_num', 'pad3',
                 'sensor_glucose_alert_log_highest_record_num', 'pad4',
                 'error_log_start', 'error_log_end',
                 'error_log_lowest_record_num', 'pad5',
                 'error_log_highest_record_num', 'pad6',
                 'blood_glucose_data_log_start', 'blood_glucose_data_log_end',
                 'blood_glucose_data_log_lowest_record_num', 'pad7',
                 'blood_glucose_data_log_highest_record_num', 'pad8',
                 'patient_event_log_start', 'patient_event_log_end',
                 'patient_event_log_lowest_record_num', 'pad9',
                 'patient_event_log_highest_record_num', 'pad10',
                 'unparsed',
                 'battery_log_start', 'battery_log_end',
                 'battery_log_lowest_record_num', 'pad11',
                 'battery_log_highest_record_num', 'pad12',
                 'misc_log_start', 'misc_log_end',
                 'misc_log_lowest_record_num', 'pad13',
                 'misc_log_highest_record_num', 'pad14',
                 'sensor_glucose_data_log_start', 'sensor_glucose_data_log_end',
                 'sensor_glucose_data_log_lowest_record_num', 'pad15',
                 'sensor_glucose_data_log_highest_record_num', 'pad16')

def string_num_to_int(string_num, big_endian = True):
    """
        Sometimes 3 bytes are used for numbers, making it impossible to use
        struct for conversion...
    """
    out = 0;
    if big_endian:
        num = string_num
    else:
        num = string_num[::-1]
    for c in num:
        out = (out << 8) | ord(c) 
    return out

class SenseonicsRecord(BaseRecord):
    __crc_func = None

    def __init__(self, buf):
        buf.set_mark()
        # Custom constructor since the text varies in length
        super(SenseonicsRecord, self).__init__(buf)

        if self.variables[-1] == 'crc':
            # Needed for CRC, do not keep the CRC itself, if its available

            self.raw_data = buf.get_slice_from_mark()[:-1]

        self.__crc_func = None
        self.__extractor = None

    def set_raw_data_from_buf(self, buf):
        """
            Store the raw data from the buffer start set by the constructor
        """
        self.raw_data = buf.get_slice_from_mark()

    def format_datetime(self, date, time):
        utc_dt = self.format_utc_datetime(date, time)
        return self.__extractor.get_local_time(utc_dt)

    def format_utc_datetime(self, date, time):
        return datetime(2000 + (date >> 9), (date >> 5) & 0x0f, date & 0x1f,
                        time >> 11, (time >> 5) & 0x3f, (time & 0x1f) << 1)

    def set_crc_func(self, crc_func):
        """
            Set the CRC function, this to share the function between
            objects since it takes time to create one for each...
        """
        self.__crc_func = crc_func

    def set_extractor(self, extractor):
        """
            Set the extractor object. Will be used later for instance to calc
            local time
        """
        self.__extractor = extractor

    def get_record_number(self):
        """
            The record number is a 3-byter so manual conversion needed
        """
        return string_num_to_int(self.record_number)

    def get_datetime(self):
        return self.format_datetime(self.date, self.time)

    def get_utc_datetime(self):
        return self.format_utc_datetime(self.date, self.time)

    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'SenseonicsRecord : Subclass did not implement convert')

    def validate_crc(self):
        calc_crc = self.__crc_func(self.raw_data)
        return calc_crc == self.crc

class SensorGlucoseRecord(SenseonicsRecord):
    format = ('<3s3HBI6HB')
    variables = ('record_number', 'date', 'time', 'value', 'alert_flags',
                 'sensor_id', 'sensor_temp', 'signal_led_on', 'ref_led_on',
                 'signal_led_off', 'ref_led_off', 'accel_value', 'accel_temp')

    def get_sensor_temp(self):
        """
            The temperature value is:
            Unsigned offset from 15 ̊C (Value 0x00 is 15 ̊C) (Each
            LSB increment represents a temperature increment of
            0.0146 ̊C, max T = 45 ̊C)
        """
        return 15.0 + float(self.sensor_temp) * 0.0146

    def convert_to_diasend(self):
        flags = [FLAG_CONTINOUS_READING]
        if self.alert_flags & 0x01:
            flags.append(FLAG_RESULT_LOW)

        if self.alert_flags & 0x02:
            flags.append(FLAG_RESULT_HIGH)

        if self.value == 0xFFFF:
            # Not a valid Sensor value
            blinding_reason = self.alert_flags >> 3
            if blinding_reason == 4:
                # GU_OutsidePhysRange
                flags.append(FLAG_OUTSIDE_RANGE)
            elif blinding_reason == 7:
                # GU_InvalidCalMgrPhase
                flags.append(FLAG_CALIBRATION_ERROR)
            elif blinding_reason == 8:
                # GU_SensorRetired
                flags.append(FLAG_STRIP_WARNING)
            elif blinding_reason == 11:
                #GU_EmptyBattery
                flags.append(FLAG_LOW_BATTERY)
            else:
                # Other reasons
                pass
            
            # Note: Currently, we choose not to store these "Sensor" values since se have no good way of displaying them
            # TODO: Maybe revise this
            return {}

# Currently far too slow to insert with UUIDs
#        val_list = [{ELEM_VAL_TYPE: VALUE_TYPE_UUID,
#                     ELEM_VAL: UUID_MASK_SENSOR_VALUE | self.get_record_number()}]
        val_list = []

        return  {
            ELEM_VAL_TYPE   : VALUE_TYPE_GLUCOSE,
            ELEM_VAL        : self.value * VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL,
            ELEM_TIMESTAMP  : self.get_datetime(),
            ELEM_FLAG_LIST  : flags,
            ELEM_VALUE_LIST : val_list,
        }

class SensorGlucoseRecordCrc(SensorGlucoseRecord):

    def __init__(self, buf):
        super(SensorGlucoseRecordCrc, self).__init__(buf)
        self.set_raw_data_from_buf(buf)
        # Add the CRC..
        (self.crc,) = buf.get_struct('B')

class BloodGlucoseRecord(SenseonicsRecord):
    format = ('2s4HB3s')
    variables = ('record_number', 'sample_date', 'sample_time', 'value',
                 'meter_identifier', 'flags', 'sensor_record_number')

    def get_sample_datetime(self):
        return self.format_datetime(self.sample_date, self.sample_time)

    def convert_to_diasend(self):
        flags = []
        if self.flags == 0:
            # Flag 0: Not entered for calibration
            # This means a normal BG value 
            flags.append(FLAG_MANUAL)
        else:
            # Flag 1-11
            # All are considered CGM Calibrations
            flags.append(FLAG_CALIBRATION)
            flags.append(FLAG_CONTINOUS_READING)
            if self.flags >= 2:
                # Flag 2-11: Rejected for different reasons
                flags.append(FLAG_CALIBRATION_ERROR)
            if self.flags == 2:
                # Flag 2: Entered for Calibration, but marked as suspicious (Blood Glucose too different from Sensor Glucose)
                flags.append(FLAG_SUSPICIOUS)
            elif self.flags == 3:
                # Flag 3: Entered for calibration but not used (i.e., rejected) because it was too low (<40)
                flags.append(FLAG_RESULT_LOW)
            elif self.flags == 4:
                # Flag 4: Entered for calibration but not used (i.e., rejected) because it was too high (>400)
                flags.append(FLAG_RESULT_HIGH)

# Currently far too slow to insert with UUIDs
#        val_list = [{ELEM_VAL_TYPE: VALUE_TYPE_UUID,
#                     ELEM_VAL: UUID_MASK_BG_VALUE | self.get_record_number()}]
        val_list = []

        return  {
            ELEM_VAL_TYPE   : VALUE_TYPE_GLUCOSE,
            ELEM_VAL        : self.value * VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL,
            ELEM_TIMESTAMP  : self.get_sample_datetime(),
            ELEM_FLAG_LIST  : flags,
            ELEM_VALUE_LIST : val_list,
        }

class BloodGlucoseRecordCrc(BloodGlucoseRecord):
    format = ('2s6HB3sB')
    variables = ('record_number', 'entry_date', 'entry_time', 'sample_date',
                 'sample_time', 'value', 'meter_identifier', 'flags',
                 'sensor_record_number', 'crc')

    def get_entry_datetime(self):
        return self.format_datetime(self.entry_date, self.entry_time)

class EnumPatientEventType(EnumBitRecord):
    format = ('B',)
    enums  = {
        'insulin_bolus' : 0x01,
        'meal'          : 0x02,
        'exercise'      : 0x04,
        'health'        : 0x08,
        'deleted'       : 0x80,
    }

class PatientEventRecord(SenseonicsRecord):
    format = ('2s2H', EnumPatientEventType, '2HB')
    variables = ('record_number', 'date', 'time', 'type',
                 'sub_type', 'sub_type_quantity', 'text_len')

    def __init__(self, buf):
        # Custom constructor since the text varies in length
        super(PatientEventRecord, self).__init__(buf)
        
        if self.text_len:
            (self.text,) = buf.get_struct("%ds" % self.text_len)
        else:
            self.text = ""

    def convert_to_diasend(self):
        val_type = None
        val = None
# Currently far too slow to insert with UUIDs
#        val_list = [{ELEM_VAL_TYPE: VALUE_TYPE_UUID,
#                     ELEM_VAL: UUID_MASK_PATIENT_EVENT_VALUE | self.get_record_number()}]
        val_list = []

        flags = [FLAG_MANUAL]
        if self.type.bits['insulin_bolus']:
            if self.sub_type == 0:
                flags.append(FLAG_INSULIN_IMPACT_RAPID_ACTING)
            elif self.sub_type == 1:
                flags.append(FLAG_INSULIN_IMPACT_SHORT_ACTING)
            elif self.sub_type == 2:
                flags.append(FLAG_INSULIN_IMPACT_INTERMEDIATE)
            elif self.sub_type == 3:
                flags.append(FLAG_INSULIN_IMPACT_LONG)
            elif self.sub_type == 4:
                flags.append(FLAG_INSULIN_IMPACT_MIX)
            elif self.sub_type == 5:
                flags.append(FLAG_INSULIN_IMPACT_MIX)
            elif self.sub_type == 6:
                flags.append(FLAG_INSULIN_IMPACT_MIX)
            elif self.sub_type == 7:
                flags.append(FLAG_INSULIN_IMPACT_OTHER)

            val_type = VALUE_TYPE_INS_BOLUS
            # Divide by ten since the quantity is in 10ths of units
            val = self.sub_type_quantity * VAL_FACTOR_BOLUS / 10

        elif self.type.bits['meal']:
            val_type = VALUE_TYPE_CARBS
            val = self.sub_type_quantity * VAL_FACTOR_CARBS
            if self.sub_type == 0:
                flags.append(FLAG_BREAKFAST)
            elif self.sub_type == 1:
                flags.append(FLAG_LUNCH)
            elif self.sub_type == 2:
                flags.append(FLAG_DINNER)
            elif self.sub_type == 3:
                flags.append(FLAG_SNACK)

        elif self.type.bits['exercise']:
            val_type = VALUE_TYPE_EXERCISE
            val = 0
            val_list.append({ELEM_VAL : self.sub_type_quantity, ELEM_VAL_TYPE : VALUE_TYPE_DURATION})

            if self.sub_type == 0:
                flags.append(FLAG_MILD_EXERCISE)
            elif self.sub_type == 1:
                flags.append(FLAG_MEDIUM_EXERCISE)
            elif self.sub_type == 2:
                flags.append(FLAG_HARD_EXERCISE)

        elif self.type.bits['health']:
            # TODO: Health events?
            pass

        if val_type:
            if self.type.bits['deleted']:
                flags.append(FLAG_RESULT_DELETED)

            return { \
                ELEM_VAL_TYPE   : val_type,
                ELEM_VAL        : val,
                ELEM_FLAG_LIST  : flags,
                ELEM_TIMESTAMP  : self.get_datetime(),
                ELEM_VALUE_LIST : val_list,
            }
        else:
            return {}

class PatientEventRecordCrc(PatientEventRecord):

    def __init__(self, buf):
        super(PatientEventRecordCrc, self).__init__(buf)
        
        self.set_raw_data_from_buf(buf)
        (self.crc,) = buf.get_struct("B")

class MiscEventRecord(SenseonicsRecord):
    format = ('4H8sB')
    variables = ('record_number', 'date', 'time', 'type', 'addition_info',
                 'crc')

    @classmethod
    def create_obj(cls, buf):
        """
            Creates a instance of a RecordLogEntry subclass
        """
        # glance at the type
        (_, _, _, event_type) = buf.peek_struct('4H')

        for sub_class in cls.__subclasses__():
            if sub_class.event_type == event_type:

                return sub_class(buf)

        # TODO: For now all events are not implemented since they are unused
        # and some are undocumented

        return MiscEventRecord(buf)

class MiscLinkSensorIdChangeRecord(MiscEventRecord):
    event_type = 0x00
    format = ('4H4s4sB',)
    variables = ('record_number', 'date', 'time', 'type', 'old_id', 'new_id',
                 'crc')

class MiscTimeZoneChangeRecord(MiscEventRecord):
    event_type = 0x01
    format = ('4HH', EnumTimezoneSign, 'H', EnumTimezoneSign, '2sB')
    variables = ('record_number', 'date', 'time', 'type', 'old_tz_offset',
                 'old_tz_offset_sign', 'new_tz_offset', 'new_tz_offset_sign',
                 'padding', 'crc')

class MiscSensorEepromChangeRecord(MiscEventRecord):
    event_type = 0x02
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorInsertDateChangeRecord(MiscEventRecord):
    event_type = 0x03
    format = ('4H2H4sB',)
    variables = ('record_number', 'date', 'time', 'type', 'old_crc', 'new_crc',
                 'padding', 'crc')

class MiscSensorEepromGainChangeRecord(MiscEventRecord):
    event_type = 0x04
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromBlinksRecord(MiscEventRecord):
    event_type = 0x05
    format = ('4HH6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'blinks',
                 'padding', 'crc')

class MiscSensorEepromGain2ChangeRecord(MiscEventRecord):
    event_type = 0x06
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromGain3ChangeRecord(MiscEventRecord):
    event_type = 0x07
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromGain4ChangeRecord(MiscEventRecord):
    event_type = 0x08
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromGain5ChangeRecord(MiscEventRecord):
    event_type = 0x09
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromGain6ChangeRecord(MiscEventRecord):
    event_type = 0x0a
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromGain7ChangeRecord(MiscEventRecord):
    event_type = 0x0b
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorEepromGain8ChangeRecord(MiscEventRecord):
    event_type = 0x0c
    format = ('4H8sB',)
    # Data spec is unclear
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscSensorLowGlucoseAlarmTshldChangeRecord(MiscEventRecord):
    event_type = 0x12
    format = ('4HB2H3sB')
    variables = ('record_number', 'date', 'time', 'type', 'interval_changed',
                 'old_thsld', 'new_thsld', 'padding', 'crc')

class MiscSensorHighGlucoseAlarmTshldChangeRecord(MiscEventRecord):
    event_type = 0x13
    format = ('4HB2H3sB',)
    variables = ('record_number', 'date', 'time', 'type', 'interval_changed',
                 'old_thsld', 'new_thsld', 'padding', 'crc')

class MiscFallingRateAlertTshldChangeRecord(MiscEventRecord):
    event_type = 0x14
    format = ('4HB2H3sB',)
    variables = ('record_number', 'date', 'time', 'type', 'alert_num_changed',
                 'old_thsld', 'new_thsld', 'padding', 'crc')

class MiscRisingRateAlertTshldChangeRecord(MiscEventRecord):
    event_type = 0x15
    format = ('4HB2H3sB',)
    variables = ('record_number', 'date', 'time', 'type', 'alert_num_changed',
                 'old_thsld', 'new_thsld', 'padding', 'crc')

class MiscSensorLowGlucoseTargetTshldChangeRecord(MiscEventRecord):
    event_type = 0x18
    format = ('4H3B5sB',)
    variables = ('record_number', 'date', 'time', 'type', 'interval_changed',
                 'old_thsld', 'new_thsld', 'padding', 'crc')

class MiscSensorHighGlucoseTargetTshldChangeRecord(MiscEventRecord):
    event_type = 0x19
    format = ('4HB2H3sB',)
    variables = ('record_number', 'date', 'time', 'type', 'interval_changed',
                 'old_thsld', 'new_thsld', 'padding', 'crc')

class MiscSensorFieldCurrentRecord(MiscEventRecord):
    event_type = 0x21
    format = ('4H2s6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'coupling',
                 'padding', 'crc')

class MiscSensorDiagLogRecord(MiscEventRecord):
    event_type = 0x22
    format = ('4H2s6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'rcon',
                 'padding', 'crc')

class MiscSystemRestartRecord(MiscEventRecord):
    event_type = 0x23
    format = ('4H2s6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'bytes',
                 'padding', 'crc')

class MiscCalibrationPhaseChangeRecord(MiscEventRecord):
    event_type = 0x25
    format = ('4H2BH4sB',)
    variables = ('record_number', 'date', 'time', 'type', 'prev_phase',
                  'new_phase', 'num_calibs', 'padding', 'crc')

class MiscRedyForCalibrationRecord(MiscEventRecord):
    event_type = 0x26
    format = ('4H2B6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'old_val',
                  'new_val', 'padding', 'crc')

class MiscBleKeyChangeRecord(MiscEventRecord):
    event_type = 0x29
    format = ('4HB7sB',)
    variables = ('record_number', 'date', 'time', 'type', 'change', 'padding',
                 'crc')

class MiscModeChangeRecord(MiscEventRecord):
    event_type = 0x2a
    format = ('4H2B6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'prev_mode',
                  'new_mode', 'padding', 'crc')

class MiscMostRecentSensorIdRecord(MiscEventRecord):
    event_type = 0x2b
    format = ('4H8sB',)
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscBleDisconnectRecord(MiscEventRecord):
    event_type = 0x2c
    format = ('4H8sB',)
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscAlgoParamsUpdatedRecord(MiscEventRecord):
    event_type = 0x2d
    format = ('4H8sB',)
    variables = ('record_number', 'date', 'time', 'type', 'padding', 'crc')

class MiscLogsErasedRecord(MiscEventRecord):
    event_type = 0x2f
    format = ('4H2B6sB',)
    variables = ('record_number', 'date', 'time', 'type', 'log', 'cause',
                 'padding', 'crc')

class GlucoseAlertRecord(SenseonicsRecord):
    format = ('3HB3H')
    variables = ('record_number', 'date', 'time', 'type',
                 'value', 'trend', 'threshold_or_time_interval')

class SenseonicsResponse(BaseRecord):

    @classmethod
    def create_obj(cls, buf, extractor = None):
        """
            Creates a instance of a SenseonicsResponse subclass
        """
        # glance at the response ID
        (response_id,) = buf.get_struct('B')

        for sub_class in cls.__subclasses__():
            if sub_class.response_id == response_id:

                return sub_class(buf, extractor)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'SenseonicsResponse : Response ID not implemented: 0x%02x' % response_id)

    def get_objects(self):
        """
            Returh the sub objects of the object
        """
        if hasattr(self, 'records'):
            return self.records
        else:
            return [self]
        

class PingResponse(SenseonicsResponse):
    format = ('I8s')
    variables = ('id', 'name')
    response_id = ST_CMD_PING | ST_RSP_BIT

    def __init__(self, buf, extractor):
        super(PingResponse, self).__init__(buf)

    def get_serial(self):
        # The serial is actually the name, prepended by 0:s
        pass

class ReadLogSensorGlucoseMeasInRecordNoRangeLongFormat(SenseonicsResponse):
    format = ('B3s3s')
    variables = ('unit', 'first_record', 'end_record')
    response_id = ST_CMD_READ_LOG_SENSOR_GLUCOSE_MEASUREMENTS_IN_RECORD_NO_RANGE_LONG_FORMAT | ST_RSP_BIT

    def __init__(self, buf, extractor):
        super(ReadLogSensorGlucoseMeasInRecordNoRangeLongFormat, self).__init__(buf)
        self.records = []
        for i in range(self.get_count()):
            record = SensorGlucoseRecord(buf)
            record.set_extractor(extractor)

            self.records.append(record)

    def get_count(self):
        """
            Of some strange reason first record number is useless and
            end record number is actually the count(!!)
        """
        return string_num_to_int(self.end_record, False)


class ReadLogBloodGlucoseDataInRecordNoRangeOneDatetime(SenseonicsResponse):
    format = ('2HB')
    variables = ('first_record', 'end_record', 'unit')

    response_id = ST_CMD_READ_LOG_OF_BLOOD_GLUCOSE_DATA_IN_RECORD_NO_RANGE_ONE_DATETIME | ST_RSP_BIT

    def __init__(self, buf, extractor):
        super(ReadLogBloodGlucoseDataInRecordNoRangeOneDatetime, self).__init__(buf)
        self.records = []
        for i in range(self.first_record, self.end_record + 1):
            record = BloodGlucoseRecord(buf)
            record.set_extractor(extractor)

            self.records.append(record)

class ReadLogOfPatientEventsInRecordNoRange(SenseonicsResponse):
    format = ('2H')
    variables = ('first_record', 'end_record')

    response_id = ST_CMD_READ_LOG_OF_PATIENT_EVENTS_IN_RECORD_NO_RANGE | ST_RSP_BIT

    def __init__(self, buf, extractor):
        super(ReadLogOfPatientEventsInRecordNoRange, self).__init__(buf)
        self.records = []
        for i in range(self.first_record, self.end_record + 1):
            # Note that the record ID's does not match the header..
            record = PatientEventRecord(buf)
            record.set_extractor(extractor)

            self.records.append(record)


class ReadLogSensorGlucoseAlertInRecordNoRange(SenseonicsResponse):
    format = ('2HIB')
    variables = ('first_record', 'end_record', 'sensor_id', 'unit')

    response_id = ST_CMD_READ_LOG_SENSOR_GLUCOSE_ALERT_IN_RECORD_NO_RANGE | ST_RSP_BIT

    def __init__(self, buf, extractor):
        super(ReadLogSensorGlucoseAlertInRecordNoRange, self).__init__(buf)
        self.records = []
        for i in range(self.first_record, self.end_record + 1):
            record = GlucoseAlertRecord(buf)
            record.set_extractor(extractor)

            self.records.append(record)


def EvalSenseonicsResultRecord(obj):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    ELEM_TIMESTAMP  > date in datetime object
    ELEM_VAL        > value (float) 
    ELEM_VAL_TYPE   > type of value (VALUE_TYPE_GLUCOSE)
    ELEM_FLAG_LIST > list of flags (int) if present
    """
    # TODO: partient events?
    if isinstance(obj, SensorGlucoseRecord) or \
       isinstance(obj, BloodGlucoseRecord) or \
       isinstance(obj, PatientEventRecord):
        return obj.convert_to_diasend()
    return {}

def EvalSenseonicsUnitRecord(obj):
    """
    Always return mg/dl 
    """
    return {ELEM_DEVICE_UNIT : "mg/dl"}


