# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Bayer Ascensia Elite XL
# @DEVICE Bayer Ascensia Dex
# @DEVICE Bayer Ascensia Breeze2
# @DEVICE Bayer Ascensia Confirm
# @DEVICE Bayer Contour
# @DEVICE Bayer Contour XT
# @DEVICE Bayer Contour Link
# @DEVICE Menarini G+
# @DEVICE Bayer 'generic device'
# @DEVICE Bayer Contour Next EZ
# @DEVICE Bayer Contour Plus
# @DEVICE Bayer Contour TS

import re
from Generic import *
from Defines import *
from datetime import datetime

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalDateTime( dateTimeStr ):
    """
    Evalute if a string is a date/time string (12 characters - only digits)
    """
    m = re.match( r'\d{12,12}', dateTimeStr, re.IGNORECASE )
    if m:
        return True

    return False

def EvalHeaderRecord( line ):
    """
    Evaluate a Bayer Contour header record. Extract groups :
    group 1 : Model
    group 2 : Software version
    group 3 : Serial number

    Example from a Contour Link
    '\x021H|\\^&||-8251|Bayer7150^3.03\\0.02^7778-1875766|||||||P|1|200811191054\r\x1785\r\n'
    
    """
    # Contours
    # Contour SW 1.xx sends '-', SW 2.xx sends 'A', SW 4.xx sends 'H', XT sends 'P' prior the serial number (not currently included though)
    # Contour TS
    # Contour link
    # Breeze, Breeze 2, Dex, Elite, Confirm
    m = re.match( r'.*Bayer(.*)\^(\d+\.\d+)\\.*\^(\d{4}.)(.*)\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*', line, re.IGNORECASE )

    # Menarini G+
    if not m:
        m = re.match( r'.*(GT-1820)\^(\d+\.\d+).*1820-(\d+)\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*', line, re.IGNORECASE )
    return m

def EvalResultRecord( line ):
    """
    Evaluate a Bayer Contour result record. Extract groups :
    group 1 : value
    group 2 : unit
    group 3 : flags - temp/lo/high
    group 4 : flags

    group 6 : year
    group 7 : month
    group 8 : day
    group 9 : hour
    group 10 : minute
    group 11 : checksum
    """
    m = re.match( r'\x02\dR\|\d{1,4}\|\^\^\^Glucose\|([\d|\.]{1,5})\|(mg/dl|mmol/l)\^[B|P|C]\|\|([\<\>\\T]*)\|([ABD]*)\|([AED\\]*)\|\|\|(\d{4,4})(\d{2,2})(\d{2,2})(\d{2,2})(\d{2,2})[^\da-f]*([\da-f]{2,2})', line, re.IGNORECASE )
    return m
    

def EvalTerminationRecord( line ):
    """
    Evaluate a Bayer Contour Termination Record
    """
    m = re.match( r'\x02\dL\|1\|N(\s+)', line, re.IGNORECASE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalBayerAscenciaContourTerminationRecord( line ):
    """Is this line a termination record!"""
    m = EvalTerminationRecord( line )
    if m:
        return True
    return False

def EvalBayerAscensiaContourSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(3) + m.group(4)
    return res


def EvalBayerAscensiaDeviceModelRecord( line ):
    """
    Is this line a model name record. If so, return a dictionary with the device model name.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        family_no = m.group(1)
        model_no = m.group(3)
        if family_no == "3883":
            res[ ELEM_DEVICE_MODEL ] = "Ascensia Elite XL"
        elif family_no == "3950":
            res[ ELEM_DEVICE_MODEL ] = "Ascensia Dex"
        elif family_no == "6115":
            res[ ELEM_DEVICE_MODEL ] = "Ascensia Breeze2"
        elif family_no == "6116":
            res[ ELEM_DEVICE_MODEL ] = "Ascensia Confirm"
        elif family_no == "7150":
            if model_no in ["7776-", "7777-", "7778-", "7779-"]:
                res[ ELEM_DEVICE_MODEL ] = "Bayer Contour Link"
            else:
                res[ ELEM_DEVICE_MODEL ] = "Bayer Contour"
        elif family_no == "GT-1820":
            res[ ELEM_DEVICE_MODEL ] = "Menarini G+"
        elif family_no == '7160':
            res[ ELEM_DEVICE_MODEL ] = "Bayer Contour Next EZ"
        elif family_no == '7600':
            res[ ELEM_DEVICE_MODEL ] = "Bayer Contour Plus"
        elif family_no == '1779A':
            res[ ELEM_DEVICE_MODEL ] = "Bayer Contour TS"

    return res


def EvalBayerAscensiaVersionRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        return {ELEM_SW_VERSION : m.group(2)}
    return res


def EvalBayerAscensiaContourUnitRecord( line ):
    """
    Always return empty string (unit part of result)
    """
    res = { "meter_unit":"" }
    return res

def EvalBayerAscensiaContourResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """   
    res = {}
    mt = EvalTerminationRecord( line )
    if mt:
        for c in mt.group(0):
            c1 = int(line[9],16)
            c2 = int(line[10],16)
            if 0x8 & c2:
                res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MMOLL
            else:
                res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MGDL
            return { SETTINGS_LIST: res }
   
    m = EvalResultRecord( line )
    if m:
        # Convert date (is in YYYYMMDDTTTT)
        try:
            res[ ELEM_TIMESTAMP ] = datetime(int(m.group(6)), int(m.group(7)), int(m.group(8)), int(m.group(9)), int(m.group(10)))
        except:
            res[ ELEM_TIMESTAMP ] = None


        # Convert value to float
        try:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
            if m.group(2) == "mmol/L":
                res[ ELEM_VAL ] = float( m.group(1) ) * 10.0
            else:
                # mg/dL values are not needed to multiply by 10
                res[ ELEM_VAL ] = float( m.group(1) ) * VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                
            res[ ELEM_DEVICE_UNIT ]  = m.group(2)
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }
         
        _flags = []
        # Check the flags
        if m.group(4) == 'B':
            _flags.append(FLAG_BEFORE_MEAL)
        elif m.group(4) == 'A':
            _flags.append(FLAG_AFTER_MEAL)        
        elif m.group(4) == 'D':
            _flags.append(FLAG_LOGBOOK)
        elif m.group(5) == "E\\A": # This is only present in (GT-1820 aka Menarini G+)
            _flags.append(FLAG_AFTER_MEAL)
        # An "E\\D" or "D" flag would mean record deleted, but the usage is different
        # for different meters and it could mean "control".

        if m.group(3) == '<':
            _flags.append(FLAG_RESULT_LOW)
        elif m.group(3) == '>':
            _flags.append(FLAG_RESULT_HIGH)        
        elif m.group(3) == 'T':
            _flags.append(FLAG_RESULT_OUTSIDE_TEMP)
            
        res [ELEM_FLAG_LIST] = _flags

    return res
    
def EvalBayerAscensiaContourChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    checkSumma = 0
    for c in line[1:-3]:
        checkSumma = ( checkSumma + ord(c) ) % 256
        
    m = EvalResultRecord( line )
    if m:
        try:
            if checkSumma == int(m.group(11), 16):
                return True
            else:
                return False
        except ValueError:
            return False        
    else:
        mt = EvalTerminationRecord(line)
        if mt:
            # Dont checksum termination record
            return True
        return False

    return True
    
def EvalBayerAscensiaContourChecksumFile( inList ):
    """
    Evaluate checksum of file.
    """
    return True

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectBayerAscensiaContour( inList ):
    """
    Detect if data comes from a Bayer Contour.
    """
    return DetectDevice( 'BayerAscensiaContour', inList, DEVICE_METER )

def AnalyseBayerAscensiaContour( inData ):
    """
    Analyse Bayer Contour
    """

    inList = inData.split('\n')

    # Empty dictionary
    d = {}
    
    d[ "eval_serial_record" ]       = EvalBayerAscensiaContourSerialRecord
    d[ "eval_device_model_record" ] = EvalBayerAscensiaDeviceModelRecord
    d[ "eval_device_version" ]      = EvalBayerAscensiaVersionRecord
    d[ "eval_unit"]                 = EvalBayerAscensiaContourUnitRecord
    d[ "eval_result_record" ]       = EvalBayerAscensiaContourResultRecord
    d[ "eval_checksum_record" ]     = EvalBayerAscensiaContourChecksumRecord
    d[ "eval_checksum_file" ]       = EvalBayerAscensiaContourChecksumFile
    d[ "eval_termination_record" ]  = EvalBayerAscenciaContourTerminationRecord   
    
    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":

    testfiles = (
        'Contour.log',
        'ContourXT.log',
        '7160P2975771.log',
        '7600P1788982.log',
        '1779A1002764.log',
        '1779A1002775.log',
        '1779A1002981.log',)

    for testfile in testfiles:
        with open('test/testcases/test_data/BayerAscensiaContour/%s' % (testfile), 'r') as f:
            d = f.read()
            results = AnalyseBayerAscensiaContour(d)
            print testfile, results[0]['header']
            #for r in results[0]['results']:
            #    print r


