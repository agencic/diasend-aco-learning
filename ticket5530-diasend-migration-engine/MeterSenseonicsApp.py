# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Senseonics Transmitter

from SenseonicsCommon import *
from Generic import *
try:
    from elementtree import ElementTree
except:
    # Python 2.5
    from xml.etree import ElementTree

from xml.parsers.expat import ExpatError
from base64 import b64decode

# --- controller
class SenseonicsDataExtractor(object):

    def __init__(self, inData):
        self.__blobs = []
        self.__device_type = None
        self.__device_id = None
        try:
            tree = ElementTree.fromstring(inData)
            # Find the body...
            body = tree.find("{http://www.w3.org/2003/05/soap-envelope}Body");
            uploaded_events = body.find("{http://tempuri.org/}UploadDeviceEvents")
            for item in list(uploaded_events):
                if item.tag == "{http://tempuri.org/}deviceType":
                    self.__device_type = item
                elif item.tag == "{http://tempuri.org/}data":
                    for data in list(item):
                        if data.tag == "{http://schemas.microsoft.com/2003/10/Serialization/Arrays}base64Binary":
                            self.__blobs.append(b64decode(data.text))
                elif item.tag == "{http://tempuri.org/}deviceID":
                    self.__device_id = item
        except ExpatError, v:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Invalid xml')

    def split(self):
        records = [self]
        for blob in self.__blobs:
            buf = Buffer(blob)
            while buf.nr_bytes_left() > 0:
                response = SenseonicsResponse.create_obj(buf, self)
                records.extend(response.get_objects())

        return records

    def get_local_time(self, dt):
        """
            Get the local time from the UTC date time
            So far we do not get the misc log from the app, so just return the
            dt since we can not do any translation
        """
        return dt

    def get_serial(self):
        if self.has_serial():
            return self.__device_id.text
        else:
            return 'UNKNOWN'

    def get_model(self):
        return DEVICE_MODEL_TRANSMITTER

    def has_model(self):
        return self.__device_type != None

    def has_serial(self):
        return self.__device_id != None

    @classmethod
    def evalModelRecord(_, obj):
        if isinstance(obj, SenseonicsDataExtractor) and obj.has_model():
            return {ELEM_DEVICE_MODEL: obj.get_model()}
        else:
            return {}

    @classmethod
    def evalSerialRecord(_, obj):
        if isinstance(obj, SenseonicsDataExtractor) and obj.has_serial():
            return {ELEM_DEVICE_SERIAL: obj.get_serial()}
        else:
            return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterSenseonicsApp( inList ):
    """
    Detect if data comes from a Senseonics App.
    """
    return DetectDevice( 'SenseonicsTransmitterAppBinary', inList, DEVICE_METER )

def AnalyseMeterSenseonicsApp( inData ):
    """
    Analyse Senseonics transmitter data from their App.
    """

    # Empty dictionary
    callbacks = {
        "eval_serial_record"        : SenseonicsDataExtractor.evalSerialRecord,
        "eval_device_model_record"  : SenseonicsDataExtractor.evalModelRecord,
        "eval_unit"                 : EvalSenseonicsUnitRecord,
        "eval_result_record"        : EvalSenseonicsResultRecord,
    }

    try:
        extractor = SenseonicsDataExtractor(inData)
    except Exception, e:

        import traceback
        traceback.print_exc()

        serial_number = 'UNKNOWN'

        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(DEVICE_MODEL_TRANSMITTER,
                                                     serial_number, e.args[0],
                                                     e.args[1])
        else:
            error_response = CreateErrorResponseList(DEVICE_MODEL_TRANSMITTER,
                                                     serial_number,
                                                     ERROR_CODE_PREPROCESSING_FAILED,
                                                     ''.join(map(str, e.args)))

        return error_response

    try:
        objects = extractor.split()

    except Exception, e:

        import traceback
        traceback.print_exc()

        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(extractor.get_model(),
                                                     extractor.get_serial(), e.args[0],
                                                     e.args[1])
        else:
            error_response = CreateErrorResponseList(extractor.get_model(),
                                                     extractor.get_serial(),
                                                     ERROR_CODE_PREPROCESSING_FAILED,
                                                     ''.join(map(str, e.args)))

        return error_response

    return AnalyseGenericMeter(objects, callbacks)

if __name__ == "__main__":

#    testfile = open('test/testcases/test_data/SenseonicsApp/AndroidSoapTest.xml')
    testfile = open('test/testcases/test_data/SenseonicsApp/iOSSoapTest.xml')
    testcase = testfile.read()
    testfile.close()
    
    ret = AnalyseMeterSenseonicsApp(testcase)
    print ret
