#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <memory.h>
#include <stdint.h>
#include "aes.h"

#define BLOCK_LEN   16
#define KEY_LENGTH  16

void decrypt( char *dst, char *src, size_t src_len )
{
    char            buf1[BLOCK_LEN], buf2[BLOCK_LEN], dbuf[2*BLOCK_LEN];
    char            *b1, *b2, *bt, *src_ptr, *dst_ptr;
    unsigned long   i, len, rlen;
    aes_ctx         ctx[1];
    const uint8_t   key[KEY_LENGTH] = {0x08,0xcd,0x4e,0xff,0x56,0x12,0x84,0x3d,0x40,0xdc,0x79,0x5f,0x66,0xb7,0x26,0x83};

    memset(ctx,0,sizeof(ctx[0]));
    aes_dec_key(key, KEY_LENGTH, ctx);
    
    rlen = src_len;
    src_ptr = src;
    dst_ptr = dst;
    
    // we need two input buffers because we have to keep the previous
    // ciphertext block - the pointers b1 and b2 are swapped once per
    // loop so that b2 points to new ciphertext block and b1 to the
    // last ciphertext block

    rlen -= BLOCK_LEN; b1 = buf1; b2 = buf2;

    memcpy( b1, src, BLOCK_LEN );
    src_ptr += BLOCK_LEN;

    // read the encrypted file a block at a time
    while(rlen > 0)
    {
        // input a block and reduce the remaining byte count
        (void)memcpy(b2,src_ptr, BLOCK_LEN);
        src_ptr += BLOCK_LEN;
        rlen -= BLOCK_LEN;

        // decrypt input buffer
        aes_dec_blk(b2, dbuf, ctx);

        // if there is only one more block do ciphertext stealing
        if(rlen > 0 && rlen < BLOCK_LEN)
        {
            // read last ciphertext block
            (void)memcpy(b2,src_ptr,BLOCK_LEN);
            src_ptr += BLOCK_LEN;
            
            // append high part of last decrypted block
            for(i = rlen; i < BLOCK_LEN; ++i)
                b2[i] = dbuf[i];

            // decrypt last block of plaintext
            for(i = 0; i < rlen; ++i)
                dbuf[i + BLOCK_LEN] = dbuf[i] ^ b2[i];

            // decrypt last but one block of plaintext
            aes_dec_blk(b2, dbuf, ctx);

            // adjust length of last output block
            len = rlen + BLOCK_LEN; rlen = 0;
        }

        // unchain CBC using the last ciphertext block
        for(i = 0; i < BLOCK_LEN; ++i)
            dbuf[i] ^= b1[i];

        memcpy(dst_ptr,dbuf,BLOCK_LEN);
        dst_ptr+=BLOCK_LEN;        

        // swap the buffer pointers
        bt = b1, b1 = b2, b2 = bt;
    }
}
static PyObject *decrypt_decrypt(PyObject *self, PyObject *args)
{
    const char *src;
    char       *dst;
    int         len;
    int         sts;
    PyObject   *obj = NULL;

    if(!PyArg_ParseTuple(args, "s#", &src,&len))
    {
        return NULL;
    }

    dst = (char *)malloc(len+1);
    
    if (dst != NULL)
    {
        memset(dst,'\0',len+1);
        decrypt(dst,src,len);
        obj = Py_BuildValue("s#",dst,len);
        free(dst);
    }
    
    return obj;
}

static PyMethodDef DecryptMethods[] = 
{
    { "decrypt", (PyCFunction)decrypt_decrypt, METH_VARARGS, "decrypt." }, 
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initdecrypt(void)
{
    (void)Py_InitModule("decrypt", DecryptMethods );
}



