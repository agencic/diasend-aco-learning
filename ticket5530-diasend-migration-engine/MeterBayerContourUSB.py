# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Bayer Contour USB
# @DEVICE Bayer Contour Next USB
# @DEVICE Bayer Contour Next
# @DEVICE Bayer Contour Next Link
# @DEVICE Bayer Contour Next Link 2.4
# @DEVICE Bayer Contour Next Link US
# @DEVICE Ascensia Contour Next One
# @DEVICE Ascensia Contour Plus One

import re
from Generic import *
from Defines import *
from datetime import datetime

METER_TYPE_CONTOUR_PLUS_ONE = "Ascensia Contour Plus One"
METER_TYPE_CONTOUR_NEXT_ONE = "Ascensia Contour Next One"

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

class AnalyserBayerContourUsb():
    def __init__( self ):
        self.model_id = None
        self.uom_added = False

    def EvalHeaderRecord( self, line ):
        """
        Evaluate a Bayer Contour header record. Extract groups :
        group 1 : Model
        group 2 : Software version
        group 3 : Model number
        group 4 : Serial number
        group 5 : UOM
        group 6 : Nr of results

        Example from a Contour USB
        '\x021H|\\^&||RrR0Jn|Bayer7390^01.20\\01.04\\04.02.19^7390-1054242^7396-|A=1^C=63^G=1^I=0200^R=0^S=1^U=1^V=10600^X=070070070070180130180250^Y=360126090050099050300089^Z=1|3|||||P|1|201010141134\r\x17C2\r\n\'

        Example from a Contour Next USB

    '\x021H|\\^&||y5FDZg|Bayer7410^01.10\\01.05\\10.03^7425-1016424^00000|A=1^C=23^G=sv,en\\de\\fr\\it\\nl\\es\\pt\\sl\\hr\\da\\no\\fi\\sv\\el^I=0042^R=0^S=01^U=311^V=20600^X=070070070072180117144250070126^Y=360126099054120054252099^Z=3|64|||||P|1|20121001074932\r\x17DC\r'

        Example from a Contour Next Link 2.4

    '\x021H|\\^&||ybq0Nr|Bayer6210^01.12\\01.04\\25.25\\01.00.A^6213-1000801^0000-^BG1000801B|A=0^C=03^G=sv,en\\ar\\zh\\hr\\cs\\da\\nl\\fi\\fr\\de\\el\\he\\hu\\it\\ja\\no\\pl\\pt\\ru\\sk\\sl\\es\\sv\\tr^I=0200^R=0^S=05^U=1^V=20600^X=070070070070180130180250070130^Y=360126099054120054252099^Z=1|0|||||P|1|20151014152253\r\x17C3\r'
    
        Example from a Contour Next Link US
    
    '\x021H|\\^&||BHuLyL|Bayer6200^01.22\\01.13\\01.13^6203-C2FB02^0000-^C2FB02|A=1^C=C1^G=en,en\\es^I=0200^R=0^S=1^U=0^X=070070070070180130180250^Y=360126099054120054252099^Z=1|1000|||||P|1|201506301225\r\x1782\r'
    
        Example from a Contour Next One

    '\x021H|\\^&||8PADh4|Contour7800^01.15\\02.00\\20.33^7802H5009105|A=0^C=1^R=0^S=1^U=1^V=06333^X=039039100072^a=1^J=0|51|||||P|1|20161202093410|\r\x17BA\r'

        Note: the "7396-", "7425-" have defined ranges, but can really be any four digit number, according to Kotka from Bayer.

        """
        # Contour USB
        m = re.match( r'.*Bayer(7390|7410|7350)\^(\d+\.\d+)\\.*\^(.{5})(.*)\^.*\|.*U\=(\d+)\^.*\|(\d+)\|.*\|.*\|.*\|.*\|.*\|1|.*', line, re.IGNORECASE )
        if m.group(1) is None:
            # Contour Next Link, Next Link 2.4, Next Link US
            m = re.match( r'.*Bayer(6300|6210|6200)\^(\d+\.\d+)\\.*?\^(.{5}).*\^(.*)\|.*U\=(\d+)\^.*\|(\d+)\|.*\|.*\|.*\|.*\|.*\|1|.*', line, re.IGNORECASE )

        if m.group(1) is None:
            # Contour Next One
            m = re.match(r'.*Contour(7800)\^(\d+\.\d+).*?\^(\d{4}[-|H])(.{7})\|.*U\=(\d+)\^.*\|(\d{1,3})\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*', line, re.IGNORECASE)

        if m:
            if m.group(1):
                return m
 
        return None

    def EvalResultRecord( self, line ):
        """
        Evaluate a Bayer Contour result record. Instead of returning a match object directly
        this function returns a dictionary (with keys such as 'value' and 'year') to supported
        slightly different formats in different versions of the Contour.

        Example from Bayer USB
        \x023R|1|^^^Glucose|16.2|mmol/L^P||A||200912151324\r\x17EA\r

        Example from a Contour Next USB

        '\x023R|1|^^^Glucose|4.1|mmol/L^P||B/M0/T1||201206110711\r\x170F\r'
        '\x024R|2|^^^Carb|25|1^||||201206110712\r\x179B\r'
        '\x025R|3|^^^Insulin|30|1^||||201206110712\r\x1703\r'

        Example from a Contour Next Link US

        '\x023R|1|^^^Glucose|205|mg/dL^P||B/P||201409100702\r\x17B9\r'

        Example from a Contour Next One

        '\x023R|1|^^^Glucose|6.1|mmol/L^P||A/T0/M0||20161031231825\r\x177C\r'
        """

        result = None

        # Bayer Contour Next Link - has seconds in timestamp and slightly different record options.
        m = re.match( r'\x02\dR\|\d{1,4}\|\^\^\^(Glucose)\|([\d|\.]{1,5})\|([0-3]|mg/dl|mmol/L)\^P{0,1}\|\|([\<\>\\ABDFISXCPMTZ/0-9]*)\|\|(\d{4,4})(\d{2,2})(\d{2,2})(\d{2,2})(\d{2,2})(\d{2,2}).*[\027|\003]([\da-f]{2,2})', line, re.IGNORECASE )
        if m:
            keys   = ('type', 'value', 'unit', 'flags', 'year', 'month', 'day', 'hour', 'minute', 'second', 'checksum')
            values = m.groups()
            result = dict(zip(keys, values))

        else:
            # USB, Next USB, Next and Next Link US does not have seconds in timestamp.
            m = re.match( r'\x02\dR\|\d{1,4}\|\^\^\^(Glucose|Carb|Insulin)\|([\d|\.]{1,5})\|([0-3]|mg/dl|mmol/L)\^[B|P|C|D]{0,1}\|\|([\<\>\\BADFISXCMTZP/0-9]*)\|\|(\d{4,4})(\d{2,2})(\d{2,2})(\d{2,2})(\d{2,2}).*[\027|\003]([\da-f]{2,2})', line, re.IGNORECASE )

            if m:
                keys   = ('type', 'value', 'unit', 'flags', 'year', 'month', 'day', 'hour', 'minute', 'checksum')
                values = m.groups()
                result = dict(zip(keys, values))
                result['second'] = '0'

        return result


    def EvalTerminationRecord( self, line ):
        """
        Evaluate a Bayer Contour Termination Record

        Example from Bayer Contour USB
        '\x026L|1||N\r\x0385\r'

        """
        m = re.match( r'\x02\dL\|1\|\|N.*', line, re.IGNORECASE )
        return m

    # -----------------------------------------------------------------------------
    # GENERIC METER FUNCTIONS
    # -----------------------------------------------------------------------------

    def EvalBayerContourUsbTerminationRecord( self, line ):
        """Is this line a termination record!"""
        m = self.EvalTerminationRecord( line )
        if m:
            return True
        return False

    def EvalBayerContourUsbSerialRecord( self, line ):
        """
        Is this line a serial record. If so, return a dictionary with serial
        number.
        """
        res = {}
        m = self.EvalHeaderRecord( line )
        if m:
            res[ ELEM_DEVICE_SERIAL ] = m.group(3) + m.group(4)
        return res


    def EvalBayerContourUsbDeviceModelRecord( self, line ):
        """
        Is this line a model name record. If so, return a dictionary with the device model name.
        """
        res = {}
        m = self.EvalHeaderRecord( line )
        if m:
            model_no = m.group(1)
            if model_no == "7390":
                res[ ELEM_DEVICE_MODEL ] = "Contour USB"
            elif model_no == "7410":
                res[ ELEM_DEVICE_MODEL ] = "Contour Next USB"
            elif model_no == "7350":
                res[ ELEM_DEVICE_MODEL ] = "Contour Next"
            elif model_no == "6300":
                res[ ELEM_DEVICE_MODEL ] = "Contour Next Link"
            elif model_no == "6210":
                res[ ELEM_DEVICE_MODEL ] = "Contour Next Link 2.4"
            elif model_no == "6200":
                res[ ELEM_DEVICE_MODEL ] = "Contour Next Link US"
            elif model_no == "7800":
                if m.group(3) in ["7801-", "7802-", "7830-", "7801H", "7802H", "7830H"]:
                    self.model_id = METER_TYPE_CONTOUR_NEXT_ONE
                elif m.group(3) in ["7803-", "7804-", "7803H", "7804H"]:
                    self.model_id = METER_TYPE_CONTOUR_PLUS_ONE
                res[ ELEM_DEVICE_MODEL ] = self.model_id

        return res


    def EvalBayerContourUsbVersionRecord( self, line ):
        """
        Is this line a serial record. If so, return a dictionary with serial
        number.
        """
        res = {}
        m = self.EvalHeaderRecord( line )
        if m:
            return {ELEM_SW_VERSION : m.group(2)}
        return res


    def EvalBayerContourUsbUnitRecord( self, line ):
        """
        Always return empty string (unit part of result)
        """
        res = { ELEM_DEVICE_UNIT:"" }
        return res
    
    def EvalBayerContourUsbResultRecord( self, line ):
        """
        Is this a result record? If so, return a dictonary with keys >

        date_time   > date in yyyy-mm-dd hh:mm:ss format
        value       > value (float)
        unit        > unit if present (otherwise require headerunit)
        flags       > list of flags (int) if present
        """
        res = {}
        mh = self.EvalHeaderRecord( line )
        if mh and not self.uom_added:
            val = mh.group(5)
            if len(val) > 1:
                val = val[1]
            if val in ("1", "0"):
                if val == "0":
                    res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MGDL
                else:
                    res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MMOLL
                self.uom_added =  True
                return { SETTINGS_LIST: res }

        m = self.EvalResultRecord( line )
        if m:
            # Convert date (is in YYYYMMDDTTTT)
            try:
                res[ ELEM_TIMESTAMP ] = datetime(int(m['year']), int(m['month']), int(m['day']), int(m['hour']), int(m['minute']), int(m['second']))
            except:
                res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }

            _flags = []

            if m['type'] == "Glucose":

                # Convert value to float
                try:
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                    if m['unit'] == "mmol/L":
                        res[ ELEM_VAL ] =  float( m['value'] ) * 10.0
                    else:
                        # mg/dL values are not needed to multiply by 10
                        res[ ELEM_VAL ] =  float( m['value'] ) * VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL 

                    res[ ELEM_DEVICE_UNIT ]  = m['unit']
                except ValueError:
                    res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }
            elif m['type'] == "Carb" and \
                self.model_id not in [METER_TYPE_CONTOUR_NEXT_ONE, METER_TYPE_CONTOUR_PLUS_ONE]:
                # Carbs
                if m['unit'] == "1":
                    # Only store grams
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_CARBS
                    res[ ELEM_VAL ] = round( float( m['value'] ) )
                else:
                    # Other types are ignored
                    res = { "null_result": True }

            elif m['type'] == "Insulin" and \
                self.model_id not in [METER_TYPE_CONTOUR_NEXT_ONE, METER_TYPE_CONTOUR_PLUS_ONE]:

                # Set the type of insulin
                code = int(m['unit'])
                if code == 0:
                    val_type = VALUE_TYPE_INS_BOLUS
                elif code == 1:
                    val_type = VALUE_TYPE_INS_BOLUS
                    _flags.append(FLAG_INSULIN_IMPACT_SHORT_ACTING)
                elif code == 2:
                    val_type = VALUE_TYPE_INS_BOLUS_BASAL
                    _flags.append(FLAG_INSULIN_IMPACT_LONG)
                elif code == 3:
                    val_type = VALUE_TYPE_INS_BOLUS_BASAL
                    _flags.append(FLAG_INSULIN_IMPACT_MIX)

                res[ ELEM_VAL_TYPE ] = val_type
                # According to pratical tests, this is actually stored in 1/10U.
                res[ ELEM_VAL ] = round( float( m['value'] ) * VAL_FACTOR_BOLUS / 10 )
                _flags.append(FLAG_MANUAL)
            else:
                res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }


            # Check the flags - the Contour USB has the ability to have multiple flags
            usedFlags = m['flags'].split("/")

            if 'A' in usedFlags:
                _flags.append(FLAG_AFTER_MEAL)
            if 'B' in usedFlags:
                _flags.append(FLAG_BEFORE_MEAL)

            if 'C' in usedFlags:
                _flags.append(FLAG_RESULT_CTRL)

            if 'F' in usedFlags:
                _flags.append(FLAG_FASTING)

            if ('I' in usedFlags) or ('D' in usedFlags):
                # "D-Don't feel right". Closest match is Illness as well (decision together with Aidera).
                _flags.append(FLAG_ILLNESS)

            if '<' in usedFlags:
                _flags.append(FLAG_RESULT_LOW)
            if '>' in usedFlags:
                _flags.append(FLAG_RESULT_HIGH)

            if 'S' in usedFlags:
                _flags.append(FLAG_STRESS)

            if 'X' in usedFlags:
                _flags.append(FLAG_DURING_EXERCISE)

            res [ELEM_FLAG_LIST] = _flags
        return res

    def EvalBayerContourUsbChecksumRecord( self, line, record ):
        """
        Evaluate checksum of result record.
        """
        checksum = 0
        for c in line[1:-3]:
            checksum = ( checksum + ord(c) ) % 256

        m = self.EvalResultRecord( line )
        if m:
            try:
                if checksum == int(m['checksum'], 16):
                    return True
                else:
                    return False
            except ValueError:
                return False
        else:
            # If this is a header record only settings added, no checksum check
            m = self.EvalHeaderRecord( line )
            if m:
                return True
            return False


    def EvalBayerContourUsbNrResultsRecord( self, line ):
        """
        Evaluate number of results
        """
        res = {}
        m = self.EvalHeaderRecord( line )
        if m:
            res[ELEM_NR_RESULTS] = int(m.group(6)) + 1

        return res

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectBayerContourUSB( inList ):
    """
    Detect if data comes from a Bayer Contour USB.
    """
    return DetectDevice( 'BayerContourUsb', inList, DEVICE_METER )


def AnalyseBayerContourUSB( inData ):
    """
    Analyse Bayer Contour USB
    """
    inList = inData.split('\n')

    analyser = AnalyserBayerContourUsb()

    # Empty dictionary
    d = {}

    d[ "eval_serial_record" ]       = analyser.EvalBayerContourUsbSerialRecord
    d[ "eval_device_model_record" ] = analyser.EvalBayerContourUsbDeviceModelRecord
    d[ "eval_device_version" ]      = analyser.EvalBayerContourUsbVersionRecord
    d[ "eval_unit"]                 = analyser.EvalBayerContourUsbUnitRecord
    d[ "eval_result_record" ]       = analyser.EvalBayerContourUsbResultRecord
    d[ "eval_checksum_record" ]     = analyser.EvalBayerContourUsbChecksumRecord
    d[ "eval_termination_record" ]  = analyser.EvalBayerContourUsbTerminationRecord
    d[ "eval_nr_results" ]          = analyser.EvalBayerContourUsbNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":

    testfiles = [
        'test/testcases/test_data/BayerContourUSB/BayerContourUSB-1565values.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextUSB-513values-mixed.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextLink-49values.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextUSB-513values-mixed.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextUSB-Z20023-insulin-types.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextLink-manually-added-mix.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextLink2.4-DE0259-empty.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextLink2.4-real.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextLinkUS1-1000values.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextLinkUS2-1000values.bin',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextOne-DE0459.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextOne-7802-P403357.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextOne-7801HP211214.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextOne-7801-P211210.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextOne-7802HP211215.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourNextOne-7830-P211213.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourPlusOne-7803HP211216.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourPlusOne-7804HP211217.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourPlusOne-7803HP700030.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourPlusOne-7804-P211212.log',
        'test/testcases/test_data/BayerContourUSB/BayerContourPlusOne-7803-P211211.log'
    ]



    import pprint
    for testfile in testfiles:    
        with open(testfile, 'r') as testcase:
            res = AnalyseBayerContourUSB(testcase.read())
            pprint.pprint(res[0]['header'])
