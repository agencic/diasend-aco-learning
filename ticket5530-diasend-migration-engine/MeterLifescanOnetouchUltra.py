# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006-2012 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE LifeScan OneTouch Ultra
# @DEVICE LifeScan OneTouch Ultra2
# @DEVICE LifeScan OneTouch Verio

# @SPECIFICATION Ultra: LifeScan ONE TOUCH® Ultra Meter RS-232 Communication Specification, AW057-521
# @SPECIFICATION Ultra2: LifeScan OneTouch® Ultra®2 Blood Glucose Meter RS- 232 Communication Protoco, AW 06443901 Rev 2
# @SPECIFICATION Verio: None, only emails and info from Stan Young "It is the OneTouchUltra2 Protocol. But they used this protocol for Verio as well." and "Verio serialnumber : Eight charactes, begins with a "B""

# Note: 
#  This module handles Ultra/Ultra2/Verio, sent by the Mk2 Transmitter 
#  and Ultra, sent by the Mk1 Transmitter. There is a separate module
#  for Ultra2, sent by the Mk1 Transmitter (MeterLifescanOnetouchUltra2)

import re
from Generic import *
from Defines import *
from datetime import datetime
                     
# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalMealComment(meal_comment):
    """
    Function that takes the meal flag from the meter, and returns the
    corresponding flag in our representation
    """
    if meal_comment == 0:
        return None
    elif meal_comment == 1:
        return FLAG_NOT_ENOUGH_FOOD
    elif meal_comment == 2:
        return FLAG_TOO_MUCH_FOOD
    elif meal_comment == 3:
        return FLAG_MILD_EXERCISE
    elif meal_comment == 4:
        return FLAG_HARD_EXERCISE
    elif meal_comment == 5:
        return FLAG_MEDICATION
    elif meal_comment == 6:
        return FLAG_STRESS
    elif meal_comment == 7:
        return FLAG_ILLNESS
    elif meal_comment == 8:
        return FLAG_FEEL_HYPO
    elif meal_comment == 9:
        return FLAG_MENSES
    elif meal_comment == 10:
        return FLAG_MEDICATION
    elif meal_comment == 11:
        return FLAG_OTHER

def EvalSerialRecord( line ):
    """
    Evaluate a Lifescan Onetouch Ultra Serial record. Extracted groups : 
    group 1 : number of result record
    group 2 : serial nr
    group 3 : unit
    group 4 : cksum
    """
    m = re.match( r'P\s+(\d+),"(\w+)\s*","(MG/DL|MMOL/L)\s*"\s+([\d|a-f]{4,4})', line, re.IGNORECASE )
    return m

def EvalUltraResultRecord( line ):
    """
    Evaluate a Lifescan Onetouch Ultra Result record. Extracted groups : 
    group 1 : day of week
    group 2 : month
    group 3 : day
    group 4 : year
    group 5 : hour
    group 6 : minutes
    group 7 : seconds
    group 8 : Control (C or empty)
    group 9 : value
    group 10: Parity error (? or empty)
    """
    # check ultra record
    m = re.match( r"""
        P\s+"(mon|tue|wed|thu|fri|sat|sun)",
        "(\d{2,2}).(\d{2,2}).(\d{2,2})",
        "(\d{2,2}).(\d{2,2}).(\d{2,2})\s*",
        "(C?)\s*(\d+|HIGH)\s*(\??)",\s*\d+\s+[\d|a-f]{4,4}
        """, line, re.IGNORECASE + re.VERBOSE )
    return m


def EvalUltra2VerioResultRecord( line ):
    """
    Evaluate a Lifescan Onetouch Ultra2 or Verio Result record. Extracted groups : 
    group 1 : day of week
    group 2 : month
    group 3 : day
    group 4 : year
    group 5 : hour
    group 6 : minutes
    group 7 : seconds
    group 8 : Control (C or empty)
    group 9 : value
    group 10: Parity error (? or empty)
    group 11: Meal flag
    group 12: user meal
    group 13: ??
    group 14: ??
    """
    # check ultra2 record
    # Test if this is a ULTRA 2 record
    m = re.match( r"""
        P\s+"(MON|TUE|WED|THU|FRI|SAT|SUN)",
        "(\d{2,2}).(\d{2,2}).(\d{2,2})",
        "(\d{2,2}).(\d{2,2}).(\d{2,2})\s*",
        "(C?)\s*(\d+|HIGH)\s*(\??)",\s*
        "(A|B|N)","(\d{2,2})",
        \s+(\d+)\s+([\d|a-f]{4,4})
        """, line, re.IGNORECASE + re.VERBOSE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalLifescanOnetouchUltraSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(2)
    return res
    
def EvalLifescanOnetouchSerialModelRecord( line ):
    """
    Detect the Lifescan OneTocuh Ultra/Ultra2/Verio model by looking at the serial record
    """
    res = {}
    
    m = EvalSerialRecord( line )
    if m:
        meter_serial = m.group(2)    
    else:
        return res

    # Ultra  : 9 characters, ends with a T
    # Ultra2 : 9 characters, ends with a Y
    # Verio  : 8 characters, starts with a B
    model_name = None
    if len(meter_serial) == 9:
        if meter_serial[-1] == "T":
            model_name = "OneTouch Ultra"
        elif meter_serial[-1] == "Y":
            model_name = "OneTouch Ultra2"
    elif ((len(meter_serial) == 8) and (meter_serial[0] == "B")):
        model_name = "OneTouch Verio (Old)"

    if model_name:
        res[ ELEM_DEVICE_MODEL ] = model_name

    return res

def EvalLifescanOnetouchUltraUnitRecord( line ):
    """
    According to the Ultra and Ultra2 documentation
    the glucose results are always mg/dL, regardless of the meter display setting (which is transmitted as well)
    """
    res = { "meter_unit":"mg/dL" }
    return res

def EvalLifescanOnetouchUltraResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        if m.group(3).lower() == 'mg/dl':
            res [ SETTING_BG_UNIT ] = SETTING_BG_UNIT_MGDL
        else:
            res [ SETTING_BG_UNIT ] = SETTING_BG_UNIT_MMOLL
        return { SETTINGS_LIST: res }

    m = EvalUltraResultRecord( line )

    if not m:
        m = EvalUltra2VerioResultRecord( line )
    
    if m:
        flags = []
        
        try:
            res[ ELEM_TIMESTAMP ] = datetime (2000 + int(m.group(4)), int(m.group(2)), int(m.group(3)), int(m.group(5)), int(m.group(6)), int(m.group(7)) )
        except:
            res[ ELEM_TIMESTAMP ] = None

        # Detect Control and parity errors
        if m.group(8) == 'C':
            flags.append(FLAG_RESULT_CTRL)
        if m.group(10) == '?':
            flags.append(FLAG_PARITY_ERROR)

        # Detect before/after meal flags
        try:
            if m.group(11) == 'A':
                flags.append(FLAG_AFTER_MEAL)
            elif m.group(11) == 'B':
                flags.append(FLAG_BEFORE_MEAL)
        except:
            # Ordinary ultra does not have this info
            pass

        # Detect other flags
        try:
            flag = int(m.group(12))
            
            diasendFlag = EvalMealComment(flag)
            if diasendFlag: 
                flags.append(diasendFlag)
        except:
            # Ordinary ultra does not have this info
            pass

        # Store the glucose value
        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        try:
            res[ ELEM_VAL ] = float( m.group(9) )

            # Add flags for HIGH / LOW values. These is a special case
            # where we get a glucose value _but_ the meter itself
            # reports HI/LO in the display.
            if res[ELEM_VAL] > VAL_FACTOR_CONV_MMOL_TO_MGDL*LIFESCAN_BG_VALUE_HIGH_MMOL:
                flags.append(FLAG_RESULT_HIGH)
            elif res[ELEM_VAL] < VAL_FACTOR_CONV_MMOL_TO_MGDL*LIFESCAN_BG_VALUE_LOW_MMOL:
                flags.append(FLAG_RESULT_LOW)

        # Non-numeric glucose value, could be a HI flag
        except ValueError:
            if 'HIGH' in m.group(9):
                res[ ELEM_VAL ] = 0
                flags.append(FLAG_RESULT_HIGH)
            else:
                res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }
        
        # Store the flags
        res ["meter_flaglist"] = flags
        
    return res

def EvalLifescanOnetouchUltraNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        try:
            # Add +1 for extra bg unit setting
            res[ "meter_nr_results" ] = int( m.group(1) ) + 1
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
    return res

def EvalLifescanOnetouchUltraChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    # calculate checksum
    checksum = 0
    for item in line[0:-6]:
        checksum = checksum + ord(item)

    try:
        if checksum == int(line[-4:], 16):
            return True
        else:
            return False
    except ValueError:
        return False

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectLifescanOnetouchUltra( inList ):
    """
    Detect if data comes from a Onetouch Ultra, Ultra 2 or Onetouch Verio.
    """
    return DetectDevice( 'LifescanOnetouchUltra', inList, DEVICE_METER )

def AnalyseLifescanOnetouchUltra( inData ):
    """
    Analyse Lifescan Onetouch Ultra and Verio devices
    """

    inList = inData.split('\n')

    # Empty dictionary
    d = {}

    d[ "eval_serial_record" ]   = EvalLifescanOnetouchUltraSerialRecord;
    d[ "eval_device_model_record" ] = EvalLifescanOnetouchSerialModelRecord;
    d[ "eval_unit"]             = EvalLifescanOnetouchUltraUnitRecord;
    d[ "eval_result_record" ]   = EvalLifescanOnetouchUltraResultRecord;
    d[ "eval_checksum_record" ] = EvalLifescanOnetouchUltraChecksumRecord
    d[ "eval_nr_results" ]      = EvalLifescanOnetouchUltraNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":

    #testfile = open('test/testcases/test_data/OneTouchVerio/Verio-BNBGD02W-empty.bin')
    #testfile = open('test/testcases/test_data/OneTouchVerio/Verio-BNBGD02W-three_values_with_flags.bin')
    #testfile = open('test/testcases/test_data/OneTouchVerio/Verio-BNBGD02W-two_values.bin')
    #testfile = open('test/testcases/test_data/OneTouchVerio/Verio-BOCCZ08K-half_full.bin')
    testfile = open('test/testcases/test_data/OneTouchVerio/Verio-BOCCZ0JK-full.bin')
    #testfile = open('test/testcases/test_data/OneTouchUltra/Ultra/Ultra-150values.bin')
    #testfile = open('test/testcases/test_data/OneTouchUltra/Ultra2/Ultra2-500values.bin')
    
    testcase = testfile.read()
    testfile.close()
    
    print AnalyseLifescanOnetouchUltra(testcase)
