# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE microdot+
# 
import re
import Common
import struct
from datetime import datetime

from Generic import *
from Defines import *

class MicrodotPlusRecord(object):

    def __init__(self, checksum_ok):
        self.checksum_ok = checksum_ok

    def is_model(self):
        return False

    def is_serial(self):
        return False

    def is_result(self):
        return False

    def is_number_of_results(self):
        return False

    def is_checksum_ok(self):
        return self.checksum_ok

class MicrodotPlusInfoRecord(MicrodotPlusRecord):

    def __init__(self, data, checksum_ok):

        super(MicrodotPlusInfoRecord, self).__init__(checksum_ok)

        self.serial_dictionary = {}
        self.model_dictionary = {}

        # Serial and model records does not get checked 
        # by Generic.py for checksum errors.
        if not checksum_ok:
            return

        (result, serial, production, device_type, sub_type, 
         fw_pad, fw_release, fw_sub_version, fw_main_version, 
         eeprom_pad, eeprom_release, eeprom_sub_version, eeprom_main_version, 
         version_id, version_build_date, version_build_time, rom_checksum) = struct.unpack('<hLLHHBBBBBBBB16s16s16sH', data)

        # If != 0 the info record is invalid. No model and no serial number.
        if result != 0:
            return

        self.serial_dictionary = {ELEM_DEVICE_SERIAL:'%d' % (serial)}

        # 0x13 = microdot+, if not, no model.
        if device_type != 0x13:
            return

        self.model_dictionary = {ELEM_DEVICE_MODEL:'Microdot/Microdot+'}

    def is_serial(self):
        return True

    def is_model(self):
        return True

    def get_serial_dictionary(self):
        return self.serial_dictionary

    def get_model_dictionary(self):
        return self.model_dictionary

class MicrodotPlusGetAllRecord(MicrodotPlusRecord):

    def __init__(self, data, checksum_ok):

        super(MicrodotPlusGetAllRecord, self).__init__(checksum_ok)

        self.dictionary = {}

        # Number of result records does not get checked 
        # by Generic.py for checksum errors.
        if not checksum_ok:
            return

        if len(data) != 4:
            return 

        response = map(ord, data)
        self.dictionary = {ELEM_NR_RESULTS:response[0] + response[1] * 0x100}

    def is_number_of_results(self):
        return True

    def get_dictionary(self):
        return self.dictionary

class MicrodotPlusResultRecord(MicrodotPlusRecord):

    def __init__(self, data, checksum_ok):

        super(MicrodotPlusResultRecord, self).__init__(checksum_ok)

        self.dictionary = {}

        (day, month, year, hour, minute, 
            value_type, battery_warning, temp_warning, 
            event, pen_no, value) = struct.unpack('<BBBBBBBBBBh', data)

        # < 0 indicates an incorrect record (i.e. incorrect EEPROM checksum).
        if value < 0:
            return

        # Setup date
        if year == 0:
            year = 100

        dt = datetime(2000+year, month, day, hour, minute)
        
        # Setup flags
        flags = []
        
        # Type specific data
        if (value_type == 1) or (value_type == 3):
            # Glucose
            self.dictionary[ELEM_VAL_TYPE]  = VALUE_TYPE_GLUCOSE
            self.dictionary[ELEM_VAL]       = value
            if value_type == 3:
                flags.append(FLAG_RESULT_CTRL)
            if value == 1001:
                flags.append(FLAG_RESULT_LOW)
            elif value == 1002:
                flags.append(FLAG_RESULT_HIGH)

        elif value_type == 2:
            # Insulin
            self.dictionary[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BOLUS
            self.dictionary[ELEM_VAL]       = value * VAL_FACTOR_BOLUS
            # @todo: Decide what to do with sub-data "event" and "pen number". Currently ignored.
            flags.append(FLAG_MANUAL)

        else:
            # Unknown value type.
            return
        
        # Add other flags
        if battery_warning == 1:
            flags.append(FLAG_LOW_BATTERY)
        if temp_warning == 1:
            flags.append(FLAG_RESULT_OUTSIDE_TEMP)
            
        # Store the global data
        self.dictionary[ELEM_TIMESTAMP] = dt
        self.dictionary[ELEM_FLAG_LIST] = flags

    def is_result(self):
        return True

    def get_dictionary(self):
        return self.dictionary

def CalculateChecksum(data):

    # Checksum calculation. XOR all bytes in the 
    # command and rotate 1 bit after each XOR. 

    checksum = 0x29db
    for byte in data:
        checksum = checksum ^ byte
        if (checksum & 1) == 1:
            msb = 0x8000
        else:
            msb = 0
        checksum = (checksum >> 1) + msb

    return checksum

def SplitData(data):

    result = []

    # Data dump consists of 'raw' responses from the device. Frame has the following format. 
    # 0x41 CG CI LEN [data...] CH CH
    # 
    # CG = command group
    # CI = command id
    # CH = checksum
    # LEN = Length of data part
    # 

    remaining_data = data

    while len(remaining_data) > 0:

        # A record must be at least 6 bytes long - header + checksum.
        if len(remaining_data) < 6:
            break

        # Extract the first parts of this record.
        header         = map(ord, remaining_data[:3])
        data_length    = ord(remaining_data[3])

        # Header + data + checksum.
        if len(remaining_data) < (4 + data_length + 2):
            break

        # Continue with the rest of the record.
        body           = remaining_data[4:4+data_length]
        checksum       = map(ord, remaining_data[4+data_length:4+data_length+2:])

        calc_checksum = CalculateChecksum(map(ord, remaining_data[:4+data_length]))
        checksum      = checksum[0] + checksum[1] * 0x100
        checksum_ok   = checksum == calc_checksum
        
        if header == [0x41, 0xd5, 0x49]:
            result.append(MicrodotPlusInfoRecord(body, checksum_ok))
        elif header == [0x41, 0xd5, 0x52]:
            result.append(MicrodotPlusGetAllRecord(body, checksum_ok))
        elif header == [0x41, 0xd5, 0x53]:
            # Setup record not used at the moment. Includes misc settings
            # such as date, time, alarms etc.
            pass
        elif header == [0x41, 0x55, 0x72]:
            block_index                = ord(body[0])
            number_of_records_in_block = ord(body[1])
            record_block               = body[2:]
            # Invalid length - each record is 12 bytes long. Ignore all records 
            # in this block = number of result error.
            if (len(record_block) % 12) == 0:
                records = [record_block[i:i+12] for i in range(0, len(record_block), 12)]
                for record in records:
                    result.append(MicrodotPlusResultRecord(record, checksum_ok))
        else:
            # Unknown header - probably best to abort the split operation. 
            break

        remaining_data = remaining_data[4+data_length+2:]

    return result

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalMicrodotPlusSerialRecord(record):
    """
    Check for serial record.
    """
    if record.is_serial():
        return record.get_serial_dictionary()
    return {}
    
def EvalMicrodotPlusUnitRecord(line):
    """
    Check for device unit - always return mg/dl 
    """
    return {ELEM_DEVICE_UNIT:"mg/dL"}
    
def EvalMicrodotPlusModelRecord(record):
    """
    Check for device model. 
    """
    if record.is_model():
        return record.get_model_dictionary()
    return {}

def EvalMicrodotPlusResultRecord(record):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    if record.is_result():
        return record.get_dictionary();
    return {}

def EvalMicrodotPlusNrResultsRecord(record):
    """
    Is this record a nr results. If so, return a dictionary with nr results.
    """
    if record.is_number_of_results():
        return record.get_dictionary()
    return {}

def EvalMicrodotPlusChecksumRecord(line, record):
    """
    Evaluate checksum of result record. Not applicable for this meter.
    """
    return line.is_checksum_ok()

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterMicrodotPlus(inList):
    """
    Detect if data comes from a MicrodotPlus.
    """
    return DetectDevice( 'MicrodotPlus', inList, DEVICE_METER );

def AnalyseMeterMicrodotPlus(inData):
    """
    Analyse data from MicrodotPlus.
    """

    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ]  = EvalMicrodotPlusModelRecord
    d[ "eval_serial_record" ]        = EvalMicrodotPlusSerialRecord
    d[ "eval_unit"]                  = EvalMicrodotPlusUnitRecord
    d[ "eval_result_record" ]        = EvalMicrodotPlusResultRecord
    d[ "eval_nr_results" ]           = EvalMicrodotPlusNrResultsRecord
    d[ "eval_checksum_record" ]      = EvalMicrodotPlusChecksumRecord

    inList = SplitData(inData)
    resList = AnalyseGenericMeter( inList, d );
    
    return resList

if __name__ == "__main__":
    #testfile = open('test/testcases/test_data/MicrodotPlus/MicrodotPlus_500.log')
    #testfile = open('test/testcases/test_data/MicrodotPlus/MicrodotPlus_113.log')
    #testfile = open('test/testcases/test_data/MicrodotPlus/MicrodotPlus_Empty_1.log')
    #testfile = open('test/testcases/test_data/MicrodotPlus/MicrodotPlus_Empty_2.log')
    #testfile = open('test/testcases/test_data/MicrodotPlus/Microdot-SN00000802-Hi-lo.log')
    testfile = open('test/testcases/test_data/MicrodotPlus/MicrodotPlus-with-insulin.log')
    testcase = testfile.read()
    testfile.close()
    
    results = AnalyseMeterMicrodotPlus(testcase)

    for result in results[0]['results']:
        print result
