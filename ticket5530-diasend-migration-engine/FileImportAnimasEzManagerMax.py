# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2009 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

from Generic import *
from Defines import *
from datetime import datetime
import PumpAnimasIR
import re

try:
    from elementtree import ElementTree
except:
    # Python 2.5
    from xml.etree import ElementTree

# -----------------------------------------------------------------------------
# Defines
# -----------------------------------------------------------------------------

                            
# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------

                        
# -----------------------------------------------------------------------------
# Global variables
# -----------------------------------------------------------------------------
global diasend_header # Contains extra TAG info for serial number

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
def EvalSerialRecord( line ):
    """
    Main function for validating a generic serial record
    """
    
    return re.match( r'!TAG!SERNO!(DVU[A-Za-z0-9@._]+)!', diasend_header, re.IGNORECASE )
    
      
def EvalBloodGlucoseRecord( elem ):
    """
    Evaluates a BGLOG XML element
    """
    
    # Looks like this
    # Q: Any Flags? Types - use them? Date format - always the same? "Ignore"? "Source"?
    """
    <BGLOG>
        <BG Type="4">305</BG>
        <REC_DATE Type="91">2006-05-16</REC_DATE>
        <REC_TIME Type="92">03:33:00</REC_TIME>
        <IGNORE Type="5">0</IGNORE>
        <SOURCE Type="5">2</SOURCE>
    </BGLOG>
    """

    if elem.tag == "BGLOG":
        # This is a blood glucose value 
        res = {}
        res[ELEM_VAL_TYPE]  = VALUE_TYPE_GLUCOSE
        res[ELEM_FLAG_LIST] = []
        res[ELEM_VAL]       = float(elem.findtext("BG")) * VAL_FACTOR_GLUCOSE_MGDL

        return res

def EvalBolusRecord( elem ):
    """
    Evaluates a BOLUSLOG XML element
    """
    
    # Looks like this
    # Q: BOLUS_TYPE? (uses like in PumpAnimasIR). SOURCE? BOLUS_DURATION units (seconds?)?
    """
    <BOLUSLOG>
        <BOLUS Type="4">605</BOLUS>
        <BOLUS_TYPE Type="4">3</BOLUS_TYPE>
        <BOLUS_DURATION Type="4">240</BOLUS_DURATION>
        <REC_DATE Type="91">2007-09-19</REC_DATE>
        <REC_TIME Type="92">12:50:00</REC_TIME>
        <SOURCE Type="5">1</SOURCE>
    </BOLUSLOG>
    """

    if elem.tag == "BOLUSLOG":
        # This is a insulin bolus value 
        res = {}
        res[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BOLUS
        res[ELEM_VAL]       = float(elem.findtext("BOLUS")) * VAL_FACTOR_BOLUS / 100
        res[ELEM_FLAG_LIST] = []
    
        bolus_type = int(elem.findtext("BOLUS_TYPE"))
        res[ELEM_FLAG_LIST] = PumpAnimasIR.InterpretBolusFlags(bolus_type)
    
        # extended? duration in seconds? @TODO check!
        duration = int(elem.findtext("BOLUS_DURATION")) / 60
        res[ELEM_VALUE_LIST] = [{ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]
        res[ELEM_FLAG_LIST].append(FLAG_BOLUS_TYPE_EXT)

        return res
    
def EvalBasalRecord( elem ):
    """
    Evaluates a BASALLOG XML element
    """
    
    # Looks like this
    # Q: BASAL unit? 
    """
    <BASALLOG>
        <BASAL Type="4">850</BASAL>
        <REC_DATE Type="91">2007-09-22</REC_DATE>
        <REC_TIME Type="92">09:00:00</REC_TIME>
        <FLAG Type="4">0</FLAG>
    </BASALLOG>
    """

    if elem.tag == "BASALLOG":
        res = {}

        # This is a insulin basal value 
        res[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_BASAL
        res[ELEM_VAL]       = float(elem.findtext("BASAL")) * VAL_FACTOR_BASAL / 100
        res[ELEM_FLAG_LIST] = []
        res[ELEM_TIMESTAMP] = parse_date_time(elem.findtext("REC_DATE"), elem.findtext("REC_TIME"))
    
        # Check the flag
        basal_flag = int(elem.findtext("FLAG"))
        if basal_flag == 1:
            res[ELEM_FLAG_LIST].append(FLAG_TEMP_MODIFIED_PERCENTAGE)

        return res
    
def EvalTotalDailyDoseRecord( elem ):
    """
    Evaluates a TDDLOG XML element
    """
    
    # Looks like this
    # Q: Unit?    
    """
    <TDDLOG>
        <BASALT Type="4">0</BASALT>
        <BOLUST Type="4">5000</BOLUST>
        <REC_DATE Type="91">2007-05-25</REC_DATE>
        <FLAG Type="4">0</FLAG>
    </TDDLOG>
    """

    if elem.tag == "TDDLOG":
        res = {}
    
        # Fetch the totals
        total_basal = int(float(elem.findtext("BASALT")) * VAL_FACTOR_TDD / 1000)
        total_bolus = int(float(elem.findtext("BOLUST")) * VAL_FACTOR_TDD / 1000)
    
        # This is a insulin total daily dose 
        _flags = []
        res[ELEM_VAL_TYPE]  = VALUE_TYPE_INS_TDD
        res[ELEM_VAL]       = total_basal + total_bolus
        res[ELEM_FLAG_LIST] = _flags
        res[ELEM_TIMESTAMP] = parse_date_time(elem.findtext("REC_DATE"), "23:59:00")
        res[ELEM_VALUE_LIST]=  [{ELEM_VAL : total_basal, ELEM_VAL_TYPE : VALUE_TYPE_INS_BASAL_TDD}]

        # Add the flags, if existing
        tdd_flag = int(elem.findtext("FLAG"))
        if tdd_flag & 0x1:
            _flags.append(FLAG_TEMP_MODIFIED)
        elif tdd_flag & 0x2:
            _flags.append(FLAG_SUSPENDED)

        return res
    
def EvalPumpSettingsRecord( elem ):
    """
    Evaluates a TDDLOG XML element
    """
    
    # @TODO Implement Basal programs, BGT (?) and ISF settings!
    
    # Looks like this    
    """ 
    <PUMPSETTINGS>
        <REC_DATE Type="91">2009-03-19</REC_DATE>
        <REC_TIME Type="92">14:05:00</REC_TIME>
        <PUMP_MODEL Type="12">Animas 2020</PUMP_MODEL>
        <SNDNORMALBOLUS Type="4">4</SNDNORMALBOLUS>
        <SNDAUDIOBOLUS Type="4">4</SNDAUDIOBOLUS>
        <SNDTEMPBASAL Type="4">0</SNDTEMPBASAL>
        <SNDALERT Type="4">4</SNDALERT>
        <SNDREMINDER Type="4">4</SNDREMINDER>
        <SNDWARNING Type="4">4</SNDWARNING>
        <SNDALARMS Type="4">4</SNDALARMS>
        <CLKMODE Type="4">0</CLKMODE>
        <ADVAUDIOBOLENABLED Type="4">1</ADVAUDIOBOLENABLED>
        <ADVAUDIOBOLSTEP Type="8">2.0</ADVAUDIOBOLSTEP>
        <ADVADVANCEDBOLUS Type="4">1</ADVADVANCEDBOLUS>
        <ADVREMINDERS Type="4">1</ADVREMINDERS>
        <ADVDELIVERY Type="4">0</ADVDELIVERY>
        <ADVBASALPROGS Type="4">1</ADVBASALPROGS>
        <ADVMAXBASAL Type="8">5.0</ADVMAXBASAL>
        <ADVMAXBOLUS Type="8">16.0</ADVMAXBOLUS>
        <ADVMAXTDD Type="8">100.0</ADVMAXTDD>
        <ADVDISPLAYTOUT Type="4">60</ADVDISPLAYTOUT>
        <ADVAUTOOFFENABLED Type="4">0</ADVAUTOOFFENABLED>
        <ADVAUTOOFFTIME Type="4">12</ADVAUTOOFFTIME>
        <ADVCARTWARNLVL Type="4">20</ADVCARTWARNLVL>
        <ADVOCCLLIMITS Type="4">0</ADVOCCLLIMITS>
        <ADVIOBENABLED Type="4">0</ADVIOBENABLED>
        <ADVIOBDURATION Type="4">5</ADVIOBDURATION>
        <ADVSKDYBGTARGET Type="4">240</ADVSKDYBGTARGET>
        <ADVSKDYCKKTN Type="4">4</ADVSKDYCKKTN>
        <ADVSKDYCKBG Type="4">2</ADVSKDYCKBG>
        <I2C_0_TIME Type="92">00:00:00</I2C_0_TIME>
        <I2C_1_TIME Type="92">NULL</I2C_1_TIME>
        <I2C_2_TIME Type="92">NULL</I2C_2_TIME>
        <I2C_3_TIME Type="92">NULL</I2C_3_TIME>
        <I2C_4_TIME Type="92">NULL</I2C_4_TIME>
        <I2C_5_TIME Type="92">NULL</I2C_5_TIME>
        <I2C_6_TIME Type="92">NULL</I2C_6_TIME>
        <I2C_7_TIME Type="92">NULL</I2C_7_TIME>
        <I2C_8_TIME Type="92">NULL</I2C_8_TIME>
        <I2C_9_TIME Type="92">NULL</I2C_9_TIME>
        <I2C_10_TIME Type="92">NULL</I2C_10_TIME>
        <I2C_11_TIME Type="92">NULL</I2C_11_TIME>
        <I2C_0_VAL Type="4">15</I2C_0_VAL>
        <I2C_1_VAL Type="4">NULL</I2C_1_VAL>
        <I2C_2_VAL Type="4">NULL</I2C_2_VAL>
        <I2C_3_VAL Type="4">NULL</I2C_3_VAL>
        <I2C_4_VAL Type="4">NULL</I2C_4_VAL>
        <I2C_5_VAL Type="4">NULL</I2C_5_VAL>
        <I2C_6_VAL Type="4">NULL</I2C_6_VAL>
        <I2C_7_VAL Type="4">NULL</I2C_7_VAL>
        <I2C_8_VAL Type="4">NULL</I2C_8_VAL>
        <I2C_9_VAL Type="4">NULL</I2C_9_VAL>
        <I2C_10_VAL Type="4">NULL</I2C_10_VAL>
        <I2C_11_VAL Type="4">NULL</I2C_11_VAL>
        <ISF_0_TIME Type="92">00:00:00</ISF_0_TIME>
        <ISF_1_TIME Type="92">NULL</ISF_1_TIME>
        <ISF_2_TIME Type="92">NULL</ISF_2_TIME>
        <ISF_3_TIME Type="92">NULL</ISF_3_TIME>
        <ISF_4_TIME Type="92">NULL</ISF_4_TIME>
        <ISF_5_TIME Type="92">NULL</ISF_5_TIME>
        <ISF_6_TIME Type="92">NULL</ISF_6_TIME>
        <ISF_7_TIME Type="92">NULL</ISF_7_TIME>
        <ISF_8_TIME Type="92">NULL</ISF_8_TIME>
        <ISF_9_TIME Type="92">NULL</ISF_9_TIME>
        <ISF_10_TIME Type="92">NULL</ISF_10_TIME>
        <ISF_11_TIME Type="92">NULL</ISF_11_TIME>
        <ISF_0_VAL Type="4">50</ISF_0_VAL>
        <ISF_1_VAL Type="4">NULL</ISF_1_VAL>
        <ISF_2_VAL Type="4">NULL</ISF_2_VAL>
        <ISF_3_VAL Type="4">NULL</ISF_3_VAL>
        <ISF_4_VAL Type="4">NULL</ISF_4_VAL>
        <ISF_5_VAL Type="4">NULL</ISF_5_VAL>
        <ISF_6_VAL Type="4">NULL</ISF_6_VAL>
        <ISF_7_VAL Type="4">NULL</ISF_7_VAL>
        <ISF_8_VAL Type="4">NULL</ISF_8_VAL>
        <ISF_9_VAL Type="4">NULL</ISF_9_VAL>
        <ISF_10_VAL Type="4">NULL</ISF_10_VAL>
        <ISF_11_VAL Type="4">NULL</ISF_11_VAL>
        <BGT_0_TIME Type="92">00:00:00</BGT_0_TIME>
        <BGT_1_TIME Type="92">NULL</BGT_1_TIME>
        <BGT_2_TIME Type="92">NULL</BGT_2_TIME>
        <BGT_3_TIME Type="92">NULL</BGT_3_TIME>
        <BGT_4_TIME Type="92">NULL</BGT_4_TIME>
        <BGT_5_TIME Type="92">NULL</BGT_5_TIME>
        <BGT_6_TIME Type="92">NULL</BGT_6_TIME>
        <BGT_7_TIME Type="92">NULL</BGT_7_TIME>
        <BGT_8_TIME Type="92">NULL</BGT_8_TIME>
        <BGT_9_TIME Type="92">NULL</BGT_9_TIME>
        <BGT_10_TIME Type="92">NULL</BGT_10_TIME>
        <BGT_11_TIME Type="92">NULL</BGT_11_TIME>
        <BGT_0_VAL Type="4">120</BGT_0_VAL>
        <BGT_1_VAL Type="4">NULL</BGT_1_VAL>
        <BGT_2_VAL Type="4">NULL</BGT_2_VAL>
        <BGT_3_VAL Type="4">NULL</BGT_3_VAL>
        <BGT_4_VAL Type="4">NULL</BGT_4_VAL>
        <BGT_5_VAL Type="4">NULL</BGT_5_VAL>
        <BGT_6_VAL Type="4">NULL</BGT_6_VAL>
        <BGT_7_VAL Type="4">NULL</BGT_7_VAL>
        <BGT_8_VAL Type="4">NULL</BGT_8_VAL>
        <BGT_9_VAL Type="4">NULL</BGT_9_VAL>
        <BGT_10_VAL Type="4">NULL</BGT_10_VAL>
        <BGT_11_VAL Type="4">NULL</BGT_11_VAL>
        <BGT_0_DELT Type="4">10</BGT_0_DELT>
        <BGT_1_DELT Type="4">NULL</BGT_1_DELT>
        <BGT_2_DELT Type="4">NULL</BGT_2_DELT>
        <BGT_3_DELT Type="4">NULL</BGT_3_DELT>
        <BGT_4_DELT Type="4">NULL</BGT_4_DELT>
        <BGT_5_DELT Type="4">NULL</BGT_5_DELT>
        <BGT_6_DELT Type="4">NULL</BGT_6_DELT>
        <BGT_7_DELT Type="4">NULL</BGT_7_DELT>
        <BGT_8_DELT Type="4">NULL</BGT_8_DELT>
        <BGT_9_DELT Type="4">NULL</BGT_9_DELT>
        <BGT_10_DELT Type="4">NULL</BGT_10_DELT>
        <BGT_11_DELT Type="4">NULL</BGT_11_DELT>
        <BAS_0_0_TIME Type="92">00:00:00</BAS_0_0_TIME>
        <BAS_0_1_TIME Type="92">NULL</BAS_0_1_TIME>
        <BAS_0_2_TIME Type="92">NULL</BAS_0_2_TIME>
        <BAS_0_3_TIME Type="92">NULL</BAS_0_3_TIME>
        <BAS_0_4_TIME Type="92">NULL</BAS_0_4_TIME>
        <BAS_0_5_TIME Type="92">NULL</BAS_0_5_TIME>
        <BAS_0_6_TIME Type="92">NULL</BAS_0_6_TIME>
        <BAS_0_7_TIME Type="92">NULL</BAS_0_7_TIME>
        <BAS_0_8_TIME Type="92">NULL</BAS_0_8_TIME>
        <BAS_0_9_TIME Type="92">NULL</BAS_0_9_TIME>
        <BAS_0_10_TIME Type="92">NULL</BAS_0_10_TIME>
        <BAS_0_11_TIME Type="92">NULL</BAS_0_11_TIME>
        <BAS_0_0_VAL Type="8">0.025</BAS_0_0_VAL>
        <BAS_0_1_VAL Type="8">NULL</BAS_0_1_VAL>
        <BAS_0_2_VAL Type="8">NULL</BAS_0_2_VAL>
        <BAS_0_3_VAL Type="8">NULL</BAS_0_3_VAL>
        <BAS_0_4_VAL Type="8">NULL</BAS_0_4_VAL>
        <BAS_0_5_VAL Type="8">NULL</BAS_0_5_VAL>
        <BAS_0_6_VAL Type="8">NULL</BAS_0_6_VAL>
        <BAS_0_7_VAL Type="8">NULL</BAS_0_7_VAL>
        <BAS_0_8_VAL Type="8">NULL</BAS_0_8_VAL>
        <BAS_0_9_VAL Type="8">NULL</BAS_0_9_VAL>
        <BAS_0_10_VAL Type="8">NULL</BAS_0_10_VAL>
        <BAS_0_11_VAL Type="8">NULL</BAS_0_11_VAL>
        <BAS_1_0_TIME Type="92">00:00:00</BAS_1_0_TIME>
        <BAS_1_1_TIME Type="92">NULL</BAS_1_1_TIME>
        <BAS_1_2_TIME Type="92">NULL</BAS_1_2_TIME>
        <BAS_1_3_TIME Type="92">NULL</BAS_1_3_TIME>
        <BAS_1_4_TIME Type="92">NULL</BAS_1_4_TIME>
        <BAS_1_5_TIME Type="92">NULL</BAS_1_5_TIME>
        <BAS_1_6_TIME Type="92">NULL</BAS_1_6_TIME>
        <BAS_1_7_TIME Type="92">NULL</BAS_1_7_TIME>
        <BAS_1_8_TIME Type="92">NULL</BAS_1_8_TIME>
        <BAS_1_9_TIME Type="92">NULL</BAS_1_9_TIME>
        <BAS_1_10_TIME Type="92">NULL</BAS_1_10_TIME>
        <BAS_1_11_TIME Type="92">NULL</BAS_1_11_TIME>
        <BAS_1_0_VAL Type="8">0.0</BAS_1_0_VAL>
        <BAS_1_1_VAL Type="8">NULL</BAS_1_1_VAL>
        <BAS_1_2_VAL Type="8">NULL</BAS_1_2_VAL>
        <BAS_1_3_VAL Type="8">NULL</BAS_1_3_VAL>
        <BAS_1_4_VAL Type="8">NULL</BAS_1_4_VAL>
        <BAS_1_5_VAL Type="8">NULL</BAS_1_5_VAL>
        <BAS_1_6_VAL Type="8">NULL</BAS_1_6_VAL>
        <BAS_1_7_VAL Type="8">NULL</BAS_1_7_VAL>
        <BAS_1_8_VAL Type="8">NULL</BAS_1_8_VAL>
        <BAS_1_9_VAL Type="8">NULL</BAS_1_9_VAL>
        <BAS_1_10_VAL Type="8">NULL</BAS_1_10_VAL>
        <BAS_1_11_VAL Type="8">NULL</BAS_1_11_VAL>
        <BAS_2_0_TIME Type="92">00:00:00</BAS_2_0_TIME>
        <BAS_2_1_TIME Type="92">NULL</BAS_2_1_TIME>
        <BAS_2_2_TIME Type="92">NULL</BAS_2_2_TIME>
        <BAS_2_3_TIME Type="92">NULL</BAS_2_3_TIME>
        <BAS_2_4_TIME Type="92">NULL</BAS_2_4_TIME>
        <BAS_2_5_TIME Type="92">NULL</BAS_2_5_TIME>
        <BAS_2_6_TIME Type="92">NULL</BAS_2_6_TIME>
        <BAS_2_7_TIME Type="92">NULL</BAS_2_7_TIME>
        <BAS_2_8_TIME Type="92">NULL</BAS_2_8_TIME>
        <BAS_2_9_TIME Type="92">NULL</BAS_2_9_TIME>
        <BAS_2_10_TIME Type="92">NULL</BAS_2_10_TIME>
        <BAS_2_11_TIME Type="92">NULL</BAS_2_11_TIME>
        <BAS_2_0_VAL Type="8">0.0</BAS_2_0_VAL>
        <BAS_2_1_VAL Type="8">NULL</BAS_2_1_VAL>
        <BAS_2_2_VAL Type="8">NULL</BAS_2_2_VAL>
        <BAS_2_3_VAL Type="8">NULL</BAS_2_3_VAL>
        <BAS_2_4_VAL Type="8">NULL</BAS_2_4_VAL>
        <BAS_2_5_VAL Type="8">NULL</BAS_2_5_VAL>
        <BAS_2_6_VAL Type="8">NULL</BAS_2_6_VAL>
        <BAS_2_7_VAL Type="8">NULL</BAS_2_7_VAL>
        <BAS_2_8_VAL Type="8">NULL</BAS_2_8_VAL>
        <BAS_2_9_VAL Type="8">NULL</BAS_2_9_VAL>
        <BAS_2_10_VAL Type="8">NULL</BAS_2_10_VAL>
        <BAS_2_11_VAL Type="8">NULL</BAS_2_11_VAL>
        <BAS_3_0_TIME Type="92">00:00:00</BAS_3_0_TIME>
        <BAS_3_1_TIME Type="92">NULL</BAS_3_1_TIME>
        <BAS_3_2_TIME Type="92">NULL</BAS_3_2_TIME>
        <BAS_3_3_TIME Type="92">NULL</BAS_3_3_TIME>
        <BAS_3_4_TIME Type="92">NULL</BAS_3_4_TIME>
        <BAS_3_5_TIME Type="92">NULL</BAS_3_5_TIME>
        <BAS_3_6_TIME Type="92">NULL</BAS_3_6_TIME>
        <BAS_3_7_TIME Type="92">NULL</BAS_3_7_TIME>
        <BAS_3_8_TIME Type="92">NULL</BAS_3_8_TIME>
        <BAS_3_9_TIME Type="92">NULL</BAS_3_9_TIME>
        <BAS_3_10_TIME Type="92">NULL</BAS_3_10_TIME>
        <BAS_3_11_TIME Type="92">NULL</BAS_3_11_TIME>
        <BAS_3_0_VAL Type="8">0.0</BAS_3_0_VAL>
        <BAS_3_1_VAL Type="8">NULL</BAS_3_1_VAL>
        <BAS_3_2_VAL Type="8">NULL</BAS_3_2_VAL>
        <BAS_3_3_VAL Type="8">NULL</BAS_3_3_VAL>
        <BAS_3_4_VAL Type="8">NULL</BAS_3_4_VAL>
        <BAS_3_5_VAL Type="8">NULL</BAS_3_5_VAL>
        <BAS_3_6_VAL Type="8">NULL</BAS_3_6_VAL>
        <BAS_3_7_VAL Type="8">NULL</BAS_3_7_VAL>
        <BAS_3_8_VAL Type="8">NULL</BAS_3_8_VAL>
        <BAS_3_9_VAL Type="8">NULL</BAS_3_9_VAL>
        <BAS_3_10_VAL Type="8">NULL</BAS_3_10_VAL>
        <BAS_3_11_VAL Type="8">NULL</BAS_3_11_VAL>
        <BAS_0_NAME Type="12">1-weekday</BAS_0_NAME>
        <BAS_1_NAME Type="12">2-other</BAS_1_NAME>
        <BAS_2_NAME Type="12">3-weekend</BAS_2_NAME>
        <BAS_3_NAME Type="12">4-exercise</BAS_3_NAME>
        <PUMP_SERIAL_NO Type="12">27-04663-12</PUMP_SERIAL_NO>
        <ADVFRIENDLYNAME Type="12">NULL</ADVFRIENDLYNAME>
        <SNDREMOTEBOLUS Type="5">0</SNDREMOTEBOLUS>
        <ADVMAX2HR Type="5">0</ADVMAX2HR>
    </PUMPSETTINGS>
    """

    settings = [ \
        {"xml" : "ADVAUDIOBOLENABLED",  "dia" : SETTING_AUDIO_BOLUS_ENABLE              }, \
        {"xml" : "ADVAUDIOBOLSTEP",     "dia" : SETTING_AUDIO_BOLUS_INCREMENT           }, \
        {"xml" : "ADVADVANCEDBOLUS",    "dia" : SETTING_ADV_BOLUS_OPTIONS_ENABLE        }, \
        {"xml" : "ADVREMINDERS",        "dia" : SETTING_BOLUS_REMINDER_OPTIONS_ENABLE   }, \
        {"xml" : "ADVDELIVERY",         "dia" : SETTING_BOLUS_DELIVERY_SPEED            }, \
        {"xml" : "ADVBASALPROGS",       "dia" : SETTING_BASAL_PROGRAMS_IN_UI            }, \
        {"xml" : "ADVMAXBASAL",         "dia" : SETTING_MAX_BASAL_RATE,                 "factor" : 10}, \
        {"xml" : "ADVMAXBOLUS",         "dia" : SETTING_MAX_BOLUS,                      "factor" : 10}, \
        {"xml" : "ADVMAXTDD",           "dia" : SETTING_MAX_TDD,                        "factor" : 10}, \
        {"xml" : "ADVMAX2HR",           "dia" : SETTING_MAX_2HOURS                      }, \
        {"xml" : "ADVDISPLAYTOUT",      "dia" : SETTING_DISPLAY_TIMEOUT                 }, \
        {"xml" : "ADVAUTOOFFENABLED",   "dia" : SETTING_AUTO_OFF_ENABLE                 }, \
        {"xml" : "ADVAUTOOFFTIME",      "dia" : SETTING_AUTO_OFF_TIMEOUT                }, \
        {"xml" : "ADVCARTWARNLVL",      "dia" : SETTING_CARTRIDGE_WARNING_LEVEL         }, \
        {"xml" : "ADVOCCLLIMITS",       "dia" : SETTING_OCCLUSION_SENSITIVITY_LEVEL     }, \
        {"xml" : "ADVIOBENABLED",       "dia" : SETTING_INSULIN_ON_BOARD_ENABLE         }, \
        {"xml" : "ADVIOBDURATION",      "dia" : SETTING_INSULIN_ON_BOARD_DECAY_DURATION }, \
        {"xml" : "ADVSKDYBGTARGET",     "dia" : SETTING_SICK_BG_OVER_LIMIT              }, \
        {"xml" : "ADVSKDYCKKTN",        "dia" : SETTING_SICK_KETOONE_CHECK_TIME         }, \
        {"xml" : "ADVSKDYCKBG",         "dia" : SETTING_SICK_CHECK_BG_TIME              } \
                ]
                
    # @TODO Find out if anything corresponds to SETTING_MAX_SETTINGS_LOCK and SETTING_LANG_SELECTION_IDX

    if elem.tag == "PUMPSETTINGS":
        res = {}
        setlist = {}
        for setting in settings:
            value = elem.findtext(setting["xml"])
            if value:
                if setting.has_key("factor"):
                    factor = setting["factor"]
                else:
                    factor = 1
                setlist[setting["dia"]] = int(float(value)) * factor
    
        res[ SETTINGS_LIST ] = setlist
        return res
        
def EvalFoodRecord( elem ):
    """
    Evaluates a FOODLOG XML element
    """
    
    # Looks like this
    """
    <FOODLOG>
        <MEALID Type="4">9</MEALID>
        <ENTERNUM Type="4">1</ENTERNUM>
        <NAME Type="12">Download from pump</NAME>
        <SERVING Type="12">1 serving</SERVING>
        <MULTIPLIER Type="4">10</MULTIPLIER>
        <CARBS Type="4">350</CARBS>
        <FIBER Type="4">0</FIBER>
        <CALORIES Type="4">0</CALORIES>
        <PROTEIN Type="4">0</PROTEIN>
        <FAT Type="4">0</FAT>
        <REC_DATE Type="91">2009-11-27</REC_DATE>
        <REC_TIME Type="92">06:29:00</REC_TIME>
    </FOODLOG>
    """
    
    if elem.tag == "FOODLOG":
        # Only pick the carbs
        res = {}
        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_CARBS
        res[ ELEM_VAL ]      = int(round(float(elem.findtext("CARBS"))/float(elem.findtext("MULTIPLIER"))))
        
        return res
    
def EvalAlarmRecord( elem ):
    """
    Evaluates an ALARMLOG XML element
    """
    
    res = {}
    # Looks like this
    """
    <ALARMLOG>
        <ALARM Type="12">Error: , Code=177-8699, Message=LOW BATTERY</ALARM>
        <PUMPALARM Type="4">177</PUMPALARM>
        <REC_DATE Type="91">2007-10-25</REC_DATE>
        <REC_TIME Type="92">09:55:00</REC_TIME>
    </ALARMLOG>
    """

    if elem.tag == "ALARMLOG":
        res = {}
        res[ ELEM_VAL_TYPE ]  = VALUE_TYPE_ALARM
        res[ ELEM_VAL ]       = PumpAnimasIR.GetAlarmCode(int(elem.findtext("PUMPALARM")))
        res[ ELEM_FLAG_LIST ] = []
    
        return res
    
def EvalPrimeRecord( elem ):
    """
    Evaluates a PRIMELOG XML element
    """
    
    # Looks like this
    """
    <PRIMELOG>
        <REC_DATE Type="91">2007-05-21</REC_DATE>
        <REC_TIME Type="92">17:35:00</REC_TIME>
        <PRIME_AMT Type="4">398</PRIME_AMT>
        <FLAGS Type="4">2</FLAGS>
    </PRIMELOG>"""
    
    if elem.tag == "PRIMELOG":
        res = {}
        _flags = []
        res[ ELEM_VAL_TYPE ]  = VALUE_TYPE_INS_PRIME
        res[ ELEM_VAL ]       = int(elem.findtext("PRIME_AMT"))
        res[ ELEM_FLAG_LIST ] = _flags
    
        _flag = int(elem.findtext("FLAGS"))
        if _flag == 1:
            _flags.append(FLAG_NOT_PRIMED)
        elif _flag == 2:
            _flags.append(FLAG_PRIMED)
        elif _flag == 3:
            _flags.append(FLAG_CANNULA_BOLUS)
        
        return res
    
def EvalSuspendRecord( elem ):
    """
    Evaluates a SUSPENDLOG XML element
    """
    
    # Looks like this
    """
    <SUSPENDLOG>
        <REC_DATE Type="91">2007-08-01</REC_DATE>
        <REC_TIME Type="92">15:41:00</REC_TIME>
        <RSM_DATE Type="91">2007-08-01</RSM_DATE>
        <RSM_TIME Type="92">16:13:00</RSM_TIME>
    </SUSPENDLOG>
    """

    if elem.tag == "SUSPENDLOG":
        return {ELEM_VAL_TYPE: VALUE_TYPE_ALARM, ELEM_VAL: ALARM_DELIVERY_SUSPENDED}
    
def parse_date_time(date, time):
    """
    Parses time of form: 11:22:33
    And date of form: 2008-09-10

    into a datetime
    """
    d_m = re.match( r"(\d{4,4})-(\d{2,2})-(\d{2,2})", date, re.IGNORECASE)
    t_m = re.match( r"(\d{2,2}):(\d{2,2}):(\d{2,2})", time, re.IGNORECASE)

    if not d_m or not t_m:
        return None

    return datetime(int(d_m.group(1)), int(d_m.group(2)), int(d_m.group(3)), \
                    int(t_m.group(1)), int(t_m.group(2)), int(t_m.group(3)))

    
def ConvertXmlToFlattenedList( inData ):
    """
    Generate a list with relevant data
    """
    # Create an XML structure
    root = ElementTree.fromstring(inData)
    if root.tag == "ezManagerMax_Data":
        nodes = root.getchildren()

    # We now have a list of elements. 
    # Only the last "PUMPSETTINGS" element should be kept, since we only support one group of settings
    found_last_settings = False
    for i in range(len(nodes)-1, 0, -1):
        if nodes[i].tag == "PUMPSETTINGS":
            if found_last_settings:
                del nodes[i]
            else:
                found_last_settings = True 
        
    return nodes
    

def EvalHistoryRecord( line ):
    """
    Evaluates a history record, i.e has timestamp
    """

    m = EvalBloodGlucoseRecord(line)
    if not m:
        m = EvalBolusRecord(line)
    if not m:
        m = EvalBasalRecord(line)
    if not m:
        m = EvalAlarmRecord(line)
    if not m:
        m = EvalPrimeRecord(line)
    if not m:
        m = EvalSuspendRecord(line)
    if not m:
        m = EvalFoodRecord(line)

    if m:
        m[ELEM_TIMESTAMP] = parse_date_time(line.findtext("REC_DATE"), line.findtext("REC_TIME"))
        
        return m


# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAnimasEzManagerMaxModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
        
    return {ELEM_DEVICE_MODEL : "File Import Animas EzManagerMax"}


def EvalAnimasEzManagerMaxSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ELEM_DEVICE_SERIAL] = m.group(1)
    return res



def EvalAnimasEzManagerMaxUnitRecord( line ):
    """
    This one always uses mg/dL?
    """
    # @TODO Is there unit information, or is it hard coded?
    return { ELEM_DEVICE_UNIT:"mg/dL" }


def EvalAnimasEzManagerMaxResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    """
    
    m = EvalPumpSettingsRecord(line)
    if not m:
        m = EvalHistoryRecord(line)
    if not m:
        m = EvalTotalDailyDoseRecord(line)
    
    if m:
        # Mark all results as originated from file import
        if m.has_key( ELEM_FLAG_LIST ):
            m[ ELEM_FLAG_LIST ].append(FLAG_ORIGIN_FILE_IMPORT)
        
        return m

    else:
        print "Unhandled:", line.tag
        return {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":line}


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectAnimasEzManagerMax( inList ):
    """
    Detect if data comes from a EzManager file.
    """
    
    return DetectDevice( 'AnimasEzManagerMaxFile', inList, DEVICE_USER )

def AnalyseAnimasEzManagerMax(inData):
    """
    Perform the analysis of an EzManager XML file
    """

    global diasend_header

    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ] = EvalAnimasEzManagerMaxModelRecord
    d[ "eval_serial_record" ]       = EvalAnimasEzManagerMaxSerialRecord
    d[ "eval_unit"]                 = EvalAnimasEzManagerMaxUnitRecord
    d[ "eval_result_record" ]       = EvalAnimasEzManagerMaxResultRecord
    
    inList = []
    
    # If this is a real file, it will have an extra header tag with the serial number to use.
    # Extract this one first

    # First find the xml start tag
    xml_pos = inData.find("<?xml")

    # @TODO Return some error on failed import
    if xml_pos >= 0:
        # Extract the diasend header
        diasend_header = inData[:xml_pos]
        
        # Release 
        inData = inData[xml_pos:]
        
        # First, try to convert from XML to TAB
        try:
            inList = ConvertXmlToFlattenedList(inData)
        except:
            # Nope, didn't work. @TODO. Report error!
            print "Convert failed"
            pass
            
        # Perform the actual analysis
    return AnalyseGenericMeter( inList, d )

# -----------------------------------------------------------------------------
# SOME CODE THAT WILL RUN ON IMPORT
# -----------------------------------------------------------------------------

if __name__ == "__main__":

    # Data from an Ultra(RRF49A3QT)
    data1 = """!TAG!SERNO!DVU123456789!<?xml version="1.0" encoding="UTF-8"?>
<ezManagerMax_Data>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">03:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">17:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">03:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-20</REC_DATE>
    <REC_TIME Type="92">17:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-20</REC_DATE>
    <REC_TIME Type="92">04:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">03:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">341</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">03:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">03:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">06:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">336</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">04:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">03:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">133</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">03:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">13:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">08:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">03:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">393</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">73</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">08:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-02-06</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">07:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">17:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">16:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">21:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">16:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">14:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">20:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">405</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">19:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">17:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">07:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">09:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">08:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">08:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">07:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">07:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">08:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">08:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">08:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">06:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">370</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">08:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">07:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">10:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">60</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">08:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">08:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-21</REC_DATE>
    <REC_TIME Type="92">07:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-11-20</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-11-19</REC_DATE>
    <REC_TIME Type="92">05:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-11-15</REC_DATE>
    <REC_TIME Type="92">08:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-14</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-11-10</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-11-09</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-11-08</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-11-07</REC_DATE>
    <REC_TIME Type="92">08:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-11-06</REC_DATE>
    <REC_TIME Type="92">08:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-11-05</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-11-04</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-11-03</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-02</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-11-01</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-10-31</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">417</BG>
    <REC_DATE Type="91">2005-10-30</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-10-30</REC_DATE>
    <REC_TIME Type="92">08:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-10-29</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-10-28</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-10-27</REC_DATE>
    <REC_TIME Type="92">07:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2005-10-26</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-10-25</REC_DATE>
    <REC_TIME Type="92">06:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-10-24</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2005-10-23</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-10-22</REC_DATE>
    <REC_TIME Type="92">07:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-10-21</REC_DATE>
    <REC_TIME Type="92">14:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-10-21</REC_DATE>
    <REC_TIME Type="92">07:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-10-20</REC_DATE>
    <REC_TIME Type="92">06:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-10-19</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-10-18</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2005-10-17</REC_DATE>
    <REC_TIME Type="92">17:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-10-17</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-10-16</REC_DATE>
    <REC_TIME Type="92">07:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-10-15</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-10-14</REC_DATE>
    <REC_TIME Type="92">06:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-10-13</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-10-12</REC_DATE>
    <REC_TIME Type="92">03:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-10-10</REC_DATE>
    <REC_TIME Type="92">06:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-10-09</REC_DATE>
    <REC_TIME Type="92">05:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-10-08</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-10-07</REC_DATE>
    <REC_TIME Type="92">07:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
</ezManagerMax_Data>
"""

    # Data from an Ultra(RRF49A3QT) and UltraSmart(THF05CBNV)
    data2 = """!TAG!SERNO!DVU234567890!<?xml version="1.0" encoding="UTF-8"?>
<ezManagerMax_Data>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">03:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">17:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">03:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-20</REC_DATE>
    <REC_TIME Type="92">17:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-20</REC_DATE>
    <REC_TIME Type="92">04:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">03:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">341</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">03:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">03:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">06:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">336</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">04:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">03:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">133</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">03:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">13:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">08:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">03:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">393</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">73</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">08:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-02-06</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">07:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">17:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">16:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">21:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">16:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">14:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">20:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">405</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">19:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">17:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">07:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">09:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">08:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">08:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">07:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">07:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">08:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">08:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">08:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">06:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">370</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">08:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">07:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">10:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">60</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">08:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">08:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-21</REC_DATE>
    <REC_TIME Type="92">07:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-11-20</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-11-19</REC_DATE>
    <REC_TIME Type="92">05:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-11-15</REC_DATE>
    <REC_TIME Type="92">08:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-14</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-11-10</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-11-09</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-11-08</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-11-07</REC_DATE>
    <REC_TIME Type="92">08:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-11-06</REC_DATE>
    <REC_TIME Type="92">08:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-11-05</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-11-04</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-11-03</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-02</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-11-01</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-10-31</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">417</BG>
    <REC_DATE Type="91">2005-10-30</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-10-30</REC_DATE>
    <REC_TIME Type="92">08:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-10-29</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-10-28</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-10-27</REC_DATE>
    <REC_TIME Type="92">07:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2005-10-26</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-10-25</REC_DATE>
    <REC_TIME Type="92">06:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-10-24</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2005-10-23</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-10-22</REC_DATE>
    <REC_TIME Type="92">07:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-10-21</REC_DATE>
    <REC_TIME Type="92">14:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-10-21</REC_DATE>
    <REC_TIME Type="92">07:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-10-20</REC_DATE>
    <REC_TIME Type="92">06:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-10-19</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-10-18</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2005-10-17</REC_DATE>
    <REC_TIME Type="92">17:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-10-17</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-10-16</REC_DATE>
    <REC_TIME Type="92">07:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-10-15</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-10-14</REC_DATE>
    <REC_TIME Type="92">06:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-10-13</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-10-12</REC_DATE>
    <REC_TIME Type="92">03:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-10-10</REC_DATE>
    <REC_TIME Type="92">06:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-10-09</REC_DATE>
    <REC_TIME Type="92">05:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-10-08</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-10-07</REC_DATE>
    <REC_TIME Type="92">07:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2005-08-24</REC_DATE>
    <REC_TIME Type="92">17:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">09:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">09:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">11:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">18:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">21:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">22:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">22:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">00:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">07:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">07:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">09:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">10:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">11:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">12:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">15:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">17:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">306</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">18:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">20:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">22:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">23:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">10:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">16:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">18:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">18:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">124</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">18:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">20:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">23:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">10:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">11:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">13:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">15:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">17:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">19:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">373</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">20:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">22:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">06:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">07:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">08:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">10:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">169</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">14:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">14:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">17:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">19:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">08:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">09:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">12:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">14:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">14:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">17:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">18:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">20:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">23:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">07:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">08:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">10:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">13:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">14:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">15:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">17:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">08:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">11:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">311</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">10:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">13:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">15:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">17:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">17:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">22:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">07:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">12:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">14:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">16:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">18:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">22:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">23:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">06:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">10:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">13:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">13:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">15:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">17:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">17:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">18:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">20:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">22:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">09:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">10:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">149</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">12:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">17:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">19:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">21:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">22:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">07:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">09:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">11:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">328</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">329</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">15:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">17:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">18:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">20:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">21:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">21:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">06:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">07:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">09:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">10:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">11:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">13:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">15:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">17:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">20:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">22:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">00:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">06:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">08:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">09:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">10:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">16:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">18:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">20:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">22:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">00:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">09:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">137</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">16:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">17:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">20:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">328</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">368</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">09:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">11:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">15:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">334</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">23:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">06:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">09:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">11:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">12:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">13:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">317</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">15:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">381</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">18:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">21:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">22:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">07:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">09:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">12:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">15:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">20:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">09:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">14:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">16:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">18:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">372</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">21:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">06:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">07:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">331</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">09:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">14:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">16:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">17:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">95</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">19:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">93</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">20:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">21:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">21:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">07:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">08:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">10:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">84</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">14:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">15:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">18:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">21:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">22:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">05:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">05:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">06:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">08:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">10:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">11:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">14:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">17:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">21:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">07:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">10:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">116</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">15:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">17:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">18:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">20:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">408</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">22:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">390</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">22:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">335</BG>
    <REC_DATE Type="91">2005-09-18</REC_DATE>
    <REC_TIME Type="92">07:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2005-09-18</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-09-18</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">13:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">14:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">15:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">16:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">19:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">107</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">19:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">19:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">00:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">22:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">23:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">325</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">23:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">07:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">08:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">13:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">15:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">17:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">18:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">375</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">21:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">547</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">578</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">22:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">500</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">23:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">511</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">00:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">04:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">10:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">11:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">12:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">83</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">13:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">15:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">16:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">18:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">20:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">21:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">00:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">13:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">17:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">17:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">22:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">00:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">06:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">11:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">18:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">23:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">09:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">12:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">13:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">18:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">20:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">04:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">15:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">17:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">110</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">90</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">110</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">20:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">22:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">13:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">17:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">19:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">313</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">20:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">22:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">323</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">23:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">10:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">11:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">18:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">21:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">00:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">10:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">12:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">14:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">17:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">19:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">20:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">23:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">322</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">278</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">09:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">340</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">13:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">361</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">13:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">350</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">15:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">317</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">15:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">16:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">17:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">355</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">19:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">21:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">01:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">07:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">09:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">09:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">10:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">13:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">14:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">15:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">17:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">23:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">80</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">107</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">07:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">09:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">17:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">18:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">18:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">19:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">20:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">21:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">22:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">00:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">63</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">05:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">10:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">10:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">12:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">14:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">15:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">16:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">20:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">22:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">00:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">06:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">07:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">111</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">09:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">14:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">15:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">18:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">19:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">20:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">22:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">00:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">10:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">10:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">15:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">17:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">17:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">19:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">22:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">00:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">11:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">13:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">15:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">15:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">18:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">362</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">19:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">00:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">06:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">07:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">13:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">116</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">14:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">15:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">323</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">17:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">19:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">22:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">143</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">01:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">100</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">06:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">07:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">09:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">13:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">18:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">20:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">09:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">09:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">14:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">16:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">17:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">19:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">21:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">00:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">01:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">07:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">10:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">396</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">13:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">412</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">13:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">369</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">14:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">15:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">16:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">18:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">20:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">23:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">322</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">06:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">11:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">399</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">22:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">413</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">23:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">347</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">01:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">07:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">10:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">12:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">15:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">15:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">18:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">01:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">10:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">12:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">339</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">13:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">14:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">96</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">19:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">20:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">21:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">06:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">09:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">11:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">13:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">14:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">23:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">06:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">06:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">108</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">10:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">13:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">14:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">16:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">19:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">21:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">06:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">137</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">14:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">14:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">20:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">20:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">22:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">00:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">143</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">15:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">346</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">21:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">367</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">23:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">382</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">12:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">16:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">18:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">20:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">00:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">01:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">10:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">11:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">14:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">376</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">23:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">01:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">07:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">10:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">11:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">17:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">18:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">19:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">10:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">14:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">19:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">21:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">329</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">338</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">00:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">10:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">14:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">15:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">18:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">19:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">20:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">20:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">00:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">320</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">10:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">17:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">20:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">00:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">09:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">10:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">13:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">21:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">22:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">316</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">07:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">311</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">12:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">13:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">18:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">21:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">299</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">23:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">10:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">12:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">18:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">19:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">22:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">406</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">00:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">486</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">01:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">373</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">11:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">11:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">13:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">14:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">15:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">17:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">135</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">18:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">456</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">22:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">386</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">22:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">01:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">07:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">09:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">12:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">12:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">15:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">19:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">21:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">00:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">05:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">07:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">361</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">14:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">17:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">19:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">417</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">22:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">358</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">07:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">10:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">11:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">14:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">15:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">17:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">23:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">07:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">09:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">15:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">15:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">18:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">22:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">00:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">13:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">18:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">395</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">21:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">384</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">23:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">23:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">00:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">83</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">69</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">90</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">69</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">10:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">73</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">11:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">11:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">314</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">18:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">20:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">01:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">320</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">11:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">18:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">20:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">21:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">07:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">14:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">15:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">17:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">21:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">23:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">321</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">08:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">14:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">16:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">18:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">20:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">21:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">23:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">06:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">09:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">11:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">16:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">17:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">19:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">96</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">00:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">13:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">108</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">14:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">16:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">22:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">00:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">06:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">09:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">09:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">13:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">14:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">19:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">443</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">22:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">449</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">00:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">410</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">00:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">02:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">10:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">12:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">16:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">18:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">20:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">360</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">00:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">370</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">00:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">01:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">143</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">10:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">11:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">12:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">15:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">17:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">321</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">22:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">23:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">08:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">09:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">12:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">18:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">21:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">05:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">09:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">16:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">18:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">109</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">19:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">67</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">19:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">125</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">20:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">21:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">23:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">09:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">09:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">14:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">14:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">17:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">19:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">20:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">05:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">09:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">11:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">12:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">13:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">18:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">21:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">22:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">10:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">11:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">14:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">14:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">17:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">19:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">367</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">20:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">23:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">10:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">11:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">11:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">12:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">18:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">18:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">01:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">93</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">11:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">14:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">19:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">339</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">21:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">368</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">22:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">353</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">23:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">108</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">08:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">10:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">12:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">16:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">100</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">19:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">20:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">00:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">149</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">13:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">15:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">21:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">00:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">05:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">06:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">306</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">08:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">10:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">10:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">124</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">15:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">17:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">20:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">00:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">06:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">09:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">12:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">110</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">13:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">95</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">14:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">122</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">14:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">18:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">123</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">18:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">20:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">22:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">06:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">06:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">12:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">14:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">15:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">18:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">21:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">355</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">00:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">05:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">11:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">12:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">14:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">377</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">23:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">07:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">10:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">12:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">18:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">19:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">337</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">10:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">14:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">15:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">19:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">21:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">149</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">10:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">13:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">14:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">18:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">19:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">20:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">23:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">08:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">12:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">14:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">17:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">17:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">18:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">23:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">06:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">359</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">287</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">16:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">18:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-02-03</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">314</BG>
    <REC_DATE Type="91">2006-02-03</REC_DATE>
    <REC_TIME Type="92">09:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">10:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">11:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">16:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">18:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">21:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">21:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">23:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">331</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">05:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">09:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">11:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">12:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">17:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">19:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">21:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">23:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">03:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">05:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">09:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">15:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">16:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">19:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">21:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">05:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">10:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">12:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">14:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">16:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">19:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">05:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">06:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">08:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">09:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">13:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">14:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">351</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">19:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">20:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">22:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">102</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">07:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">09:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">10:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">14:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">16:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">21:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">04:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">06:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">335</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">22:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">321</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">23:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">15:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">363</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">22:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">08:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">16:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">17:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">415</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">00:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">05:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">287</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">17:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">17:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">17:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">20:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">06:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">08:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">09:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">12:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">17:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">19:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">21:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">22:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">05:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">05:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">09:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">147</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">11:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">14:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">314</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">19:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">23:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">05:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">06:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">13:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">14:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">17:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">109</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">20:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">21:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">05:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">11:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">15:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">15:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">139</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">19:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">20:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">05:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">139</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">08:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">09:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">09:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">18:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">20:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">23:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">06:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">09:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">12:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">15:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">17:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">19:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">19:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">04:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">05:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">09:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">16:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">18:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">21:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">05:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">08:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">10:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">10:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">12:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">13:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">16:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">22:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">05:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">123</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">05:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">06:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">08:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">12:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">14:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">16:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">16:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">19:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">05:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">08:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">10:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">14:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">17:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">18:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">20:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">22:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">05:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">05:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">331</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">09:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">10:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">10:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">15:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">16:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">17:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">05:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">07:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">12:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">329</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">14:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">17:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">22:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">05:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">11:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">13:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">14:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">14:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">16:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">18:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">05:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">11:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">11:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">21:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">22:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">04:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">06:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">11:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">312</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">13:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">11:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">19:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">21:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">03:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">04:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">06:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">08:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">13:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">169</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">16:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">19:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">06:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">94</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">77</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">21:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">98</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">21:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">122</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">22:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">22:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">05:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">08:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">12:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">14:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">19:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">346</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">21:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">04:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">147</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">10:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">359</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">13:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">373</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">22:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">22:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">05:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">09:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">415</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">19:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">19:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">21:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">23:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">05:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">08:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">16:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">17:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">19:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">135</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">22:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">05:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">08:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">278</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">15:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">16:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">22:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">116</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">05:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">06:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">12:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">14:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">15:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">16:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">22:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">05:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">09:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">14:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">16:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">340</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">19:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">23:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">06:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">09:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">10:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">11:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">13:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">14:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">16:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">22:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">05:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">06:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">08:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">12:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">15:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">16:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">19:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">303</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">22:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">05:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">08:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">09:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">12:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">13:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">16:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">133</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">16:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">17:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">18:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">19:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">21:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">23:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">08:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">11:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">306</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">11:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">15:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">19:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">05:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">06:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">08:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">11:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">13:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">15:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">17:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">18:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">22:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">00:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">09:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">12:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">15:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">19:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">21:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">22:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">05:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">06:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">08:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">09:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">12:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">17:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">333</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">22:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">125</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">98</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">05:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">06:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">17:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">19:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">20:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">21:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">08:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">09:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">10:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">75</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">20:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">22:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">04:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">09:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">12:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">13:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">15:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">16:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">19:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">22:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">22:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">05:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">05:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">08:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">11:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">13:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">15:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">16:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">139</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">19:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">21:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">23:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">05:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">09:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">10:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">11:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">13:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">13:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">14:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-06-10</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-06-11</REC_DATE>
    <REC_TIME Type="92">20:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-06-11</REC_DATE>
    <REC_TIME Type="92">23:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">05:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">05:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">169</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">10:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">12:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">14:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">16:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">16:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">18:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">18:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">19:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">21:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">23:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">05:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">05:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">08:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">11:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">12:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">13:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">14:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">20:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">393</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">22:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">350</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">05:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">322</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">22:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">22:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">90</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">04:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">80</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">06:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">06:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">08:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">09:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">11:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">12:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">14:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">16:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">17:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">18:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">21:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">23:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">11:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">12:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">13:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">14:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">17:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">22:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">00:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">09:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">10:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">11:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">13:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">14:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">15:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">17:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">20:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">325</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">21:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">23:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">09:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">13:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">16:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">18:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">21:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">413</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">22:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">07:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">10:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">11:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">13:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">17:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">20:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">08:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">11:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">12:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">12:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">13:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">14:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">16:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">18:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">369</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">22:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">10:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">12:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">15:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">15:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">17:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">20:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">364</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">09:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">10:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">11:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">13:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">14:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">15:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">18:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">22:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">00:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">299</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">13:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">14:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">15:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">17:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">22:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">23:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">278</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">00:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">01:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">10:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">11:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">14:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">15:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">17:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">20:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">22:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">10:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">13:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">13:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">16:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">18:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">21:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">23:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">08:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">10:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">11:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">11:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">12:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">12:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">12:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">15:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">88</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">16:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">17:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">19:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">21:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">23:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">09:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">10:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">11:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">13:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">15:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">303</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">00:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">08:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">11:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">14:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">15:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">18:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">21:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">23:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">06:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">09:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">12:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">16:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">23:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">05:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">11:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">11:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">14:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">17:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">20:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">23:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">03:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">09:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">11:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">16:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">18:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">20:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">23:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">05:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">07:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">09:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">10:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">13:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">15:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">20:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">22:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">09:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">13:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">14:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">17:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">18:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">71</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">21:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">21:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">09:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">12:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">14:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">16:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">17:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">19:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">22:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">06:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">14:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">17:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">21:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">22:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">313</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">05:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">07:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">299</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">11:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">12:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">15:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">17:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">22:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">23:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">00:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">350</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">10:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">13:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">15:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">23:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">00:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">05:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">06:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">06:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">09:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">11:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">12:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">13:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">18:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">22:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">04:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">08:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">09:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">109</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">15:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">93</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">16:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">16:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">16:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">17:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">20:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">22:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">10:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">12:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">13:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">17:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">21:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">23:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">05:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">05:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">09:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">12:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">18:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">21:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">21:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">05:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">311</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">07:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">07:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">08:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">11:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">14:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">15:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">20:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">22:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">23:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">09:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">11:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">137</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">13:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">17:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">19:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">303</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">09:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">123</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">19:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">400</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">23:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">335</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">23:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">05:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">07:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">09:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">11:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">12:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">18:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">21:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">124</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">22:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">11:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">13:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">14:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">15:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">17:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">70</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">19:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">106</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">19:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">21:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">23:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">05:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">14:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">16:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">20:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">22:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">23:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">05:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">13:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">16:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">20:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">65</BG>
    <REC_DATE Type="91">2006-09-01</REC_DATE>
    <REC_TIME Type="92">05:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
</ezManagerMax_Data>
"""

    # Data from Ultra(RRF49A3QT),  UltraSmart(THF05CBNV) and 2020(27-04663-12)
    data3 = """!TAG!SERNO!DVU345678901!<?xml version="1.0" encoding="UTF-8"?>
<ezManagerMax_Data>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">03:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">17:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">03:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-20</REC_DATE>
    <REC_TIME Type="92">17:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-20</REC_DATE>
    <REC_TIME Type="92">04:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">03:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">341</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">03:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">03:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">06:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">336</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">04:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">03:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">133</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">03:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">13:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">08:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">03:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">393</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">73</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">08:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-02-06</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">07:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">17:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">16:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">21:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">16:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">14:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">20:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">405</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">19:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">17:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">07:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">09:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">08:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">08:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">07:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">07:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">08:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">08:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">08:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">06:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">370</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">08:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">07:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">10:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">60</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">08:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">08:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-21</REC_DATE>
    <REC_TIME Type="92">07:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-11-20</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-11-19</REC_DATE>
    <REC_TIME Type="92">05:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-11-15</REC_DATE>
    <REC_TIME Type="92">08:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-14</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-11-10</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-11-09</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-11-08</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-11-07</REC_DATE>
    <REC_TIME Type="92">08:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-11-06</REC_DATE>
    <REC_TIME Type="92">08:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-11-05</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-11-04</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-11-03</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-02</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-11-01</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-10-31</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">417</BG>
    <REC_DATE Type="91">2005-10-30</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-10-30</REC_DATE>
    <REC_TIME Type="92">08:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-10-29</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-10-28</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-10-27</REC_DATE>
    <REC_TIME Type="92">07:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2005-10-26</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-10-25</REC_DATE>
    <REC_TIME Type="92">06:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-10-24</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2005-10-23</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-10-22</REC_DATE>
    <REC_TIME Type="92">07:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-10-21</REC_DATE>
    <REC_TIME Type="92">14:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-10-21</REC_DATE>
    <REC_TIME Type="92">07:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-10-20</REC_DATE>
    <REC_TIME Type="92">06:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-10-19</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-10-18</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2005-10-17</REC_DATE>
    <REC_TIME Type="92">17:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-10-17</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-10-16</REC_DATE>
    <REC_TIME Type="92">07:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-10-15</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-10-14</REC_DATE>
    <REC_TIME Type="92">06:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-10-13</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-10-12</REC_DATE>
    <REC_TIME Type="92">03:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-10-10</REC_DATE>
    <REC_TIME Type="92">06:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-10-09</REC_DATE>
    <REC_TIME Type="92">05:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-10-08</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-10-07</REC_DATE>
    <REC_TIME Type="92">07:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2005-08-24</REC_DATE>
    <REC_TIME Type="92">17:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">09:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">09:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">11:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">18:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">21:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">22:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2005-08-25</REC_DATE>
    <REC_TIME Type="92">22:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">00:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">07:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">07:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">09:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">10:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">11:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">12:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">15:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">17:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">306</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">18:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">20:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">22:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-08-26</REC_DATE>
    <REC_TIME Type="92">23:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">10:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">16:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">18:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">18:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">124</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">18:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">20:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-08-27</REC_DATE>
    <REC_TIME Type="92">23:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">10:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">11:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">13:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">15:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">17:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">19:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">373</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">20:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2005-08-28</REC_DATE>
    <REC_TIME Type="92">22:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">06:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">07:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">08:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">10:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">169</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">14:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">14:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">17:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">19:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-08-29</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">08:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">09:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">12:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">14:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">14:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">17:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">18:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">20:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-08-30</REC_DATE>
    <REC_TIME Type="92">23:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">07:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">08:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">10:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">13:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">14:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">15:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">17:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-08-31</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">08:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">11:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-09-01</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">311</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">10:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">13:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">15:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">17:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">17:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">22:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-09-02</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">07:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">12:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">14:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">16:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">18:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">22:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2005-09-03</REC_DATE>
    <REC_TIME Type="92">23:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">06:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">10:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">13:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">13:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">15:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">17:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">17:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">18:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">20:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">22:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2005-09-04</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">09:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">10:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">149</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">12:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">17:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">19:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">21:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-09-05</REC_DATE>
    <REC_TIME Type="92">22:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">07:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">09:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">11:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">328</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">329</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">15:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">17:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">18:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">20:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">21:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-09-06</REC_DATE>
    <REC_TIME Type="92">21:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">06:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">07:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">07:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">09:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">10:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">11:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">13:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">15:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">17:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">20:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-07</REC_DATE>
    <REC_TIME Type="92">22:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">00:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">06:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">08:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">09:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">10:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">16:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">18:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">20:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-08</REC_DATE>
    <REC_TIME Type="92">22:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">00:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">09:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">137</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">16:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">17:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">20:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">328</BG>
    <REC_DATE Type="91">2005-09-09</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">368</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">09:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">11:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">15:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">334</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-09-10</REC_DATE>
    <REC_TIME Type="92">23:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">06:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">09:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">11:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">12:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">13:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">317</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">15:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">381</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">18:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">21:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-09-11</REC_DATE>
    <REC_TIME Type="92">22:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">07:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">09:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">12:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">15:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">20:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-09-12</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">09:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">14:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">16:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">18:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">372</BG>
    <REC_DATE Type="91">2005-09-13</REC_DATE>
    <REC_TIME Type="92">21:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">06:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">07:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">331</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">09:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">14:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">16:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">17:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">95</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">19:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">93</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">20:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">21:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2005-09-14</REC_DATE>
    <REC_TIME Type="92">21:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">07:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">08:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">10:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">84</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">14:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">15:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">18:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">21:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-09-15</REC_DATE>
    <REC_TIME Type="92">22:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">05:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">05:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">06:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">08:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">10:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">11:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">14:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">17:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">126</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">21:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-09-16</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">07:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">10:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">116</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">15:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">17:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">18:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">20:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">408</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">22:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">390</BG>
    <REC_DATE Type="91">2005-09-17</REC_DATE>
    <REC_TIME Type="92">22:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">335</BG>
    <REC_DATE Type="91">2005-09-18</REC_DATE>
    <REC_TIME Type="92">07:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2005-09-18</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-09-18</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">13:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">14:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">15:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">16:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">19:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">107</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">19:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">19:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-11-24</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">00:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">22:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">23:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">325</BG>
    <REC_DATE Type="91">2005-11-25</REC_DATE>
    <REC_TIME Type="92">23:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">07:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">08:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">13:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">15:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">17:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">18:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">375</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">21:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">547</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">578</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">22:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">500</BG>
    <REC_DATE Type="91">2005-11-26</REC_DATE>
    <REC_TIME Type="92">23:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">511</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">00:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">04:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">10:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">11:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">12:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">83</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">13:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">15:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">16:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">18:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">20:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2005-11-27</REC_DATE>
    <REC_TIME Type="92">21:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">00:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">13:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">17:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">17:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-11-28</REC_DATE>
    <REC_TIME Type="92">22:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">00:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">06:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">11:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">18:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-11-29</REC_DATE>
    <REC_TIME Type="92">23:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">09:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">12:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">13:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">18:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-11-30</REC_DATE>
    <REC_TIME Type="92">20:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">04:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">15:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">17:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">110</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">87</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">90</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">110</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">19:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">20:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-01</REC_DATE>
    <REC_TIME Type="92">22:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">06:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">13:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">17:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">19:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">313</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">20:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">22:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">323</BG>
    <REC_DATE Type="91">2005-12-02</REC_DATE>
    <REC_TIME Type="92">23:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">10:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">11:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">18:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">21:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-12-03</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">00:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">07:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">10:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">12:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">14:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">17:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">19:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">20:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-04</REC_DATE>
    <REC_TIME Type="92">23:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">322</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">278</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">09:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">340</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">13:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">361</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">13:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">350</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">15:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">317</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">15:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">16:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">17:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">355</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">19:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">21:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-12-05</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">01:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">07:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">09:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">09:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">10:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">13:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">14:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">15:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">17:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2005-12-06</REC_DATE>
    <REC_TIME Type="92">23:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">80</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">107</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">07:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">09:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">17:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">18:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">18:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">19:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">20:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">21:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2005-12-07</REC_DATE>
    <REC_TIME Type="92">22:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">00:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">63</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">05:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">10:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">10:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">12:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">14:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">15:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">16:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">20:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2005-12-08</REC_DATE>
    <REC_TIME Type="92">22:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">00:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">06:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">07:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">111</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">09:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">14:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">15:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">18:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">19:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">20:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2005-12-09</REC_DATE>
    <REC_TIME Type="92">22:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">00:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">07:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">10:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">10:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">15:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">17:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">17:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">19:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-10</REC_DATE>
    <REC_TIME Type="92">22:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">00:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">11:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">13:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">15:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">15:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">18:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">362</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">19:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2005-12-11</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">00:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">06:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">07:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">13:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">116</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">14:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">15:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">323</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">17:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">19:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2005-12-12</REC_DATE>
    <REC_TIME Type="92">22:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">143</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">01:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">100</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">06:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">07:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">09:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">13:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">18:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">20:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-13</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">09:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">09:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">14:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">16:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">17:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">19:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2005-12-14</REC_DATE>
    <REC_TIME Type="92">21:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">00:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">01:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">07:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">10:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">396</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">13:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">412</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">13:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">369</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">14:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">15:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">16:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">18:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">20:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2005-12-15</REC_DATE>
    <REC_TIME Type="92">23:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">322</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">06:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">11:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">399</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">22:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">413</BG>
    <REC_DATE Type="91">2005-12-16</REC_DATE>
    <REC_TIME Type="92">23:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">347</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">01:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">07:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">10:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">12:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">15:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">15:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-12-17</REC_DATE>
    <REC_TIME Type="92">18:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">01:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">10:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">12:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">339</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">13:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">14:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">96</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">19:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">20:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2005-12-18</REC_DATE>
    <REC_TIME Type="92">21:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">06:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">09:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">11:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">13:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">14:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2005-12-19</REC_DATE>
    <REC_TIME Type="92">23:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">06:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">06:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">108</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">10:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">12:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">13:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">14:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">16:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">19:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">21:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2005-12-20</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">06:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">137</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">14:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">14:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">20:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">20:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2005-12-21</REC_DATE>
    <REC_TIME Type="92">22:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">00:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">143</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">15:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">346</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">21:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">367</BG>
    <REC_DATE Type="91">2005-12-22</REC_DATE>
    <REC_TIME Type="92">23:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">382</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">07:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">12:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">16:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">18:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">20:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-23</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">00:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">01:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">10:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">11:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">14:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">376</BG>
    <REC_DATE Type="91">2005-12-24</REC_DATE>
    <REC_TIME Type="92">23:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">01:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">07:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">10:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">11:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">17:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">18:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">19:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2005-12-25</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">07:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">10:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">14:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">19:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">21:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">329</BG>
    <REC_DATE Type="91">2005-12-26</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">338</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">00:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">08:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">10:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">12:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">14:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">15:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">18:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">19:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">20:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2005-12-27</REC_DATE>
    <REC_TIME Type="92">20:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">00:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">320</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">10:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">17:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2005-12-28</REC_DATE>
    <REC_TIME Type="92">20:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">00:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">09:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">10:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">13:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">21:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2005-12-29</REC_DATE>
    <REC_TIME Type="92">22:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">316</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">07:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">311</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">12:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">13:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">18:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">21:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">299</BG>
    <REC_DATE Type="91">2005-12-30</REC_DATE>
    <REC_TIME Type="92">23:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">10:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">12:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">18:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">19:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2005-12-31</REC_DATE>
    <REC_TIME Type="92">22:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">406</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">00:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">486</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">01:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">373</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">11:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">11:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">13:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">14:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">15:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">17:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">135</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">18:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">456</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">22:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">386</BG>
    <REC_DATE Type="91">2006-01-01</REC_DATE>
    <REC_TIME Type="92">22:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">01:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">07:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">09:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">12:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">12:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">15:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">19:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-02</REC_DATE>
    <REC_TIME Type="92">21:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">00:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">05:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">07:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">361</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">09:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">14:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">17:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">19:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">417</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">22:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">358</BG>
    <REC_DATE Type="91">2006-01-03</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">07:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">10:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">11:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">14:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">15:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">17:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-01-04</REC_DATE>
    <REC_TIME Type="92">23:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">07:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">09:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">15:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">15:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">18:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-01-05</REC_DATE>
    <REC_TIME Type="92">22:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">00:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">13:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">18:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">395</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">21:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">384</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">23:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-06</REC_DATE>
    <REC_TIME Type="92">23:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">00:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">83</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">69</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">90</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">69</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">10:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">73</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">11:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">11:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">314</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">18:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">20:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2006-01-07</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">01:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">07:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">320</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">11:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">18:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">20:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">21:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-01-08</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">07:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">08:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">09:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">12:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">14:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">15:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">17:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">21:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-09</REC_DATE>
    <REC_TIME Type="92">23:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">321</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">07:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">08:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">14:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">16:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">18:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">20:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">21:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-01-10</REC_DATE>
    <REC_TIME Type="92">23:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">06:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">09:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">11:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">16:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">17:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">19:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">96</BG>
    <REC_DATE Type="91">2006-01-11</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">00:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">06:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">114</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">13:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">108</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">14:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">15:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">16:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-01-12</REC_DATE>
    <REC_TIME Type="92">22:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">00:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">06:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">09:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">09:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">13:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">14:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">19:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">443</BG>
    <REC_DATE Type="91">2006-01-13</REC_DATE>
    <REC_TIME Type="92">22:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">449</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">00:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">410</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">00:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">02:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">10:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">12:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">16:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">18:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-01-14</REC_DATE>
    <REC_TIME Type="92">20:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">360</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">00:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">370</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">00:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">01:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">07:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">143</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">10:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">11:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">12:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">13:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">15:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">17:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">321</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">22:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-01-15</REC_DATE>
    <REC_TIME Type="92">23:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">07:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">08:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">09:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">12:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">18:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">21:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-01-16</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">05:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">09:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">16:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">18:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">109</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">19:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">67</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">19:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">125</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">20:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">21:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-01-17</REC_DATE>
    <REC_TIME Type="92">23:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">09:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">09:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">14:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">290</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">14:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">17:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">19:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-01-18</REC_DATE>
    <REC_TIME Type="92">20:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">05:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">06:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">09:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">11:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">12:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">13:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">18:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">21:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-19</REC_DATE>
    <REC_TIME Type="92">22:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">10:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">11:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">14:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">14:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">17:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">19:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">367</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">20:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-20</REC_DATE>
    <REC_TIME Type="92">23:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">10:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">11:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">11:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">12:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">18:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-01-21</REC_DATE>
    <REC_TIME Type="92">18:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">01:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">08:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">93</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">11:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">14:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">19:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">339</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">21:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">368</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">22:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">353</BG>
    <REC_DATE Type="91">2006-01-22</REC_DATE>
    <REC_TIME Type="92">23:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">108</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">08:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">09:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">10:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">12:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">16:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">206</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">100</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">19:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">20:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-23</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">00:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">12:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">149</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">13:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">15:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-24</REC_DATE>
    <REC_TIME Type="92">21:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">00:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">05:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">06:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">306</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">07:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">08:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">10:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">10:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">124</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">12:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">15:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">17:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-01-25</REC_DATE>
    <REC_TIME Type="92">20:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">00:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">112</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">06:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">09:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">12:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">110</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">13:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">95</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">14:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">122</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">14:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">18:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">123</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">18:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">20:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-26</REC_DATE>
    <REC_TIME Type="92">22:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">06:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">06:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">12:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">14:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">15:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">18:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2006-01-27</REC_DATE>
    <REC_TIME Type="92">21:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">355</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">00:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">05:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">11:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">12:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">14:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">377</BG>
    <REC_DATE Type="91">2006-01-28</REC_DATE>
    <REC_TIME Type="92">23:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">07:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">10:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">12:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">12:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">18:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">19:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-01-29</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">337</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">10:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">14:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">15:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">19:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-01-30</REC_DATE>
    <REC_TIME Type="92">21:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">149</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">10:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">13:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">14:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">18:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">19:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">20:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-01-31</REC_DATE>
    <REC_TIME Type="92">23:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">08:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">12:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">14:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">17:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">17:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">18:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">20:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">20:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-02-01</REC_DATE>
    <REC_TIME Type="92">23:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">06:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">359</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">08:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">09:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">12:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">287</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">16:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">18:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">21:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-02-02</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-02-03</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">314</BG>
    <REC_DATE Type="91">2006-02-03</REC_DATE>
    <REC_TIME Type="92">09:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">10:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">11:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">12:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">14:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">16:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">18:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">21:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">21:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-04-08</REC_DATE>
    <REC_TIME Type="92">23:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">331</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">05:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">09:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">11:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">12:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">17:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">19:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">21:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-04-09</REC_DATE>
    <REC_TIME Type="92">23:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">03:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">05:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">09:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">15:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">16:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">19:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-04-10</REC_DATE>
    <REC_TIME Type="92">21:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">05:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">09:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">10:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">12:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">14:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">16:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">19:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">78</BG>
    <REC_DATE Type="91">2006-04-11</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">05:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">06:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">08:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">09:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">13:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">14:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">17:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">351</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">19:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">319</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">20:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-04-12</REC_DATE>
    <REC_TIME Type="92">22:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">102</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">06:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">07:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">09:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">10:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">14:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">16:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">21:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">342</BG>
    <REC_DATE Type="91">2006-04-13</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">04:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">06:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">335</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">22:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">321</BG>
    <REC_DATE Type="91">2006-04-14</REC_DATE>
    <REC_TIME Type="92">23:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">08:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">15:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">363</BG>
    <REC_DATE Type="91">2006-04-15</REC_DATE>
    <REC_TIME Type="92">22:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">06:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">08:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">16:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">17:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">415</BG>
    <REC_DATE Type="91">2006-04-16</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">00:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">05:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">287</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">17:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">17:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">17:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">20:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-04-17</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">06:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">08:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">09:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">12:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">17:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">19:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">21:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-04-18</REC_DATE>
    <REC_TIME Type="92">22:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">05:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">05:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">09:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">147</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">11:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">14:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">314</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">19:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-04-19</REC_DATE>
    <REC_TIME Type="92">23:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">05:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">06:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">07:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">07:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">13:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">14:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">17:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">109</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">20:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-04-20</REC_DATE>
    <REC_TIME Type="92">21:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">05:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">11:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">15:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">15:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">139</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">19:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-04-21</REC_DATE>
    <REC_TIME Type="92">20:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">05:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">139</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">08:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">09:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">09:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">18:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">20:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-22</REC_DATE>
    <REC_TIME Type="92">23:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">06:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">09:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">12:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">15:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">17:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">19:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">19:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-04-23</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">04:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">05:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">09:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">16:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">18:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-04-24</REC_DATE>
    <REC_TIME Type="92">21:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">05:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">162</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">08:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">10:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">10:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">12:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">13:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">16:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-04-25</REC_DATE>
    <REC_TIME Type="92">22:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">05:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">123</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">05:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">06:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">08:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">08:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">12:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">14:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">16:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">16:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">326</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">19:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-04-26</REC_DATE>
    <REC_TIME Type="92">21:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">05:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">08:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">10:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">14:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">17:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">18:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">127</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">20:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">144</BG>
    <REC_DATE Type="91">2006-04-27</REC_DATE>
    <REC_TIME Type="92">22:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">05:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">05:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">331</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">07:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">09:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">10:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">167</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">10:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-04-28</REC_DATE>
    <REC_TIME Type="92">12:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">15:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">16:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">17:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-05-01</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">05:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">07:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">136</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">12:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">329</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">14:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">17:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-05-02</REC_DATE>
    <REC_TIME Type="92">22:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">05:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">11:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">13:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">14:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">14:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">16:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">18:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-05-03</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">05:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">08:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">11:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">11:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">21:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-04</REC_DATE>
    <REC_TIME Type="92">22:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">04:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">309</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">06:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">11:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">312</BG>
    <REC_DATE Type="91">2006-05-05</REC_DATE>
    <REC_TIME Type="92">13:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">11:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">19:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">21:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-05-07</REC_DATE>
    <REC_TIME Type="92">21:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">03:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">04:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">06:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">08:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">11:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">13:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">169</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">16:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">19:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-05-08</REC_DATE>
    <REC_TIME Type="92">22:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">05:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">06:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">300</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">07:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">08:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">11:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">13:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">15:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">94</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">77</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">21:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">98</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">21:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">122</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">22:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-05-09</REC_DATE>
    <REC_TIME Type="92">22:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">05:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">310</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">08:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">09:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">11:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">12:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">14:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">19:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">346</BG>
    <REC_DATE Type="91">2006-05-10</REC_DATE>
    <REC_TIME Type="92">21:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">131</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">04:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">05:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">08:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">147</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">10:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">359</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">13:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">20:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">373</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">22:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">344</BG>
    <REC_DATE Type="91">2006-05-11</REC_DATE>
    <REC_TIME Type="92">22:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">05:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">09:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">14:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-05-12</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">415</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">19:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">354</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">19:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">21:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">23:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-05-14</REC_DATE>
    <REC_TIME Type="92">23:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">05:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">08:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">243</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">16:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">17:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">19:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">135</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-15</REC_DATE>
    <REC_TIME Type="92">22:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">05:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">08:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">08:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">278</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">15:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">16:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-05-16</REC_DATE>
    <REC_TIME Type="92">22:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">116</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">05:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">06:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">12:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">14:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">15:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">16:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">20:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-05-17</REC_DATE>
    <REC_TIME Type="92">22:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">05:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">09:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">13:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">13:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">14:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">16:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">340</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">19:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-05-18</REC_DATE>
    <REC_TIME Type="92">23:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">06:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">09:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">10:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">11:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">13:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-05-19</REC_DATE>
    <REC_TIME Type="92">14:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">16:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-05-21</REC_DATE>
    <REC_TIME Type="92">22:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">05:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">06:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">08:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">12:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">15:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">16:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">19:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">303</BG>
    <REC_DATE Type="91">2006-05-22</REC_DATE>
    <REC_TIME Type="92">22:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">05:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">08:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">09:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">12:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">13:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">14:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">16:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">133</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">16:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">17:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">18:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">19:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">20:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">21:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-05-23</REC_DATE>
    <REC_TIME Type="92">23:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">08:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">08:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">11:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">306</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">13:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-05-24</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">11:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">238</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">15:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">129</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">19:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-05-28</REC_DATE>
    <REC_TIME Type="92">21:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">05:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">06:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">08:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">11:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">13:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">15:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">17:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">281</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">18:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-05-29</REC_DATE>
    <REC_TIME Type="92">22:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">00:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">06:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">09:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">11:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">12:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">273</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">15:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">269</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">19:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">21:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-05-30</REC_DATE>
    <REC_TIME Type="92">22:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">05:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">324</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">06:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">08:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">09:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">12:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">17:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">333</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">280</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">22:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">125</BG>
    <REC_DATE Type="91">2006-05-31</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">98</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">05:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">06:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">15:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">17:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">19:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">20:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-06-01</REC_DATE>
    <REC_TIME Type="92">21:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">08:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">08:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">09:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">10:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-06-02</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">75</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">19:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">20:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-06-06</REC_DATE>
    <REC_TIME Type="92">22:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">04:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">08:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">09:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">12:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">183</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">13:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">15:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">16:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">19:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">22:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-06-07</REC_DATE>
    <REC_TIME Type="92">22:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">05:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">05:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">115</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">08:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">289</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">11:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">13:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">15:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">16:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">139</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">19:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">21:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">307</BG>
    <REC_DATE Type="91">2006-06-08</REC_DATE>
    <REC_TIME Type="92">23:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">05:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">09:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">10:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">11:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">13:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">13:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-06-09</REC_DATE>
    <REC_TIME Type="92">14:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-06-10</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-06-11</REC_DATE>
    <REC_TIME Type="92">20:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-06-11</REC_DATE>
    <REC_TIME Type="92">23:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">05:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">05:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">08:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">169</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">08:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">10:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">10:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">12:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">14:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">16:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">16:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">18:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">272</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">18:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">292</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">19:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">21:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-06-12</REC_DATE>
    <REC_TIME Type="92">23:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">05:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">05:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">07:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">08:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">134</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">11:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">12:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">13:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">14:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">301</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">20:26:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">393</BG>
    <REC_DATE Type="91">2006-06-13</REC_DATE>
    <REC_TIME Type="92">22:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">350</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">05:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">07:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">10:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">322</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">22:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-06-14</REC_DATE>
    <REC_TIME Type="92">22:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">90</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">04:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">80</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">06:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">06:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">103</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">06:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">08:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">09:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-06-15</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">11:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">12:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">268</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">14:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">16:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">17:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">18:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">21:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-07-29</REC_DATE>
    <REC_TIME Type="92">23:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">308</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">11:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">12:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">348</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">13:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">247</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">14:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">17:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">20:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">327</BG>
    <REC_DATE Type="91">2006-07-30</REC_DATE>
    <REC_TIME Type="92">22:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">00:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">305</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">06:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">09:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">10:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">11:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">270</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">13:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">193</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">14:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">15:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">17:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">20:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">325</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">21:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-07-31</REC_DATE>
    <REC_TIME Type="92">23:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">279</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">06:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">09:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">13:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">16:47:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">18:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">21:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">413</BG>
    <REC_DATE Type="91">2006-08-01</REC_DATE>
    <REC_TIME Type="92">22:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">284</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">07:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">07:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">10:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">11:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">13:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">17:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">185</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">20:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-08-02</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">00:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">07:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">08:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">214</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">10:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">11:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">12:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">145</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">12:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">12:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">13:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">244</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">14:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">16:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">18:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">369</BG>
    <REC_DATE Type="91">2006-08-03</REC_DATE>
    <REC_TIME Type="92">22:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">10:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">12:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">12:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">150</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">15:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">213</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">15:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">17:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">20:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">364</BG>
    <REC_DATE Type="91">2006-08-04</REC_DATE>
    <REC_TIME Type="92">22:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">09:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">158</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">10:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">228</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">11:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">285</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">12:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">13:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">267</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">14:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">211</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">15:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">288</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">18:46:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">343</BG>
    <REC_DATE Type="91">2006-08-05</REC_DATE>
    <REC_TIME Type="92">22:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">00:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">74</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">07:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">09:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">11:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">299</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">12:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">13:37:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">14:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">15:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">17:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">22:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">260</BG>
    <REC_DATE Type="91">2006-08-06</REC_DATE>
    <REC_TIME Type="92">23:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">278</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">00:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">01:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">09:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">187</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">10:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">11:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">12:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">117</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">14:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">15:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">17:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">20:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">22:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">297</BG>
    <REC_DATE Type="91">2006-08-07</REC_DATE>
    <REC_TIME Type="92">23:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">298</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">197</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">10:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">13:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">205</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">13:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">14:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">16:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">277</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">18:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">209</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">19:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">21:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">219</BG>
    <REC_DATE Type="91">2006-08-08</REC_DATE>
    <REC_TIME Type="92">23:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">141</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">08:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">151</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">10:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">11:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">11:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">179</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">11:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">156</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">12:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">172</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">12:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">12:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">118</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">15:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">88</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">16:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">17:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">19:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">21:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-09</REC_DATE>
    <REC_TIME Type="92">23:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">261</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">09:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">10:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">227</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">11:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">121</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">13:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">302</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">15:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">303</BG>
    <REC_DATE Type="91">2006-08-10</REC_DATE>
    <REC_TIME Type="92">18:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">00:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">06:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">08:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">215</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">11:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">236</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">14:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">15:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">18:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">19:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">21:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-08-11</REC_DATE>
    <REC_TIME Type="92">23:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">06:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">09:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">174</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">12:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">14:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">16:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">250</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">295</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">20:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">330</BG>
    <REC_DATE Type="91">2006-08-12</REC_DATE>
    <REC_TIME Type="92">23:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">229</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">00:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">05:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">113</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">11:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">186</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">11:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">14:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">17:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">20:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-08-13</REC_DATE>
    <REC_TIME Type="92">23:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">173</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">03:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">237</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">09:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">11:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">14:32:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">201</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">16:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">220</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">16:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">18:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">20:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">177</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">22:49:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">159</BG>
    <REC_DATE Type="91">2006-08-14</REC_DATE>
    <REC_TIME Type="92">23:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">05:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">07:00:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">09:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">194</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">10:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">13:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">13:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">266</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">15:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">241</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">20:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-08-15</REC_DATE>
    <REC_TIME Type="92">22:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">128</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">06:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">09:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">232</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">13:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">14:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">17:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">171</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">18:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">71</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">21:13:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">148</BG>
    <REC_DATE Type="91">2006-08-16</REC_DATE>
    <REC_TIME Type="92">21:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">291</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">00:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">06:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">09:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">11:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">242</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">12:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">188</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">14:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">248</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">16:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">17:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">251</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">19:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">22:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-08-17</REC_DATE>
    <REC_TIME Type="92">23:16:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">296</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">282</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">06:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">184</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">09:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">14:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">218</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">17:02:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">21:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">304</BG>
    <REC_DATE Type="91">2006-08-18</REC_DATE>
    <REC_TIME Type="92">22:01:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">313</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">05:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">318</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">07:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">299</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">08:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">195</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">153</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">11:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">202</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">12:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">315</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">15:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">176</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">17:14:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">166</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">17:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">22:39:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">192</BG>
    <REC_DATE Type="91">2006-08-19</REC_DATE>
    <REC_TIME Type="92">23:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">235</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">00:31:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">350</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">06:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">10:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">203</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">13:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">198</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">207</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">15:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">17:15:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">19:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">132</BG>
    <REC_DATE Type="91">2006-08-20</REC_DATE>
    <REC_TIME Type="92">23:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">178</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">00:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">05:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">06:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">263</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">06:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">224</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">08:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">249</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">09:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">256</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">11:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">12:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">13:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">17:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">276</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">18:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">20:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-21</REC_DATE>
    <REC_TIME Type="92">22:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">257</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">04:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">253</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">08:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">175</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">09:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">286</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">163</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">13:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">109</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">15:24:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">93</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">16:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">89</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">16:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">119</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">16:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">17:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">293</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">18:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">20:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">212</BG>
    <REC_DATE Type="91">2006-08-22</REC_DATE>
    <REC_TIME Type="92">22:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">283</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">05:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">10:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">208</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">11:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">12:35:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">190</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">13:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">264</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">14:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">255</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">17:12:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">99</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">19:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">21:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">199</BG>
    <REC_DATE Type="91">2006-08-23</REC_DATE>
    <REC_TIME Type="92">23:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">05:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">164</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">05:54:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">08:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">09:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">12:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">230</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">18:28:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">21:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">231</BG>
    <REC_DATE Type="91">2006-08-24</REC_DATE>
    <REC_TIME Type="92">21:58:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">332</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">05:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">311</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">07:06:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">275</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">07:23:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">181</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">08:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">09:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">160</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">11:40:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">196</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">14:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">254</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">15:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">233</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">18:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">101</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">20:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">120</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">22:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">222</BG>
    <REC_DATE Type="91">2006-08-25</REC_DATE>
    <REC_TIME Type="92">23:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">262</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">09:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">168</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">11:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">137</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">13:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">226</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">17:19:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">265</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">19:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">303</BG>
    <REC_DATE Type="91">2006-08-26</REC_DATE>
    <REC_TIME Type="92">23:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">06:38:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">189</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">09:09:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">123</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">17:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">142</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">19:08:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">400</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">23:33:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">335</BG>
    <REC_DATE Type="91">2006-08-27</REC_DATE>
    <REC_TIME Type="92">23:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">259</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">05:20:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">234</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">07:57:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">09:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">85</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">11:03:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">138</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">11:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">191</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">12:59:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">245</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">18:48:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">130</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">21:50:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">124</BG>
    <REC_DATE Type="91">2006-08-28</REC_DATE>
    <REC_TIME Type="92">22:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">05:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">217</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">07:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">165</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">204</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">11:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">271</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">13:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">14:36:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">246</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">15:45:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">154</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">17:17:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">70</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">19:25:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">106</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">19:41:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">155</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">21:04:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">240</BG>
    <REC_DATE Type="91">2006-08-29</REC_DATE>
    <REC_TIME Type="92">23:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">274</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">05:10:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">200</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">182</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">10:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">157</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">10:56:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">170</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">13:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">258</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">14:55:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">16:29:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">146</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">18:21:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">140</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">20:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">161</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">22:44:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">210</BG>
    <REC_DATE Type="91">2006-08-30</REC_DATE>
    <REC_TIME Type="92">23:52:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">294</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">05:43:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">221</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">08:34:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">180</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">10:53:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">13:11:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">216</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">13:27:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">239</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">16:51:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">152</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">20:22:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">252</BG>
    <REC_DATE Type="91">2006-08-31</REC_DATE>
    <REC_TIME Type="92">21:42:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">65</BG>
    <REC_DATE Type="91">2006-09-01</REC_DATE>
    <REC_TIME Type="92">05:30:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">2</SOURCE>
  </BGLOG>
  <BOLUSLOG>
    <BOLUS Type="4">455</BOLUS>
    <BOLUS_TYPE Type="4">1</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">13:19:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">605</BOLUS>
    <BOLUS_TYPE Type="4">3</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">240</BOLUS_DURATION>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">12:50:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">0</BOLUS>
    <BOLUS_TYPE Type="4">1</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2007-09-18</REC_DATE>
    <REC_TIME Type="92">03:05:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">0</BOLUS>
    <BOLUS_TYPE Type="4">1</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">15:39:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">0</BOLUS>
    <BOLUS_TYPE Type="4">1</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2007-07-15</REC_DATE>
    <REC_TIME Type="92">19:51:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">380</BOLUS>
    <BOLUS_TYPE Type="4">1</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2007-06-13</REC_DATE>
    <REC_TIME Type="92">15:35:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">500</BOLUS>
    <BOLUS_TYPE Type="4">1</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2007-05-25</REC_DATE>
    <REC_TIME Type="92">10:55:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">01:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">25</BASAL>
    <REC_DATE Type="91">2007-10-26</REC_DATE>
    <REC_TIME Type="92">08:48:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2007-10-26</REC_DATE>
    <REC_TIME Type="92">08:45:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">25</BASAL>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">09:04:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">09:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">25</BASAL>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <REC_TIME Type="92">22:19:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">04:06:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">800</BASAL>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">850</BASAL>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <REC_TIME Type="92">09:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">950</BASAL>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <REC_TIME Type="92">04:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">800</BASAL>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">850</BASAL>
    <REC_DATE Type="91">2007-09-22</REC_DATE>
    <REC_TIME Type="92">09:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">950</BASAL>
    <REC_DATE Type="91">2007-09-22</REC_DATE>
    <REC_TIME Type="92">04:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">800</BASAL>
    <REC_DATE Type="91">2007-09-22</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">850</BASAL>
    <REC_DATE Type="91">2007-09-21</REC_DATE>
    <REC_TIME Type="92">09:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">950</BASAL>
    <REC_DATE Type="91">2007-09-21</REC_DATE>
    <REC_TIME Type="92">04:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">800</BASAL>
    <REC_DATE Type="91">2007-09-21</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">850</BASAL>
    <REC_DATE Type="91">2007-09-20</REC_DATE>
    <REC_TIME Type="92">09:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">950</BASAL>
    <REC_DATE Type="91">2007-09-20</REC_DATE>
    <REC_TIME Type="92">04:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">800</BASAL>
    <REC_DATE Type="91">2007-09-20</REC_DATE>
    <REC_TIME Type="92">00:00:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">850</BASAL>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">17:24:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">425</BASAL>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">13:24:00</REC_TIME>
    <FLAG Type="4">1</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">850</BASAL>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">12:48:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">12:36:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">25</BASAL>
    <REC_DATE Type="91">2007-09-14</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2007-09-13</REC_DATE>
    <REC_TIME Type="92">18:06:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">25</BASAL>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">16:19:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">15:43:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">25</BASAL>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">15:25:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <TDDLOG>
    <BASALT Type="4">0</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <FLAG Type="4">2</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">0</BASALT>
    <BOLUST Type="4">5000</BOLUST>
    <REC_DATE Type="91">2007-05-25</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">0</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-06-07</REC_DATE>
    <FLAG Type="4">1</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">0</BASALT>
    <BOLUST Type="4">3800</BOLUST>
    <REC_DATE Type="91">2007-06-13</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">0</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-07-15</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">192</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <FLAG Type="4">2</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-02</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-03</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-04</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-05</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-06</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-07</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">252</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-08-08</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">356</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-14</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-15</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-16</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-17</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">432</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-18</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">8118</BASALT>
    <BOLUST Type="4">10600</BOLUST>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <FLAG Type="4">1</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">20700</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-20</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">20700</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-21</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">20700</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-22</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">41</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">20700</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">575</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">280</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-25</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-26</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-27</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-28</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-29</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-09-30</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-01</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-02</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-03</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-04</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-05</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-06</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-07</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-08</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-09</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-10</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-11</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-12</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-13</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-14</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-15</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-16</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-17</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-18</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-19</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-20</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-21</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-22</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-23</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-24</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-25</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">574</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-26</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-27</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-28</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-29</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-30</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-10-31</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-01</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-02</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-03</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-04</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-05</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-06</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-07</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-08</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-09</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-10</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-11</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-12</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-13</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-14</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-15</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-16</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-17</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-18</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-19</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-20</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-21</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-22</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-23</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-24</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-25</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-26</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-27</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-28</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-29</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-11-30</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-01</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-02</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-03</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-04</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-05</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-06</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-07</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-08</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-09</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-10</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-11</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-12</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-13</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-14</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-15</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-16</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-17</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-18</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-19</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-20</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-21</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-22</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-23</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-24</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-25</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">576</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-26</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">103</BASALT>
    <BOLUST Type="4">0</BOLUST>
    <REC_DATE Type="91">2007-12-27</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <PUMPSETTINGS>
    <REC_DATE Type="91">2009-03-19</REC_DATE>
    <REC_TIME Type="92">14:05:00</REC_TIME>
    <PUMP_MODEL Type="12">Animas 2020</PUMP_MODEL>
    <SNDNORMALBOLUS Type="4">4</SNDNORMALBOLUS>
    <SNDAUDIOBOLUS Type="4">4</SNDAUDIOBOLUS>
    <SNDTEMPBASAL Type="4">0</SNDTEMPBASAL>
    <SNDALERT Type="4">4</SNDALERT>
    <SNDREMINDER Type="4">4</SNDREMINDER>
    <SNDWARNING Type="4">4</SNDWARNING>
    <SNDALARMS Type="4">4</SNDALARMS>
    <CLKMODE Type="4">0</CLKMODE>
    <ADVAUDIOBOLENABLED Type="4">1</ADVAUDIOBOLENABLED>
    <ADVAUDIOBOLSTEP Type="8">2.0</ADVAUDIOBOLSTEP>
    <ADVADVANCEDBOLUS Type="4">1</ADVADVANCEDBOLUS>
    <ADVREMINDERS Type="4">1</ADVREMINDERS>
    <ADVDELIVERY Type="4">0</ADVDELIVERY>
    <ADVBASALPROGS Type="4">1</ADVBASALPROGS>
    <ADVMAXBASAL Type="8">5.0</ADVMAXBASAL>
    <ADVMAXBOLUS Type="8">16.0</ADVMAXBOLUS>
    <ADVMAXTDD Type="8">100.0</ADVMAXTDD>
    <ADVDISPLAYTOUT Type="4">60</ADVDISPLAYTOUT>
    <ADVAUTOOFFENABLED Type="4">0</ADVAUTOOFFENABLED>
    <ADVAUTOOFFTIME Type="4">12</ADVAUTOOFFTIME>
    <ADVCARTWARNLVL Type="4">20</ADVCARTWARNLVL>
    <ADVOCCLLIMITS Type="4">0</ADVOCCLLIMITS>
    <ADVIOBENABLED Type="4">0</ADVIOBENABLED>
    <ADVIOBDURATION Type="4">5</ADVIOBDURATION>
    <ADVSKDYBGTARGET Type="4">240</ADVSKDYBGTARGET>
    <ADVSKDYCKKTN Type="4">4</ADVSKDYCKKTN>
    <ADVSKDYCKBG Type="4">2</ADVSKDYCKBG>
    <I2C_0_TIME Type="92">00:00:00</I2C_0_TIME>
    <I2C_1_TIME Type="92">NULL</I2C_1_TIME>
    <I2C_2_TIME Type="92">NULL</I2C_2_TIME>
    <I2C_3_TIME Type="92">NULL</I2C_3_TIME>
    <I2C_4_TIME Type="92">NULL</I2C_4_TIME>
    <I2C_5_TIME Type="92">NULL</I2C_5_TIME>
    <I2C_6_TIME Type="92">NULL</I2C_6_TIME>
    <I2C_7_TIME Type="92">NULL</I2C_7_TIME>
    <I2C_8_TIME Type="92">NULL</I2C_8_TIME>
    <I2C_9_TIME Type="92">NULL</I2C_9_TIME>
    <I2C_10_TIME Type="92">NULL</I2C_10_TIME>
    <I2C_11_TIME Type="92">NULL</I2C_11_TIME>
    <I2C_0_VAL Type="4">15</I2C_0_VAL>
    <I2C_1_VAL Type="4">NULL</I2C_1_VAL>
    <I2C_2_VAL Type="4">NULL</I2C_2_VAL>
    <I2C_3_VAL Type="4">NULL</I2C_3_VAL>
    <I2C_4_VAL Type="4">NULL</I2C_4_VAL>
    <I2C_5_VAL Type="4">NULL</I2C_5_VAL>
    <I2C_6_VAL Type="4">NULL</I2C_6_VAL>
    <I2C_7_VAL Type="4">NULL</I2C_7_VAL>
    <I2C_8_VAL Type="4">NULL</I2C_8_VAL>
    <I2C_9_VAL Type="4">NULL</I2C_9_VAL>
    <I2C_10_VAL Type="4">NULL</I2C_10_VAL>
    <I2C_11_VAL Type="4">NULL</I2C_11_VAL>
    <ISF_0_TIME Type="92">00:00:00</ISF_0_TIME>
    <ISF_1_TIME Type="92">NULL</ISF_1_TIME>
    <ISF_2_TIME Type="92">NULL</ISF_2_TIME>
    <ISF_3_TIME Type="92">NULL</ISF_3_TIME>
    <ISF_4_TIME Type="92">NULL</ISF_4_TIME>
    <ISF_5_TIME Type="92">NULL</ISF_5_TIME>
    <ISF_6_TIME Type="92">NULL</ISF_6_TIME>
    <ISF_7_TIME Type="92">NULL</ISF_7_TIME>
    <ISF_8_TIME Type="92">NULL</ISF_8_TIME>
    <ISF_9_TIME Type="92">NULL</ISF_9_TIME>
    <ISF_10_TIME Type="92">NULL</ISF_10_TIME>
    <ISF_11_TIME Type="92">NULL</ISF_11_TIME>
    <ISF_0_VAL Type="4">50</ISF_0_VAL>
    <ISF_1_VAL Type="4">NULL</ISF_1_VAL>
    <ISF_2_VAL Type="4">NULL</ISF_2_VAL>
    <ISF_3_VAL Type="4">NULL</ISF_3_VAL>
    <ISF_4_VAL Type="4">NULL</ISF_4_VAL>
    <ISF_5_VAL Type="4">NULL</ISF_5_VAL>
    <ISF_6_VAL Type="4">NULL</ISF_6_VAL>
    <ISF_7_VAL Type="4">NULL</ISF_7_VAL>
    <ISF_8_VAL Type="4">NULL</ISF_8_VAL>
    <ISF_9_VAL Type="4">NULL</ISF_9_VAL>
    <ISF_10_VAL Type="4">NULL</ISF_10_VAL>
    <ISF_11_VAL Type="4">NULL</ISF_11_VAL>
    <BGT_0_TIME Type="92">00:00:00</BGT_0_TIME>
    <BGT_1_TIME Type="92">NULL</BGT_1_TIME>
    <BGT_2_TIME Type="92">NULL</BGT_2_TIME>
    <BGT_3_TIME Type="92">NULL</BGT_3_TIME>
    <BGT_4_TIME Type="92">NULL</BGT_4_TIME>
    <BGT_5_TIME Type="92">NULL</BGT_5_TIME>
    <BGT_6_TIME Type="92">NULL</BGT_6_TIME>
    <BGT_7_TIME Type="92">NULL</BGT_7_TIME>
    <BGT_8_TIME Type="92">NULL</BGT_8_TIME>
    <BGT_9_TIME Type="92">NULL</BGT_9_TIME>
    <BGT_10_TIME Type="92">NULL</BGT_10_TIME>
    <BGT_11_TIME Type="92">NULL</BGT_11_TIME>
    <BGT_0_VAL Type="4">120</BGT_0_VAL>
    <BGT_1_VAL Type="4">NULL</BGT_1_VAL>
    <BGT_2_VAL Type="4">NULL</BGT_2_VAL>
    <BGT_3_VAL Type="4">NULL</BGT_3_VAL>
    <BGT_4_VAL Type="4">NULL</BGT_4_VAL>
    <BGT_5_VAL Type="4">NULL</BGT_5_VAL>
    <BGT_6_VAL Type="4">NULL</BGT_6_VAL>
    <BGT_7_VAL Type="4">NULL</BGT_7_VAL>
    <BGT_8_VAL Type="4">NULL</BGT_8_VAL>
    <BGT_9_VAL Type="4">NULL</BGT_9_VAL>
    <BGT_10_VAL Type="4">NULL</BGT_10_VAL>
    <BGT_11_VAL Type="4">NULL</BGT_11_VAL>
    <BGT_0_DELT Type="4">10</BGT_0_DELT>
    <BGT_1_DELT Type="4">NULL</BGT_1_DELT>
    <BGT_2_DELT Type="4">NULL</BGT_2_DELT>
    <BGT_3_DELT Type="4">NULL</BGT_3_DELT>
    <BGT_4_DELT Type="4">NULL</BGT_4_DELT>
    <BGT_5_DELT Type="4">NULL</BGT_5_DELT>
    <BGT_6_DELT Type="4">NULL</BGT_6_DELT>
    <BGT_7_DELT Type="4">NULL</BGT_7_DELT>
    <BGT_8_DELT Type="4">NULL</BGT_8_DELT>
    <BGT_9_DELT Type="4">NULL</BGT_9_DELT>
    <BGT_10_DELT Type="4">NULL</BGT_10_DELT>
    <BGT_11_DELT Type="4">NULL</BGT_11_DELT>
    <BAS_0_0_TIME Type="92">00:00:00</BAS_0_0_TIME>
    <BAS_0_1_TIME Type="92">NULL</BAS_0_1_TIME>
    <BAS_0_2_TIME Type="92">NULL</BAS_0_2_TIME>
    <BAS_0_3_TIME Type="92">NULL</BAS_0_3_TIME>
    <BAS_0_4_TIME Type="92">NULL</BAS_0_4_TIME>
    <BAS_0_5_TIME Type="92">NULL</BAS_0_5_TIME>
    <BAS_0_6_TIME Type="92">NULL</BAS_0_6_TIME>
    <BAS_0_7_TIME Type="92">NULL</BAS_0_7_TIME>
    <BAS_0_8_TIME Type="92">NULL</BAS_0_8_TIME>
    <BAS_0_9_TIME Type="92">NULL</BAS_0_9_TIME>
    <BAS_0_10_TIME Type="92">NULL</BAS_0_10_TIME>
    <BAS_0_11_TIME Type="92">NULL</BAS_0_11_TIME>
    <BAS_0_0_VAL Type="8">0.025</BAS_0_0_VAL>
    <BAS_0_1_VAL Type="8">NULL</BAS_0_1_VAL>
    <BAS_0_2_VAL Type="8">NULL</BAS_0_2_VAL>
    <BAS_0_3_VAL Type="8">NULL</BAS_0_3_VAL>
    <BAS_0_4_VAL Type="8">NULL</BAS_0_4_VAL>
    <BAS_0_5_VAL Type="8">NULL</BAS_0_5_VAL>
    <BAS_0_6_VAL Type="8">NULL</BAS_0_6_VAL>
    <BAS_0_7_VAL Type="8">NULL</BAS_0_7_VAL>
    <BAS_0_8_VAL Type="8">NULL</BAS_0_8_VAL>
    <BAS_0_9_VAL Type="8">NULL</BAS_0_9_VAL>
    <BAS_0_10_VAL Type="8">NULL</BAS_0_10_VAL>
    <BAS_0_11_VAL Type="8">NULL</BAS_0_11_VAL>
    <BAS_1_0_TIME Type="92">00:00:00</BAS_1_0_TIME>
    <BAS_1_1_TIME Type="92">NULL</BAS_1_1_TIME>
    <BAS_1_2_TIME Type="92">NULL</BAS_1_2_TIME>
    <BAS_1_3_TIME Type="92">NULL</BAS_1_3_TIME>
    <BAS_1_4_TIME Type="92">NULL</BAS_1_4_TIME>
    <BAS_1_5_TIME Type="92">NULL</BAS_1_5_TIME>
    <BAS_1_6_TIME Type="92">NULL</BAS_1_6_TIME>
    <BAS_1_7_TIME Type="92">NULL</BAS_1_7_TIME>
    <BAS_1_8_TIME Type="92">NULL</BAS_1_8_TIME>
    <BAS_1_9_TIME Type="92">NULL</BAS_1_9_TIME>
    <BAS_1_10_TIME Type="92">NULL</BAS_1_10_TIME>
    <BAS_1_11_TIME Type="92">NULL</BAS_1_11_TIME>
    <BAS_1_0_VAL Type="8">0.0</BAS_1_0_VAL>
    <BAS_1_1_VAL Type="8">NULL</BAS_1_1_VAL>
    <BAS_1_2_VAL Type="8">NULL</BAS_1_2_VAL>
    <BAS_1_3_VAL Type="8">NULL</BAS_1_3_VAL>
    <BAS_1_4_VAL Type="8">NULL</BAS_1_4_VAL>
    <BAS_1_5_VAL Type="8">NULL</BAS_1_5_VAL>
    <BAS_1_6_VAL Type="8">NULL</BAS_1_6_VAL>
    <BAS_1_7_VAL Type="8">NULL</BAS_1_7_VAL>
    <BAS_1_8_VAL Type="8">NULL</BAS_1_8_VAL>
    <BAS_1_9_VAL Type="8">NULL</BAS_1_9_VAL>
    <BAS_1_10_VAL Type="8">NULL</BAS_1_10_VAL>
    <BAS_1_11_VAL Type="8">NULL</BAS_1_11_VAL>
    <BAS_2_0_TIME Type="92">00:00:00</BAS_2_0_TIME>
    <BAS_2_1_TIME Type="92">NULL</BAS_2_1_TIME>
    <BAS_2_2_TIME Type="92">NULL</BAS_2_2_TIME>
    <BAS_2_3_TIME Type="92">NULL</BAS_2_3_TIME>
    <BAS_2_4_TIME Type="92">NULL</BAS_2_4_TIME>
    <BAS_2_5_TIME Type="92">NULL</BAS_2_5_TIME>
    <BAS_2_6_TIME Type="92">NULL</BAS_2_6_TIME>
    <BAS_2_7_TIME Type="92">NULL</BAS_2_7_TIME>
    <BAS_2_8_TIME Type="92">NULL</BAS_2_8_TIME>
    <BAS_2_9_TIME Type="92">NULL</BAS_2_9_TIME>
    <BAS_2_10_TIME Type="92">NULL</BAS_2_10_TIME>
    <BAS_2_11_TIME Type="92">NULL</BAS_2_11_TIME>
    <BAS_2_0_VAL Type="8">0.0</BAS_2_0_VAL>
    <BAS_2_1_VAL Type="8">NULL</BAS_2_1_VAL>
    <BAS_2_2_VAL Type="8">NULL</BAS_2_2_VAL>
    <BAS_2_3_VAL Type="8">NULL</BAS_2_3_VAL>
    <BAS_2_4_VAL Type="8">NULL</BAS_2_4_VAL>
    <BAS_2_5_VAL Type="8">NULL</BAS_2_5_VAL>
    <BAS_2_6_VAL Type="8">NULL</BAS_2_6_VAL>
    <BAS_2_7_VAL Type="8">NULL</BAS_2_7_VAL>
    <BAS_2_8_VAL Type="8">NULL</BAS_2_8_VAL>
    <BAS_2_9_VAL Type="8">NULL</BAS_2_9_VAL>
    <BAS_2_10_VAL Type="8">NULL</BAS_2_10_VAL>
    <BAS_2_11_VAL Type="8">NULL</BAS_2_11_VAL>
    <BAS_3_0_TIME Type="92">00:00:00</BAS_3_0_TIME>
    <BAS_3_1_TIME Type="92">NULL</BAS_3_1_TIME>
    <BAS_3_2_TIME Type="92">NULL</BAS_3_2_TIME>
    <BAS_3_3_TIME Type="92">NULL</BAS_3_3_TIME>
    <BAS_3_4_TIME Type="92">NULL</BAS_3_4_TIME>
    <BAS_3_5_TIME Type="92">NULL</BAS_3_5_TIME>
    <BAS_3_6_TIME Type="92">NULL</BAS_3_6_TIME>
    <BAS_3_7_TIME Type="92">NULL</BAS_3_7_TIME>
    <BAS_3_8_TIME Type="92">NULL</BAS_3_8_TIME>
    <BAS_3_9_TIME Type="92">NULL</BAS_3_9_TIME>
    <BAS_3_10_TIME Type="92">NULL</BAS_3_10_TIME>
    <BAS_3_11_TIME Type="92">NULL</BAS_3_11_TIME>
    <BAS_3_0_VAL Type="8">0.0</BAS_3_0_VAL>
    <BAS_3_1_VAL Type="8">NULL</BAS_3_1_VAL>
    <BAS_3_2_VAL Type="8">NULL</BAS_3_2_VAL>
    <BAS_3_3_VAL Type="8">NULL</BAS_3_3_VAL>
    <BAS_3_4_VAL Type="8">NULL</BAS_3_4_VAL>
    <BAS_3_5_VAL Type="8">NULL</BAS_3_5_VAL>
    <BAS_3_6_VAL Type="8">NULL</BAS_3_6_VAL>
    <BAS_3_7_VAL Type="8">NULL</BAS_3_7_VAL>
    <BAS_3_8_VAL Type="8">NULL</BAS_3_8_VAL>
    <BAS_3_9_VAL Type="8">NULL</BAS_3_9_VAL>
    <BAS_3_10_VAL Type="8">NULL</BAS_3_10_VAL>
    <BAS_3_11_VAL Type="8">NULL</BAS_3_11_VAL>
    <BAS_0_NAME Type="12">1-weekday</BAS_0_NAME>
    <BAS_1_NAME Type="12">2-other</BAS_1_NAME>
    <BAS_2_NAME Type="12">3-weekend</BAS_2_NAME>
    <BAS_3_NAME Type="12">4-exercise</BAS_3_NAME>
    <PUMP_SERIAL_NO Type="12">27-04663-12</PUMP_SERIAL_NO>
    <ADVFRIENDLYNAME Type="12">NULL</ADVFRIENDLYNAME>
    <SNDREMOTEBOLUS Type="5">0</SNDREMOTEBOLUS>
    <ADVMAX2HR Type="5">0</ADVMAX2HR>
  </PUMPSETTINGS>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=128-6d6d, Message=REPLACE BATTERY</ALARM>
    <PUMPALARM Type="4">128</PUMPALARM>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">21:00:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=128-7265, Message=REPLACE BATTERY</ALARM>
    <PUMPALARM Type="4">128</PUMPALARM>
    <REC_DATE Type="91">2007-12-27</REC_DATE>
    <REC_TIME Type="92">04:15:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=177-7283, Message=LOW BATTERY</ALARM>
    <PUMPALARM Type="4">177</PUMPALARM>
    <REC_DATE Type="91">2007-12-24</REC_DATE>
    <REC_TIME Type="92">23:42:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=177-8699, Message=LOW BATTERY</ALARM>
    <PUMPALARM Type="4">177</PUMPALARM>
    <REC_DATE Type="91">2007-10-25</REC_DATE>
    <REC_TIME Type="92">09:55:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=144-5664, Message=EMPTY CARTRIDGE</ALARM>
    <PUMPALARM Type="4">144</PUMPALARM>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">00:18:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=178-566c, Message=LOW CARTRIDGE</ALARM>
    <PUMPALARM Type="4">178</PUMPALARM>
    <REC_DATE Type="91">2007-09-23</REC_DATE>
    <REC_TIME Type="92">01:12:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=128-6767, Message=REPLACE BATTERY</ALARM>
    <PUMPALARM Type="4">128</PUMPALARM>
    <REC_DATE Type="91">2007-09-14</REC_DATE>
    <REC_TIME Type="92">08:56:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=128-866e, Message=REPLACE BATTERY</ALARM>
    <PUMPALARM Type="4">128</PUMPALARM>
    <REC_DATE Type="91">2007-08-08</REC_DATE>
    <REC_TIME Type="92">10:28:00</REC_TIME>
  </ALARMLOG>
  <ALARMLOG>
    <ALARM Type="12">Error: , Code=177-8697, Message=LOW BATTERY</ALARM>
    <PUMPALARM Type="4">177</PUMPALARM>
    <REC_DATE Type="91">2007-08-06</REC_DATE>
    <REC_TIME Type="92">23:46:00</REC_TIME>
  </ALARMLOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-05-21</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <PRIME_AMT Type="4">10</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-05-21</REC_DATE>
    <REC_TIME Type="92">17:35:00</REC_TIME>
    <PRIME_AMT Type="4">398</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-06-13</REC_DATE>
    <REC_TIME Type="92">15:35:00</REC_TIME>
    <PRIME_AMT Type="4">5</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-06-13</REC_DATE>
    <REC_TIME Type="92">15:35:00</REC_TIME>
    <PRIME_AMT Type="4">2405</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">16:15:00</REC_TIME>
    <PRIME_AMT Type="4">10</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">16:15:00</REC_TIME>
    <PRIME_AMT Type="4">580</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">15:19:00</REC_TIME>
    <PRIME_AMT Type="4">15</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">15:19:00</REC_TIME>
    <PRIME_AMT Type="4">1904</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-14</REC_DATE>
    <REC_TIME Type="92">09:05:00</REC_TIME>
    <PRIME_AMT Type="4">15</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-14</REC_DATE>
    <REC_TIME Type="92">09:05:00</REC_TIME>
    <PRIME_AMT Type="4">568</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">12:43:00</REC_TIME>
    <PRIME_AMT Type="4">30</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">12:43:00</REC_TIME>
    <PRIME_AMT Type="4">677</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-19</REC_DATE>
    <REC_TIME Type="92">12:42:00</REC_TIME>
    <PRIME_AMT Type="4">1033</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">09:01:00</REC_TIME>
    <PRIME_AMT Type="4">766</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">04:16:00</REC_TIME>
    <PRIME_AMT Type="4">10</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-09-24</REC_DATE>
    <REC_TIME Type="92">04:16:00</REC_TIME>
    <PRIME_AMT Type="4">585</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-10-26</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <PRIME_AMT Type="4">5</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2007-10-26</REC_DATE>
    <REC_TIME Type="92">08:44:00</REC_TIME>
    <PRIME_AMT Type="4">536</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">01:00:00</REC_TIME>
    <RSM_DATE Type="91">1999-12-31</RSM_DATE>
    <RSM_TIME Type="92">00:00:00</RSM_TIME>
  </SUSPENDLOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">01:03:00</REC_TIME>
    <RSM_DATE Type="91">1999-12-31</RSM_DATE>
    <RSM_TIME Type="92">00:00:00</RSM_TIME>
  </SUSPENDLOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">01:06:00</REC_TIME>
    <RSM_DATE Type="91">1999-12-31</RSM_DATE>
    <RSM_TIME Type="92">00:00:00</RSM_TIME>
  </SUSPENDLOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">01:04:00</REC_TIME>
    <RSM_DATE Type="91">2004-01-01</RSM_DATE>
    <RSM_TIME Type="92">01:05:00</RSM_TIME>
  </SUSPENDLOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2004-01-01</REC_DATE>
    <REC_TIME Type="92">01:01:00</REC_TIME>
    <RSM_DATE Type="91">1999-12-31</RSM_DATE>
    <RSM_TIME Type="92">00:00:00</RSM_TIME>
  </SUSPENDLOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2007-08-01</REC_DATE>
    <REC_TIME Type="92">15:41:00</REC_TIME>
    <RSM_DATE Type="91">2007-08-01</RSM_DATE>
    <RSM_TIME Type="92">16:13:00</RSM_TIME>
  </SUSPENDLOG>
</ezManagerMax_Data>
"""
    # Data from ticket #918
    data4 = """!TAG!SERNO!DVU345678901!<?xml version="1.0" encoding="UTF-8"?>
<ezManagerMax_Data>
  <BGLOG>
    <BG Type="4">223</BG>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">09:07:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">1</SOURCE>
  </BGLOG>
  <BGLOG>
    <BG Type="4">225</BG>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">00:18:00</REC_TIME>
    <IGNORE Type="5">0</IGNORE>
    <SOURCE Type="5">1</SOURCE>
  </BGLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">1</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">350</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
  </FOODLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">1</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">700</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">12:22:00</REC_TIME>
  </FOODLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">2</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">300</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <REC_TIME Type="92">12:02:00</REC_TIME>
  </FOODLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">1</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">300</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
  </FOODLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">2</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">350</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">05:44:00</REC_TIME>
  </FOODLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">1</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">400</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
  </FOODLOG>
  <FOODLOG>
    <MEALID Type="4">9</MEALID>
    <ENTERNUM Type="4">1</ENTERNUM>
    <NAME Type="12">Download from pump</NAME>
    <SERVING Type="12">1 serving</SERVING>
    <MULTIPLIER Type="4">10</MULTIPLIER>
    <CARBS Type="4">400</CARBS>
    <FIBER Type="4">0</FIBER>
    <CALORIES Type="4">0</CALORIES>
    <PROTEIN Type="4">0</PROTEIN>
    <FAT Type="4">0</FAT>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">09:07:00</REC_TIME>
  </FOODLOG>
  <BOLUSLOG>
    <BOLUS Type="4">400</BOLUS>
    <BOLUS_TYPE Type="4">3</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">09:09:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">80</BOLUS>
    <BOLUS_TYPE Type="4">5</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">09:07:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">600</BOLUS>
    <BOLUS_TYPE Type="4">3</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">120</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">17:44:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">270</BOLUS>
    <BOLUS_TYPE Type="4">6</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">09:10:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">190</BOLUS>
    <BOLUS_TYPE Type="4">6</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">05:44:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">170</BOLUS>
    <BOLUS_TYPE Type="4">4</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">0</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">00:18:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">490</BOLUS>
    <BOLUS_TYPE Type="4">6</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">120</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <REC_TIME Type="92">19:37:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">160</BOLUS>
    <BOLUS_TYPE Type="4">6</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <REC_TIME Type="92">12:02:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">540</BOLUS>
    <BOLUS_TYPE Type="4">6</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">12:22:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">550</BOLUS>
    <BOLUS_TYPE Type="4">3</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">21:32:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BOLUSLOG>
    <BOLUS Type="4">260</BOLUS>
    <BOLUS_TYPE Type="4">6</BOLUS_TYPE>
    <BOLUS_DURATION Type="4">30</BOLUS_DURATION>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">06:29:00</REC_TIME>
    <SOURCE Type="5">1</SOURCE>
  </BOLUSLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">20:34:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">1300</BASAL>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">700</BASAL>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">1300</BASAL>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">700</BASAL>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">1300</BASAL>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">700</BASAL>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">1300</BASAL>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">700</BASAL>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">16:37:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">16:34:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">700</BASAL>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">1300</BASAL>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">17:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">700</BASAL>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">08:01:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">1300</BASAL>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">01:58:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <BASALLOG>
    <BASAL Type="4">0</BASAL>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">01:52:00</REC_TIME>
    <FLAG Type="4">0</FLAG>
  </BASALLOG>
  <TDDLOG>
    <BASALT Type="4">25670</BASALT>
    <BOLUST Type="4">8100</BOLUST>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">25765</BASALT>
    <BOLUST Type="4">5400</BOLUST>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">25800</BASALT>
    <BOLUST Type="4">6498</BOLUST>
    <REC_DATE Type="91">2009-11-29</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">25800</BASALT>
    <BOLUST Type="4">12300</BOLUST>
    <REC_DATE Type="91">2009-11-30</REC_DATE>
    <FLAG Type="4">0</FLAG>
  </TDDLOG>
  <TDDLOG>
    <BASALT Type="4">21315</BASALT>
    <BOLUST Type="4">4799</BOLUST>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <FLAG Type="4">2</FLAG>
  </TDDLOG>
  <PUMPSETTINGS>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">20:38:00</REC_TIME>
    <PUMP_MODEL Type="12">Animas 2020 (intl)</PUMP_MODEL>
    <SNDNORMALBOLUS Type="4">4</SNDNORMALBOLUS>
    <SNDAUDIOBOLUS Type="4">4</SNDAUDIOBOLUS>
    <SNDTEMPBASAL Type="4">4</SNDTEMPBASAL>
    <SNDALERT Type="4">4</SNDALERT>
    <SNDREMINDER Type="4">4</SNDREMINDER>
    <SNDWARNING Type="4">4</SNDWARNING>
    <SNDALARMS Type="4">4</SNDALARMS>
    <CLKMODE Type="4">1</CLKMODE>
    <ADVAUDIOBOLENABLED Type="4">0</ADVAUDIOBOLENABLED>
    <ADVAUDIOBOLSTEP Type="8">2.0</ADVAUDIOBOLSTEP>
    <ADVADVANCEDBOLUS Type="4">1</ADVADVANCEDBOLUS>
    <ADVREMINDERS Type="4">1</ADVREMINDERS>
    <ADVDELIVERY Type="4">1</ADVDELIVERY>
    <ADVBASALPROGS Type="4">4</ADVBASALPROGS>
    <ADVMAXBASAL Type="8">5.0</ADVMAXBASAL>
    <ADVMAXBOLUS Type="8">16.0</ADVMAXBOLUS>
    <ADVMAXTDD Type="8">100.0</ADVMAXTDD>
    <ADVDISPLAYTOUT Type="4">60</ADVDISPLAYTOUT>
    <ADVAUTOOFFENABLED Type="4">0</ADVAUTOOFFENABLED>
    <ADVAUTOOFFTIME Type="4">12</ADVAUTOOFFTIME>
    <ADVCARTWARNLVL Type="4">20</ADVCARTWARNLVL>
    <ADVOCCLLIMITS Type="4">1</ADVOCCLLIMITS>
    <ADVIOBENABLED Type="4">1</ADVIOBENABLED>
    <ADVIOBDURATION Type="4">5</ADVIOBDURATION>
    <ADVSKDYBGTARGET Type="4">239</ADVSKDYBGTARGET>
    <ADVSKDYCKKTN Type="4">4</ADVSKDYCKKTN>
    <ADVSKDYCKBG Type="4">2</ADVSKDYCKBG>
    <I2C_0_TIME Type="92">00:00:00</I2C_0_TIME>
    <I2C_1_TIME Type="92">NULL</I2C_1_TIME>
    <I2C_2_TIME Type="92">NULL</I2C_2_TIME>
    <I2C_3_TIME Type="92">NULL</I2C_3_TIME>
    <I2C_4_TIME Type="92">NULL</I2C_4_TIME>
    <I2C_5_TIME Type="92">NULL</I2C_5_TIME>
    <I2C_6_TIME Type="92">NULL</I2C_6_TIME>
    <I2C_7_TIME Type="92">NULL</I2C_7_TIME>
    <I2C_8_TIME Type="92">NULL</I2C_8_TIME>
    <I2C_9_TIME Type="92">NULL</I2C_9_TIME>
    <I2C_10_TIME Type="92">NULL</I2C_10_TIME>
    <I2C_11_TIME Type="92">NULL</I2C_11_TIME>
    <I2C_0_VAL Type="4">15</I2C_0_VAL>
    <I2C_1_VAL Type="4">NULL</I2C_1_VAL>
    <I2C_2_VAL Type="4">NULL</I2C_2_VAL>
    <I2C_3_VAL Type="4">NULL</I2C_3_VAL>
    <I2C_4_VAL Type="4">NULL</I2C_4_VAL>
    <I2C_5_VAL Type="4">NULL</I2C_5_VAL>
    <I2C_6_VAL Type="4">NULL</I2C_6_VAL>
    <I2C_7_VAL Type="4">NULL</I2C_7_VAL>
    <I2C_8_VAL Type="4">NULL</I2C_8_VAL>
    <I2C_9_VAL Type="4">NULL</I2C_9_VAL>
    <I2C_10_VAL Type="4">NULL</I2C_10_VAL>
    <I2C_11_VAL Type="4">NULL</I2C_11_VAL>
    <ISF_0_TIME Type="92">00:00:00</ISF_0_TIME>
    <ISF_1_TIME Type="92">NULL</ISF_1_TIME>
    <ISF_2_TIME Type="92">NULL</ISF_2_TIME>
    <ISF_3_TIME Type="92">NULL</ISF_3_TIME>
    <ISF_4_TIME Type="92">NULL</ISF_4_TIME>
    <ISF_5_TIME Type="92">NULL</ISF_5_TIME>
    <ISF_6_TIME Type="92">NULL</ISF_6_TIME>
    <ISF_7_TIME Type="92">NULL</ISF_7_TIME>
    <ISF_8_TIME Type="92">NULL</ISF_8_TIME>
    <ISF_9_TIME Type="92">NULL</ISF_9_TIME>
    <ISF_10_TIME Type="92">NULL</ISF_10_TIME>
    <ISF_11_TIME Type="92">NULL</ISF_11_TIME>
    <ISF_0_VAL Type="4">54</ISF_0_VAL>
    <ISF_1_VAL Type="4">NULL</ISF_1_VAL>
    <ISF_2_VAL Type="4">NULL</ISF_2_VAL>
    <ISF_3_VAL Type="4">NULL</ISF_3_VAL>
    <ISF_4_VAL Type="4">NULL</ISF_4_VAL>
    <ISF_5_VAL Type="4">NULL</ISF_5_VAL>
    <ISF_6_VAL Type="4">NULL</ISF_6_VAL>
    <ISF_7_VAL Type="4">NULL</ISF_7_VAL>
    <ISF_8_VAL Type="4">NULL</ISF_8_VAL>
    <ISF_9_VAL Type="4">NULL</ISF_9_VAL>
    <ISF_10_VAL Type="4">NULL</ISF_10_VAL>
    <ISF_11_VAL Type="4">NULL</ISF_11_VAL>
    <BGT_0_TIME Type="92">00:00:00</BGT_0_TIME>
    <BGT_1_TIME Type="92">NULL</BGT_1_TIME>
    <BGT_2_TIME Type="92">NULL</BGT_2_TIME>
    <BGT_3_TIME Type="92">NULL</BGT_3_TIME>
    <BGT_4_TIME Type="92">NULL</BGT_4_TIME>
    <BGT_5_TIME Type="92">NULL</BGT_5_TIME>
    <BGT_6_TIME Type="92">NULL</BGT_6_TIME>
    <BGT_7_TIME Type="92">NULL</BGT_7_TIME>
    <BGT_8_TIME Type="92">NULL</BGT_8_TIME>
    <BGT_9_TIME Type="92">NULL</BGT_9_TIME>
    <BGT_10_TIME Type="92">NULL</BGT_10_TIME>
    <BGT_11_TIME Type="92">NULL</BGT_11_TIME>
    <BGT_0_VAL Type="4">108</BGT_0_VAL>
    <BGT_1_VAL Type="4">NULL</BGT_1_VAL>
    <BGT_2_VAL Type="4">NULL</BGT_2_VAL>
    <BGT_3_VAL Type="4">NULL</BGT_3_VAL>
    <BGT_4_VAL Type="4">NULL</BGT_4_VAL>
    <BGT_5_VAL Type="4">NULL</BGT_5_VAL>
    <BGT_6_VAL Type="4">NULL</BGT_6_VAL>
    <BGT_7_VAL Type="4">NULL</BGT_7_VAL>
    <BGT_8_VAL Type="4">NULL</BGT_8_VAL>
    <BGT_9_VAL Type="4">NULL</BGT_9_VAL>
    <BGT_10_VAL Type="4">NULL</BGT_10_VAL>
    <BGT_11_VAL Type="4">NULL</BGT_11_VAL>
    <BGT_0_DELT Type="4">11</BGT_0_DELT>
    <BGT_1_DELT Type="4">NULL</BGT_1_DELT>
    <BGT_2_DELT Type="4">NULL</BGT_2_DELT>
    <BGT_3_DELT Type="4">NULL</BGT_3_DELT>
    <BGT_4_DELT Type="4">NULL</BGT_4_DELT>
    <BGT_5_DELT Type="4">NULL</BGT_5_DELT>
    <BGT_6_DELT Type="4">NULL</BGT_6_DELT>
    <BGT_7_DELT Type="4">NULL</BGT_7_DELT>
    <BGT_8_DELT Type="4">NULL</BGT_8_DELT>
    <BGT_9_DELT Type="4">NULL</BGT_9_DELT>
    <BGT_10_DELT Type="4">NULL</BGT_10_DELT>
    <BGT_11_DELT Type="4">NULL</BGT_11_DELT>
    <BAS_0_0_TIME Type="92">00:00:00</BAS_0_0_TIME>
    <BAS_0_1_TIME Type="92">08:00:00</BAS_0_1_TIME>
    <BAS_0_2_TIME Type="92">17:00:00</BAS_0_2_TIME>
    <BAS_0_3_TIME Type="92">NULL</BAS_0_3_TIME>
    <BAS_0_4_TIME Type="92">NULL</BAS_0_4_TIME>
    <BAS_0_5_TIME Type="92">NULL</BAS_0_5_TIME>
    <BAS_0_6_TIME Type="92">NULL</BAS_0_6_TIME>
    <BAS_0_7_TIME Type="92">NULL</BAS_0_7_TIME>
    <BAS_0_8_TIME Type="92">NULL</BAS_0_8_TIME>
    <BAS_0_9_TIME Type="92">NULL</BAS_0_9_TIME>
    <BAS_0_10_TIME Type="92">NULL</BAS_0_10_TIME>
    <BAS_0_11_TIME Type="92">NULL</BAS_0_11_TIME>
    <BAS_0_0_VAL Type="8">1.3</BAS_0_0_VAL>
    <BAS_0_1_VAL Type="8">0.7</BAS_0_1_VAL>
    <BAS_0_2_VAL Type="8">1.3</BAS_0_2_VAL>
    <BAS_0_3_VAL Type="8">NULL</BAS_0_3_VAL>
    <BAS_0_4_VAL Type="8">NULL</BAS_0_4_VAL>
    <BAS_0_5_VAL Type="8">NULL</BAS_0_5_VAL>
    <BAS_0_6_VAL Type="8">NULL</BAS_0_6_VAL>
    <BAS_0_7_VAL Type="8">NULL</BAS_0_7_VAL>
    <BAS_0_8_VAL Type="8">NULL</BAS_0_8_VAL>
    <BAS_0_9_VAL Type="8">NULL</BAS_0_9_VAL>
    <BAS_0_10_VAL Type="8">NULL</BAS_0_10_VAL>
    <BAS_0_11_VAL Type="8">NULL</BAS_0_11_VAL>
    <BAS_1_0_TIME Type="92">00:00:00</BAS_1_0_TIME>
    <BAS_1_1_TIME Type="92">NULL</BAS_1_1_TIME>
    <BAS_1_2_TIME Type="92">NULL</BAS_1_2_TIME>
    <BAS_1_3_TIME Type="92">NULL</BAS_1_3_TIME>
    <BAS_1_4_TIME Type="92">NULL</BAS_1_4_TIME>
    <BAS_1_5_TIME Type="92">NULL</BAS_1_5_TIME>
    <BAS_1_6_TIME Type="92">NULL</BAS_1_6_TIME>
    <BAS_1_7_TIME Type="92">NULL</BAS_1_7_TIME>
    <BAS_1_8_TIME Type="92">NULL</BAS_1_8_TIME>
    <BAS_1_9_TIME Type="92">NULL</BAS_1_9_TIME>
    <BAS_1_10_TIME Type="92">NULL</BAS_1_10_TIME>
    <BAS_1_11_TIME Type="92">NULL</BAS_1_11_TIME>
    <BAS_1_0_VAL Type="8">0.0</BAS_1_0_VAL>
    <BAS_1_1_VAL Type="8">NULL</BAS_1_1_VAL>
    <BAS_1_2_VAL Type="8">NULL</BAS_1_2_VAL>
    <BAS_1_3_VAL Type="8">NULL</BAS_1_3_VAL>
    <BAS_1_4_VAL Type="8">NULL</BAS_1_4_VAL>
    <BAS_1_5_VAL Type="8">NULL</BAS_1_5_VAL>
    <BAS_1_6_VAL Type="8">NULL</BAS_1_6_VAL>
    <BAS_1_7_VAL Type="8">NULL</BAS_1_7_VAL>
    <BAS_1_8_VAL Type="8">NULL</BAS_1_8_VAL>
    <BAS_1_9_VAL Type="8">NULL</BAS_1_9_VAL>
    <BAS_1_10_VAL Type="8">NULL</BAS_1_10_VAL>
    <BAS_1_11_VAL Type="8">NULL</BAS_1_11_VAL>
    <BAS_2_0_TIME Type="92">00:00:00</BAS_2_0_TIME>
    <BAS_2_1_TIME Type="92">NULL</BAS_2_1_TIME>
    <BAS_2_2_TIME Type="92">NULL</BAS_2_2_TIME>
    <BAS_2_3_TIME Type="92">NULL</BAS_2_3_TIME>
    <BAS_2_4_TIME Type="92">NULL</BAS_2_4_TIME>
    <BAS_2_5_TIME Type="92">NULL</BAS_2_5_TIME>
    <BAS_2_6_TIME Type="92">NULL</BAS_2_6_TIME>
    <BAS_2_7_TIME Type="92">NULL</BAS_2_7_TIME>
    <BAS_2_8_TIME Type="92">NULL</BAS_2_8_TIME>
    <BAS_2_9_TIME Type="92">NULL</BAS_2_9_TIME>
    <BAS_2_10_TIME Type="92">NULL</BAS_2_10_TIME>
    <BAS_2_11_TIME Type="92">NULL</BAS_2_11_TIME>
    <BAS_2_0_VAL Type="8">0.0</BAS_2_0_VAL>
    <BAS_2_1_VAL Type="8">NULL</BAS_2_1_VAL>
    <BAS_2_2_VAL Type="8">NULL</BAS_2_2_VAL>
    <BAS_2_3_VAL Type="8">NULL</BAS_2_3_VAL>
    <BAS_2_4_VAL Type="8">NULL</BAS_2_4_VAL>
    <BAS_2_5_VAL Type="8">NULL</BAS_2_5_VAL>
    <BAS_2_6_VAL Type="8">NULL</BAS_2_6_VAL>
    <BAS_2_7_VAL Type="8">NULL</BAS_2_7_VAL>
    <BAS_2_8_VAL Type="8">NULL</BAS_2_8_VAL>
    <BAS_2_9_VAL Type="8">NULL</BAS_2_9_VAL>
    <BAS_2_10_VAL Type="8">NULL</BAS_2_10_VAL>
    <BAS_2_11_VAL Type="8">NULL</BAS_2_11_VAL>
    <BAS_3_0_TIME Type="92">00:00:00</BAS_3_0_TIME>
    <BAS_3_1_TIME Type="92">NULL</BAS_3_1_TIME>
    <BAS_3_2_TIME Type="92">NULL</BAS_3_2_TIME>
    <BAS_3_3_TIME Type="92">NULL</BAS_3_3_TIME>
    <BAS_3_4_TIME Type="92">NULL</BAS_3_4_TIME>
    <BAS_3_5_TIME Type="92">NULL</BAS_3_5_TIME>
    <BAS_3_6_TIME Type="92">NULL</BAS_3_6_TIME>
    <BAS_3_7_TIME Type="92">NULL</BAS_3_7_TIME>
    <BAS_3_8_TIME Type="92">NULL</BAS_3_8_TIME>
    <BAS_3_9_TIME Type="92">NULL</BAS_3_9_TIME>
    <BAS_3_10_TIME Type="92">NULL</BAS_3_10_TIME>
    <BAS_3_11_TIME Type="92">NULL</BAS_3_11_TIME>
    <BAS_3_0_VAL Type="8">0.0</BAS_3_0_VAL>
    <BAS_3_1_VAL Type="8">NULL</BAS_3_1_VAL>
    <BAS_3_2_VAL Type="8">NULL</BAS_3_2_VAL>
    <BAS_3_3_VAL Type="8">NULL</BAS_3_3_VAL>
    <BAS_3_4_VAL Type="8">NULL</BAS_3_4_VAL>
    <BAS_3_5_VAL Type="8">NULL</BAS_3_5_VAL>
    <BAS_3_6_VAL Type="8">NULL</BAS_3_6_VAL>
    <BAS_3_7_VAL Type="8">NULL</BAS_3_7_VAL>
    <BAS_3_8_VAL Type="8">NULL</BAS_3_8_VAL>
    <BAS_3_9_VAL Type="8">NULL</BAS_3_9_VAL>
    <BAS_3_10_VAL Type="8">NULL</BAS_3_10_VAL>
    <BAS_3_11_VAL Type="8">NULL</BAS_3_11_VAL>
    <BAS_0_NAME Type="12">1-weekday</BAS_0_NAME>
    <BAS_1_NAME Type="12">2-other</BAS_1_NAME>
    <BAS_2_NAME Type="12">3-weekend</BAS_2_NAME>
    <BAS_3_NAME Type="12">4-exercise</BAS_3_NAME>
    <PUMP_SERIAL_NO Type="12">49-40867-12</PUMP_SERIAL_NO>
    <ADVFRIENDLYNAME Type="12">NULL</ADVFRIENDLYNAME>
    <SNDREMOTEBOLUS Type="5">0</SNDREMOTEBOLUS>
    <ADVMAX2HR Type="5">0</ADVMAX2HR>
  </PUMPSETTINGS>
  <PRIMELOG>
    <REC_DATE Type="91">2009-11-27</REC_DATE>
    <REC_TIME Type="92">01:52:00</REC_TIME>
    <PRIME_AMT Type="4">1169</PRIME_AMT>
    <FLAGS Type="4">2</FLAGS>
  </PRIMELOG>
  <PRIMELOG>
    <REC_DATE Type="91">2009-11-28</REC_DATE>
    <REC_TIME Type="92">16:32:00</REC_TIME>
    <PRIME_AMT Type="4">70</PRIME_AMT>
    <FLAGS Type="4">3</FLAGS>
  </PRIMELOG>
  <SUSPENDLOG>
    <REC_DATE Type="91">2009-12-01</REC_DATE>
    <REC_TIME Type="92">20:31:00</REC_TIME>
    <RSM_DATE Type="91">1999-12-31</RSM_DATE>
    <RSM_TIME Type="92">00:00:00</RSM_TIME>
  </SUSPENDLOG>
</ezManagerMax_Data>
"""

    #print AnalyseAnimasEzManagerMax(data1) 
    #print AnalyseAnimasEzManagerMax(data2) 
    print AnalyseAnimasEzManagerMax(data4) 
