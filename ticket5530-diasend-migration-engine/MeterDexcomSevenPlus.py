# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Dexcom SEVEN PLUS

import sys

import crcmod

from Generic import *
from Defines import *
import Common
import Buffer
import MeterDexcomSupport.R2Record as R2Record

# Standard C0 / C1 control codes.
SOH = 0x01
SOF = 0x01
STX = 0x02
EOT = 0x04
ACK = 0x06
NAK = 0x15
CAN = 0x18

# Dexcom Seven Plus commands.
AM_I_FIRMWARE               = 0x1b
READ_BLOCK                  = 0x01
READ_DATABASE               = 0x17
READ_DATABASE_REVISION      = 0x1c
READ_DISPLAY_TIME_OFFSET    = 0x37
READ_FIRMWARE_HEADER        = 0x0c
READ_GMT                    = 0x39
READ_HIGH_GLUCOSE_THRESHOLD = 0x2d
READ_HW_CONFIG              = 0x07
READ_INTERNAL_TIME          = 0x13
READ_LOW_GLUCOSE_THRESHOLD  = 0x2b
READ_SETTINGS               = 0x10
READ_TRANSMITTER_ID         = 0x29

class ConvertException(Exception):
    """
    Unable to convert (unserialize) the data. 
    """
    def __init__(self, text):
        self.text = text
        
    def __str__(self):
        return self.text

class DexcomSevenPlus(object):
    """
    A class holding analyzed data from a Dexcom Seven Plus device. 
    """

    def __init__(self, data):
        """
        Instance the object using the data provided. 
        """
        # Feed the raw data into a new buffer object. 
        self.buffer = Buffer.Buffer(data)

        self.firmware_header = None
        self.hw_config = None
        self.records = []
        
    def GetSerialNumber(self):
        """
        Return serial number. 
        """
        if self.hw_config != None:
            return self.hw_config.device_id
        return 0
        
    def SplitDatabaseIntoBlocks(self):
        """
        Split the database into 64k blocks and sort the blocks. The test data we have suggests that
        the blocks are already sorted but the reference implementation performs this operation.
        
        The blocks are inserted into a dictionary with the block index as key (and with a check 
        that block index is unique).
        """
        
        self.blocks = {}
        
        while True:
           
            try:    
                # Create a new Buffer.Buffer holding a single block.
                block = self.buffer.get_sub_buffer(0x10000)
                
            except Buffer.ExtractException:
                if self.buffer.nr_bytes_left() != 0:
                    raise ConvertException('Total buffer size is not a multiple of block size')
                # No more data - let's move on. 
                break

            block_header = R2Record.R2BlockHeader(block, R2Record.R2BlockHeader.RecordLength())

            if block_header.header_tag != 0x5844:
                raise ConvertException('Incorrect header tag on block header (%04x)' % (block_header.header_tag))
                break
                
            if self.blocks.has_key(block_header.number):
                raise ConvertException('Block number already exists (%d)' % (block_header.number))
                break
                
            self.blocks[block_header.number] = (block_header, block)

    def AddSettingIfAvailable(self, settings, record, setting_id, record_variable):
        """
        There are several (6) versions of the settings record (depending on the version of the 
        firmware) with different settings (variables). This function will add a setting to the 
        settings list if and only if it is availble in the record instance. 
        
        @param settings
            List of settings into the new settings will be added. 
            
        @param record
            R2 settings record instance
            
        @param setting_id
            The setting id from Defines.py. 
            
        @param record_variable
            The R2 settings record variable that holds the setting. 
            
        @return True if setting was added.
        """
        if record.__dict__.has_key(record_variable):
            settings[setting_id] = record.__dict__[record_variable]
            return True
            
        return False

    def CreateSettingsRecord(self, record):
        """
        Create a dictionary with all settings (extracted from the latest settings record found
        in the database). Trying to match the CGM settings from Animas. 
        
        SETTING_CGM_SOUND_TYPE_USER_HIGH_GLUCOSE_WARNING	0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High	m_highGlucoseAlertType	Vibrate = 0, VibrateThenBeep = 1, Silent = 2, Disabled = 3					
        SETTING_CGM_SOUND_TYPE_USER_LOW_GLUCOSE_WARNING	0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High	m_lowGlucoseAlertType	Vibrate = 0, VibrateThenBeep = 1, Silent = 2, Disabled = 3					
        SETTING_CGM_SOUND_TYPE_RISE_RATE_WARNING	0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High	m_upRateAlertType	Vibrate = 0, VibrateThenBeep = 1, Silent = 2, Disabled = 3					
        SETTING_CGM_SOUND_TYPE_FALL_RATE_WARNING	0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High	m_downRateAlertType	Vibrate = 0, VibrateThenBeep = 1, Silent = 2, Disabled = 3					
        SETTING_CGM_SOUND_TYPE_TRM_OUT_OF_RANGE_WARNING	0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High	m_outOfRangeAlertType	Vibrate = 0, VibrateThenBeep = 1, Silent = 2, Disabled = 3					
        SETTING_CGM_SOUND_TYPE_OTHER_WARNING	0=Off, 1= Vibrate, 2=Low, 3=Medium, 4=High	m_otherAlertType	Vibrate = 0, VibrateThenBeep = 1, Silent = 2, Disabled = 3					
        SETTING_CGM_HIGH_GLUCOSE_WARNING_LIMIT	mg/dL	m_highGlucoseThreshold	mg/dL					
        SETTING_CGM_LOW_GLUCOSE_WARNING_LIMIT	mg/dL	m_lowGlucoseThreshold	mg/dL					
        SETTING_CGM_RISE_RATE_LIMIT	mg/dL	m_upRate	mg/dL					
        SETTING_CGM_FALL_RATE_LIMIT	mg/dL	m_downRate	mg/dL					
        SETTING_CGM_SNOOZE_TIME_USER_HIGH_GLUCOSE	0-300 min	m_highSnooze	min					
        SETTING_CGM_SNOOZE_TIME_USER_LOW_GLUCOSE	0-300 min	m_lowSnooze	min					
        SETTING_CGM_SNOOZE_TIME_TRM_OUT_OF_RANGE	0-300 min	m_outOfRangeMinutes	min					
        SETTING_CGM_USER_HIGH_GLUCOSE_WARNING	0 = Disable, 1= Enable	See AlertType setting						
        SETTING_CGM_USER_LOW_GLUCOSE_WARNING	0 = Disable, 1= Enable	See AlertType setting						
        SETTING_CGM_RISE_RATE_WARNING	0 = Disable, 1= Enable	See AlertType setting						
        SETTING_CGM_FALL_RATE_WARNING	0 = Disable, 1= Enable	See AlertType setting						
        SETTING_CGM_TRM_OUT_OF_RANGE_WARNING	0 = Disable, 1= Enable	See AlertType setting						
        SETTING_CGM_TRANSMITTER_ID	Transmitter id	m_transmitterSerialNumber						
        """
        settings = {}
        
        # These settings are identical to settings in the Animas Vibe pump - use the same 
        # defines. 
        #
        # NOTE : the thresholds must be in the range 56-401. 55 (for low) and 402 (for high) 
        # indicates OFF (see R2_MAXIMUM_HIGH_GLUCOSE_THRESHOLD and R2_MINIMUM_HIGH_GLUCOSE_THRESHOLD)
        #
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_HIGH_GLUCOSE_WARNING_LIMIT, 'high_glucose_threshold')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_LOW_GLUCOSE_WARNING_LIMIT, 'low_glucose_threshold')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_RISE_RATE_LIMIT, 'up_rate')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_RISE_RATE_LIMIT, 'down_rate')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_SNOOZE_TIME_USER_HIGH_GLUCOSE, 'high_snooze')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_SNOOZE_TIME_USER_LOW_GLUCOSE, 'low_snooze')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_SNOOZE_TIME_TRM_OUT_OF_RANGE, 'out_of_range_minutes')
        self.AddSettingIfAvailable(settings, record, SETTING_CGM_TRANSMITTER_ID, 'transmitter_serial_number')   
        
        # These settings are unique to the Dexcom Seven Plus (but holds information that is available 
        # in another format in the Animas Vibe).
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_HIGH_GLUCOSE_ALERT_TYPE, 'high_glucose_alert_type')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_LOW_GLUCOSE_ALERT_TYPE, 'low_glucose_alert_type')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_UP_RATE_ALERT_TYPE, 'up_rate_alert_type')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_UP_RATE_ALERT_TYPE, 'down_rate_alert_type')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_OUT_OF_RANGE_ALERT_TYPE, 'out_of_range_alert_type')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_OTHER_ALERT_TYPE, 'other_alert_type')

        # These settings are truly unique to the Dexcom Seven Plus. 
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_BEEP_ON_RECEIVE, 'beep_on_receive')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_DISPLAY_MODE, 'display_mode')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_BACKLIGHT_ENABLED, 'backlight_enabled')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_RECORDS_IN_LAST_METER_TRANSFER, 'records_in_last_meter_transfer')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_METER_TYPE, 'meter_type')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_LAST_METER_TRANSFER_TIME, 'last_meter_transfer_time')
        self.AddSettingIfAvailable(settings, record, SETTING_DEXCOM_METER_SERIAL, 'meter_serial')
        
        return {'record_type':'settings', 'record_value':settings}

    def ExtractDatabase(self):
        """
        Extract the database and store the individual records in a list (for later retrieval 
        by the generic analyze functions). 
        
        Raises ConvertException if an error occurs. 
        
        @return Number of records.
        """
        
        # Split database into 64k blocks
        self.SplitDatabaseIntoBlocks()
        
        skew_offset = 0
        user_offset = 0
        latest_settings_record = None
        
        nr_records = 0
        
        for block_key in sorted(self.blocks.keys()):
        
            empty_records = 0
            
            block_header    = self.blocks[block_key][0]
            block           = self.blocks[block_key][1]

            # Check the status of the block - should we extract it?
            if block_header.status in [R2Record.R2_BLOCK_STATUS_USED]: 
                pass    
            elif block_header.status in [R2Record.R2_BLOCK_STATUS_READY_TO_ERASE, \
                                         R2Record.R2_BLOCK_STATUS_ERASED]:
                continue
            else:
                raise ConvertException('Found invalid block status %d' % (block_header.status))
            
            # Extract all records from this blocks.
            while True:

                # It is possible to parse the last section of a block where
                # that section is erased and smaller than our criteria of 128 bytes.
                # It is also possible for a record to exactly fit at the end of 
                # a block and not have any end of block record.  Both of these are
                # cases that will cause us to walk off the end of the current block.
                # So ... we'll handle it just like a (sort of) normal EOB case.
                if block.nr_bytes_left() == 0:
                    break
            
                # Peek at the record type. 
                record_type = ord(block.peek_slice(1))

                #print record_type, block.nr_bytes_left()
                
                # No more data in this block - move on. 
                if record_type == R2Record.R2_RECORD_TYPE_ENDOFBLOCK:
                    break
                    
                elif record_type == R2Record.R2_RECORD_TYPE_UNASSIGNED:
                    empty_records += 1
                    if empty_records > 128:
                        break
                    # Must consume this byte...
                    block.consume(1)
                    continue
                
                # Retrieve the class of the record type. 
                record_class = R2Record.r2_record_type_class[record_type]

                # Create an instance of the record type. 
                record = record_class(block, record_class.RecordLength())
                
                # The type parameter in header should match the type parameter in the footer.
                if not record.CheckMatchingHeaderAndFooter():
                    raise ConvertException('Failed to match header.type with footer.type for record type %d' % (record_type))
                
                # Check record CRC.
                if not record.CheckCRC():
                    raise ConvertException('Failed CRC check for record of type %d' % (record_type))            
                                
                if record_type in [R2Record.R2_RECORD_TYPE_METER, \
                                   R2Record.R2_RECORD_TYPE_METER2, \
                                   R2Record.R2_RECORD_TYPE_METER3]:
                                   
                    nr_records += 1
                    self.records.append(record.GetRecordDictionary(user_offset + skew_offset))
                    
                elif record_type in [R2Record.R2_RECORD_TYPE_SENSOR, \
                                     R2Record.R2_RECORD_TYPE_SENSOR2, \
                                     R2Record.R2_RECORD_TYPE_SENSOR3]:
                                     
                    # Note from Dexcom (Phil Mayo).
                    # 
                    # You are correct to ignore values below 39 (<=38) as these are not estimated 
                    # glucose values and are instead "special flags".  Also, if the code does not 
                    # already filter out values above 401 (>=402) then you can do that as well 
                    # (the only one that comes to mind is 0x0FFF which is just "temporarily empty").  
                    # The receiver will only compute estimated glucose values from 39 to 401 
                    # (inclusive).  Therefore, for any reporting and charting purposes take into 
                    # consideration that 39 and 401 are railed values ... meaning "39 or below" 
                    # and "401 or above" where you may or may not want to treat those values as exact.
                    if record.GetGlucoseValue() >= 39 and record.GetGlucoseValue() <= 401:
                        nr_records += 1
                        self.records.append(record.GetRecordDictionary(user_offset + skew_offset))
                    
                elif record_type in [R2Record.R2_RECORD_TYPE_SETTINGS, \
                                     R2Record.R2_RECORD_TYPE_SETTINGS2, \
                                     R2Record.R2_RECORD_TYPE_SETTINGS3, \
                                     R2Record.R2_RECORD_TYPE_SETTINGS4, \
                                     R2Record.R2_RECORD_TYPE_SETTINGS5, \
                                     R2Record.R2_RECORD_TYPE_SETTINGS6]:
                                     
                    user_offset = record.external_offset
                    skew_offset = record.skew_offset
                
                    if skew_offset > 0:
                        print "skew", skew_offset
                    
                    if latest_settings_record == None or record.GetDateTime() > latest_settings_record.GetDateTime():
                        latest_settings_record = record
                        
                elif record_type in [R2Record.R2_RECORD_TYPE_EVENT]:
                
                    # We only store carbs and insulin at the moment. Health and excercise is not 
                    # linked to a glucose record which makes it a bit hard to map those events to 
                    # our database. Confirmed with Daniel.
                    if record.event_type in [R2Record.R2_EVENT_TYPE_CARBS, R2Record.R2_EVENT_TYPE_INSULIN]: 
                        nr_records += 1
                        self.records.append(record.GetRecordDictionary(user_offset + skew_offset))
                    
                elif record_type in [R2Record.R2_RECORD_TYPE_LOG, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x02, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x07, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x09, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x0D, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x0E, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x0F, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x10, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x12, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x15, \
                                     R2Record.R2_RECORD_TYPE_RESERVED0x18]:
                                    
                    pass

                elif record_type in [R2Record.R2_RECORD_TYPE_CORRUPTED, \
                                     R2Record.R2_RECORD_TYPE_UNKNOWN1, \
                                     R2Record.R2_RECORD_TYPE_BADTIMEMETER]:
                                     
                    raise ConvertException('Invalid record type %d' % (record_type))
                        
                else:
                                     
                    raise ConvertException('Invalid record type %d' % (record_type))

        # Add settings.
        settings = self.CreateSettingsRecord(latest_settings_record)
        if len(settings) > 0:
            nr_records += 1
            self.records.insert(0, settings)

        return nr_records
        
    def Analyse(self):
        """
        Analyse the first part from the Dexcom extractor. This part consists of 
        responses from the device itself. 

        Frame format : CMD SOH RES LEN_L LEN_H PAYLOAD CRC_L CRC_H

        CMD is the request command - this is added by the extractor and is not part of
        the standard Dexcom format. 

        RES is the result of the command. If all is ok this should be ACK but it can
        also contain an error code (e.g. invalid parameter).
        """

        nr_records = 0

        crc_func = crcmod.predefined.mkCrcFun('xmodem')

        while(True):

            try:

                # No more data in buffer - time to leave.
                if self.buffer.nr_bytes_left() == 0:
                    break

                # Extract the command id (the request). This is added by the 
                # extractor and is not part of the standard Dexcom communication
                # frame (and should thus not be part of the checksum calculation).
                command, = self.buffer.get_struct('<B')

                # Set mark in buffer so we can retrieve the slice of data used
                # for checksum calculation.
                self.buffer.set_mark()

                # Extract header. 
                control, result, payload_len = self.buffer.get_struct('<BBH')

                if control != SOH:
                    raise ConvertException('Control byte in frame not SOH (0x%x)' % (control))

                # The result should be == ACK otherwise we got an error from 
                # the device. 
                
                # Some commands are mandatory - for all others just continue. 
                if result != ACK and command in [READ_FIRMWARE_HEADER, READ_HW_CONFIG, READ_DATABASE]:
                    raise ConvertException('Received NAK on a mandatory command %d' % (command))

                # Create instances for those classes we are interested in.
                if command == READ_FIRMWARE_HEADER:
                    self.firmware_header = R2Record.R2FWHeader(self.buffer, payload_len)
                
                elif command == READ_HW_CONFIG:
                    if payload_len == 1024:
                        self.hw_config = R2Record.R2HWConfigV2(self.buffer, payload_len)
                    elif payload_len == 128:
                        self.hw_config = R2Record.R2HWConfig(self.buffer, payload_len)
                    else:
                        raise ConvertException('Found hw config record with invalid payload length %d' % (payload_len))
                
                else:
                    # If no R2 record class exists use the generic variant that
                    # simple consumes payload_len bytes of data. 
                    R2Record.R2Generic(self.buffer, payload_len)
                    
                # Extract header + payload for checksum calculation. 
                crc_frame = self.buffer.get_slice_from_mark()

                # Calculate checksum (CCITT xmodem).
                crc_calc = crc_func(crc_frame)

                # Extract checksum.
                crc_sent, = self.buffer.get_struct('<H')

                if crc_sent != crc_calc:
                    raise ConvertException('Command crc check failed - sent %x calculated %x' % (crc_sent, crc_calc))

                # A successful READ_BLOCK command is followed by 1024 (in our case since the 
                # terminal extracted 1024 bytes) bytes of raw memory dump. Skip this at the moment
                # to reach the next block "on-the-other-side".
                if command == READ_BLOCK:
                    self.buffer.consume(1024)
                    
                # A successful READ_DATABASE is followed by the complete database dump.
                if command == READ_DATABASE:
                    nr_records = self.ExtractDatabase()

            except Buffer.ExtractException, e:
                raise ConvertException('Failed to extract from buffer %s' % (e))    
                
        # Some interesting data is not found in the database but in the commands that was 
        # requested before we started the extraction of the database. The HW-config does e.g. 
        # include the serial number and the clock mode (12/24 hours). 
        try:
            # Find clock-mode in hw-config.
            if self.hw_config.firmware_flags & R2Record.R2_FIRMWARE_FLAGS_CLOCK_24H:
                self.records.insert(0, {'record_type':'settings', 'record_value':{SETTING_TIME_FORMAT:1}})
            else:
                self.records.insert(0, {'record_type':'settings', 'record_value':{SETTING_TIME_FORMAT:0}})
            nr_records += 1                

            # Find serial number in hw-config. Note : serial number is 'not' a record because it 
            # is handled in it's own function and does not count in the number of records check. 
            serial_number = {'record_type':'serial_number'}
            serial_number['record_value'] = self.hw_config.device_id
            self.records.insert(0, serial_number)
                            
        except KeyError:
            # No serial number!
            raise ConvertException('Found no serial or clock_mode number')
 
        # Add number of results records
        record = {'record_type':'nr_results', 'record_value':nr_records}
        self.records.insert(0, record)
 
# -----------------------------------------------------------------------------
# Interface Generic
# -----------------------------------------------------------------------------

def EvalDexcomSevenPlusUnitRecord( line ):
    """
    Generic callback for unit record.
    """
    return { 'meter_unit':'mg/dL' }

def EvalDexcomSevenPlusSerialRecord(line):
    """
    Generic callback for serial number record.
    """
    res = {}

    if line['record_type'] == 'serial_number':
        res['meter_serial'] = line['record_value']

    return res

def EvalDexcomSevenPlusNrOfResultRecord(line):
    """
    Generic callback for number of results record.
    """
    res = {}

    if line['record_type'] == 'nr_results':
        res['meter_nr_results'] = line['record_value']

    return res

def EvalDexcomSevenPlusResultRecord( line ):
    """
    Generic callback for result record. 
    
    For the Dexcom Seven Plus we handle settings using this function as well.
    """
    res = {}

    if line['record_type'] == 'meter':
        res[ELEM_TIMESTAMP]     = line['record_date_time']
        res[ELEM_VAL]           = line['record_value']
        res[ELEM_FLAG_LIST]     = line['record_flags']
        res[ELEM_VAL_TYPE]      = VALUE_TYPE_GLUCOSE
    elif line['record_type'] == 'sensor':
        res[ELEM_TIMESTAMP]     = line['record_date_time']
        res[ELEM_VAL]           = line['record_value']
        res[ELEM_FLAG_LIST]     = line['record_flags']
        res[ELEM_VAL_TYPE]      = VALUE_TYPE_GLUCOSE
    elif line['record_type'] == 'carbs':
        res[ELEM_TIMESTAMP]     = line['record_date_time']
        res[ELEM_VAL]           = line['record_value']
        res[ELEM_FLAG_LIST]     = line['record_flags']
        res[ELEM_VAL_TYPE]      = VALUE_TYPE_CARBS
    elif line['record_type'] == 'insulin':
        res[ELEM_TIMESTAMP]     = line['record_date_time']
        res[ELEM_VAL]           = line['record_value']
        res[ELEM_FLAG_LIST]     = line['record_flags']
        res[ELEM_VAL_TYPE]      = VALUE_TYPE_INS_BOLUS
    elif line['record_type'] == 'settings':
        res[SETTINGS_LIST]      = line['record_value']

    return res

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectDexcomSevenPlus( inList ):
    """
    Detect if data comes from a Dexcom Seven Plus device.
    """
    return DetectDevice( 'DexcomSevenPlus', inList, DEVICE_METER )

def AnalyseDexcomSevenPlus( inData ):
    """
    Analyse Dexcom Seven Plus. 
    """

    # Empty dictionary
    meter_callbacks = {}

    meter_callbacks[ "meter_type" ]               = 'Dexcom Seven Plus'
    meter_callbacks[ "eval_serial_record" ]       = EvalDexcomSevenPlusSerialRecord
    meter_callbacks[ "eval_result_record" ]       = EvalDexcomSevenPlusResultRecord
    meter_callbacks[ "eval_nr_results" ]          = EvalDexcomSevenPlusNrOfResultRecord
    meter_callbacks[ "eval_unit" ]                = EvalDexcomSevenPlusUnitRecord

    try:
        dexcom = DexcomSevenPlus(inData)
        dexcom.Analyse()
    except ConvertException, e:
        return CreateErrorResponseList('Dexcom Seven Plus', dexcom.GetSerialNumber(), ERROR_CODE_PREPROCESSING_FAILED, e)

    resList = AnalyseGenericMeter( dexcom.records, meter_callbacks );

    return resList

if __name__ == '__main__':

    f = open('test/testcases/test_data/DexcomSevenPlus/ep036.bin', 'rb')
    raw_data = f.read()
    f.close()

    res = AnalyseDexcomSevenPlus(raw_data)
    
#    for i in res:
#        print res
    
    
