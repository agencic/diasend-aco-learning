# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2008 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE SOOIL Dana Diabecare R

import re
from Generic import *
from Defines import *
from datetime import datetime
from datetime import time
import struct
import Common
import MeterLifescanOnetouchUltraSmart

# -----------------------------------------------------------------------------
# DEFINES 
# -----------------------------------------------------------------------------

CMD_SERIAL_NUMBER               = '\x32\x07'
CMD_IDENTIFICATION              = '\x03\x04'
CMD_RECORD_HISTORY              = '\x31'
CMD_GLUCOSE_UNIT                = '\x32\x09'
CMD_BASAL_RATE                  = '\x32\x02'
CMD_BASAL_PROFILES              = '\x32\x06'
CMD_BASAL_CURRENT_PROFILE       = '\x32\x0c'

CMD_SUB_HISTORY_BOLUS           = 0x01
CMD_SUB_HISTORY_DAILY_TOTAL     = 0x02
CMD_SUB_HISTORY_PRIME           = 0x03
CMD_SUB_HISTORY_GLUCOSE         = 0x04
CMD_SUB_HISTORY_ALARM           = 0x05
CMD_SUB_HISTORY_ERROR           = 0x06
CMD_SUB_HISTORY_CARBOHYDRATE    = 0x07
CMD_SUB_HISTORY_REFILEE         = 0x08
CMD_SUB_HISTORY_SUSPEND         = 0x09
CMD_SUB_HISTORY_BASAL_HOUR      = 0x0a
CMD_SUB_HISTORY_BASAL_TEMP      = 0x0b
CMD_SUB_HISTORY_END_OF_GROUP    = 0xf1

# -----------------------------------------------------------------------------
# GLOBALS
# -----------------------------------------------------------------------------

glob_last_history_record = None
glob_current_basal_profile_evaluated = False

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
    
def UpdateCRC(crc, byte):
 
    li_crc = 0;

    li_crc = (crc >> 8) | (crc << 8);
    li_crc ^= byte;
    li_crc ^= (li_crc & 0xff) >> 4;
    li_crc ^= (li_crc << 8) << 4;

    li_crc ^= ((li_crc & 0xff)<< 5) | (((li_crc & 0xff) >> 3) << 8);
    li_crc &= 0xffff

    return li_crc
            
 
def CalcCRC16Checksum(string):
    crc = 0;
    for ch in string:
        crc = UpdateCRC(crc, ord(ch))

    return chr(crc >> 8) + chr(crc & 0xff)

def SplitData( inData ):
    """
    Splits incoming data into a list. The data is binary frames
    """
    
    outData = []
    
    while inData:
        packet_length = ord(inData[2])+7
        out_packet = inData[:packet_length]
        outData.append(out_packet)
        inData = inData[packet_length:]

    return outData  
    
# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Dana Serial record. Extracted elements: 
    serial
    """
    if line[4:6] != CMD_SERIAL_NUMBER:
        return None
        
    # This is a serial number row!
    res = {}    
    res["serial"]   = line[6:16]
    return res
    
def EvalUnitRecord( line ):
    """
    Evaluate a Dana Unit record. Extracted elements: 
    unit
    """
    if line[4:6] != CMD_GLUCOSE_UNIT:
        return None
    
    # This row contains the unit!
    res = {}    
    res["unit"]   = ord(line[6])
    return res
    
def EvalBasalRateRecord( line ):
    """
    Evaluate a Dana Basal profile record. Extracted elements: 
    """
    if line[4:6] != CMD_BASAL_RATE:
        return None
    
    res = {}
    # Splice up the data into 2-and-2 (24 slices)
    res["24h"] = Common.SplitCount(line[6:6+2*24], 2)
    return res

def EvalBasalProfileRecord( line ):
    """
    Evaluate a Dana basal profile record
    """
    if line[4:6] != CMD_BASAL_PROFILES:
        return None

    # This line contains a record
    res = {}
    res["profile_no"] = str(ord(line[6])+1) #Indexes returned 0..3, displays 1..4
    res["24h"] = Common.SplitCount(line[7:7+2*24], 2)

    return res
    
def EvalCurrentBasalProfileRecord( line ):
    """
    Evaluate the current basal profile record
    """
    global glob_current_basal_profile_evaluated

    if line[4:6] != CMD_BASAL_CURRENT_PROFILE:
        return None

    glob_current_basal_profile_evaluated = True
    res = {}
    res["profile_no"] = str(ord(line[6])+1) #Indexes returned 0..3, displays 1..4
    return res
    
def EvalHistoryRecord( line ):
    if line[4] != CMD_RECORD_HISTORY:
        return None
    
    # This line contains a record!
    res = {} 
    res["subcmd"]   = ord(line[5])   
    
    # "End-of-group" info messages should be ignored
    if res["subcmd"] == CMD_SUB_HISTORY_END_OF_GROUP:
        return
    
    # This should be valid data for a history record
    res["type"]     = ord(line[6])
    res["year"]     = ord(line[7])
    res["month"]    = ord(line[8])
    res["date"]     = ord(line[9])
    res["hour"]     = ord(line[10])
    res["minute"]   = ord(line[11])
    res["second"]   = ord(line[12])
    res["code"]     = ord(line[13])
    res["value"]    = ord(line[14]) << 8 | ord(line[15])
    return res


# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalDanaSerialRecord( line ):
    """
    Is this line a serial record? If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m["serial"]

    return res
    

def EvalDanaDeviceModelRecord( line ):
    """
    The device model is fixed
    """
    res = { "device_model":"DANA Diabecare R" }
    return res

def EvalDanaUnitRecord( line ):
    """
    Is this line a unit record? If so, return a dictionary
    """
    res = {}
    m = EvalUnitRecord( line )
    if m:
        if m["unit"] == 0:
            unit = "mg/dl"
        else:
            unit = "mmol/l"
    
        # Note: This seems only to be the display unit. The values seems always to be stored in mg/dl        
        res["meter_unit"] = "mg/dl" 
    return res

def EvalDanaResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys
    """

    global glob_last_history_record
    res = {}

    m = EvalHistoryRecord(line)
    if m:
        
        if m["subcmd"] == CMD_SUB_HISTORY_BOLUS:
            # Retrieve the date
            try:
                res[ ELEM_TIMESTAMP ] = datetime(2000 + m["year"], m["month"], m["date"], m["hour"], m["minute"])
            except:
                # Invalid date, should be ignored according to Dana
                return {"null_result":True}

            # From Dana specification: History Code & 0x0F -> Bolus Injection Progress Hour 
            #                          History Second -> Bolus Injection Progress Minute 

            if (m["code"] & 0xf0) == 0x80:   #Ordinary bolus (step)
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS
                res[ ELEM_VAL ] = m["value"]*100
            elif (m["code"] & 0xf0) == 0xc0: #Extended bolus (has a duration)
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS
                res[ ELEM_VAL ] = m["value"]*100
                duration = (m["code"] & 0xf) * 60 + m["second"]
                res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]
                res[ ELEM_FLAG_LIST ] = [FLAG_BOLUS_TYPE_EXT]
            elif (m["code"] & 0xf0) == 0x90: # Dual bolus - extended part
                #Just save for future use (we also need the "dual bolus step" part)
                glob_last_history_record = m
                res = {"null_result":True} 
            elif (m["code"] & 0xf0) == 0xa0: # Dual bolus - step part
                #Verify that last record contained the extended part of this dual bolus pair
                #If the extended part is missing (has been demonstrated), we will drop this record
                if not glob_last_history_record:
                    return {"null_result":True}
                #Create the combo (dual) bolus result
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS
                res[ ELEM_VAL ] = m["value"]*100
                res[ ELEM_FLAG_LIST ] = [FLAG_BOLUS_TYPE_COMBO_EXT]
                duration = (glob_last_history_record["code"] & 0xf) * 60 + glob_last_history_record["second"]
                res[ ELEM_VALUE_LIST ] = [{ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_EXT, ELEM_VAL : glob_last_history_record["value"]*100}, \
                                          {ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]
            elif (m["code"] & 0xf0) == 0x40: # Old Pump firmware which we don't support
                    return { "error_code":ERROR_CODE_TOO_OLD_SW_VER, "line":line, "fault_data":line }
            if (m["code"] & 0xf0) != 0x90:
                # Only keep last result if it is needed
                glob_last_history_result = None
            
        elif m["subcmd"] == CMD_SUB_HISTORY_DAILY_TOTAL:
            # The total TDD. 
            try:
                res[ ELEM_TIMESTAMP ] = datetime(2000 + m["year"], m["month"], m["date"])
            except:
                # Invalid date, should be ignored according to Dana
                return {"null_result":True}
                
            basal_daily_total = m["hour"]*0x100 + m["minute"]
            bolus_daily_total = m["second"]*0x100 + m["code"]
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_TDD
            res[ ELEM_VAL ] = (m["value"]*VAL_FACTOR_TDD)/100  # The result is in factor *100
            res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : (basal_daily_total*VAL_FACTOR_TDD)/100, ELEM_VAL_TYPE : VALUE_TYPE_INS_BASAL_TDD}]
 
        elif m["subcmd"] == CMD_SUB_HISTORY_PRIME:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_PRIME
            res[ ELEM_VAL ] = m["value"]
            
        elif m["subcmd"] == CMD_SUB_HISTORY_ERROR:
            # Internal Dana use only
            pass
            
        elif m["subcmd"] == CMD_SUB_HISTORY_ALARM:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_ALARM
            if m["code"] == ord('B'):
                # "Battery error"
                alarm = ALARM_BATTERY_LOW
            elif m["code"] == ord('O'):
                # "Occlusion alarm"
                alarm = ALARM_OCCLUSION
            elif m["code"] == ord('C'):
                # "Check error"
                alarm = ALARM_CHECK_ERROR
            elif m["code"] == ord('S'):
                # "Shutdown alarm"
                alarm = ALARM_SHUTDOWN
            elif m["code"] == ord('D'):
                # "Daily Max Full"
                alarm = ALARM_DAILY_MAX_FULL 
            
            res[ ELEM_VAL ] = alarm

        elif m["subcmd"] == CMD_SUB_HISTORY_GLUCOSE:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
            res[ ELEM_VAL ] = m["value"]

        elif m["subcmd"] == CMD_SUB_HISTORY_CARBOHYDRATE:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_CARBS
            res[ ELEM_VAL ] = m["value"]

        elif m["subcmd"] == CMD_SUB_HISTORY_REFILEE:
            # @TODO: Check that it's ok to drop the value
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
            res[ ELEM_VAL ] = DIASEND_EVENT_REFILL

        elif m["subcmd"] == CMD_SUB_HISTORY_SUSPEND:
            if m["code"] == ord('O'):
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
                res[ ELEM_VAL ] = DIASEND_EVENT_DELIVERY_SUSPENDED
            else:
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
                res[ ELEM_VAL ] = DIASEND_EVENT_DELIVERY_RESUMED

        elif m["subcmd"] == CMD_SUB_HISTORY_BASAL_HOUR:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BASAL
            res[ ELEM_VAL ] = (m["value"]*VAL_FACTOR_BASAL)/100  #value is expressed in factor *100

        elif m["subcmd"] == CMD_SUB_HISTORY_BASAL_TEMP:
            if m["code"] == ord('O'):
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
                res[ ELEM_VAL ] = DIASEND_EVENT_TEMP_BASAL_ON
            else:
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
                res[ ELEM_VAL ] = DIASEND_EVENT_TEMP_BASAL_OFF
            
        else:
            return { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }

        if not ELEM_TIMESTAMP in res:
            # If timestamp is not set, it means there is no "special" handling of the timestamp fields.
            # Meaning we can store timestamp "as usual".
            
            # Ok timestamp to store
            try:
                res[ ELEM_TIMESTAMP ] = datetime(2000 + m["year"], m["month"], m["date"], m["hour"], m["minute"], m["second"])
            except:
                # Invalid date, should be ignored according to Dana
                return {"null_result":True}

        # Some values are deliberatly skipped
        if not res.has_key(ELEM_VAL_TYPE):
            return {"null_result":True}
    
    if not m:
        m = EvalBasalProfileRecord(line)
        if m:
            periods = []
            for i in range(24):
                start_time = time(i, 0)
                rate = 10 * (ord(m["24h"][i][1]) | ord(m["24h"][i][0]) << 8)
                periods.append((start_time, rate))

            res[PROGRAM_TYPE] = PROGRAM_TYPE_BASAL
            res[PROGRAM_NAME] = m["profile_no"]
            res[PROGRAM_PERIODS] = periods
    
    if not m:
        m= EvalCurrentBasalProfileRecord(line)
        if m:
            res[SETTINGS_LIST] = {SETTING_ACTIVE_BASAL_PROGRAM_ID : m["profile_no"]}


    if not m:    
        m = EvalBasalRateRecord(line)
        if m:
            return {"null_result":True}

    return res
    
def EvalDanaChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    # Check the checksum on frame level (all messages from the pump contains the checksum)
    # First check the checksum, but do not include the two start and two stop bytes, the length and the checksum itself    
    return CalcCRC16Checksum(line[3:-4]) == line[-4:-2]

def EvalDanaAllLinesEvaluated():
    """
    Just make sure that we have received current basal profile
    because then we know that it is a supported firmware version
    """
    global glob_current_basal_profile_evaluated

    if not glob_current_basal_profile_evaluated:
        return { "error_code":ERROR_CODE_TOO_OLD_SW_VER, "line":"", "fault_data":"" }
        
    return { "null_result":True } 

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectDanaDiabecare( inList ):
    """
    Detect if data comes from a Dana Diabecare R pump. 
    """
    return DetectDevice( "DanaDiabecare", inList, DEVICE_PUMP )

def AnalyseDanaDiabecare( inData ):
    """
    Analyse Dana Diabecare Pump
    """
    global glob_last_history_record 
    global current_basal_profile_evaluated
   
    glob_last_history_record = None
    glob_current_basal_profile_evaluated = False
    inList = SplitData (inData)
    
    # Eval dict
    d = { \
        "eval_serial_record" : EvalDanaSerialRecord, \
        "eval_device_model_record" : EvalDanaDeviceModelRecord, \
        "eval_unit" : EvalDanaUnitRecord, \
        "eval_result_record" : EvalDanaResultRecord, \
        "eval_checksum_record" : EvalDanaChecksumRecord, \
        "all_lines_evaluated": EvalDanaAllLinesEvaluated, \
    }

    resList = AnalyseGenericMeter( inList, d );

    return resList

if __name__ == "__main__":
                                           
    #testfile = open('test/testcases/test_data/DanaDiabecare/DanaDiabecare-ticket2256.bin')
    #testfile = open('test/testcases/test_data/DanaDiabecare/DanaDiabecare-Z19968-ticket3445-invalid-date.bin')    
    testfile = open('test/testcases/test_data/DanaDiabecare/DanaDiabecare-Z19997-alarm-d.bin')
    testcase = testfile.read()
    testfile.close()

    #res = AnalyseDanaDiabecare(dataRev2011)
    res = res = AnalyseDanaDiabecare(testcase)

    print res
