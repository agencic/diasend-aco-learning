# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014-2015 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE Infopia Element
# @DEVICE Infopia GlucoLab
# @DEVICE Infopia GluNEO
# @DEVICE BBraun Omnitest 5
# @DEVICE Infopia Finetest Lite
#

import traceback
import re
import struct

from Generic import *
from Defines import *

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def CalculateChecksum(line):
    calc_checksum = 0

    for c in line:
        calc_checksum += ord(c) - ord('0')

    c1 = calc_checksum & 0xff
    c2 = (c1 * 2) & 0xff

    return c1 * 0x100 + c2

def EvalSerialRecord(line):

    m = re.match(r'I([A-Z0-9]*)(.{2})$', line)
    if m:
        checksum      = ord(m.group(2)[0]) * 0x100 + ord(m.group(2)[1])
        calc_checksum = CalculateChecksum(m.group(1))

        if checksum == calc_checksum:
            return {ELEM_DEVICE_SERIAL: m.group(1)}

    return {}

def EvalUnitRecord(line):
    return {ELEM_DEVICE_UNIT: "mg/dL"}

def EvalModelRecord(line):

    result = {}

    serial_number_dict = EvalSerialRecord(line)
    if ELEM_DEVICE_SERIAL in serial_number_dict:
        if serial_number_dict[ELEM_DEVICE_SERIAL].startswith('G21B'):
            result = {ELEM_DEVICE_MODEL: 'Infopia Element'}
        elif serial_number_dict[ELEM_DEVICE_SERIAL].startswith('G22A'):
            result = {ELEM_DEVICE_MODEL: 'Infopia GlucoLab'}
        elif serial_number_dict[ELEM_DEVICE_SERIAL].startswith('G101'):
            result = {ELEM_DEVICE_MODEL: 'Infopia GluNEO'}
        elif serial_number_dict[ELEM_DEVICE_SERIAL].startswith('GAF'):
            result = {ELEM_DEVICE_MODEL: 'B Braun Omnitest 5'}
        elif serial_number_dict[ELEM_DEVICE_SERIAL].startswith('G103'):
            result = {ELEM_DEVICE_MODEL: 'Infopia Finetest Lite'}

    return result

def EvalResultRecord(line):

    result = {}

    def _fuzzy_to_int(value):
        try:
            return int(value)
        except ValueError:
            return value    

    m = re.match(r'^(\d{3})(\d{1})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{3})(\d{3})(.{2})$', line)
    if m:
        keys   = ('uid', 'icon', 'year', 'month', 'day', 'hour', 'minute', 'value', 'temp', 'checksum')
        values = map(_fuzzy_to_int, m.groups()[:9])
        items  = dict(zip(keys, values))

        checksum = struct.unpack('>H', line[-2:])[0]
        calc_checksum = CalculateChecksum(line[:-2])

        if checksum == calc_checksum:

            result = {
                ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                ELEM_TIMESTAMP: datetime(2000+items['year'], items['month'], items['day'], items['hour'], items['minute']),
                ELEM_VAL      : items['value'] * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL,
                ELEM_FLAG_LIST: []
                }

            # Setup generic flags
            if items['icon'] == 1:
                result[ELEM_FLAG_LIST].append(FLAG_BEFORE_MEAL)
            elif items['icon'] == 2:
                result[ELEM_FLAG_LIST].append(FLAG_AFTER_MEAL)
            # This is not in use for the Omnitest 5
            elif items['icon'] == 3:
                result[ELEM_FLAG_LIST].append(FLAG_MEDICATION)
            # This is not in use for the Omnitest 5
            elif items['icon'] == 4:
                result[ELEM_FLAG_LIST].append(FLAG_AFTER_EXERCISE)
            elif items['icon'] == 5:
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL)
            # This was introduced in the Omnitest 5 spec
            elif items['icon'] == 6:
                result[ELEM_FLAG_LIST].append(FLAG_EVENT)
            elif items['icon'] != 0:
                # We got something unexpected -> thow an exception
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                                'EvalResultRecord - Unknown icon: %d' % (items['icon']))

            # Lo/Hi-flags are <10 and >600 according to user manual
            if items['value'] < 10:
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
            elif items['value'] > 600:
                result[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
                

    return result

def EvalChecksumRecord(line, record):
    return True

def EvalChecksumFile(inList):
    return True

def EvalNrResultsRecord(line):
    m = re.match(r'S([0-9]{3})\r\n$', line)
    if m:
        return {ELEM_NR_RESULTS: int(m.group(1))}
    return {}

def SplitLines(data):

    result = []

    try:
        idx1 = data.index('TAG_SERIAL')
        idx2 = data.index('TAG_GLUCOSE')
    except ValueError:
        # Failed to find tags.
        return []

    serial_data  = data[idx1+10:idx2] # 10 = len('TAG_SERIAL')
    result.append(serial_data)

    glucose_data = data[idx2+11:]     # 11 = len('TAG_GLUCOSE') 

    # first comes number of results.
    result.append(glucose_data[:6])

    if len(glucose_data[6:]) % 22 == 0:
         result.extend([glucose_data[6:][i:i+22] for i in range(0, len(glucose_data[6:]), 22)])

    return result

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterInfopiaElement(inList):
    """
    Detect if data comes from a Infopia Element.
    """

    return DetectDevice( 'Infopia_Element', inList, DEVICE_METER )


def AnalyseMeterInfopiaElement(lines):
    """
    Analyse data from Infopia Element.
    """
    d = {
         "eval_device_model_record": EvalModelRecord,
         "eval_serial_record"      : EvalSerialRecord,
         "eval_unit"               : EvalUnitRecord,
         "eval_result_record"      : EvalResultRecord,
         "eval_checksum_record"    : EvalChecksumRecord,
         "eval_checksum_file"      : EvalChecksumFile,
         "eval_nr_results"         : EvalNrResultsRecord}

    lines    = SplitLines(lines)
    res_list = AnalyseGenericMeter(lines, d)

    return res_list

if __name__ == "__main__":

    test_files = (
        'test/testcases/test_data/InfopiaElement/G21B14F1000001.log',
        'test/testcases/test_data/InfopiaElement/G21B14F1000002.log',
        'test/testcases/test_data/InfopiaElement/G21B14F1000003.log',
        'test/testcases/test_data/InfopiaGlucoLab/G22A14F1000001.log',
        'test/testcases/test_data/InfopiaGlucoLab/G22A14F1000003.log',
        'test/testcases/test_data/InfopiaGlucoLab/InfopiaGlucoLab-hi.log',
        'test/testcases/test_data/InfopiaGluNEO/G101B14F1000001.log',
        'test/testcases/test_data/InfopiaGluNEO/G101B14F1000002.log',
        'test/testcases/test_data/InfopiaGluNEO/G101B14F1000003.log',
        'test/testcases/test_data/BBraunOmnitest5/empty.log',
        'test/testcases/test_data/BBraunOmnitest5/500-entries.log',
        'test/testcases/test_data/InfopiaFineTestLite/G103D15A0902751.log',
        'test/testcases/test_data/InfopiaFineTestLite/G103D15A0902752.log,'
        'test/testcases/test_data/InfopiaFineTestLite/G103D15A0902753.log ')

    for test_file in test_files:
        with open(test_file, 'rb') as f:
            data = f.read()
            results = AnalyseMeterInfopiaElement(data)
            print results[0]
