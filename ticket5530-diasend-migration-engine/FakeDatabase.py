# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import EventLog
import time
from Generic import *
from Defines import *



def InsertHistoryIntoDatabase( terminal_serial, meter_serial, entry ):
    """
    Insert log entry into history.
    """
    print "History : " + terminal_serial + ": " + meter_serial + ": " + entry


def InsertListIntoDatabase( inList, device_class, allowDupsWithinTrans ):
    """
    Insert list of values into database.
    
    if allowDupsWithinTrans is set to True, the code first checks if elements in the list exists in the database,
     and afterwards inserts the non-existing elements to the database. Otherwhise each element is handled
     individiually.
    """
    print "DB insert:"
    print device_class
    print repr(inList)
    return True

# ------------------------------------------------------------------------------

def UpdateIncomingData( inList, sizeOfIncomingData ):
    """
    Update information about transfer
    """
    print "Update: "
    print repr(inList)
    print sizeOfIncomingData

# ------------------------------------------------------------------------------


def InsertErrorLogIntoDatabase( inList ):
    """
    Insert list of values into database.
    """
    print "Error:"
    print repr(inList)

# ------------------------------------------------------------------------------

def CheckSubscriptionLevel(terminal_serial, device_class):
    """
    Check if the subscription for this terminal allows data from this device type
    """
    global connection

    # No special subscription is currently required for Meters or Pens
    if (device_class == DEVICE_METER) or (device_class == DEVICE_PEN):
        return True

    return True

# ------------------------------------------------------------------------------

def SelectDatabase(terminal_serial):
    """
    Given a serial number of the terminal, 
    this function will set up the configuration of the database
    """
