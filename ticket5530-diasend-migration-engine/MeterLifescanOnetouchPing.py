# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006-2009 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE LifeScan OneTouch Ping Meter

import sys
import re
from Generic import *
from Defines import *
import time
import datetime
import Common

value_list = []
meter_serial = ''
RECORD_SIZE = 16

RECORD_TYPE_GLUCOSE = 0
RECORD_TYPE_DIET_RECORD = 20
    
# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
def SplitData( inData ):
    """
    Splits incoming data into a list. The data is a mixture of ascii,
    (the header) and binary result records.
    The result records are just fixed length binary chunks
    """

    # The first line of the data from is ASCII and terminated by cr lf
    _rindex = inData.find('\r\n')
    _ptr = _rindex+2
    _bodyList = [inData[:_rindex]]

    # Then follows <num_records><record_data_blob>, where one record is 16 bytes long
    # There is probably a more elegant Python solution for the following :)
    while _ptr < len(inData):
        _numElements = ord(inData[_ptr])
        _ptr += 1
        _ptrEnd = _ptr + _numElements * RECORD_SIZE
        _body = inData[_ptr:_ptrEnd]
        _bodyList += Common.SplitCount(_body, RECORD_SIZE)
        _ptr = _ptrEnd

    return _bodyList


def EvalFlags( ctrl, food, exer, health ):
    """
    Evaluates the flags of a One Touch Ping 
    Returns a list of found flags
    """
    ret_flags = []
    
    if ctrl == 1: 
        ret_flags.append(FLAG_RESULT_CTRL)
    
    if food == 1:
        ret_flags.append(FLAG_BEFORE_BREAKFAST)
    elif food == 2:
        ret_flags.append(FLAG_AFTER_BREAKFAST)
    elif food == 3:
        ret_flags.append(FLAG_BEFORE_LUNCH)
    elif food == 4:
        ret_flags.append(FLAG_AFTER_LUNCH)
    elif food == 5:
        ret_flags.append(FLAG_BEFORE_DINNER)
    elif food == 6:
        ret_flags.append(FLAG_AFTER_DINNER)
    elif food == 7:
        ret_flags.append(FLAG_NIGHT)

    if exer == 1:
        ret_flags.append(FLAG_BEFORE_EXERCISE)
    elif exer == 2:
        ret_flags.append(FLAG_DURING_EXERCISE)
    elif exer == 3:
        ret_flags.append(FLAG_AFTER_EXERCISE)

    if (health & 0x1) != 0:
        ret_flags.append(FLAG_STRESS)
    if (health & 0x2) != 0:
        ret_flags.append(FLAG_FEEL_HYPO)
    if (health & 0x4) != 0:
        ret_flags.append(FLAG_ILLNESS)
    if (health & 0x8) != 0:
        ret_flags.append(FLAG_MENSES)
    if (health & 0x10) != 0:
        ret_flags.append(FLAG_VACATION)
    if (health & 0x20) != 0:
        ret_flags.append(FLAG_OTHER)

    return ret_flags
    
def EvalResultRecord( line ):
    """
    Main function for validating a Ping result record
    """
    d = {}
    # timestamp in minutes from 1st January 2000
    _time_stamp = ord(line[2])*0x10000+ord(line[1])*0x100+ord(line[0])
    # convert to seconds
    _time_stamp = _time_stamp * 60
    d[ ELEM_TIMESTAMP ] = datetime.datetime(2000,1,1) + datetime.timedelta(0,_time_stamp)

    # Some serious bit stuffing:
    _device      = ord(line[3]) & 0x0f
    _record_type = ((ord(line[4]) & 0x03)<<4) + ((ord(line[3]) & 0xf0) >> 4)
    _deleted     = (0 == ((ord(line[4]) & 0x04) >> 2))
    _corrupt     = (0 == ((ord(line[15]) & 0x04) >> 2))  
    
    if (_deleted or _corrupt):
        # Deleted or corrupt record - ignore
        return {}
        
    elif (_record_type == RECORD_TYPE_GLUCOSE):
        d[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        _value          = ((ord(line[5]) & 0x1f) << 5) + ((ord(line[4]) & 0xf8) >> 3)
        _flag_ctrl      = (ord(line[5]) & 0x20) >> 5  
        _flag_food      = ((ord(line[6]) & 0x03) << 1) + ((ord(line[5]) & 0x80) >> 7)
        _flag_exercise  = (ord(line[6]) & 0x18) >> 3
        _flags_health   = ((ord(line[7]) & 0x07) << 3) + ((ord(line[6]) & 0xe0) >> 5)
            
        d[ ELEM_VAL ] = float(_value)
        d[ ELEM_FLAG_LIST ] = EvalFlags(_flag_ctrl, _flag_food, _flag_exercise, _flags_health)
        
        # Add flags for HIGH / LOW values. These is a special case
        # where we get a glucose value _but_ the meter itself
        # reports HI/LO in the display.
        if d[ ELEM_VAL ] > (VAL_FACTOR_CONV_MMOL_TO_MGDL * LIFESCAN_BG_VALUE_HIGH_MMOL):
            d[ ELEM_FLAG_LIST ].append(FLAG_RESULT_HIGH)
        elif d[ ELEM_VAL ] < (VAL_FACTOR_CONV_MMOL_TO_MGDL * LIFESCAN_BG_VALUE_LOW_MMOL):
            d[ ELEM_FLAG_LIST ].append(FLAG_RESULT_LOW)
        
#    ** Disabling carbs, since we need to handle this differently because of duplicates when using a pump **
#    elif (_record_type == RECORD_TYPE_DIET_RECORD):
#        d[ ELEM_VAL_TYPE ] = VALUE_TYPE_CARBS
#
#        # bits 47..35 contains the main value field
#        _value          = (ord(line[5]) << 5) + ((ord(line[4]) & 0xf8) >> 3)
#
#        # Extract the diet record data from the main value field
#        _meal_segment = _value & 0x07
#        _carbs = _value >> 3
#        
#        # Meal segment definitions. Note that "4" (alcohol) is considered invalid
#        _meal_type_dict = {
#            0:FLAG_BREAKFAST,
#            1:FLAG_LUNCH,
#            2:FLAG_DINNER,
#            3:FLAG_SNACK
#        }

#        # Validate the meal segment and set the value
#        if _meal_segment in _meal_type_dict:
#            d[ ELEM_FLAG_LIST] = []
#            d[ ELEM_FLAG_LIST ].append(_meal_type_dict[_meal_segment])
#            d[ ELEM_VAL ] = float(_carbs)
#        else:
#            # Invalid meal segment - ignore
#            return {}
    else:
        # Not handled record type - ignore
        return {}
    
    return d
    
# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Lifescan Onetouch Ping Serial record. Extracted groups : 
    group 1 : serial nr
    group 2 : cksum
    """
    m = re.match( r'@(\w+)\s+([\d|a-f]{4,4})', line, re.IGNORECASE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalLifescanOnetouchPingSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(1)
    return res
    
def EvalLifescanOnetouchPingUnitRecord( line ):
    """
    Always return mg/dl 
    """
    return { "meter_unit":"mg/dL" }

def EvalLifescanOnetouchPingChecksumRecord( line, record ):
    """
    Evaluate checksum of result record. 
    Not applicable for this meter.
    """
    return True

def EvalLifescanOnetouchPingResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    res = {}
    
    # Length of the result records
    if len(line) != RECORD_SIZE:
        return res
        
    res = EvalResultRecord(line)
    
    return res


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectLifescanOnetouchPing( inList ):
    """
    Detect if data comes from a Onetouch Ping.
    """
    return DetectDevice( 'LifescanOnetouchPingMeter', inList, DEVICE_METER )


def AnalyseLifescanOnetouchPing( inData ):
    """
    Analyse Lifescan Onetouch Ping 
    """
    # Empty dictionary
    d = {}

    d[ "meter_type" ]           = "Onetouch Ping Meter"
    d[ "eval_serial_record" ]   = EvalLifescanOnetouchPingSerialRecord
    d[ "eval_unit"]             = EvalLifescanOnetouchPingUnitRecord
    d[ "eval_result_record" ]   = EvalLifescanOnetouchPingResultRecord
    d[ "eval_checksum_record" ] = EvalLifescanOnetouchPingChecksumRecord
    
    inList = SplitData (inData)
    
    resList = AnalyseGenericMeter( inList, d );
    
    return resList

# -----------------------------------------------------------------------------
# SOME CODE THAT WILL RUN ON IMPORT
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    testfile = open('test/testcases/test_data/OneTouchPing/OneTouchPing-with-carbs.log')
    testcase = testfile.read()
    testfile.close()

    res = AnalyseLifescanOnetouchPing(testcase)
    print res
