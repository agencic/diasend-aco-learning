# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# -----------------------------------------------------------------------------

# @DEVICE BLE Generic Device (via mobile app / BLE)

from Generic import *
from Defines import *
import time
from datetime import datetime
from datetime import timedelta
import struct
import io
import math

# -----------------------------------------------------------------------------
# DEFINES
# -----------------------------------------------------------------------------
DEVICE_BLE_DIASEND_SEPARATOR = "!TAG!SEPARATOR!"

NO_MANUFACTURER_PROVIDED = "N/A"

MANUFACTURER_ROCHE = "Roche"
MODEL_ROCHE_FRIENDLY_NAME_GROUP_1 = "Accu-Chek Aviva Connect"
MODEL_ROCHE_FRIENDLY_NAME_GROUP_2 = "Accu-Check Performa Connect"
MODEL_ROCHE_UNKNOWN = "Accu-Check Generic Device"

MANUFACTURER_KRONOS = "AgaMatrix"
MODEL_KRONOS = "Kronos"
MODEL_KRONOS_FRIENDLY_NAME = "AgaMatrix Wavesense Jazz"
MODEL_KRONOS_UNKNOWN = "AgaMatrix Generic Device"

MANUFACTURER_NIPRO = "Nipro Diagnostics Inc"
MODEL_NIPRO_TRUE_METRIX_AIR = "TRUE METRIX AIR"
MODEL_NIPRO_FRIENDLY_NAME_TRUE_METRIX_AIR = "Nipro True Metrix Air"
MODEL_NIPRO_UNKNOWN = "Nipro Generic Device"

GATT_GLUCOSE_LENGTH_PAYLOAD_RECORD = 17

MDER_S_POSITIVE_INFINITY = 0x07FE
MDER_S_NaN = 0x07FF
MDER_S_NRes = 0x0800
MDER_S_RESERVED_VALUE = 0x0801
MDER_S_NEGATIVE_INFINITY = 0x0802
FIRST_S_RESERVED_VALUE = MDER_S_POSITIVE_INFINITY


# -----------------------------------------------------------------------------
# HELPER FUNCTIONS
# -----------------------------------------------------------------------------
def U16toSFLOAT(int_data):
    mantissa = int_data & 0x0FFF
    exponent = int_data >> 12

    if (exponent >= 0x0008):
        exponent = -((0x000F + 1) - exponent)

    output = 0.0

    if (mantissa >= FIRST_S_RESERVED_VALUE and mantissa <= MDER_S_NEGATIVE_INFINITY):
        output = float('nan')
    else:
        if (mantissa >= 0x0800):
            mantissa = -((0x0FFF + 1) - mantissa)
        magnitude = pow(10.0, exponent)
        output = (mantissa * magnitude)

    return output

# -----------------------------------------------------------------------------
# CLASS FUNCTIONS
# -----------------------------------------------------------------------------
class MeterGATTProfileAnalyser(object):
    def __init__(self):
        self.inList = []
        self.flagList = []
        self.valueList = []
        self.serial = {}
        self.model = {}
        self.manufacturer = {}
        self.combined_values = []

    def split(self, indata):
        """
        Splits incoming data into a list. 
        
        The data consists of records, all starting with a Diasend-specific separator
        """
        # Split according to tags
        self.inList = indata.split(DEVICE_BLE_DIASEND_SEPARATOR)
    
        # Remove any empty lines (i.e the first one)
        self.inList = filter(None, self.inList)

        tempModel = {}
        tempManufacturer = {}

        for oneTag in self.inList:
            if oneTag.startswith("TAG!SERIAL!"):self.EvalGATTProfileGlucoseSerialRecord(oneTag)
            if oneTag.startswith("TAG!VALUES!"):self.EvalGATTProfileGlucoseResultRecord(oneTag)
            if oneTag.startswith("TAG!FLAGS!"):self.EvalGATTProfileGlucoseFlagRecord(oneTag)

            if oneTag.startswith("TAG!MODEL!"):tempModel = oneTag
            if oneTag.startswith("TAG!MANUFACTURER!"):tempManufacturer = oneTag

        if tempModel:
            self.EvalGATTProfileGlucoseDeviceModelRecord(tempModel,tempManufacturer)

        self.updateResultsWithFlags()

    def updateResultsWithFlags(self):
        for oneValue in self.valueList:
            flagsToAdd = []
            for flagItems in self.flagList:
                if (flagItems['sequence_number'] == oneValue['sequence_number']):
                    flagsToAdd += flagItems['flags']
            self.combined_values.append({
                ELEM_TIMESTAMP      : oneValue[ELEM_TIMESTAMP],
                ELEM_VAL_TYPE       : VALUE_TYPE_GLUCOSE,
                ELEM_VAL            : oneValue[ELEM_VAL],
                ELEM_DEVICE_UNIT    : oneValue[ELEM_DEVICE_UNIT],
                ELEM_FLAG_LIST      : flagsToAdd
            })
 

    def EvalGATTProfileGlucoseFlagRecord(self,line):
        """
        Evaluate BLE device flags
        """
        plx = (io.BytesIO(line[10:]))
        while plx.read(1):
            plx.seek(-1,1)
            # Create placeholder for flags
            o = type("FlagObject", (object,),{})

            # Extract all flags according to org.bluetooth.characteristic.glucose_measurement_context
            o.datafields      = struct.unpack("B", plx.read(1))[0]
            o.sequence_number = struct.unpack("<H", plx.read(2))[0]

            # Use datafields to determine what more fields are available
            o.c1              = o.datafields & 0x80 != 0
            o.c2              = o.datafields & 0x01 != 0
            o.c3              = o.datafields & 0x02 != 0
            o.c4              = o.datafields & 0x04 != 0
            o.c5              = o.datafields & 0x08 != 0
            o.c6              = o.datafields & 0x10 != 0
            o.c7              = o.datafields & 0x40 != 0
            o.c8              = o.datafields & 0x20 == 0
            o.c9              = o.datafields & 0x20 != 0

            if o.c1:
                o.extended_flags = struct.unpack("B", plx.read(1))[0]
            if o.c2:
                o.carb_id = struct.unpack("B", plx.read(1))[0]
                o.carb_kg = U16toSFLOAT(struct.unpack("<H", plx.read(2))[0])
            if o.c3:
                o.meal_info = struct.unpack("B", plx.read(1))[0]
            if o.c4:
                o.c4_info = struct.unpack("B", plx.read(1))[0]
                o.tester_info = o.c4_info & 0x0F
                o.health = (o.c4_info & 0xF0)>>4
            if o.c5:
                o.exercise_duration  = struct.unpack("<H", plx.read(2))[0]
                o.exercise_intensity = struct.unpack("B", plx.read(1))[0]
            if o.c6:
                o.medication_id = struct.unpack("B", plx.read(1))[0]
            if (o.c6 and o.c8):
                o.medication_kg = U16toSFLOAT(struct.unpack("<H", plx.read(2))[0])
            if (o.c6 and o.c9):
                o.medication_l  = U16toSFLOAT(struct.unpack("<H", plx.read(2))[0])
            if o.c7:
                o.hba1c = U16toSFLOAT(struct.unpack("<H", plx.read(2))[0])

            # Convert the flags
            ###################################
            # All flags covered by BLE standard
            ###################################
            # Breakfast             - C2 active and carb id = 1
            # Lunch                 - C2 active and carb id = 2
            # Dinner                - C2 active and carb id = 3
            # Snack                 - C2 active and carb id = 4
            # Drink                 - C2 active and carb id = 5
            # Supper                - C2 active and carb id = 6
            # Brunch                - C2 active and carb id = 7
            # Carb units            - C2 active sfloat in kg
            # Before meal           - C3 active and meal = 1
            # After meal            - C3 active and meal = 2
            # Fasting               - C3 active and meal = 3
            # Casual                - C3 active and meal = 4
            # Bedtime               - C3 active and meal = 5
            # Self test             - C4 active and tester = 1
            # Professional test     - C4 active and tester = 2
            # Lab test              - C4 active and tester = 3
            # Tester not available  - C4 active and tester = 15
            # Minor health issue    - C4 active and info = 1
            # Major health issue    - C4 active and info = 2
            # During menses         - C4 active and info = 3
            # Under stress          - C4 active and info = 4
            # No health issues      - C4 active and info = 5
            # Healt value NA        - C4 active and info = 15
            # Excercise in seconds  - C5 active and duration < 65535
            # Excercise overrun     - C5 active and duration = 65535
            # Excercise intensity % - C5 active and intensity is in percent
            # Rapid acting insulin  - C6 active and medication id = 1
            # Short acting insulin  - C6 active and medication id = 2
            # Intermediate insulin  - C6 active and medication id = 3
            # Long acting insulin   - C6 active and medication id = 4
            # Pre mixed insulin     - C6 active and medication id = 5
            # Medication in kg      - C6 active and C8 active, SFLOAT
            # Medication in l       - C6 active and C9 active, SFLOAT
            # HbA1c                 - C7 active
            flags = []
            if o.c2:
                if o.carb_id == 1: flags.append(FLAG_BREAKFAST)
                if o.carb_id == 2: flags.append(FLAG_LUNCH)
                if o.carb_id == 3: flags.append(FLAG_DINNER)
                if o.carb_id == 4: flags.append(FLAG_SNACK)
                if o.carb_id == 5: pass # Drink 
                if o.carb_id == 6: pass # Supper
                if o.carb_id == 7: pass # Brunch
                if o.carb_kg: pass # Carbs should be read out
            if o.c3:
                if o.meal_info == 1: flags.append(FLAG_BEFORE_MEAL)
                if o.meal_info == 2: flags.append(FLAG_AFTER_MEAL)
                if o.meal_info == 3: flags.append(FLAG_FASTING)
                if o.meal_info == 4: pass # Casual
                if o.meal_info == 5: flags.append(FLAG_BEFORE_SLEEP)
            if o.c4:
                if o.tester_info == 1: pass # Self test
                if o.tester_info == 2: pass # Professional test
                if o.tester_info == 3: pass # Lab test
                if o.tester_info == 15: pass # Tester not available
                if o.health == 1: pass # Minor health issue
                if o.health == 2: pass # Major health issue
                if o.health == 3: pass # During menses
                if o.health == 4: pass # Under stress
                if o.health == 5: pass # No health issues
                if o.health == 15: pass # Health unknown
            if o.c5: pass # Exercise
            if o.c6:
                if o.c8: pass # Insuline value in kg
                if o.c9: pass # Insuline value in liters
            if o.c7: pass # HbA1c value

            # Create the value to return
            res = {
                'sequence_number'    : o.sequence_number,
                'flags'         : flags
            }
            self.flagList.append(res)

    def EvalGATTProfileGlucoseResultRecord(self,line):
        plx = (io.BytesIO(line[11:]))
        while plx.read(1):
            # Move the pointer back one step, so we can read the byte again
            plx.seek(-1, 1)
            
            # Create placeholder object
            o = type("Dummy", (object,), {})
           
            # Extract the data according to org.bluetooth.characteristic.glucose_measurement
            o.flags           = struct.unpack("B", plx.read(1))[0]
            o.sequence_number = struct.unpack("<H", plx.read(2))[0]
            o.year            = struct.unpack("<H", plx.read(2))[0]
            o.month           = struct.unpack("B", plx.read(1))[0]
            o.day             = struct.unpack("B", plx.read(1))[0]
            o.hour            = struct.unpack("B", plx.read(1))[0]
            o.minute          = struct.unpack("B", plx.read(1))[0]
            o.second          = struct.unpack("B", plx.read(1))[0]

            # The flag field determines how the rest should be read
            o.flag_time_offset_present     = o.flags & 0x01 != 0
            o.flag_gcts_present            = o.flags & 0x02 != 0
            o.flag_gc_units_mol_l          = o.flags & 0x04 != 0
            o.flag_sensor_status_a_present = o.flags & 0x08 != 0
            o.flag_context_info_follows    = o.flags & 0x10 != 0

            # Time offset, in minutes
            if o.flag_time_offset_present:
                o.time_offset_minutes = struct.unpack("<h", plx.read(2))[0]
            else:
                o.time_offset_minutes = 0
                
            # Value
            o.value = U16toSFLOAT(struct.unpack("<H", plx.read(2))[0])
            if math.isnan(o.value):
                o.value = 0
            if o.flag_gc_units_mol_l:
                # Value is stored in mol/l
                o.calc_value = o.value * 1000 * VAL_FACTOR_GLUCOSE_MMOL
                o.calc_unit = "mmol/l"
            else:
                # Value is stored in kg/L
                o.calc_value = o.value * 100000 * VAL_FACTOR_GLUCOSE_MGDL
                o.calc_unit = "mg/dl"
              
                # Adjust value for Generic rounding issue
                o.calc_value = o.calc_value * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL

            # Type
            if o.flag_gcts_present:
                _byteval = struct.unpack("B", plx.read(1))[0]
                o.type = _byteval & 0x0f
                o.sample_location = _byteval & 0xf0 >> 4
                flags = []
                if o.type == 1 : pass # Capillary whole blood
                if o.type == 2 : pass # Capillary plasma
                if o.type == 3 : pass # Venous whole blood
                if o.type == 4 : pass # Venous plasma
                if o.type == 5 : pass # Arterial blood
                if o.type == 6 : pass # Arterial plasma
                if o.type == 7 : pass # Undetermined blood
                if o.type == 8 : pass # Undetermined plasma
                if o.type == 9 : pass # Interestital fluid
                if o.type == 10 : pass # Control solution
                if o.sample_location == 1 : pass # Finger
                if o.sample_location == 2 : pass # Alternate test site
                if o.sample_location == 3 : pass # Earlobe
                if o.sample_location == 4 : pass # Control solution
                if o.sample_location == 15 : pass # Location not available
                res = {
                    'sequence_number'    : o.sequence_number,
                    'flags'         : flags
                }
                self.flagList.append(res)

            # Append any flag info found in value
            if o.flag_sensor_status_a_present:
                flags = []
                o.sensor_status = struct.unpack("<H", plx.read(2))[0]
                if o.sensor_status & 0x0001 != 0 : flags.append(FLAG_LOW_BATTERY)
                if o.sensor_status & 0x0002 != 0 : pass # Sensor malfunction
                if o.sensor_status & 0x0004 != 0 : pass # Insufficient blood size
                if o.sensor_status & 0x0008 != 0 : flags.append(FLAG_STRIP_WARNING)
                if o.sensor_status & 0x0010 != 0 : pass # Wrong strip type
                if o.sensor_status & 0x0020 != 0 : flags.append(FLAG_RESULT_HIGH)
                if o.sensor_status & 0x0040 != 0 : flags.append(FLAG_RESULT_LOW)
                if o.sensor_status & 0x0080 != 0 : flags.append(FLAG_RESULT_OUTSIDE_TEMP)
                if o.sensor_status & 0x0100 != 0 : pass # Read was interrupted
                if o.sensor_status & 0x0200 != 0 : pass # Device fault
                if o.sensor_status & 0x0400 != 0 : pass # Bad date or time value
                res = {
                    'sequence_number'    : o.sequence_number,
                    'flags'         : flags
                }
                self.flagList.append(res)
               
            # Create the finished value
            res = {
                ELEM_TIMESTAMP      : datetime(o.year, o.month, o.day, o.hour, o.minute, o.second) + timedelta(minutes = o.time_offset_minutes),
                ELEM_VAL_TYPE       : VALUE_TYPE_GLUCOSE,
                ELEM_VAL            : o.calc_value,
                ELEM_DEVICE_UNIT    : o.calc_unit,
                ELEM_FLAG_LIST      : [],
                'sequence_number'   : o.sequence_number
            }
            self.valueList.append(res)

    def EvalGATTProfileGlucoseSerialRecord(self,line):
        """
        Evaluate GATT glucose profile serial number.
        """
        m = re.match(r'^TAG!SERIAL!(\d{8})$', line)
        if m:
            self.serial = m.group(1);
        else:
            m = re.match(r'^TAG!SERIAL!([\dA-Z]*)$', line)
        if m:
            self.serial = m.group(1);

    def EvalGATTProfileGlucoseDeviceModelRecord(self,modelTag,manufacturerTag):
        """
        Evaluate GATT glucose profile model.
        """
        serialPrefix = ""

        mod = re.match(r'^TAG!MODEL!([\dA-Za-z ]*)$', modelTag)
        man = re.match(r'^TAG!MANUFACTURER!([\dA-Za-z ]*)$', manufacturerTag)
        if mod:
            if man.group(1) == MANUFACTURER_ROCHE:
                serialPrefix = mod.group(1)
                if serialPrefix in ["483","484","497","498","499","500","502"]:
                    self.model = MODEL_ROCHE_FRIENDLY_NAME_GROUP_1
                elif serialPrefix in ["479","501","503","765"]:
                    self.model = MODEL_ROCHE_FRIENDLY_NAME_GROUP_2
                else:
                    self.model = MODEL_ROCHE_UNKNOWN
            elif man.group(1) == MANUFACTURER_KRONOS:
                if mod.group(1) == MODEL_KRONOS:
                    self.model = MODEL_KRONOS_FRIENDLY_NAME
                else:
                    self.model = MODEL_KRONOS_UNKNOWN
            elif man.group(1) == MANUFACTURER_NIPRO:
                if mod.group(1) == MODEL_NIPRO_TRUE_METRIX_AIR:
                    self.model = MODEL_NIPRO_FRIENDLY_NAME_TRUE_METRIX_AIR
                else:
                    self.model = MODEL_NIPRO_UNKNOWN
            else:
                if mod.group(1) == "GLUCOSE":
                    self.model = "Menarini Glucocard SM Arkray"

            self.combined_values.append({ELEM_DEVICE_SERIAL:serialPrefix+self.serial})
            self.combined_values.append({'meter_type': self.model})

    def eval_serial(self, data):
        return data if ELEM_DEVICE_SERIAL in data else {}

    def eval_result(self, data):
        if ELEM_VAL_TYPE in data or SETTINGS_LIST in data:
            return data
        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectGATTProfileGlucose( inList ):
    """
    Detect if data comes from a GATT Glucose Profile-device
    """
    return DetectDevice( 'GATTProfileGlucose', inList, DEVICE_METER );

def AnalyseGATTProfileGlucose(inData):
    """
    Analyse GATT Glucose Profile-device
    """
    try:
        gatt_profile_analyser = MeterGATTProfileAnalyser()
        gatt_profile_analyser.split(inData)
        callbacks = {
            'meter_type'                : gatt_profile_analyser.model,
            'eval_serial_record'        : gatt_profile_analyser.eval_serial,
            'eval_result_record'        : gatt_profile_analyser.eval_result,
        }

    except Exception, e:

        import traceback
        traceback.print_exc()

        serial_number = gatt_profile_analyser.serial

        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList('Serial',
                                                     serial_number, e.args[0],
                                                     e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList('Serial',
                                                     serial_number,
                                                     ERROR_CODE_PREPROCESSING_FAILED,
                                                     ''.join(map(str, e.args)))

        return error_response

    return AnalyseGenericMeter(gatt_profile_analyser.combined_values, callbacks)

if __name__ == "__main__":
    BASE_PATH = "test/testcases/test_data/GATTProfileGlucose/"
    #testFile = "LOG_DT_2016-05-30_14-30-14_SEQ_0_8B78667B-8182-426E-87A0-C8A067692785.log" #AGAMATRIX WAVESENSE JAZZ
    testFile = "LOG_DT_2016-05-30_14-10-14_SEQ_49_84410C07-F9D1-4A5F-96B3-0EB531465872.log" #Accu-Chek 500
    #testFile = "LOG_DT_2016-05-31_09-21-59_SEQ_1_7722F0AA-9B47-4A43-BC40-C99FEB7B5449.log" #Nipro
    print("\n\n==> Analyzing " + testFile + " <==")
    time.sleep(1)
    testfile = open(BASE_PATH + testFile)
    testcase = testfile.read()
    testfile.close()

    res = AnalyseGATTProfileGlucose(testcase)
    print(res)

