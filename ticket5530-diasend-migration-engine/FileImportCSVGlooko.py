# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Glooko

import re
from Generic import *
from Defines import *
from datetime import *


# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord(line):
    """
    Evaluates the serial record (user device) - comes from the generic Email Import Handler format
    
    """
    return re.match( r'^!TAG!SERNO!(DVU[A-Za-z0-9@._]+)!TAG!BODY!', line, re.IGNORECASE )


def EvalUnitRecord(line):
    """
    Evaluates the unit record
    
    Glucose Unit,mg/dL
    
    """
    return re.match( r'Glucose Unit,(mg/dL|mmol/L)', line, re.IGNORECASE )

def EvalBGResultRecord( line ):
    """
    Evaluate an Glooko CSV file BG result record. Extracted groups : 
    group 1 : date
    group 2 : BG value
    group 3 : Flag
    group 4 : Control Solution

    "Apr 01, 2013 6:25 PM",92,B,NO
    "Apr 01, 2013 3:18 PM",108,B,NO
    "Apr 01, 2013 10:45 AM",111,B,NO
    "Mar 28, 2013 2:25 PM",118,N,NO
    "Mar 19, 2013 10:43 AM",114,A,NO
    "Mar 15, 2013 8:55 AM",118,B,YES 
    "mar 15, 2013 18:55",118,B,YES 
    
    """
    return re.match( r'"(.*)",(\d+),(A|B|N),(NO|YES).*', line, re.IGNORECASE )

def EvalCarbResultRecord( line ):
    """
    Evaluate an Glooko CSV file Carb result record. Extract groups : 
    group 1 : date
    group 2 : comment
    group 3 : carb value
    group 4 : calories
    group 5 : fat

    "Apr 08, 2013 2:52 PM","",30,(null),(null)
    "Apr 03, 2013 10:34 AM","6" Double Meatball Marinara (with Cheese)",82,860,42
    "Apr 02, 2013 9:32 AM","Wheat Bagel, Cream Cheese",58.84,342,6.98
    "Apr 01, 2013 3:18 PM","6" Meatball Sub",59,480,18
    "apr 01, 2013 18:25","6" Meatball Sub",59,480,18
    
    """
    return re.match( r'"(.*)","(.*)",(\d+\.?\d+),(\d+\.?\d+|\(null\)),(\d+\.?\d+|\(null\)).*', line, re.IGNORECASE )
    
def EvalDateTimeField( dtstring ):
    """
    Evaluate a Glooko CSV datetime field, which can come in a number of different flavours:
    
    1. apr 01, 2013 6:25 PM
    2. Apr 01, 2013 18:25
    3. apr 01, 2013 18:25    
    """

    # First, set uppercase on the first character - since the format decode needs uppercase.
    dtstring = dtstring[0].upper()+dtstring[1:]
    # Try the "Apr 08, 2013 2:52 PM" format (covers case 1 above)
    try:
        return datetime.strptime(dtstring, '%b %d, %Y %I:%M %p')
    except:
        pass
        
    # Try the "Apr 08, 2013 14:52" format (covers case 2 and 3 above)
    try:
        return datetime.strptime(dtstring, '%b %d, %Y %H:%M')
    except:
        pass
        
    return False
        
    

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalGlookoSerialRecord( line ):
    """
    """
    res = {}
    m = EvalSerialRecord(line)
    if m:
        res[ELEM_DEVICE_SERIAL] = m.group(1)

    return res

def EvalGlookoUnitRecord( line ):
    """
    """
    res = {}
    res[ELEM_DEVICE_UNIT] = "mg/dL"

    return res
    
def EvalGlookoResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    flags       > list of flags (int) if present
    """
    res = {}
    flags = []
    flags.append(FLAG_ORIGIN_FILE_IMPORT)

    m = EvalBGResultRecord( line )
    if m:
        # This line is a BG record
        if m.group(3) == 'B':
            flags.append(FLAG_BEFORE_MEAL)
        elif m.group(3) == 'A':
            flags.append(FLAG_AFTER_MEAL)
            
        if m.group(4) == 'YES':
            flags.append(FLAG_RESULT_CTRL)
            
        res[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
        res[ELEM_VAL] = float(m.group(2))

    else:
        m = EvalCarbResultRecord( line )
        if m:
            # This line is a carb record
            res[ELEM_VAL_TYPE] = VALUE_TYPE_CARBS
            res[ELEM_VAL] = float(m.group(3))
                
    if res:
        # A valid result has been given - set the timestamp and flags
        dt = EvalDateTimeField(m.group(1))
        if dt:
            res[ELEM_TIMESTAMP] = dt
            res[ELEM_FLAG_LIST] = flags
        else:
            res = { "error_code":ERROR_CODE_DATE_TIME, "line":line, "fault_data":m.group(1) }
        
    return res


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectCSVGlooko( inList ):
    """
    Detect if data comes from a Glooko CSV file.
    """
    return DetectDevice( 'CSVGlooko', inList, DEVICE_USER );

def AnalyseCSVGlooko( inData ):
    """
    Analyse Glooko data
    """

    # Setup the handlers
    d = {}

    d[ "meter_type" ]           = "Glooko CSV"
    d[ "eval_serial_record"]    = EvalGlookoSerialRecord
    d[ "eval_unit"]             = EvalGlookoUnitRecord
    d[ "eval_result_record" ]   = EvalGlookoResultRecord

    # Split up all rows
    inList = inData.split('\r\n')

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    
    #testfile = open('test/testcases/test_data/FileCSVGlooko/glooko.bin')
    #testfile = open('test/testcases/test_data/FileCSVGlooko/glooko-rn-linefeed.log')
    testfile = open('test/testcases/test_data/FileCSVGlooko/glooko-alt-date-format.log')
    
    testcase = testfile.read()
    testfile.close()
    
    print AnalyseCSVGlooko(testcase)
