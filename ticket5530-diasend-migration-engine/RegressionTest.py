# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import getopt
import os
import sys
import requests
import base64

import Config

import test

from test.TestcaseReader import readTestcases
from test.TestcaseRunner import runTestcase

class RegressionTest(object):

    def __init__(self):    
        self.host        = Config.settings["test-address"]
        self.port        = int(Config.settings["test-port"])
        self.path        = os.path.join(sys.path[0], "test/testcases")       # Path where testcases are put
        self.protocol    = 2
        self.testcase    = None
        self.compression = None
        
        self.connector_api = False
        self.cookie        = ""

        self.connector_api_username = Config.settings["connector-username"]
        self.connector_api_password = Config.settings["connector-password"]

    def usage(self):
        print("""All arguments are optional, valid options are:
                -p <port> -h <host> -t <path to testcases> -c <testcase> -r protocol [2|3] -g z-o compression [LZMA|ZLIB]""")

    def cmdline(self, argv):

        try:
            opts, args = getopt.getopt(argv[1:], "p:h:t:r:c:o:g", ["port=", "host=", "testcasepath=", "protocol=", 
                "testcase=","compression=", "glooko"])
        except getopt.GetoptError:
            # print help information and exit:
            self.usage()
            sys.exit(2)

        for o, a in opts:
            if o in ("-p", "--port"):
                self.port = int(a)
            elif o in ("-h", "--host"):
                self.host = a
            elif o in ("-t", "--testcasepath"):
                self.path = a
            elif o in ("-r", "--protocol"):
                self.protocol = int(a)
            elif o in ("-c", "--testcase"):
                self.testcase = a
            elif o in ("-g", "--glooko"):
                self.connector_api = True
            elif o in ("-o", "--compression"):
                if a.upper() in ("LZMA", "ZLIB"):
                    self.compression = a.upper()
                else:
                    print("Invalid compression: ", a)
                    self.usage()
                    sys.exit()
            else:
                print("Invalid argument: ", o)
                self.usage()
                sys.exit()

    def print_result(self, result):
        """
        Formats and prints a report for the specified result
        """
        if "data_file" in result:
            data_file = " ("+result["data_file"]+") "
        else:
            data_file = ""
        
        print(result["testcase"] + data_file + ": " + result["result"])

        if result["result"] != "SUCCESS":
            print("\t" + result["comment"])

    def request_connector_api_cookie(self):

        self.cookie = ""

        payload = {"userLogin": {}, "deviceInformation": {}}

        payload["userLogin"]["email"]    = self.connector_api_username
        payload["userLogin"]["password"] = self.connector_api_password

        payload["deviceInformation"]["applicationType"]    = "kiosk"
        payload["deviceInformation"]["applicationVersion"] = "3.7.0"
        payload["deviceInformation"]["device"]             = "LGE LG-V400"
        payload["deviceInformation"]["deviceId"]           = "220141334314e6d4"
        payload["deviceInformation"]["deviceManufacturer"] = "LGE"
        payload["deviceInformation"]["deviceModel"]        = "LG-V400"
        payload["deviceInformation"]["gitHash"]            = "g26a7557"
        payload["deviceInformation"]["os"]                 = "android"
        payload["deviceInformation"]["osVersion"]          = "5.0.2"
        payload["deviceInformation"]["serialNumber"]       = "LGV4009bd363be"
        payload["deviceInformation"]["buildNumber"]        = "37"

        url = Config.settings["connector-sign_in_url"]

        headers = {"Accept": "application/json"}

        result = requests.post(url, headers=headers, json=payload)

        if result.ok:
            if 'Set-Cookie' in result.headers:
                print repr(result.headers['Set-Cookie'].rstrip())
                self.cookie = base64.b64encode(result.headers['Set-Cookie'].rstrip())
        else:
            return False

        return True

    def execute(self):
        
        # first check command line
        self.cmdline(sys.argv)

        print (self.host, self.port, self.path, self.protocol, self.compression)

        # Now open testcases
        cases = readTestcases(self.path)
        cases.sort()

        print("Found testcase files: ")

        if self.testcase:
            cases = [self.testcase]

        print(cases)
        
        results = []

        if self.connector_api:
            print "Request Connector API cookie"
            if not self.request_connector_api_cookie():
                print "   Failed to request Connector API cookie"
                sys.exit()

        # loop over all cases and run them
        for case in cases:
            
            res = runTestcase(case, self.host, self.port, self.protocol, compression = self.compression, cookie = self.cookie)
            
            if isinstance(res, list):
                results += res
            else:
                results.append(res)

        map(self.print_result, results)

if __name__ == "__main__":
    regression_test = RegressionTest()
    regression_test.execute()
