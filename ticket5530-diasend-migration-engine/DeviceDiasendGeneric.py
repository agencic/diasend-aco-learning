# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2007 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Generic Diasend Protocol Device
# @DEVICE Generic Diasend Protocol Device (Ecap)
# @DEVICE Generic Diasend Protocol Device (NovoPen5)

from Generic import *
from Defines import *
import Defines
from struct import unpack
import datetime
import time

# -----------------------------------------------------------------------------
# Defines
# -----------------------------------------------------------------------------
PEN_FLAG_VALID = 0x01
PEN_FLAG_BASAL = 0x02
PEN_FLAG_BOLUS = 0x04

VID_NOVONORDISK = 0x4E4E

PID_NN_ECAP     = 0xECA9
PID_NN_NovoPen5 = 0x0005

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
def SplitData( inData ):
    """
    Splits incoming data into a list. The data is a mixture of ascii,
    (the header) and binary result records.
    The result records are just fixed length binary chunks
    """
    body = inData
    resultList = []

    # Loop through the body, and check the length of each record, and divide after that
    while len(body) > 6:
        # Check the length from the body package, add 6 for the header, and 2 for the checksum
        blen = ord(body[4]) + 6 
        # Check so there is enough data in the buffer
        if len(body) >= blen:
            resultList.append(body[:blen])
            body = body[blen:]
        else:
            body = []
            
    return resultList

def EvalRecord(line):
    """
    Eval the common parts of a diasend generic record
    """
    d = {}
    
    if (len(line) < 6):
        return None;
        
    # The common header fits, extract it.
    d["record_index"] = unpack("<I", line[:4])[0]
    d["data_len"]     = unpack("B", line[4:5])[0]
    d["record_type"]  = unpack("B", line[5:6])[0]
    if (len(line) < (d["data_len"] + 6)):
        return None

    # Data is there, extract it
    d["record_data"]  = line[6:6+d["data_len"]]

    return d
    
def EvalSerialRecord( line ):
    """
    Main function for validating a diasend generic serial record
    """
    d = {}
    
    m = EvalRecord(line)
    if m == None or m["record_type"] != 0:
        # Failed to eval the generic parts
        return None

    
    _data = m["record_data"]

    if len(_data) < 80:
        return None
    
    d["vendor_id"]    = unpack("<H", _data[:2])[0]
    d["product_id"]   = unpack("<H", _data[2:4])[0]
    d["prot_minor"]   = unpack("<H", _data[4:6])[0]
    d["prot_major"]   = unpack("<H", _data[6:8])[0]
    d["class_of_dev"] = unpack("<I", _data[8:12])[0]
    d["last_written"] = unpack("<I", _data[12:16])[0]
    d["serial"]       = _data[16:80]

    return d
    
def EvalResultRecord( line ):
    """
    Main function for validating a diasend generic result record
    """
    d = {}

    m = EvalRecord(line)
    if m:
        data = m["record_data"]

        d["type"] = m["record_type"]

        if m["record_type"] == 2:
            if len(data) >= 7:
                d["timestamp"] = unpack("<I", data[ :4])[0]
                d["value"]     = unpack("<H", data[4:6])[0]
                d["flags"]     = unpack("B",  data[6:7])[0]
        elif m["record_type"] == 0x81:
            # should probably check if this is a pen cap or not, but for now 0x81 is only defined for pen caps
            if len(data) >= 7:
                d["timestamp"] = unpack("<I", data[ :4])[0]
                d["delta_time"]= unpack("<H", data[4:6])[0]
                d["temp"]      = unpack("B",  data[6:7])[0]

    return d

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalDiasendGenericModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """

    res = {}
    m = EvalSerialRecord( line )
    if m:
        if m["vendor_id"] == VID_NOVONORDISK:
            if m["product_id"] == PID_NN_NovoPen5:
                res[ "device_model" ] = "NovoPen5"
            elif m["product_id"] == PID_NN_ECAP:
                res[ "device_model" ] = "ECap"

    return res


def EvalDiasendGenericClassRecord( line ):
    """
    Is this line a device class record. If so, return a dictionary with device
    class.
    """

    res = {}
    m = EvalSerialRecord( line )
    if m:
        major_class = m["class_of_dev"] & 0xff

        if major_class == 1:
            res[ "device_class" ] = Defines.DEVICE_METER
        elif major_class == 2:
            res[ "device_class" ] = Defines.DEVICE_PUMP
        elif major_class == 3:
            res[ "device_class" ] = Defines.DEVICE_PEN
        elif major_class == 4:
            res[ "device_class" ] = Defines.DEVICE_PEN_CAP

    return res


def EvalDiasendGenericSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        # The string is maximum 64 bytes, or null terminated.
        res[ "meter_serial" ] = m["serial"].split('\0')[0]
    return res
    
def EvalDiasendGenericUnitRecord( line ):
    """
    Always return mg/dl 
    """
    return { "meter_unit":"mg/dL" }

def EvalDiasendGenericResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    res = {}
    m = EvalResultRecord( line )
    if m:
        if m["type"] == 2:
            # So far only pens are using this type, so we need no more extra checks
            if (m["flags"] & PEN_FLAG_VALID):
                now = datetime.datetime.now()

                try:
                    # Time stamp is how long time since now the record was recorded
                    res[ELEM_TIMESTAMP] = datetime.datetime.fromtimestamp(time.mktime(now.timetuple()) - m["timestamp"]) 
                except:
                    return  { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m["timestamp"] }
            
                if (m["flags"] & PEN_FLAG_BASAL):
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS_BASAL
                else:
                    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_INS_BOLUS

                res[ ELEM_VAL ] = float( m["value"] * 1000)
            else:
                return {"null_result":True} 
        elif m["type"] == 0x81:
            # So far only pen caps are using this type so wee need no more extra checks
            
            # A zero Delta-time means no real record (just the current time). 
            # Therefore, don't store it in the database (Ticket p624:#501)  
            if int(m["delta_time"]) == 0:
                return {"null_result":True}

            # A real record...
            res[ ELEM_VAL ] = float( m["delta_time"])
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_PEN_CAP_OFF
            res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : m["temp"], ELEM_VAL_TYPE : VALUE_TYPE_TEMP}]
            # timestamp is seconds since 2000, while the datetime operate in seconds since 1970
            try:
                res[ELEM_TIMESTAMP] = datetime.datetime.fromtimestamp(m["timestamp"] + 946681200)
            except:
                res[ELEM_TIMESTAMP] = datetime.datetime.fromtimestamp(946681200)
        elif m["type"] == 0xfe:
            # This is a debug element, drop it
            return {"null_result":True} 
        
    return res


def EvalDiasendGenericNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        if (m["last_written"] == 0):
            res[ "meter_nr_results" ] = 0
        elif (m["last_written"] - 0x10000) >= 0:
            res[ "meter_nr_results" ] = (m["last_written"] - 0x10000) + 1
        else:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m["last_written"] }
    return res

def EvalDiasendGenericChecksumRecord( line, record ):
    """
    No checksums in the data, since blocks has checksums, just return true
    """
    return True

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectDiasendGeneric( inList ):
    """
    Detect if data comes from a Diasend Generic capable device.
    """
    # Note that we don't know the device type yet, we must analyse the data first...
    return DetectDevice( 'DiasendGeneric', inList, Defines.DEVICE_TOO_EARLY )


def AnalyseDiasendGeneric( inData ):
    """
    Analyse Diasend Generic
    """
    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ] = EvalDiasendGenericModelRecord
    d[ "eval_device_class_record" ] = EvalDiasendGenericClassRecord
    d[ "eval_serial_record" ]       = EvalDiasendGenericSerialRecord
    d[ "eval_unit"]                 = EvalDiasendGenericUnitRecord
    d[ "eval_result_record" ]       = EvalDiasendGenericResultRecord
    d[ "eval_checksum_record" ]     = EvalDiasendGenericChecksumRecord
    d[ "eval_nr_results" ]          = EvalDiasendGenericNrResultsRecord
    
    inList = SplitData (inData)
    
    resList = AnalyseGenericMeter( inList, d );
    
    return resList

# -----------------------------------------------------------------------------
# SOME CODE THAT WILL RUN ON IMPORT
# -----------------------------------------------------------------------------

if __name__ == "__main__":

    body = '\x00\x00\x00\x00P\x00NN\x05\x00\x01\x00\x01\x00\x03\x00\x00\x00\x02\x00\x01\x00Next_NovoPen_#1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x07\x020*\x00\x00d\x00\x03\x01\x00\x01\x00\x07\x02 \x1c\x00\x00d\x00\x05\x02\x00\x01\x00\x07\x02\x10\x0e\x00\x00d\x00\x03'
    #'\x00\x00\x00\x00P\x00NN\x05\x00\x01\x00\x01\x00\x03\x00\x00\x00\x02\x00\x01\x00Next_NovoPen_#1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x07\x02`T\x05\x00d\x00\x03\x01\x00\x01\x00\x07\x02ub\x05\x00d\x00\x05\x02\x00\x01\x00\x07\x02\x8ap\x05\x00d\x00\x03'

    record_idx0 = '\x00\x00\x00\x00P\x00NN\xA9\xEC\x01\x00\x01\x00\x04\x00\x00\x00\x02\x00\x01\x00Serial\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

    # Pen cap specific battery info etc 
    record_class_specific = '\x00\x10\x00\x00\x06\x80\x01\x00\x00\x00\x01\x00'

    record1 = '\x00\x00\x01\x00\x07\x81\x00\x00\x00\x00\x01\x00\x02'
    record2 = '\x00\x00\x01\x00\x07\x81\x03\x00\x00\x00\x04\x00\x05'
    record3 = '\x00\x00\x01\x00\x07\x81\x06\x00\x00\x00\x07\x00\x08'

    body = record_idx0 + record_class_specific + record1 + record2 + record3

    body = "\x00\x00\x00\x00P\x00NN\xa9\xec\x01\x00\x01\x00\x04\x00\x00\x00\x03\x00\x01\x00NN-12346\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x06\x80\xbe\x0b+\x0f\x01\x00\x00\x00\x01\x00\x07\x81\x9b4'\x0f\x01\x00\xff\x01\x00\x01\x00\x07\x81\x9e4'\x0f\x01\x00\xff\x02\x00\x01\x00\x07\x81\xa14'\x0f\x02\x00\xff\x03\x00\x01\x00\x07\x81\xa44'\x0f\x02\x00\xff"
    res = AnalyseDiasendGeneric(body)
    print res

