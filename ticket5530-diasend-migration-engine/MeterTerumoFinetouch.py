# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Terumo FineTouch

import re
from Generic import *
from Defines import *
from datetime import datetime
from datetime import time
import re

# -----------------------------------------------------------------------------
# DEFINES 
# -----------------------------------------------------------------------------

glob_current_year = None

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------


def SplitData( inData ):
    """
    Splits incoming data into a list. The data is ASCII (with some unicode Chinese characters)
    """
    
    # Split on LF characters and remove empty lines and start/end messages
    return filter(lambda word: word not in ["#MSD_START", "#MSD_END", ""], inData.split('\n'))
    
# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Finetouch Serial record. Extracted elements: 
    serial
    """
    m = re.match( r'#(\d{10})', line, re.IGNORECASE)
    return m
    
def EvalYearRecord( line ):
    """
    Evaluate a Finetouch Year record. Extracted elements: 
    year
    """
    m = re.match( r'(\d{4})\x94N', line, re.IGNORECASE)
    return m
    
def EvalHistoryRecord( line ):
    """
    Evaluate a Finetouch Result Record. Extracted elements
    month, day, hour, minute, glucose value, extra data
    """
   
    m = re.match( r'\t([\d ]{2})\x8c\x8e([\d ]{2})\x93\xfa([\d ]{2})\x8e\x9e([\d ]{2})\x95\xaa ([\d ]{3}| Lo| Hi)mg/dl(.*)', line, re.IGNORECASE)
    return m


# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalFinetouchSerialRecord( line ):
    """
    Is this line a serial record? If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(1)

    return res
    

def EvalFinetouchDeviceModelRecord( line ):
    """
    The device model is fixed
    """
    res = { "device_model":"Terumo FineTouch" }
    return res

def EvalFinetouchUnitRecord( line ):
    """
    The unit is fixed
    """
    res = { "meter_unit":"mg/dl" }
    return res

def EvalFinetouchResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys
    """
    res = {}

    m = EvalYearRecord(line)
    if m:
        global glob_current_year
        glob_current_year = int(m.group(1))
        
    m = EvalHistoryRecord(line)
    if m:
        res[ ELEM_TIMESTAMP ] = datetime(glob_current_year, int(m.group(1)), int(m.group(2)), int(m.group(3)), int(m.group(4)), 0)
        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        flags = []
        res[ ELEM_FLAG_LIST ] = flags

        if m.group(5) == " Hi":
            flags.append(FLAG_RESULT_HIGH)
            value = 0
        elif m.group(5) == " Lo":
            flags.append(FLAG_RESULT_LOW)
            value = 0
        else:
            value = int(m.group(5))
            
        if m.group(6):
            # An extra flag value. Should be " T"(+possibly random bytes) or " C"
            if len(m.group(6)) >= 2 and m.group(6)[1] == "T":
                flags.append(FLAG_RESULT_OUTSIDE_TEMP)
            elif len(m.group(6)) == 2 and m.group(6)[1] == "C":
                flags.append(FLAG_RESULT_CTRL)
            else:
                # Unknown flag data
                return { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(6) }

        res[ ELEM_VAL ] = value
        
    return res
    

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectTerumoFinetouch( inList ):
    """
    Detect if data comes from an Terumo Finetouch. 
    """
    return DetectDevice( "TerumoFinetouch", inList, DEVICE_METER )

def AnalyseTerumoFinetouch( inData ):
    """
    Analyse Terumo Finetouch meter
    """
    inList = SplitData (inData)
    
    global glob_current_year
    glob_current_year = None
    
    # Eval dict
    d = { \
        "eval_serial_record" : EvalFinetouchSerialRecord, \
        "eval_device_model_record" : EvalFinetouchDeviceModelRecord, \
        "eval_unit" : EvalFinetouchUnitRecord, \
        "eval_result_record" : EvalFinetouchResultRecord
    }

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    # 060705-1777: 2678
    data = '\n#MSD_START\n\n#0607051777\n2007\x94N\n\t 3\x8c\x8e 8\x93\xfa12\x8e\x9e56\x95\xaa  86mg/dl\n\t 3\x8c\x8e13\x93\xfa15\x8e\x9e 7\x95\xaa 124mg/dl\n\t 3\x8c\x8e15\x93\xfa10\x8e\x9e47\x95\xaa  86mg/dl\n\t 3\x8c\x8e27\x93\xfa 8\x8e\x9e36\x95\xaa  94mg/dl\n\t 4\x8c\x8e24\x93\xfa 9\x8e\x9e51\x95\xaa  86mg/dl\n\t 4\x8c\x8e26\x93\xfa20\x8e\x9e54\x95\xaa  88mg/dl\n\t 5\x8c\x8e 2\x93\xfa12\x8e\x9e34\x95\xaa 117mg/dl\n\t 5\x8c\x8e 4\x93\xfa10\x8e\x9e34\x95\xaa  98mg/dl\n\t 5\x8c\x8e 4\x93\xfa10\x8e\x9e40\x95\xaa 113mg/dl\n\t 5\x8c\x8e 7\x93\xfa20\x8e\x9e51\x95\xaa 159mg/dl\n\t 5\x8c\x8e 7\x93\xfa21\x8e\x9e10\x95\xaa 138mg/dl\n\t 5\x8c\x8e 7\x93\xfa21\x8e\x9e11\x95\xaa 133mg/dl\n\t 5\x8c\x8e 8\x93\xfa 6\x8e\x9e52\x95\xaa 108mg/dl\n\t 5\x8c\x8e 8\x93\xfa17\x8e\x9e19\x95\xaa  87mg/dl\n\t 5\x8c\x8e 8\x93\xfa23\x8e\x9e19\x95\xaa 140mg/dl\n\t 5\x8c\x8e17\x93\xfa10\x8e\x9e46\x95\xaa 107mg/dl\n\t 5\x8c\x8e17\x93\xfa10\x8e\x9e53\x95\xaa 133mg/dl\n\t 5\x8c\x8e17\x93\xfa10\x8e\x9e56\x95\xaa  87mg/dl\n\t 5\x8c\x8e17\x93\xfa11\x8e\x9e27\x95\xaa 100mg/dl\n\t 5\x8c\x8e17\x93\xfa11\x8e\x9e29\x95\xaa 124mg/dl\n\t 5\x8c\x8e17\x93\xfa12\x8e\x9e 0\x95\xaa 126mg/dl\n\t 5\x8c\x8e17\x93\xfa14\x8e\x9e20\x95\xaa  94mg/dl\n\t 5\x8c\x8e17\x93\xfa14\x8e\x9e23\x95\xaa  86mg/dl\n\t 5\x8c\x8e17\x93\xfa15\x8e\x9e35\x95\xaa 188mg/dl\n\t 5\x8c\x8e17\x93\xfa15\x8e\x9e40\x95\xaa 133mg/dl\n\t 5\x8c\x8e18\x93\xfa10\x8e\x9e11\x95\xaa 100mg/dl\n\t 5\x8c\x8e18\x93\xfa13\x8e\x9e 3\x95\xaa 157mg/dl\n\t 5\x8c\x8e18\x93\xfa13\x8e\x9e27\x95\xaa 150mg/dl\n\t 5\x8c\x8e18\x93\xfa13\x8e\x9e34\x95\xaa 169mg/dl\n\t 5\x8c\x8e18\x93\xfa14\x8e\x9e34\x95\xaa 107mg/dl\n\t 5\x8c\x8e18\x93\xfa16\x8e\x9e 9\x95\xaa 140mg/dl\n\t 5\x8c\x8e31\x93\xfa15\x8e\x9e50\x95\xaa  89mg/dl\n\t 5\x8c\x8e31\x93\xfa15\x8e\x9e57\x95\xaa  96mg/dl\n\t 5\x8c\x8e31\x93\xfa16\x8e\x9e 2\x95\xaa  87mg/dl\n\t 6\x8c\x8e 1\x93\xfa 6\x8e\x9e37\x95\xaa  91mg/dl\n\t 6\x8c\x8e 2\x93\xfa10\x8e\x9e20\x95\xaa 113mg/dl\n\t 6\x8c\x8e 2\x93\xfa10\x8e\x9e38\x95\xaa 142mg/dl\n\t 6\x8c\x8e 2\x93\xfa10\x8e\x9e41\x95\xaa  96mg/dl\n\t 6\x8c\x8e 3\x93\xfa11\x8e\x9e30\x95\xaa  81mg/dl\n\t 6\x8c\x8e 4\x93\xfa 6\x8e\x9e27\x95\xaa  91mg/dl\n\t 6\x8c\x8e29\x93\xfa13\x8e\x9e47\x95\xaa  93mg/dl\n\t 7\x8c\x8e 3\x93\xfa16\x8e\x9e 2\x95\xaa  73mg/dl\n\t 7\x8c\x8e12\x93\xfa 9\x8e\x9e24\x95\xaa 103mg/dl\n\t 7\x8c\x8e18\x93\xfa14\x8e\x9e 7\x95\xaa 105mg/dl\n\t 8\x8c\x8e 2\x93\xfa15\x8e\x9e11\x95\xaa 126mg/dl\n\t 8\x8c\x8e 2\x93\xfa15\x8e\x9e20\x95\xaa 105mg/dl\n\t 8\x8c\x8e 7\x93\xfa11\x8e\x9e52\x95\xaa 136mg/dl\n\t 8\x8c\x8e10\x93\xfa 8\x8e\x9e34\x95\xaa  95mg/dl\n\t 8\x8c\x8e20\x93\xfa13\x8e\x9e32\x95\xaa  93mg/dl\n\t 9\x8c\x8e10\x93\xfa13\x8e\x9e12\x95\xaa 109mg/dl\n\t 9\x8c\x8e13\x93\xfa14\x8e\x9e44\x95\xaa 136mg/dl\n\t10\x8c\x8e13\x93\xfa 9\x8e\x9e49\x95\xaa 115mg/dl\n\t10\x8c\x8e13\x93\xfa10\x8e\x9e 5\x95\xaa  82mg/dl\n\t10\x8c\x8e13\x93\xfa13\x8e\x9e27\x95\xaa 146mg/dl\n\t10\x8c\x8e13\x93\xfa13\x8e\x9e35\x95\xaa 130mg/dl\n\t10\x8c\x8e13\x93\xfa13\x8e\x9e44\x95\xaa 102mg/dl\n\t10\x8c\x8e13\x93\xfa13\x8e\x9e48\x95\xaa 195mg/dl\n\t10\x8c\x8e13\x93\xfa14\x8e\x9e22\x95\xaa 167mg/dl\n\t11\x8c\x8e 1\x93\xfa13\x8e\x9e48\x95\xaa 303mg/dl\n\t11\x8c\x8e 1\x93\xfa13\x8e\x9e51\x95\xaa 304mg/dl\n\t11\x8c\x8e 9\x93\xfa12\x8e\x9e25\x95\xaa 121mg/dl\n\t11\x8c\x8e20\x93\xfa10\x8e\x9e 8\x95\xaa  Lomg/dl\n2008\x94N\n\t 1\x8c\x8e 7\x93\xfa14\x8e\x9e54\x95\xaa  81mg/dl\n\t 1\x8c\x8e 7\x93\xfa14\x8e\x9e55\x95\xaa  87mg/dl\n\t 1\x8c\x8e 9\x93\xfa 9\x8e\x9e59\x95\xaa  97mg/dl\n\t 1\x8c\x8e 9\x93\xfa10\x8e\x9e39\x95\xaa 116mg/dl\n\t 1\x8c\x8e22\x93\xfa17\x8e\x9e49\x95\xaa  89mg/dl\n\t 1\x8c\x8e25\x93\xfa17\x8e\x9e 0\x95\xaa 139mg/dl\n\t 1\x8c\x8e25\x93\xfa17\x8e\x9e 1\x95\xaa 102mg/dl\n\t 2\x8c\x8e 5\x93\xfa11\x8e\x9e28\x95\xaa  89mg/dl\n\t 3\x8c\x8e14\x93\xfa14\x8e\x9e 8\x95\xaa  95mg/dl\n\t 8\x8c\x8e18\x93\xfa12\x8e\x9e22\x95\xaa  80mg/dl\n\t10\x8c\x8e20\x93\xfa12\x8e\x9e22\x95\xaa 125mg/dl\n\t11\x8c\x8e11\x93\xfa19\x8e\x9e14\x95\xaa 101mg/dl\n\t11\x8c\x8e11\x93\xfa18\x8e\x9e23\x95\xaa 103mg/dl\n\t11\x8c\x8e14\x93\xfa17\x8e\x9e15\x95\xaa 123mg/dl\n2009\x94N\n\t 1\x8c\x8e23\x93\xfa15\x8e\x9e30\x95\xaa  74mg/dl\n\t 1\x8c\x8e29\x93\xfa10\x8e\x9e57\x95\xaa 135mg/dl\n\t 1\x8c\x8e29\x93\xfa12\x8e\x9e55\x95\xaa 106mg/dl\n\t 2\x8c\x8e 5\x93\xfa14\x8e\x9e 9\x95\xaa 101mg/dl\n\t 2\x8c\x8e17\x93\xfa11\x8e\x9e59\x95\xaa  86mg/dl\n\t 2\x8c\x8e17\x93\xfa14\x8e\x9e10\x95\xaa  95mg/dl\n\t 2\x8c\x8e27\x93\xfa14\x8e\x9e12\x95\xaa 101mg/dl\n\t 3\x8c\x8e12\x93\xfa14\x8e\x9e 9\x95\xaa  91mg/dl\n\t 3\x8c\x8e17\x93\xfa13\x8e\x9e23\x95\xaa 103mg/dl\n\t 3\x8c\x8e30\x93\xfa11\x8e\x9e29\x95\xaa 113mg/dl\n\t 3\x8c\x8e30\x93\xfa12\x8e\x9e16\x95\xaa  92mg/dl\n\t 5\x8c\x8e11\x93\xfa14\x8e\x9e21\x95\xaa 103mg/dl\n\t 5\x8c\x8e13\x93\xfa 9\x8e\x9e56\x95\xaa  81mg/dl\n\t 7\x8c\x8e 7\x93\xfa15\x8e\x9e29\x95\xaa  95mg/dl\n\t 7\x8c\x8e17\x93\xfa13\x8e\x9e47\x95\xaa  97mg/dl\n\t 8\x8c\x8e13\x93\xfa16\x8e\x9e 0\x95\xaa  75mg/dl\n\t 8\x8c\x8e20\x93\xfa14\x8e\x9e35\x95\xaa  86mg/dl\n\t11\x8c\x8e24\x93\xfa13\x8e\x9e 3\x95\xaa  77mg/dl\n\t12\x8c\x8e10\x93\xfa13\x8e\x9e10\x95\xaa  82mg/dl\n\t12\x8c\x8e18\x93\xfa14\x8e\x9e 3\x95\xaa  Himg/dl\n\t 1\x8c\x8e18\x93\xfa14\x8e\x9e 3\x95\xaa  Himg/dl\n\n\n\n\n#MSD_END\n'
    
    # With Temp
    data2 = '\n#MSD_START\n\n#0803135660\n2009\x94N\n\t 5\x8c\x8e 9\x93\xfa11\x8e\x9e 2\x95\xaa 108mg/dl\n\t 8\x8c\x8e19\x93\xfa12\x8e\x9e31\x95\xaa  96mg/dl\n\t 9\x8c\x8e22\x93\xfa11\x8e\x9e 9\x95\xaa  72mg/dl\n\t 9\x8c\x8e29\x93\xfa11\x8e\x9e39\x95\xaa  82mg/dl\n\t10\x8c\x8e14\x93\xfa13\x8e\x9e17\x95\xaa  99mg/dl\n\t10\x8c\x8e20\x93\xfa 8\x8e\x9e19\x95\xaa  68mg/dl\n\t10\x8c\x8e20\x93\xfa 8\x8e\x9e20\x95\xaa  66mg/dl\n\t10\x8c\x8e21\x93\xfa12\x8e\x9e21\x95\xaa 109mg/dl\n\t10\x8c\x8e21\x93\xfa12\x8e\x9e22\x95\xaa  67mg/dl\n\t10\x8c\x8e27\x93\xfa12\x8e\x9e47\x95\xaa  49mg/dl\n\t11\x8c\x8e 6\x93\xfa10\x8e\x9e32\x95\xaa  79mg/dl\n\t11\x8c\x8e11\x93\xfa14\x8e\x9e12\x95\xaa 129mg/dl\n\t11\x8c\x8e11\x93\xfa14\x8e\x9e26\x95\xaa  98mg/dl\n2008\x94N\n\t11\x8c\x8e25\x93\xfa17\x8e\x9e54\x95\xaa  80mg/dl\n2009\x94N\n\t10\x8c\x8e25\x93\xfa18\x8e\x9e 3\x95\xaa  89mg/dl\n\t10\x8c\x8e25\x93\xfa18\x8e\x9e11\x95\xaa  Lomg/dl\n\t10\x8c\x8e25\x93\xfa18\x8e\x9e19\x95\xaa  80mg/dl\n\t10\x8c\x8e25\x93\xfa18\x8e\x9e27\x95\xaa  95mg/dl\n2010\x94N\n\t 1\x8c\x8e 1\x93\xfa17\x8e\x9e39\x95\xaa  86mg/dl\n\t 1\x8c\x8e 1\x93\xfa19\x8e\x9e 9\x95\xaa  89mg/dl\n\t 3\x8c\x8e12\x93\xfa 1\x8e\x9e26\x95\xaa 157mg/dl\n\t 3\x8c\x8e12\x93\xfa 1\x8e\x9e42\x95\xaa 310mg/dl T\n\t 3\x8c\x8e12\x93\xfa 1\x8e\x9e44\x95\xaa 204mg/dl T\n\t 3\x8c\x8e12\x93\xfa 1\x8e\x9e45\x95\xaa 273mg/dl\n\n\n\n\n#MSD_END\n'
    
    res = AnalyseTerumoFinetouch(data2)
    print res
