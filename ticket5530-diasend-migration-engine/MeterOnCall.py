# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Acon On Call Plus
# @DEVICE Acon On Call Advanced
# @DEVICE Acon On Call Resolve
# @DEVICE Acon On Call Vivid


import re
from datetime import datetime
import crcmod 

from Generic import *
from Defines import *
import Common

ON_CALL_PLUS_DEVICE_NAME        = "On Call Plus"
ON_CALL_ADVANCED_DEVICE_NAME    = "On Call Advanced"
ON_CALL_RESOLVE_DEVICE_NAME     = "On Call Resolve/Vivid"

ON_CALL_RECORD_TYPE_MEASUREMENT         = "measurement"
ON_CALL_RECORD_TYPE_SERIALNUMBER        = "serialnumber"
ON_CALL_RECORD_TYPE_NUMBER_OF_RESULT    = "nr_of_result"

# It seems the CRC is wrong for the "No result" record on On Call Plus
# At least two meters have reported "96" instead of the calculated "18442"
ON_CALL_PLUS_IGNORE_NO_RESULT_CHECKSUM = True

# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS 
# -----------------------------------------------------------------------------

def ConvertGlucoseValue(device_name, dt, value, mark):
    """
    Add glucose value to collection. Will return null_result if necessary
    (but will always return a record).

    @param device_name
        Name of device (e.g. ON_CALL_ADVANCED_DEVICE_NAME).

    @param dt
        String with date and time month(2)day(2)year(2)hour(2)minute(2)

    @param value
        String with measurement (todo mg or mmol?)

    @param mark
        String with flags

        bit 0 -> After meal mark
        bit 1 -> Before meal mark
        bit 2 -> Number mark
        bit 3 -> Control mark

    @return dictionary ->
            {
                "record_type":ON_CALL_RECORD_TYPE_MEASUREMENT,
                "record_value":[datetime, value, [flags]]
                "record_checksum_ok":True
            }
    """
    result = {}

    result["record_type"] = ON_CALL_RECORD_TYPE_MEASUREMENT
    result["record_value"] = []

    year    = 2000+int(dt[4:6])
    month   = int(dt[0:2])
    day     = int(dt[2:4])
    hour    = int(dt[6:8])
    minute  = int(dt[8:10])

    result["record_value"].append(datetime(year, month, day, hour, minute))
    result["record_value"].append(int(value))

    flags = []
    if int(mark) & 0x01:
        flags.append(FLAG_AFTER_MEAL)
    if int(mark) & 0x02:
        flags.append(FLAG_BEFORE_MEAL)
    if int(mark) & 0x08:
        flags.append(FLAG_RESULT_CTRL)
    elif int(mark) & 0x04:
        # Special case - if number mark is set but not control 
        # the user has flagged this value as invalid. So we do not
        # want to add it to the database. 
        return {"null_result":True}

    if device_name in (ON_CALL_ADVANCED_DEVICE_NAME, ON_CALL_RESOLVE_DEVICE_NAME):
        if int(value) <= 9:
            flags.append(FLAG_RESULT_LOW)
        if int(value) >= 601:
            flags.append(FLAG_RESULT_HIGH)
    else: # Plus
        if int(value) <= 20:
            flags.append(FLAG_RESULT_LOW)
        if int(value) >= 600:
            flags.append(FLAG_RESULT_HIGH)

    result["record_value"].append(flags)

    # Checksum is calculated on number of results _and_ results.
    # Add the actual result to the number of results record; the 
    # measurements always says "Yes I am ok". 
    result["record_checksum_ok"] = True

    return result

def ConvertOnCallDataToList(in_data):
    """
    Split (and convert) data from an OnCall. 

    Data from an OnCall comes in two chunks separated by \r. The first chunk
    holds the serial number and the second chunks holds number of results
    and all results. This function converts those chunks into a list. Each
    item in the list holds a serial number, number of results or a single 
    measurement.

    Item (dictionary)
        "record_type"   ON_CALL_RECORD_TYPE_SERIALNUMBER
                        ON_CALL_RECORD_TYPE_NUMBER_OF_RESULT
                        ON_CALL_RECORD_TYPE_MEASUREMENT
        "record_value"  Value of record in a format that is expected
                        by Generic.py. 
        "record_checksum_ok"    Is checksum ok (True) or not (False).

    This function also performs checksum control. If an incorrect
    checksum is discovered an error item is inserted into the list
    (and should be picked up by Generic.py).

    Returns list of items _and_ model name. 
    """

    results = []
    lines = in_data.split('\r')

    crc_func = crcmod.mkCrcFun(0x18005, 0xffff, True)

    # This will affect the HI/LO limits if we for some reason don't get the 
    # serial record first. 
    device_name = ON_CALL_PLUS_DEVICE_NAME

    for line in lines:
        m = EvalSerialRecord(line)
        if m != None:
            result = {}
            result["record_type"] = ON_CALL_RECORD_TYPE_SERIALNUMBER
            result["record_value"] = m.group(3)
            result["record_checksum_ok"] = crc_func(m.group(1)) == int(m.group(4)) 
            results.append(result)
            if m.group(2) == 'D':
                device_name = ON_CALL_ADVANCED_DEVICE_NAME 
            elif m.group(2) == 'G':
                device_name = ON_CALL_RESOLVE_DEVICE_NAME
            elif m.group(2) == 'A':
                device_name = ON_CALL_PLUS_DEVICE_NAME
            continue

        m = EvalNoResultRecord(line)
        if m != None:
            result = {}
            result["record_type"] = ON_CALL_RECORD_TYPE_NUMBER_OF_RESULT
            result["record_value"] = 0
            result["record_checksum_ok"] = crc_func(m.group(1)) == int(m.group(2))
            if (device_name == ON_CALL_PLUS_DEVICE_NAME) and ON_CALL_PLUS_IGNORE_NO_RESULT_CHECKSUM:
                result["record_checksum_ok"] = True
            results.append(result)
            continue

        m = EvalResultRecord(line)
        if m != None:
            result = {}
            result["record_type"] = ON_CALL_RECORD_TYPE_NUMBER_OF_RESULT
            result["record_value"] = int(m.group(3))
            result["record_checksum_ok"] = crc_func(m.group(1)) == int(m.group(5)) 
            results.append(result)
            if m.group(2) in ['D', 'G']:
                records = re.findall(r'\x1e(\d{10,10}) (\d+) (\d+) (\d+) (\d+) (\d+) ', m.group(4), re.IGNORECASE)
                for r in records:
                    results.append(ConvertGlucoseValue(device_name, r[0], r[1], r[2]))
            else:
                records = re.findall(r'\x1e(\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) ', m.group(4), re.IGNORECASE)
                for r in records:
                    # Convert the date to the same format as Advanced/Resolve. 
                    results.append(ConvertGlucoseValue(device_name, "%02d%02d%02d%02d%02d" % (int(r[0]), int(r[1]), int(r[2]), int(r[3]), int(r[4])), r[5], r[6]))

    return results, device_name 

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord(line):
    """
    Check (and extract) serial number of meter. 

    Example : &DB 404A0004128 \x0625940\r

    Extracted parts : 

    0   String matched
    1   String for CRC match
    2   Protocol
    3   Serial number
    4   Checksum of record
    """
    m = re.match(r'(&([DGA])B ([^ ]+) )\x06(\d+)', line, re.IGNORECASE)
    return m

def EvalNoResultRecord(line):
    """
    &DZ y \x0618630

    Extracted parts : 

    0   String matched
    1   String for CRC match
    2   Checksum of record
    """
    m = re.match(r'(&[DGA]Z y )\x06(\d+)', line, re.IGNORECASE)
    return m

def EvalResultRecord(line):
    """
    &DZ 150 \x1e0531100700 601 0 0 0 0 ... 0 0 0 \x1e\x0610110\r

    0   String matched
    1   String for CRC match
    2   Protocol
    3   Number of result
    4   Results
    5   Checksum for record
    """
    m = re.match(r'(&([DGA])Z (\d+) (.*)\x1e)\x06(\d+)', line, re.IGNORECASE)
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalOnCallUnitRecord( line ):
    """
    Always return mg/dl 
    """
    return { "meter_unit":"mg/dL" }

def EvalOnCallChecksumFile(records):
    """
    Check for checksum error by looping all records created by 
    ConvertOnCallDataToList looking for the result stored in 
    the record_checksum_ok key.
    """
    for record in records:
        if not record.has_key("null_result") and not record["record_checksum_ok"]:
            return False
    return True

def EvalOnCallSerialRecord(line):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}

    if line["record_type"] == ON_CALL_RECORD_TYPE_SERIALNUMBER:
        res["meter_serial"] = line["record_value"]

    return res

def EvalOnCallNrOfResultRecord(line):
    """
    Is this a number of results record? If so, return a dictionary with 
    the number of results. 
    """
    res = {}
    if line["record_type"] == ON_CALL_RECORD_TYPE_NUMBER_OF_RESULT:
        res["meter_nr_results"] = line["record_value"]

    return res

def EvalOnCallResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    res = {}

    if line.has_key("null_result"):
        return line

    if line["record_type"] == ON_CALL_RECORD_TYPE_MEASUREMENT:
        res[ELEM_TIMESTAMP]     = line["record_value"][0]
        res[ELEM_VAL]           = line["record_value"][1]
        res[ELEM_FLAG_LIST]     = line["record_value"][2]
        res[ELEM_VAL_TYPE]      = VALUE_TYPE_GLUCOSE

    return res

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectOnCall( inList ):
    """
    Detect if data comes from a On Call device.
    """
    return DetectDevice( 'OnCall', inList, DEVICE_METER )

def AnalyseOnCall( inData ):
    """
    Analyse On Call. 
    """

    inList, model = ConvertOnCallDataToList(inData)

    # Empty dictionary
    d = {}

    d[ "meter_type" ]               = model
    d[ "eval_serial_record" ]       = EvalOnCallSerialRecord
    d[ "eval_result_record" ]       = EvalOnCallResultRecord
    d[ "eval_nr_results" ]          = EvalOnCallNrOfResultRecord
    d[ "eval_unit" ]                = EvalOnCallUnitRecord
    d[ "eval_checksum_file" ]       = EvalOnCallChecksumFile

    resList = AnalyseGenericMeter( inList, d );

    return resList

if __name__ == "__main__":

    testfile = open('test/testcases/test_data/OnCall/OnCallAdvanced-404A000B718-22byte_length_meter_id.bin')
    #testfile = open('test/testcases/test_data/GlucoMenLXPLUS/ep054.bin')


    testcase = testfile.read()
    testfile.close()

    print AnalyseOnCall(testcase)

"""

    test_data = (("Full (300)", 0, 300, "&DB 404A0004128 \x0625940\r&DZ 300 \x1e0101100030 601 0 0 0 0 \x1e0102100030 601 0 0 0 0 \x1e0103100030 601 0 0 0 0 \x1e0104100030 601 0 0 0 0 \x1e0105100030 601 0 0 0 0 \x1e0106100030 601 0 0 0 0 \x1e0107100130 601 0 0 0 0 \x1e0108100130 601 0 0 0 0 \x1e0109100130 601 0 0 0 0 \x1e0110100130 601 0 0 0 0 \x1e0111100130 19 0 0 0 0 \x1e0112100130 19 0 0 0 0 \x1e0113100230 19 0 0 0 0 \x1e0114100230 19 0 0 0 0 \x1e0115100230 19 0 0 0 0 \x1e0116100230 19 0 0 0 0 \x1e0117100230 19 0 0 0 0 \x1e0118100230 19 0 0 0 0 \x1e0119100330 19 0 0 0 0 \x1e0120100330 19 0 0 0 0 \x1e0121100330 80 0 0 0 0 \x1e0122100330 81 0 0 0 0 \x1e0123100330 82 0 0 0 0 \x1e0124100330 83 0 0 0 0 \x1e0125100430 84 0 0 0 0 \x1e0126100430 85 0 0 0 0 \x1e0127100430 86 0 0 0 0 \x1e0128100430 87 0 0 0 0 \x1e0129100430 88 0 0 0 0 \x1e0130100430 89 0 0 0 0 \x1e0131100530 90 0 0 0 0 \x1e0201100530 91 0 0 0 0 \x1e0202100530 92 0 0 0 0 \x1e0203100530 93 0 0 0 0 \x1e0204100530 94 0 0 0 0 \x1e0205100530 95 0 0 0 0 \x1e0206100630 96 0 0 0 0 \x1e0207100630 97 0 0 0 0 \x1e0208100630 98 0 0 0 0 \x1e0209100630 99 0 0 0 0 \x1e0210100630 100 0 0 0 0 \x1e0211100630 101 0 0 0 0 \x1e0212100730 102 0 0 0 0 \x1e0213100730 103 0 0 0 0 \x1e0214100730 104 0 0 0 0 \x1e0215100730 105 0 0 0 0 \x1e0216100730 106 0 0 0 0 \x1e0217100730 107 0 0 0 0 \x1e0218100830 108 0 0 0 0 \x1e0219100830 109 0 0 0 0 \x1e0220100830 110 0 0 0 0 \x1e0221100830 111 0 0 0 0 \x1e0222100830 112 0 0 0 0 \x1e0223100830 113 0 0 0 0 \x1e0224100930 114 0 0 0 0 \x1e0225100930 115 0 0 0 0 \x1e0226100930 116 0 0 0 0 \x1e0227100930 117 0 0 0 0 \x1e0228100930 118 0 0 0 0 \x1e0301100930 119 0 0 0 0 \x1e0302101030 120 0 0 0 0 \x1e0303101030 121 0 0 0 0 \x1e0304101030 122 0 0 0 0 \x1e0305101030 123 0 0 0 0 \x1e0306101030 124 0 0 0 0 \x1e0307101030 125 0 0 0 0 \x1e0308101130 126 0 0 0 0 \x1e0309101130 127 0 0 0 0 \x1e0310101130 128 0 0 0 0 \x1e0311101130 129 0 0 0 0 \x1e0312101130 130 0 0 0 0 \x1e0313101130 131 0 0 0 0 \x1e0314101230 132 0 0 0 0 \x1e0315101230 133 0 0 0 0 \x1e0316101230 134 0 0 0 0 \x1e0317101230 135 0 0 0 0 \x1e0318101230 136 0 0 0 0 \x1e0319101230 137 0 0 0 0 \x1e0320101330 138 0 0 0 0 \x1e0321101330 139 0 0 0 0 \x1e0322101330 140 0 0 0 0 \x1e0323101330 141 0 0 0 0 \x1e0324101330 142 0 0 0 0 \x1e0325101330 143 0 0 0 0 \x1e0326101430 144 0 0 0 0 \x1e0327101430 145 0 0 0 0 \x1e0328101430 146 0 0 0 0 \x1e0329101430 147 0 0 0 0 \x1e0330101430 148 0 0 0 0 \x1e0331101430 149 0 0 0 0 \x1e0401101530 150 0 0 0 0 \x1e0402101530 151 0 0 0 0 \x1e0403101530 152 0 0 0 0 \x1e0404101530 153 0 0 0 0 \x1e0405101530 154 0 0 0 0 \x1e0406101530 155 0 0 0 0 \x1e0407101630 156 0 0 0 0 \x1e0408101630 157 0 0 0 0 \x1e0409101630 158 0 0 0 0 \x1e0410101630 159 0 0 0 0 \x1e0411101630 160 0 0 0 0 \x1e0412101630 161 0 0 0 0 \x1e0413101730 162 0 0 0 0 \x1e0414101730 163 0 0 0 0 \x1e0415101730 164 0 0 0 0 \x1e0416101730 165 0 0 0 0 \x1e0417101730 166 0 0 0 0 \x1e0418101730 167 0 0 0 0 \x1e0419101830 168 0 0 0 0 \x1e0420101830 169 0 0 0 0 \x1e0421101830 170 0 0 0 0 \x1e0422101830 171 0 0 0 0 \x1e0423101830 172 0 0 0 0 \x1e0424101830 173 0 0 0 0 \x1e0425101930 174 0 0 0 0 \x1e0426101930 175 0 0 0 0 \x1e0427101930 176 0 0 0 0 \x1e0428101930 177 0 0 0 0 \x1e0429101930 178 0 0 0 0 \x1e0430101930 179 0 0 0 0 \x1e0501102030 180 0 0 0 0 \x1e0502102030 181 0 0 0 0 \x1e0503102030 182 0 0 0 0 \x1e0504102030 183 0 0 0 0 \x1e0505102030 184 0 0 0 0 \x1e0506102030 185 0 0 0 0 \x1e0507102130 186 0 0 0 0 \x1e0508102130 187 0 0 0 0 \x1e0509102130 188 0 0 0 0 \x1e0510102130 189 0 0 0 0 \x1e0511102130 190 0 0 0 0 \x1e0512102130 191 0 0 0 0 \x1e0513102230 192 0 0 0 0 \x1e0514102230 193 0 0 0 0 \x1e0515102230 194 0 0 0 0 \x1e0516102230 195 0 0 0 0 \x1e0517102230 196 0 0 0 0 \x1e0518102230 197 0 0 0 0 \x1e0519102330 198 0 0 0 0 \x1e0520102330 199 0 0 0 0 \x1e0521102330 200 0 0 0 0 \x1e0522102330 201 0 0 0 0 \x1e0523102330 202 0 0 0 0 \x1e0524102330 203 0 0 0 0 \x1e0525100700 204 0 0 0 0 \x1e0526100700 205 0 0 0 0 \x1e0527100700 206 0 0 0 0 \x1e0528100700 207 0 0 0 0 \x1e0529100700 208 0 0 0 0 \x1e0530100700 209 0 0 0 0 \x1e0531100700 210 0 0 0 0 \x1e0601100700 211 0 0 0 0 \x1e0602100700 212 0 0 0 0 \x1e0603100700 213 0 0 0 0 \x1e0604100700 214 0 0 0 0 \x1e0605100700 215 0 0 0 0 \x1e0606100700 216 0 0 0 0 \x1e0607100700 217 0 0 0 0 \x1e0608100700 218 0 0 0 0 \x1e0609100700 219 0 0 0 0 \x1e0610100700 220 0 0 0 0 \x1e0611100700 221 0 0 0 0 \x1e0612100700 222 0 0 0 0 \x1e0613100700 223 0 0 0 0 \x1e0614100700 224 0 0 0 0 \x1e0615100700 225 0 0 0 0 \x1e0616100700 226 0 0 0 0 \x1e0617100700 227 0 0 0 0 \x1e0618100700 228 0 0 0 0 \x1e0619100700 229 0 0 0 0 \x1e0620100700 230 0 0 0 0 \x1e0621100700 231 0 0 0 0 \x1e0622100700 232 0 0 0 0 \x1e0623100700 233 0 0 0 0 \x1e0624100700 234 0 0 0 0 \x1e0625100700 235 0 0 0 0 \x1e0626100700 236 0 0 0 0 \x1e0627100700 237 0 0 0 0 \x1e0628100700 238 0 0 0 0 \x1e0629100700 239 0 0 0 0 \x1e0630100700 240 0 0 0 0 \x1e0701100700 241 0 0 0 0 \x1e0702100700 242 0 0 0 0 \x1e0703100700 243 0 0 0 0 \x1e0704100700 244 0 0 0 0 \x1e0705100700 245 0 0 0 0 \x1e0706100700 246 0 0 0 0 \x1e0707100700 247 0 0 0 0 \x1e0708100700 248 0 0 0 0 \x1e0709100700 249 0 0 0 0 \x1e0710100700 250 0 0 0 0 \x1e0711100700 251 0 0 0 0 \x1e0712100700 252 0 0 0 0 \x1e0713100700 253 0 0 0 0 \x1e0714100700 254 0 0 0 0 \x1e0715100700 255 0 0 0 0 \x1e0716100700 256 0 0 0 0 \x1e0717100700 257 0 0 0 0 \x1e0718100700 258 0 0 0 0 \x1e0719100700 259 0 0 0 0 \x1e0720100700 260 0 0 0 0 \x1e0721100700 261 0 0 0 0 \x1e0722100700 262 0 0 0 0 \x1e0723100700 263 0 0 0 0 \x1e0724100700 264 0 0 0 0 \x1e0725100700 265 0 0 0 0 \x1e0726100700 266 0 0 0 0 \x1e0727100700 267 0 0 0 0 \x1e0728100700 268 0 0 0 0 \x1e0729100700 269 0 0 0 0 \x1e0730100700 270 0 0 0 0 \x1e0731100700 271 0 0 0 0 \x1e0801100700 272 0 0 0 0 \x1e0802100700 273 0 0 0 0 \x1e0803100700 274 0 0 0 0 \x1e0804100700 275 0 0 0 0 \x1e0805100700 276 0 0 0 0 \x1e0806100700 277 0 0 0 0 \x1e0807100700 278 0 0 0 0 \x1e0808100700 279 0 0 0 0 \x1e0809100700 280 0 0 0 0 \x1e0810100700 281 0 0 0 0 \x1e0811100700 282 0 0 0 0 \x1e0812100700 283 0 0 0 0 \x1e0813100700 284 0 0 0 0 \x1e0814100700 285 0 0 0 0 \x1e0815100700 286 0 0 0 0 \x1e0816100700 287 0 0 0 0 \x1e0817100700 288 0 0 0 0 \x1e0818100700 289 0 0 0 0 \x1e0819100700 290 0 0 0 0 \x1e0820100700 291 0 0 0 0 \x1e0821100700 292 0 0 0 0 \x1e0822100700 293 0 0 0 0 \x1e0823100700 294 0 0 0 0 \x1e0824100700 295 0 0 0 0 \x1e0825100700 296 0 0 0 0 \x1e0826100700 297 0 0 0 0 \x1e0827100700 298 0 0 0 0 \x1e0828100700 299 0 0 0 0 \x1e0829100700 300 0 0 0 0 \x1e0830100700 301 0 0 0 0 \x1e0831100700 302 0 0 0 0 \x1e0901100700 303 0 0 0 0 \x1e0902100700 304 0 0 0 0 \x1e0903100700 305 0 0 0 0 \x1e0904100700 306 0 0 0 0 \x1e0905100700 307 0 0 0 0 \x1e0906100700 308 0 0 0 0 \x1e0907100700 309 0 0 0 0 \x1e0908100700 310 0 0 0 0 \x1e0909100700 311 0 0 0 0 \x1e0910100700 312 0 0 0 0 \x1e0911100700 313 0 0 0 0 \x1e0912100700 314 0 0 0 0 \x1e0913100700 315 0 0 0 0 \x1e0914100700 316 0 0 0 0 \x1e0915100700 317 0 0 0 0 \x1e0916100700 318 0 0 0 0 \x1e0917100700 319 0 0 0 0 \x1e0918100700 320 0 0 0 0 \x1e0919100700 321 0 0 0 0 \x1e0920100700 322 0 0 0 0 \x1e0921100700 323 0 0 0 0 \x1e0922100700 324 0 0 0 0 \x1e0923100700 325 0 0 0 0 \x1e0924100700 326 0 0 0 0 \x1e0925100700 327 0 0 0 0 \x1e0926100700 328 0 0 0 0 \x1e0927100700 329 0 0 0 0 \x1e0928100700 330 1 0 0 0 \x1e0929100700 331 1 0 0 0 \x1e0930100700 332 1 0 0 0 \x1e1001100700 333 1 0 0 0 \x1e1002100700 334 1 0 0 0 \x1e1003100700 335 1 0 0 0 \x1e1004100700 336 1 0 0 0 \x1e1005100700 337 1 0 0 0 \x1e1006100700 338 1 0 0 0 \x1e1007100700 339 1 0 0 0 \x1e1008100700 340 2 0 0 0 \x1e1009100700 341 2 0 0 0 \x1e1010100700 342 2 0 0 0 \x1e1011100700 343 2 0 0 0 \x1e1012100700 344 2 0 0 0 \x1e1013100700 345 2 0 0 0 \x1e1014100700 346 2 0 0 0 \x1e1015100700 347 2 0 0 0 \x1e1016100700 348 2 0 0 0 \x1e1017100700 349 2 0 0 0 \x1e1018100700 350 12 0 0 0 \x1e1019100700 351 12 0 0 0 \x1e1020100700 352 12 0 0 0 \x1e1021100700 353 12 0 0 0 \x1e1022100700 354 12 0 0 0 \x1e1023100700 355 12 0 0 0 \x1e1024100700 356 12 0 0 0 \x1e1025100700 357 12 0 0 0 \x1e1026100700 358 12 0 0 0 \x1e1027100700 359 12 0 0 0 \x1e\x067918\r"), \
                 ("Half full (150)", 0, 150, "&DB 404A000412C \x0638262\r&DZ 150 \x1e0531100700 601 0 0 0 0 \x1e0601100700 601 0 0 0 0 \x1e0602100700 601 0 0 0 0 \x1e0603100700 601 0 0 0 0 \x1e0604100700 601 0 0 0 0 \x1e0605100700 19 0 0 0 0 \x1e0606100700 19 0 0 0 0 \x1e0607100700 19 0 0 0 0 \x1e0608100700 19 0 0 0 0 \x1e0609100700 19 0 0 0 0 \x1e0610100700 220 0 0 0 0 \x1e0611100700 221 0 0 0 0 \x1e0612100700 222 0 0 0 0 \x1e0613100700 223 0 0 0 0 \x1e0614100700 224 0 0 0 0 \x1e0615100700 225 0 0 0 0 \x1e0616100700 226 0 0 0 0 \x1e0617100700 227 0 0 0 0 \x1e0618100700 228 0 0 0 0 \x1e0619100700 229 0 0 0 0 \x1e0620100700 230 0 0 0 0 \x1e0621100700 231 0 0 0 0 \x1e0622100700 232 0 0 0 0 \x1e0623100700 233 0 0 0 0 \x1e0624100700 234 0 0 0 0 \x1e0625100700 235 0 0 0 0 \x1e0626100700 236 0 0 0 0 \x1e0627100700 237 0 0 0 0 \x1e0628100700 238 0 0 0 0 \x1e0629100700 239 0 0 0 0 \x1e0630100700 240 0 0 0 0 \x1e0701100700 241 0 0 0 0 \x1e0702100700 242 0 0 0 0 \x1e0703100700 243 0 0 0 0 \x1e0704100700 244 0 0 0 0 \x1e0705100700 245 0 0 0 0 \x1e0706100700 246 0 0 0 0 \x1e0707100700 247 0 0 0 0 \x1e0708100700 248 0 0 0 0 \x1e0709100700 249 0 0 0 0 \x1e0710100700 250 0 0 0 0 \x1e0711100700 251 0 0 0 0 \x1e0712100700 252 0 0 0 0 \x1e0713100700 253 0 0 0 0 \x1e0714100700 254 0 0 0 0 \x1e0715100700 255 0 0 0 0 \x1e0716100700 256 0 0 0 0 \x1e0717100700 257 0 0 0 0 \x1e0718100700 258 0 0 0 0 \x1e0719100700 259 0 0 0 0 \x1e0720100700 260 0 0 0 0 \x1e0721100700 261 0 0 0 0 \x1e0722100700 262 0 0 0 0 \x1e0723100700 263 0 0 0 0 \x1e0724100700 264 0 0 0 0 \x1e0725100700 265 0 0 0 0 \x1e0726100700 266 0 0 0 0 \x1e0727100700 267 0 0 0 0 \x1e0728100700 268 0 0 0 0 \x1e0729100700 269 0 0 0 0 \x1e0730100700 270 0 0 0 0 \x1e0731100700 271 0 0 0 0 \x1e0801100700 272 0 0 0 0 \x1e0802100700 273 0 0 0 0 \x1e0803100700 274 0 0 0 0 \x1e0804100700 275 0 0 0 0 \x1e0805100700 276 0 0 0 0 \x1e0806100700 277 0 0 0 0 \x1e0807100700 278 0 0 0 0 \x1e0808100700 279 0 0 0 0 \x1e0809100700 280 0 0 0 0 \x1e0810100700 281 0 0 0 0 \x1e0811100700 282 0 0 0 0 \x1e0812100700 283 0 0 0 0 \x1e0813100700 284 0 0 0 0 \x1e0814100700 285 0 0 0 0 \x1e0815100700 286 0 0 0 0 \x1e0816100700 287 0 0 0 0 \x1e0817100700 288 0 0 0 0 \x1e0818100700 289 0 0 0 0 \x1e0819100700 290 0 0 0 0 \x1e0820100700 291 0 0 0 0 \x1e0821100700 292 0 0 0 0 \x1e0822100700 293 0 0 0 0 \x1e0823100700 294 0 0 0 0 \x1e0824100700 295 0 0 0 0 \x1e0825100700 296 0 0 0 0 \x1e0826100700 297 0 0 0 0 \x1e0827100700 298 0 0 0 0 \x1e0828100700 299 0 0 0 0 \x1e0829100700 300 0 0 0 0 \x1e0830100700 301 0 0 0 0 \x1e0831100700 302 0 0 0 0 \x1e0901100700 303 0 0 0 0 \x1e0902100700 304 0 0 0 0 \x1e0903100700 305 0 0 0 0 \x1e0904100700 306 0 0 0 0 \x1e0905100700 307 0 0 0 0 \x1e0906100700 308 0 0 0 0 \x1e0907100700 309 0 0 0 0 \x1e0908100700 310 0 0 0 0 \x1e0909100700 311 0 0 0 0 \x1e0910100700 312 0 0 0 0 \x1e0911100700 313 0 0 0 0 \x1e0912100700 314 0 0 0 0 \x1e0913100700 315 0 0 0 0 \x1e0914100700 316 0 0 0 0 \x1e0915100700 317 0 0 0 0 \x1e0916100700 318 0 0 0 0 \x1e0917100700 319 0 0 0 0 \x1e0918100700 320 0 0 0 0 \x1e0919100700 321 0 0 0 0 \x1e0920100700 322 0 0 0 0 \x1e0921100700 323 0 0 0 0 \x1e0922100700 324 0 0 0 0 \x1e0923100700 325 0 0 0 0 \x1e0924100700 326 0 0 0 0 \x1e0925100700 327 0 0 0 0 \x1e0926100700 328 0 0 0 0 \x1e0927100700 329 0 0 0 0 \x1e0928100700 330 1 0 0 0 \x1e0929100700 331 1 0 0 0 \x1e0930100700 332 1 0 0 0 \x1e1001100700 333 1 0 0 0 \x1e1002100700 334 1 0 0 0 \x1e1003100700 335 1 0 0 0 \x1e1004100700 336 1 0 0 0 \x1e1005100700 337 1 0 0 0 \x1e1006100700 338 1 0 0 0 \x1e1007100700 339 1 0 0 0 \x1e1008100700 340 2 0 0 0 \x1e1009100700 341 2 0 0 0 \x1e1010100700 342 2 0 0 0 \x1e1011100700 343 2 0 0 0 \x1e1012100700 344 2 0 0 0 \x1e1013100700 345 2 0 0 0 \x1e1014100700 346 2 0 0 0 \x1e1015100700 347 2 0 0 0 \x1e1016100700 348 2 0 0 0 \x1e1017100700 349 2 0 0 0 \x1e1018100700 350 12 0 0 0 \x1e1019100700 351 12 0 0 0 \x1e1020100700 352 12 0 0 0 \x1e1021100700 353 12 0 0 0 \x1e1022100700 354 12 0 0 0 \x1e1023100700 355 12 0 0 0 \x1e1024100700 356 12 0 0 0 \x1e1025100700 357 12 0 0 0 \x1e1026100700 358 12 0 0 0 \x1e1027100700 359 12 0 0 0 \x1e\x0610110\r"), \
                 ("Empty (0)", 0, 0, "&DB 404A000410E \x0662932\r&DZ y \x0618630\r"), \
                 ("Checksum error serial number", 12, 0, "&DB 404A000512C \x0638262\r&DZ 150 \x1e0531100700 601 0 0 0 0 \x1e0601100700 601 0 0 0 0 \x1e0602100700 601 0 0 0 0 \x1e0603100700 601 0 0 0 0 \x1e0604100700 601 0 0 0 0 \x1e0605100700 19 0 0 0 0 \x1e0606100700 19 0 0 0 0 \x1e0607100700 19 0 0 0 0 \x1e0608100700 19 0 0 0 0 \x1e0609100700 19 0 0 0 0 \x1e0610100700 220 0 0 0 0 \x1e0611100700 221 0 0 0 0 \x1e0612100700 222 0 0 0 0 \x1e0613100700 223 0 0 0 0 \x1e0614100700 224 0 0 0 0 \x1e0615100700 225 0 0 0 0 \x1e0616100700 226 0 0 0 0 \x1e0617100700 227 0 0 0 0 \x1e0618100700 228 0 0 0 0 \x1e0619100700 229 0 0 0 0 \x1e0620100700 230 0 0 0 0 \x1e0621100700 231 0 0 0 0 \x1e0622100700 232 0 0 0 0 \x1e0623100700 233 0 0 0 0 \x1e0624100700 234 0 0 0 0 \x1e0625100700 235 0 0 0 0 \x1e0626100700 236 0 0 0 0 \x1e0627100700 237 0 0 0 0 \x1e0628100700 238 0 0 0 0 \x1e0629100700 239 0 0 0 0 \x1e0630100700 240 0 0 0 0 \x1e0701100700 241 0 0 0 0 \x1e0702100700 242 0 0 0 0 \x1e0703100700 243 0 0 0 0 \x1e0704100700 244 0 0 0 0 \x1e0705100700 245 0 0 0 0 \x1e0706100700 246 0 0 0 0 \x1e0707100700 247 0 0 0 0 \x1e0708100700 248 0 0 0 0 \x1e0709100700 249 0 0 0 0 \x1e0710100700 250 0 0 0 0 \x1e0711100700 251 0 0 0 0 \x1e0712100700 252 0 0 0 0 \x1e0713100700 253 0 0 0 0 \x1e0714100700 254 0 0 0 0 \x1e0715100700 255 0 0 0 0 \x1e0716100700 256 0 0 0 0 \x1e0717100700 257 0 0 0 0 \x1e0718100700 258 0 0 0 0 \x1e0719100700 259 0 0 0 0 \x1e0720100700 260 0 0 0 0 \x1e0721100700 261 0 0 0 0 \x1e0722100700 262 0 0 0 0 \x1e0723100700 263 0 0 0 0 \x1e0724100700 264 0 0 0 0 \x1e0725100700 265 0 0 0 0 \x1e0726100700 266 0 0 0 0 \x1e0727100700 267 0 0 0 0 \x1e0728100700 268 0 0 0 0 \x1e0729100700 269 0 0 0 0 \x1e0730100700 270 0 0 0 0 \x1e0731100700 271 0 0 0 0 \x1e0801100700 272 0 0 0 0 \x1e0802100700 273 0 0 0 0 \x1e0803100700 274 0 0 0 0 \x1e0804100700 275 0 0 0 0 \x1e0805100700 276 0 0 0 0 \x1e0806100700 277 0 0 0 0 \x1e0807100700 278 0 0 0 0 \x1e0808100700 279 0 0 0 0 \x1e0809100700 280 0 0 0 0 \x1e0810100700 281 0 0 0 0 \x1e0811100700 282 0 0 0 0 \x1e0812100700 283 0 0 0 0 \x1e0813100700 284 0 0 0 0 \x1e0814100700 285 0 0 0 0 \x1e0815100700 286 0 0 0 0 \x1e0816100700 287 0 0 0 0 \x1e0817100700 288 0 0 0 0 \x1e0818100700 289 0 0 0 0 \x1e0819100700 290 0 0 0 0 \x1e0820100700 291 0 0 0 0 \x1e0821100700 292 0 0 0 0 \x1e0822100700 293 0 0 0 0 \x1e0823100700 294 0 0 0 0 \x1e0824100700 295 0 0 0 0 \x1e0825100700 296 0 0 0 0 \x1e0826100700 297 0 0 0 0 \x1e0827100700 298 0 0 0 0 \x1e0828100700 299 0 0 0 0 \x1e0829100700 300 0 0 0 0 \x1e0830100700 301 0 0 0 0 \x1e0831100700 302 0 0 0 0 \x1e0901100700 303 0 0 0 0 \x1e0902100700 304 0 0 0 0 \x1e0903100700 305 0 0 0 0 \x1e0904100700 306 0 0 0 0 \x1e0905100700 307 0 0 0 0 \x1e0906100700 308 0 0 0 0 \x1e0907100700 309 0 0 0 0 \x1e0908100700 310 0 0 0 0 \x1e0909100700 311 0 0 0 0 \x1e0910100700 312 0 0 0 0 \x1e0911100700 313 0 0 0 0 \x1e0912100700 314 0 0 0 0 \x1e0913100700 315 0 0 0 0 \x1e0914100700 316 0 0 0 0 \x1e0915100700 317 0 0 0 0 \x1e0916100700 318 0 0 0 0 \x1e0917100700 319 0 0 0 0 \x1e0918100700 320 0 0 0 0 \x1e0919100700 321 0 0 0 0 \x1e0920100700 322 0 0 0 0 \x1e0921100700 323 0 0 0 0 \x1e0922100700 324 0 0 0 0 \x1e0923100700 325 0 0 0 0 \x1e0924100700 326 0 0 0 0 \x1e0925100700 327 0 0 0 0 \x1e0926100700 328 0 0 0 0 \x1e0927100700 329 0 0 0 0 \x1e0928100700 330 1 0 0 0 \x1e0929100700 331 1 0 0 0 \x1e0930100700 332 1 0 0 0 \x1e1001100700 333 1 0 0 0 \x1e1002100700 334 1 0 0 0 \x1e1003100700 335 1 0 0 0 \x1e1004100700 336 1 0 0 0 \x1e1005100700 337 1 0 0 0 \x1e1006100700 338 1 0 0 0 \x1e1007100700 339 1 0 0 0 \x1e1008100700 340 2 0 0 0 \x1e1009100700 341 2 0 0 0 \x1e1010100700 342 2 0 0 0 \x1e1011100700 343 2 0 0 0 \x1e1012100700 344 2 0 0 0 \x1e1013100700 345 2 0 0 0 \x1e1014100700 346 2 0 0 0 \x1e1015100700 347 2 0 0 0 \x1e1016100700 348 2 0 0 0 \x1e1017100700 349 2 0 0 0 \x1e1018100700 350 12 0 0 0 \x1e1019100700 351 12 0 0 0 \x1e1020100700 352 12 0 0 0 \x1e1021100700 353 12 0 0 0 \x1e1022100700 354 12 0 0 0 \x1e1023100700 355 12 0 0 0 \x1e1024100700 356 12 0 0 0 \x1e1025100700 357 12 0 0 0 \x1e1026100700 358 12 0 0 0 \x1e1027100700 359 12 0 0 0 \x1e\x0610110\r"), \
                 ("Checksum error results", 12, 0, "&DB 404A000412C \x0638562\r&DZ 150 \x1e0531100700 601 0 0 0 0 \x1e0601100700 601 0 0 0 0 \x1e0602100700 601 0 0 0 0 \x1e0603100700 601 0 0 0 0 \x1e0604100700 601 0 0 0 0 \x1e0605100700 19 0 0 0 0 \x1e0606100700 19 0 0 0 0 \x1e0607100700 19 0 0 0 0 \x1e0608100700 19 0 0 0 0 \x1e0609100700 19 0 0 0 0 \x1e0610100700 220 0 0 0 0 \x1e0611100700 221 0 0 0 0 \x1e0612100700 222 0 0 0 0 \x1e0613100700 223 0 0 0 0 \x1e0614100700 224 0 0 0 0 \x1e0615100700 225 0 0 0 0 \x1e0616100700 226 0 0 0 0 \x1e0617100700 227 0 0 0 0 \x1e0618100700 228 0 0 0 0 \x1e0619100700 229 0 0 0 0 \x1e0620100700 230 0 0 0 0 \x1e0621100700 231 0 0 0 0 \x1e0622100700 232 0 0 0 0 \x1e0623100700 233 0 0 0 0 \x1e0624100700 234 0 0 0 0 \x1e0625100700 235 0 0 0 0 \x1e0626100700 236 0 0 0 0 \x1e0627100700 237 0 0 0 0 \x1e0628100700 238 0 0 0 0 \x1e0629100700 239 0 0 0 0 \x1e0630100700 240 0 0 0 0 \x1e0701100700 241 0 0 0 0 \x1e0702100700 242 0 0 0 0 \x1e0703100700 243 0 0 0 0 \x1e0704100700 244 0 0 0 0 \x1e0705100700 245 0 0 0 0 \x1e0706100700 246 0 0 0 0 \x1e0707100700 247 0 0 0 0 \x1e0708100700 248 0 0 0 0 \x1e0709100700 249 0 0 0 0 \x1e0710100700 250 0 0 0 0 \x1e0711100700 251 0 0 0 0 \x1e0712100700 252 0 0 0 0 \x1e0713100700 253 0 0 0 0 \x1e0714100700 254 0 0 0 0 \x1e0715100700 255 0 0 0 0 \x1e0716100700 256 0 0 0 0 \x1e0717100700 257 0 0 0 0 \x1e0718100700 258 0 0 0 0 \x1e0719100700 259 0 0 0 0 \x1e0720100700 260 0 0 0 0 \x1e0721100700 261 0 0 0 0 \x1e0722100700 262 0 0 0 0 \x1e0723100700 263 0 0 0 0 \x1e0724100700 264 0 0 0 0 \x1e0725100700 265 0 0 0 0 \x1e0726100700 266 0 0 0 0 \x1e0727100700 267 0 0 0 0 \x1e0728100700 268 0 0 0 0 \x1e0729100700 269 0 0 0 0 \x1e0730100700 270 0 0 0 0 \x1e0731100700 271 0 0 0 0 \x1e0801100700 272 0 0 0 0 \x1e0802100700 273 0 0 0 0 \x1e0803100700 274 0 0 0 0 \x1e0804100700 275 0 0 0 0 \x1e0805100700 276 0 0 0 0 \x1e0806100700 277 0 0 0 0 \x1e0807100700 278 0 0 0 0 \x1e0808100700 279 0 0 0 0 \x1e0809100700 280 0 0 0 0 \x1e0810100700 281 0 0 0 0 \x1e0811100700 282 0 0 0 0 \x1e0812100700 283 0 0 0 0 \x1e0813100700 284 0 0 0 0 \x1e0814100700 285 0 0 0 0 \x1e0815100700 286 0 0 0 0 \x1e0816100700 287 0 0 0 0 \x1e0817100700 288 0 0 0 0 \x1e0818100700 289 0 0 0 0 \x1e0819100700 290 0 0 0 0 \x1e0820100700 291 0 0 0 0 \x1e0821100700 292 0 0 0 0 \x1e0822100700 293 0 0 0 0 \x1e0823100700 294 0 0 0 0 \x1e0824100700 295 0 0 0 0 \x1e0825100700 296 0 0 0 0 \x1e0826100700 297 0 0 0 0 \x1e0827100700 298 0 0 0 0 \x1e0828100700 299 0 0 0 0 \x1e0829100700 300 0 0 0 0 \x1e0830100700 301 0 0 0 0 \x1e0831100700 302 0 0 0 0 \x1e0901100700 303 0 0 0 0 \x1e0902100700 304 0 0 0 0 \x1e0903100700 305 0 0 0 0 \x1e0904100700 306 0 0 0 0 \x1e0905100700 307 0 0 0 0 \x1e0906100700 308 0 0 0 0 \x1e0907100700 309 0 0 0 0 \x1e0908100700 310 0 0 0 0 \x1e0909100700 311 0 0 0 0 \x1e0910100700 312 0 0 0 0 \x1e0911100700 313 0 0 0 0 \x1e0912100700 314 0 0 0 0 \x1e0913100700 315 0 0 0 0 \x1e0914100700 316 0 0 0 0 \x1e0915100700 317 0 0 0 0 \x1e0916100700 318 0 0 0 0 \x1e0917100700 319 0 0 0 0 \x1e0918100700 320 0 0 0 0 \x1e0919100700 321 0 0 0 0 \x1e0920100700 322 0 0 0 0 \x1e0921100700 323 0 0 0 0 \x1e0922100700 324 0 0 0 0 \x1e0923100700 325 0 0 0 0 \x1e0924100700 326 0 0 0 0 \x1e0925100700 327 0 0 0 0 \x1e0926100700 328 0 0 0 0 \x1e0927100700 329 0 0 0 0 \x1e0928100700 330 1 0 0 0 \x1e0929100700 331 1 0 0 0 \x1e0930100700 332 1 0 0 0 \x1e1001100700 333 1 0 0 0 \x1e1002100700 334 1 0 0 0 \x1e1003100700 335 1 0 0 0 \x1e1004100700 336 1 0 0 0 \x1e1005100700 337 1 0 0 0 \x1e1006100700 338 1 0 0 0 \x1e1007100700 339 1 0 0 0 \x1e1008100700 340 2 0 0 0 \x1e1009100700 341 2 0 0 0 \x1e1010100700 342 2 0 0 0 \x1e1011100700 343 2 0 0 0 \x1e1012100700 344 2 0 0 0 \x1e1013100700 345 2 0 0 0 \x1e1014100700 346 2 0 0 0 \x1e1015100700 347 2 0 0 0 \x1e1016100700 348 2 0 0 0 \x1e1017100700 349 2 0 0 0 \x1e1018100700 350 12 0 0 0 \x1e1019100700 351 12 0 0 0 \x1e1020100700 352 12 0 0 0 \x1e1021100700 353 12 0 0 0 \x1e1022100700 354 12 0 0 0 \x1e1023100700 355 12 0 0 0 \x1e1024100700 356 12 0 0 0 \x1e1025100700 357 12 0 0 0 \x1e1026100700 358 12 0 0 0 \x1e1027100700 359 12 0 0 0 \x1e\x0610110\r"))

    for data in test_data:
        _ret = AnalyseOnCall(data[3])
        if len(_ret[0]["results"]) > 0:
            if _ret[0]["results"][0].has_key("fault_data"):
                if _ret[0]["results"][0]["error_code"] == data[1]:
                    print "OK   : TC %s (recieved expected error code %d)" % (data[0], data[1])
                else:
                    print "FAIL : TC %s (recieved error code %d expected %d)" % (data[0], data[1])
            else:
                if len(_ret[0]["results"]) == data[2]:
                    print "OK   : TC %s" % (data[0])
                else:
                    print "FAIL : TC %s (got %d number of results, expected %d)" % (data[0], len(_ret[0]["results"]), data[2]) 

        else:
            if data[2] == 0:
                print "OK   : TC %s" % (data[0])
            else:
                print "FAIL : TC %s (no data extracted - expected %d records)" % (data[0], data[2])

"""