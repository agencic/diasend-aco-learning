# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012-2015 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Dexcom G4
# @DEVICE Dexcom G5

import crcmod

from Generic import *
from Defines import *
import Buffer

import MeterDexcomSupport.G4Record
import MeterDexcomSupport.Record

SOH = 0x01

DEXCOM_CMD_ACK = 1
DEXCOM_CMD_NAK = 2
DEXCOM_CMD_INVALIDCOMMAND = 3
DEXCOM_CMD_INVALIDPARAM = 4
DEXCOM_CMD_INCOMPLETEPACKETRECEIVED = 5
DEXCOM_CMD_RECEIVERERROR = 6
DEXCOM_CMD_INVALIDMODE = 7
DEXCOM_CMD_PING = 10
DEXCOM_CMD_READFIRMWAREHEADER = 11
DEXCOM_CMD_READDATABASEPARITIONINFO = 15
DEXCOM_CMD_READDATABASEPAGERANGE = 16
DEXCOM_CMD_READDATABASEPAGES = 17
DEXCOM_CMD_READDATABASEPAGEHEADER = 18
DEXCOM_CMD_READTRANSMITTERID = 25
DEXCOM_CMD_WRITETRANSMITTERID = 26
DEXCOM_CMD_READLANGUAGE = 27
DEXCOM_CMD_WRITELANGUAGE = 28
DEXCOM_CMD_READDISPLAYTIMEOFFSET = 29
DEXCOM_CMD_WRITEDISPLAYTIMEOFFSET = 30
DEXCOM_CMD_READRTC = 31
DEXCOM_CMD_RESETRECEIVER = 32
DEXCOM_CMD_READBATTERYLEVEL = 33
DEXCOM_CMD_READSYSTEMTIME = 34
DEXCOM_CMD_READSYSTEMTIMEOFFSET = 35
DEXCOM_CMD_WRITESYSTEMTIME = 36
DEXCOM_CMD_READGLUCOSEUNIT = 37
DEXCOM_CMD_WRITEGLUCOSEUNIT = 38
DEXCOM_CMD_READBLINDEDMODE = 39
DEXCOM_CMD_WRITEBLINDEDMODE = 40
DEXCOM_CMD_READCLOCKMODE = 41
DEXCOM_CMD_WRITECLOCKMODE = 42
DEXCOM_CMD_READDEVICEMODE = 43
DEXCOM_CMD_ERASEDATABASE = 45
DEXCOM_CMD_SHUTDOWNRECEIVER = 46
DEXCOM_CMD_WRITEPCPARAMETERS = 47
DEXCOM_CMD_READBATTERYSTATE = 48
DEXCOM_CMD_READHARDWAREBOARDID = 49
DEXCOM_CMD_ENTERFIRMWAREUPGRADEMODE = 50
DEXCOM_CMD_READFLASHPAGE = 51
DEXCOM_CMD_WRITEFLASHPAGE = 52
DEXCOM_CMD_ENTERSAMBAACCESSMODE = 53
DEXCOM_CMD_READFIRMWARESETTINGS = 54
DEXCOM_CMD_READENABLESETUPWIZARDFLAG = 55
DEXCOM_CMD_WRITEENABLESETUPWIZARDFLAG = 56
DEXCOM_CMD_READSETUPWIZARDSTATE = 57
DEXCOM_CMD_WRITESETUPWIZARDSTATE = 58

DEXCOM_RECORD_TYPE_MANUFACTURINGDATA = 0
DEXCOM_RECORD_TYPE_FIRMWAREPARAMETERDATA = 1
DEXCOM_RECORD_TYPE_PCSOFTWAREPARAMETER = 2
DEXCOM_RECORD_TYPE_SENSORDATA = 3
DEXCOM_RECORD_TYPE_EGVDATA = 4
DEXCOM_RECORD_TYPE_CALSET = 5
DEXCOM_RECORD_TYPE_ABERRATION = 6
DEXCOM_RECORD_TYPE_INSERTIONTIME = 7
DEXCOM_RECORD_TYPE_RECEIVERLOGDATA = 8
DEXCOM_RECORD_TYPE_RECEIVERERRORDATA = 9
DEXCOM_RECORD_TYPE_METERDATA = 10
DEXCOM_RECORD_TYPE_USEREVENTDATA = 11
DEXCOM_RECORD_TYPE_USERSETTINGDATA = 12

DEXCOM_PAGE_SIZE = 528
DEXCOM_PAGE_HEADER_SIZE = 28

DEXCOM_RESULT_HIGH_VALUE = 401
DEXCOM_RESULT_LO_VALUE = 39

class DexcomG4(object):
    """
    A class holding analyzed data from a Dexcom G4 device.
    """

    command_to_class_with_version = {
        DEXCOM_CMD_READTRANSMITTERID: MeterDexcomSupport.G4Record.TransmitterIdRecord.create
    }

    command_to_class = {
        DEXCOM_CMD_READDATABASEPARITIONINFO: MeterDexcomSupport.G4Record.DatabasePartitionInfo,
        DEXCOM_CMD_READDATABASEPAGERANGE: MeterDexcomSupport.G4Record.DatabasePageRange,
        DEXCOM_CMD_READFIRMWARESETTINGS: MeterDexcomSupport.G4Record.FirmwareSettingsRecord,
        DEXCOM_CMD_READHARDWAREBOARDID: MeterDexcomSupport.G4Record.HardwareBoardIdRecord,
        DEXCOM_CMD_READBATTERYSTATE: MeterDexcomSupport.G4Record.BatteryStateRecord,
        DEXCOM_CMD_READDEVICEMODE: MeterDexcomSupport.G4Record.DeviceModeRecord,
        DEXCOM_CMD_READCLOCKMODE: MeterDexcomSupport.G4Record.ClockModeRecord,
        DEXCOM_CMD_READBLINDEDMODE: MeterDexcomSupport.G4Record.BlindedModeRecord,
        DEXCOM_CMD_READGLUCOSEUNIT: MeterDexcomSupport.G4Record.GlucoseUnitRecord,
        DEXCOM_CMD_READSYSTEMTIMEOFFSET: MeterDexcomSupport.G4Record.SystemTimeOffsetRecord,
        DEXCOM_CMD_READSYSTEMTIME: MeterDexcomSupport.G4Record.SystemTimeRecord,
        DEXCOM_CMD_READBATTERYLEVEL: MeterDexcomSupport.G4Record.BatteryLevelRecord,
        DEXCOM_CMD_READRTC: MeterDexcomSupport.G4Record.RTCRecord,
        DEXCOM_CMD_READDISPLAYTIMEOFFSET: MeterDexcomSupport.G4Record.DisplayTimeOffsetRecord,
        DEXCOM_CMD_READLANGUAGE: MeterDexcomSupport.G4Record.LanguageRecord,
        DEXCOM_CMD_READFIRMWAREHEADER: MeterDexcomSupport.G4Record.FirmwareHeaderRecord}

    record_type_to_class = {
        DEXCOM_RECORD_TYPE_MANUFACTURINGDATA: MeterDexcomSupport.G4Record.DatabasePageManufacturingParameterRecord,
        DEXCOM_RECORD_TYPE_FIRMWAREPARAMETERDATA: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_PCSOFTWAREPARAMETER: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_SENSORDATA: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_EGVDATA: MeterDexcomSupport.G4Record.EGVDataRecord,
        DEXCOM_RECORD_TYPE_CALSET: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_ABERRATION: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_INSERTIONTIME: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_RECEIVERLOGDATA: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_RECEIVERERRORDATA: MeterDexcomSupport.Record.RecordConsumeData,
        DEXCOM_RECORD_TYPE_METERDATA: MeterDexcomSupport.G4Record.MeterDataRecord,
        DEXCOM_RECORD_TYPE_USEREVENTDATA: MeterDexcomSupport.G4Record.UserEventRecord,
        DEXCOM_RECORD_TYPE_USERSETTINGDATA: MeterDexcomSupport.Record.RecordConsumeData}

    def __init__(self, data):
        """
        Instance the object using the data provided.
        """
        # Feed the raw data into a new buffer object.
        self.buffer = Buffer.Buffer(data)

        # List with extracted records in Diasend format.
        self.records = []

    def Analyse(self):
        """
        Analyse the data returned from the Dexcom extractor.

        Frame format : CMD SOH LEN_L LEN_H RES PAYLOAD CRC_L CRC_H

        CMD is the request command - this is added by the extractor and is not part of
        the standard Dexcom format.

        RES is the result of the command. If all is ok this should be ACK but it can
        also contain an error code (e.g. invalid parameter).
        """
        transmitter_version = None
        partition_info = None
        page_header_crc_ok = True
        
        crc_func = crcmod.predefined.mkCrcFun('xmodem')

        self.records.append({ELEM_DEVICE_UNIT: 'mg/dL'})

        while(True):

            try:
                # No more data in buffer - time to leave.
                if self.buffer.nr_bytes_left() == 0:
                    break

                # Extract the command id (the request). This is added by the
                # extractor and is not part of the standard Dexcom communication
                # frame (and should thus not be part of the checksum calculation).
                command, = self.buffer.get_struct('<B')

                # Set mark in buffer so we can retrieve the slice of data used
                # for checksum calculation.
                self.buffer.set_mark()

                # Extract frame header.
                control, total_len, result, = self.buffer.get_struct('<BHB')

                if control != SOH:
                    raise MeterDexcomSupport.Record.ConvertException('Control byte in frame not SOH (0x%x)' % (control))

                if result != DEXCOM_CMD_ACK:
                    raise MeterDexcomSupport.Record.ConvertException('Device returned incorrect result 0x%x' % (result))

                # SOF + LEN (16) + RES + CHKSUM (16) = 6
                if total_len < 6:
                    raise MeterDexcomSupport.Record.ConvertException('Record length too small')

                payload_len = total_len - 6

                if command in DexcomG4.command_to_class:
                    record = DexcomG4.command_to_class[command](self.buffer, payload_len)
                    if isinstance(record, MeterDexcomSupport.G4Record.FirmwareHeaderRecord):
                        transmitter_version = record.get_transmitter_version()
                        self.records.append(record.ConvertToDiasendFormat())
                    elif isinstance(record, MeterDexcomSupport.G4Record.DatabasePartitionInfo):
                        partition_info = record
                        record.get_egv_record_revision()
                    elif isinstance(record, MeterDexcomSupport.G4Record.BlindedModeRecord):
                        if record.blinded_mode == 1:
                            raise MeterDexcomSupport.G4Record.BlindedDeviceException()

                elif command in DexcomG4.command_to_class_with_version:
                    record = DexcomG4.command_to_class_with_version[command](transmitter_version, self.buffer, payload_len)

                # The read page command needs to be handle seperately - the response hold database
                # pages (which can include multiple records).
                elif command == DEXCOM_CMD_READDATABASEPAGES:

                    if (payload_len % DEXCOM_PAGE_SIZE) != 0:
                        raise MeterDexcomSupport.Record.ConvertException('Payload length must be multiple of page size')

                    number_of_pages = payload_len / DEXCOM_PAGE_SIZE

                    # Each read can contain multiple database pages.
                    for page_index in xrange(number_of_pages):

                        # Each page starts with a header.
                        header = MeterDexcomSupport.G4Record.DatabasePageHeader(self.buffer, DEXCOM_PAGE_HEADER_SIZE)
                        if not header.page_header_crc_ok:
                            page_header_crc_ok = False

                        if header.record_type in DexcomG4.record_type_to_class:
                            record_class = DexcomG4.record_type_to_class[header.record_type]

                            # This is a record type that we aren't interested in - we simple discard it.
                            if record_class == MeterDexcomSupport.Record.RecordConsumeData:
                                record = MeterDexcomSupport.Record.RecordConsumeData(self.buffer, DEXCOM_PAGE_SIZE - DEXCOM_PAGE_HEADER_SIZE)
                            else:
                                record_size = record_class.RecordLengthRevision(partition_info)
                                for record_index in xrange(header.number_of_records):
                                    # Create record instance (and consume the data in the buffer).
                                    record = record_class.create(partition_info, self.buffer, record_size)
                                    # Try to convert record to Diasend format - if the record doesn't contain information
                                    # that should be added to the Diasend database we will get an empty dictionary back.
                                    record_dictionary = record.ConvertToDiasendFormat()
                                    if len(record_dictionary) > 0:
                                        self.records.append(record_dictionary)

                                # Remove the excess bytes (end of page).
                                remaining_bytes = DEXCOM_PAGE_SIZE - DEXCOM_PAGE_HEADER_SIZE - (record_size * header.number_of_records)
                                if remaining_bytes > 0:
                                    self.buffer.consume(remaining_bytes)
                        else:
                            raise MeterDexcomSupport.Record.ConvertException('Unknown record type 0x%x' % (header.record_type))
                else:
                    raise MeterDexcomSupport.Record.ConvertException('Unknown command 0x%x' % (command))

                # Extract header + payload for checksum calculation.
                crc_frame = self.buffer.get_slice_from_mark()

                # Calculate checksum (CCITT xmodem).
                crc_calc = crc_func(crc_frame)

                # Extract checksum.
                crc_sent, = self.buffer.get_struct('<H')

                if crc_sent != crc_calc:
                    raise MeterDexcomSupport.Record.ConvertException('Command crc check failed - sent %x calculated %x' % (crc_sent, crc_calc))

            except Buffer.ExtractException, e:
                raise MeterDexcomSupport.Record.ConvertException('Failed to extract from buffer %s' % (e))
        
        if transmitter_version is not None:
            if transmitter_version != 4 and not page_header_crc_ok:
                raise MeterDexcomSupport.Record.ConvertException('Page header crc check failed')

        # Unit and serial and model, should not be counted as a result.
        # Note : some data dumps has shown multiple records with serial-number. 
        number_of_records_to_skip = 0
        for record in self.records:
            if (ELEM_DEVICE_SERIAL in record) or (ELEM_DEVICE_UNIT in record) or (ELEM_DEVICE_MODEL in record):
                number_of_records_to_skip += 1
            if ELEM_VAL_TYPE in record:
                if record[ELEM_VAL_TYPE] == VALUE_TYPE_GLUCOSE:
                    if record[ELEM_VAL] <= DEXCOM_RESULT_LO_VALUE:
                        record[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)
                    if record[ELEM_VAL] >= DEXCOM_RESULT_HIGH_VALUE: 
                        record[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
        self.records.append({ELEM_NR_RESULTS: len(self.records) - number_of_records_to_skip})

    def GetSerialNumber(self):
        """
        Return serial number (if extracted from data), otherwise empty string.
        """
        for record in self.records:
            if ELEM_DEVICE_SERIAL in record:
                return record[ELEM_DEVICE_SERIAL]
        return ''

    def GetModelName(self):
        """
        Return the model name (if extracted from data), otherwise ?.
        """
        for record in self.records:
            if ELEM_DEVICE_MODEL in record:
                return record[ELEM_DEVICE_MODEL]
        return 'Dexcom G?'

    def EvalUnitRecord(self, line):
        return line if ELEM_DEVICE_UNIT in line else {}

    def EvalSerialRecord(self, line):
        return line if ELEM_DEVICE_SERIAL in line else {}

    def EvalModelRecord(self, line):
        return line if ELEM_DEVICE_MODEL in line else {}

    def EvalResultRecord(self, line):
        return line if ELEM_VAL_TYPE in line else {}

    def EvalNrResultRecord(self, line):
        return line if ELEM_NR_RESULTS in line else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------


def DetectDexcomG4(inList):
    """
    Detect if data comes from a Dexcom G4 device.
    """
    return DetectDevice('DexcomG4', inList, DEVICE_METER)


def AnalyseDexcomG4(inData):
    """
    Analyse Dexcom G4.
    """

    try:
        dexcom = DexcomG4(inData)
        dexcom.Analyse()

        meter_callbacks = {}
        meter_callbacks["eval_device_model_record"] = dexcom.EvalModelRecord
        meter_callbacks["eval_serial_record"] = dexcom.EvalSerialRecord
        meter_callbacks["eval_result_record"] = dexcom.EvalResultRecord
        meter_callbacks["eval_nr_results"] = dexcom.EvalNrResultRecord
        meter_callbacks["eval_unit"] = dexcom.EvalUnitRecord

    except MeterDexcomSupport.Record.ConvertException, e:
        return CreateErrorResponseList(dexcom.GetModelName(),
                                       dexcom.GetSerialNumber(),
                                       ERROR_CODE_PREPROCESSING_FAILED,
                                       e.text)

    except MeterDexcomSupport.G4Record.BlindedDeviceException:
        return CreateErrorResponseList(dexcom.GetModelName(),
                                       dexcom.GetSerialNumber(),
                                       ERROR_CODE_BLINDED_DEVICE,
                                       'Rejected transmission from blinded device')

    resList = AnalyseGenericMeter(dexcom.records, meter_callbacks)

    return resList

if __name__ == '__main__':

    test_files = ['DexcomG4-manual-boluses.bin',
        'DexcomG4EP168.bin',
        'DexcomG5-DE00293.bin',
        'DexcomG5-ticket4648.bin',
        'DexcomG4-ticket2605.bin',
        'DexcomG4EP197.bin',
        'DexcomG5-DE0294.bin',
        'Fail/DexcomG4-blinded.bin', 
        'DexcomG4-blinded-unblinded.bin']
        
    for test_file in test_files:
        with open('test/testcases/test_data/DexcomG4/%s' % (test_file), 'rb') as f:
            raw_data = f.read()

        res = AnalyseDexcomG4(raw_data)
        for r in res[0]['results']:
            print r
        
        meter_type = res[0]['header']['meter_type']
        meter_serial = res[0]['header']['meter_serial']
        nr_results = len(res[0]['results'])
        is_ok = False
        if nr_results > 0:
            is_ok = 'device_value' in res[0]['results'][0]

        f_desc = ''
        f_code = 0
        if not is_ok:
            f_desc = res[0]['results'][0]['fault_data']
            f_code = res[0]['results'][0]['error_code']
            
        if is_ok:
            print '%s : %s - OK - number of results %d' % (meter_type, meter_serial, nr_results)
        else:
            print '%s : %s - FAIL - code %d - description %s' % (meter_type, meter_serial, f_code, f_desc)
        
