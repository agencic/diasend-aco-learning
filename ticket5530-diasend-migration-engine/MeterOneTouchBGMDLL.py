# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE OneTouch Verio
# @DEVICE OneTouch Select Plus
# @DEVICE OneTouch Verio Flex
# @DEVICE LifeScan OneTouch Select Plus Flex

import crcmod
from datetime import datetime, timedelta
import re

from Generic import *
from Defines import *

import Buffer

map_meal_flag_blood_test_to_flag = {
    0x01 : FLAG_BEFORE_MEAL, 
    0x02 : FLAG_AFTER_MEAL, 
    0x03 : FLAG_FASTING, 
    0x04 : FLAG_BEFORE_SLEEP
}

map_meal_flag_control_solution_to_flag = {
    0x01 : FLAG_CONTROL_SOLUTION_TYPE_LOW, 
    0x02 : FLAG_CONTROL_SOLUTION_TYPE_MID, 
    0x03 : FLAG_CONTROL_SOLUTION_TYPE_HIGH 
}

map_meal_flag_linearity_solution_to_flag = {
    0x01 : FLAG_LINEARITY_SOLUTION_LEVEL_1, 
    0x02 : FLAG_LINEARITY_SOLUTION_LEVEL_2, 
    0x03 : FLAG_LINEARITY_SOLUTION_LEVEL_3, 
    0x04 : FLAG_LINEARITY_SOLUTION_LEVEL_4,
    0x05 : FLAG_LINEARITY_SOLUTION_LEVEL_5
}

map_user_comment_to_flag = {
    0x01 : FLAG_NOT_ENOUGH_FOOD,
    0x02 : FLAG_TOO_MUCH_FOOD,
    0x03 : FLAG_MILD_EXERCISE,
    0x04 : FLAG_HARD_EXERCISE,
    0x05 : FLAG_MEDICATION,
    0x06 : FLAG_STRESS,
    0x07 : FLAG_ILLNESS,
    0x08 : FLAG_FEEL_HYPO,
    0x09 : FLAG_MENSES,
    0x0A : FLAG_VACATION,
    0x0B : FLAG_OTHER    
}

# To support new devices add them to this dictionary. Key is
# first character in serial number.
map_brand_to_market_name = {
    'X' : 'OneTouch Verio',
    'J' : 'OneTouch Select Plus',
    'E' : 'OneTouch Select Plus',
    'Z' : 'OneTouch Verio Flex',
    'G' : 'LifeScan OneTouch Select Plus Flex'
}

def EvalResultRecord(line):
    if ELEM_VAL in line or SETTINGS_LIST in line:
        return line
    return {}

def EvalUnitRecord(line):
    return { "meter_unit":"mg/dL" }

def EvalChecksumRecord(line, record):
    return True

def EvalNrResultsRecord(line):
    if ELEM_NR_RESULTS in line:
        return line
    return {}

def EvalSerialRecord(line):
    if ELEM_DEVICE_SERIAL in line:
        return line
    return {}

def EvalDeviceModelRecord( line ):
    if ELEM_DEVICE_MODEL in line:
        return line
    return {}

def CreateGlucoseRecord(datum):

    result = {}

    if datum['corrupt_record'] != 0x00:
        return result

    result[ELEM_VAL_TYPE]  = VALUE_TYPE_GLUCOSE
    result[ELEM_TIMESTAMP] = datetime(2000,1,1,0,0,0) + timedelta(seconds=datum['timestamp'])
    result[ELEM_VAL]       = datum['glucose_value'] * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
    result[ELEM_FLAG_LIST] = []

    if datum['sample_type'] == 0:
        if datum['meal_flag'] in map_meal_flag_blood_test_to_flag:
            result[ELEM_FLAG_LIST].append(map_meal_flag_blood_test_to_flag[datum['meal_flag']])
        if result[ELEM_VAL] > (VAL_FACTOR_CONV_MMOL_TO_MGDL * LIFESCAN_BG_VALUE_HIGH_MMOL):
            result[ELEM_FLAG_LIST].append(FLAG_RESULT_HIGH)
        elif result[ELEM_VAL] < (VAL_FACTOR_CONV_MMOL_TO_MGDL * LIFESCAN_BG_VALUE_LOW_MMOL):
            result[ELEM_FLAG_LIST].append(FLAG_RESULT_LOW)

    elif datum['sample_type'] == 1:
        result[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL)
        if datum['meal_flag'] in map_meal_flag_control_solution_to_flag:
            result[ELEM_FLAG_LIST].append(map_meal_flag_control_solution_to_flag[datum['meal_flag']])

    elif datum['sample_type'] == 2:
        result[ELEM_FLAG_LIST].append(FLAG_RESULT_CTRL_2)
        if datum['meal_flag'] in map_meal_flag_linearity_solution_to_flag:
            result[ELEM_FLAG_LIST].append(map_meal_flag_linearity_solution_to_flag[datum['meal_flag']])
            
    else:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unknown sample type %d' % (datum['sample_type']))

    if datum['user_comment'] in map_user_comment_to_flag:
        result[ELEM_FLAG_LIST].append(map_user_comment_to_flag[datum['user_comment']])

    return result

def SplitData(model_serial_list, data):
    '''
    Split [data] (raw) from transmitter/uploader. Return a list of dictionaries 
    for Generic.py. Will raise Exception with descriptive error text if split 
    fails.
    '''

    result = []

    lines = re.split('!TAG!([A-Z_]+)!', data)

    # Ignore 1st element; this is data before the 1st tag (should be
    # empty).
    it = iter(lines[1:])
    tag_dict = dict(zip(it, it))

    if not all (key in tag_dict for key in ('SERIAL', 'MODEL', 'BRAND')):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Nonexisting mandatory tags')

    if len(tag_dict['SERIAL']) == 0 or not tag_dict['SERIAL'][0] in map_brand_to_market_name:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unsupported brand %s %s' % (tag_dict['SERIAL'], tag_dict['BRAND']))

    model_serial_list[0] = tag_dict['SERIAL']
    model_serial_list[1] = map_brand_to_market_name[tag_dict['SERIAL'][0]]

    result.append({ELEM_DEVICE_SERIAL : tag_dict['SERIAL']})
    result.append({ELEM_DEVICE_MODEL  : map_brand_to_market_name[tag_dict['SERIAL'][0]]})

    # Currently only support for enhanced glucose record.
    if not 'GLUCOSE_ENHANCED' in tag_dict:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Nonexisting enhanced glucose records')

    buffer = Buffer.Buffer(tag_dict['GLUCOSE_ENHANCED'])
    buffer.set_mark()

    (total_length, status) = buffer.get_struct('<IB')

    # 7 = header + crc, 18 = length per record
    # There is no real information regarding total number of records from the device 
    # so we use the length as a substitute. 
    result.append({ELEM_NR_RESULTS: (total_length - 7) / 18})

    if status != 0:
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Device status != 0 (OK) (%d)' % (status))

    while buffer.nr_bytes_left() > 2:
        keys = ('length', 'index', 'sample_type', 'test_counter', 
                'timestamp', 'glucose_value', 'meal_flag', 
                'user_comment', 'battery_level', 'corrupt_record') 
        d = dict(zip(keys, buffer.get_struct('<HHBHIHBBHB')))

        record = CreateGlucoseRecord(d)
        if (record):
            result.append(record)

    crc_func = crcmod.predefined.mkCrcFun('crc-ccitt-false')
    calc_crc = crc_func(buffer.get_slice_from_mark())
    crc = buffer.get_struct('<H')

    if crc[0] != calc_crc:
        raise Exception(ERROR_CODE_CHECKSUM, 'Incorrect CRC 0x%x != 0x%x' % (crc[0], calc_crc))

    return result

def DetectMeterOneTouchBGMDLL(data):
    '''
    Check if data comes from OneTouch BGM DLL. 
    '''
    return DetectDevice('OneTouchBGMDLL', data, DEVICE_METER)

def AnalyseMeterOneTouchBGMDLL(data):
    '''
    Analyse data from OneTouch BMG DLL.
    '''

    model_serial_list = ['', '']
    data_list = []
    try:
        data_list = SplitData(model_serial_list, data)

        d = {}
        d[ "eval_serial_record" ]       = EvalSerialRecord
        d[ "eval_unit"]                 = EvalUnitRecord
        d[ "eval_result_record" ]       = EvalResultRecord
        d[ "eval_checksum_record" ]     = EvalChecksumRecord
        d[ "eval_nr_results" ]          = EvalNrResultsRecord
        d[ "eval_device_model_record" ] = EvalDeviceModelRecord
     
        res_list = AnalyseGenericMeter(data_list, d)

    except Exception as e:

        # Uncomment for developement. 
        #import traceback
        #traceback.print_exc()
        
        # The 'standard' way is to raise Exception with two parameters, diasend error code and 
        # description.
        if len(e.args) == 2:
            res_list = CreateErrorResponseList(model_serial_list[1], model_serial_list[0], e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            res_list = CreateErrorResponseList(model_serial_list[1], model_serial_list[0], ERROR_CODE_PREPROCESSING_FAILED, 
                ''.join(map(str, e.args)))

    return res_list

if __name__ == "__main__":

    import pprint 

    test_files = ['OneTouchInvue_JAGQL025.log',
        'OneTouchVerio_XBGDT08M.log',
        'OneTouchVerio_XBGDT0BF.log',
        'OneTouchVerio_XBGPH27W.log',
        'OneTouchInvue_JAGQL026.log',
        'OneTouchVerio_XBGDT09P.log',
        'OneTouchVerio_XBGNN2NP.log',
        'OneTouchVerio_XBGPH2B9.log',
        'OneTouchSelectPlus-Eserial.log',
        'GAHTQRNX.log',]

    for test_file in test_files:
        with open('test/testcases/test_data/OneTouchBGMDLL/%s' % (test_file), 'rb') as f:
            data = f.read()
            results = AnalyseMeterOneTouchBGMDLL(data)
            print results[0]['header']
            #for r in results[0]['results']:
            #    print r
