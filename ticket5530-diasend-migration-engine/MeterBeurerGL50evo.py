# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2016 Diasend AB, http://diasend.com
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE beurer GL 50 evo
# 
import re
import itertools
import struct
from datetime import datetime
import string

import Common
from Generic import *
from Defines import *
import Buffer

class BeurerGL50evoAnalyser(object):

    def __init__(self, data):

        self.result = []

        self.inc_nr_results = 0

        # List index n tag, index n+1 tag payload
        items = re.split(r'!TAG:([A-Z]*):', data)[1:]

        # Unordered dictionary - key tag, value payload
        d = dict(zip(*[iter(items)]*2))

        if 'SERIAL' in d:
            # Remove non-printable characters
            serial = filter(lambda x: x in string.printable, d['SERIAL'])
            self.result.append({ELEM_DEVICE_SERIAL: serial})

        if 'MODEL' in d:
            # Remove non-printable characters
            model = filter(lambda x: x in string.printable, d['MODEL'])
            if model == 'GL50evo':
                self.result.append({ELEM_DEVICE_MODEL: 'Beurer GL50 evo'})

        nr_results = 0

        if 'DATA' in d:

            # Convert BCD characters to int
            def bcd(chars):
                res = 0 
                for char in chars:
                    for val in (ord(char) >> 4, ord(char) & 0x0f):
                        if val > 9:
                            raise ValueError('Invalid BCD character')
                        res = res * 10 + val
                return res

            buffer = Buffer.Buffer(d['DATA'])

            # Number of values located at position 3874
            nr_results = buffer.peek_struct('H', 3874)[0]
            self.result.append({ELEM_NR_RESULTS: nr_results})

            for index in range(nr_results):

                bcd_str = buffer.get_slice(8)

                value  = bcd(bcd_str[0:2]) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
                year   = bcd(bcd_str[2:3])
                month  = bcd(bcd_str[3:4])
                day    = bcd(bcd_str[4:5])
                hour   = bcd(bcd_str[5:6])
                minute = bcd(bcd_str[6:7])
                marker = bcd(bcd_str[7:8])

                flags = []

                flags.append(FLAG_BEFORE_MEAL)  if marker == 2 else None
                flags.append(FLAG_AFTER_MEAL)   if marker == 3 else None
                flags.append(FLAG_GENERAL)      if marker == 4 else None

                mg_value = bcd(bcd_str[0:2])
                if mg_value > 630:
                    flags.append(FLAG_RESULT_HIGH)
                elif mg_value < 20:
                    flags.append(FLAG_RESULT_LOW)

                self.result.append({
                    ELEM_VAL_TYPE  : VALUE_TYPE_GLUCOSE,
                    ELEM_TIMESTAMP : datetime(2000 + year, month, day, hour, minute, 0),
                    ELEM_FLAG_LIST : flags,
                    ELEM_VAL       : value})

            bcd_str = buffer.get_slice(8)

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalBeurerGL50evoSerialRecord(line):
    return line if ELEM_DEVICE_SERIAL in line else {}

def EvalBeurerGL50evoUnitRecord(line):
    return { ELEM_DEVICE_UNIT:"mg/dL" }
    
def EvalBeurerGL50evoModelRecord(line):
    return line if ELEM_DEVICE_MODEL in line else {}

def EvalBeurerGL50evoResultRecord(line):
    return line if ELEM_VAL in line else {}

def EvalBeurerGL50evoChecksumRecord(line, record):
    return True

def EvalBeurerGL50evoChecksumFile(line):
    return True

def EvalBeurerGL50evoNrResultsRecord(line):
    return line if ELEM_NR_RESULTS in line else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterBeurerGL50evo(inList):
    """
    Detect if data comes from beurer GL50 evo
    """
    return DetectDevice('beurerGL50evo', inList, DEVICE_METER);

def AnalyseMeterBeurerGL50evo(inData):
    """
    Analyse beurer GL50 evo
    """

    d = {}

    d[ "eval_device_model_record" ]  = EvalBeurerGL50evoModelRecord
    d[ "eval_serial_record" ]        = EvalBeurerGL50evoSerialRecord
    d[ "eval_unit"]                  = EvalBeurerGL50evoUnitRecord
    d[ "eval_result_record" ]        = EvalBeurerGL50evoResultRecord
    d[ "eval_checksum_record" ]      = EvalBeurerGL50evoChecksumRecord
    d[ "eval_checksum_file" ]        = EvalBeurerGL50evoChecksumFile
    d[ "eval_nr_results" ]           = EvalBeurerGL50evoNrResultsRecord

    analyser = BeurerGL50evoAnalyser(inData)

    resList = AnalyseGenericMeter(analyser.result, d);
    
    return resList

if __name__ == "__main__":

    testfiles = (
        'A24_005873.log',
        'A25_008136.log',
        'A24_005872.log',
        'BeurerGL50evo-after-meal.log'
        )

    for testfile in testfiles:
        with open('test/testcases/test_data/beurerGL50evo/%s' % (testfile), 'r') as f:
            d = f.read()
            results = AnalyseMeterBeurerGL50evo(d)
            print results[0]['header']
            for r in results[0]['results']:
                print r
