# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2007 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Haemedic GlucoSure Extra
# @DEVICE Haemedic GlucoSure Max

import re
from Generic import *
from Defines import *
from datetime import *

import Common

RECORD_LINE_SIZE = 9
RECORD_LINES     = 4

# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------


def SplitData( inData ):
    """
    Splits incoming data into a list. The data consists of a number of results record,
    serial number and 4 * 9 bytes large result record
    """

    inList  = []

    # The start of the body is the serial record, terminated by \n
    index = inData.find("\n");
    if index > 0:
        inList.append(inData[:index + 1])
        
        body = inData[index + 1:]

        # Next part is the number result record terminated by \n
        index = body.find("\n");
        if index > 0:
            inList.append(body[:index + 1])

            body = body[index + 1:]

            # The rest consists of 4 x 9 bytes records
            inList += Common.SplitCount(body, RECORD_LINES * RECORD_LINE_SIZE)

    return inList

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Haemedic Glucosure. Extracted groups : 
    group 1 : serial number
    
    Example:
    '!TAG!SERNO!A1000002\n'
    """
    m = re.match( r'^!TAG!SERNO!(.*)\n', line, re.IGNORECASE )
    return m



    m = re.match( r'^\x021H\|.*\|.*\|.*\|GT-1910\^\d+\.\d+\\.*\^1910-(.*)\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\|.*\r', line, re.IGNORECASE )

    return m

def EvalResultRecord( line ):
    """
    Evaluate a Haemedic Glucosure result record. Extract groups : 
    group 1 : Memory index
    group 2 : Checksum 1
    group 3 : Date
    group 4 : Checksum 2
    group 5 : time
    group 6 : Checmsum 3
    group 7 : Value and flags
    group 8 : checksum 4

    example:
    '\x02v000177\r\x02w0F5BE1\r\x02x73806B\r\x02y004BC4\r'
        
    """
    if len(line) != (RECORD_LINE_SIZE * RECORD_LINES):
        return None
    
    m = re.match( r'^\x02v([\d|A-F]{4})([\d|A-F]{2})\r\x02w([\d|A-F]{4})([\d|A-F]{2})\r\x02x([\d|A-F]{4})([\d|A-F]{2})\r\x02y([\d|A-F]{4})([\d|A-F]{2})\r', line, re.IGNORECASE )
    return m

def EvalNrResultRecord( line ):
    """
    Evaluate a Haemedic Glucosure nr of result record. Extract groups : 
    group 1 : Number of results

    example

    !TAG!RESULTS!300\n
    """

    m = re.match( r'^!TAG!RESULTS!(\d{1,3})', line, re.IGNORECASE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalHaemedicGlucosureSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    
    # This info is not received from the manufacturer yet, return a bogus
    
    res = {}
    m = EvalSerialRecord( line )
    
    # There was an old series of meters without serial number which had the memory set to 
    # 0xFFFFFFFF -> we can not allow this number since all of them have the same number
    if m and m.group(1) != 'FFFFFFFF':
        res[ "meter_serial" ] = m.group(1)
    return res
    
def EvalHaemedicGlucosureUnitRecord( line ):
    """
    Always return mg/dl 
    """
    res = { "meter_unit":"mg/dL" }
    return res

def EvalHaemedicGlucosureResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    ELEM_TIMESTAMP  > date in datetime object
    ELEM_VAL        > value (float) 
    ELEM_VAL_TYPE   > type of value (VALUE_TYPE_GLUCOSE)
    meter_flaglist > list of flags (int) if present
    """
    
    res = {}
    m = EvalResultRecord( line )
     
    if m:
        # Check if the result is empty
        if m.group(3) == 'FFFF' and m.group(5) == 'FFFF' and m.group(7) == 'FFFF':
            return {"null_result":True} 
        
        record_date = int(m.group(3), 16)
        year  = record_date >> 9
        month = (record_date >> 5) & 0xf
        day   = record_date & 0x1f
        
        record_time = int(m.group(5), 16)        
        hour = (record_time >> 6) & 0x1f
        minute = record_time & 0x3f 
        
        try:
            res[ ELEM_TIMESTAMP ] = datetime(2000 + year, month, day, hour, minute)
        except:
            res[ ELEM_TIMESTAMP ] = None

        record_value = int(m.group(7), 16)

        flags = record_value >> 13
                
        flag_list = []
        res["meter_flaglist"] = flag_list

        # Check flags byte 
        # Bit 0x01 - Control value
        # Bit 0x02 - Low
        # Bit 0x04 - High

        if flags & 0x01:
            flag_list.append(FLAG_RESULT_CTRL)

        if flags & 0x02:
            flag_list.append(FLAG_RESULT_LOW)    

        if flags & 0x04:
            flag_list.append(FLAG_RESULT_HIGH)
        
        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        res[ ELEM_VAL ]  = float(record_value & 0x3ff)            
    
    return res

def EvalHaemedicGlucosureChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    
    Each line of the record has a checksum
    """
    chunk = line
    
    while len(chunk) >= RECORD_LINE_SIZE:
        high_byte = int(chunk[2:4], 16)
        low_byte = int(chunk[4:6], 16)
        calculated_checksum = (ord(chunk[1]) + high_byte + low_byte) & 0xff;
        record_checksum = int(chunk[6:8], 16)
        
        if record_checksum != calculated_checksum:
            return False
        
        chunk = chunk[RECORD_LINE_SIZE:]
    
    return True

def EvalHaemedicGlucosureChecksumFile( inList ):
    """
    Evaluate checksum of result record.
    
    Not supported by this mete
    """
    return True

def EvalHaemedicGlucosureNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalNrResultRecord( line )
    if m:
        try:
            res[ "meter_nr_results" ] = int( m.group(1), 10 )
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
    return res


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectHaemedicGlucosure( inList ):
    """
    Detect if data comes from a Haemedic Glucosure.
    """
    return DetectDevice( 'HaemedicGlucosure', inList, DEVICE_METER );

def AnalyseHaemedicGlucosure( inData ):
    """
    Analyse Haemedic Glucosure
    """

    inList = SplitData(inData)
    
    # Empty dictionary
    d = {}

    d[ "meter_type" ]           = "GlucoSure Extra"
    d[ "eval_serial_record" ]   = EvalHaemedicGlucosureSerialRecord
    d[ "eval_unit"]             = EvalHaemedicGlucosureUnitRecord
    d[ "eval_result_record" ]   = EvalHaemedicGlucosureResultRecord
    d[ "eval_checksum_record" ] = EvalHaemedicGlucosureChecksumRecord
    d[ "eval_checksum_file" ]   = EvalHaemedicGlucosureChecksumFile
    d[ "eval_nr_results" ]      = EvalHaemedicGlucosureNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
        
    body =  '!TAG!SERNO!A1000002\n!TAG!RESULTS!44\n\x02v000177\r\x02w0F5BE1\r\x02x73806B\r\x02y004BC4\r\x02v000278\r\x02w0F5BE1\r\x02xB37BA6\r\x02y01F16B\r\x02v000379\r\x02w0F5BE1\r\x02x637B56\r\x02y0051CA\r\x02v00047A\r\x02w0F5BE1\r\x02x737A65\r\x02y02138E\r\x02v00057B\r\x02w0F5BE1\r\x02xD37AC5\r\x02y022CA7\r\x02v00067C\r\x02w0F5BE1\r\x02x337924\r\x02y011993\r\x02v00077D\r\x02w0F5BE1\r\x02xD374BF\r\x02y01148E\r\x02v00087E\r\x02w0F5BE1\r\x02x53733E\r\x02y011D97\r\x02v00097F\r\x02w0F5BE1\r\x02x0372ED\r\x02y0032AB\r\x02v000A80\r\x02w0F5BE1\r\x02x83716C\r\x02y004AC3\r\x02v000B81\r\x02w0F5BE1\r\x02x83716C\r\x02y013AB4\r\x02v000C82\r\x02w0F5BE1\r\x02xB36E99\r\x02y012CA6\r\x02v000D83\r\x02w0F5AE0\r\x02x536E39\r\x02y0042BB\r\x02v000E84\r\x02w0F59DF\r\x02xC36DA8\r\x02y002FA8\r\x02v000F85\r\x02w0F5FE5\r\x02x336914\r\x02y00269F\r\x02v001086\r\x02w0F5FE5\r\x02x236803\r\x02y0046BF\r\x02v001187\r\x02w0F5EE4\r\x02x0364DF\r\x02y025AD5\r\x02v001288\r\x02w0F5DE3\r\x02x63643F\r\x02y0147C1\r\x02v001389\r\x02w0F5DE3\r\x02xC3639E\r\x02y0133AD\r\x02v00148A\r\x02w0F5CE2\r\x02xC3629D\r\x02y0054CD\r\x02v00158B\r\x02w0F5BE1\r\x02x535F2A\r\x02y0041BA\r\x02v00168C\r\x02w0F5AE0\r\x02x535F2A\r\x02y0051CA\r\x02v00178D\r\x02w0F59DF\r\x02x235FFA\r\x02y0136B0\r\x02v00188E\r\x02w0F58DE\r\x02x535E29\r\x02y0144BE\r\x02v00198F\r\x02w0F57DD\r\x02xD35EA9\r\x02y014DC7\r\x02v001A90\r\x02w0F56DC\r\x02xF35DC8\r\x02y004EC7\r\x02v001B91\r\x02w0F55DB\r\x02xC35D98\r\x02y00249D\r\x02v001C92\r\x02w0F54DA\r\x02xF35CC7\r\x02y0041BA\r\x02v001D93\r\x02w0F53D9\r\x02x435B16\r\x02y004EC7\r\x02v001E94\r\x02w0F52D8\r\x02xE35BB6\r\x02y010882\r\x02v001F95\r\x02w0F51D7\r\x02x135AE5\r\x02y01BC36\r\x02v002096\r\x02w0F50D6\r\x02xC3548F\r\x02y0040B9\r\x02v002197\r\x02w0F4FD5\r\x02x3353FE\r\x02y010F89\r\x02v002298\r\x02w0F4ED4\r\x02x43530E\r\x02y011E98\r\x02v002399\r\x02w0F4DD3\r\x02x93525D\r\x02y001C95\r\x02v00249A\r\x02w0F4DD3\r\x02xB34E79\r\x02y01F06A\r\x02v00259B\r\x02w0F4CD2\r\x02x234DE8\r\x02y011993\r\x02v00269C\r\x02w0F4BD1\r\x02xC34D88\r\x02y01017B\r\x02v00279D\r\x02w0F4AD0\r\x02xD34C97\r\x02y061C9B\r\x02v00289E\r\x02w0F49CF\r\x02x134CD7\r\x02y000079\r\x02v00299F\r\x02w0F48CE\r\x02x534B16\r\x02y004AC3\r\x02v002AA0\r\x02w0F47CD\r\x02x134AD5\r\x02y01F46E\r\x02v002BA1\r\x02w0F46CC\r\x02xD34A95\r\x02y011791\r\x02v002CA2\r\x02w0F45CB\r\x02x0349C4\r\x02y0143BD\r'

    print AnalyseHaemedicGlucosure(body)
