# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Changed  : $LastChangedDate$
# Revision : $LastChangedRevision$
# Author   : $LastChangedBy$
# -----------------------------------------------------------------------------

import uuid
import gzip
import time
import asynchat
import asyncore
import socket
import sys
import ssl
import pylzma

import Config
from Defines import *
from Analyse import AnalyseIncomingData
from Analyse import AnalyseIncomingHeader
from Analyse import GetLastAnalyseResult
import traceback

exec "from %s import InsertHistoryIntoDatabase" % Config.settings["database-module"]
exec "from %s import GetDBVersion" % Config.settings["database-module"]
exec "from %s import UpdateIncomingData" % Config.settings["database-module"]

from Generic import *

import Common

from simplecrypto import decrypt
import EventLog
import crcmod.predefined
import os
from Header import CHeader
from Header import GetProtVer
import zlib

MAX_RECV_LENGTH = 262144
encrypted = True

object_counter = 0
		

# -----------------------------------------------------------------------------
# Local Functions
# -----------------------------------------------------------------------------

def ValidateBlockChecksum(obj_idx, block, pos_eob):
    
    # extract checksum, the checksum is either 4 bytes -> CRC16, or 8 bytes ->CRC32
    checksum_start = block[:pos_eob].rfind('!')
    
    if checksum_start < 0:
        EventLog.EventLogError('#%d:\tFailed to find checksum - send NAK' % obj_idx)
        return False

    checksum_str = block[checksum_start + 1:pos_eob]
    EventLog.EventLogInfo('#%d:\tExtracted checksum %s' % (obj_idx, checksum_str))

    try:
        checksum = int(checksum_str, 16)
    except:
        EventLog.EventLogError('#%d:\tFailed to convert checksum to integer - send NAK' % obj_idx)
        return False

    if (pos_eob - 6 - len(checksum_str)) < 0:
        EventLog.EventLogError('#%d:\tThis block only contains %d bytes - too little - send NAK' % (obj_idx, len(block)))
        return False

    data = block[:pos_eob - 6 - len(checksum_str)]

    # CRC16 uses 4 hex chars (16 bits), otherwhise it is CRC32 (32bits)
    if len(checksum_str) > 4:
        calculated_checksum = zlib.crc32(data)
        if calculated_checksum < 0:
            calculated_checksum = calculated_checksum + 0xffffffff + 1
    else:
        calculated_checksum = crc.calculate_crc(data)

    EventLog.EventLogInfo("#%d:\tCalculated checksum %li Extracted checksum %li" % (obj_idx, calculated_checksum,checksum))

    # checksum failed -> send nack
    if calculated_checksum != checksum:
        EventLog.EventLogError('#%d:\tIncorrect checksum in block - send NAK' % obj_idx)
        return False

    return True


# -----------------------------------------------------------------------------
# Aidera Diasend asynchronous GPRS server
# -----------------------------------------------------------------------------

class DiasendProtocolHandler( object, asynchat.async_chat ):
    """
    Diasend client protocol handler. Stores all incoming data until terminator
    is detected. When terminator is detected analyse the incoming data.
    """
    recv_block_size = 1024
    
    STATE_IDLE          = 1 # No data yet
    STATE_SSL_ACCEPTING = 2 # SSL handshake begun
    STATE_SSL_CLOSING   = 3 # SSL unwrap begun
    STATE_SSL_CONNECTED = 4 # SSL connection up and running
    STATE_CONNECTED     = 5 # Unencrypted connection up and running
    STATE_CLOSED        = 6 # Closed

    def __init__(self, conn, addr):
        global object_counter

        self.set_terminator("!EOT!DIASEND")
        self.ibuffer = ""
        self.cbuffer = ""
        self.version = 0
        self.incoming_size = 0
        self.rsp_on_eot = False
        self.header = None
        self.creation_time = int (time.time())
        self.transfer_logged = False
        self.addr = addr
        self.index = object_counter
        object_counter = object_counter + 1
        
        # Used to restart a locked SSL connection
        self.ssl_error_count = 0

        # Time when SSL accept state was entered.
        self._ssl_accepting_started = time.time()

        # State of connection. 
        self.connection_state = DiasendProtocolHandler.STATE_IDLE   

        # Kill any previously registered sockets that has been disconnected
        # from the client side. 
        self.kill_zombies()
    
        # Add ourself to the asyncore map (with the connection socket we got 
        # from the server).
        asynchat.async_chat.__init__(self, conn)

    def handle_read_event(self):
        """
        Intercept a read event to perform SSL stuff if necessary, otherwise
        call the ordinary asyncore handle_read_event.
        """
        if self.connection_state == DiasendProtocolHandler.STATE_SSL_ACCEPTING:
            self._do_ssl_handshake()
        elif self.connection_state == DiasendProtocolHandler.STATE_SSL_CLOSING:
            self._do_ssl_shutdown()
        else:
            super(DiasendProtocolHandler, self).handle_read_event()

    def handle_write_event(self):
        """
        Intercept a write event to perform SSL stuff if necessary, otherwise
        call the ordinary asyncore handle_write_event.
        """
        
        if self.connection_state == DiasendProtocolHandler.STATE_SSL_ACCEPTING:
            if (time.time() - self._ssl_accepting_started) > 10 and self.ssl_error_count == 0:
                self.ssl_error_count = 1
                EventLog.EventLogInfo('#%d:\tSSL handshake slow' % (self.index))

            if (time.time() - self._ssl_accepting_started) > 30:
                EventLog.EventLogInfo('#%d:\tSSL handshake aborted' % (self.index))
                self.connection_state = DiasendProtocolHandler.STATE_SSL_CLOSING
            else:
                self._do_ssl_handshake()

        elif self.connection_state == DiasendProtocolHandler.STATE_SSL_CLOSING:
            self.ssl_error_count = 0
            self._do_ssl_shutdown()
        else:
            self.ssl_error_count = 0
            super(DiasendProtocolHandler, self).handle_write_event()

    def secure_connection(self):
        """
        Upgrade our socket to an encrypted socket. Note that no actual SSL 
        handshaking starts here - the actual work is done in the 
        handle_read_event and handle_write_event functions.
        """
        EventLog.EventLogInfo('#%d:\tSSL handshake begin' % (self.index))
        certfile    = os.path.join(sys.path[0], "ssl-keys/cert.pem")
        keyfile     = os.path.join(sys.path[0], "ssl-keys/privkey.pem")
        ssl_version = ssl.PROTOCOL_TLSv1
        self.socket = ssl.wrap_socket(self.socket, 
                certfile=certfile, keyfile=keyfile, server_side=True,
                do_handshake_on_connect=False,
                ssl_version=ssl_version)
        self.connection_state = DiasendProtocolHandler.STATE_SSL_ACCEPTING
        self._ssl_accepting_started = time.time()
                
        # TODO : suppress_ragged_eofs=False)

    def _do_ssl_handshake(self):
        """
        Perform SSL handshake on a non-blocking socket.
        """
        try:
            self.socket.do_handshake()
        except ssl.SSLError, err:
            print "SSL handshake %s" % (err)
            if err.args[0] in (ssl.SSL_ERROR_WANT_READ, ssl.SSL_ERROR_WANT_WRITE):
                return
            elif err.args[0] == ssl.SSL_ERROR_EOF:
                return self.handle_close()
            elif err.args[0] == ssl.SSL_ERROR_SSL:
                self.handle_failed_ssl_handshake()
            else:
                raise
        else:
            EventLog.EventLogInfo('#%d:\tSSL connected' % (self.index))
            self.connection_state = DiasendProtocolHandler.STATE_SSL_CONNECTED

    def _do_ssl_shutdown(self):
        """
        Perform SSL shutdown on a non-blocking socket.
        """
        self.connection_state = DiasendProtocolHandler.STATE_SSL_CLOSING
        try:
            self.socket = self.socket.unwrap()
        except ssl.SSLError, err:
            #print "SSL shutdown %s" % (err)
            if err.args[0] in (ssl.SSL_ERROR_WANT_READ, ssl.SSL_ERROR_WANT_WRITE):
                return
            elif err.args[0] == ssl.SSL_ERROR_SSL:
                pass
            else:
                raise
        except socket.error, err:
            # Any "socket error" corresponds to a SSL_ERROR_SYSCALL
            # return from OpenSSL's SSL_shutdown(), corresponding to
            # a closed socket condition. See also:
            # http://www.mail-archive.com/openssl-users@openssl.org/msg60710.html
            pass
        self.connection_state = DiasendProtocolHandler.STATE_CLOSED
        super(DiasendProtocolHandler, self).close()

    def handle_failed_ssl_handshake(self):
        """
        What to do here? TODO!
        """
        raise NotImplementedError("must be implemented in subclass")

    def send(self, data):
        """
        Send data - this function is added to handle SSL errors that asyncore 
        can't handle. 
        """
        try:
            return super(DiasendProtocolHandler, self).send(data)
        except ssl.SSLError, err:
            if err.args[0] in (ssl.SSL_ERROR_EOF, ssl.SSL_ERROR_ZERO_RETURN):
                return 0
            raise

    def recv(self, buffer_size):
        """
        Receive data - this function is added to handle SSL errors that asyncore 
        can't handle. 
        """
        try:
            return super(DiasendProtocolHandler, self).recv(buffer_size)
        except ssl.SSLError, err:
            if err.args[0] in (ssl.SSL_ERROR_EOF, ssl.SSL_ERROR_ZERO_RETURN):
                self.handle_close()
                return ''
            if err.args[0] in (ssl.SSL_ERROR_WANT_READ,
                                ssl.SSL_ERROR_WANT_WRITE):
                return ''
            raise

    def collect_incoming_data(self, data):
        """called whenever incoming data arrives"""
        # print "--->", repr(data)
        self.ibuffer = self.ibuffer + data
        self.incoming_size += len( data )
        # if too much data - throw away
        if len( self.ibuffer ) > MAX_RECV_LENGTH:
            terminal_serial = 'unknown'
            if self.header:
                terminal_serial = self.header.getTerminalSerial()
            EventLog.EventLogWarning('#%d: Received too much data - throw away' % self.index)
            InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Failed : Too much data received' )
            self.send_nak()
            self.close_when_done()

    def found_terminator(self):
        """called when terminator found in incoming data"""
        if self.connection_state == DiasendProtocolHandler.STATE_IDLE:
            if "STARTTLS" in self.ibuffer:
                self.send_data( RSP_OK )
                self.secure_connection()
                self.ibuffer = ""
            else:
                self.connection_state = DiasendProtocolHandler.STATE_CONNECTED
                self.found_data()
        else:
            self.found_data()

    def found_data(self):
        """called when terminator found in incoming data"""
        global encrypted
        
        terminal_serial = 'unknown'

        EventLog.EventLogInfo('#%d: Received data of size: %d' % (self.index, len(self.ibuffer)))

        try:
            # This is the first complete chunk of data, must contain header tags
            if not self.header:
                
                EventLog.EventLogInfo('#%d:\tHeader block: %s' % (self.index, repr(self.ibuffer)))
                
                # First part, check protocol version
                prot_ver = GetProtVer(self.ibuffer)
                
                # We only support major 2
                if (prot_ver == None) or (prot_ver["major"] < 2):
                    # Unsupported version
                    # Protocol version 1 is now depricated
                    ver_str = "Unknown"
                    if prot_ver:
                        ver_str = str(prot_ver["major"])
                    EventLog.EventLogError('#%d:\tTerminal is running unsupported protocol version: %s' % (self.index, ver_str))

                    InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Failed : Depricated Protocol version: %s' % (ver_str) )
                    self.send_nak()
                    self.transfer_logged = True

                    if self.connection_state == DiasendProtocolHandler.STATE_SSL_CONNECTED:
                        self.handle_close()

                    self.close_when_done()
                    return

            # We have now stopped anything but prot version 2
            #Find end of block
            pos_eob = self.ibuffer.find( '!BLOCK' )
            pos_eot = -1
            
            if pos_eob >= 0:
                # We got end of a block, verify checksum
                if not ValidateBlockChecksum(self.index, self.ibuffer, pos_eob):
                    # Incorrect checksum, throw data and NAK
                    self.ibuffer = ""
                    self.send_nak()
                    if self.connection_state == DiasendProtocolHandler.STATE_SSL_CONNECTED:
                        self.handle_close()
                    return
            else:
                # Not an end of block, is it an end of transfer?
                # Is this end of a block? -> !XXXX!BLOCK!EOT!DIASEND where XXXX is equal to checksum
                pos_eot = self.ibuffer.find( '!TRANSFER' )


            if not self.header:
                # No header this must be the first data...
                
                # The header comes in a block, if no end of block found, NAK
                if pos_eob < 0:
                    self.ibuffer = ""
                    self.send_nak()
                    if self.connection_state == DiasendProtocolHandler.STATE_SSL_CONNECTED:
                        self.handle_close()
                    return
                
                self.header = CHeader(self.ibuffer)

                if self.header.getTerminalSerial():
                    terminal_serial = self.header.getTerminalSerial()
                    # TODO: Select database this early! Will make logs end up in the correct server

                EventLog.EventLogInfo('#%d:\tProtocol version %d.%d' % (self.index, prot_ver["major"], prot_ver["minor"]))
                
                # Check which response we should send to the header, ACK, NACK or LOAD
                EventLog.EventLogSetIndex(self.index)
                header_res = AnalyseIncomingHeader(self.header)
        
                self.send_data ( header_res["rsp_header"] )
                if header_res["rsp_header"] == RSP_NAK:
                    # We NAK:ed the header, set it to None
                    # To let the terminal have a chance to retransmit
                    self.header = None
                elif header_res["rsp_header"] == RSP_LOAD:
                    InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Load : Ordered firmware upgrade' )
                    self.transfer_logged = True

                if header_res.has_key("rsp_transfer"):
                    self.rsp_on_eot = header_res["rsp_transfer"]

                # We consumed the in buffer, consume it
                self.ibuffer = ""
                return

            else:
                # This is not the first block
                if self.header.getTerminalSerial():
                    terminal_serial = self.header.getTerminalSerial()

                prot_ver = self.header.getProtocolVersion()
                EventLog.EventLogInfo('#%d:\tCached protocol version %d.%d' % (self.index, prot_ver["major"], prot_ver["minor"]))

            if pos_eob >= 0:
                # end of block!
                
                payload_len_str = None

                # extract payload-length, its between the last two !
                payload_end = self.ibuffer[:pos_eob].rfind("!")
                if payload_end > -1:
                    payload_start = self.ibuffer[:payload_end].rfind("!")
                    if payload_start > -1:
                        payload_len_str = self.ibuffer[payload_start + 1: payload_end]
    
                if payload_len_str == None:
                    EventLog.EventLogError('#%d:\tFailed to convert payload to integer - send NAK' % self.index)
                    self.ibuffer = ""
                    self.send_nak()
                    if self.connection_state == DiasendProtocolHandler.STATE_SSL_CONNECTED:
                        self.handle_close()
                    return

                EventLog.EventLogInfo('#%d:\tExtracted payload %s' % (self.index, payload_len_str,))

                try:
                    payload_len = int(payload_len_str, 16)
                except:
                    EventLog.EventLogError('#%d:\tFailed to convert payload to integer - send NAK' % self.index)
                    self.ibuffer = ""
                    self.send_nak()
                    if self.connection_state == DiasendProtocolHandler.STATE_SSL_CONNECTED:
                        self.handle_close()
                    return

                if self.header.usingEncryption():

                    # extract encrypted block
                    encrypted_block = self.ibuffer[:payload_start]  # REMOVE !XXXX!XXXX

                    # decrypt block
                    decrypted_block = decrypt.decrypt( encrypted_block )

                    # remove stuff that shouldn't be there
                    decrypted_block = decrypted_block[:payload_len]

                else:
                    decrypted_block = self.ibuffer[:payload_start]  # REMOVE !XXXX!XXXX
        
                # send ack
                self.send_data( RSP_OK )

                # copy buffers
                self.cbuffer += decrypted_block
                self.ibuffer = ""

            # Is this end of transfer? -> TRANSFER!EOT!DIASEND
            elif pos_eot >= 0:
        
                EventLog.EventLogInfo('#%d:\tEnd of transfer' % self.index)

                result_code = 0

                prot_ver = self.header.getProtocolVersion()
                if (prot_ver["major"] > 2) or (prot_ver["major"] == 2 and prot_ver["minor"] >= 2):
                    # From version 2.2 and up the terminal sends a result code, 
                    # where 0 is OK, if not, no reason to analyse...
                    # The end of the transmission looks like this:
                    # !0000!0000!XX!TRANSFER!EOT!DIASEND
                    # Where xx is a hexadecimal number which is the response code
                    result_code = -1
                    result_end = self.ibuffer[:pos_eob].rfind("!")
                    if result_end > -1:
                        result_start = self.ibuffer[:result_end].rfind("!")
                        if result_start > -1:
                            result_code = int(self.ibuffer[result_start + 1: result_end],16)
                
                if result_code < 0:
                    # Got no result code??
                    EventLog.EventLogInfo('#%d:\tNo result code from terminal' % self.index)
                    InsertHistoryIntoDatabase(self.header.getTerminalSerial(), 'unknown', 'Failed : No result code from terminal %s' % self.header.getConnectionInfo() )
                    self.send_nak()
                elif result_code > 0:
                    EventLog.EventLogInfo('#%d:\tTerminal sent code %d' % (self.index, result_code))
                    InsertHistoryIntoDatabase(self.header.getTerminalSerial(), 'unknown', 'Failed : Terminal sent code: %d %s' % (result_code,self.header.getConnectionInfo()) )
                    self.send_nak()
                else:
                    # Fine no problem reported from the terminal
                    if self.rsp_on_eot:
                        self.send_data( self.rsp_on_eot )
                        EventLog.EventLogInfo('#%d:\tFast response: %s' % (self.index, self.rsp_on_eot))
                    else:
                        comp_len = len(self.cbuffer)
                        comp_algo = None
                        # If compression is used, first uncompress the data.
                        if self.header.usingZlibCompression():
                            comp_algo = "zlib"
                            self.cbuffer = zlib.decompress(self.cbuffer)
                        elif self.header.usingLzmaCompression():
                            comp_algo = "lzma"
                            self.cbuffer = lzma.decompress(self.cbuffer)

                        if comp_algo:
                            EventLog.EventLogInfo("#%d:\tUncompressed %s buffer %d (%d)" % (self.index, comp_algo, comp_len, len(self.cbuffer)))

                        try:
                            datapath = Config.settings["log-datapath"].rstrip()
                        except KeyError, e:
                            datapath = '/tmp'

                        unique_filename = datapath+'/LOG_DT_%s_SEQ_%d_%s.log.gz' % (datetime.now().strftime('%Y-%m-%d_%H-%M-%S'), self.index, str(uuid.uuid4()).upper())
                        
                        EventLog.EventLogInfo('#%d:\tWriting buffer to %s' % (self.index, unique_filename))
                        
                        try:
                            compressed_file = gzip.open(unique_filename, 'wb')
                            compressed_file.write(self.cbuffer)
                            compressed_file.close()
                        except IOError:
                            # Failed to write - add it to the log instead
                            EventLog.EventLogInfo("#%d:\tBuffer for analysis:\n%s" % (self.index, repr(self.cbuffer)))
                        
                        # analyse incoming data
                        if AnalyseIncomingData(self.cbuffer, self.incoming_size, self.header):
                            self.send_data(RSP_OK)
                        else:
                            self.send_nak()

                self.cbuffer = ""
                self.ibuffer = ""
                self.transfer_logged = True

                self.handle_close()

            else:
                EventLog.EventLogError('#%d:\tShould not end up here - failed to find both block and eot markers' % self.index)
                self.cbuffer = ""
                self.ibuffer = ""
                self.send_nak()

        except:
            # Got exception, log and NAK
            EventLog.EventLogError('#%d:\tGot exception during analyse of data: %s' % (self.index, traceback.format_exc()))
            if self.header:
                terminal_serial = self.header.getTerminalSerial()
            
            InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Failed : Got exception' )
            
            self.transfer_logged = True
            
            self.send_nak()
            self.close_when_done()

            
    def send_data(self,data):
        EventLog.EventLogInfo('#%d: Server sent %s to terminal' % (self.index, data))
        self.push(data)
    
    def send_nak(self):
        """
        Send NAK with stored error code. 
        """

        response = RSP_NAK
        if self.header != None:
            prot_ver = self.header.getProtocolVersion()
            if prot_ver["major"] >= 3:
                code, option = GetLastAnalyseResult()
                if option:
                    option = "!"+option
                else:
                    option = ""
                # !NAK!CODE!OPTION
                response +=  str("!%04d%s" % (code, option))

        print "send_nak():", response
        self.send_data(response)




    # 20-minute zombie timeout.
    zombie_timeout = 20 * 60

    def kill_zombies (self):
        now = int (time.time())
        for channel in asyncore.socket_map.values():
            if channel.__class__ == self.__class__:
                if (now - channel.creation_time) > channel.zombie_timeout:
                    terminal_serial = 'unknown'
                    device_info = ''
                    if channel.header:
                        terminal_serial = channel.header.getTerminalSerial()
                        device_info = channel.header.getConnectionInfo()
                    EventLog.EventLogError('#%d: Zombie timeout, data recv: %d' % (channel.index, channel.incoming_size))
                    if not channel.transfer_logged:
                        InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Failed : 20 minute inactivity timeout, from %s (recvd data: %d) %s' % (channel.addr[0], channel.incoming_size, device_info))
                        channel.transfer_logged = True
                    else:
                        InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Info : 20 minute inactivity timeout but server side processing is done %s' % device_info)
                        
                    channel.close()

    def handle_close(self):

        if not self.connection_state in [DiasendProtocolHandler.STATE_CONNECTED, DiasendProtocolHandler.STATE_IDLE]:
            try:
                self._do_ssl_shutdown()
            except ValueError, e:
                print str(e)
                
        terminal_serial = 'unknown'
        if self.header:
            terminal_serial = self.header.getTerminalSerial()
            UpdateIncomingData(terminal_serial, self.incoming_size)
        
        if not self.transfer_logged:
            device_info = ''
            if self.header:
                device_info = self.header.getConnectionInfo()

            InsertHistoryIntoDatabase( terminal_serial, 'unknown', 'Failed : Connection closed from %s, data not handled (recvd data: %d) %s' % (self.addr[0], self.incoming_size, device_info) )
            self.transfer_logged = True

        if self.connection_state == DiasendProtocolHandler.STATE_CONNECTED:
            # Not a SSL connection, so do a plain close
            self.close()
        
class DiasendSocket( asyncore.dispatcher ):
    """
    This class is subclassed from the asyncore.dispather class which is a wrapper
    around a low-level socket object. This class makes it possbile to add a
    network channel listening on the diasend port. Each time a connection is
    detected on the diasend port an instance of the DiasendRequestHandler is
    created and this handler then takes care of the actual data from the
    diasend client.
    """
    def __init__(self, host, port, handler):
        """
        Constructor - setup ip port for tcp/ip listening on the diasend port.
        """
        asyncore.dispatcher.__init__(self)
        self.data = ""
        self.handler = handler
        self.create_socket( socket.AF_INET, socket.SOCK_STREAM )
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(10)

    def handle_accept(self):
        """
        Handle incoming connections.
        """
        conn, addr = self.accept()
        print conn, addr
        EventLog.EventLogInfo('Incoming connection from %s %s' % (repr(addr[0]), repr(addr[1])),)
        self.handler(conn,addr)
        
try:
    if Config.settings["service-enabled"] != "true":
        print("AsyncServer.py was not started - disabled by configuration...")
        EventLog.EventLogDebug("AsyncServer.py was not started - disabled by configuration...")
        sys.exit(0) 

    EventLog.EventLogDebug('AsyncServer.py started...')
    
    db_ver = GetDBVersion()
    # Check if the database layout we intend to use is installed...
    if not db_ver or Common.GetNumericVersion(db_ver) < REQ_DB_VERSION:
        print "Incorrect database layout, we need %s, but %s is installed" % (REQ_DB_VERSION, Common.GetNumericVersion(db_ver))
        sys.exit()

    _port = int(Config.settings["server-port"])

    # python AsyncServer.py - encrypted
    # python AsyncServer.py no - unencrypted
    # python AsyncServer.py yes 50000 - encrypted port 50000
    if len(sys.argv) > 1:
        if 'no' in sys.argv[1]:
            encrypted=False
        else:
            encrypted=True
        if len(sys.argv) > 2:
            _port = int(sys.argv[2])

    # Empty string == INADDR_ANY
    d=DiasendSocket('', _port, DiasendProtocolHandler)

    EventLog.EventLogDebug('socket instance : %s' % repr(d),)
    asyncore.loop()
except:
    print "Unexpected error:", sys.exc_info()[0]

