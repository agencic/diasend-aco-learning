# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2016 Diasend AB, Sweden, http://www.diasend.com
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Abbott LibreLink

import json
import aniso8601
import zlib
import base64
import logging

#logging.basicConfig(level=logging.DEBUG)

from Generic import *

DEVICE_MODEL = 'Abbott LibreLink'

class AbbottCloudAnalyser(object):

    def __init__(self):
        self.data_points = []
        self.events = []
        self.user_device_serial = 'UNKNOWN'
        self.pcode = ''
        self.unknown_events = {}

    def split(self, data):
        data_d = json.loads(data)

        self.user_device_serial = data_d['user_device_serial']
        self.pcode = data_d['pcode']
        self.data_points.append({ELEM_DEVICE_SERIAL: self.user_device_serial})

        z = zlib.decompress(base64.b64decode(data_d['payload']))
        d = json.loads(z)
	if not 'status' in d:
           raise Exception('JSON format error (1)')

        if d['status'] == 1:
           # No values to analyse
           self.data_points = []
           return
            

        if not 'result' in d:
            raise Exception('JSON format error (2)')

        if not 'scheduledContinuousGlucoseEntries' in d['result']:
            raise Exception('JSON format error (3)')
	
	for data_entry in d['result']['scheduledContinuousGlucoseEntries']:
	    flags = [FLAG_CONTINOUS_READING]
            if data_entry['valueInMgPerDl'] >= 500:
               flags += [FLAG_RESULT_HIGH]
            if data_entry['valueInMgPerDl'] <= 40:
               flags += [FLAG_RESULT_LOW]
            self.data_points.append({
                    ELEM_VAL_TYPE : VALUE_TYPE_GLUCOSE,
                    ELEM_TIMESTAMP: aniso8601.parse_datetime(data_entry['timestamp']),
                    ELEM_VAL: int(data_entry['valueInMgPerDl']) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL,
                    ELEM_FLAG_LIST: flags,
                })

    def eval_serial(self, data):
        return data if ELEM_DEVICE_SERIAL in data else {}

    def eval_model(self, data):
        return {ELEM_DEVICE_MODEL: 'Abbott LibreLink'}
        
    def eval_class(self, data):
        return {ELEM_DEVICE_CLASS: DEVICE_CLOUD}

    def eval_unit(self, data):
        return {ELEM_DEVICE_UNIT: 'mg/dl'}

    def eval_result(self, data):
        if ELEM_VAL_TYPE in data or SETTINGS_LIST in data:
            return data
        return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterAbbottCloud(data):
    '''
    Detect if data comes from Abbott Cloud extractor.
    '''
    return DetectDevice(DEVICE_MODEL, data, DEVICE_CLOUD)

def AnalyseMeterAbbottCloud(data):
    '''
    Analyse data from Abbott cloud.
    '''
    try:

        abbott_cloud_analyser = AbbottCloudAnalyser()
        abbott_cloud_analyser.split(data)

        callbacks = {
            'eval_serial_record'        : abbott_cloud_analyser.eval_serial,
            'eval_device_model_record'  : abbott_cloud_analyser.eval_model,
            'eval_device_class_record'  : abbott_cloud_analyser.eval_class,
            'eval_unit'                 : abbott_cloud_analyser.eval_unit,
            'eval_result_record'        : abbott_cloud_analyser.eval_result,
            'eval_device_class_record'  : abbott_cloud_analyser.eval_class
        }

    except Exception, e:

        import traceback
        traceback.print_exc()

        serial_number = abbott_cloud_analyser.user_device_serial
        
        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(DEVICE_MODEL,
                                                     serial_number, e.args[0],
                                                     e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(DEVICE_MODEL,
                                                     serial_number,
                                                     ERROR_CODE_PREPROCESSING_FAILED,
                                                     ''.join(map(str, e.args)))

        return error_response

    return AnalyseGenericMeter(abbott_cloud_analyser.data_points, callbacks)

if __name__ == "__main__":

    test_files = [
        'device_data_1.json' 
    ]

    for test_file in test_files:
        with open('test/testcases/test_data/AbbottCloud/%s' % (test_file), 'rb') as f:
            data = f.read()
            result = AnalyseMeterAbbottCloud(data)
