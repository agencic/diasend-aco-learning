# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------
#  _________ _______            ________  
# /   _____/ \      \           \_____  \ 
# \_____  \  /   |   \   ______   _(__  < 
# /        \/    |    \ /_____/  /       \
#/_______  /\____|__  /         /______  /
#        \/         \/                 \/ 

# @DEVICE i-SENS CareSens N 1000 SN-3
# @DEVICE i-SENS CareSens N POP 1000 SN-3
# @DEVICE i-SENS CareSens BGMS (TEE2)
# @DEVICE i-SENS alphacheck professional NFC (Acura Plus)
# @DEVICE Arkray GLUCOCARD Shine

from Generic import *
from Defines import *
from Buffer import Buffer
from Record import BaseRecord, EnumRecord, EnumBitRecord
import datetime
import crcmod.predefined

class SN3Record(BaseRecord):
    '''
    Base class for all records.

    This class also contains a factory method for creating
    instances of the sub-classes (depending on the currently extracted
    message type).

    '''

    is_shine = False

    @classmethod
    def create(cls, buf):
        '''
        The messages has a generic part which we parse here to identify message
        type.
        The format is:
        <STX> <HDR 4 bytes> <SIZE> <CMD 4 bytes> <DATA N bytes> <CRC 2 bytes> <ETX>
        '''
        buf.set_mark()
        (stx, hdr, size, cmd) = buf.get_struct("s4sB4s")

        obj = None

        for sub_class in cls.__subclasses__():
            if sub_class.command == cmd:
                create_op = getattr(sub_class, "create_obj", None)
                # Remove 3 from the length (2 bytes CRC + ETX)
                if callable(create_op):
                    obj = create_op(buf, size - 7)
                else:
                    obj = sub_class(buf, size - 7)
                break

        if not obj:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'SN3Record : unknown message command %s' % (cmd))

        # Store the raw data and CRC to be able to verify it
        obj.data = buf.get_slice_from_mark()
        (obj.crc, obj.etx) = buf.get_struct(">Hs")

        return obj

    def convert_to_diasend(self):
        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'SN3Record : Subclass did not implement convert %s')

    def verify_crc(self):
        """
            Verify the CRC which is defined as the checksum of everything
            except the CRC itself
        """
        crc_func = crcmod.predefined.mkCrcFun('crc-ccitt-false')
        crc = crc_func(self.data + self.etx)
        return self.crc == crc

class ReadSerialNumber(SN3Record):
    command = "RSNB"
    format = ('14s',)
    variables = ('serial', )

    def get_model(self):
        serial = self.get_serial()
        if serial.startswith('A5') or serial.startswith('A9'):
            return 'i-SENS CareSens N 1000 (SN-3)'
        elif serial.startswith('A1') or serial.startswith('B5'):
            return 'i-SENS CareSens N POP 1000 (SN-3)'
        elif serial.startswith('F0'):
            return 'i-SENS CareSens BGMS/TEE2'
        elif serial.startswith('K0'):
            return 'i-SENS alphacheck professional NFC / Acura Plus'
        elif serial.startswith('E6'):
            SN3Record.is_shine = True
            return 'GlucoCard Shine'
        else:
            print self.get_serial()
            return None

    def get_serial(self):
        """
            The serial contains \r and is null terminated -> zap
        """
        return self.serial.strip().rstrip("\x00")

class ReadTimeInformation(SN3Record):
    command = "RTIM"
    format = ('6B',)
    variables = ('year', 'month', 'day', 'hour', 'minute', 'second')

    def convert_to_diasend(self):
        return None

class CurrentIndexOfGlucoseResult(SN3Record):
    command = "NCOT"
    format = ('>H',)
    variables = ('num',)

    def convert_to_diasend(self):
        return {ELEM_NR_RESULTS: self.num}

class EnumGlucoseFlags(EnumBitRecord):
    format = ('B',)
    enums  = {
        'control_solution'  : 0x01,
        'post_meal'         : 0x02,
        'low'               : 0x04,
        'high'              : 0x08,
        'fasting'           : 0x10,
        'normal'            : 0x20,
        'ketone'            : 0x40,
        'use_low_high'      : 0x80
    }

    def is_ketones(self):
        return self.bits['ketone']

    def to_diasend_list(self, is_shine=False):
        ret = []

        if self.bits['control_solution']:
            ret.append(FLAG_RESULT_CTRL)

        if not is_shine:
            if self.bits['use_low_high']:
                if self.bits['low']:
                    ret.append(FLAG_RESULT_LOW)
                if self.bits['high']:
                    ret.append(FLAG_RESULT_HIGH)
    
            if self.bits['fasting']:
                ret.append(FLAG_FASTING)

        if not self.bits['normal']:
            if self.bits['post_meal']:
                ret.append(FLAG_AFTER_MEAL)
            else:
                ret.append(FLAG_BEFORE_MEAL)

        return ret


class GlucoseResultDataTransmission(SN3Record):
    command = "GLUC"
    format = ('6B', EnumGlucoseFlags, '>H',)
    variables = ('year', 'month', 'date', 'hour', 'minute', 'second', 'flags', \
                 'result')

    def convert_to_diasend(self):
        date = datetime.datetime(2000 + self.year, self.month, self.date,
                                 self.hour, self.minute, self.second)
        flags = self.flags.to_diasend_list(SN3Record.is_shine)
        if self.flags.is_ketones() and not SN3Record.is_shine:
            val_type = VALUE_TYPE_KETONES
            val_factor = VAL_FACTOR_KETONES_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
        else:
            val_type = VALUE_TYPE_GLUCOSE
            val_factor = VAL_FACTOR_GLUCOSE_MGDL * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
            if SN3Record.is_shine:
                if self.result > 600:
                    flags.append(FLAG_RESULT_HIGH)
                elif self.result < 20:
                    flags.append(FLAG_RESULT_LOW)

        return {ELEM_TIMESTAMP : date, ELEM_VAL_TYPE : val_type, \
                ELEM_FLAG_LIST: flags, ELEM_VAL : self.result * val_factor}


class SN3DataExtractor(object):

    def __init__(self, data):
        self.__data   = data
        self.__serial = None

    def get_serial(self):
        return self.__serial

    def split(self):
        # The buffer contains of tandem packets received from the device.
        # All but the first record has the timestamp and CRC thrown away
        # The CRC is validated on the device and the timestamp is just
        # interesting once if we want to check the datetime of the device
        # this saves us 6 bytes * the number of records which are up to 15.000
        buf = Buffer(self.__data)
        # We dont need the data anymore, can potentially save RAM
        self.__data = None

        records = []

        while buf.index < len(buf):
            record = SN3Record.create(buf)
            records.append(record)

            if isinstance(record, ReadSerialNumber):
                self.__serial = record

        return records

    def EvalSN3SerialRecord( self, record ):
        """
        Is this line a serial record. If so, return a dictionary with serial
        number.
        """
        if isinstance(record, ReadSerialNumber):
            return {ELEM_DEVICE_SERIAL: record.get_serial()}
        else:
            return {}

    def EvalSN3ModelRecord( self, record ):
        """
        Is this a model record -> return a dict
        """
        if isinstance(record, ReadSerialNumber) and record.get_model():
            return {ELEM_DEVICE_MODEL: record.get_model()}
        else:
            return {}

    def EvalSN3UnitRecord( self, line ):
        """
        Always return mg/dl
        """
        return {ELEM_DEVICE_UNIT : "mg/dl"}

    def EvalSN3ResultRecord( self, record ):
        """
        Is this a result record? If so, return its dict
        """
        if isinstance(record, GlucoseResultDataTransmission):
            return record.convert_to_diasend()
        else:
            return {}

    def EvalSN3NoResultsRecord( self, record ):
        """
        Is this line a nr results. If so, return a dict.
        """
        if isinstance(record, CurrentIndexOfGlucoseResult):
            return record.convert_to_diasend()
        else:
            return {}

    def EvalSN3ChecksumRecord( self, record, dictionary ):
        """
        Evaluate checksum of result record.
        """
        return record.verify_crc()

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectiSensCareSensSN3( inList ):
    """
    Detect if data comes from a i-SENS CareSens SN-3
    """
    return DetectDevice( 'i-Sens Caresens SN-3', inList, DEVICE_METER );

def AnalyseiSensCareSensSN3( data ):
    """
    Analyse iSense Caresens SN-3
    """
    
    try:
        extractor = SN3DataExtractor(data)
        diasend_records = extractor.split()

    except Exception, e:

        import traceback
        traceback.print_exc()

        serial_record = extractor.get_serial()
        if serial_record:
            serial_number = serial_record.get_serial()
            model = serial_record.get_model()
        else:
            serial_number = 'UNKNOWN'
            model = "UNKNOWN"

        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(model, serial_number, e.args[0], e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(model, serial_number, ERROR_CODE_PREPROCESSING_FAILED,
                ''.join(map(str, e.args)))

        return error_response

    callbacks = {}

    callbacks["eval_serial_record"]       = extractor.EvalSN3SerialRecord
    callbacks["eval_device_model_record"] = extractor.EvalSN3ModelRecord
    callbacks["eval_unit"]                = extractor.EvalSN3UnitRecord
    callbacks["eval_result_record"]       = extractor.EvalSN3ResultRecord
    callbacks["eval_nr_results"]          = extractor.EvalSN3NoResultsRecord
    callbacks["eval_checksum_record"]     = extractor.EvalSN3ChecksumRecord

    return AnalyseGenericMeter(diasend_records, callbacks)


if __name__ == "__main__":

    testfiles = (
        'CareSensN1000SN3.bin',
        'CareSensNPOP1000SN3-full.bin',
        'CareSensNPOP1000SN3.bin',
        'F018120Z1885.log',
        'F018229D2851.log',
        'F018229D2852.log',
        'K010401E0002.log',
        'GlucocardShine-DE0420.log',
        'GlucocardShine-DE0421.log',
        'GlucocardShine-DE0422.log',)

    for testfile in testfiles:
        with open('test/testcases/test_data/iSensCareSensSN3/%s' % (testfile), 'r') as f:
            d = f.read()
            results = AnalyseiSensCareSensSN3(d)
            print testfile, results[0]['header']
            #for r in results[0]['results']:
            #    print r

