# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re

from Defines import *
from Generic import *

MODEL_ABBOTT_FREESTYLE_INSULINX = 'Abbott FreeStyle InsuLinx'

class AnalyseInsulinx(object):

    @staticmethod
    def is_of_model(commands):
        for command in commands:
            if len(AnalyseInsulinx.convert_command_serlnum(command)) > 0:
                return True
        return False
        
    @staticmethod
    def convert_result_to_diasend(command):
        command_to_extract_dict = {
            'serlnum'  : AnalyseInsulinx.convert_command_serlnum, 
            'foodunits': AnalyseInsulinx.convert_command_foodunits, 
            'carbratio': AnalyseInsulinx.convert_command_carbratio, 
            'svgsratio': AnalyseInsulinx.convert_command_svgsratio, 
            'insdose'  : AnalyseInsulinx.convert_command_insdose, 
            'cttype'   : AnalyseInsulinx.convert_command_cttype, 
            'bgdrop'   : AnalyseInsulinx.convert_command_bgdrop, 
            'bgtrgt'   : AnalyseInsulinx.convert_command_bgtrgt, 
            'bgtgrng'  : AnalyseInsulinx.convert_command_bgtgrng, 
            'inslog'   : AnalyseInsulinx.convert_command_inslog, 
            'actinscal': AnalyseInsulinx.convert_command_actinscal, 
            'tagsenbl' : AnalyseInsulinx.convert_command_tagsenbl, 
            'event'    : AnalyseInsulinx.convert_command_event, 
            'result'   : AnalyseInsulinx.convert_command_result, 
            'resmtrcfg': AnalyseInsulinx.convert_command_resmtrcfg
        }

        if command.get_name() in command_to_extract_dict:
            return command_to_extract_dict[command.get_name()](command)

        return []

    @staticmethod
    def create_single_value_setting(row, valid_range, setting_id):
        """
        Helper function that extracts a row which holds a single digit value in the 
        supported range (list with the supported values, e.g. [0, 1]).
        """
        valid_range_string = "".join("%s" % i for i in valid_range)
        m = re.match(r'^([%s])\r\n$' % valid_range_string, row, re.IGNORECASE)
        if m:
            settings = {setting_id:int(m.group(1))}
            return {SETTINGS_LIST: settings}

        return None

    @staticmethod
    def convert_command_serlnum(command):
        result = []

        if command.get_name() != 'serlnum':
            return result

        if command.nr_of_records() != 1:
            return result

        record = command.get_records()[0]

        m = re.match(r'^(.{1,13})', record, re.IGNORECASE)
        if m != None:
            result.append({ELEM_DEVICE_SERIAL:m.group(1)})

            if m.group(1).startswith('LAG') or m.group(1).startswith('LAM'):
                result.append({ELEM_DEVICE_MODEL: MODEL_ABBOTT_FREESTYLE_OPTIUM_NEO})
            elif m.group(1).startswith('JAG') or m.group(1).startswith('JAM'):
                result.append({ELEM_DEVICE_MODEL: MODEL_ABBOTT_FREESTYLE_INSULINX})
            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Unknown device model with serial number' % (record))

            if m.group(1)[2] == 'G':
                result.append({SETTINGS_LIST: { SETTING_BG_UNIT: SETTING_BG_UNIT_MGDL }})
            elif m.group(1)[2] == 'M':
                result.append({SETTINGS_LIST: { SETTING_BG_UNIT: SETTING_BG_UNIT_MMOLL }})

        return result

    @staticmethod
    def convert_command_foodunits(command):
        """
        Get the food units (by grams of carbs or by servings) in the insulin calculator.

        Y\r\n where:

        Y is a number between 0 and 1 representing food entered by grams of carbs or by serving
            0 = by Grams of carbs
            1 = by Servings

        """
        result = []

        if command.get_name() != 'foodunits':
            return result

        if command.nr_of_records() != 1:
            return result

        setting = AnalyseInsulinx.create_single_value_setting(command.get_records()[0], [0, 1], SETTING_INSULIN_CALCULATOR_FOOD_UNIT)
        if setting:
            result.append(setting)

        return result

    @staticmethod
    def convert_command_carbratio(command):
        """
        Get carbohydrate insulin ratio and the status of mode of time.

        X,Y\r\n where:

        X is a number between 0 and 4 representing T\r\n
            0 = No fixed time 1 = Morning
            2 = Midday
            3 = Evening
            4 = Night
        
        Y is a number between 1 and 50 representing carbohydrate ratio per grams 

        T is a number between 0 and 1 representing the time mode
            0 = "No fixed time" used
            1 = Fixed time used (Morning/Midday/Evening/Night)
        """
        result = []

        if command.get_name() != 'carbratio':
            return result

        for record in command.get_records():

            m = re.match(r'^([01234]),(\d+)\r\n$', record, re.IGNORECASE)
            if m:
                settings = {SETTING_CARB_INSULIN_RATIO+int(m.group(1)) : int(m.group(2))}
                result.append({SETTINGS_LIST: settings})
            else:
                m = re.match(r'^([01])\r\n$', record, re.IGNORECASE)
                if m:
                    settings = {SETTING_CARB_INSULIN_RATIO_TIME_MODE:int(m.group(1))}
                    result.append({SETTINGS_LIST: settings})

        return result

    @staticmethod
    def convert_command_svgsratio(command):
        """
        Get the Carbohydrate Insulin Ratio per Serving and the status of mode of time.

        Additional Response:
        
        Y ,X\r\n
        T\r\n
        where:
        
        Y is a number between 0 and 4 representing
            0 = No fixed time 
            1 = Morning
            2 = Midday
            3 = Evening
            4 = Night
        X is a number between 1 and 30 representing carbohydrate insulin ratio per serving. 
        1 unit represents 0.5 units of insulin per carbohydrate ratio per serving. 

        T is a number between 0 and 1 representing the time mode
            0 = "No Fixed time" used
            1 = Fixed time used (Morning/Midday/Evening/Night)
        """
        result = []


        if command.get_name() != 'svgsratio':
            return result

        for record in command.get_records():

            m = re.match(r'^([01234]),(\d+)\r\n$', record, re.IGNORECASE)
            if m:
                settings = {SETTING_CARB_INSULIN_PER_SERVING_RATIO+int(m.group(1)) : int(m.group(2))}
                result.append({SETTINGS_LIST: settings})
            else:
                m = re.match(r'^([01])\r\n$', record, re.IGNORECASE)
                if m:
                    settings = {SETTING_CARB_INSULIN_PER_SERVING_RATIO_TIME_MODE:int(m.group(1))}
                    result.append({SETTINGS_LIST: settings})

        return result


    @staticmethod
    def convert_command_insdose(command):
        """
        Get Insulin Calculator Dose Unit.

        X is a number between 0 and 1 representing dose increment rate
            1 unit = 1
            0.5 units = 0
        """
        result = []

        if command.get_name() != 'insdose':
            return result

        for record in command.get_records():
            setting = AnalyseInsulinx.create_single_value_setting(record, [0, 1], SETTING_INSULIN_CALCULATOR_DOSE_UNIT)
            if setting:
                result.append(setting)
            
        return result

    @staticmethod
    def convert_command_cttype(command):
        """
        Get the type of correction target (Single Target or Range Target). 

        Additional Response:
        Y\r\n where:

        Y is a number between 0 and 1 representing the status of the correction target status of the correction setup
            0 = Single Target
            1 = Target Range
        """
        result = []

        if command.get_name() != 'cttype':
            return result

        for record in command.get_records():
            setting = AnalyseInsulinx.create_single_value_setting(command.get_records()[0], [0, 1], SETTING_TYPE_OF_CORRECTION_TARGET)
            if setting:
                result.append(setting)

        return result

    @staticmethod
    def convert_command_bgdrop(command):
        """
        Get Blood Glucose drop (Correction Factor) and the status of mode of time.

        Additional Response:
        Y,X\r\n where:

        Y is a number between 0 and 4 representing T\r\n
            0 = No fixed time 1 = Morning
            2 = Midday
            3 = Evening
            4 = Night
        
        X is a number between 1 and 99 representing Blood Glucose drop (correction factor)

        T is a number between 0 and 1 representing the time mode
            0 = "No fixed time" used
            1 = Fixed time used (Morning/Midday/Evening/Night)
        """
        result = []

        if command.get_name() != 'bgdrop':
            return result

        for record in command.get_records():
            m = re.match(r'^([01234]),(\d+)\r\n$', record, re.IGNORECASE)
            if m:
                settings = {SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR+int(m.group(1)) : int(m.group(2))}
                result.append({SETTINGS_LIST: settings})
            else:
                m = re.match(r'^([01])\r\n$', record, re.IGNORECASE)
                if m:
                    settings = {SETTING_BLOOD_GLUCOSE_DROP_CORRECTION_FACTOR_TIME_MODE:int(m.group(1))}
                    result.append({SETTINGS_LIST: settings})

        return result

    @staticmethod
    def convert_command_bgtrgt(command):
        """
        Get Correction Blood Glucose Single Target and the status of mode of time
        Additional Response:
        Y,X\r\n where:
        
        Y is a number between 0 and 4 representing T\r\n
            0 = No fixed time 
            1 = Morning
            2 = Midday
            3 = Evening
            4 = Night
        
        X is a number between 70 and 180 representing Blood Glucose Single target T is a number between 0 and 1 
        representing the time mode
            0 = "No fixed time" used
            1 = Fixed time used (Morning/Midday/Evening/Night)
        """
        result = []

        if command.get_name() != 'bgtrgt':
            return result

        for record in command.get_records():
            m = re.match(r'^([01234]),(\d+)\r\n$', record, re.IGNORECASE)
            if m:
                settings = {SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET+int(m.group(1)) : int(m.group(2))}
                result.append({SETTINGS_LIST: settings})
            else:
                m = re.match(r'^([01])\r\n$', record, re.IGNORECASE)
                if m:
                    settings = {SETTING_BLOOD_CORRECTION_GLUCOSE_SINGLE_TARGET_TIME_MODE:int(m.group(1))}
                    result.append({SETTINGS_LIST: settings})

        return result

    @staticmethod
    def convert_command_bgtgrng(command):
        """
        Get Correction Blood Glucose Target ratio range and the status of mode of time.

        Additional Response:

        X,Y,Z\r\n where:
        T\r\n
        where:

        X is a number between 0 and 4 representing
            0 = No fixed time 
            1 = Morning
            2 = Midday
            3 = Evening
            4 = Night

        Y is a number between 70 and 180 representing Blood Glucose Low target

        Z is a number between 70 and 180 representing Blood Glucose High target 

        T is a number between 0 and 1 representing the time mode
            0 = "No fixed time" used
            1 = Fixed time used (Morning/Midday/Evening/Night)
        """
        result = []

        if command.get_name() != 'bgtgrng':
            return result

        for record in command.get_records():
            m = re.match(r'^([01234]),(\d+),(\d+)\r\n$', record, re.IGNORECASE)
            if m:
                settings = {}
                fix_time = int(m.group(1)) << 1
                settings[SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET+fix_time]     = int(m.group(2))
                settings[SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET+fix_time + 1] = int(m.group(3))
                result.append({SETTINGS_LIST: settings})
            else:
                m = re.match(r'^([01])\r\n$', record, re.IGNORECASE)
                if m:
                    settings = {SETTING_BLOOD_CORRECTION_GLUCOSE_RATIO_RANGE_TARGET_TIME_MODE:int(m.group(1))}
                    result.append({SETTINGS_LIST: settings})

        return result

    @staticmethod
    def convert_command_inslog(command):
        """
        Get the manual insulin log feature

        X,Y\r\n Where
        X is a number between 0 and 1 representing the status of the manual insulin log
            1 = Enable manual insulin log
            0 = Disable manual insulin log
        Y is a number between 0 and 1 representing the enable/disable status more than one type of insulin.
            1 = Long acting insulin on
            0 = Long acting insulin off

        Bit 0 -> 0 = Off, 1 = On; Bit 1 -> 0 long lasting insulin Off, 1 = On
        """
        result = []

        if command.get_name() != 'inslog':
            return result

        for record in command.get_records():            
            m = re.match(r'^([01]),([0,1])\r\n$', record, re.IGNORECASE)
            if m:
                settings = {SETTING_INSULIN_MANUAL_LOG:int(m.group(1)) + (int(m.group(2)) << 1)}
                result.append({SETTINGS_LIST: settings})

        return result

    @staticmethod
    def convert_command_actinscal(command):
        """
        $actinscal? - Get active Insulin Calculator (Easy/Advanced/Disabled)

        Additional Response: X\r\n
        Where:
        X is a number between 0 and 2 representing the active Insulin calculator status
            0 = Disable Insulin calculator
            1 = Enable Easy Insulin calculator
            2 = Enable Advanced Insulin calculator
        """
        result = []

        if command.get_name() != 'actinscal':
            return result

        for record in command.get_records():
            setting = AnalyseInsulinx.create_single_value_setting(command.get_records()[0], [0, 1, 2], SETTING_INSULIN_CALCULATOR)
            if setting:
                result.append(setting)

        return result

    @staticmethod
    def convert_command_tagsenbl(command):
        """
        Get the Enable/Disable Status of Smart Tags Feature

        Additional Response:
        X is a number between 0 and 1 representing the smart tags enable/disable status. 
            0 = Smart Tags Feature Disabled
            1 = Smart Tags Feature Enabled
        """
        result = []

        if command.get_name() != 'tagsenbl':
            return result

        for record in command.get_records():
            setting = AnalyseInsulinx.create_single_value_setting(command.get_records()[0], [0, 1], SETTING_SMART_TAGS)
            if setting:
                result.append(setting)
    
        return result

    @staticmethod
    def convert_command_event(command):
        """
        Extract error / alarm information.

        The device has much more event / error information than we store in Diasend so 
        we can bunch them all together and only look at the first couple of parameters 
        (which are identical for all events / errors.)
        """
        result = []

        if command.get_name() != 'event':
            return result

        for record in command.get_records():

            # Insulinx      VE,ET,MM,DD,YY,HH,MT,DTV,ERCD,ESD1,ESD2,ESD3,ESD4,ESD5

            m = re.match(r'^(?:\d+,)?([12]),(\d+),(\d{1,2}),(\d{1,2}),(\d{2}),(\d{1,2}),(\d{1,2}),([01])(.*)', record, re.IGNORECASE)
            if not m:
                # Optium Neo has a new line Numevents: at the end of the dump of events. 
                # Let's ignore it for now.
                if not re.match(r'Numevents: \d+', record):
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse event - invalid format %s' % (record))

            else:
                # We skip the ERCD and ESD#.
                keys   = ('ve', 'et', 'mm', 'dd', 'yy', 'hh', 'mt', 'dtv')
                values = map(int, m.groups()[:8])
                items  = dict(zip(keys, values))
                if items['ve'] == 1:
                    event_map = {
                        #32:ALARM_INSULINX_ERROR, 
                        33:ALARM_BATTERY_LOW, 
                        34:ALARM_BATTERY_OUTAGE, 
                        35:ALARM_DATE_CHANGED, 
                        36:ALARM_INSULINX_TIME_LOST, 
                        37:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_1, 
                        38:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_2, 
                        39:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_3, 
                        40:ALARM_INSULINX_INSULIN_CALCULATOR_SETUP_CHANGE_4, 
                        41:ALARM_BATTERY_REMOVED, 
                        42:ALARM_INSULINX_RESET_TO_FACTORY_CONFIGURATION, 
                        43:ALARM_INSULINX_RESULT_CLEARED, 
                        #44:ALARM_INSULINX_REBOOT, 
                        #45:ALARM_INSULINX_RECOVERY
                        }
                        
                    if items['et'] in event_map:
                        dt = datetime(2000+items['yy'], items['mm'], items['dd'], items['hh'], items['mt'])

                        value = {
                            ELEM_TIMESTAMP : dt,
                            ELEM_VAL_TYPE  : VALUE_TYPE_ALARM,
                            ELEM_VAL       : event_map[items['et']],
                            ELEM_FLAG_LIST : []
                        }

                        # Check Date-Time-Valid flag
                        if items['dtv'] == 0:
                            # Time reading is not reliable (often occurs during removal of battery etc)
                            value[ELEM_FLAG_LIST].append(FLAG_LOST_TIME)

                        result.append(value)
                    else:
                        # As certain events should be ignored, we should not fail if not found.
                        pass
                else:
                    # 've' flag means event is marked as invalid. We should just ignore such records.
                    pass

        return result

    @staticmethod
    def convert_command_result(command):
        '''
        Record to hold all kinds of results (glucose, manual insulin, carb etc). Record has sub-types where
        some sub-types is shared between Insulinx and Optium Neo; and others are not. 
        '''

        result = []

        def extract_record_type_0(body, dt, external_flags):
            '''
            Glucose and Ketone.
            '''
            m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+|HI|LO),(\d+),(\d+)', body, re.IGNORECASE)

            if m != None:
                value = {}
                flags = external_flags

                value[ELEM_TIMESTAMP]   = dt

                if m.group(1) == '0':
                    value[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
                else:
                    value[ELEM_VAL_TYPE] = VALUE_TYPE_KETONES

                if m.group(2) == '1':
                    flags.append(FLAG_RESULT_LOW_TEMP)
                elif m.group(2) == '2':
                    flags.append(FLAG_RESULT_HIGH_TEMP)

                if m.group(4) == '1':
                    flags.append(FLAG_BEFORE_MEAL)
                elif m.group(4) == '2':
                    flags.append(FLAG_AFTER_MEAL)

                alternative_value = 0
                # Glucose
                if m.group(1) == '0': 
                    if m.group(6) == 'LO':
                        alternative_value = 19
                        flags.append(FLAG_RESULT_LOW)
                    elif m.group(6) == 'HI':
                        alternative_value = 501
                        flags.append(FLAG_RESULT_HIGH)
                    elif int(m.group(6)) < 20:
                        flags.append(FLAG_RESULT_LOW)
                    elif int(m.group(6)) > 500:
                        flags.append(FLAG_RESULT_HIGH)

                # Ketone
                elif m.group(1) == '1':
                    if m.group(6) == 'HI':
                        alternative_value = 145
                        flags.append(FLAG_RESULT_HIGH)
                    elif int(m.group(6)) > 144:
                        flags.append(FLAG_RESULT_HIGH)

                try:
                    value[ELEM_VAL] = int(m.group(6))
                except ValueError:
                    value[ELEM_VAL] = alternative_value

                # Adjust mg -> mmol factor.
                value[ELEM_VAL] = value[ELEM_VAL] * 18.0 / 18.016

                if m.group(5) == '1':
                    flags.append(FLAG_RESULT_CTRL)

                value[ELEM_FLAG_LIST] = flags
                
                result.append(value)

            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid format %s' % (body))

        def extract_record_type_1(body, dt, external_flags):
            '''
            Insulin calculator bolus and carbs.
            '''
            
            m = re.match(r'^(\d),(\d),(-?\d+),(-?\d+),(\d+),(\d+),(\d+),(\d+)', body, re.IGNORECASE)
            if m != None:
                
                # Start with the bolus part
                
                flags = external_flags
                secondary = []

                rec = {
                    "insulin_style"  : int(m.group(1)),
                    "style_data"     : int(m.group(2)),
                    "correction"     : int(m.group(3)) * 0.5,
                    "override"       : int(m.group(4)) * 0.5,
                    "insulin_result" : int(m.group(5)) * 0.5,
                    "meal_insulin"   : int(m.group(6)) * 0.5,
                    "iob"            : int(m.group(7)) * 0.5,
                    "carbs"          : int(m.group(8)),
                    "correction_enabled" : int(m.group(3) != 127)
                }
                
                insulin_value = {
                    ELEM_TIMESTAMP  : dt,
                    ELEM_VAL_TYPE   : VALUE_TYPE_INS_BOLUS,
                    ELEM_VAL        : rec["insulin_result"] * VAL_FACTOR_BOLUS,
                    ELEM_FLAG_LIST  : flags,
                    ELEM_VALUE_LIST : secondary
                }
                
                if (rec["meal_insulin"] > 0):
                    secondary.append({ELEM_VAL : rec["meal_insulin"] * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL})
                    
                if (rec["correction"] != 0.0 and rec["correction_enabled"]):
                    secondary.append({ELEM_VAL : rec["correction"] * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR})

                if (rec["iob"] > 0):
                    secondary.append({ELEM_VAL : rec["iob"] * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_IOB})

                if (rec["override"] != 0.0  ):
                    secondary.append({ELEM_VAL : rec["override"] * VAL_FACTOR_BOLUS, ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_OVERRIDE})
                
                # Considered as a manual record
                flags.append(FLAG_MANUAL)
                
                result.append(insulin_value)
                
                # ...then the carbs part, if available
                if (rec["carbs"] > 0):
                    carb_value = {
                        ELEM_TIMESTAMP : dt,
                        ELEM_VAL_TYPE  : VALUE_TYPE_CARBS,
                        ELEM_VAL       : rec["carbs"],
                    }
                
                    result.append(carb_value)

        def extract_record_type_2(body, dt, external_flags):
            '''
            Manual insulin.
            '''
            
            m = re.match(r'^(\d),(\d+)', body, re.IGNORECASE)
            if m != None:
                
                rec = {
                    "is_rapid" : int(m.group(1)) == 0,
                    "insulin" : int(m.group(2)) * 0.5,
                }
                
                value = {
                        ELEM_TIMESTAMP : dt,
                        ELEM_VAL       : rec["insulin"] * VAL_FACTOR_BOLUS,
                        ELEM_FLAG_LIST : external_flags
                }
                
                # Set the insulin type
                if rec["is_rapid"]:
                    value[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS
                else:
                    value[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS_BASAL
                    value[ELEM_FLAG_LIST].append(FLAG_INSULIN_IMPACT_LONG)
                
                value[ELEM_FLAG_LIST].append(FLAG_MANUAL)

                result.append(value)

        if command.get_name() != 'result':
            return result

        total_number_records = 0

        for record in command.get_records():

            # In this record type we extract the header first - looking for date and 
            # sub-type. Then we extract the sub-type. 
            
            m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(.*)$', record, re.IGNORECASE)
            if m:
                keys   = ('rt', 'rn', 'mm', 'dd', 'yy', 'hh', 'mt', 'dtv')
                values = map(int, m.groups()[:8])
                items  = dict(zip(keys, values))

                total_number_records += 1

                flags = [FLAG_LOST_TIME] if items['dtv'] == 0 else []

                try:
                    dt = datetime(2000+items['yy'], items['mm'], items['dd'], items['hh'], items['mt'])
                except ValueError:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED, 'Parse result - invalid datetime format %s' % (record))

                record_types = {
                    0 : extract_record_type_0, 
                    1 : extract_record_type_1, 
                    2 : extract_record_type_2
                }

                if items['rt'] in record_types:
                    record_types[items['rt']](m.group(9), dt, flags)

            if m is None:
                m = re.match(r'^(\d+),([\da-fA-F]{8,8})', record, re.IGNORECASE)
                if m is not None:
                    # Internal check - we are able to validate the number of data points which 
                    # is different from total number of records (which includes events etc).
                    if int(m.group(1)) != total_number_records:
                        raise Exception(ERROR_CODE_NR_RESULTS, 'Incorrect number of results %d != %d' % (int(m.group(1)), total_number_records))

        return result

    @staticmethod
    def convert_command_resmtrcfg(command):
        result = []

        if command.get_name() != 'resmtrcfg':
            return result

        if command.nr_of_records() != 1:
            return result

        record = command.get_records()[0]

        m = re.match(r'^(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+),(\d+) (\d+) (\d+).*$', record, re.IGNORECASE)
        if m:
            keys = (
                'qc_test_time', 
                'tag_result', 
                'insulin_dose_reset_time', 
                'evening_dose', 
                'morning_dose', 
                'breakfast_insulin', 
                'lunch_insulin', 
                'dinner_insulin', 
                'titration_start_day', 
                'titration_start_month', 
                'titration_start_year', 
                'insulin_titration_feature', 
                'titration_range_bottom', 
                'titration_range_top', 
                'titration_period', 
                'default_max_titrate_dose', 
                'dose_factor', 
                'titration_auto_shut_off', 
                'correction', 
                'hi_indicator_value', 
                'lo_indicator_value', 
                'indicator_pattern_time', 
                'indicator_on_off', 
                'insulin_reset_day', 
                'insulin_reset_month', 
                'insulin_reset_year', 
                'pc_only_setting_defaults_changed', 
                'insulin_setup_mode', 
                'time_format', 
                'date_format', 
                'engineering_mode_inactivity_time', 
                'engineering_mode_ad_hoc', 
                'night_check_time', 
                'night_check_min_bg'
            )
            values = map(int, m.groups())
            items  = dict(zip(keys, values))

            setting_list = {
                SETTING_LONG_ACTING_INSULIN_BASE_DOSE_MORNING   : items['morning_dose'],
                SETTING_LONG_ACTING_INSULIN_BASE_DOSE_EVENING   : items['evening_dose'],
                SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_BREAKFAST: items['breakfast_insulin'],
                SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_LUNCH    : items['lunch_insulin'],
                SETTING_RAPID_ACTING_INSULIN_BASE_DOSE_DINNER   : items['dinner_insulin'],
                SETTING_FIRST_DAY_OF_WEEK                       : items['date_format'],
                SETTING_TIME_FORMAT                             : 0 if items['time_format'] == 1 else 1 # See comment below
            }

            # Note SETTING_TIME_FORMAT : diasend 0 = 12h, 1 = 24h. Abbott 0 = 24h, 1 = 12h.

            result.append({SETTINGS_LIST: setting_list})

            return result
