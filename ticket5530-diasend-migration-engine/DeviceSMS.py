# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2009 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Generic SMS

from Generic import *
from Defines import *
import datetime

import Config
exec "import %s" % Config.settings["database-module"]

# Rules and functionalities
# 
#     * The timestamp of the glucose readings, comments, ketones, carbs and insulin doses will be server time upon receiving the comment.
#     * If ticking the "Skicka SMS-notifiering" box, an SMS will be sent to the patient with the first characters of the message until the limit of 160 characters in total is reached. The rest will be truncated. Message format: "Vårdgivare: [message body]...//".
#     * We will have sender ID "diasend.com" when sending out SMS.
#           o Question: Is it possible to include a replyto-number so the patient can answer dircetly by pressing reply on their mobile phone? 
#     * The SMS syntaxes/codes for entering data are (examples):
#           o "G6" = A glucose reading of 6. (interprets as mmol/l if parent clinic account is set to mmol/l, and the same rule for mg/dl)
#           o "I6" = An insulin dose of 6 units.
#           o "KT6" = A ketone value of 6. (interprets as mmol/l if parent clinic account is set to mmol/l, and the same rule for mg/dl)
#           o "C123" or "KH123" = An intake of 123 grams of carbohydrates. (note that this will not be visualized in the regular web application)
#           o "TH12:00-13:00" = A training session duration 12:00-13:00 with intensity "High".
#           o "TM12:00-13:00" = A training session duration 12:00-13:00 with intensity "Medium".
#           o "TL12:00-13:00" = A training session duration 12:00-13:00 with intensity "Low".
#           o No or erroneous syntax = Enter message as a comment (unless covered by any case in "Special syntax interpretation rules" below).
#           o All codes may be entered in the same message, if it's separated by a space. E.g. "G7 I4 TL12:00-13:00". 
#     * The glucose readings will be stored in regards to what unit the parent clinic account is set to. 
# 
# Special syntax interpretation rules
# 
#     * Comma "," and dot "." should both be valid decimal dividers.
#     * For example "TL1200-1300" should be interpreted as "TL12:00-13:00".
#     * For example "T12:00-13:00" should be interpreted as "TM12:00-13:00". (Medium is default)
#     * Cross rules of above mentioned special syntaxes will apply, such as "T1200-1300" should be interpreted as "TM12:00-13:00".

# -----------------------------------------------------------------------------
# Defines
# -----------------------------------------------------------------------------
BG_FACTOR = 0
# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
    
def EvalSerialRecord( line ):
    """
    Main function for validating a SMS generic serial record
    
    The SMS servers send the phone number as serial, but we use
    device_user as device and the corresponding seria
    """
    return re.match( r'!TAG!SERNO!(\+\d+)', line, re.IGNORECASE )

    

def EvalBGRecord(line):
    """
    Example of nice BG records are:
    G11 G1,2 G1.2 G1
    """
    global BG_FACTOR
    m = re.match( r'G(\d+[\.\,]?\d*)$', line, re.IGNORECASE )
    if m:
        val_str = m.group(1)
        # Replace any , with .
        val_str = val_str.replace(",", ".")
        val = float(val_str) * BG_FACTOR
        return {"type" : VALUE_TYPE_GLUCOSE, "value" : val }

    return None

def EvalKetoneRecord(line):
    """
    Example of nice BG records are:
    KT11 KT1,2 KT1.2 KT1
    """
    global BG_FACTOR
    m = re.match( r'KT(\d+[\.\,]?\d*)$', line, re.IGNORECASE )
    if m:
        val_str = m.group(1)
        # Replace any , with .
        val_str = val_str.replace(",", ".")
        val = float(val_str) * BG_FACTOR
        return {"type" : VALUE_TYPE_KETONES, "value" : val }

    return None

def EvalInsulinRecord(line):
    """
    Example of nice Insulin records are:
    I11 I1,2 I1.2 I1
    """
    m = re.match( r'I(\d+[\.\,]?\d*)$', line, re.IGNORECASE )
    if m:
        val_str = m.group(1)
        # Replace any , with .
        val_str = val_str.replace(",", ".")
        val = float(val_str) * VAL_FACTOR_BOLUS
        return {"type" : VALUE_TYPE_INS_BOLUS, "value" : val }

    return None

def EvalCarbonRecord(line):
    """
    Example of nice Carbon records are:
    KH11 C99
    """
    m = re.match( r'(?:C|KH)(\d+[\.\,]?\d*)$', line, re.IGNORECASE )
    if m:
        val_str = m.group(1)
        # Replace any , with .
        val_str = val_str.replace(",", ".")
        val = float(val_str) * VAL_FACTOR_CARBS
        return {"type" : VALUE_TYPE_CARBS, "value" : val }

    return None

def EvalTime(line):
    """
    Example of correct time strings:
    
    01
    1140
    01.40
    01:50
    """
    m = re.match( r'(\d{2,2})$', line, re.IGNORECASE )
    if m:
        return datetime.time(int(m.group(1)))

    m = re.match( r'(\d{2,2})[\.\:]?(\d{2,2})$', line, re.IGNORECASE )
    if m:
        return datetime.time(int(m.group(1)), int(m.group(2)))
    
    return None

def EvalTraningRecord(line):
    """
    Example of nice excersise records:
    'TH12:00-13:00'
    'TM13:00-13:05'
    'TL14:30-15:00'
    'TL1700-TL1813'
    'T10:11-11:00'
    'T10.00-10.01'
    'TM19-20'
    """
    m = re.match( r'T([LMH]{0,1})', line, re.IGNORECASE )
    if m:
        rest = line[len(m.group(0)):]
        times = line[len(m.group(0)):].split("-")

        # We must only have one start and end time
        if len(times) <> 2:
            return None
        start_time = EvalTime(times[0])
        end_time = EvalTime(times[1])

        if not start_time or not end_time:
            return None

        dt = datetime.datetime.now()
        # Insert start time
        dt = dt.replace(hour = start_time.hour, minute = start_time.minute, second = start_time.second, microsecond = 0)
        
        # Calculate the duration
        if end_time < start_time:
            # Over two days
            duration = (24 - start_time.hour) * 60
            duration -= start_time.minute
            duration += end_time.hour * 60 + end_time.minute
            #Start time was yesterday
            dt -= datetime.timedelta(days = 1)
        else:
            duration = (end_time.hour - start_time.hour) * 60 + (end_time.minute - start_time.minute)
        
        
        # TODO support TM23-01
        flags = []
        ret = {"type" : VALUE_TYPE_EXERCISE, "value" : 0, "flags" : flags, "duration" : duration, "datetime": dt}

        if m.group(1) == "" or m.group(1) == "m" or m.group(1) == "M":
            flags.append(FLAG_MEDIUM_EXERCISE)
        elif m.group(1) == "l" or m.group(1) == "L":
            flags.append(FLAG_MILD_EXERCISE)
        elif m.group(1) == "h" or m.group(1) == "H":
            flags.append(FLAG_HARD_EXERCISE)
        else:
            return None
        
        return ret
    
    return None
        
# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalSmsModelRecord( line ):
    """
    Is this line a device model record. If so, return a dictionary with device
    model.
    """
    return {ELEM_DEVICE_MODEL : "SMS"}


def EvalSmsSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        serial = Database.GetUserDeviceFromMobileNr(m.group(1))
        print repr(serial)
        if serial:
            res[ELEM_DEVICE_SERIAL] = serial
    return res
    
def EvalSmsUnitRecord( line ):
    """
    A bit tricky, we need to query the database to check which unit the
    clinic the patient which uses this phone is registered to.
    """
    global BG_FACTOR
    m = EvalSerialRecord( line )
    if m:
        unit = Database.GetTerminalDefaultUnit(m.group(1))
        if unit:
            if unit == "mmol/l":
                BG_FACTOR = VAL_FACTOR_GLUCOSE_MMOL
            else:
                BG_FACTOR = VAL_FACTOR_GLUCOSE_MGDL
            return { ELEM_DEVICE_UNIT:unit }
    return {}

def EvalSmsResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    m = EvalBGRecord(line)
    if not m:
        m = EvalKetoneRecord(line)
    if not m:
        m = EvalInsulinRecord(line)
    if not m:
        m = EvalCarbonRecord(line)
    if not m:
        m = EvalTraningRecord(line)
    
    if m:
        # Got some value!
        res = {ELEM_VAL_TYPE: m["type"], ELEM_VAL: m["value"]}
        if m.has_key("datetime"):
            res[ELEM_TIMESTAMP] = m["datetime"]
        else:
            # Remove seconds and microseconds
            res[ELEM_TIMESTAMP] = datetime.datetime.now().replace(second = 0, microsecond = 0)

        if m.has_key("flags"):
            res[ELEM_FLAG_LIST] = m["flags"]
        else:
            res[ELEM_FLAG_LIST] = []

        res[ELEM_FLAG_LIST].append(FLAG_MANUAL)
        
        if m.has_key("duration"):
            res[ELEM_VALUE_LIST] = [{ELEM_VAL : m["duration"], ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]

        return res
    if EvalSerialRecord(line):
        return {}
    else:
        # Return error on all unknowns...
        return { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":line }


def EvalDiasendGenericNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        if (m["last_written"] == 0):
            res[ "meter_nr_results" ] = 0
        elif (m["last_written"] - 0x10000) >= 0:
            res[ "meter_nr_results" ] = (m["last_written"] - 0x10000) + 1
        else:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m["last_written"] }
    return res

def EvalDiasendGenericChecksumRecord( line, record ):
    """
    No checksums in the data, since blocks has checksums, just return true
    """
    return True

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectSMS( inList ):
    """
    Detect if data comes from a SMS.
    """
    return DetectDevice( 'SMS', inList, DEVICE_USER )


def AnalyseSMS( inData ):
    """
    Analyse SMS
    """
    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ] = EvalSmsModelRecord
    d[ "eval_serial_record" ]       = EvalSmsSerialRecord
    d[ "eval_unit"]                 = EvalSmsUnitRecord
    d[ "eval_result_record" ]       = EvalSmsResultRecord
    
    # Convert to unicode
    uindata = inData.decode('utf-8')
    # Split on space, remove empty lines
    inList = filter(len, uindata.split(" "))

    resList = AnalyseGenericMeter( inList, d );
    res = resList[0]
    
    if res.has_key("results") and ResultIsError(res["results"]):
        # Check the error code
        error_code = res["results"][0]["error_code"]
        if error_code == ERROR_CODE_VALUE_ERROR:
            # Got an error generate a huge comment for everything, except for everything
            # before the first space, that is the phone number
            r = {COMMENT_TYPE: uindata[uindata.find(" ")+1:]}
            r[ELEM_TIMESTAMP] = datetime.datetime.now().replace(second = 0, microsecond = 0).isoformat(' ')
            res["results"] = [r]
        
    return [res]

# -----------------------------------------------------------------------------
# SOME CODE THAT WILL RUN ON IMPORT
# -----------------------------------------------------------------------------

if __name__ == "__main__":

    data = "!TAG!SERNO!+46708889791 G1 I1 i2.5 KT5 TH12:00-13:00 TM13:00-13:05 TL14:30-15:00 TL1700-1813 T10:11-11:00 G1.5 G1,6 T10.00-10.01 TM19-20 T23-01 C123 KH39"
    res = AnalyseSMS(data)
    print res

