# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Nova Max Link

# Decoding is based on Nova Max Link Glucose Meter Data Collection 
# Specification, v 1.0

import re
from Generic import *
from Defines import *
from datetime import datetime


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------


dallas_1wire_lookup = [
     0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65, 
   157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220, 
    35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98, 
   190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255, 
    70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7, 
   219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154, 
   101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36, 
   248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185, 
   140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205, 
    17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80, 
   175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238, 
    50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115, 
   202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139, 
    87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22, 
   233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168, 
   116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53]
 

def UpdateCRC(initial_crc, byte):
    """
    Update the initial crc with another checksum byte
    """
    return dallas_1wire_lookup[initial_crc ^ byte]
    

def SplitIndata(indata):
    """
    Splits incoming data into a list. 
    
    The data consists of records, all terminated by ']\r\n'

    """
    
    inList = []
    
    terminator = ']\r\n'
    
    while len(indata) > 0:
        idx = indata.find(terminator)

        # Terminator not found (?)
        if idx <= 0:
            return []

        inList.append(indata[:idx + len(terminator)])

        indata = indata[idx + len(terminator):]
    
    return inList

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSettingsRecord( line ):
    """
    Evaluate a Nova Max Link header record. Extracted groups : 
     1  Meter Board ID
     2  Type (6 = 916 MHz)
     3  SKU: Date format, hours format, Glucose units (bit fields)
     4  Settings: Glucose units, beep, Glucose result RF send
     5  A1 Hour
     6  A1 Min
     7  A2 Hour
     8  A2 Min
     9  A3 Hour
    10  A3 Min
    11  A4 Hour
    12  A4 Min
    13  Meter ID (serial number)
    14  csum
    
    The settings record looks like this:
    '[\r\nT11298063256,  6,  0,110,00,00,00,00,00,00,00,00,9071201294B\r\n2F\r\n]\r\n'
    """
    
    if len(line) != 72:
        return None
    
    result = re.match(r'\[\r\n(\w{12,12}),([\d ]{1,3}),([\d ]{1,3}),([\d ]{1,3}),([\d]{2,2}),([\d]{2,2}),([\d]{2,2}),([\d]{2,2}),([\d]{2,2}),'
       '([\d]{2,2}),([\d]{2,2}),([\d]{2,2}),(\w{11,11})\r\n([\dA-F]{2,2})\r\n\]\r\n.*', line, re.IGNORECASE )
    
    return result


def EvalResultRecord( line ):
    """
    Evaluate a Nova Max Link binary represented result record. Extracted value : 


    "Glu/Ctl"
    "Value"
    "Unit"
    "Mark" '1' is mark
    "Date" YYYYMMDD
    "Time" HHmm
    
    Lines look like this:
    '\nGlu, 419,mg/dL,0,20101127,2220'
    """

    # The length of the result records are always 30-32 bytes long 
    if ((len(line) < 30) or (len(line) > 32)):
        return None
    
    return re.match( r'\n(Glu|Cntl), *(LO|HI|[0-9\.]*), *(mmol/L|mg/dL),([0-9]{1,1}),([0-9]{8,8}),([0-9]{4,4})', line, re.IGNORECASE )

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalNovaMaxLinkSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}

    m = EvalSettingsRecord(line)
    if m:
        res[ "meter_serial" ] = m.group(13)

    return res


def EvalNovaMaxLinkResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    # The result record contains many results, split it first
    # The main split function does not split each result on this meter
    # because the checksum is common for all results
    
    # first check that this really is a result record
    # with one or more records
    # The combined records look like this:
    # [
    # Glu,  LO,mg/dL,0,20110101,1800
    # ...
    # ]
    # Where the first result field is Glu or Cntl

    if not re.match( r'\[\r\n(Glu|Cntl)([\s\S]*)[\]]{1,1}', line, re.IGNORECASE ):
        return {}

    # The record seem correct -> split
    lines = line.split('\r') 

    res = []

    for l in lines:
        m = EvalResultRecord(l)
        if m:
            rec = {} 
            
            flags = []
            
            if m.group(1) == "Cntl":
                flags.append(FLAG_RESULT_CTRL)

            value = 0
                
            if m.group(2) == "LO":
                flags.append(FLAG_RESULT_LOW)
            elif m.group(2) == "HI":
                flags.append(FLAG_RESULT_HIGH)
            else:
                # Normal value
                value = float(m.group(2))

            rec[ELEM_DEVICE_UNIT] = m.group(3)
            # The analyser shall return the value * 10 if a mmoll value
            if rec[ELEM_DEVICE_UNIT].lower() == "mmol/l":
                value *= 10
           
            if m.group(4) == "1":
                flags.append(FLAG_CHECK_MARK)


            year    = int(m.group(5)[:4])
            month   = int(m.group(5)[4:6])
            day     = int(m.group(5)[6:8])
            
            hour    = int(m.group(6)[:2])
            min     = int(m.group(6)[2:4])
            
            try:
                rec[ ELEM_TIMESTAMP ] = datetime(year, month, day, hour, min)
            except:
                rec[ ELEM_TIMESTAMP ] = None
            
            rec[ ELEM_FLAG_LIST ] = flags
            rec[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
            rec[ ELEM_VAL ] = value

            res.append(rec)

    return res


def EvalNovaMaxLinkChecksumRecord( line, record ):
    """
    Evaluate checksum of result record. 
    
    The meter uses dallas one wire CRC
    """
    crc = 0
    
    for c in line[:len(line) - 7]:
        crc = UpdateCRC(crc, ord(c))

    given_crc = int(line[len(line) - 7: len(line) - 5], 16)
    
    return crc == given_crc

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectNovaMaxLink( inList ):
    """
    Detect if data comes from a Nova Max Link.
    """
    return DetectDevice( 'NovaMaxLink', inList, DEVICE_METER )

def AnalyseNovaMaxLink( inData ):
    """
    Analyse Nova Max Link
    """

    inList = SplitIndata(inData)
    
    # Empty dictionary
    d = {}

    d[ "meter_type" ]               = "Nova Max Link"
    d[ "eval_serial_record" ]       = EvalNovaMaxLinkSerialRecord
    d[ "eval_result_record" ]       = EvalNovaMaxLinkResultRecord
    d[ "eval_checksum_record" ]     = EvalNovaMaxLinkChecksumRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":

    #testfile = open('test/testcases/test_data/NovaMaxLink/NovaMaxLink-9071201294B.bin')
    #testfile = open('test/testcases/test_data/NovaMaxLink/NovaMaxLink-9071231294B-400_values.bin')
    testfile = open('test/testcases/test_data/NovaMaxLink/NovaMaxLink-9147462067B-values_and_flags.bin')
    testcase = testfile.read()
    testfile.close()

    print AnalyseNovaMaxLink(testcase)
