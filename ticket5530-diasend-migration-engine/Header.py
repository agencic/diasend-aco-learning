# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2007 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re
import Common
import base64
import Defines

def EvalProtVerPacket( line ):
    m = re.match( r'!HDR!PROTVER!(\d*)\.(\d*)', line, re.IGNORECASE )
    return m

def EvalHWIDPacket( line ):
    m = re.match( r'!HDR!HWID!([\S ]+)', line, re.IGNORECASE )
    return m

def EvalSWIDPacket( line ):
    m = re.match( r'!HDR!SWID!([\S]+)( \S+|)', line, re.IGNORECASE )
    return m

def EvalBLIDPacket( line ):
    m = re.match( r'!HDR!BLID!([\S ]+)', line, re.IGNORECASE )
    return m

def EvalSERNOPacket( line ):
    m = re.match( r'!HDR!SERNO!([\S ]+)', line, re.IGNORECASE )
    return m

def EvalMeterTypePacket( line ):
    m = re.match( r'!HDR!METERTYPE!([\S ]+)', line, re.IGNORECASE )
    return m

def EvalCompressionPacket( line ):
    m = re.match( r'!HDR!COMPRESSION!([\S]+)', line, re.IGNORECASE )
    return m

def EvalInterfacePacket( line ):
    m = re.match( r'!HDR!INTERFACE!([\S]+)', line, re.IGNORECASE )
    return m

def EvalEncryptionPacket( line ):
    m = re.match( r'!HDR!ENCRYPTION!([\S]+)', line, re.IGNORECASE )
    return m

def EvalUserInformationPacket( line ):
    m = re.match( r'!HDR!USERINFO!([\S]+)', line, re.IGNORECASE )
    print line 
    print m
    return m

def EvalClinicProperty( line ):
    m = re.match( r'!HDR!CLINIC!YES', line, re.IGNORECASE )
    return m

def EvalLocation( line ):
    m = re.match( r'!HDR!LOCATION!([\S]+)', line, re.IGNORECASE )
    return m

def EvalCookie( line ):
    m = re.match( r'!HDR!COOKIE!([\S]+)', line, re.IGNORECASE )
    return m

def EvalOS( line ):
    m = re.match( r'!HDR!OS!([\S ]+)', line, re.IGNORECASE )
    return m

class CHeader:
    "This class contain all header information of a transfer"
    __buffer = None
    __prot_ver_major = None
    __prot_ver_minor = None
    __hw_id = None
    __sw_id = None
    __sw_ver = None
    __bl_id = None
    __terminal_serial = "0"
    __device_type = "Unknown"
    # Comes from the meter data
    __device_serial = "0"
    # If compression is used on the data
    __zlib_compression = False
    __lzma_compression = False
    # Name of the interface to which the device was connected
    __interface = "Unknown"
    __encryption = True
    __user_firstname  = "" 
    __user_lastname   = "" 
    __user_pcode      = "" 
    __user_password   = "" 
    __user_action     = "" 
    __user_info_valid = False
    __clinic_property = False
    __location        = ""
    __operating_system = ""
    __user_cookie     = None
    
    def __init__(self, buffer):
        self.__buffer = buffer
        inList = buffer.split('\n')
        
        for line in inList:
            m = EvalProtVerPacket( line )
            if m:
                self.__prot_ver_major = int(m.group(1))
                self.__prot_ver_minor = int(m.group(2))

            m = EvalHWIDPacket( line )
            if m:
                self.__hw_id = m.group(1)
                
            m = EvalSWIDPacket( line )
            if m:
		print "SWID:", m.groups()
                self.__sw_id = m.group(1)
                self.__sw_ver = m.group(2).strip()

            m = EvalBLIDPacket( line )
            if m:
                self.__bl_id = m.group(1)

            m = EvalSERNOPacket( line )
            if m:
                self.__terminal_serial = m.group(1)
                
            m = EvalMeterTypePacket( line )
            if m:
                self.__device_type = m.group(1)
                
            m = EvalCompressionPacket( line )
            if m:
                if m.group(1) == "ENABLED" or m.group(1) == "ENABLED!ZLIB":
                    self.__zlib_compression = True
                elif m.group(1) == "ENABLED!LZMA":
                    self.__lzma_compression = True
            
            m = EvalInterfacePacket( line )
            if m:
                self.__interface = m.group(1)
               
            m = EvalEncryptionPacket( line )
            if m:
                if m.group(1) == "DISABLED":
                    self.__encryption = False 
    
            m = EvalUserInformationPacket( line )
            if m:
                self.ExtractUserInformation(m.group(1))

            m = EvalClinicProperty( line )
            if m:
                self.__clinic_property = True
            
            m = EvalLocation( line )
            if m:
                self.__location = m.group(1)    

            # Check if there is a cookie
            m = EvalCookie( line )
            if m:
                self.__user_cookie     = base64.b64decode(m.group(1))
            m = EvalOS( line )
            if m:
                self.__operating_system = m.group(1)
   
    def ExtractUserInformation( self, line ):
        m = re.match( r'FIRSTNAME!([\S]*)!LASTNAME!([\S]*)!PCODE!([\S]+)!PW!([\S]+)!ACTION!([\S]+)', line, re.IGNORECASE)
        if m:
            print "MATCH"
            try:
                self.__user_firstname  = base64.b64decode(m.group(1))
                print self.__user_firstname
                self.__user_lastname   = base64.b64decode(m.group(2))
                print self.__user_lastname
                self.__user_pcode      = base64.b64decode(m.group(3))
                print self.__user_pcode
                self.__user_password   = base64.b64decode(m.group(4))
                print self.__user_password
                self.__user_action     = m.group(5)
                print self.__user_action
                self.__user_info_valid = True
            

            # TypeError : Base64 unable to decode data. 
            # IndexError : Regex failed to match all parts. 
            except (TypeError, IndexError):
                print "convert error"
                self.__user_info_valid = False
               
        if not self.__user_info_valid:
            self.__user_firstname  = "" 
            self.__user_lastname   = "" 
            self.__user_pcode      = "" 
            self.__user_password   = "" 
            self.__user_action     = "" 

    def getUserFirstName(self): 
        return self.__user_firstname

    def getUserLastName(self): 
        return self.__user_lastname

    def getUserPCode(self): 
        return self.__user_pcode 

    def getUserPassword(self): 
        return self.__user_password

    def getUserAction(self): 
        return self.__user_action

    def getUserCookie(self):
        return self.__user_cookie
    
    def getUserInformationAvailable(self):
        return self.__user_info_valid

    def getClinicProperty(self):
        return self.__clinic_property   

    def getLocation(self):
        return self.__location

    def isSoftwareTransmitter(self):
        """
        Use article number (SWID) to decide if terminal is a software transmitter
        Example SWID="A10024 R1a"
        """        

        if self.getSoftwareID() and (self.getSoftwareID() in Defines.sw_transmitters):
            return True
        return False

    def getBuffer(self):
        """
        Get the internal buffer
        """
        return self.__buffer
    
    def getHardwareID(self):
        """
        Get the hardware info
        """
        return self.__hw_id

    def getBootloaderID(self):
        """
        Get the bootloader info
        """
        return self.__bl_id

    def getBootloaderVersion(self):
        """
        Get the bootloader version
        """
        if self.__bl_id:
            m = re.match( r'(\w*)\s*(\w*)', self.__bl_id, re.IGNORECASE )
            if m:
                return m.group(2)
            else:
                # Special case, some versions did not have a space between article number and version number    
                m = re.match( r'A06105(\w*)', self.__bl_id, re.IGNORECASE )
                if m:
                    return m.group(1)

        return None

    def getBootloaderNumericVersion(self):
        """
        Get the numeric boot loader version
        """
        str_ver = self.getBootloaderVersion()
        if (str_ver):
            return Common.GetNumericVersion(str_ver)
        else:
            return None

    def getSoftwareID(self):
        """
        Get the software info
        """
    
        return self.__sw_id
        
    def GetSoftwareVersion(self):
        """
        Get the software version
        """
        if self.__bl_id:
            if self.__sw_ver:
                return self.__sw_ver
        return None

    def getTerminalSerial(self):
        """
        Get the terminal serial number
        """
        return self.__terminal_serial

    def isTerminalSerialUnknown(self):
        return (self.__terminal_serial == "0")

    def isTerminalSerialKnown(self):
        return (self.__terminal_serial != "0")

    def getDeviceType(self):
        """
        Get the device type number
        """
        return self.__device_type

    def getProtocolVersion(self):
        """
        Get the protocol version, returns dictionary with keys:
        major: Major version
        minor: Minor version
        """
        if self.__prot_ver_major == None or self.__prot_ver_minor == None:
            return None
        else:
            return {"major": self.__prot_ver_major, "minor": self.__prot_ver_minor}

    def setDeviceSerial(self, serial):
        """
        Set the serial number of the current device
        """
        self.__device_serial = serial

    def getDeviceSerial(self):
        """
        Get the serial number of the current device
        """
        return self.__device_serial

    def areMandatoryTagsSet(self):
        """
        Performs a sanity check of the header
        """
        if self.__prot_ver_major == None or self.__prot_ver_minor == None or self.__hw_id == None or self.__sw_id == None or self.__bl_id == None:
            return False
        else:
            return True
    
    def toDictionary(self):
        """
        Returns a dictionary corresponding to the object
        """
        return {"terminal_serial" : self.getTerminalSerial(), "terminal_swid": self.getSoftwareID(), "terminal_hwid": self.getHardwareID(), "terminal_blid": self.getBootloaderID() }

    def usingZlibCompression(self):
        """
        Returns a boolean stating usage of zlib compression of the body
        """
        return self.__zlib_compression

    def usingLzmaCompression(self):
        """
        Returns a boolean statuing usage of lzma compression of the body
        """
        return self.__lzma_compression
        
    def usingEncryption(self):
        """
        Returning a boolean stating usage of encryption of the body
        """
        return self.__encryption
    
    def getInterface(self):
        """
        Return the name of the interface
        """
        return self.__interface
        
    def getConnectionInfo(self):
        """
        Returns a string containing the device type and the interface
        """
        return "(%s via %s)" % (self.getDeviceType(), self.getInterface())
    def getOS(self):
        """
        Return a string containing operating system information
        """
        return self.__operating_system

def GetProtVer( buffer ):
    inList = buffer.split('\n')
    for line in inList:
        m = EvalProtVerPacket(line)
        if m:
            try:
                return {"major": int(m.group(1)), "minor": int(m.group(2))}
            except:
                pass

    return None

if __name__ == "__main__":
    header_string = '!HDR!DIASEND\r\n!HDR!PROTVER!2.1\r\n!HDR!HWID!A06101R1d\r\n!HDR!SWID!A06102 trunk\r\n!HDR!BLID!A06105 R2d\r\n!HDR!SERNO!S6020761259174\r\n!HDR!METERTYPE!AnimasIR1200\r\n!HDR!METERDATA\r\n'

    header = CHeader(header_string)
    
    print header.getDeviceType()
    print header.getTerminalSerial()
    print header.getBootloaderVersion()
