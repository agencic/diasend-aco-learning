# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# A buffer class that holds binary data and makes it possible for a consumer
# to extract data in a controlled fashion. 
# -----------------------------------------------------------------------------

import struct

class ExtractException(Exception):
    """
    Raise this exception if data couldn't be extracted. 
    """
    def __init__(self, text):
        self.text = text
        
    def __str__(self):
        return self.text
        
class Buffer:
    """
    Buffer class - holds binary data (as a Python string). Has an index internally which makes 
    it possible to 'consume' data in the buffer (the caller does not need to hold his own index).
    """
    
    def __init__(self, data):
        self.index  = 0
        self.data   = data
        self.mark   = None
        
    def __len__(self):
        return len(self.data)
        
    def nr_bytes_left(self):
        """
        How many bytes are left in the buffer?
        """
        return len(self.data) - self.index
        
    def check_if_nr_of_bytes_left(self, nr_bytes):
        """
        Is it possible to extract nr_bytes of data from the buffer? If not raise ExtractException.
        """
        if (len(self.data) - self.index) < nr_bytes:
            raise ExtractException('Buffer tried to extract %d bytes - only got %d.' % (nr_bytes, len(self.data) - self.index))
        
    def set_mark(self):
        """
        Set a bookmark (can be used later for retrieval of data from this point).
        """
        self.mark = self.index

    def get_distance_to_mark(self):
        """
        How many bytes are there between the mark and the current position in the buffer?
        """
        if self.mark != None:
            return self.index - self.mark
        return 0
        
    def get_slice_from_mark(self):
        """
        Get data from the bookmark to the current position.
        """
        if self.mark != None:
            return self.data[self.mark:self.index]
        return ''
        
    def get_sub_buffer(self, nr_bytes):
        """
        Create a new Buffer object with nr_bytes data from the current position.
        """
        self.check_if_nr_of_bytes_left(nr_bytes)
        return Buffer(self.get_slice(nr_bytes))

    def peek_slice(self, nr_bytes):
        """
        Return a slice of data (nr_bytes long) from the current position. Does _not_ consume
        the data.
        """
        self.check_if_nr_of_bytes_left(nr_bytes)
        value = self.data[self.index:self.index+nr_bytes]
        return value

    def get_slice(self, nr_bytes):
        """
        Return a slice of data (nr_bytes long) from the current position. Consume the data (i.e. 
        increase the index).
        """
        value = self.peek_slice(nr_bytes)
        self.index += nr_bytes
        return value
        
    def print_buffer(self, index, nr_bytes):
        """
        Print the buffer from index to index + nr_bytes.
        """
        print "".join(["%02x " % ord(i) for i in self.data[index:index+nr_bytes]])
        
    def peek_struct(self, format, offset=0):
        """
        Unpack data from the buffer using the Python struct module. Does _not_ consume the data.
        """
        self.check_if_nr_of_bytes_left(struct.calcsize(format) + offset)
        return struct.unpack(format, self.data[self.index+offset:self.index+offset+struct.calcsize(format)])
        
    def get_struct(self, format):
        """
        Unpack data from the buffer using the Python struct module. Consumes the data. 
        """
        self.check_if_nr_of_bytes_left(struct.calcsize(format))
        ret = struct.unpack(format, self.data[self.index:self.index+struct.calcsize(format)])
        self.index += struct.calcsize(format)
        return ret
        
    def consume(self, nr_bytes):
        """
        Consume nr_bytes of data. Return NOTHING. 
        """
        self.check_if_nr_of_bytes_left(nr_bytes)
        self.index += nr_bytes

    def reset(self):
        self.index = 0

    def find(self, search_string):
        """
        Find string in data, if found return nr of bytes to search_string else return -1 
        """
        index_diff = -1
        new_index = self.data.find(search_string, self.index)
        if new_index != -1:
            index_diff = new_index - self.index
        return index_diff
        
