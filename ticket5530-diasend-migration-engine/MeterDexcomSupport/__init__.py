# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# A buffer class that holds binary data and makes it possible for a consumer
# to extract data in a controlled fashion. 
# -----------------------------------------------------------------------------
