# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import crcmod
import xml
import datetime

import Record
import xmltodict
import Defines

# All times are delta from 2009-01-01.
G4_BASE_TIME = datetime.datetime(2009, 1, 1)

crc_func = crcmod.predefined.mkCrcFun('xmodem')


class BlindedDeviceException(Exception):
    pass

class FirmwareHeaderRecord(Record.RecordXML):
    """
    G4 firmware header. XML with attributes :

    @FirmwareVersion
    @DexBootVersion
    @RFVersion
    @ApiVersion
    @ProductName
    @TestApiVersion
    @SchemaVersion
    @PortVersion
    @SoftwareNumber
    @ProductId
    """
    def get_transmitter_version(self):
        """
        So far only G4 and G5 is known -> return these versions
        """
        if int(self.doc['FirmwareHeader']['@ApiVersion'].split('.')[0]) >= 3:
            return 5
        else:
            return 4

    def ConvertToDiasendFormat(self):
        """
        Extract the model name and return in Diasend format.
        """
        return {Defines.ELEM_DEVICE_MODEL: 'Dexcom G%d' % self.get_transmitter_version()}

class FirmwareSettingsRecord(Record.RecordXML):
    """
    G4 firmware settings, XML with attributes :

    @FirmwareImageId
    """
    pass


class DatabasePartitionInfo(Record.RecordXML):
    """
    """
    def get_partition(self, name):
        partitions = self.doc['PartitionInfo']['Partition']
        for part in partitions:
            if part['@Name'] == name:
                return part

        return None

    def get_partition_revision(self, name):
        part = self.get_partition(name)
        return int(part['@RecordRevision'])

    def get_partition_length(self, name):
        part = self.get_partition(name)
        return int(part['@RecordLength'])

    def get_egv_record_revision(self):
        return self.get_partition_revision('EGVData')

    def get_egv_record_length(self):
        return self.get_partition_length('EGVData')

    def get_meter_data_record_revision(self):
        return self.get_partition_revision('MeterData')

    def get_meter_data_record_length(self):
        return self.get_partition_length('MeterData')

    def get_user_event_record_revision(self):
        return self.get_partition_revision('UserEventData')

    def get_user_event_record_length(self):
        return self.get_partition_length('UserEventData')

    def get_manufacturing_data_record_revision(self):
        return self.get_partition_revision('ManufacturingData')

    def get_manufacturing_data_record_length(self):
        return self.get_partition_length('ManufacturingData')

    def get_user_setting_record_revision(self):
        return self.get_partition_revision('UserSettingData')

    def get_user_setting_record_length(self):
        return self.get_partition_length('UserSettingData')


class TransmitterIdRecord(Record.BaseRecord):
    """
    Base class for transmitter IDs.
    """
    @classmethod
    def create(cls, version, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        for sub_class in cls.__subclasses__():
            if sub_class.version == version:
                return sub_class(buf, payload_len)

        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            'TransmitterIdRecord : unknown message type %s' % (msg_type))


class TransmitterIdRecordG4(TransmitterIdRecord):
    """
    G4 transmitter id.

    char transmitter_id[5]
    """
    version = 4
    format = ('5s',)
    variables = ('transmitter_id',)


class TransmitterIdRecordG5(TransmitterIdRecord):
    """
    G5 transmitter id.

    char transmitter_id[4]
    """
    version = 5
    format = ('6s',)
    variables = ('transmitter_id',)


class LanguageRecord(Record.BaseRecord):
    """
    G4 language.

    uint16_t language_id

    None = 0,
    English = 0x0409, // CultureInfo : en-US 0x0409 English (United States)
    French = 0x040C, // CultureInfo : fr-FR 0x040C French (France)
    German = 0x0407, // CultureInfo : de-DE 0x0407 German (Germany)
    Dutch = 0x0413, // CultureInfo : nl-NL 0x0413 Dutch (Netherlands)
    Spanish = 0x040A, // CultureInfo : es-ES_tradnl 0x040A Spanish (Spain, Traditional Sort)
    Swedish = 0x041D, // CultureInfo : sv-SE 0x041D Swedish (Sweden)
    Italian = 0x0410, // CultureInfo : it-IT 0x0410 Italian (Italy)
    Czech = 0x0405, // CultureInfo Name="cs-CZ" LCID="1029" EnglishName="Czech (Czech Republic)"
    Finnish = 0x040B, // CultureInfo Name="fi-FI" LCID="1035" EnglishName="Finnish (Finland)"
    French_Canada = 0x0C0C, // CultureInfo Name="fr-CA" LCID="3084" EnglishName="French (Canada)"
    Polish = 0x0415, // CultureInfo Name="pl-PL" LCID="1045" EnglishName="Polish (Poland)"
    Portugese_Brazil = 0x0416, // CultureInfo Name="pt-BR" LCID="1046" EnglishName="Portuguese (Brazil)"
    """
    format = ('H',)
    variables = ('language_id',)


class DisplayTimeOffsetRecord(Record.BaseRecord):
    """
    G4 display time offset.

    int32_t display_time_offset
    """
    format = ('i',)
    variables = ('display_time_offset',)


class RTCRecord(Record.BaseRecord):
    """
    G4 RTC raw value.

    uint32_t_t rtc
    """
    format = ('I',)
    variables = ('rtc',)


class BatteryLevelRecord(Record.BaseRecord):
    """
    G4 battery level.

    uint32_t_t battery_level
    """
    format = ('I',)
    variables = ('battery_level',)


class SystemTimeRecord(Record.BaseRecord):
    """
    G4 system time.

    uint32_t_t system_time
    """
    format = ('I',)
    variables = ('system_time',)


class SystemTimeOffsetRecord(Record.BaseRecord):
    """
    G4 system time offset.

    int32_t system_time_offset
    """
    format = ('i',)
    variables = ('system_time_offset',)


class GlucoseUnitRecord(Record.BaseRecord):
    """
    G4 glucose unit.

    uint8_t glucose_unit

    None,
    mgPerDL = 1,
    mmolPerL = 2,
    """
    format = ('B',)
    variables = ('glucose_unit',)


class BlindedModeRecord(Record.BaseRecord):
    """
    G4 blinded record setting.

    uint8_t blinded_mode

    Unblinded = 0,
    Blinded = 1,
    """
    format = ('B',)
    variables = ('blinded_mode',)


class ClockModeRecord(Record.BaseRecord):
    """
    G4 clock mode.

    uint8_t clock_mode

    ClockMode24Hour = 0,
    ClockMode12Hour = 1,
    """
    format = ('B',)
    variables = ('clock_mode',)


class DeviceModeRecord(Record.BaseRecord):
    """
    G4 device mode.

    uint8_t device_mode

    Normal = 0,
    Manufactuiring = 1,
    """
    format = ('B',)
    variables = ('device_mode',)


class BatteryStateRecord(Record.BaseRecord):
    """
    G4 battery state.

    uint8_t battery_state

    Charging = 1,
    NotCharging = 2,
    NTCFault = 3,
    BadBattery = 4
    """
    format = ('B',)
    variables = ('battery_state',)


class HardwareBoardIdRecord(Record.BaseRecord):
    """
    G4 hardware board id.

    uint16_t hardware_board_id
    """
    format = ('H',)
    variables = ('hardware_board_id',)


class DatabasePageRange(Record.BaseRecord):
    """
    G4 database page range.

    uint32_t_t first_page
    uint32_t_t last_page
    """
    format = ('II',)
    variables = ('first_page', 'last_page')


class DatabasePageHeader(Record.BaseRecord):
    """
    G4 database page header.

    uint32_t first_record_index // Logical page index from 0..N
    uint32_t number_of_records
    uint8_t  record_type
    int8_t   revision
    uint32_t page_number // The logical page index from 0..N
    uint32_t reserved_2
    uint32_t reserved_3
    uint32_t reserved_4
    uint16_t crc
    """
    format = ('IIBbIIIIH',)
    variables = ('first_record_index', 'number_of_records', 'record_type', 'revision', 'page_number', 'reserved_2', 'reserved_3', 'reserved_4', 'crc')

    def Validate(self):
        """
        Validate checksum.
        """
        self.page_header_crc_ok = True
        calc_crc = crc_func(self.raw_buffer[:-2])
        if calc_crc != self.crc:
            self.page_header_crc_ok = False


class DatabasePageManufacturingParameterRecord(Record.BaseRecord):
    """
    Base class for Meter Data Records.
    """
    @classmethod
    def create(cls, partition_info, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        revision = partition_info.get_manufacturing_data_record_revision()
        for sub_class in cls.__subclasses__():
            if revision in sub_class.revisions:
                return sub_class(buf, payload_len)

        raise Exception(Defines.ERROR_CODE_PREPROCESSING_FAILED,
            'DatabasePageManufacturingParameterRecord : unknown revision: %d' % (revision))

    @classmethod
    def RecordLengthRevision(cls, partition_info):
        return partition_info.get_manufacturing_data_record_length()

    def Validate(self):
        """
        Validate checksum.
        """
        calc_crc = crc_func(self.raw_buffer[:-2])
        if calc_crc != self.crc:
            raise Record.ConvertException('Invalid checksum database manufacturing parameter record')


class DatabasePageManufacturingParameterRecordRev1(DatabasePageManufacturingParameterRecord):
    """
    Database page with manufacturing parameters; e.g. serial number.

    uint32_t system_seconds
    uint32_t display_seconds
    string xml_data
    uint16_t crc
    """
    revisions = [1]
    format = ('II490sH',)
    variables = ('system_seconds', 'display_seconds', 'xml_data', 'crc')

    def __init__(self, buffer, payload_len, consume_data=True):
        super(DatabasePageManufacturingParameterRecord, self).__init__(buffer, payload_len, consume_data)
        try:
            self.doc = xmltodict.parse(self.xml_data.replace('\0', ''))
        except xml.parsers.expat.ExpatError:
            raise Record.ConvertException('Invalid XML data')

    def ConvertToDiasendFormat(self):
        """
        Extract the serial number and return in Diasend format.
        """
        try:
            return {Defines.ELEM_DEVICE_SERIAL: self.doc['ManufacturingParameters']['@SerialNumber']}
        except KeyError:
            raise Record.ConvertException('Serial number not available in manufacturing parameters')


class MeterDataRecord(Record.BaseRecord):
    """
    Base class for Meter Data Records.
    """
    @classmethod
    def create(cls, partition_info, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        revision = partition_info.get_meter_data_record_revision()
        for sub_class in cls.__subclasses__():
            if revision in sub_class.revisions:
                return sub_class(buf, payload_len)

        raise Exception(Defines.ERROR_CODE_PREPROCESSING_FAILED,
            'MeterDataRecord : unknown revision: %d' % (revision))

    @classmethod
    def RecordLengthRevision(cls, partition_info):
        return partition_info.get_meter_data_record_length()

    def Validate(self):
        """
        Validate checksum.
        """
        calc_crc = crc_func(self.raw_buffer[:-2])
        if calc_crc != self.crc:
            raise Record.ConvertException('Invalid checksum meter data record')


class MeterDataRecordRev1(MeterDataRecord):
    """
    G4 meter record.

    uint32_t system_seconds
    uint32_t display_seconds
    uint16_t meter_value
    uint32_t meter_time // System time that meter value was taken (as indicated by user during manual entry).
    uint16_t crc
    """
    revisions = [1]
    format = ('IIHIH',)
    variables = ('system_seconds', 'display_seconds', 'meter_value', 'meter_time', 'crc')

    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.display_seconds)

        # Adjust mg -> mmol factor.
        self.meter_value = self.meter_value * 18.0 / 18.016

        return {Defines.ELEM_VAL_TYPE: Defines.VALUE_TYPE_GLUCOSE,
            Defines.ELEM_VAL: self.meter_value,
            Defines.ELEM_FLAG_LIST: [Defines.FLAG_CONTINOUS_READING, Defines.FLAG_CALIBRATION],
            Defines.ELEM_TIMESTAMP: dt}


class MeterDataRecordRev3(MeterDataRecord):
    """
    G5 meter record.

    uint32_t system_seconds
    uint32_t display_seconds
    uint16_t meter_value
    uint8_t entry_type (MeterEntryType)
    uint32_t meter_time // System time that meter value was taken (as indicated by user during manual entry).
    uint32_t meter_transmitter_time
    uint16_t crc
    """
    revisions = [3]
    format = ('2IHB2IH',)
    variables = ('system_seconds', 'display_seconds', 'meter_value', 'entry_type', 'meter_time', 'meter_transmitter_time', 'crc')

    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.display_seconds)

        # Adjust mg -> mmol factor.
        self.meter_value = self.meter_value * 18.0 / 18.016

        return {Defines.ELEM_VAL_TYPE: Defines.VALUE_TYPE_GLUCOSE,
            Defines.ELEM_VAL: self.meter_value,
            Defines.ELEM_FLAG_LIST: [Defines.FLAG_CONTINOUS_READING, Defines.FLAG_CALIBRATION],
            Defines.ELEM_TIMESTAMP: dt}


class EGVDataRecord(Record.BaseRecord):
    """
    Base class for EGV Data Records.
    """
    @classmethod
    def create(cls, partition_info, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        revision = partition_info.get_egv_record_revision()
        for sub_class in cls.__subclasses__():
            if revision in sub_class.revisions:
                return sub_class(buf, payload_len)

        raise Exception(Defines.ERROR_CODE_PREPROCESSING_FAILED,
            'EGVDataRecord : unknown revision: %d' % (revision))

    @classmethod
    def RecordLengthRevision(cls, partition_info):
        return partition_info.get_egv_record_length()

    def Validate(self):
        """
        Validate checksum.
        """
        calc_crc = crc_func(self.raw_buffer[:-2])
        if calc_crc != self.crc:
            raise Record.ConvertException('Invalid checksum EGV data record')


class EGVDataRecordRev1(EGVDataRecord):
    """
    EGV (?) data record.

    uint32_t system_seconds
    uint32_t display_seconds
    uint16_t glucose_value_with_flags
    uint8_t trend_arrow_and_noise
    uint16_t crc;
    """
    revisions = [1,2]
    format = ('IIHbH',)
    variables = ('system_seconds', 'display_seconds', 'glucose_value_with_flags', 'trend_arrow_and_noise', 'crc')

    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        # TODO
        if self.glucose_value_with_flags < 39 or self.glucose_value_with_flags > 0x3ff:
            return {}

        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.display_seconds)

        # Adjust mg -> mmol factor.
        self.glucose_value_with_flags = (self.glucose_value_with_flags & 0x3ff) * 18.0 / 18.016

        return {Defines.ELEM_VAL_TYPE: Defines.VALUE_TYPE_GLUCOSE,
            Defines.ELEM_VAL: self.glucose_value_with_flags,
            Defines.ELEM_FLAG_LIST: [Defines.FLAG_CONTINOUS_READING],
            Defines.ELEM_TIMESTAMP: dt}


class EGVDataRecordRev4(EGVDataRecord):
    """
    EGV (?) data record.

    uint32_t system_seconds
    uint32_t display_seconds
    uint16_t glucose_value_with_flags
    uint32_t egv_system_time_seconds
    uint32_t egv_transmitter_time_seconds
    int8_t filtered_rate
    uint8_t trend_arrow_and_noise
    uint8_t algorithm_state
    uint16_t crc;
    """
    revisions = [4]
    format = ('2IH2Ib2BH',)
    variables = ('system_seconds', 'display_seconds', 'glucose_value_with_flags',
                 'egv_system_time_seconds', 'egv_transmitter_time_seconds',
                 'filtered_rate', 'trend_arrow_and_noise', 'algorithm_state',
                 'crc')

    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        # TODO
        if self.glucose_value_with_flags < 39 or self.glucose_value_with_flags > 0x3ff:
            return {}

        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.display_seconds)

        # Adjust mg -> mmol factor.
        self.glucose_value_with_flags = (self.glucose_value_with_flags & 0x3ff) * 18.0 / 18.016

        return {Defines.ELEM_VAL_TYPE: Defines.VALUE_TYPE_GLUCOSE,
            Defines.ELEM_VAL: self.glucose_value_with_flags,
            Defines.ELEM_FLAG_LIST: [Defines.FLAG_CONTINOUS_READING],
            Defines.ELEM_TIMESTAMP: dt}


class UserEventRecord(Record.BaseRecord):
    """
    Base class for User Event Records.
    """
    @classmethod
    def create(cls, partition_info, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        revision = partition_info.get_user_event_record_revision()
        for sub_class in cls.__subclasses__():
            if revision in sub_class.revisions:
                return sub_class(buf, payload_len)

        raise Exception(Defines.ERROR_CODE_PREPROCESSING_FAILED,
            'UserEventRecord : unknown revision: %d' % (revision))

    @classmethod
    def RecordLengthRevision(cls, partition_info):
        return partition_info.get_user_event_record_length()

    def Validate(self):
        """
        Validate checksum.
        """
        calc_crc = crc_func(self.raw_buffer[:-2])
        if calc_crc != self.crc:
            raise Record.ConvertException('Invalid checksum user event record')


class UserEventRecordRev1(UserEventRecord):
    """
    User event record.

    uint32_t system_seconds
    uint32_t display_seconds
    uint8_t event_type
    uint8_t event_sub_type
    uint32_t event_time // System time that the event occurred.
    uint32_t event_value
    uint16_t crc
    """
    revisions = [1]
    format = ('IIBBIIH',)
    variables = ('system_seconds', 'display_seconds', 'event_type', 'event_sub_type', 'event_time', 'event_value', 'crc')

    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.event_time)

        # Carbs = 1,
        # Insulin = 2,
        # Health = 3,
        # Exercise = 4,

        if self.event_type == 1:
            return {Defines.ELEM_VAL_TYPE: Defines.VALUE_TYPE_CARBS,
                Defines.ELEM_VAL: self.event_value,
                Defines.ELEM_FLAG_LIST: [],
                Defines.ELEM_TIMESTAMP: dt}

        elif self.event_type == 2:
            return {Defines.ELEM_VAL_TYPE: Defines.VALUE_TYPE_INS_BOLUS,
                # According to tests, this value is reported with 1/100 U resolution
                Defines.ELEM_VAL: self.event_value * Defines.VAL_FACTOR_BOLUS / 100,
                Defines.ELEM_FLAG_LIST: [Defines.FLAG_MANUAL],
                Defines.ELEM_TIMESTAMP: dt}

        return {}


class UserSettingRecord(Record.BaseRecord):
    """
    Base class for User Event Records.
    """
    @classmethod
    def create(cls, partition_info, buf, payload_len):
        '''
        Factory : look at the class attribute tag of all
        sub-classes and create an instance of the sub-class
        that matches.
        '''
        revision = partition_info.get_user_setting_record_revision()
        for sub_class in cls.__subclasses__():
            if revision in sub_class.revisions:
                a = sub_class(buf, payload_len)
                return a

        raise Exception(Defines.ERROR_CODE_PREPROCESSING_FAILED,
            'UserSettingRecord : unknown revision: %d' % (revision))

    @classmethod
    def RecordLengthRevision(cls, partition_info):
        return partition_info.get_user_setting_record_length()

    def Validate(self):
        """
        Validate checksum.
        """
        calc_crc = crc_func(self.raw_buffer[:-2])
        if calc_crc != self.crc:
            raise Record.ConvertException('Invalid checksum user setting record')
            
class UserSettingRecordRev3(UserSettingRecord):
    """
    User setting record.

    UInt32 SystemSeconds;//4 4
    UInt32 DisplaySeconds;//4 8
    Int32 SystemTimeOffset;//4 12
    Int32 DisplayTimeOffset;//4 16
    UInt32 TransmitterId;//4 20
    UInt32 EnableFlags;//4 24
    Int16 HighAlarmLevelValue;//2 26
    UInt16 HighAlarmSnoozeTime;//2 28
    Int16 LowAlarmLevelValue;//2 30
    UInt16 LowAlarmSnoozeTime;//2 32
    Int16 RiseRateValue;//2 34
    Int16 FallRateValue;//2 36
    UInt16 OutOfRangeAlarmSnoozeTime;//2 38
    LanguageType Language;//2 40
    UserProfile CurrentUserProfile;//1 41
    SetUpWizardState CurrentSetUpWizardState;//1 42
    byte Reserved_0;//1 43
    byte Reserved_1;//1 44
    byte Reserved_2;//1 45
    byte Reserved_3;//1 46
    
    UInt16 m_crc;//2 48 
    """
    revisions = [3]
    format = ('IIiiIIhHhHhhHHBBBBBBH',)
    variables = ('system_seconds', 'display_seconds', 'system_time_offset', 'display_time_offset', 'transmitter_id', 'enable_flags', 'high_alarm_level_value', 'high_alarm_snooze_time', 'low_alarm_level_value', 'low_alarm_snooze_time', 'rise_rate_value', 'fall_rate_value', 'out_of_range_alarm_snooze_time', 'language', 'current_user_profile', 'current_setup_wizard_state', 'reserved_0', 'reserved_1', 'reserved_2', 'reserved_3', 'crc')
    
    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        
        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.display_seconds)        
        return {'datetime': dt, 'blinded': self.enable_flags & 1}
        
class UserSettingRecordRev5(UserSettingRecord):
    """
    User setting record.

    public UInt32 SystemSeconds;//4 4
    public UInt32 DisplaySeconds;//4 8
    public Int32 SystemTimeOffset;//4 12
    public Int32 DisplayTimeOffset;//4 16
    
    public byte[] TransmitterId;//6 22
    
    private uint EnableFlags;//4 26
    public short HighAlarmLevelValue;//2 28
    public ushort HighAlarmSnoozeTime;//2 30
    public short LowAlarmLevelValue;//2 32
    public ushort LowAlarmSnoozeTime;//2 34
    public short RiseRateValue;//2 36
    public short FallRateValue;//2 38
    
    public ushort OutOfRangeAlarmSnoozeTime;//2 40
    
    public LanguageType Language;//2 42
    public UserProfile CurrentUserProfile;//1 43
    public SetUpWizardState CurrentSetUpWizardState;//1 44
    
    public byte Reserved_0;//1 45
    public byte Reserved_1;//1 46
    public byte Reserved_2;//1 47
    public byte Reserved_3;//1 48
    public UInt16 m_crc; //2 50
    """
    revisions = [5]
    format = ('IIii6sIhHhHhhHHBBBBBBH',)
    variables = ('system_seconds', 'display_seconds', 'system_time_offset', 'display_time_offset', 'transmitter_id', 'enable_flags', 'high_alarm_level_value', 'high_alarm_snooze_time', 'low_alarm_level_value', 'low_alarm_snooze_time', 'rise_rate_value', 'fall_rate_value', 'out_of_range_alarm_snooze_time', 'language', 'current_user_profile', 'current_setup_wizard_state', 'reserved_0', 'reserved_1', 'reserved_2', 'reserved_3', 'crc')
    
    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format supported by Generic.
        """
        
        dt = G4_BASE_TIME + datetime.timedelta(microseconds=1000000 * self.display_seconds)        
        return {'datetime': dt, 'blinded': self.enable_flags & 1}
        