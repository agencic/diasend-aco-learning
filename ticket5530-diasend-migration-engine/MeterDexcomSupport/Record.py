# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import struct
import datetime
import crcmod
import xmltodict

class ConvertException(Exception):
    """
    Unable to convert (unserialize) the data.
    """
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return self.text


class BaseRecord(object):
    """
    Base class for all Dexcom records.
    """

    def __init__(self, buffer, payload_len, consume_data=True):
        """
        Construct a record from the buffer provided (unserialize using the
        format string provided by the sub-class).

        @param buffer
            Buffer.Buffer instance - holds binary data.

        @param payload_len
            Length (extracted from header) of payload. Must match the length
            of the struct format string.

        @param consume_data
            If True consume the data in the buffer when unserializing.

        Function will raise Record.ConvertException if an error is detected.
        """
        # Retrieve the expected payload length from the class and compare with the
        # provided length.
        my_payload_len = self.RecordLength()
        if my_payload_len != payload_len:
            raise ConvertException('Payload length does not match record (payload %d record %d).' % (payload_len, my_payload_len))

        index = 0

        # Save raw buffer for CRC check (later).
        self.raw_buffer = buffer.peek_slice(my_payload_len)

        # The format string can be divided into sub-string where each substring is either a
        # Python struct string _or_ a record class.
        for sub_format in self.format:

            # This is a Python struct string - unserialize.
            if isinstance(sub_format, str):
                if len(sub_format) > 0:

                    # We only support little endian so add the Python struct '<' constant.
                    format_string = '<' + sub_format
                    sub_format_len = struct.calcsize(format_string)
                    if consume_data:
                        index += self.Unserialize(buffer.get_slice(sub_format_len), format_string, self.variables, index)
                    else:
                        index += self.Unserialize(buffer.peek_slice(sub_format_len), format_string, self.variables, index)

            # This is is a class! Create an instance and store it in the provided variable name.
            # Recursion rocks!
            else:
                self.__dict__[self.variables[index]] = sub_format(buffer, sub_format.RecordLength())
                index += 1

            self.Validate()

    def __str__(self):
        """
        Poor mans generic string of a class. If necessary this can be made to look really pretty
        using the format and variables strings.
        """
        return str(self.__dict__)

    def Unserialize(self, data, format, variables, index=0):
        """
        Unserialize data in the form of a string (containing binary data) and map
        the data to instance variables.

        The function uses Python struct module to unserialize a C struct into
        a tuple. This tuple is then mapped into variables from the self.variables
        tuple (provided by the sub-class).

        If the sub-class has the following variables :

            self.format = "<BB"
            self.variables = ("byte1", "byte2")

        a call to unserialize('\x12\x34') will create variables byte1 and byte2
        in the sub-class and set byte1 == 0x12 and byte2 == 0x34.

        @param data
            The binary data to unpack.

        @param format
            The Python struct format string.

        @param variables
            A tuple with the variable names. Note : this can be a tuple with more variables then
            necessary - just use the index to indicate where the function should start
            mapping.

        @param index
            Index into the variables tuple - where to start mapping data to variables.

        @return Number of variables matched.
        """
        data = struct.unpack(format, data)
        my_variables = variables[index:index + len(data)]
        self.__dict__ = dict(self.__dict__, **dict(zip(my_variables, data)))

        return len(data)

    @classmethod
    def BuildFormatString(cls, standalone=True):
        """
        Build a Python struct format string by (if necessary recursively) walk
        through the format variable of the class.

        @param cls
            Class reference

        @param standalone
            Prefix the string with '<' (little endian) if True.

        @return format string.
        """
        if standalone:
            format_string = '<'
        else:
            format_string = ''
        for format in cls.format:
            if isinstance(format, str):
                format_string += format
            else:
                format_string += format.BuildFormatString(False)
        return format_string

    @classmethod
    def RecordLength(cls):
        """
        Return length (as reported by struct.calcsize) for a record.
        """
        format = cls.BuildFormatString()
        return struct.calcsize(format)

    def ConvertToDiasendFormat(self):
        """
        Convert record to Diasend format.

        Returns dictionary - empty if no useful data.
        """
        return {}

    def Validate(self):
        return True


class RecordConsumeData(BaseRecord):
    """
    Generic record that simply consumes the number of bytes
    provided in the constructor.
    """
    format = ('',)
    variables = ('',)

    def __init__(self, buffer, payload_len, consume_data=True):
        """
        Create struct format with a single parameter.
        """
        if payload_len > 0:
            RecordConsumeData.format = ('%ds' % (payload_len),)
            RecordConsumeData.variables = ('all'),
        else:
            RecordConsumeData.format = ('',)
            RecordConsumeData.variables = ('',)

        super(RecordConsumeData, self).__init__(buffer, payload_len, consume_data)


class RecordXML(RecordConsumeData):
    """
    Generic record where the payload consists of data in XML-format. Uses the xmltodict
    module to convert the XML data to a dictionary.
    """
    def __init__(self, buffer, payload_len, consume_data=True):
        super(RecordXML, self).__init__(buffer, payload_len, consume_data)
        try:
            self.doc = xmltodict.parse(self.all)
        except xml.parsers.expat.ExpatError, e:
            raise ConvertException('Invalid XML data')
