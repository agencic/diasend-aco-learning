# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
import struct
import datetime
import crcmod

from Defines import *
import Buffer

crc_func = crcmod.predefined.mkCrcFun('xmodem')

# R2 record types from R2DataEnums.cs
R2_RECORD_TYPE_UNASSIGNED       = 0xFF
R2_RECORD_TYPE_ENDOFBLOCK       = 0x00
R2_RECORD_TYPE_CORRUPTED        = 0x01
R2_RECORD_TYPE_UNKNOWN1         = 0x0B
R2_RECORD_TYPE_LOG              = 0x03
R2_RECORD_TYPE_EVENT            = 0x14
R2_RECORD_TYPE_METER            = 0x04
R2_RECORD_TYPE_METER2           = 0x11
R2_RECORD_TYPE_METER3           = 0x16
R2_RECORD_TYPE_BADTIMEMETER     = 0x05
R2_RECORD_TYPE_SENSOR           = 0x06
R2_RECORD_TYPE_SENSOR2          = 0x0A
R2_RECORD_TYPE_SENSOR3          = 0x17
R2_RECORD_TYPE_SETTINGS         = 0x08
R2_RECORD_TYPE_SETTINGS2        = 0x0C
R2_RECORD_TYPE_SETTINGS3        = 0x13
R2_RECORD_TYPE_SETTINGS4        = 0x19
R2_RECORD_TYPE_SETTINGS5        = 0x1A
R2_RECORD_TYPE_SETTINGS6        = 0x1B
R2_RECORD_TYPE_RESERVED0x02     = 0x02
R2_RECORD_TYPE_RESERVED0x07     = 0x07
R2_RECORD_TYPE_RESERVED0x09     = 0x09
R2_RECORD_TYPE_RESERVED0x0D     = 0x0D
R2_RECORD_TYPE_RESERVED0x0E     = 0x0E
R2_RECORD_TYPE_RESERVED0x0F     = 0x0F
R2_RECORD_TYPE_RESERVED0x10     = 0x10
R2_RECORD_TYPE_RESERVED0x12     = 0x12
R2_RECORD_TYPE_RESERVED0x15     = 0x15
R2_RECORD_TYPE_RESERVED0x18     = 0x18

# Type of meter - determines if the value is manual or not. 
# Note from Phil Mayo . 
# "ManualEntry" enum for meter type is correct for manual bg.
R2_METER_TYPE_MANUAL = 0
R2_METER_TYPE_ONETOUCH = 1

# Which type of time do you want?
R2_DISPLAY_TIME    = "display_time"
R2_INTERNAL_TIME   = "internal_time"

# All times are delta from 1970-01-01. 
R2_BASE_TIME = datetime.datetime(1970, 1, 1)

# R2 event types.
R2_EVENT_TYPE_CARBS = 0
R2_EVENT_TYPE_INSULIN = 1
R2_EVENT_TYPE_EXCERCISE = 2
R2_EVENT_TYPE_HEALTH = 3

# R2 firmware flags defines.
R2_FIRMWARE_FLAGS_UNKNOWN           = 0x0000
R2_FIRMWARE_FLAGS_ONETOUCH          = 0x0001
R2_FIRMWARE_FLAGS_MANUAL            = 0x0002
R2_FIRMWARE_FLAGS_GLUCOSE_MMOL      = 0x0004
R2_FIRMWARE_FLAGS_CLOCK_24H         = 0x0008
R2_FIRMWARE_FLAGS_SUPPORTS_3DAY     = 0x0010
R2_FIRMWARE_FLAGS_SUPPORTS_5DAY     = 0x0020
R2_FIRMWARE_FLAGS_SUPPORTS_7DAY     = 0x0040

# Event record - exercise string conversion
R2_EXERCISE_TYPE = { \
    0:'LIGHT', \
    1:'MEDIUM', \
    2:'HARD' }

# Event record - health string conversion
R2_HEALTH_TYPE = { \
    1:'ILLNESS' , \
    2:'STRESS' , \
    3:'HIGH SYMPTOMS' , \
    4:'LOW SYMPTOMS' , \
    5:'PERIOD' , \
    6:'ALCOHOL'}

# Log record - health string conversion
R2_LOG_TYPE = { \
    0:'Unknown' , \
    1:'Trend1Hour' , \
    2:'Trend3Hour' , \
    3:'Trend9Hour' , \
    4:'Connection' , \
    5:'Disconnect' , \
    6:'ConnectFailed' , \
    7:'BloodDrop' , \
    8:'LowGlucoseAlert' , \
    9:'UserSelectedLowGlucoseAlert' , \
    10:'UserSelectedHighGlucoseAlert' , \
    11:'OutOfCalibration' , \
    12:'BatteryCharging' , \
    13:'Boot' , \
    14:'Debug' , \
    15:'NewSTSInsertion' , \
    16:'STSConfirmation' , \
    17:'STSFailed' , \
    18:'STSRemoval' , \
    19:'XtalTrim' , \
    20:'HighLowLead' , \
    21:'HighLowUnits' , \
    22:'HighLowHighAlert' , \
    23:'HighLowLowAlert' , \
    24:'TDLead' , \
    25:'TD12or24' , \
    26:'TDTimeSet' , \
    27:'SNLead' , \
    28:'SNSetTransmitter' , \
    29:'NewBloodGlucoseMeter' , \
    30:'DualBloodDrop' , \
    31:'OneOfTwoDrops' , \
    32:'SensorShutoff' , \
    34:'ManualBGEntry' , \
    35:'ManualBGEntryLead' , \
    36:'ManualBGEntryConfirmation' , \
    37:'STSSelect' , \
    38:'LicenseEntryLead' , \
    39:'LicenseEntry' , \
    40:'SettingsLead' , \
    41:'MeterSelection' , \
    42:'About' , \
    43:'DongleProgram' , \
    44:'BeepVibeSetting' , \
    45:'OptionsMenu' , \
    46:'Powerdown' , \
    47:'EventsEntry' , \
    48:'ExerciseEntry' , \
    49:'CarbEntry' , \
    50:'InsulinEntry' , \
    51:'ExerciseTimeEntry' , \
    52:'InsulinTimeEntry' , \
    53:'CarbTimeEntry' , \
    54:'StartSessionProcessing' , \
    55:'StopSessionProcessing' , \
    56:'HighSnooze' , \
    57:'LowSnooze' , \
    58:'HighBeepVibeSetting' , \
    59:'LowBeepVibeSetting' , \
    60:'AlertsMenu' , \
    61:'SettingsMenu' , \
    62:'OtherBeepVibeSetting' , \
    63:'OutOfRangeBeepVibeSetting' , \
    64:'OutOfRangeSetting' , \
    65:'ExerciseDuration' , \
    66:'Trend' , \
    67:'CarbResults' , \
    68:'InsulinResults' , \
    69:'ExerciseResults' , \
    70:'HealthEntry' , \
    71:'HealthTimeEntry' , \
    72:'HealthResults' , \
    73:'UpRateSetting' , \
    74:'DownRateSetting' , \
    75:'UpRateBeepVibeSetting' , \
    76:'DownRateBeepVibeSetting' , \
    0x100:'AlertBatteryCharging', \
    0x110:'AlertMeterConnected', \
    0x120:'AlertMeterCommComplete', \
    0x130:'AlertFailedMeterComm', \
    0x140:'AlertLowGlucose', \
    0x150:'AlertUserSelectLowGlucose', \
    0x160:'AlertUserSelectHighGlucose', \
    0x170:'AlertNormalGlucose', \
    0x180:'AlertCalibrationRequired', \
    0x190:'AlertPossibleOutlier', \
    0x1A0:'AlertOutlierConditionResolved', \
    0x1B0:'AlertLowBattery', \
    0x1C0:'AlertFingerStickRequest', \
    0x1D0:'AlertFailedShortTermSensor', \
    0x1E0:'AlertMeterCommSuccessful', \
    0x1F0:'AlertNewMeterDeteceted', \
    0x200:'AlertDualBloodDrop', \
    0x210:'AlertOneOfTwoDrops', \
    0x220:'AlertSensorRemoved', \
    0x230:'AlertSensorShutoff', \
    0x240:'AlertSilentSensorShutoff', \
    0x250:'AlertAlertsComplete', \
    0x260:'AlertDongleSuccess', \
    0x270:'AlertDongleFailure', \
    0x280:'AlertDongleConnected', \
    0x2A0:'AlertDiscrepancy', \
    0x2B0:'AlertSensorHighWedgeError', \
    0x2C0:'AlertSensorLowWedgeError', \
    0x2D0:'AlertNormalWedge', \
    0x2E0:'AlertMeterResults', \
    0x2F0:'AlertOutOfRange', \
    0x300:'AlertPacketReceived', \
    0x310:'AlertOutOfRangeDisabled', \
    0x320:'AlertRateUp', \
    0x330:'AlertRateMediumUp', \
    0x340:'AlertRateFlat', \
    0x350:'AlertRateMediumDown', \
    0x360:'AlertRateDown', \
    0x370:'AlertTest', \
    0x380:'AlertSetTime', \
    0x390:'AlertRecoverableError', \
    0x3A0:'AlertAberrationDetected', \
    0x3B0:'AlertForcedSensorShutoff' }
    
# R2 Block header statuses
R2_BLOCK_STATUS_USED                = 0xC00C
R2_BLOCK_STATUS_READY_TO_ERASE      = 0x8008
R2_BLOCK_STATUS_ERASED              = 0xF00F
    
# R2 glucose threshold values that indicate OFF. 
R2_MAXIMUM_HIGH_GLUCOSE_THRESHOLD   = 402
R2_MINIMUM_HIGH_GLUCOSE_THRESHOLD   = 55
    
class ConvertException(Exception):
    """
    Unable to convert (unserialize) the data. 
    """
    def __init__(self, text):
        self.text = text
        
    def __str__(self):
        return self.text

class BaseR2Record(object):
    """
    Base class for R2 records.
    """
    
    def __init__(self, buffer, payload_len, consume_data = True):
        """
        Construct an R2 record from the buffer provided (unserialize using the 
        format string provided by the sub-class). 
        
        @param buffer
            Buffer.Buffer instance - holds binary data. 
            
        @param payload_len
            Length (extracted from header) of payload. Must match the length 
            of the struct format string. 
            
        @param consume_data
            If True consume the data in the buffer when unserializing. 
            
        Function will raise R2Record.ConvertException if an error is detected.
        """
        
        # Retrieve the expected payload length from the class and compare with the
        # provided length. 
        my_payload_len = self.RecordLength()
        if my_payload_len != payload_len:
            raise ConvertException('Payload length does not match record (payload %d record %d).' % (payload_len, my_payload_len))
        
        index = 0
        
        # Save raw buffer for CRC check (later). 
        self.raw_buffer = buffer.peek_slice(my_payload_len)
        
        # The format string can be divided into sub-string where each substring is either a 
        # Python struct string _or_ an R2 class.
        for sub_format in self.format:
        
            # This is a Python struct string - unserialize.
            if isinstance(sub_format, str):
                if len(sub_format) > 0:
                
                    # We only support little endian so add the Python struct '<' constant. 
                    format_string = '<' + sub_format
                    sub_format_len = struct.calcsize(format_string)
                    if consume_data:
                        index += self.Unserialize(buffer.get_slice(sub_format_len), format_string, self.variables, index)
                    else:
                        index += self.Unserialize(buffer.peek_slice(sub_format_len), format_string, self.variables, index)
                        
            # This is is a class! Create an instance and store it in the provided variable name. 
            # Recursion rocks!
            else:
                self.__dict__[self.variables[index]] = sub_format(buffer, sub_format.RecordLength())
                index += 1
                
    def __str__(self):
        """
        Poor mans generic string of a class. If necessary this can be made to look really pretty 
        using the format and variables strings. 
        """
        return str(self.__dict__)
                                
    def Unserialize(self, data, format, variables, index = 0):
        """
        Unserialize data in the form of a string (containing binary data) and map 
        the data to instance variables. 
        
        The function uses Python struct module to unserialize a C struct into 
        a tuple. This tuple is then mapped into variables from the self.variables
        tuple (provided by the sub-class). 
        
        If the sub-class has the following variables : 
        
            self.format = "<BB"
            self.variables = ("byte1", "byte2")
            
        a call to unserialize('\x12\x34') will create variables byte1 and byte2
        in the sub-class and set byte1 == 0x12 and byte2 == 0x34.
        
        @param data
            The binary data to unpack. 
            
        @param format
            The Python struct format string.
            
        @param variables
            A tuple with the variable names. Note : this can be a tuple with more variables then
            necessary - just use the index to indicate where the function should start 
            mapping. 
            
        @param index
            Index into the variables tuple - where to start mapping data to variables. 
            
        @return Number of variables matched.
        """
        data = struct.unpack(format, data)
        my_variables = variables[index:index+len(data)]
        self.__dict__ = dict(self.__dict__, **dict(zip(my_variables, data)))
        
        return len(data)
        
    @classmethod
    def BuildFormatString(cls, standalone = True):
        """
        Build a Python struct format string by (if necessary recursively) walk 
        through the format variable of the class. 
        
        @param cls 
            Class reference
            
        @param standalone
            Prefix the string with '<' (little endian) if True.
            
        @return format string.
        """
        if standalone:
            format_string = '<'
        else:
            format_string = ''
        for format in cls.format:
            if isinstance(format, str):
                format_string += format
            else:
                format_string += format.BuildFormatString(False)
        return format_string
        
    @classmethod
    def RecordLength(cls):
        """
        Return length (as reported by struct.calcsize) for a record.
        """
        format = cls.BuildFormatString()
        return struct.calcsize(format)
        
    def CheckMatchingHeaderAndFooter(self):
        """
        Check if the type in the header and footer matches.
        
        @return True if mathed. 
        """
        if self.__dict__.has_key('header') and self.__dict__.has_key('footer'):
            return self.header.type == self.footer.type
        
        return False

    def CheckCRC(self):
        """
        Check CRC of record.
        """
        global crc_func
        
        if self.__dict__.has_key('footer'):
            # The CRC should be calculated on the entire buffer except the footer. 
            size_of_footer = self.footer.RecordLength()
            crc_calc = crc_func(self.raw_buffer[:-size_of_footer])
            
            return crc_calc == self.footer.crc
        
        return False

class R2Generic(BaseR2Record):
    """
    Generic R2 record that simply consumes the number of bytes
    provided in the constructor.
    """
    format = ('',)
    variables = ('',)
    
    def __init__(self, buffer, payload_len, consume_data = True):
        """
        Create struct format with a single 
        """
        if payload_len > 0:
            R2Generic.format = ('%ds' % (payload_len),)
            R2Generic.variables = ('all'),
        else:
            R2Generic.format = ('',)
            R2Generic.variables = ('',)
    
        super(R2Generic, self).__init__(buffer, payload_len, consume_data)

# --- R2 Records

class R2BlockHeader(BaseR2Record):
    """
    public UInt16 m_headerTag;
    public UInt16 m_status;
    public UInt32 m_number;
    """
    format      = ('HHI',)
    variables   = ('header_tag', 'status', 'number')

class R2RecordHeader(BaseR2Record):
    """
    public byte m_type;
    public UInt32 m_previousAddress;
    public UInt32 m_previousBlock;
    """
    format      = ('BII',)
    variables   = ('type', 'previous_address', 'previous_block')

class R2DatabaseKey(BaseR2Record):
    """
    public Int32 m_recordNumber;
    public UInt64 m_timeStamp;
    public UInt32 m_address;
    public UInt32 m_block;
    """
    format      = ('IQII',)
    variables   = ('record_number', 'timestamp', 'address', 'block')

class R2DatabaseBlockPrefix(BaseR2Record):
    """
    public R2BlockHeader m_blockHeader;
    public R2RecordHeader m_recordHeader;
    public R2DatabaseKey m_databaseKey;    
    """
    format      = (R2BlockHeader, R2RecordHeader, R2DatabaseKey)
    variables   = ('block_header', 'record_header', 'database_key')

class R2DatabaseRecordPrefix(BaseR2Record):
    """
    public R2RecordHeader m_recordHeader;
    public R2DatabaseKey m_databaseKey;
    """
    format      = (R2RecordHeader, R2DatabaseKey)
    variables   = ('record_header', 'database_key')
    
class R2RecordFooter(BaseR2Record):
    """
    public UInt16 m_crc;
    public byte m_type;
    """
    format      = ('HB')
    variables   = ('crc', 'type')

class R2RecordFooterUpdateable(BaseR2Record):
    """
    public UInt16 m_crc;
    public UInt16 m_data;
    public byte m_type;
    """
    format      = ('HHB')
    variables   = ('crc', 'data', 'type')

class R2RecordFooterUpdateableWithCrc(BaseR2Record):
    """
    public UInt16 m_crc;
    public UInt16 m_updateableData;
    public UInt16 m_updateableCrc;
    public byte m_type;
    """
    format      = ('HHHB')
    variables   = ('crc', 'updateable_data', 'updateable_crc', 'type')

class R2LogRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte m_eventType;
    public byte m_data0;
    public UInt16 m_data1;
    public UInt16 m_data2;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBHH', R2RecordFooter)
    variables   = ('header', 'key', 'event_type', 'data0', 'data1', 'data2', 'footer')
    
    def __str__(self):
        return "LOG %s %x %x %x" % (R2_LOG_TYPE[self.event_type], self.data0, self.data1, self.data2)

class R2EventRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte m_eventType;
    public byte m_eventValue0;     // Unused as of 7.0.0.0
    public UInt16 m_eventValue1; // Insulin units, or Exercise type, or Carbs in grams, or Health type (Illness, Stress, High Symptoms, Low Symptoms, Period, Alcohol).
    public UInt32 m_eventValue2; // Unused as of 7.0.0.0
    public UInt64 m_timeStamp;     // Event's time entered by user: Display Time
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBHIQ', R2RecordFooter)
    variables   = ('header', 'key', 'event_type', 'event_value0', 'event_value1', 'event_value2', 'timestamp', 'footer')
        
    def GetDateTime(self, type, user_offset):
        """
        Return a datetime object representing the time associated with the event record. Internally
        time is divided into internal which is represented by the key.timestamp variable and 
        user (or displayed) which is internal + a global user offset that is stored in the 
        event records. 
        """

        # Note : to match the time extracted in FileImportDexcomXML we have to add 0.5 
        # seconds to the time . 

        if type == R2_DISPLAY_TIME:
            return R2_BASE_TIME + datetime.timedelta(microseconds = 500000 + 1000 * (self.key.timestamp + user_offset))
        else:
            return R2_BASE_TIME + datetime.timedelta(microseconds = 500000 + 1000 * (self.key.timestamp))
            
    def GetRecordDictionary(self, user_offset):
        """
        Get a generic record (dictionary) of the meter record. 
        """
        record = {}
        
        if self.event_type == R2_EVENT_TYPE_CARBS:
            record['record_type'] = 'carbs'
            record['record_value'] = self.event_value1
            record['record_date_time'] = self.GetDateTime(R2_DISPLAY_TIME, user_offset)
            record['record_flags'] = []

        elif self.event_type == R2_EVENT_TYPE_INSULIN:
            record['record_type'] = 'insulin'
            # Unit is stored with factor 100, i.e. 3.5 is stored as 350
            record['record_value'] = (self.event_value1 * VAL_FACTOR_BOLUS) / 100
            record['record_date_time'] = self.GetDateTime(R2_DISPLAY_TIME, user_offset)
            record['record_flags'] = []

        return record

class R2MeterRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public UInt16 m_glucoseValue;
    public UInt64 m_glocoseTimeStamp;
    public UInt64 m_worldTimeStamp;
    public R2RecordFooterUpdateable m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'HQQ', R2RecordFooterUpdateable)
    variables   = ('header', 'key', 'glucose_value', 'glucose_timestamp', 'world_timestamp', 'footer')

    def GetDateTime(self, type):
        """
        Return a datetime object representing the time associated with the meter record. If type is
        R2_DISPLAY_TIME the display (or user) time is returned, otherwise return
        the internal time.
        """

        # Note : to match the time extracted in FileImportDexcomXML we have to add 0.5 
        # seconds to the time . 

        if type == R2_DISPLAY_TIME:
            return R2_BASE_TIME + datetime.timedelta(microseconds = 500000 + 1000 * (self.world_timestamp))
        else:
            return R2_BASE_TIME + datetime.timedelta(microseconds = 500000 + 1000 * (self.glucose_timestamp))

    def GetGlucoseValue(self):
        """
        Return sensor glucose value. 
        """
        # Meter3 redefined glucose values to have high nibble "flags".
        if self.header.type == R2_RECORD_TYPE_METER3:
            return self.glucose_value & 0xfff
        else:
            return self.glucose_value
            
    def GetRecordDictionary(self, user_offset):
        """
        Get a generic record (dictionary) of the meter record. 
        """   
        record = {'record_type':'meter'}
        # This version of the backend has no support for different BG conversions factors for 
        # different devices. So we make this rather fishy recalculation to force Generic to 
        # use 18.016 instead of 18.0. This is done to match the XML file format import function
        # (which has the same recalculation).
        record['record_value'] = self.GetGlucoseValue()*18.0/18.016
        record['record_date_time'] = self.GetDateTime(R2_DISPLAY_TIME)
        record['record_flags'] = []
        
        # Only R2Meter2Record has the meter_type - ignore it (or set it to non-manual) for older
        # firmware.
        if self.__dict__.has_key('meter_type'):
            if self.meter_type == R2_METER_TYPE_MANUAL:
                record['record_flags'].append(FLAG_MANUAL)
                # Note from Phil Mayo : 
                # Manual entry of bg values are discrete and each taken into account for calibration.
                record['record_flags'].append(FLAG_CALIBRATION)
                
                # p624:#2051. 
                record['record_flags'].append(FLAG_CONTINOUS_READING)
        
        return record
        
class R2Meter2Record(R2MeterRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Byte m_meterType;
    public UInt16 m_glucoseValue;
    public UInt64 m_glocoseTimeStamp;
    public UInt64 m_worldTimeStamp;
    public R2RecordFooterUpdateable m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BHQQ', R2RecordFooterUpdateable)
    variables   = ('header', 'key', 'meter_type', 'glucose_value', 'glucose_timestamp', 'world_timestamp', 'footer')
        
class R2SensorRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public UInt16 unknown1;
    public R2RecordFooterUpdateableWithCrc m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'H', R2RecordFooterUpdateableWithCrc)
    variables   = ('header', 'key', 'unknown1', 'footer')
    
    def GetDateTime(self, user_offset = 0):
        """
        Return a datetime object representing the time associated with the sensor record. Internally
        time is divided into internal which is represented by the key.timestamp variable and 
        user (or displayed) which is internal + a global user offset that is stored in the 
        settings records. 
        """
        
        # Note : to match the time extracted in FileImportDexcomXML we have to add 0.5 
        # seconds to the time . 
        
        return R2_BASE_TIME + datetime.timedelta(microseconds = 500000 + 1000 * (self.key.timestamp + user_offset))
        
    def GetGlucoseValue(self):
        """
        Return sensor glucose value. 
        """
        # Sensor3 redefined glucose values to have high nibble "flags"
        if self.header.type == R2_RECORD_TYPE_SENSOR3:
            return self.footer.updateable_data & 0xfff
        else:
            return self.footer.updateable_data

    def GetRecordDictionary(self, user_offset):
        """
        Get a generic record (dictionary) of the sensor record. 
        """     
        record = {'record_type':'sensor'}
        # See comment R2MeterRecord.GetGlucoseValue.
        record['record_value'] = self.GetGlucoseValue()*18.0/18.016
        record['record_date_time'] = self.GetDateTime(user_offset)
        record['record_flags'] = [FLAG_CONTINOUS_READING]
        
        return record

class R2Sensor2Record(R2SensorRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public UInt32 unknown1;
    public UInt64 m_receivedTime;
    public UInt32 m_transmitterId;
    public R2RecordFooterUpdateableWithCrc m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'IQI', R2RecordFooterUpdateableWithCrc)
    variables   = ('header', 'key', 'unknown1', 'received_time', 'transmitter_id', 'footer')

class R2SettingsRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Int64 m_skewOffset;
    public Int64 m_externalOffset;
    public UInt32 m_lowGlucoseThreshold;
    public UInt32 m_highGlucoseThreshold;
    public UInt32 m_transmitterSerialNumber;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
    public string m_meterSerialNumber;
    public byte m_glucoseUnits;
    public byte m_clockMode;
    public byte m_displayMode;
    public byte m_unused;
    public UInt32 m_recordsInLastMeterTransfer;
    public UInt64 unknown1;
    public UInt64 m_lastMeterTransferTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'qqIII12sBBBBIQQ', R2RecordFooter)
    variables   = ('header', 'key', 'skew_offset', 'external_offset', 'low_glucose_threshold', \
                   'high_glucose_threshold', 'transmitter_serial_number', 'meter_serial_number', \
                   'glucose_units', 'clock_mode', 'display_mode', 'unused', 'records_in_last_meter_transfer', \
                   'unknown1', 'last_meter_transfer_time', 'footer')
                   
    def GetDateTime(self):
        """
        Return a datetime object representing the time associated with the settings record.
        We return the internal time here (this must be best for determining the last 
        settings record).
        """

        # Note : to match the time extracted in FileImportDexcomXML we have to add 0.5 
        # seconds to the time . 

        return R2_BASE_TIME + datetime.timedelta(microseconds = 500000 + 1000 * (self.key.timestamp))

class R2Settings2Record(R2SettingsRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Int64 m_skewOffset;
    public Int64 m_externalOffset;
    public UInt32 m_lowGlucoseThreshold;
    public UInt32 m_highGlucoseThreshold;
    public UInt32 m_transmitterSerialNumber;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
    public string m_meterSerialNumber;
    public byte m_glucoseUnits;
    public byte m_clockMode;
    public byte m_displayMode;
    public byte m_unused1;
    public UInt32 m_recordsInLastMeterTransfer;
    public byte unknown2;
    public byte unknown3;
    public UInt16 m_unused2;
    public UInt64 unknown1;
    public UInt64 m_lastMeterTransferTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'qqIII12sBBBBIBBHQQ', R2RecordFooter)
    variables   = ('header', 'key', 'skew_offset', 'external_offset', 'low_glucose_threshold', \
                   'high_glucose_threshold', 'transmitter_serial_number', 'meter_serial_number', \
                   'glucose_units', 'clock_mode', 'display_mode', 'unknown1', 'records_in_last_meter_transfer', \
                   'unknown2', 'unknown3', 'unused2', 'unknown1', 'last_meter_transfer_time', 'footer')

class R2Settings3Record(R2SettingsRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Int64 m_skewOffset;
    public Int64 m_externalOffset;
    public UInt32 m_lowGlucoseThreshold;
    public UInt32 m_highGlucoseThreshold;
    public UInt32 m_transmitterSerialNumber;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
    public string m_meterSerialNumber;
    public byte m_beepOnReceive;
    public byte m_backlightEnabled;
    public byte m_displayMode;
    public byte m_unused;
    public UInt32 m_recordsInLastMeterTransfer;
    public UInt16 m_highSnooze;
    public byte m_meterType;
    public byte m_isVibeOnly;
    public UInt16 m_lowSnooze;
    public byte m_unused2;
    public byte m_unused3;
    public UInt64 unknown1;
    public UInt64 m_lastMeterTransferTime;
    public R2RecordFooter m_footer;    
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'qqIII12sBBBBIHBBHBBQQ', R2RecordFooter)
    variables   = ('header', 'key', 'skew_offset', 'external_offset', 'low_glucose_threshold', \
                   'high_glucose_threshold', 'transmitter_serial_number', 'meter_serial_number', \
                   'beep_on_receive', 'backlight_enabled', 'display_mode', 'unused', 'records_in_last_meter_transfer', \
                   'high_snooze', 'meter_type', 'is_vibe_only', 'low_snooze', 'unused2', 'unused3', 'unknown1', \
                   'last_meter_transfer_time', 'footer')

class R2Settings4Record(R2SettingsRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Int64 m_skewOffset;
    public Int64 m_externalOffset;
    public UInt16 m_lowGlucoseThreshold;
    public byte m_lowGlucoseAlertType;
    public byte m_outOfRangeAlertType;
    public UInt16 m_highGlucoseThreshold;
    public byte m_highGlucoseAlertType;
    public byte m_otherAlertType;
    public UInt32 m_transmitterSerialNumber;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
    public string m_meterSerialNumber;
    public byte m_beepOnReceive;
    public byte m_backlightEnabled;
    public byte m_displayMode;
    public byte m_unused;
    public UInt32 m_recordsInLastMeterTransfer;
    public UInt16 m_highSnooze;
    public byte m_meterType;
    public byte m_reserved;
    public UInt16 m_lowSnooze;
    public UInt16 m_outOfRangeMinutes;
    public UInt64 unknown1;
    public UInt64 m_lastMeterTransferTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'qqHBBHBBI12sBBBBIHBBHHQQ', R2RecordFooter)
    variables   = ('header', 'key', 'skew_offset', 'external_offset', 'low_glucose_threshold', \
                   'low_glucose_alert_type', 'out_of_range_alert_type', 'high_glucose_threshold', \
                   'high_glucose_alert_type', 'other_alert_type', 'transmitter_serial_number', \
                   'meter_serial_number', 'beep_on_receive', 'backlight_enabled', 'display_mode', \
                   'unused', 'records_in_last_meter_transfer', 'high_snooze', 'meter_type', 'reserved', \
                   'low_snooze', 'out_of_range_minutes', 'unknown1', 'last_meter_transfer_time', 'footer')

class R2Settings5Record(R2SettingsRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Int64 m_skewOffset;
    public Int64 m_externalOffset;
    public UInt16 m_lowGlucoseThreshold;
    public byte m_lowGlucoseAlertType;
    public byte m_outOfRangeAlertType;
    public UInt16 m_highGlucoseThreshold;
    public byte m_highGlucoseAlertType;
    public byte m_otherAlertType;
    public UInt32 m_transmitterSerialNumber;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
    public string m_meterSerialNumber;
    public byte m_beepOnReceive;
    public byte m_backlightEnabled;
    public byte m_displayMode;
    public byte m_upRateAlertType;
    public UInt32 m_recordsInLastMeterTransfer;
    public UInt16 m_highSnooze;
    public byte m_meterType;
    public byte m_downRateAlertType;
    public UInt16 m_lowSnooze;
    public UInt16 m_outOfRangeMinutes;
    public byte m_upRate;
    public byte m_downRate;
    public UInt16 m_reserved;
    public UInt64 unknown1;
    public UInt64 m_lastMeterTransferTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'qqHBBHBBI12sBBBBIHBBHHBBHQQ', R2RecordFooter)
    variables   = ('header', 'key', 'skew_offset', 'external_offset', 'low_glucose_threshold', \
                   'low_glucose_alert_type', 'out_of_range_alert_type', 'high_glucose_threshold', \
                   'high_glucose_alert_type', 'other_alert_type', 'transmitter_serial_number', \
                   'meter_serial_number', 'beep_on_receive', 'backlight_enabled', 'display_mode', \
                   'up_rate_alert_type', 'records_in_last_meter_transfer', 'high_snooze', 'meter_type', \
                   'down_rate_alert_type', 'low_snooze', 'out_of_range_minutes', 'up_rate', 'down_rate', \
                   'reserved', 'unknown1', 'last_meter_transfer_time', 'footer')

class R2Settings6Record(R2SettingsRecord):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public Int64 m_skewOffset;
    public Int64 m_externalOffset;
    public UInt16 m_lowGlucoseThreshold;
    public byte m_lowGlucoseAlertType;
    public byte m_outOfRangeAlertType;
    public UInt16 m_highGlucoseThreshold;
    public byte m_highGlucoseAlertType;
    public byte m_otherAlertType;
    public UInt32 m_transmitterSerialNumber;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
    public string m_meterSerialNumber;
    public byte m_beepOnReceive;
    public byte m_backlightEnabled;
    public byte m_displayMode;
    public byte m_upRateAlertType;
    public UInt32 m_recordsInLastMeterTransfer;
    public UInt16 m_highSnooze;
    public byte m_meterType;
    public byte m_downRateAlertType;
    public UInt16 m_lowSnooze;
    public UInt16 m_outOfRangeMinutes;
    public byte m_upRate;
    public byte m_downRate;
    public UInt16 m_reserved;
    public UInt64 unknown0;
    public UInt64 unknown1;
    public UInt64 m_lastMeterTransferTime;
    public R2RecordFooter m_footer;
    """
    format = (R2RecordHeader, R2DatabaseKey, 'qqHBBHBBI12sBBBBIHBBHHbbHQQQ', R2RecordFooter)
    variables =  ('header', 'key', 'skew_offset', 'external_offset', 'low_glucose_threshold', \
                  'low_glucose_alert_type', 'out_of_range_alert_type', 'high_glucose_threshold', \
                  'high_glucose_alert_type', 'other_alert_type', 'transmitter_serial_number', \
                  'meter_serial_number', 'beep_on_receive', 'backlight_enabled', 'display_mode', \
                  'up_rate_alert_type', 'records_in_last_meter_transfer', 'high_snooze', 'meter_type', \
                  'down_rate_alert_type', 'low_snooze', 'out_of_range_minutes', 'up_rate', \
                  'down_rate', 'reserved', 'unknown0', 'unknown1', 'last_meter_transfer_time', \
                  'footer')

class R2Reserved0x07Record(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2RecordFooter)
    variables   = ('header', 'key', 'unknown1', 'unknown2', 'footer')

class R2Reserved0x09Record(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public UInt32 unknownA;
    public UInt32 unknownB;
    public UInt32 unknownC;
    public R2DatabaseKey unknownD;
    public R2DatabaseKey unknownE;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public UInt64 m_sensorInsertionTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'III', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddQ', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'unknownD', 'unknownE', 'unknown0', \
                   'unknown1', 'unknown2', 'unknown3', 'unknown4', 'unknown5', 'unknown6', 'unknown7', \
                   'sensor_insertion_time', 'footer')

class R2Reserved0x0DRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte unknownA;
    public byte unknownB;
    public byte unknownC;
    public byte m_padding1;
    public UInt16 m_sensorLife;
    public R2DatabaseKey unknownD;
    public R2DatabaseKey unknownE;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public UInt64 m_sensorInsertionTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBBBH', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddQ', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'padding1', 'sensor_life', 'unknownD', \
                   'unknownE', 'unknown0', 'unknown1', 'unknown2', 'unknown3', 'unknown4', 'unknown5', 'unknown6', \
                   'unknown7', 'sensor_insertion_time', 'footer')

class R2Reserved0x10Record(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte unknownA;
    public byte unknownB;
    public byte unknownC;
    public byte unknownD;
    public byte unknownE;
    public byte unknownF;
    public UInt16 m_sensorLife;
    public R2DatabaseKey unknownG;
    public R2DatabaseKey unknownH;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public UInt64 m_sensorInsertionTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBBBBBH', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddQ', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'unknownD', 'unknownE', 'unknownF', \
                   'sensor_life', 'unknownG', 'unknownH', 'unknown0', 'unknown1', 'unknown2', 'unknown3', \
                   'unknown4', 'unknown5', 'unknown6', 'unknown7', 'sensor_insertion_time', 'footer')

class R2Reserved0x12Record(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte unknownA; 
    public byte unknownB; 
    public byte unknownC; 
    public byte unknownD; 
    public byte unknownE; 
    public byte unknownF; 
    public UInt16 m_sensorLife;
    public byte unknownG;
    public byte unknownH;
    byte m_reserved1;
    byte m_reserved2;
    public R2DatabaseKey unknownI;
    public R2DatabaseKey unknownJ;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public UInt64 m_sensorInsertionTime;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBBBBBHBBBB', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddQ', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'unknownD', 'unknownE', 'unknownF', \
                   'sensor_life', 'unknownG', 'unknownH', 'reserved1', 'reserved2', 'unknownI', 'unknownJ, \
                   ''unknown0', 'unknown1', 'unknown2', 'unknown3', 'unknown4', 'unknown5', 'unknown6', 'unknown7', \
                   'sensor_insertion_time', 'footer')

class R2InternalSubRecord(BaseR2Record):
    """
    public UInt64 unknown1;
    public double unknown2;
    public double unknown3;
    """
    format      = ('Qdd',)
    variables   = ('unknown1', 'unknown2', 'unknown3')

class R2Reserved0x18Record(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte unknownA;
    public byte unknownB;
    public byte unknownC;
    public byte unknownD;
    public byte unknownE;
    public R2InternalSubRecord unknown0;
    public R2InternalSubRecord unknown1;
    public R2InternalSubRecord unknown2;
    public R2InternalSubRecord unknown3;
    public R2InternalSubRecord unknown4;
    public R2InternalSubRecord unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public UInt64 m_sensorInsertionTime;
    public R2DatabaseKey unknown8;
    public System.Double unknown9;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBBBB', R2InternalSubRecord, R2InternalSubRecord, R2InternalSubRecord, \
                   R2InternalSubRecord, R2InternalSubRecord, R2InternalSubRecord, 'ddQ', R2DatabaseKey, 'd', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'unknownD', 'unknownE', 'unknown0', 'unknown1', \
                   'unknown2', 'unknown3', 'unknown4', 'unknown5', 'unknown6', 'unknown7', 'sensor_insertion_time', \
                   'unknown8', 'unknown9', 'footer')

class R2Reserved0x02Record(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public UInt32 unknownA;
    public UInt32 unknownB;
    public UInt32 unknownC;
    public R2DatabaseKey unknownD;
    public R2DatabaseKey unknownE;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public System.Double unknown8;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'III', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddd', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'unknownD', 'unknownE', 'unknown0', \
                   'unknown1', 'unknown2', 'unknown3', 'unknown4', 'unknown5', 'unknown6', 'unknown7', \
                   'unknown8', 'footer')

class R2Reserved0x0ERecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte unknownA;
    public byte unknownB;
    public byte unknownC;
    public byte m_padding1;
    public UInt16 m_sensorLife;
    public R2DatabaseKey unknownE;
    public R2DatabaseKey unknownF;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public System.Double unknown8;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBBBH', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddd', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'padding1', 'sensor_life', 'unknownD', \
                   'unknownE', 'unknown0', 'unknown1', 'unknown2', 'unknown3', 'unknown4', 'unknown5', 'unknown6', \
                   'unknown7', 'unknown8', 'footer')

class R2Reserved0x0FRecord(BaseR2Record):
    """
    public R2RecordHeader m_header;
    public R2DatabaseKey m_key;
    public byte unknownA;
    public byte unknownB;
    public byte unknownC;
    public byte unknownD;
    public byte unknownE;
    public byte unknownF;
    public UInt16 m_sensorLife;
    public R2DatabaseKey unknownG;
    public R2DatabaseKey unknownH;
    public R2DatabaseKey unknown0;
    public R2DatabaseKey unknown1;
    public R2DatabaseKey unknown2;
    public R2DatabaseKey unknown3;
    public R2DatabaseKey unknown4;
    public R2DatabaseKey unknown5;
    public System.Double unknown6;
    public System.Double unknown7;
    public System.Double unknown8;
    public R2RecordFooter m_footer;
    """
    format      = (R2RecordHeader, R2DatabaseKey, 'BBBBBBH', R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, \
                   R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, R2DatabaseKey, 'ddd', R2RecordFooter)
    variables   = ('header', 'key', 'unknownA', 'unknownB', 'unknownC', 'unknownD', 'unknownE', 'unknownF', \
                   'sensor_life', 'unknownG', 'unknownH', 'unknown0', 'unknown1', 'unknown2', 'unknown3', \
                   'unknown4', 'unknown5', 'unknown6', 'unknown7', 'unknown8', 'footer')

class R2HWConfig(BaseR2Record):
    """
    public UInt32 m_deviceId;            // The product ID
    public UInt64 unknown0001;
    public UInt64 unknown0002;
    public UInt32 m_firmwareFlags;
    public byte unknown0003;
    public byte m_pad0;
    public UInt16 m_pad1;
    public UInt32 unknown0004;
    public UInt32 unknown0005;
    public Int32 unknown0006;
    public Int32 unknown0007;
    public UInt32 unknown0008;
    public UInt32 unknown0009;
    public Int32 unknown000A;
    public Int32 unknown000B;
    public Int32 unknown000C;
    public UInt16 unknown000D;
    public UInt16 m_reserved1;
    public UInt32 m_hardwareProductNumber;    // Like "8205"
    public byte m_hardwareProductRevision;    // Like "14"
    public Guid m_receiverInstanceId;
    /// <summary></summary>
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 35)]
    byte[] m_reserved2;
    /// <summary></summary>
    public UInt32 m_crc; //a 32 bit crc using the same algorithm as the R2 Receiver OS's crc.
    """
    format      = ('I16sI44sIB16s35sI',)
    variables   = ('device_id', 'pad1', 'firmware_flags', 'pad2', 'hw_product_number', \
                   'hw_product_revision', 'guid', 'pad3', 'crc'), \

class R2HWConfigHeader(BaseR2Record):
    """
    /// <summary>Should be DX CFG</summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
    public string m_configSignature;
    /// <summary>Size of the config image</summary>
    public UInt32 m_configSize;
    /// <summary>Rev of the config image. Should be 2.</summary>
    public UInt32 m_configVersion;
    /// <summary>A number that must match the BIOS to be compatible</summary>
    public UInt32 m_biosCompatibilityNumber;
    """
    format      = ('8sIII',)
    variables   = ('config_signature', 'config_version', 'bios_compatibility_number')

class R2HWConfigV2(BaseR2Record):
    """
    /// <summary>Should be DX CFG</summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
    public string m_configSignature;
    public UInt32 m_configSize;
    public UInt32 m_configVersion;
    public UInt32 unknown0001;
    public UInt32 unknown0002;
    public UInt32 unknown0003;
    public UInt32 unknown0004;
    public UInt32 unknown0005;
    public UInt32 unknown0006;
    public UInt32 unknown0007;
    public UInt32 unknown0008;
    public UInt32 unknown0009;
    public UInt64 unknown000A;
    public UInt64 unknown000B;
    public UInt32 m_firmwareFlags;
    /// <summary>The product / receiver ID</summary>
    public UInt32 m_deviceId;
    /// <summary>Like "8205"</summary>
    public UInt32 m_hardwareProductNumber;
    public byte unknown000C;
    /// <summary>Like "14"</summary>
    public byte m_hardwareProductRevision;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
    private byte[] m_reserved1;
    public Guid m_receiverInstanceId;

    /// <summary>
    /// This should take us to a total size of 1024
    /// </summary>
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 920)]
    public byte[] m_reserved2;

    /// <summary>A 32 bit crc using the same algorithm as the R2 Receiver OS's crc.</summary>
    public UInt32 m_crc;
    """
    format      = ('8sII52sIIIBB2s16s920sI',)
    variables   = ('config_signature', 'config_size', 'config_version', 'pad1', \
                   'firmware_flags', 'device_id', 'hw_product_number', \
                   'pad2', 'hw_product_revision', 'pad3', 'guid', 'pad4', 'crc')

class R2FWHeader(BaseR2Record):
    """
    public UInt16 m_code;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
    public string m_signature;
    public UInt32 m_revision;
    """
    format      = ('H8sI',)
    variables   = ('code', 'signature', 'revision')
    
class R2FWHeaderVer2(BaseR2Record):
    """
    public UInt16 m_code;        // two bytes --> "DX"
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
    public string m_signature;
    public UInt32 m_revision;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
    public byte[] m_reserved1;
    public UInt32 unknown0001;
    public UInt32 unknown0002;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
    public byte[] unknown0003;
    public UInt32 unknown0004;
    public UInt32 m_databaseRevision;
    public UInt32 m_firmwareHeaderRevision;
    public UInt32 unknown0005;
    public UInt32 unknown0006;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
    public byte[] m_reserved2;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
    public string m_productString;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
    public byte[] m_reserved3;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
    public string m_buildDateAndTime;
    """
    format      = ('H8sI2sII16sIIIII4s32s32s32s',)
    variables   = ('code', 'signature', 'revision', 'reserved1', 'unknown0001', \
                   'unknown0002', 'unknown0003', 'unknown0004', 'database_revision', \
                   'firmware_header_revision', 'unknown0005', 'unknown0006', 'reserved2' \
                   'product_string', 'reserved3', 'build_date_and_time')

class R2FWHeaderVer3(BaseR2Record):
    """
    public UInt16 m_code;        // two bytes --> "DX"
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
    public string m_signature;
    public UInt32 m_revision;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
    public byte[] m_reserved1;
    public UInt32 unknown0001;
    public UInt32 unknown0002;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
    public byte[] unknown0003;
    public UInt32 unknown0004;
    public UInt32 m_databaseRevision;
    public UInt32 m_firmwareHeaderRevision;
    public UInt32 unknown0005;
    public UInt32 unknown0006;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
    public byte[] m_reserved2;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
    public string m_productString;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
    public byte[] m_reserved3;
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
    public string m_buildDateAndTime;
    public UInt32 unknown0007;
    public UInt32 unknown0008;    
    """   
    format      = ('H8sI2sII16sIIIII4s32s32s32sII',)
    variables   = ('code', 'signature', 'revision', 'reserved1', 'unknown0001', \
                   'unknown0002', 'unknown0003', 'unknown0004', 'database_revision', \
                   'firmware_header_revision', 'unknown0005', 'unknown0006', 'reserved2' \
                   'product_string', 'reserved3', 'build_date_and_time', 'unknown0007', 'unknown0008') 
                   
r2_record_type_class = { \
    R2_RECORD_TYPE_LOG          : R2LogRecord, \
    R2_RECORD_TYPE_EVENT        : R2EventRecord, \
    R2_RECORD_TYPE_METER        : R2MeterRecord, \
    R2_RECORD_TYPE_METER2       : R2Meter2Record, \
    R2_RECORD_TYPE_METER3       : R2Meter2Record, \
    R2_RECORD_TYPE_SENSOR       : R2SensorRecord, \
    R2_RECORD_TYPE_SENSOR2      : R2Sensor2Record, \
    R2_RECORD_TYPE_SENSOR3      : R2Sensor2Record, \
    R2_RECORD_TYPE_SETTINGS     : R2SettingsRecord, \
    R2_RECORD_TYPE_SETTINGS2    : R2Settings2Record, \
    R2_RECORD_TYPE_SETTINGS3    : R2Settings3Record, \
    R2_RECORD_TYPE_SETTINGS4    : R2Settings4Record, \
    R2_RECORD_TYPE_SETTINGS5    : R2Settings5Record, \
    R2_RECORD_TYPE_SETTINGS6    : R2Settings6Record, \
    R2_RECORD_TYPE_RESERVED0x02 : R2Reserved0x02Record, \
    R2_RECORD_TYPE_RESERVED0x07 : R2Reserved0x07Record, \
    R2_RECORD_TYPE_RESERVED0x09 : R2Reserved0x09Record, \
    R2_RECORD_TYPE_RESERVED0x0D : R2Reserved0x0DRecord, \
    R2_RECORD_TYPE_RESERVED0x0E : R2Reserved0x0ERecord, \
    R2_RECORD_TYPE_RESERVED0x0F : R2Reserved0x0FRecord, \
    R2_RECORD_TYPE_RESERVED0x10 : R2Reserved0x10Record, \
    R2_RECORD_TYPE_RESERVED0x12 : R2Reserved0x12Record, \
    R2_RECORD_TYPE_RESERVED0x18 : R2Reserved0x18Record \
}
          
if __name__ == '__main__':

    buffer = Buffer.Buffer('1' * 1000)
    
    r1 = R2LogRecord(buffer, R2LogRecord.RecordLength())
    print type(r1.header.previous_address)
    
