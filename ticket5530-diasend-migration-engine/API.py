# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import EventLog
from ConnectorAPI import * 
import time
import Config
from Generic import *
from Defines import *
import types

# -----------------------------------------------------------------------------
# "Global" variables (only global if you import them from another module)
# -----------------------------------------------------------------------------
connection = None

endpoint = Config.settings["connector-endpoint"]

clinic = False

# -----------------------------------------------------------------------------
# Function definitions
# -----------------------------------------------------------------------------

def ToStr(val):
    """
    Checks if it is a unicode -> decode as UTF8, otherwise to normal string conversion
    """
    # If we have a unicode string, create an UT8 string
    if type(val) == types.UnicodeType:
        return val.encode("utf8")
    else:
        # just create an ASCII string
        return str(val)
    

def IsDatabaseConnected():
    """
    Is database connected?
    """
    global connection

    if connection:
        return True
    else:
        return False
        
# ------------------------------------------------------------------------------

def ConnectToDatabase():
    """
    Establish connection to database
    """
    global connection, clinic 
    connector_endpoint = endpoint
    if clinic:
        connector_endpoint = endpoint + "/" + ENDPOINT_CLINIC
    connection = ConnectorAPI(connector_endpoint)
    return True

# ------------------------------------------------------------------------------

def ReconnectToDatabase():

    """
    There are situations when we want to switch database,
    for example if we have an ongoing transfer, but
    detect that server location has changed (DDA)
    This function will close any current connection and
    reconnect to the database that is currently set
    """
    DisconnectFromDatabase()
    return ConnectToDatabase()

# ------------------------------------------------------------------------------

def GetLastInsertID():
    """
    Return the ID returned during last insert
    """
    global connection

    if not connection:
        return None
        
    return None

# ------------------------------------------------------------------------------

def DisconnectFromDatabase():
    """
    Disconnect from database.
    """
    global connection

    if connection:
        connection = None

# ------------------------------------------------------------------------------

def DoesQueryReturnResults( sqlQ, paramQ ):
    """
    Check if a sql query with a given paramQ retuns any results. Note : also returns false
    when something goes wrong; it is the responsibility of another part of the
    code to handle 'no connection' etc. This function _only_ indicates true
    if it has found a value in the database.
    """
    global connection

    # Are we conntected?
    if not connection:
        return False
    return True 

# ------------------------------------------------------------------------------

def IsValuePresentInDatabase( value, device_id ):
    """
    Check if value is already present in database. Note : also returns false
    when something goes wrong; it is the responsibility of another part of the
    code to handle 'no connection' etc. This function _only_ indicates true
    if it has found a value in the database.
    """
    # Validate if dictionary contains the values that we need to determine
    # if this is a new value or not
    return False

# ------------------------------------------------------------------------------

def InsertHistoryIntoDatabase( terminal_serial, meter_serial, entry ):
    """
    Insert log entry into history. If the device with serial number "meter_serial" 
    isn't found the history entry will be created without a device.
    """
    EventLog.EventLogInfo("InsertHistory into database %s, %s, %s" % (terminal_serial, meter_serial, entry ))
    return True

# ------------------------------------------------------------------------------
def PerformQuery( sqlQ, paramQ, retResults = False, force_commit = False ):
    """
    Performs an sql query, returns True on success, else False
    """
    global connection
    return True

def InsertFlagListIntoDatabase(flag_list, last_id):
    """
    Inserts a list of flags into a flag table
    """
    return True

def InsertValueListIntoDatabase(value_list, last_id):
    """
    Inserts a list of values into a flag table
    """
    return True

def InsertBlackBoxHistory(value, device_id, transfer_id):
    """
    Store device specific debug information (records) in the database.
    """
    if not BLACK_BOX_HISTORY_RECORD in value:
        return False
    return True

# ------------------------------------------------------------------------------
def InsertValueIntoDatabase( value ):
    """
    Insert new value into database
    """
    global connection
    
    for value_list in value[ ELEM_VALUE_LIST ]:
        connection.add_value_list(value_list[ELEM_VAL], ToStr(value_list[ELEM_VAL_TYPE]))

    flags = []
    if value.has_key( "meter_flaglist" ) and len(value[ "meter_flaglist"]) > 0:
        flags = value["meter_flaglist"]
    connection.add_value(value[ELEM_VAL], value[ELEM_TIMESTAMP], flags, ToStr(value[ELEM_VAL_TYPE]))

    return True        

# ------------------------------------------------------------------------------
def CreateDeviceSettingGroup(device_id, transfer_id):
    """
    Create a new device setting group (a group with all device settings for 
    a specific device / transfer combination).
    """
    return True

# ------------------------------------------------------------------------------
def InsertPumpSettingsIntoDatabaseNG(value):
    """
    Insert pump settings into the new structure (where all pump settings are 
    stored and not only the settings from the last transfer).
    """
    global connection
    if not connection:
        if not ConnectToDatabase():
            pass

    if not value.has_key( SETTINGS_LIST ):
        return False

    # Build query to insert all settings in one go
    for key in value[ SETTINGS_LIST ].keys():
        val = value[ SETTINGS_LIST ][key]

        # If we have a unicode string, create an UT8 string
        if type(val) == types.UnicodeType:
            val = val.encode("utf8")
        else:
            # just create an ASCII string
            val = ToStr(val)
        connection.add_device_setting(ToStr(key), val)
    

# ------------------------------------------------------------------------------
def InsertPumpProgramsInDatabaseNG( group_id, value ):
    """
    Inserts a new pump program using the new "save all pump settings" database.
    """

    if not (value.has_key(PROGRAM_TYPE) and value.has_key(PROGRAM_NAME) and value.has_key(PROGRAM_PERIODS)):
        return False

    global connection
    if not connection:
        if not ConnectToDatabase():
            pass

    # Now add all settings
    for period in value[ PROGRAM_PERIODS ]:
        t = period[0]
        ts = "%02d:%02d:%02d" % (t.hour, t.minute, t.second)

        if not connection.add_device_program(value[PROGRAM_TYPE], value[PROGRAM_NAME], ts, ToStr(period[1])):
            return False

        if len(period) > 2:
            # There are secondary values available
            for (t,v) in period[2]:
                if not connection.add_device_program_value(t, v):
                    return False

    return True

# ------------------------------------------------------------------------------


def RemovePresentElementsFromList(inList, device_id):
    """
    Remove all elements from the list that already exists in the database
    
    Returns a new filtered list
    """
    return False
    
# ------------------------------------------------------------------------------
def FindInDatabase(field, table, key, value, force_ci=False):
    """
    Checks in the database for a terminal.
    Returns the sequence number, or None
    """
    return None

def UpdateDeviceInDatabase(seq, devicetype = None, deviceclass = None):
    """
    Updates an existing device
    """
    return False

def UpdateDeviceVersionInDatabase(device_id, device_version, version_recommended = None):
    """
    Sets the version of a device
    """
    return None

def InsertTerminalInDatabase( serial ):
    """
    Creates a new terminal entry in the database
    Returns the sequence number of it, or 0.
    """

    return True

def InsertTerminalServerLocation( serial, location ):
    """
    Create a new terminal location entry in database
    """

    return True

def UpdateTerminalServerLocation( serial, location ):
    """
    Update the server location (if it has changed).
    """
    return True

def SetTerminalLocationType( serial, location ):
    """
    Set the location field of terminals table
    valid locations are {home, clinic}
    """
    return True

def SetTerminalClass( serial, terminalclass ):
    """
    Set the terminal class field for terminal.
    """
    return True

def AddTerminalPermission( serial, permission ):
    """
    Set the terminal class field for terminal.
    """
    return True
  
def InsertSoftTerminalInDatabase( serial_prefix, location ):
    """
    Create a new terminal entry in database
    This is a soft terminal, so it will have a serial
    number on the form of <SERIAL-PREFIX>-XXXXXXXXXXXX
    """
    return False

def IsTerminalRegistered( terminal_serial, location ):
    """
    Switches to the database specified by 'location' and checks
    whether the specified terminal serial is registered there
    """

    return False

def InsertTransferInDatabase( terminal_id, device_id ):
    """
    Creates a new terminal entry in the database
    Returns the sequence number of it, or 0.
    """
    return False


def InsertDeviceInDatabase( serial, obfuscated_serial, device_type = None, device_class = None):
    """
    Creates a new device entry in the database
    Returns the sequence number of it, or 0.
    """
    return False
    

def GetTerminalIdFromDatabase(header):
    """
    Check the database for a specific terminal, 
    if not found, it inserts a new terminal and returns the ID
    """    
    global connection
    if not connection:
        if not ConnectToDatabase():
            pass

    if header.isSoftwareTransmitter():
        source_type = SOFTWARE_UPLOADER
    else:
        source_type = SOFTWARE_TRANSMITTER

    return connection.get_terminal_id(header.getUserCookie(), header.getSoftwareID(), header.GetSoftwareVersion(), header.getTerminalSerial(), header.getOS(), source_type)

def GetDeviceIdFromDatabase(meter_serial, devicetype = None, device_class = None):
    """
    Retrieve device id for device with serial number. Will try both the serial number as is and the 
    encrypted variant. If no device is found None will be returned. 

    Note : device type will be updated if provided.
    """
    return None

def HasDeviceTypeConflict(device_serial, device_type):
    """
    Check if we find at least one other device with same serial number and 
    a _different_ meter type in the database (serial number collision).

    Note : Will return False (no conflict) if devicetype in db is set to 
    NULL. 
    """
    return False

def InsertListIntoDatabase( inList, device_class, allowDupsWithinTrans, cookie, is_clinic ):
    """
    Insert list of values into database.
    
    if allowDupsWithinTrans is set to True, the code first checks if elements in the list exists in the database,
     and afterwards inserts the non-existing elements to the database. Otherwhise each element is handled
     individiually.
    """
    global clinic

    if inList.has_key("header"):
        header = inList["header"]
    else:
        return (False, ERROR_CODE_DATABASE)

    clinic = is_clinic

    global connection
    ReconnectToDatabase()
    
    allValuesInserted = True

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            pass

    device_id   = None

    device_serial = header["meter_serial"]
    obfuscated_serial = None

    # If there is a device version available, store it
#    if header.has_key(ELEM_SW_VERSION):
#        version_recommended = None
#        if header.has_key(PARAM_DEVICE_VERSION_RECOMMENDED):
#            version_recommended = header[PARAM_DEVICE_VERSION_RECOMMENDED]
#        else:
#            version_recommended = None
#        if not UpdateDeviceVersionInDatabase(device_id, header[ELEM_SW_VERSION], version_recommended):
#            DisconnectFromDatabase()
#            return (False, ERROR_CODE_DATABASE)


    results = []
        
    if inList.has_key("results"):
        results = inList["results"]

    for value in results:
        if value.has_key("meter_flaglist"):
            flags = ToStr(len(value["meter_flaglist"]))
        else:
            flags = "0"
        if value.has_key( ELEM_VAL_TYPE ):
            allValuesInserted = InsertValueIntoDatabase( value )

        elif value.has_key( SETTINGS_LIST ):
            if allValuesInserted:
                # print "INSERT INTO DATABASE SERIAL %s Advanced Settings" % (header["meter_serial"])
                InsertPumpSettingsIntoDatabaseNG(value)

        elif value.has_key( PROGRAM_TYPE ):
            if allValuesInserted:
                # print "INSERT INTO DATABASE SERIAL %s Device Program %s" % ( header["meter_serial"], ToStr(value[PROGRAM_NAME]) )
                InsertPumpProgramsInDatabaseNG(None, value)
           
        # No meaning to continue if something went wrong
        # The transfer will be NAK:ed
        if not allValuesInserted:
            print "   FAILED TO INSERT: " + repr(value)
            break;
    # If user is a clinic, let API determine device_class just mark it as a device
    if clinic:
        device_class = SOURCE_TYPE_DEVICE
    if not connection.start_sync(device_serial, header["meter_type"], device_class, cookie):
        print "Cannot sync data"
        return (False, ERROR_CODE_DATABASE)

    return (allValuesInserted, ERROR_CODE_DATABASE)


# ------------------------------------------------------------------------------

def UpdateIncomingData( terminal_serial, sizeOfIncomingData ):
    """
    Update information about transfer
    """
    return True


# ------------------------------------------------------------------------------

def InsertErrorIntoDatabase( value ):
    """
    Insert error into database
    """
    errorcode = "0"
    hw_nr = "0"
    sw_nr = "0"
    bl_nr = "0"
    serial_terminal = "0"
    serial_meter = "0"
    metertype = "unknown"
    data = ""

    if value.has_key("header"):
        h = value["header"]

        if h.has_key( "terminal_hwid" ):
            hw_nr = ToStr(h["terminal_hwid"])

        if h.has_key( "terminal_swid" ):
            sw_nr = ToStr(h["terminal_swid"])

        if h.has_key( "terminal_blid" ):
            bl_nr = ToStr(h["terminal_blid"])

        if h.has_key( "terminal_serial" ):
            serial_terminal = ToStr(h["terminal_serial"])

        if h.has_key( "meter_serial" ):
            serial_meter = ToStr(h["meter_serial"])

        if h.has_key( "meter_type" ):
            metertype = ToStr(h["meter_type"])

    if value.has_key("results"):
        r = value["results"][0]

        if r.has_key( "error_code" ):
            errorcode = ToStr(r["error_code"])

        if r.has_key( "fault_data" ):
            data = ToStr(r["fault_data"])


    EventLog.EventLogError("Errorlog (errorcode,hw_nr,sw_nr,bl_nr,serial_terminal,serial_meter,metertype,data) (%s,%s,%s,%s,%s,%s,%s,%s)" % ( errorcode,hw_nr,sw_nr,bl_nr,serial_terminal,serial_meter,metertype,data))
    return True


# ------------------------------------------------------------------------------

def InsertErrorLogIntoDatabase( inList ):
    """
    Insert list of values into database.
    """
    InsertErrorIntoDatabase( inList )
    return True

# ------------------------------------------------------------------------------

def CheckSubscriptionLevel(terminal_serial, device_class):
    """
    Check if the subscription for this terminal allows data from this device type
    """    
    return True

# ------------------------------------------------------------------------------

def SelectDefaultDatabase():
    """
    Selects the default database
    """
    return None

def CheckLatestOldTransmitterVersion():
    """
    Returns the current version of the old Transmitter software
    which can be downloaded from the update site
    """
    return None

def SelectDatabase(terminal_serial):
    """
    Given a serial number of the terminal, 
    this function will set up the configuration of the database
    """
    return None

def SelectDatabaseByLocation(location):
    
    return True

def GetDeviceUsers(device_id):
    """
    For a specific device, find all users that "owns" the device
    """
    return None

def GetUserDeviceFromUsername(user):
    """
    Finds the device_user for the user
    """
    return None

def GetUserDeviceFromMobileNr(nr):
    """
    Finds the device_user for the user that has this nr as mobile nr
    """
    return None 
    
def InsertComment(pcode, comment, timestamp):
    """
    Inserts a comment in the database for a given user
    """
    return False
    
def InsertDeviceComment(device_id, comment, timestamp):
    """
    Inserts a comment to all users owning the device
    """
    return False

def GetTerminalDefaultUnit(terminal_serial):
    """
    Checks which unit the customer that has this terminal uses (mg/dl or mmol/l)
    """
    unit = "mmol/l"
    return unit

def IsTerminalBlacklisted(terminal_serial):
    """
    Checks if the terminal has the blacklist property set
    """
    return False

def GetDBVersion():
    """
    Check the version of the current database layout
    """
    return "R1r"
        
# ------------------------------------------------------------------------------
# Functions, used by functionality required by the software transmitter
# They are user-centered and knows things about the internal storage, so they differ from other Backend functions

def HasUserFileUploadAccess(user):
    """
    Checks if a user has fileupload access (srpa)
    """
    return False

def HasUserFileUploadAccessByClinic(user):
    """
    Checks if a user has fileupload access by clinic.
    """
    return False

def IsUserExpired(user):
    """
    Checks if all owners of this device are expired
    """
    return False

def IsUserActive(user):
    """
    Checks if all owners of this device are archived
    """
    return True

def HasClinicDDAAccess(terminal_id, admin):
    """
    Checks if clinic has allow_dda property
    """
    return True
    
def SetFileUploadAccess(pcode):
    """
    Sets fileupload access as SRPA for the specified used
    """
    return True
    
def CreateUser(pcode, password, firstname, lastname):
    """
    Create a user with the specified properties.
    """
    return False

def RegisterTerminalOnClinic( terminal_id, clinic ):
    """
    Register a clinc (admin) for this terminal
    """
    return True

def GetTerminalAdmin( terminal_id ):
    """
    Checks if terminal is owned by a clinic
    """
    return None
   
def IsTerminalRegisteredOnClinic( terminal_id ):
    return False
 
def RegisterDeviceOnUser(pcode, device_id, deviceclass):
    """
    Registers the device on the specific user.
    @TODO: Fill the "extra" parameter, and check the number of devices on each class
    """
    return True

def DeviceRegisteredOnUser(pcode, device_id):
    """
    Is this device registered on this pcode?
    """
    return True

def UsersRegisteredAsOwnerOfDevice(device_id):
    """
    Return of list of users (pcodes) that is registered
    as owners of the device.
    """
    return []

def UnregisterDeviceOnUser(username, device_id, device_class):
    """
    Unregister device from user.
    """
    return

def NumberOfRegisteredDevicesOnUser(pcode, device_class):
    """
    Return number of registered devices of class device_class on 
    user pcode. 
    """
    return 0

def PumpIsPrimaryForNumberOfUsers(device_id):
    """
    Return number of users who has this pump as a primary device. 
    """
    return 0

def PumpIsPrimaryForUser(pcode, device_id):
    """
    Is this pump a primary pump for this user?
    """
    return False

def GetClinicOfClinicUser(username):
    """
    Return which clinic a clinic user belongs to
    """
    return None

def ClinicRequiresEncryptedDeviceSerial(terminal_id, admin, device_class):
    """
    Check if the clinic requires encrypted serial numbers (for devices). 
    """
    return False

def UserRequiresEncryptedDeviceSerial(user, device_class):
    """
    Checks if a user (patient) requires encrypted serial numbers for devices.
    """
    return False
    
def HasClinicUnitedSupport(clinic):
    """
    Checks if a clinic has United package support.
    """
    return False

def HasPatientAnimasSupport(pcode):
    """
    Checks if a clinic has Animas package support.
    """
    return False

    
def IsClinicBelongingToAnimasMiddleAdmins(clinic):
    """
    Checks if the specified clinic belongs to the configured Animas Middle admins
    """
    return False
    
def IsDeviceCoveredByRule(device_type, rule):
    """
    Checks if a device is covered by a specified rule
    """
    return False

def EndSyncIntoDatabase(operating_system, operating_system_version, software_version, terminal_serial, is_software_transmitter, cookie):
    global connection

    if not connection:
        if not ConnectToDatabase():
            pass
    if is_software_transmitter:
        application_type = SOFTWARE_UPLOADER
    else: 
        application_type = SOFTWARE_TRANSMITTER
    connection.end_sync(operating_system, operating_system_version, software_version, terminal_serial, application_type, cookie)

if __name__ == "__main__":
    if not connection:
        if not ConnectToDatabase():
            print "failed"
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            pass

    DisconnectFromDatabase()
