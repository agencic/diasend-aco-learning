#!/bin/bash

cd crypt
make 
cp *.so ..
cd -

cd crc
make
cp *.so ..
cd -

cd test/encrypt
make
cp *.so ..
cd -

cd ssl-keys
./generate-keys.sh
cd -

