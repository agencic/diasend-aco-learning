# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2012 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Roche Accu-Chek Mobile U1 (mass storage device)

import re
from Generic import *
from Defines import *
from datetime import datetime


# -----------------------------------------------------------------------------
# CRC calculation. Perform CRC calculation on UTF-8 encoded string
# -----------------------------------------------------------------------------
# The following table from the javascript code seems to be a CRC-CCITT table 
# based on polynomial x^0 + x^5 + x^12 ( + x^16)
crc_lcm_table = (0,4489,8978,12955,17956,22445,25910,29887,35912,40385,44890,48851,51820,56293,
59774,63735,4225,264,13203,8730,22181,18220,30135,25662,40137,36160,49115,44626,56045,52068,
63999,59510,8450,12427,528,5017,26406,30383,17460,21949,44362,48323,36440,40913,60270,64231,
51324,55797,12675,8202,4753,792,30631,26158,21685,17724,48587,44098,40665,36688,64495,60006,
55549,51572,16900,21389,24854,28831,1056,5545,10034,14011,52812,57285,60766,64727,34920,39393,
43898,47859,21125,17164,29079,24606,5281,1320,14259,9786,57037,53060,64991,60502,39145,35168,
48123,43634,25350,29327,16404,20893,9506,13483,1584,6073,61262,65223,52316,56789,43370,47331,
35448,39921,29575,25102,20629,16668,13731,9258,5809,1848,65487,60998,56541,52564,47595,43106,
39673,35696,33800,38273,42778,46739,49708,54181,57662,61623,2112,6601,11090,15067,20068,24557,
28022,31999,38025,34048,47003,42514,53933,49956,61887,57398,6337,2376,15315,10842,24293,20332,
32247,27774,42250,46211,34328,38801,58158,62119,49212,53685,10562,14539,2640,7129,28518,32495,
19572,24061,46475,41986,38553,34576,62383,57894,53437,49460,14787,10314,6865,2904,32743,28270,
23797,19836,50700,55173,58654,62615,32808,37281,41786,45747,19012,23501,26966,30943,3168,7657,
12146,16123,54925,50948,62879,58390,37033,33056,46011,41522,23237,19276,31191,26718,7393,3432,
16371,11898,59150,63111,50204,54677,41258,45219,33336,37809,27462,31439,18516,23005,11618,15595,
3696,8185,63375,58886,54429,50452,45483,40994,37561,33584,31687,27214,22741,18780,15843,11370,
7921,3960)
                        
# This function should really use crcmod, but was not able to reproduce the same results as 
# the following code and table which is ported to python from the javascript code 
def crc_lcm(in_str):                
    crc = 0xFFFF #initial value

    for ch in in_str:
       crc = (crc >> 8) ^ crc_lcm_table[(crc & 0xFF ^ ord(ch))]
    return ~crc & 0xffff 


# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalHeaderRecord( line ):
    """
    Evaluate a Accu-chek Mobile U1 (LCM) heading/settings record. Extract groups :
    group 1 : Year
    group 2 : Month
    group 3 : Day of month
    group 4 : Serial number
    group 5 : Checksum
    
    Example from a Accu-chek Mobile (LCM)
    [7,2,1,1,0,2012,4,17,140,70,140,0,70,'U100098746',1,1287612,0,1010010010,60944]
    """
    
    # Accu-chek LCM
    m = re.match( r'var settings=\[\d+,\d+,\d+,\d+,\d+,(\d+),(\d+),(\d+),\d+,\d+,\d+,\d+,\d+,\'(\w+)\',\d+,\d+,\d+,\d+,(\d+)\];.*', line, re.IGNORECASE )

    return m

def EvalResultRecord( line ):
    """
    Evaluate a Accu-chek Mobile U1 LCM result record. Extract groups :
    group 1 : value
    group 2 : flags
    group 3 : year
    group 4 : month
    group 5 : day
    group 6 : hour
    group 7 : minute
    group 8 : second
    group 9 : checksum    
    
    Example from Accu-Chek Mobile U1 (LCM)
    [536,0,'2012/04/12','09:43:00',54766]
        536 => BG in md/dl (the exact conversion factor / algorithm is unknown - but it is close to 18,018)
        0 => Flags
        '2012/04/12','09:43:00' => Timestamp
        54766 => Checksum - algorithm TBD  
    """         

    # Accu-chek Mobile U1 LCM
    m = re.match ( r'\[(\d+),(\d+),\'(\d+)/(\d+)/(\d+)\',\'(\d+):(\d+):(\d+)\',(\d+)\][\],;]', line, re.IGNORECASE )
    
    
    return m
    


# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAccuchekMobileLcmSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        res[ ELEM_DEVICE_SERIAL ] = m.group(4)
    return res


def EvalAccuchekMobileLcmDeviceModelRecord( line ):
    """
    Is this line a model name record. If so, return a dictionary with the device model name.
    """
    
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        res[ ELEM_DEVICE_MODEL ] = "Accu-Chek Mobile U1"
    return res


def EvalAccuchekMobileLcmUnitRecord( line ):
    """
    Just set mg/dL until protocol spec has arrived.
    """
    
    res = {ELEM_DEVICE_UNIT : "mg/dl"}

    return res
    
def EvalAccuchekMobileLcmResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """   
    res = {}
    m = EvalResultRecord( line)
    
    
    if m:
        # Convert date (is in YYYYMMDDTTTT)
        try:
            res[ ELEM_TIMESTAMP ] = datetime(int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)), int(m.group(7)))
        except:
            res[ ELEM_TIMESTAMP ] = None


        # Convert value to float
        try:
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
            res[ ELEM_VAL ] = float( m.group(1) ) * 18.0 / VAL_FACTOR_CONV_MMOL_TO_MGDL
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line }
       
        # Flags
        flags = []
        
        sum_flags = int(m.group(2))
        
        if (sum_flags & 1) == 1:
            flags.append(FLAG_BEFORE_MEAL)

        if (sum_flags & 2) == 2:
            flags.append(FLAG_AFTER_MEAL)
        
        if (sum_flags & 4) == 4:
            flags.append(FLAG_OTHER)

        if (sum_flags & 8) == 8:
            flags.append(FLAG_RESULT_CTRL)
                                  
        # Temperature is outside +10 .. +40 deg C.
        if (sum_flags & 16) == 16:
            flags.append(FLAG_RESULT_OUTSIDE_TEMP)
            
        # Lo/Hi values are coded as 1 / 999
        if int(m.group(1)) == 1:
            flags.append(FLAG_RESULT_LOW)
        elif int(m.group(1)) == 999:
            flags.append(FLAG_RESULT_HIGH)
        
        res [ELEM_FLAG_LIST] = flags

    return res
    
def EvalAccuchekMobileLcmChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    checkSum = 0

    m = EvalResultRecord( line )
    if m:
        try:                      
            checksumStr = m.group(1) + m.group(2) + m.group(3) + "/" + m.group(4) + "/" + m.group(5) + m.group(6) + ":" + m.group(7) + ":" + m.group(8)
            checkSumCalc = crc_lcm(checksumStr)
            
            if checkSumCalc == int(m.group(9)):
                return True
            else:
                return False
        except ValueError:
            return False        
    else:
        return False

    return True
    

    
# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectAccuchekMobileLcm( inList ):
    """
    Detect if data comes from a Accu-chek Mobile LCM USB.
    """
    return DetectDevice( 'AccuchekLcm', inList, DEVICE_METER )

def AnalyseAccuchekMobileLcm( inData ):
    """
    Analyse Accu-chek Mobile LCM USB
    """

    inList = inData.split('\n')

    # Empty dictionary
    d = {}
    
    d[ "eval_serial_record" ]       = EvalAccuchekMobileLcmSerialRecord
    d[ "eval_device_model_record" ] = EvalAccuchekMobileLcmDeviceModelRecord
    d[ "eval_unit"]                 = EvalAccuchekMobileLcmUnitRecord
    d[ "eval_result_record" ]       = EvalAccuchekMobileLcmResultRecord
    d[ "eval_checksum_record" ]     = EvalAccuchekMobileLcmChecksumRecord

    
    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
                                       
    #testfile = open('test/testcases/test_data/AccuChekLcm/AccuChekLcm-U100098746.bin')
    #testfile = open('test/testcases/test_data/AccuChekLcm/AccuChekLcm-U100098739-six_values_with_flags.bin') 
    testfile = open('test/testcases/test_data/AccuChekLcm/AccuChekLcm-with-HI.bin') 
    testcase = testfile.read()
    testfile.close()
    print AnalyseAccuchekMobileLcm(testcase)

