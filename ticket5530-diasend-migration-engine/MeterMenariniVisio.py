# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Menarini GlucoMen Visio

import re
from Generic import *
from Defines import *
from datetime import datetime
import Common

# -----------------------------------------------------------------------------
# DEFINES OF STATUS BITS
# -----------------------------------------------------------------------------
RESULT_WORKING_HIGH = 0x00 # Result between the result_low_limit and the result_high_limit, taken when the temperature falls within the higher working region. 
RESULT_WORKING_LOW  = 0x01 # Result between the result_low_limit and the result_high_limit, taken when the temperature falls within the lower working region. 
RESULT_NORMAL       = 0x02 # Result between the result_low_limit and the result_high_limit.
RESULT_HIGH         = 0x03 # Result greater than the result_high_limit.
RESULT_LOW          = 0x04 # Result less than the result_low_limit
RESULT_OUTSIDE_TEMP = 0x10 # Result outside the normal temperature range but within the lockout limits.
RESULT_ERRONEOUS    = 0x20 # Result marked by the user as being "Erroneous".
RESULT_CTRL         = 0x40 # Result marked by the user as being that for a "Control" solution.
RESULT_AVERAGE      = 0x80 # + Used to determine the number of results used to calculate an average, ( e.g. if 10 results were averaged the status would be 128 + 10 = 138 ).


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------
def SplitIndata(indata):
    """
    Splits incoming data into a list. The data is a mixture of ascii,
    (the header and first line of data) and binary result records.
    The result records are just fixed length 9 octet long binary chunks
    """
    
    inList = []
    
    # Next 13 bytes is the header, but skip the line feed and S
    inList.append(indata[2:13])

    _body = indata[13:]

    # Sanity check, this is the start of the results
    if _body[0:3] != '\r\rS' or _body[-1] != '\r':
        return []

    # Remove the chars infront and the trailing line feed
    _body = _body[3:-1]

    # The results have a fixed length of 13
    inList += Common.SplitCount(_body, 13)
    
    return inList

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalHeaderRecord( line ):
    """
    Evaluate a Menarini Visio header record. Extracted groups : 
    serial_number
    n_results
    result_high_limit
    result_low_limit
    measurements_unit
    csum
    """
    if len(line) != 11:
        return None
    
    # there is a risk that header records are 11 bytes long, 
    # so check that the line does not start with !HDR!
    if line.startswith("!HDR!"):
        return None
    
    _m = {}
    _m["serial_number"]     = ord(line[0]) + (ord(line[1]) << 8) + (ord(line[2]) << 16) + (ord(line[3]) << 24)
    _m["n_results"]         = ord(line[4]) + (ord(line[5]) << 8)
    _m["result_high_limit"] = ord(line[6]) + (ord(line[7]) << 8)
    _m["result_low_limit"]  = ord(line[8])
    _m["measurements_unit"] = ord(line[9])
    _m["csum"]              = ord(line[10])

    return _m

def EvalResultRecord( line ):
    """
    Evaluate a Menarini Visio binary represented Result record. Extracted value : 
    "result"
    "hours"
    "minutes"
    "day"
    "month"
    "year"
    "temperature"
    "status"
    "csum"
    """

    # The length of the result records are always 13 bytes
    if len(line) != 13:
        return None
        
    # there is a risk that header records are 9 bytes long, 
    # so check that the line does not start with !HDR!
    if line.startswith("!HDR!"):
        return None
    
    _m = {}
    
    _m["result"]     = ord(line[0]) + (ord(line[1]) << 8)
    _m["hours"]      = ord(line[2])
    _m["minutes"]    = ord(line[3])
    _m["day"]        = ord(line[4])
    _m["month"]      = ord(line[5])
    _m["year"]       = ord(line[6])
    _m["temperature"]= line[7:11]
    _m["status"]     = ord(line[11])
    _m["csum"]       = ord(line[12])
    
    return _m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalMenariniVisioSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        res[ "meter_serial" ] = "%d" % m["serial_number"]
    return res

def EvalMenariniVisioUnitRecord( line ):
    """
    Is this line a unit record? If so, return a dictionary with unit.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        if m["measurements_unit"] == 1:
            res[ "meter_unit" ] = "mg/dL"
        else:
            res[ "meter_unit" ] = "mmol/L"
        
    return res

def EvalMenariniVisioResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    res = {}
    
    rec = EvalResultRecord( line )
    
    if not rec:
        return res
    
    try:
        res[ ELEM_TIMESTAMP ] = datetime(2000 + rec["year"], rec["month"], rec["day"], rec["hours"], rec["minutes"] )
    except:
        res[ ELEM_TIMESTAMP ] = None
    
    res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
    res[ ELEM_VAL ] = float(rec["result"])

    _flags = []

    # First check lower 4 bits
    if rec["status"] & 0xf   == RESULT_WORKING_HIGH:
        _flags.append(FLAG_RESULT_WORKING_HIGH)
    elif rec["status"] & 0xf == RESULT_WORKING_LOW:
        _flags.append(FLAG_RESULT_WORKING_LOW)
    elif rec["status"] & 0xf == RESULT_HIGH:
        _flags.append(FLAG_RESULT_HIGH)
    elif rec["status"] & 0xf == RESULT_LOW:
        _flags.append(FLAG_RESULT_LOW)

    # Now check upper bits
    if rec["status"] & RESULT_OUTSIDE_TEMP:
        _flags.append(FLAG_RESULT_OUTSIDE_TEMP)
    if rec["status"] & RESULT_ERRONEOUS:
        _flags.append(FLAG_RESULT_ERRONEOUS)
    if rec["status"] & RESULT_CTRL:
        _flags.append(FLAG_RESULT_CTRL)
    if rec["status"] & RESULT_AVERAGE:
        _flags.append(FLAG_RESULT_AVERAGE)

    res["meter_flaglist"] = _flags

    return res

def EvalMenariniVisioNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalHeaderRecord( line )
    if m:
        try:
            res[ "meter_nr_results" ] = m["n_results"]
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m["n_results"] }
    return res

def EvalMenariniVisioChecksumRecord( line, record ):
    """
    Evaluate checksum of result record. 
    """
    _csum = 0x55
    
    for i in range(len(line) - 1):
        _csum += ord(line[i])
        _csum %= 256

    return _csum == ord(line[-1])

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMenariniVisio( inList ):
    """
    Detect if data comes from a Menarini Visio.
    """
    return DetectDevice( 'MenariniVisio', inList, DEVICE_METER )

def AnalyseMenariniVisio( inData ):
    """
    Analyse Menarini Visio
    """

    inList = SplitIndata(inData)
    
    # Empty dictionary
    d = {}

    d[ "meter_type" ]               = "GlucoMen Visio"
    d[ "eval_serial_record" ]       = EvalMenariniVisioSerialRecord
    d[ "eval_unit"]                 = EvalMenariniVisioUnitRecord
    d[ "eval_result_record" ]       = EvalMenariniVisioResultRecord
    d[ "eval_checksum_record" ]     = EvalMenariniVisioChecksumRecord
    d[ "eval_nr_results" ]          = EvalMenariniVisioNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    _body= "\x0D\x53\x1B\x27\x69\x02\x0A\x00\x58\x02\x14\x01\x7B\x0D\x0D\x53\x4A\x00\x0A\x18\x07\x03\x00\xE0\xBE\xCC\x41\x42\xB8\x4A\x00\x0A\x18\x07\x03\x00\x10\x03\xD1\x41\x42\x32\x4A\x00\x0A\x18\x07\x03\x00\xB0\xFB\xD2\x41\x42\xCB\x4A\x00\x0A\x18\x07\x03\x00\x60\x1F\xD4\x41\x42\xA1\xEF\x00\x0A\x18\x07\x03\x00\xA0\x19\xD4\x41\x42\x80\xEF\x00\x0A\x18\x07\x03\x00\x30\x64\xD4\x41\x42\x5B\xEF\x00\x0A\x18\x07\x03\x00\x40\x4D\xD4\x41\x42\x54\xEF\x00\x0A\x18\x07\x03\x00\x40\x4D\xD4\x41\x42\x54\xEF\x00\x0A\x18\x07\x03\x00\x00\x81\xD4\x41\x42\x48\xC2\x00\x09\x2C\x1E\x01\x07\xF0\x60\xAF\x41\x02\xB4\x0D"
    _ret = AnalyseMenariniVisio(_body)
    
    print _ret
