# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Mendor Discreet
# @DEVICE alphacheck professional
# @DEVICE i-SENS CareSens N
# @DEVICE i-SENS CareSens N 500
# @DEVICE i-SENS CareSens N 1000
# @DEVICE i-SENS CareSens N POP 500
# @DEVICE i-SENS CareSens N POP 1000
# @DEVICE i-SENS COOL/Bravo
# @DEVICE i-SENS CareSens N Voice

import re
from Generic import *
from Defines import *
from struct import unpack, pack
import time
import datetime

DISCREET_GLUCOSE_RECORD_SIZE = 8

class Record(object):
    def __init__(self):
        pass

    def is_serial(self):
        return False

    def is_model(self):
        return False

    def is_number_of_results(self):
        return False

    def is_result(self):
        return False

class RecordSerialAndModel(Record):

    # Regular expressions for trying to determine which 
    # serial number system the memory dump from 0x1034. 
    sn0_regex = re.compile(r'[A-Z].{7}', re.DOTALL)
    sn1_regex = re.compile(r'.{4}[\000-\021]\000[\000-\031]\000', re.DOTALL)
    sn2_regex = re.compile(r'[A-Z0-9]{2}[A-Z][\001-\004].[\000-\001][A-Z]\001', re.DOTALL)

    map_model_code_to_device_sn_1 = {
        #0x00:['CSP', 'CareSens I', 251], 
        #0x01:['ISP', 'CareSens II', 251], 
        #0x02:['GSP', 'Maxima ', 251], 
        0x03 :['CNP', 'i-SENS CareSens N', 251], 
        #0x04:['NCP', 'NoCoding 1', 251], 
        #0x05:['HDP', 'Handok ', 251], 
        #0x06:['ACP', 'Acura', 251], 
        0x07:['NTP', 'i-SENS COOL/Bravo', 251], 
        #0x08:['ECP', 'e-CheKer', 251], 
        #0x09:['AQP', 'Acura', 251], 
        #0x0a:['ITP', 'ISOTECH', 251], 
        #0x0b:['CNM', 'CareSens N upgrade', 251], 
        0x0c :['APP', 'alphacheck professional', 501], 
        #0x0d:['CLP', 'CareSens LINK', 501], 
        #0x0e:['DAP', 'AUTO CHEK', 251], 
        0x0f :['MDP', 'Mendor Discreet', 251], 
        0x10 :['CMP', 'i-SENS CareSens N 500', 501], 
        0x11:['CVP', 'CareSens N Voice', 501]
        }

    map_model_code_to_device_sn_2 = {
        'A0':'CareSens N POP 500',
        'A1':'CareSens N POP 1000',
        'A5':'i-SENS CareSens N 500',
        'A6':'i-SENS CareSens N 1000',
        'A9':'i-SENS CareSens N 1000'
    }

    def __init__(self):
        self.value_1034 = None
        self.value_103c = None
        self.serial_dictionary = {}
        self.model_dictionary = {}
        self.max_number_records = 0

    def is_serial(self):
        return True

    def is_model(self):
        return True

    def set_1034(self, data):
        self.value_1034 = data
        self.build_serial_and_model_dictionary()

    def set_103c(self, data):
        self.value_103c = data
        self.build_serial_and_model_dictionary()

    def determine_numbering_system(self):
        """
        Determine if this meter uses the SN-0, SN-1 or SN-2
        numbering systems. SN-0 is 'invented' by us as a definition
        of the Mendor numbering system. 
        """

        if self.value_1034 == None and self.value_103c == None:
            return -1

        # If only data from 0x1034 exists this can't be a Mendor Discreet. 
        # Just look at the 0x103b flag. Note : we do not support SN-2 here
        # because 0x103c is needed later in the process (for number of 
        # supported records).
        if self.value_1034 != None and self.value_103c == None:
            a_1034 = map(ord, self.value_1034)
            return 1 if a_1034[7] == 0x00 else -1

        else:

            a_1034 = map(ord, self.value_1034)
            a_103c = map(ord, self.value_103c)

            # If 0x103c contains 0x1100 this _can_ be a Mendor Discreet, if not 
            # use the 0x103b flag.
            # If 0x103c contains 0x1100 but 0x103b is not one of the supported
            # flag values we assume we do have a Mendor Discreet. 
            if a_1034[7] == 0x00 and not ([a_103c[0], a_103c[1]] == [0x11, 0x00]):
                return 1
            if a_1034[7] == 0x01 and not ([a_103c[0], a_103c[1]] == [0x11, 0x00]):
                return 2
            if a_1034[7] not in [0x00, 0x01] and ([a_103c[0], a_103c[1]] == [0x11, 0x00]):
                return 0

            # This is the tough one. 0x103b (=a_1034[7]) may indicate SN-1 or SN-2, but 
            # 0x103c indicates SN-0. Let's use the re-fallback. 
            if RecordSerialAndModel.sn2_regex.match(self.value_1034):
                return 2
            if RecordSerialAndModel.sn1_regex.match(self.value_1034):
                return 1
            if RecordSerialAndModel.sn0_regex.match(self.value_1034):
                return 0

        return -1

    def build_serial_and_model_dictionary(self):

        numbering_system = self.determine_numbering_system()

        if numbering_system == 0:
            serial0 = self.value_1034[0]
            serial1 = unpack("<H", self.value_1034[2:4])[0]
            serial2 = unpack("<H", self.value_1034[4:6])[0]
            serial3 = unpack("<H", self.value_1034[6:8])[0]
            self.serial_dictionary  = {ELEM_DEVICE_SERIAL:'%c%04d%04d%04d' % (serial0, serial1, serial2, serial3)}

            # Menarini had relabeled the Mendor Discreet but there is a serial
            # number system we can use to determine model:            
            # For GlucoMen READY, SN is made as below:
            # MWWYYPPVVXXXX
            # where PP digits are for product:
            # in case of READY PP digits are 10 (mmol/L) or 11 (mg/dL).
            # in case of Mendor Discreet PP digits are 02, 03, 04, 05, 07.
            pp = self.serial_dictionary[ELEM_DEVICE_SERIAL][5:7]
            if pp == "10" or pp == "11":
                self.model_dictionary   = {ELEM_DEVICE_MODEL:'GlucoMen READY'}
            else:
                self.model_dictionary   = {ELEM_DEVICE_MODEL:'Mendor Discreet'}
            self.max_number_records = 501
            print '%c%04d%04d%04d' % (serial0, serial1, serial2, serial3)

        elif numbering_system == 1:
            product_count         = unpack("<H", self.value_1034[0:2])[0]
            mass_production_count = ord(self.value_1034[2])
            model_code            = ord(self.value_1034[4])
            program_version       = chr(ord(self.value_1034[6])+ord('A'))
            if model_code in RecordSerialAndModel.map_model_code_to_device_sn_1:
                model_code_abbrev = RecordSerialAndModel.map_model_code_to_device_sn_1[model_code][0]
                self.serial_dictionary = {ELEM_DEVICE_SERIAL:'%s%02d%c%05d' % (model_code_abbrev, mass_production_count, 
                        program_version, product_count)}
                self.model_dictionary = {ELEM_DEVICE_MODEL:RecordSerialAndModel.map_model_code_to_device_sn_1[model_code][1]}
                self.max_number_records = RecordSerialAndModel.map_model_code_to_device_sn_1[model_code][2]

        elif numbering_system == 2:
            model_code      = self.value_1034[:2]
            factory_code    = self.value_1034[2]
            production_date = unpack('<H', self.value_1034[4:6])[0]
            production_year = self.value_1034[6]
            product_count   = unpack('<H', self.value_103c)[0]
            self.serial_dictionary = {ELEM_DEVICE_SERIAL:'%s%s%03d%s%05d' % (model_code, factory_code, 
                    production_date, production_year, product_count)}
            if model_code in RecordSerialAndModel.map_model_code_to_device_sn_2:
                self.model_dictionary = {ELEM_DEVICE_MODEL:RecordSerialAndModel.map_model_code_to_device_sn_2[model_code]}
            self.max_number_records = ord(self.value_1034[3]) * 250 + 1

    def get_serial_dictionary(self):
        return self.serial_dictionary

    def get_model_dictionary(self):
        return self.model_dictionary

    def get_model(self):
        if ELEM_DEVICE_MODEL in  self.model_dictionary:
            return self.model_dictionary[ELEM_DEVICE_MODEL]
        return ''

    def get_max_number_of_records(self):
        return self.max_number_records

class RecordNumberOfResults(Record):

    def __init__(self, data):
        self.dictionary = {ELEM_NR_RESULTS:unpack("<H", data)[0]}

    def is_number_of_results(self):
        return True

    def get_dictionary(self):
        return self.dictionary

class RecordResult(Record):

    def __init__(self, data, record_serial_and_model):
        
        self.dictionary = {}

        # When the memory is full, a final 0xFF block is stored 
        if data == (chr(0xff)*DISCREET_GLUCOSE_RECORD_SIZE):
            # Invalid or empty result records are still counted in the 
            # number of results.
            self.dictionary = {'null_result':True}
            return

        # - Decode the fields -
        
        # Date/time
        try:    
            date = datetime.datetime(2000+ord(data[0]), ord(data[1]), 
                ord(data[2]), ord(data[3]), ord(data[4]), ord(data[5]))
        except:
            # CareSens devices can actually store invalid date/times (such as 2014-02-29). We need to ignore those records.
            self.dictionary = {'null_result':True}
            return
            
        # Glucose value
        glucose_value = unpack("<H", data[6:8])[0]
        
        # set flags
        flags = []

        if glucose_value >= 20000:
            glucose_value -= 20000
            flags.append(FLAG_RESULT_CTRL)

        if glucose_value >= 10000:
            glucose_value -= 10000
            if record_serial_and_model.get_model() in ['Mendor Discreet', 'GlucoMen READY']:
                flags.append(FLAG_GENERAL)
            else:
                flags.append(FLAG_AFTER_MEAL)

        # To prevent future mayhem (e.g. if the next version wants to store a flag
        # by adding 30000 to the glucose value) we assume that the actual glucose
        # value is 1000 mg/dl or lower.  
        glucose_value = glucose_value % 1000

        if glucose_value > 600:
            flags.append(FLAG_RESULT_HIGH)
        elif glucose_value < 20:
            flags.append(FLAG_RESULT_LOW)

        self.dictionary = {}
        self.dictionary[ELEM_TIMESTAMP] = date
        self.dictionary[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
        # Adjust the factor according to the (false) factor in Generic. @TODO: Change this when Generic changes!
        self.dictionary[ELEM_VAL] = glucose_value * 18.0/VAL_FACTOR_CONV_MMOL_TO_MGDL
        self.dictionary[ELEM_FLAG_LIST] = flags

    def is_result(self):
        return True

    def get_dictionary(self):
        return self.dictionary

def SplitData( inData ):
    """
    Splits incoming data into a list. 
    
    The data contains of binary records prefixed by "!TAG!<descr>!"
    The last item is the records, which is a block of record chunks with fixed size
    """

    records = []

    record_serial_and_model = RecordSerialAndModel()

    # Tag !TAG!SERNO! is a memory dump at 0x1034, 8 bytes long.    
    match = re.compile(r'.*!TAG!SERNO!(.{8,8}).*', re.DOTALL).match(inData)
    if match:    
        record_serial_and_model.set_1034(match.group(1))

    # Tag !TAG!MODEL! is a memory dump at 0x103c, 2 bytes long.    
    match = re.compile(r'.*!TAG!MODEL!(.{2,2}).*', re.DOTALL).match(inData)
    if match:
        record_serial_and_model.set_103c(match.group(1))

    # The data from 0x1034 and 0x103c is combined into one class. 
    # Both are necessary to try to determine model and serial number. 
    records.append(record_serial_and_model)

    number_of_records_in_results = 0
    # Tag !TAG!RESULTS! are the results at 0xe200. This tag must be last
    # in the data dump. 
    match = re.compile(r'.*!TAG!RESULTS!(.*)', re.DOTALL).match(inData)
    if match:
        # All results are 8 bytes long. 
        if (len(match.group(1)) % 8) == 0:
        
            # Only allow extraction of the maximum number of records. This will 
            # trigger a number of error fault in generic if the device contains
            # more values than expected (which is intended).
            number_of_records_in_results = len(match.group(1)) / 8
            number_of_records_to_extract = min(record_serial_and_model.get_max_number_of_records(), 
                number_of_records_in_results)
            bytes_to_request = number_of_records_to_extract * 8

            # Split result block into individual records.
            records.extend([RecordResult(match.group(1)[i:i+8], record_serial_and_model) for i in range(0, bytes_to_request, 8)])

    # Tag !TAG!NORESULTS!! is a memory dump at 0x1086, 2 bytes long.    
    match = re.compile(r'.*!TAG!NORESULTS!(.{2,2}).*', re.DOTALL).match(inData)
    if match:
        records.append(RecordNumberOfResults(match.group(1)))
    else:
        # Some devices don't have the ability to report number of results. In that case the 
        # number of results tag is missing and we recreate the number of records by 
        # looking at the actual number of records. Note : this may trigger a number of
        # results error in generic if the device contains more records than expected.
        records.append(RecordNumberOfResults(pack('<H', number_of_records_in_results)))

    return records

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalDiscreetAndCareSensSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    if line.is_serial():
        return line.get_serial_dictionary()
    return {}

def EvalDiscreetAndCareSensModelRecord( line ):
    """
    Detect the model by looking at the serial record
    In some cases, the exact model will not be known - and will be detected when the serial number is decoded
    """
    if line.is_model():
        return line.get_model_dictionary()
    return {}

def EvalDiscreetAndCareSensUnitRecord( line ):
    """
    Always return mg/dl 
    """
    return {ELEM_DEVICE_UNIT : "mg/dl"}

def EvalDiscreetAndCareSensResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    ELEM_TIMESTAMP  > date in datetime object
    ELEM_VAL        > value (float) 
    ELEM_VAL_TYPE   > type of value (VALUE_TYPE_GLUCOSE)
    ELEM_FLAG_LIST > list of flags (int) if present
    """
    if line.is_result():
        return line.get_dictionary()
    return {}

def EvalDiscreetAndCareSensNoResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    if line.is_number_of_results():
        return line.get_dictionary()
    return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectDiscreetAndCareSens( inList ):
    """
    Detect if data comes from a Mendor Discreet or i-SENS CareSens
    """
    return DetectDevice( 'Discreet and CareSens', inList, DEVICE_METER );

def AnalyseDiscreetAndCareSens( inData ):
    """
    Analyse Mendor Discreet / iSense Caresens
    """
    
    # Empty dictionary
    d = {}

    d["eval_serial_record"]       = EvalDiscreetAndCareSensSerialRecord
    d["eval_device_model_record"] = EvalDiscreetAndCareSensModelRecord
    d["eval_unit"]                = EvalDiscreetAndCareSensUnitRecord
    d["eval_result_record"]       = EvalDiscreetAndCareSensResultRecord
    d["eval_nr_results"]          = EvalDiscreetAndCareSensNoResultsRecord

    inList = SplitData(inData)
    resList = AnalyseGenericMeter(inList, d)
    return resList

if __name__ == "__main__":

    # These lines triggers the SN-x regular expression tests. 

    r = RecordSerialAndModel()
    r.set_1034('A0A\x02\xac\x00A\x01')
    r.set_103c('\x11\x00')
    if r.get_serial_dictionary()[ELEM_DEVICE_SERIAL] != 'A0A172A00017':
        print 'failed to use SN-2 regular expression'

    r.set_1034('7\x07\x03\x00\x0c\x00\x01\x00')
    r.set_103c('\x11\x00')
    if r.get_serial_dictionary()[ELEM_DEVICE_SERIAL] != 'APP03B01847':
        print 'failed to use SN-1 regular expression'

    r.set_1034('A\x00\x7f\x0eg\x00\xa5\x00')
    r.set_103c('\x11\x00')
    if r.get_serial_dictionary()[ELEM_DEVICE_SERIAL] != 'A371101030165':
        print 'failed to use SN-0 regular expression'

    #testfile = open('test/testcases/test_data/iSensCareSens/CareSens-CNP20F13520-few-values.bin')
    #testfile = open('test/testcases/test_data/iSensCareSens/CareSens-CNP20F09597-empty.bin')
    #testfile = open('test/testcases/test_data/iSensCareSens/Fail/CareSens-CNP17E14976-full.bin') # Should fail, incorrect development unit N 250ea with 500 values
    #testfile = open('test/testcases/test_data/iSensCareSens/CareSens-ticket2162-transfer120209.bin')
    #testfile = open('test/testcases/test_data/iSensCareSens/CareSens-ticket2162-transfer120315.bin')

    #testcase = open('test/testcases/test_data/MendorDiscreet/Discreet-A371101030933-few-values.bin').read()
    #testfile = open('test/testcases/test_data/MendorDiscreet/Discreet-A251101031143-full.bin')
    #estcase = open('test/testcases/test_data/MendorDiscreet/Discreet-Z19020.bin').read()
    #testfile = open('test/testcases/test_data/iSensCareSens/alphacheck_professional_500.log')
    #testfile = open('test/testcases/test_data/iSensCareSens/CareSensNPOP_500.log')
    #testfile = open('test/testcases/test_data/iSensCareSens/CareSens-illegal-date.log')
    #testcase = open('test/testcases/test_data/iSensCareSens/CareSensNVoice-CVP01B02486-full.log').read()
    #testcase = open('test/testcases/test_data/iSensCareSens/CareSensN500Rev2-empty.log').read()
    #testcase = open('test/testcases/test_data/iSensCareSens/CareSensN500Rev2-half-full.log').read()
    #testcase = open('test/testcases/test_data/iSensCareSens/CareSensN500Rev2-full.log').read()
    testcase = open('test/testcases/test_data/MenariniGlucoMenReady/Menarini-GlucoMen-Ready.bin').read()

    results = AnalyseDiscreetAndCareSens(testcase)
    print results
