# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
import os.path

def walk_fun(file_list, dirname, filenames):
    
    for filename in filenames:
        fn = os.path.join(dirname, filename)

        if os.path.isfile(fn):
            if fn.endswith(".test"):
                file_list += [fn]

def readTestcases(path):
    """
    Traverse the path, and read all the testcases files recursively
    """
    
    file_list = []
    
    os.path.walk(path, walk_fun, file_list)
    
    return file_list
    
def readTestdata(path):
    """
    Walks only one level
    """
    file_list = []
 
    path = "test/testcases/test_data/" + path
 
    if not os.path.isdir(path):
        print "Invalid directory:", path
        return file_list
        
    root, dirs, filenames = os.walk(path).next()
    
    for filename in filenames:
        # Only add real files
        if not filename.startswith("."):
            file_list += [os.path.join(root, filename)]
        
    return file_list
    
if __name__ == "__main__":
    readTestcases("testcases")
    print readTestdata("testcases/test_data/InsuletOmniPod")