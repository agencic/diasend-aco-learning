# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Extract data from an Endian CSV file and insert it into a DeviceDataCollection. 
#
# Format : 
# 
# TYPE,DATETIME,VALUE,FLAGS,COMMENT
# TERMINAL_SERIAL,,S100010100,,
# DEVICE_SERIAL,,32-323-34,,
# GLUCOSE,2011-01-10 10:00:00,115,,Show (3-minute rule)
# GLUCOSE,2011-01-10 10:30:00,115,FLAG_MANUAL;FLAG_CONTINOUS,
# GLUCOSE,2011-01-10 11:00:00,115,,
# GLUCOSE,2011-01-10 11:30:00,115,,
# GLUCOSE,2011-01-10 12:00:00,115,,
# GLUCOSE,2011-01-10 12:30:00,115,,
#
import csv
import sys
import os
import datetime
from decimal import *

import Defines
from DeviceDataCollection import DeviceDataCollection

ITEM_TYPE = 0
ITEM_DATETIME = 1
ITEM_VALUE = 2
ITEM_FLAGS = 3
ITEM_COMMENT = 4

ITEM_TYPE_TERMINAL_ID = 'TERMINAL_SERIAL'
ITEM_TYPE_DEVICE_ID = 'DEVICE_SERIAL'
ITEM_TYPE_GLUCOSE = 'GLUCOSE'
ITEM_TYPE_INSULIN = 'INSULIN'
ITEM_TYPE_CARBS = 'CARBS'

def ConvertStringToDateTime(s):
    """Convert a date-time string into a datetime object. The %f operator is not
       supported on python 2.5 and earlier so we use this function instead.
    """

    parts = s.split('.')
    
    # Datetime without microseconds.
    dt = datetime.datetime.strptime(parts[0], "%Y-%m-%d %H:%M:%S")

    # Add micrseconds (we have ms).
    dt = dt.replace(microsecond=int(parts[1]) * 1000)

    # Add 500 ms to round to the same second as in the file xml import.
    round_time = datetime.timedelta(microseconds=500000)
    dt = dt + round_time
    
    # Remove the microsecond part (finish the rounding).
    dt = dt.replace(microsecond = 0)
    
    return dt
    
def ConvertTerminalId(collection, item):
    """
    Add terminal id to DeviceDataCollection"
    """
    terminal_id = item[ITEM_VALUE]
    collection.SetTerminalSerialNumber(terminal_id)

def ConvertDeviceId(collection, item):
    """
    Add device id to DeviceDataCollection"
    """
    device_id = item[ITEM_VALUE]
    collection.SetDeviceSerialNumber(device_id)

def ConvertGlucose(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])
    
    value = round(int(item[ITEM_VALUE]) * 1000 / 18.016)

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_GLUCOSE, dt, value, flags)

def ConvertInsulin(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE]) * 100

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_INS_BOLUS, dt, value, flags)

def ConvertCarbs(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE])

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass
        
    collection.AddItem(Defines.VALUE_TYPE_CARBS, dt, value, flags)

def ConvertCSVFile(filename):

    collection = DeviceDataCollection()
    reader = csv.reader(open(filename, "rb"))
    for item in reader:
        if item[ITEM_TYPE] == ITEM_TYPE_TERMINAL_ID:
            ConvertTerminalId(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_DEVICE_ID:
            ConvertDeviceId(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_GLUCOSE:
            ConvertGlucose(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_INSULIN:
            ConvertInsulin(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_CARBS:
            ConvertCarbs(collection, item)

    return collection

if __name__ == '__main__':

    if len(sys.argv) != 2:
        print "usage : %s csv-file" % (sys.argv[0])
        sys.exit(0)

    collection = ConvertCSVFile(sys.argv[1])
    
    collection.PrintStat()
    
#    collection.DeleteDataFromDatabase(collection.device_serial_number)
#    collection.AddDataToDatabase(auto_create_device=True)

