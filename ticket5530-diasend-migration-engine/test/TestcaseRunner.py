# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
import traceback
import TestVariables
from TestcaseReader import readTestdata
import socket
import encrypt
import crc
import time
import sys
import ssl
import os
import zlib
import lzma

ENCRYPTION_BLOCK_SIZE = 16
SERVER_MAX_RECV_LENGTH = 32768
END_OF_BLOCK_SIZE = 16 # actually 28, but the server strips away the "!EOT!DIASEND" terminator
MAX_CHUNK_SIZE = SERVER_MAX_RECV_LENGTH-ENCRYPTION_BLOCK_SIZE-END_OF_BLOCK_SIZE


def recvData(s, expected, result):
    recv = s.recv(256)
    if recv[:len(expected)] != expected:
        print("Got: " + recv + " Expected: " + expected)
        result["comment"] = "Expected: " + expected + ", got: " + recv
        return False
    
    return True


def sendEndOfBlock(s, len, crc):
    s.send("!%04x!%04x!BLOCK!EOT!DIASEND" % (len, crc))

def runCase(host, port, data, result):

    functionBeforeTestrun   = TestVariables.functionBeforeTestrun
    functionAfterTestrun    = TestVariables.functionAfterTestrun

    TestVariables.SetRunningTestCase(result)

    # Run function to setup prerequisites if it exists   
    if functionBeforeTestrun and not functionBeforeTestrun():
        result["comment"] = "Couldn't setup prerequisites"
        return

    # These variables might be changed by the functionBeforeTestrun function
    # so they must be extracted after that function is called!
    protocol                = TestVariables.protocol
    compression             = TestVariables.compression
    hdr                     = TestVariables.header
    tail                    = TestVariables.tail
    responses               = TestVariables.responses
    post_header_delay       = TestVariables.post_header_delay

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((host, port))
    except:
        print sys.exc_info()
        result["trace"]    = traceback.extract_stack()
        result["comment"]  = "Failed to connect to server"
        s.close()
        return

    # If we run with compression, we have to re-chunk completely
    if data and protocol != 3 and compression:
        flat = "".join(data)
        if compression == "LZMA":
            options = {
                'level': 0,
                'format': 'xz'
            }
            data = [lzma.compress(flat, options=options)]
        elif compression == "ZLIB":
            data = [zlib.compress(flat)]

    # If the data chunk(s) are larger than the server can handle
    # we have to re-chunk
    chunks = None
    if data != None:
        chunks = []
        for chunk in data:
            if len(chunk) > MAX_CHUNK_SIZE:
                for i in range(0, len(chunk), MAX_CHUNK_SIZE):
                    chunks.append(chunk[i:i+MAX_CHUNK_SIZE])
            else:
                chunks.append(chunk)
    if protocol == 3:
        s.send("!STARTTLS!EOT!DIASEND")
        
        s.recv(10)
        
        s = ssl.wrap_socket(s,
            ca_certs     = os.path.join(sys.path[0], "ssl-keys/cert.pem"),
            cert_reqs    = ssl.CERT_REQUIRED,
            server_side  = False,
            ssl_version  = ssl.PROTOCOL_TLSv1)

    if responses == None:
        # There are not responses define, asume an OK per element
        num_responses = 0;
        if hdr != None:
            num_responses += 1
            
        if chunks != None:
            num_responses += len(chunks)
            
        if tail != None:
            num_responses += 1
        
        responses = []
        
        for i in range(num_responses):
            responses.append("!OK")
    
    response_no = 0
            
    # We are now connected to the remote host, start sending header if there is any
    if hdr:
        s.send(hdr)
        sendEndOfBlock(s, 4321, crc.calculate_crc(hdr))
        if not recvData(s, responses[response_no], result):
            s.close()
            return
        response_no += 1

    time.sleep(post_header_delay)

    if chunks:
        for chunk in chunks:
            if protocol != 3:
                enc = encrypt.encrypt(chunk)
            else:
                enc = chunk
            s.send(enc)
            sendEndOfBlock(s, len(chunk), crc.calculate_crc(enc))
            if not recvData(s, responses[response_no], result):
                s.close()
                return
            response_no += 1

    if tail:
        if protocol == 3:
           s.send("!00")    # terminal result code
        s.send(tail)
        if not recvData(s, responses[response_no], result):
            s.close()
            return
        response_no += 1

    if protocol == 3:
        s.unwrap()
    
    s.close()

    # Run function to do additional checking if it exists   
    if functionAfterTestrun and not functionAfterTestrun():
        result["comment"] = "Additional checking failed (functionAfterTestrun)"
        return
    
    result["result"] = "SUCCESS"


def runTestcase(file, host, port, protocol=2,compression=None,cookie=""):

    test_desc = file
    if compression:
        test_desc += "@" + compression 
    print ("Running testcase: " + test_desc)

    result = {"testcase" : test_desc, "result" : "FAIL"}
    
    TestVariables.init()

    if len(cookie) > 0:
        TestVariables.setCookie(cookie)

    if not TestVariables.protocol:
        TestVariables.setProtocolVersion(protocol)

    TestVariables.setCompression(compression)

    try:
        # exec the testcase file, must contain python code!
        execfile(file)
    except:
        result["trace"]    = traceback.extract_stack()
        result["comment"]  = "Failed to exec testcase file"        
        return result
        
    if TestVariables.data_directory:
        # The testcase contains a data directory. Run the test with all the sub data files
        filenames = readTestdata(TestVariables.data_directory)
        
        if not filenames:
            result["comment"] = "No files in the specified data directory '"+TestVariables.data_directory+"'"
            return result
            
        # Since this can be several files, we will return a list of results
        results = []
            
        for filename in filenames:
            # Extract the data from the file
            result_single = result.copy()
            result_single["data_file"] = filename
            f = open(filename, "rb")
            databin = [f.read()]
            f.close()
           
            # Run the case
            runCase(host, port, databin, result_single)
                
            # Now, store this result to the list of results
            results.append(result_single)
        
        return results
    else:
        # Testcase is read from file -> run it.
        runCase(host, port, TestVariables.data, result)
    
    return result
    
if __name__ == "__main__":
    res = runTestcase("testcases/AbbottFreestyleFlash.test", "192.168.2.28", 50000)
    print(res)
