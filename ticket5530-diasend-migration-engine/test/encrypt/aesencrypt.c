// -----------------------------------------------------------------------------
// Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
// Developed by Endian Technologies AB, Sweden, http://endian.se
// This code is based on the "aesexam.c" example code, which is
// Copyright (c) 2001, Dr Brian Gladman <                 >, Worcester, UK.
// -----------------------------------------------------------------------------
#define	 __AESENCRYPT_C__

#include <encryption.h>

#include <stdint.h>
#include <stdlib.h>


//  This code implements AES in CBC mode with ciphertext stealing.
//  Ciphertext stealing modifies the encryption of the last two CBC
//  blocks. It can be applied invariably to the last two plaintext
//  blocks or only applied when the last block is a partial one. For
//  a plaintext consisting of N blocks, with the last block possibly
//  a partial one, ciphertext stealing works as shown below (note the
//  reversal of the last two ciphertext blocks).
//
//    +---------+   +---------+   +---------+   +-------+-+
//    |  P:N-4  |   |  P:N-3  |   |  P:N-2  |   | P:N-1 |0|
//    +---------+   +---------+   +---------+   +-------+-+
//         |             |             |             |
//         v             v             v             v
//  +----->x      +----->x      +----->x      +----->x   x = xor
//  |      |      |      |      |      |      |      |
//  |      v      |      v      |      v      |      v
//  |    +---+    |    +---+    |    +---+    |    +---+
//  |    | E |    |    | E |    |    | E |    |    | E |
//  |    +---+    |    +---+    |    +---+    |    +---+
//  |      |      |      |      |      |      |      |
//  |      |      |      |      |      v      |      |
//  |      |      |      |      | +-------+-+ |      |
//  |      |      |      |      | | C:n-1 |x|-+      |
//  |      |      |      |      | +-------+-+        |
//  |      |      |      |      |     |              |
//  |      |      |      |      |     +------------+ |
//  |      |      |      |      |      +-----------|-+
//  |      |      |      |      |      |           |
//  |      v      |      v      |      v           v
//  | +---------+ | +---------+ | +---------+  +-------+
// -+ |  C:N-4  |-+ |  C:N-3  |-+ |  C:N-2  |  | C:N-1 |
//    +---------+   +---------+   +---------+  +-------+


// The KEY_LENGTH and KEY_DATA are defined in "encryption.h"
static const uint8_t	key[KEY_LENGTH] = KEY_DATA;


static void fillrand(char* dst, int len)
{
	srand( 42 /* systemticks_get() */);
	while (--len)
	{
		*dst++ = (char)(rand()&0xff);
	}
}


int aes_encrypt(void* dst, const void* src, int len)
{
	int		i;
	aes_ctx		ctx[1];
	char            buf[BLOCK_SIZE], dbuf[2*BLOCK_SIZE];
	//volatile uint32_t tbefore=0, telapsed;  //useful for encryption performance measurement


	//tbefore = systemticks_get();	//useful for encryption performance measurement
	//setup the aes context
	memset(ctx, 0, sizeof(ctx[0]));	//ensure all flags are initially set to zero
        aes_enc_key(key, KEY_LENGTH, ctx);

	//set a random IV
	fillrand(dbuf, BLOCK_SIZE);

	//write the IV
	memcpy(dst, dbuf, BLOCK_SIZE);
	dst += BLOCK_SIZE;
	
	//read the src a block at a time 
	while (len>0)
	{  
		//read a block and reduce the remaining byte count
		memcpy(buf, src, BLOCK_SIZE);
		src += BLOCK_SIZE;
		len -= BLOCK_SIZE;
	
		//do CBC chaining prior to encryption
		for (i=0; i<BLOCK_SIZE; ++i)
			buf[i] ^= dbuf[i];
	
		//encrypt the block
		aes_enc_blk(buf, dbuf, ctx);
	
		//if there is only one more block do ciphertext stealing
		if (len>0 && len<BLOCK_SIZE)
		{
			//move the previous ciphertext to top half of double buffer
			//since len bytes of this are output last
			for (i=0; i<BLOCK_SIZE; ++i)
				dbuf[i+BLOCK_SIZE] = dbuf[i];
		
			//read last part of plaintext into bottom half of buffer
			memcpy(dbuf, src, len);
			src += len;
		
			//clear the remainder of the bottom half of buffer
			for (i=0; i<BLOCK_SIZE-len; ++i)
				dbuf[len+i] = 0;
		
			//do CBC chaining from previous ciphertext
			for (i=0; i<BLOCK_SIZE; ++i)
			    dbuf[i] ^= dbuf[i+BLOCK_SIZE];
		
			//encrypt the final block
			aes_enc_blk(dbuf, dbuf, ctx);
		}
	
		//write the encrypted block
		memcpy(dst, dbuf, BLOCK_SIZE);
		dst += BLOCK_SIZE;
	}

	//telapsed = systemticks_get() - tbefore;	//useful for encryption performance measurement
	return 0;
}

