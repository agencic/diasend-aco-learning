// -----------------------------------------------------------------------------
// Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
// Developed by Endian Technologies AB, Sweden, http://endian.se
// -----------------------------------------------------------------------------
#ifndef __ENCRYPTION_H__
#define __ENCRYPTION_H__

#include <aes.h>
#include <string.h>


#define ENCRYPTION_BLOCK_SIZE	16

extern int aes_encrypt(void* dst, const void* src, int len);


#ifdef __AESENCRYPT_C__

#define KEY_LENGTH		16
#define KEY_DATA		{0x08,0xcd,0x4e,0xff,0x56,0x12,0x84,0x3d,0x40,0xdc,0x79,0x5f,0x66,0xb7,0x26,0x83}

#endif //__AESENCRYPT_C__



#endif //__ENCRYPTION_H__

