#include <Python.h>
#include "encryption.h"


static PyObject *encrypt_encrypt(PyObject *self, PyObject *args)
{
    const char  *src;
    char        *dst;
    int         len;
    PyObject    *obj;

    if(!PyArg_ParseTuple(args, "s#", &src,&len))
    {
        return NULL;
    }

    if (len < ENCRYPTION_BLOCK_SIZE)
    {
        len = ENCRYPTION_BLOCK_SIZE;
    }
    else if (len % ENCRYPTION_BLOCK_SIZE)
    {        
    	//pad datalen to even block size
		len += (ENCRYPTION_BLOCK_SIZE - len % ENCRYPTION_BLOCK_SIZE);     	
    }

    /* make space for the random vector aswell */
    dst = (char *)malloc(len + ENCRYPTION_BLOCK_SIZE + 1);
    memset(dst,'\0',len + ENCRYPTION_BLOCK_SIZE + 1);
    aes_encrypt(dst,src,len);
    obj = Py_BuildValue("s#",dst,len + ENCRYPTION_BLOCK_SIZE);
    free(dst);
    
    return obj;
}

static PyMethodDef EncryptMethods[] = 
{
    { "encrypt", (PyCFunction)encrypt_encrypt, METH_VARARGS, "encrypt." }, 
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initencrypt(void)
{
    (void)Py_InitModule("encrypt", EncryptMethods );
}
