# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# DeviceDataCollection is a class that holds all data for a single device. The
# class is able to populate itself from the database but it can also be populated
# "manually". The main purpose of the collection is to be able to compare to 
# bodies of data (one from the database and one from a control set).

import TestVariables
import csv
from Defines import *

class Item(object):
    """
    An Item is equal to a row in the telemeddata table with all flags collected
    into a Python list.
    """

    def __init__(self, name, date_time, value, flags, sec_values):
        """
        Constructor.

        name        Type of item ["Alarm", "Prime", "TDD", "Basal", "Bolus", "Glucose"]
        found       Internal variable (has this item been matched by an item in another
                    collection).
        date_time   Date and time of item.
        value       Value of item.
        flags       List of flags from telemeddata_flags.
        sec_values  Dictionary of secondary values from telemeddata_values.
        """
        self.name = name
        self.found = False
        self.date_time = date_time
        self.value = value
        self.flags = flags
        self.flags.sort()
        self.sec_values = sec_values

    def __str__(self):
        """
        Create string of item.
        """
        s = "%s : " % (self.__dict__["name"])
        for item in self.__dict__.iteritems():
            if not item[0] in ('found', 'name'):
                s += "%s = %s  " % (item[0], item[1])
        return s

    def __eq__(self, other):
        """
        To be equal two items must have the same attributes with the same
        values. Note the use of self.found to prevent the same item being
        matched twice.
        """
        return self.__dict__ == other.__dict__

    def WriteRow(self, writer):
        """
        Write this item to a csv-file.
        """
        writer.writerow((self.date_time, self.name, self.value, self.flags))

class Items(dict):
    """
    Collection of one type of Items (e.g. glucose values or alarms). The idea is to
    use the DateTime object of an Item as the key (even if it is not enforced by this
    class; it is just an ordinary dictionary that can use any key). The value is a list
    of Item instances that corresponds to the key.
    """

    def __init__(self, name):
        """
        Constructor.

        name Type of item ["Alarm", "Prime", "TDD", "Basal", "Bolus", "Glucose"]
        """
        super(Items, self).__init__()
        self.name = name

    def __missing__(self, key):
        """
        Default factory run when a key is missing. Create the key and add an empty list.
        This makes it possibe to assign directly to an nonexisting key.
        """
        new_list = list()
        self[key] = new_list
        return new_list

    def __len__(self):
        """
        Return number of Item instances in this dictionary (note that each value
        can hold mulitple instances).
        """
        number_items = 0

        for key, items in self.iteritems():
            number_items += len(items)

        return number_items

    def __eq__(self, other):
        """
        Compare this collection with another collection and return boolean.
        """
        equal, my_missing, other_missing = self.CompareWithCollection(other)
        return equal

    def WriteRows(self, writer):
        """
        Loop over all items in the dictionary and write it to a csv file. 
        """
        for key in sorted(self.iterkeys()):
            items = self[key]
            for item in items:
                item.WriteRow(writer)

    def AddNewItem(self, date_time, value, flags, sec_values = {}):
        """
        Create a new Item instance and add it to myself. 
        """
        new_item = Item(self.name, date_time, value, flags, sec_values)
        self[date_time].append(new_item)
        return new_item

    def CompareWithCollection(self, other):
        """
        Compare this collection with another collection. Return three parameters;
        equal, my_missing, other_missing where :

            equal           True/False are the collections equal?
            my_missing      List of Item instances that only exits in self.
            other_missing   List of Item instances that only exists in other.
        """
        # We want to find the slice of items that can be found in
        # both dictionaries. We loop through one and try to look it
        # up in the other.

        # First look if we can the same DateTime object in both
        # dictionaries.
        for db_key, db_items in self.iteritems():
            if other.has_key(db_key):
                # Now look at the Item instances for the DateTime.
                for db_item in db_items:
                    for other_item in other[db_key]:
                        if db_item == other_item:
                            db_item.found = True
                            other_item.found = True
                            break

        my_missing = []
        other_missing = []

        for db_key, db_items in self.iteritems():
            for db_item in db_items:
                if not db_item.found:
                    my_missing.append(db_item)

        for other_key, other_items in other.iteritems():
            for other_item in other_items:
                if not other_item.found:
                    other_missing.append(other_item)

        return (len(my_missing) == 0 and len(other_missing) == 0), my_missing, other_missing

class DeviceDataCollection(object):
    """
    A class that holds data (such as glucose values) for a singel device.
    The class has functions for retrieving such data from the database; but
    also functions that makes it possible to populate the collection from the
    an external source. A collection can be compared to another collection.
    """

    def __init__(self, ignore_sec_values = False):
        """
        Constructor.
        """
        self.value_types = [ \
            VALUE_TYPE_GLUCOSE, \
            VALUE_TYPE_KETONES, \
            VALUE_TYPE_INS_BASAL, \
            VALUE_TYPE_INS_BOLUS, \
            VALUE_TYPE_INS_BOLUS_EXT, \
            VALUE_TYPE_INS_BOLUS_BASAL, \
            VALUE_TYPE_INS_TDD, \
            VALUE_TYPE_INS_BASAL_TDD, \
            VALUE_TYPE_ALARM, \
            VALUE_TYPE_INS_PRIME, \
            VALUE_TYPE_PEN_CAP_OFF, \
            VALUE_TYPE_TEMP, \
            VALUE_TYPE_DURATION, \
            VALUE_TYPE_EXERCISE, \
            VALUE_TYPE_CARBS, \
            VALUE_TYPE_EVENT, \
            VALUE_TYPE_INS_BOLUS_IOB, \
            VALUE_TYPE_INS_BOLUS_OVERRIDE, \
            VALUE_TYPE_INS_BOLUS_SUGGESTED, \
            VALUE_TYPE_INS_BOLUS_SUGGESTED_MEAL, \
            VALUE_TYPE_INS_BOLUS_SUGGESTED_CORR, \
            VALUE_TYPE_INS_BOLUS_PROGRAMMED, \
            VALUE_TYPE_INS_BOLUS_PROGRAMMED_MEAL, \
            VALUE_TYPE_INS_BOLUS_PROGRAMMED_CORR]
        self.Reset()
        self.ignore_sec_values = ignore_sec_values

    def __eq__(self, other):
        """
        Compare me with the provided collection.
        """
        for value_type in self.value_types:
            if not self.items[value_type].__eq__(other.items[value_type]):
                return False

        return True

    def __ne__(self, other):
        """
        Compare me with the provided collection.
        """
        return not self.__eq__(other)

    def __len__(self):
        """
        Return number of items in the collection.
        """
        res = 0
        for value_type in self.value_types:
            res += len(self.items[value_type])

        return res

    def WriteCSV(self, filename):
        """
        Write comma separated file of the collection.
        """
        f = open(filename, "wb")
        writer = csv.writer(f)
        for value_type in self.value_types:
            self.items[value_type].WriteRows(writer)
        f.close()

    def PrintDifferences(self, other):
        """
        Print the differences between myself and another collection.
        """
        for value_type in self.value_types:
            equal, my_missing, other_missing = self.items[value_type].CompareWithCollection( \
                    other.items[value_type])
            if not equal:
                for item in my_missing:
                    print "Me    %s" % (item)
                for item in other_missing:
                    print "Other %s" % (item)

    def SetTerminalSerialNumber(self, terminal_serial_number):
        """
        Set terminal number. 
        """
        self.terminal_serial_number = terminal_serial_number
    
    def SetDeviceSerialNumber(self, device_serial_number):
        """
        Set device serial number. 
        """
        self.device_serial_number = device_serial_number
        
    def GetDeviceSerialNumber(self):
        """
        Get it. 
        """
        return self.device_serial_number

    def Reset(self):
        self.SetTerminalSerialNumber('')
        self.SetDeviceSerialNumber('')
        self.items = {}

        for value_type in self.value_types:
            self.items[value_type] = Items(value_type)

    def DeleteDataFromDatabase(self, serial_number):
        """
        Delete device data from the database.
        """
        query = "SELECT seq FROM devices WHERE serialnumber = %s"
        device_ids = TestVariables.PerformDatabaseQuery(query, (serial_number))
        if len(device_ids) > 0:
            device_id = device_ids[0][0]
        else:
            return False
        
        query = """DELETE FROM telemeddata_flags WHERE telemeddata_seq in
                    (SELECT seq FROM telemeddata WHERE device_id = %s)"""
        TestVariables.PerformDatabaseQuery(query, (device_id))

        query = """DELETE FROM telemeddata_values WHERE telemeddata_seq in
                    (SELECT seq FROM telemeddata WHERE device_id = %s)"""
        TestVariables.PerformDatabaseQuery(query, (device_id))

        query = """DELETE FROM telemeddata WHERE device_id = %s"""
        TestVariables.PerformDatabaseQuery(query, (device_id))

        return True

    def CollectDataFromDatabase(self, serial_number, filter = None):
        """
        Collect device data from the database. 

        The parameter is a list of the types of data that should be 
        extracted, e.g. [VALUE_TYPE_GLUCOSE, VALUE_TYPE_ALARM]. If 
        None then all types are extracted.
        """

        query = "SELECT seq FROM devices WHERE serialnumber = %s"
        device_ids = TestVariables.PerformDatabaseQuery(query, (serial_number))

        if len(device_ids) > 0:
            device_id = device_ids[0][0]
        else:
            return False

        query = """SELECT seq, timestamp, value, flag FROM telemeddata
                   LEFT JOIN telemeddata_flags ON (telemeddata.seq = telemeddata_flags.telemeddata_seq)
                   WHERE telemeddata.device_id = %s AND type = %s"""

        if filter:
            value_types = filter
        else:
            value_types = self.value_types

        for value_type in value_types:
            values = TestVariables.PerformDatabaseQuery(query, (device_id, value_type))
            values = self.SeparateFlags(values)

            for value in values:
                
                # Try to find secondary values. 
                sec_value_dict = {}
                sub_query = "SELECT type, value FROM telemeddata_values WHERE telemeddata_seq = %s"
                sec_value_result = TestVariables.PerformDatabaseQuery(sub_query, (value[0]))
                if len(sec_value_result) > 0:
                    sec_value_dict = dict(sec_value_result)

                self.AddItem(value_type, value[1], value[2], value[3], sec_value_dict)

        return True

    def AddDataToDatabase(self, auto_create_device=False):
        """
        Add the data in the collection to the database.
        """
        # Get device id (seq) from database
        query = "SELECT seq FROM devices WHERE serialnumber = %s"
        device_ids = TestVariables.PerformDatabaseQuery(query, (self.device_serial_number))

        if type(device_ids) is tuple and len(device_ids) > 0:
            device_id = device_ids[0][0]
        elif auto_create_device:
            query = "INSERT INTO devices (serialnumber, devicetype, deviceclass) VALUES (%s, %s, %s)"
            result = TestVariables.PerformDatabaseQuery(query, (self.device_serial_number, "Diasend Test Device", "device_meter"))
            device_id = TestVariables.GetLastInsertId()
        else:
            return False

        # Get terminal id (seq) from database
        query = "SELECT seq FROM terminals WHERE terminal_id = %s"
        terminal_ids = TestVariables.PerformDatabaseQuery(query, (self.terminal_serial_number))

        if type(terminal_ids) is tuple and len(terminal_ids) > 0:
            terminal_id = terminal_ids[0][0]
        else:
            return False

        # Create a new transfer id
        query = "INSERT INTO transfers (timestamp, terminal_id, device_id) VALUES (now(), %s, %s)"
        TestVariables.PerformDatabaseQuery(query, (terminal_id, device_id))
        transfer_id = TestVariables.GetLastInsertId()

        if type(transfer_id) is type(None):
            return False

        for value_type in self.value_types:
            for key, list_of_items in self.items[value_type].iteritems():
                for item in list_of_items:
                    query = "INSERT INTO telemeddata (device_id, transfer_id, timestamp, type, value) VALUES (%s,%s,%s,%s,%s)"
                    result = TestVariables.PerformDatabaseQuery(query, (device_id, transfer_id, item.date_time, value_type, item.value))
                    value_id = TestVariables.GetLastInsertId()
                    if len(item.flags) > 0:
                        for flag in item.flags:
                            query = "INSERT INTO telemeddata_flags (telemeddata_seq, flag) VALUES (%s, %s)"
                            result = TestVariables.PerformDatabaseQuery(query, (value_id, flag))

    def AddItem(self, value_type, date_time, value, flags, sec_values = {}):
        """
        Helper function : add data to collection.
        """
        if self.ignore_sec_values:
            sec_values = {}
        return self.items[value_type].AddNewItem(date_time, value, flags, sec_values)

    def PrintStat(self):
        for value_type in self.value_types:
            print "%s %d" % (value_type, len(self.items[value_type]))

    def SeparateFlags(self, database_list):
        """
        Take a list of rows from the database with the following format :

            ('seq', 'item1', ... 'itemN-1', 'flag')

        Several rows with the same seq number can exists. Convert this to
        a list of where each row has the following format.

            ['seq', 'item1', ... 'itemN-1', ['flag1', ... 'flagN']]

        and each row has a unique 'seq' value.
        """

        d = {}

        for row in database_list:
            seq = int(row[0])
            if not d.has_key(seq):
                # {seq, ['seq', 'item1', ..., 'itemN-1', [flag1, ..., flagN]]}

                new_row = list(row[:-1])
                new_row.append([])
                d[seq] = new_row

            if row[-1] != None:
                d[seq][-1].append(row[-1])

        l = []
        for row in d.iteritems():
            l.append(row[1])

        l.sort()

        return l

if __name__ == "__main__":

    def AssertTrue(statement, message):
        if not statement:
            print "Test failed : %s" % (message)
        else:
            print "Test ok     : %s" % (message)

    def AssertFalse(statement, message):
        if statement:
            print "Test failed : %s" %(message)
        else:
            print "Test ok     : %s" % (message)

    import datetime

    collection_1 = DeviceDataCollection()
    collection_2 = DeviceDataCollection()

    date_1 = datetime.datetime(2011, 1, 1, 12, 00, 00)
    date_2 = datetime.datetime(2011, 1, 1, 12, 00, 01)

    AssertTrue(collection_1 == collection_2, "Compare empty collections")

    collection_1.AddItem(VALUE_TYPE_GLUCOSE, date_1, 200, [])
    collection_2.AddItem(VALUE_TYPE_GLUCOSE, date_1, 200, [])
    AssertTrue(collection_1 == collection_2, "Compare non-empty collections (1)")

    collection_1.AddItem(VALUE_TYPE_GLUCOSE, date_1, 201, [])
    AssertFalse(collection_1 == collection_2, "Compare collections with different number of items")

    collection_2.AddItem(VALUE_TYPE_GLUCOSE, date_1, 201, [])
    AssertTrue(collection_1 == collection_2, "Compare non-empty collections (2)")

    collection_1.AddItem(VALUE_TYPE_GLUCOSE, date_1, 202, [1])
    collection_2.AddItem(VALUE_TYPE_GLUCOSE, date_1, 202, [2])
    AssertFalse(collection_1 == collection_2, "Compare collections with different flags")

    collection_1.AddItem(VALUE_TYPE_GLUCOSE, date_1, 202, [2])
    collection_2.AddItem(VALUE_TYPE_GLUCOSE, date_1, 202, [1])
    AssertTrue(collection_1 == collection_2, "Compare non-empty collections (3)")

    collection_1.AddItem(VALUE_TYPE_GLUCOSE, date_1, 203, [1])
    collection_2.AddItem(VALUE_TYPE_GLUCOSE, date_2, 203, [1])
    AssertFalse(collection_1 == collection_2, "Compare collections with different dates")

    collection_1.AddItem(VALUE_TYPE_GLUCOSE, date_2, 203, [1])
    collection_2.AddItem(VALUE_TYPE_GLUCOSE, date_1, 203, [1])
    AssertTrue(collection_1 == collection_2, "Compare non-empty collections (4)")

    collection_1.AddItem(VALUE_TYPE_ALARM, date_1, 1, [])
    collection_1.AddItem(VALUE_TYPE_INS_TDD, date_1, 100, [])
    collection_1.AddItem(VALUE_TYPE_INS_BASAL, date_1,  100, [])
    collection_1.AddItem(VALUE_TYPE_INS_BASAL, date_1,  101, [])
    collection_1.AddItem(VALUE_TYPE_INS_BASAL, date_1,  101, [1])
    collection_1.AddItem(VALUE_TYPE_INS_BASAL, date_2,  102, [])
    collection_1.AddItem(VALUE_TYPE_INS_BASAL, date_2,  103, [])
    collection_1.AddItem(VALUE_TYPE_INS_BASAL, date_2,  103, [1])
    collection_2.AddItem(VALUE_TYPE_ALARM, date_1, 1, [])
    collection_2.AddItem(VALUE_TYPE_INS_TDD, date_1, 100, [])
    collection_2.AddItem(VALUE_TYPE_INS_BASAL, date_1,  100, [])
    collection_2.AddItem(VALUE_TYPE_INS_BASAL, date_1,  101, [])
    collection_2.AddItem(VALUE_TYPE_INS_BASAL, date_1,  101, [1])
    collection_2.AddItem(VALUE_TYPE_INS_BASAL, date_2,  102, [])
    collection_2.AddItem(VALUE_TYPE_INS_BASAL, date_2,  103, [])
    collection_2.AddItem(VALUE_TYPE_INS_BASAL, date_2,  103, [1])
    AssertTrue(collection_1 == collection_2, "Compare non-empty collections with all data types")

    collection_1.AddItem(VALUE_TYPE_ALARM, date_1, 2, [])
    equal, my_missing, other_missing = collection_1.items[VALUE_TYPE_ALARM].CompareWithCollection(collection_2.items[VALUE_TYPE_ALARM])
    AssertFalse(collection_1 == collection_2, "One more alarm in collection_1")
    AssertTrue(len(my_missing) == 1 and len(other_missing) == 0, "One more alarm in collection_1 (size)")
    for item in my_missing:
        print "              Item only in collection_1 : %s" % (item)
    for item in other_missing:
        print "              Item only in collection_2 : %s" % (item)

    collection_2.AddItem(VALUE_TYPE_ALARM, date_1, 2, [])
    AssertTrue(collection_1 == collection_2, "Compare non-empty collections (5)")

    collection_2.AddItem(VALUE_TYPE_ALARM, date_1, 5, [])
    equal, my_missing, other_missing = collection_1.items[VALUE_TYPE_ALARM].CompareWithCollection(collection_2.items[VALUE_TYPE_ALARM])
    AssertFalse(collection_1 == collection_2, "One more alarm in collection_2")
    AssertTrue(len(my_missing) == 0 and len(other_missing) == 1, "One more alarm in collection_2 (size)")
    for item in my_missing:
        print "              Item only in collection_1 : %s" % (item)
    for item in other_missing:
        print "              Item only in collection_2 : %s" % (item)

