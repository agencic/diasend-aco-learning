# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import MySQLdb
import base64
import socket
import ssl
import os 
import sys
import crc

class AccountTest(object):

    TERMINAL_TYPE_CLINIC = "tt_clinic"
    TERMINAL_TYPE_PATIENT = "tt_patient"

    def __init__(self, host, port):
        self.terminal_type = AccountTest.TERMINAL_TYPE_PATIENT
        self.terminal_serial = ""
        self.device_serial = ""
        self.account = ""
        self.password = ""
        self.host = host
        self.port = port
        self.location = ""
        self.firstname = base64.b64encode("first")
        self.lastname = base64.b64encode("last")
        self.connection = None

    def chunks(self, s, n):
        for start in range(0, len(s), n):
            yield s[start:start+n]

    def SetTerminalType(self, terminal_type):
        self.terminal_type = terminal_type

    def SetTerminalSerial(self, serial):
        self.terminal_serial = serial

    def SetAccountCredentials(self, account, password):
        self.account = base64.b64encode(account)
        self.password = base64.b64encode(password)

    def SetDeviceSerial(self, serial):
        self.device_serial = serial

    def SetLocation(self, location):
        self.location = location

    def ConnectToDatabase(self):
        host        = "localhost"
        port        = 3306
        db          = "diasend"
        user        = "root"
        passwd      = ""
        try:
            self.connection = MySQLdb.connect( host=host, user=user, passwd=passwd, port=port, db=db )
            self.connection.autocommit(True)
        except MySQLdb.MySQLError, e:
            print "Connect to database error: ", repr(e)
            self.connection = None

    def PerformQuery(self, query, param):
        result = ()
        if self.connection == None:
            return ()

        try:
            cursor = self.connection.cursor()
            cursor.execute(query, param)
            result = cursor.fetchall()
            cursor.close()

        except MySQLdb.MySQLError, e:
            print "Failed to execute query %s with params %s" % (query, param), e
            return ()

        return result

    def ResetDatabase(self):

        terminals = ("NN", "NY", "YN", "YY")

        for terminal in terminals:
            query = 'update terminals set admin = %s where terminal_id = %s'
            param = ("t01_adm_%s" % (terminal), "DDA-DIAS-t01-t01_adm_%s" % (terminal))
            self.PerformQuery(query, param)

        query = 'delete from user_properties where property = "%s" and pcode like "t01_user_%%"'
        param = ("fileupload_srpa")
        self.PerformQuery(query, param)

        query = 'delete from device_ownership where pcode like "t01_user_%%"'
        param = None 
        self.PerformQuery(query, param)

    def FindDeviceInDatabase(self, serial):
        query = "SELECT seq FROM devices WHERE serialnumber = %s"
        param = (serial)
        device_ids = self.PerformQuery(query, param)
        if len(device_ids) > 0:
            return True
        
        return False
    
    def DeleteDevice(self, serial):
        query = "SELECT seq FROM devices WHERE serialnumber = %s"
        param = (serial)
        device_ids = self.PerformQuery(query, param)
        if len(device_ids) > 0:
            device_id = device_ids[0][0]
        else:
            return False
       
        query = """DELETE FROM telemeddata_flags WHERE telemeddata_seq in
                    (SELECT seq FROM telemeddata WHERE device_id = %s)"""
        param = (device_id)
        self.PerformQuery(query, param)

        query = """DELETE FROM telemeddata_values WHERE telemeddata_seq in
                    (SELECT seq FROM telemeddata WHERE device_id = %s)"""
        param = (device_id)
        self.PerformQuery(query, param)

        query = """DELETE FROM telemeddata WHERE device_id = %s"""
        param = (device_id)
        self.PerformQuery(query, param)

        query = """SELECT seq FROM device_programs WHERE device_id = %s"""
        param = (device_id)
        programs = self.PerformQuery(query, param)
        if len(programs) > 0:
            for program in programs:
                query = """DELETE FROM device_program_values WHERE device_program_period_id = %s"""
                param = (program)
                self.PerformQuery(query, param)

        query = """DELETE FROM device_programs WHERE device_id = %s"""
        param = (device_id)
        self.PerformQuery(query, param)
        
        query = """DELETE FROM device_settings WHERE device_id = %s"""
        param = (device_id)
        self.PerformQuery(query, param)
        
        query = """DELETE FROM transfers WHERE device_id = %s"""
        param = (device_id)
        self.PerformQuery(query, param)
        
        query = """DELETE FROM device_ownership WHERE device_id = %s"""
        param = (device_id)
        self.PerformQuery(query, param)
        
        query = """DELETE FROM devices WHERE seq = %s"""
        param = (device_id)
        self.PerformQuery(query, param)
        
        return True

    def Send(self, data, device_type, expected_response, serial_in_db):
        
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host, self.port))
       
        s.send("!STARTTLS!EOT!DIASEND")
        response = s.recv(1024)
        
        s = ssl.wrap_socket(s,
            ca_certs     = os.path.join(sys.path[0], "../ssl-keys/cert.pem"),
            cert_reqs    = ssl.CERT_NONE,
            server_side  = False,
            ssl_version  = ssl.PROTOCOL_TLSv1)
        
        header = ('!HDR!DIASEND\r\n' 
                  '!HDR!PROTVER!3.0\r\n'
                  '!HDR!SWID!A10024\r\n'
                  '!HDR!SERNO!%s\r\n'
                  '!HDR!COMPRESSION!DISABLED\r\n'
                  '!HDR!ENCRYPTION!DISABLED\r\n'
                  '!HDR!INTERFACE!/dev/ttyusbserial\r\n'
                  '!HDR!LOCATION!%s\r\n'
                  '!HDR!METERTYPE!%s\r\n!HDR!METERDATA\r\n') % (self.terminal_serial, self.location, device_type)
       
        if self.terminal_type == AccountTest.TERMINAL_TYPE_CLINIC:
            header += '!HDR!CLINIC!YES\r\n'
        else:
            header += '!HDR!CLINIC!NO\r\n'
        
        if len(self.account) > 0:
            header += '!HDR!USERINFO!FIRSTNAME!%s!LASTNAME!%s!PCODE!%s!PW!%s!ACTION!LOGIN\r\n' \
                    % (self.firstname, self.lastname, self.account, self.password)
        else:
            header += '!HDR!USERINFO\r\n'
    
        header += '!%04x!%04x!BLOCK!EOT!DIASEND' % (len(header), crc.calculate_crc(header))
        s.send(header)
        response = s.recv(1024)
        if not "!OK" in response:
            return (False, "Did not receive !OK on header")
        
        for chunk in self.chunks(data, 3000):
            chunk += '!%04x!%04x!BLOCK!EOT!DIASEND' % (len(chunk), crc.calculate_crc(chunk))
            s.send(chunk)
            response = s.recv(1024)
            if not "!OK" in response:
                return (False, "Did not receive !OK on chunk")
   
        tail = '!0000!0000!00!TRANSFER!EOT!DIASEND'
        s.send(tail)
        response = s.recv(1024)
        
        s.close()

        ok = True
        desc = ""

        if not expected_response in response:
            ok = False
            desc = "Expected response %s got %s, " % (expected_response, response)

        if serial_in_db != None:
            if not self.FindDeviceInDatabase(serial_in_db):
                ok = False
                desc += "Didn't find device in db"
            if serial_in_db == "23-37217-99":
                if self.FindDeviceInDatabase("ESlIjbKyoAdppwV6QadjDQ5w=="):
                    ok = False
                    desc += "Found both"
            elif serial_in_db == "ESlIjbKyoAdppwV6QadjDQ5w==":
                if self.FindDeviceInDatabase("23-37217-99"):
                    ok = False
                    desc += "Found both"
        else:
            if self.FindDeviceInDatabase("23-37217-99"):
                ok = False
                desc += "Found unencrypted device"
            elif self.FindDeviceInDatabase("ESlIjbKyoAdppwV6QadjDQ5w=="):
                ok = False
                desc += "Found encrypted device"

        return (ok, desc)

if __name__ == '__main__':
    at = AccountTest("192.168.2.25", 50006)
    at.ConnectToDatabase()

    at.SetTerminalSerial("DDA-DIAS-t01-t01_adm_YY")
    at.SetAccountCredentials("t01_user_YYYY", "t01_user_YYYY")
    at.SetLocation("SE");

    f = open("testcases/test_data/AnimasIRUnity/ep005.bin", "rb")
    animas_data = f.read()
    f.close()
                
    nr_ok = 0
    nr_fail = 0

    # has_file_access, has_file_access_by_clinic, active, french
    test_data = { 
                "DDA-DIAS-t01-t01_adm_NN": 
                { # If user not archived we have an ok transfer. If user is archived the 
                  # device is still created but we get USER_REQUIRED.
                  AccountTest.TERMINAL_TYPE_PATIENT :  
                    (("t01_user_NNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!OK", "23-37217-99"), 
                     ("t01_user_NNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!OK", "23-37217-99"),
                     ("t01_user_NYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!OK", "23-37217-99"),
                     ("t01_user_YNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!OK", "23-37217-99"),
                     ("t01_user_YYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="), 
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YY", "!NAK!0021", None),
                     ("t01_adm_NN",    "!NAK!0021", None), # USER_REQUIRED if admin and terminal type patient. 
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!NAK!0021", None), 
                     ("t01_adm_YY",    "!NAK!0021", None)), 
                 # Terminal registered on clinic without DDA access -> USER_REQUIRED. 
                 # If user is patient device created before terminal check.
                 AccountTest.TERMINAL_TYPE_CLINIC :  
                    (("t01_user_NNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!NAK!0023", "23-37217-99"), 
                     ("t01_user_NNYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!NAK!0023", "23-37217-99"),
                     ("t01_user_NYYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!NAK!0023", "23-37217-99"),
                     ("t01_user_YNYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!NAK!0023", "23-37217-99"),
                     ("t01_user_YYYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN",  "!OK", "23-37217-99"), 
                     ("t01_clinic_user_t01_adm_YY",  "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="), 
                     ("t01_adm_NN",    "!NAK!0021", None), 
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!OK", "23-37217-99"), 
                     ("t01_adm_YY",    "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==")), 
                },
                "DDA-DIAS-t01-t01_adm_YN": 
                { # If user not archived we have an ok transfer. If user is archived the 
                  # device is still created but we get USER_REQUIRED. That we got a 
                  # terminal with DDA access makes no difference.
                  AccountTest.TERMINAL_TYPE_PATIENT :  
                    (("t01_user_NNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!OK", "23-37217-99"), 
                     ("t01_user_NNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!OK", "23-37217-99"),
                     ("t01_user_NYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!OK", "23-37217-99"),
                     ("t01_user_YNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!OK", "23-37217-99"),
                     ("t01_user_YYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YY", "!NAK!0021", None),
                     ("t01_adm_NN",    "!NAK!0021", None),  # USER_REQUIRED if admin and terminal type patient.
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!NAK!0021", None), 
                     ("t01_adm_YY",    "!NAK!0021", None)), 
                 # Tranfser from terminal of clinic type where terminal is attached
                 # to a clinic with DDA access -> OK.
                 AccountTest.TERMINAL_TYPE_CLINIC :  
                    (("t01_user_NNNN", "!OK", "23-37217-99"),  
                     ("t01_user_NNNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!OK", "23-37217-99"), 
                     ("t01_user_NNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!OK", "23-37217-99"),  
                     ("t01_user_NYNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!OK", "23-37217-99"),
                     ("t01_user_NYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!OK", "23-37217-99"),  
                     ("t01_user_YNNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!OK", "23-37217-99"),
                     ("t01_user_YNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!OK", "23-37217-99"),  
                     ("t01_user_YYNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!OK", "23-37217-99"),
                     ("t01_user_YYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN",  "!OK", "23-37217-99"), 
                     ("t01_clinic_user_t01_adm_YY",  "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="), 
                     ("t01_adm_NN",    "!NAK!0021", None), 
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!OK", "23-37217-99"), 
                     ("t01_adm_YY",    "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==")), 
                }, 
                "DDA-DIAS-t01-t01_adm_NY": 
                { # If user not archived we have an ok transfer. If user is archived the 
                  # device is still created but we get USER_REQUIRED. That we got a 
                  # terminal with HIDE_PII makes no difference.
                  AccountTest.TERMINAL_TYPE_PATIENT :  
                    (("t01_user_NNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!OK", "23-37217-99"), 
                     ("t01_user_NNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!OK", "23-37217-99"),
                     ("t01_user_NYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!OK", "23-37217-99"),
                     ("t01_user_YNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!OK", "23-37217-99"),
                     ("t01_user_YYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YY", "!NAK!0021", None),
                     ("t01_adm_NN",    "!NAK!0021", None), # USER_REQUIRED if admin and terminal type patient.
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!NAK!0021", None), 
                     ("t01_adm_YY",    "!NAK!0021", None)), 
                 # Tranfser from terminal of clinic type where terminal is attached
                 # to a clinic with DDA access -> OK.
                 AccountTest.TERMINAL_TYPE_CLINIC :  
                    (("t01_user_NNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!NAK!0023", "23-37217-99"), 
                     ("t01_user_NNYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!NAK!0023", "23-37217-99"),
                     ("t01_user_NYYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!NAK!0023", "23-37217-99"),
                     ("t01_user_YNYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!NAK!0023", "23-37217-99"),
                     ("t01_user_YYYY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN",  "!OK", "23-37217-99"), 
                     ("t01_clinic_user_t01_adm_YY",  "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="), 
                     ("t01_adm_NN",    "!NAK!0021", None), 
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!OK", "23-37217-99"), 
                     ("t01_adm_YY",    "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==")), 
                }, 
                "DDA-DIAS-t01-t01_adm_YY": 
                { # If user not archived we have an ok transfer. If user is archived the 
                  # device is still created but we get USER_REQUIRED. That we got a 
                  # terminal with HIDE_PII makes no difference.
                  AccountTest.TERMINAL_TYPE_PATIENT :  
                    (("t01_user_NNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!OK", "23-37217-99"), 
                     ("t01_user_NNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_NYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!OK", "23-37217-99"),
                     ("t01_user_NYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YNNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!OK", "23-37217-99"),
                     ("t01_user_YNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!NAK!0023", "23-37217-99"),  
                     ("t01_user_YYNY", "!NAK!0023", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!OK", "23-37217-99"),
                     ("t01_user_YYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YY", "!NAK!0021", None),
                     ("t01_adm_NN",    "!NAK!0021", None), # USER_REQUIRED if admin and terminal type patient.
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!NAK!0021", None), 
                     ("t01_adm_YY",    "!NAK!0021", None)), 
                 # Tranfser from terminal of clinic type where terminal is attached
                 # to a clinic with DDA access -> OK.
                 AccountTest.TERMINAL_TYPE_CLINIC :  
                    (("t01_user_NNNN", "!OK", "23-37217-99"),  
                     ("t01_user_NNNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NNYN", "!OK", "23-37217-99"), 
                     ("t01_user_NNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_NYNN", "!OK", "23-37217-99"),  
                     ("t01_user_NYNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_NYYN", "!OK", "23-37217-99"),
                     ("t01_user_NYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YNNN", "!OK", "23-37217-99"),  
                     ("t01_user_YNNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YNYN", "!OK", "23-37217-99"),
                     ("t01_user_YNYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),
                     ("t01_user_YYNN", "!OK", "23-37217-99"),  
                     ("t01_user_YYNY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="),  
                     ("t01_user_YYYN", "!OK", "23-37217-99"),
                     ("t01_user_YYYY", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==") ,
                     ("t01_clinic_user_t01_adm_NN", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_NY", "!NAK!0021", None),
                     ("t01_clinic_user_t01_adm_YN",  "!OK", "23-37217-99"), 
                     ("t01_clinic_user_t01_adm_YY",  "!OK", "ESlIjbKyoAdppwV6QadjDQ5w=="), 
                     ("t01_adm_NN",    "!NAK!0021", None), 
                     ("t01_adm_NY",    "!NAK!0021", None), 
                     ("t01_adm_YN",    "!OK", "23-37217-99"), 
                     ("t01_adm_YY",    "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==")), 
                }, 
            }

    # Test all combinations - delete device between each test. 
    print "Test 1"
    for terminal_serial, value in test_data.iteritems():
        for terminal_type, users in value.iteritems():
            at.SetTerminalSerial(terminal_serial)
            at.SetTerminalType(terminal_type)
       
            print "------>"

            for user in users:
                at.ResetDatabase()
                at.DeleteDevice("23-37217-99")
                at.DeleteDevice("ESlIjbKyoAdppwV6QadjDQ5w==")
                at.SetAccountCredentials(user[0], user[0])
                result, desc = at.Send(animas_data, "AnimasIR", user[1], user[2])
                desc = desc.ljust(64, " ")
                if result:
                    print "--> Ok : Terminal type %s serial %s User %s Expected response %s Expected device in db %s" % \
                        (terminal_type, terminal_serial, user[0], user[1], user[2])
                    nr_ok += 1
                else:
                    print "--> Failed (%s) : Terminal type %s serial %s User %s Expected response %s Expected device in db %s" % \
                        (desc, terminal_type, terminal_serial, user[0], user[1], user[2])
                    nr_fail += 1

    # Create an unencrypted device and keep that device for all tests.
    print "Test 2 (unencrypted device present)"
    
    at.DeleteDevice("23-37217-99")
    at.DeleteDevice("ESlIjbKyoAdppwV6QadjDQ5w==")
    at.SetTerminalSerial("DDA-DIAS-t01-t01_adm_NN")
    at.SetTerminalType(AccountTest.TERMINAL_TYPE_PATIENT)
    at.SetAccountCredentials("t01_user_NNYN", "t01_user_NNYN")
    at.Send(animas_data, "AnimasIR", "!OK", "23-37217-99")
    
    for terminal_serial, value in test_data.iteritems():
        for terminal_type, users in value.iteritems():
            at.SetTerminalSerial(terminal_serial)
            at.SetTerminalType(terminal_type)
            print "------>"
            for user in users:
                at.ResetDatabase()
                at.SetAccountCredentials(user[0], user[0])
                expected_serial_in_db = "23-37217-99"
                result, desc = at.Send(animas_data, "AnimasIR", user[1], expected_serial_in_db) 
                desc = desc.ljust(64, " ")
                if result:
                    print "--> Ok : Terminal type %s serial %s User %s Expected response %s Expected device in db %s" % \
                        (terminal_type, terminal_serial, user[0], user[1], expected_serial_in_db)
                    nr_ok += 1
                else:
                    print "--> Failed (%s) : Terminal type %s serial %s User %s Expected response %s Expected device in db %s" % \
                        (desc, terminal_type, terminal_serial, user[0], user[1], expected_serial_in_db)
                    nr_fail += 1

    # Create an encrypted device and keep that device for all tests.
    print "Test 3 (encrypted device present)"
    
    at.DeleteDevice("23-37217-99")
    at.DeleteDevice("ESlIjbKyoAdppwV6QadjDQ5w==")
    at.SetTerminalSerial("DDA-DIAS-t01-t01_adm_NN")
    at.SetTerminalType(AccountTest.TERMINAL_TYPE_PATIENT)
    at.SetAccountCredentials("t01_user_NNYY", "t01_user_NNYY")
    at.Send(animas_data, "AnimasIR", "!OK", "ESlIjbKyoAdppwV6QadjDQ5w==")
    
    for terminal_serial, value in test_data.iteritems():
        for terminal_type, users in value.iteritems():
            at.SetTerminalSerial(terminal_serial)
            at.SetTerminalType(terminal_type)
            print "------>"
            for user in users:
                at.ResetDatabase()
                at.SetAccountCredentials(user[0], user[0])
                expected_serial_in_db = "ESlIjbKyoAdppwV6QadjDQ5w=="
                result, desc = at.Send(animas_data, "AnimasIR", user[1], expected_serial_in_db) 
                desc = desc.ljust(64, " ")
                if result:
                    print "--> Ok : Terminal type %s serial %s User %s Expected response %s Expected device in db %s" % \
                        (terminal_type, terminal_serial, user[0], user[1], expected_serial_in_db)
                    nr_ok += 1
                else:
                    print "--> Failed (%s) : Terminal type %s serial %s User %s Expected response %s Expected device in db %s" % \
                        (desc, terminal_type, terminal_serial, user[0], user[1], expected_serial_in_db)
                    nr_fail += 1
   
    print "Ok %d Fail %d" % (nr_ok, nr_fail)

    
"""
    at = AccountTest("192.168.2.25", 50006)
    data = ('!TAG!SERNO!AF%s\r\n!TAG!RESULTS!6\r\n'
            '0 127 0x2 197 7 9 19 13 25 48 0x8456\r\n'
            '1 124 0x2 197 7 9 18 1 17 48 0x9316\r\n'
            '2 121 0x2 197 7 9 17 20 59 4 0x515e\r\n'
            '3 125 0x2 197 7 9 14 16 48 14 0xb6ac\r\n'
            '4 128 0x0 197 7 9 14 2 39 54 0x396b\r\n'
            '5 123 0x0 197 7 9 14 2 38 16 0x1a40\r\n') % ("11112222")

    at.SetTerminalType(AccountTest.TERMINAL_TYPE_CLINIC)
    at.SetTerminalSerial("DDA-DIAS-t01-t01_adm_YY")
    at.SetAccountCredentials("t01_user_YYYY", "t01_user_YYYY")
    at.SetLocation("SE");
    at.Send(data, "KEYNOTE")

    f = open("testcases/test_data/AnimasIRUnity/ep005.bin", "rb")
    animas_data = f.read()
    f.close()

    at.Send(animas_data, "AnimasIR")
"""

