import MySQLdb
import inspect
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
import Password

connection = None

#default_host = Config.settings["database-address"]
#default_passwd = Config.settings["database-password"]
#default_user = Config.settings["database-user"]
#default_port = int(Config.settings["database-port"])
#default_db = Config.settings["database-db"]

#host   = default_host
#user   = default_user
#passwd = default_passwd
#port   = default_port
#db     = default_db

def ConnectToDatabase():
    host        = "localhost"
    port        = 3306
    db          = "diasend_db1"
    user        = "root"
    passwd      = "aidera"
    try:
        connection = MySQLdb.connect( host=host, user=user, passwd=passwd, port=port, db=db )
    except MySQLdb.MySQLError, e:
        print "Connect to database error: ", repr(e)

    print connection

    return connection

def PerformQuery(connection, query, param):
    result = ()
    try:
        cursor = connection.cursor()
        cursor.execute(query, param)
        result = cursor.fetchall()
        cursor.close()

    except MySQLdb.MySQLError, e:
        print e, query, param

    return result

def BuildName(l):
    res = ''
    for item in l:
        if item:
            res += 'Y'
        else:
            res += 'N'
    return res

def CreateAdmin(connection, name, password):
    encpasswd = Password.create(password, password)

    query = 'INSERT INTO admins (name, salt, hash, title, phone, support, misc) VALUES (%s, %s, %s, "", "", "", "")'
    param = (name, password, encpasswd)
    PerformQuery(connection, query, param)

    query = 'INSERT INTO admin_properties (admin, property, value) VALUES (%s, "allow_dda", "1")'
    param = (name)
    PerformQuery(connection, query, param)

    query = 'INSERT INTO admin_properties (admin, property, value) VALUES (%s, "individual_login_support", "1")'
    param = (name)
    PerformQuery(connection, query, param)

    query = 'INSERT INTO admin_properties (admin, property, value) VALUES (%s, "diasend_one", "1")'
    param = (name)
    PerformQuery(connection, query, param)

    query = 'INSERT INTO admin_properties (admin, property, value) VALUES (%s, "emr_support", "1")'
    param = (name)
    PerformQuery(connection, query, param)



    return name

def CreateTerminal(connection, serial, admin):
    query = 'INSERT INTO terminals (terminal_id, admin, terminalclass) VALUES (%s, %s, "terminal_dda")'
    param = (serial, admin)
    PerformQuery(connection, query, param)

    query = 'INSERT INTO terminal_permissions (terminal_id, permission) VALUES (%s, "device_pump")'
    param = (serial)
    PerformQuery(connection, query, param)

    return serial

def CreateClinicUser(connection, name, password, admin):
    query = 'INSERT INTO clinic_users (username, password, firstname, lastname, admin) VALUES (%s, %s, "first", "last", %s)'
    param = (name, password, admin)

    PerformQuery(connection, query, param)

    return name

def CreateUser(connection, name, password, has_file_access, has_file_access_by_clinic, active, country_code, firstname, lastname):

    encpasswd = Password.create(password, password)

    if active:
        query = 'INSERT INTO users (pcode, pnumber, salt, hash, archived, firstname, lastname) VALUES (%s, "1234", %s, %s, "No", %s, %s)'
    else:
        query = 'INSERT INTO users (pcode, pnumber, salt, hash, archived, firstname, lastname) VALUES (%s, "1234", %s, %s, "Yes", %s, %s)'

    param = (name, password, encpasswd, firstname, lastname)

    PerformQuery(connection, query, param)

    if has_file_access:
        query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "fileupload_srpa", "1")'
        param = (name)
        PerformQuery(connection, query, param)

    if has_file_access_by_clinic:
        query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "fileupload_admin", "1")'
        param = (name)
        PerformQuery(connection, query, param)

    if country_code:
        query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "country", %s)'
        param = (name, country_code)
        PerformQuery(connection, query, param)

    query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "cgm", "1")'
    param = (name)
    PerformQuery(connection, query, param)


    query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "account_expire", "2630573273")'
    param = (name)
    PerformQuery(connection, query, param)

    return name


def CreateUserOwnership(connection, user, admin):
    query = 'INSERT INTO user_ownership (pcode, admin, comment, access_type) VALUES (%s, %s, "", "access_srpa")'
    param = (user, admin)
    PerformQuery(connection, query, param)


def CreateDevice(connection, serial, device_class):
    query = 'INSERT INTO devices (serialnumber, deviceclass) VALUES (%s, %s)'
    param = (serial, device_class)
    PerformQuery(connection, query, param)

    query = "SELECT * FROM devices WHERE serialnumber=%s"
    param = (serial)
    res = PerformQuery(connection, query, param)

    return res[0][0]


def CreateDeviceOwnership(connection, user, device_serial):
    query = 'INSERT INTO device_ownership (pcode, device_id) VALUES (%s, %s)'
    param = (user, device_serial)
    PerformQuery(connection, query, param)


if __name__ == '__main__':
    connection = ConnectToDatabase()


    user1 = CreateUser(connection, "V1_CGM@diasend.com", "diatest13", False, False, True, "107", "Adam", "Tester")
    user2 = CreateUser(connection, "V2_CGM@diasend.com", "diatest13", False, False, True, "107", "Anna", "Tester")
    user3 = CreateUser(connection, "V3_CGM@diasend.com", "diatest13", False, False, True, "107", "Tim", "Svensson")
    user4 = CreateUser(connection, "V1_Combo@diasend.com", "diatest13", False, False, True, "101", "Jim", "Olsson")
    user5 = CreateUser(connection, "V1_Meter@diasend.com", "diatest13", False, False, True, "102", "Daniel", "Polssen")

    device1 = CreateDevice(connection, "V_CGM001", "device_pump")
    device2 = CreateDevice(connection, "V_CGM002", "device_pump")
    device3 = CreateDevice(connection, "V_CGM003", "device_pump")
    device4 = CreateDevice(connection, "V_Combo01", "device_combo")
    device5 = CreateDevice(connection, "V_Meter01", "device_meter")

    CreateDeviceOwnership(connection, user1, device1);
    CreateDeviceOwnership(connection, user2, device2);
    CreateDeviceOwnership(connection, user3, device3);
    CreateDeviceOwnership(connection, user4, device4);
    CreateDeviceOwnership(connection, user5, device5);


    admin1 = CreateAdmin(connection, "V1_Clinic", "diatest13")
    CreateTerminal(connection, "S100010100", admin1)


    admin2 = CreateAdmin(connection, "V2_Clinic", "diatest13")
    CreateTerminal(connection, "S100010102", admin2)


    CreateUserOwnership(connection, user1, admin1)
    CreateUserOwnership(connection, user2, admin1)
    CreateUserOwnership(connection, user3, admin1)
    CreateUserOwnership(connection, user5, admin1)

    CreateUserOwnership(connection, user4, admin2)


    connection.commit()
    connection.close()
