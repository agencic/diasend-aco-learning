# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, Sweden, http://www.diasend.com
# -----------------------------------------------------------------------------

# Extract data from a CSV file and insert it into a DeviceDataCollection.
#
# Format : 
# 
# TYPE,DATETIME,VALUE,FLAGS,COMMENT
# TERMINAL_SERIAL,,S100010100,,
# DEVICE_SERIAL,,32-323-34,,
# GLUCOSE,2011-01-10 10:00:00,115,,Show (3-minute rule)
# GLUCOSE,2011-01-10 10:30:00,115,FLAG_MANUAL;FLAG_CONTINOUS,
# GLUCOSE,2011-01-10 11:00:00,115,,
# GLUCOSE,2011-01-10 11:30:00,115,,
# GLUCOSE,2011-01-10 12:00:00,115,,
# GLUCOSE,2011-01-10 12:30:00,115,,
#

import csv
import sys
import os
import datetime
import random
import time
from decimal import *
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
import Defines
from DeviceDataCollection import DeviceDataCollection

ITEM_TYPE = 0
ITEM_DATETIME = 1
ITEM_VALUE = 2
ITEM_FLAGS = 3
ITEM_COMMENT = 4
ITEM_RANDOM_FROM_DATE = 1
ITEM_RANDOM_TO_DATE = 2
ITEM_RANDOM_AMOUNT = 3
ITEM_RANDOM_FROM_VALUE = 4
ITEM_RANDOM_TO_VALUE = 5
ITEM_RANDOM_FLAGS = 6

ITEM_TYPE_TERMINAL_ID = 'TERMINAL_SERIAL'
ITEM_TYPE_DEVICE_ID = 'DEVICE_SERIAL'
ITEM_TYPE_GLUCOSE = 'GLUCOSE'
ITEM_TYPE_BASAL = 'INSULIN_BASAL'
ITEM_TYPE_BOLUS = 'INSULIN_BOLUS'
ITEM_TYPE_CARBS = 'CARBS'
ITEM_TYPE_INSULIN_ALARM = 'INSULIN_ALARM'
ITEM_TYPE_INSULIN_TDD = 'INSULIN_TDD'
ITEM_TYPE_INSULIN_PRIME = 'INSULIN_PRIME'
ITEM_TYPE_EVENT = 'EVENT'
ITEM_TYPE_RANDOM_GLUCOSE = 'RANDOM_GLUCOSE'
ITEM_TYPE_RANDOM_INSULIN_BASAL = 'RANDOM_INSULIN_BASAL'
ITEM_TYPE_RANDOM_INSULIN_BOLUS = 'RANDOM_INSULIN_BOLUS'


def ConvertStringToDateTime(s):
    """Convert a date-time string into a datetime object. The %f operator is not
       supported on python 2.5 and earlier so we use this function instead.
    """

    parts = s.split('.')
    
    # Datetime without microseconds.
    dt = datetime.datetime.strptime(parts[0], "%Y-%m-%d %H:%M:%S")

    # Add micrseconds (we have ms).
    if len(parts) > 1:
        dt = dt.replace(microsecond=int(parts[1]) * 1000)

    # Add 500 ms to round to the same second as in the file xml import.
    round_time = datetime.timedelta(microseconds=500000)
    dt = dt + round_time
    
    # Remove the microsecond part (finish the rounding).
    dt = dt.replace(microsecond = 0)
    
    return dt
    
def ConvertTerminalId(collection, item):
    """
    Add terminal id to DeviceDataCollection"
    """
    terminal_id = item[ITEM_VALUE]
    collection.SetTerminalSerialNumber(terminal_id)

def ConvertDeviceId(collection, item):
    """
    Add device id to DeviceDataCollection"
    """
    device_id = item[ITEM_VALUE]
    collection.SetDeviceSerialNumber(device_id)

def ConvertGlucose(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])
    
    value = item[ITEM_VALUE]

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_GLUCOSE, dt, value, flags)


def ConvertInsulinBolus(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE]) * 100

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_INS_BOLUS, dt, value, flags)


def ConvertInsulinBasal(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE]) * 100

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_INS_BASAL, dt, value, flags)


def ConvertInsulinAlarm(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE]) * 100

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_ALARM, dt, value, flags)


def ConvertInsulinTDD(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE]) * 100

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_INS_TDD, dt, value, flags)


def ConvertInsulinPrime(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE]) * 100

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_INS_PRIME, dt, value, flags)


def ConvertEvent(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE])

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_EVENT, dt, value, flags)


def ConvertCarbs(collection, item):

    dt = ConvertStringToDateTime(item[ITEM_DATETIME])

    value = int(item[ITEM_VALUE])

    flags = item[ITEM_FLAGS].split(';')
    for index, flag in enumerate(flags):
        if len(flag) > 0:
            flags[index] = Defines.__dict__[flag.strip()]
        else:
            flags[index] = 0

    try:
        flags.remove(0)
    except ValueError:
        pass

    collection.AddItem(Defines.VALUE_TYPE_CARBS, dt, value, flags)


def strTimeProp(start, end, format):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """
    prop = random.random()
    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))


def randomDate(start, end):
    return strTimeProp(start, end, '%Y-%m-%d %H:%M:%S')


def RandomizeItems(collection, item):
    fromdate = item[ITEM_RANDOM_FROM_DATE]
    todate = item[ITEM_RANDOM_TO_DATE]
    values_to_generate = int(item[ITEM_RANDOM_AMOUNT])
    fromvalue = int(item[ITEM_RANDOM_FROM_VALUE])
    tovalue = int(item[ITEM_RANDOM_TO_VALUE])
    flags = item[ITEM_RANDOM_FLAGS]

    items = []
    for x in range(0,values_to_generate):
        date = randomDate(fromdate, todate)
        newitem = {}
        newitem[ITEM_DATETIME] = date
        newitem[ITEM_VALUE] = fromvalue + random.random() * (tovalue - fromvalue)
        newitem[ITEM_FLAGS] = flags
        items.append(newitem)

    return items

def RandomizeGlucose(collection, item):
    items = RandomizeItems(collection, item)
    for i in items:
        ConvertGlucose(collection, i)


def RandomizeInsulinBasal(collection, item):
    items = RandomizeItems(collection, item)
    for i in items:
        ConvertInsulinBasal(collection, i)


def RandomizeInsulinBolus(collection, item):
    items = RandomizeItems(collection, item)
    for i in items:
        ConvertInsulinBolus(collection, i)


def ConvertCSVFile(filename):

    collection = DeviceDataCollection()
    reader = csv.reader(open(filename, "rb"))
    for item in reader:
        if len(item) == 0 or item[ITEM_TYPE][0] == "#":
            pass
        elif item[ITEM_TYPE] == ITEM_TYPE_TERMINAL_ID:
            ConvertTerminalId(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_DEVICE_ID:
            ConvertDeviceId(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_GLUCOSE:
            ConvertGlucose(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_BASAL:
            ConvertInsulinBasal(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_BOLUS:
            ConvertInsulinBolus(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_CARBS:
            ConvertCarbs(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_INSULIN_ALARM:
            ConvertInsulinAlarm(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_INSULIN_TDD:
            ConvertInsulinTDD(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_INSULIN_PRIME:
            ConvertInsulinPrime(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_EVENT:
            ConvertEvent(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_RANDOM_GLUCOSE:
            RandomizeGlucose(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_RANDOM_INSULIN_BASAL:
            RandomizeInsulinBasal(collection, item)

        elif item[ITEM_TYPE] == ITEM_TYPE_RANDOM_INSULIN_BOLUS:
            RandomizeInsulinBolus(collection, item)

        else:
            print "Error, file contains items of type",item[ITEM_TYPE]

    return collection


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print "usage : %s csv-file" % (sys.argv[0])
        sys.exit(0)

    collection = ConvertCSVFile(sys.argv[1])
    
    collection.PrintStat()

#    collection.DeleteDataFromDatabase(collection.device_serial_number)
    collection.AddDataToDatabase()

