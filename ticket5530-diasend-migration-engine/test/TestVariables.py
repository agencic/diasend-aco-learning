# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import base64
import sys
import os
import Database #from parent directory
global header
global responses
global data
global tail
global post_header_delay
global protocol
global compresssion
global functionBeforeTestrun
global functionAfterTestrun
global data_directory
global running_test_case
global running_test_case_data_file
global cookie

global latest_sw_ver

last_insert_id = 0

def init():
    global header
    global responses
    global data
    global tail
    global latest_sw_ver
    global post_header_delay
    global protocol
    global compression
    global functionBeforeTestrun
    global functionAfterTestrun
    global data_directory
    global last_insert_id
    global cookie

    last_insert_id = 0
    header            = None
    responses         = None
    data              = None
    tail              = "!TRANSFER!EOT!DIASEND"
    latest_sw_ver     = Database.CheckLatestOldTransmitterVersion()
    if not latest_sw_ver:
        latest_sw_ver = 'R5b'
    
    post_header_delay = 0
    protocol          = None
    compression       = None
    functionBeforeTestrun  = None
    functionAfterTestrun = None
    data_directory = None

    cookie = ""

def SetRunningTestCase(result_dict):
    """
    Store the name of the current test case (file) and if available data file. 
    """
    global running_test_case
    global running_test_case_data_file
    
    if result_dict.has_key('testcase'):
        running_test_case = result_dict['testcase']
    else:
        running_test_case = ''
        
    if result_dict.has_key('data_file'):
        running_test_case_data_file = result_dict['data_file']
    else:
        running_test_case_data_file = ''

class UserInfoTag: 
    __user_firstname  = "" 
    __user_lastname   = "" 
    __user_pcode      = "" 
    __user_password   = "" 
    __user_action     = ""

    def __init__(self, pcode, first_name, last_name, password, action):

        self.__user_pcode = base64.b64encode(pcode)
        self.__user_firstname = base64.b64encode(first_name)
        self.__user_lastname = base64.b64encode(last_name)
        self.__user_password = base64.b64encode(password)
        self.__user_action = action

    def getTag(self):
        tag = "!HDR!USERINFO!FIRSTNAME!"+self.__user_firstname+"!LASTNAME!"+self.__user_lastname+"!PCODE!"+ \
                self.__user_pcode+"!PW!"+self.__user_password+"!ACTION!"+self.__user_action+"\r\n"
        return tag


def PerformDatabaseQuery(sqlQ, paramQ):
    """
    Perform a database query and return the result
    """
    
    global last_insert_id
    
    if not Database.IsDatabaseConnected():
         Database.ConnectToDatabase()

    retval = Database.PerformQuery( sqlQ, paramQ, True )
    last_insert_id = Database.GetLastInsertID()

    Database.DisconnectFromDatabase()
    
    return retval

def GetLastInsertId():
    global last_insert_id
    
    return last_insert_id

def default_header(device_type, terminal = 'S6019999999991', interface_name = None, swid="A06102 P2a", location="",is_clinic_terminal=False, userinfo=None):
    """
    Returns a working header, device type must be given
    """

    if protocol == 3:
        # TODO : Fix hard-coded terminal serial
        header = (
            "!HDR!DIASEND\r\n"
            "!HDR!PROTVER!3.0\r\n"
            "!HDR!SWID!A10024\r\n"
            "!HDR!SERNO!789f90cc-04bb-11e7-a48d-56847afe9799\r\n"
            "!HDR!METERTYPE!" + device_type + "\r\n"
            "!HDR!INTERFACE!device\r\n"
            "!HDR!LOCATION!US\r\n"
            "!HDR!OS!Windows 8\r\n"
            "!HDR!SWVER!3.0.0\r\n"
            "!HDR!USERINFO!\r\n"    
         )

        if len(cookie) > 0:
            header += '!HDR!COOKIE!%s\r\n' % (cookie)
    else:
        header = (
            "!HDR!DIASEND\r\n"
            "!HDR!PROTVER!2.0\r\n"
            "!HDR!HWID!A06101 R1b\r\n"
            "!HDR!SWID!" + swid + "\r\n"
            "!HDR!BLID!A06105 P2a\r\n"
            "!HDR!SERNO!" + terminal + "\r\n"
            "!HDR!ENCRYPTION!ENABLED\r\n"
            "!HDR!METERTYPE!" + device_type + "\r\n"
        )

    if interface_name:
            header = header + "!HDR!INTERFACE!" + interface_name + "\r\n"
        
    if location != "":
        header += "!HDR!LOCATION!"+location+"\r\n"

    if is_clinic_terminal == True:
        header += "!HDR!CLINIC!YES\r\n"

    if userinfo != None:
        header += userinfo.getTag()

    if protocol == 3:
        header += "!HDR!COMPRESSION!DISABLED\r\n" + \
                  "!HDR!ENCRYPTION!DISABLED\r\n"
    elif compression:
        header += "!HDR!COMPRESSION!ENABLED!%s\r\n" % compression
        
    header = header + "!HDR!METERDATA\r\n"
    
    return header

def setProtocolVersion(protver):
    global protocol

    protocol = protver

def setCompression(comp):
    global compression

    compression = comp

def setCookie(fresh_cookie):
    global cookie

    cookie = fresh_cookie
