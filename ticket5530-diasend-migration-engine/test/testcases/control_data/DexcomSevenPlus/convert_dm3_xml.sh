#!/bin/bash
#
# Use this script to convert Dexcom DM3 xml files to something that is easily parsed. 
# 
echo "TERMINAL_SERIAL,,S100010100,,"
sed -e 's/        <Sensor DisplayTime=\"\([0-9\-]*\) \([0-9:\.]*\)\" InternalTime=\"[0-9\-]* [0-9:\.]*\" Value=\"\([0-9]*\)\".*/GLUCOSE,\1 \2,\3,FLAG_CONTINOUS_READING/' \
    -e 's/        <Meter DisplayTime=\"\([0-9\-]*\) \([0-9:\.]*\)\" InternalTime=\"[0-9\-]* [0-9:\.]*\" Value=\"\([0-9]*\)\".*/GLUCOSE,\1 \2,\3,FLAG_MANUAL;FLAG_CALIBRATION;FLAG_CONTINOUS_READING/' \
    -e 's/        <Event DisplayTime=\"\([0-9\-]*\) \([0-9:\.]*\)\" InternalTime=\"[0-9\-]* [0-9:\.]*\" EventType=\"Carbs\" Decription=\"\([0-9]*\)\".*/CARBS,\1 \2,\3,,/' \
    -e 's/        <Event DisplayTime=\"\([0-9\-]*\) \([0-9:\.]*\)\" InternalTime=\"[0-9\-]* [0-9:\.]*\" EventType=\"Insulin\" Decription=\"\([0-9]*\)\".*/INSULIN,\1 \2,\3,,/' \
    -e 's/.<Patient Id=\"{[0-9A-Z\-]*}\" FirstName=\"[A-Za-z0-9]*\" LastName=\"[A-Za-z0-9]*\" MiddleName=\"[A-Za-z0-9]*\" Initials=\"[A-Za-z0-9]*\" DisplayName=\"[A-Za-z0-9]*\" PatientNumber=\"[A-Za-z0-9]*\" ReceiverNumber=\"\([A-Za-z0-9]*\)\".*/DEVICE_SERIAL,,\1,,/' \
    -e '    /.*<.*/ d' $1
