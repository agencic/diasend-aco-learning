# 2011-04-16

These files with control data has been extracted using the Dexcom DM3 software (export as XML). The XML files have the following "problems"  :

 * Each entry records an internal time that we don't store in the database. 
 * For meter records - no information regarding flags (manual/onetouch, calibration)
 
