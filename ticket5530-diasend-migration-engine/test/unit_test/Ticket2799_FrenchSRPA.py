# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# A unit test for validating ticket p624:#2799. A French SRPA should always need to 
# register the device (pump) regardless of previous device registration. 
# 
# This test requires the regression database to work (see 
# unit_test_regression_database.sql).
# 

from BackendCommunication import BackendCommunication

passed = True

data = open('../testcases/test_data/AnimasPacked/AnimasVibePacked.bin', 'r').read()

bc = BackendCommunication('localhost', 50010)
bc.location = 'US'

bc.transfer_without_serial_number('AnimasIR', data)

if bc.transfer('AnimasIR', data):
    passed = False
    print 'NAK expected (1)'
 
if not bc.transfer_with_credentials('AnimasIR', data, 'french.srpa@diasend.com', '11111111'):
    passed = False
    print 'OK expected (2)'

if bc.transfer('AnimasIR', data):
    passed = False
    print 'NAK expected (3)'

if not bc.transfer_with_credentials('AnimasIR', data, 'french.srpa@diasend.com', '11111111'):
    passed = False
    print 'OK expected (4)'

data = open('../testcases/test_data/AnimasPacked/Animas2020Packed.bin', 'r').read()

if not bc.transfer_with_credentials('AnimasIR', data, 'swedish.srpa@diasend.com', '11111111'):
    passed = False
    print 'OK expected (5)'

if not bc.transfer('AnimasIR', data):
    passed = False
    print 'OK expected (6)'

if not bc.transfer_with_credentials('AnimasIR', data, 'swedish.srpa@diasend.com', '11111111'):
    passed = False
    print 'OK expected (7)'

if passed:
    print 'Ok'
else:
    print 'Failed'