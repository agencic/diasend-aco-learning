# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import socket
import base64
import crc
import ssl
import os
import sys

class BackendCommunication(object):
    '''
    Class for backend communication - simulates an Uploader. 
    '''

    def __init__(self, host, port, protocol_version=3):
        self.host             = host
        self.port             = port
        self.protocol_version = protocol_version

        self.location        = ''
        self.clinic          = False
        self.terminal_serial = ''

    def chunks(self, s, n):
        for start in range(0, len(s), n):
            yield s[start:start+n]

    def _transfer_to_backend(self, device_type, data, username, password):

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host, self.port))
       
        s.send("!STARTTLS!EOT!DIASEND")
        response = s.recv(1024)
        
        s = ssl.wrap_socket(s,
            ca_certs     = os.path.join(sys.path[0], "../ssl-keys/cert.pem"),
            cert_reqs    = ssl.CERT_NONE,
            server_side  = False,
            ssl_version  = ssl.PROTOCOL_TLSv1)
        
        header = ('!HDR!DIASEND\r\n' 
                  '!HDR!PROTVER!3.0\r\n'
                  '!HDR!SWID!A10024\r\n'
                  '!HDR!SERNO!%s\r\n'
                  '!HDR!COMPRESSION!DISABLED\r\n'
                  '!HDR!ENCRYPTION!DISABLED\r\n'
                  '!HDR!INTERFACE!/dev/ttyusbserial\r\n'
                  '!HDR!LOCATION!%s\r\n'
                  '!HDR!METERTYPE!%s\r\n!HDR!METERDATA\r\n') % (self.terminal_serial, self.location, device_type)
       
        if self.clinic:
            header += '!HDR!CLINIC!YES\r\n'
        else:
            header += '!HDR!CLINIC!NO\r\n'
        
        if len(username) > 0:
            header += '!HDR!USERINFO!FIRSTNAME!!LASTNAME!!PCODE!%s!PW!%s!ACTION!LOGIN\r\n' % (
                base64.b64encode(username), base64.b64encode(password))
        else:
            header += '!HDR!USERINFO\r\n'
    
        header += '!%04x!%04x!BLOCK!EOT!DIASEND' % (len(header), crc.calculate_crc(header))
        s.send(header)
        response = s.recv(1024)
        if not "!OK" in response:
            return (False, "Did not receive !OK on header")
        
        for chunk in self.chunks(data, 3000):
            chunk += '!%04x!%04x!BLOCK!EOT!DIASEND' % (len(chunk), crc.calculate_crc(chunk))
            s.send(chunk)
            response = s.recv(1024)
            if not "!OK" in response:
                return (False, "Did not receive !OK on chunk")
   
        tail = '!0000!0000!00!TRANSFER!EOT!DIASEND'
        s.send(tail)
        response = s.recv(1024)

        s.close()

        return response

    def transfer_without_serial_number(self, device_type, data):
        '''
        Send request to backend with empty serialnumber. Backend should
        respond NAK with a newly created serial number.
        '''
        self.terminal_serial = ''
        response = self._transfer_to_backend('AnimasIR', data, '', '')
        response_bits = response.split('!')
        if len(response_bits) != 4:
            return False
        if response_bits[1] == 'NAK' and response_bits[2] == '0010':
            self.terminal_serial = response_bits[3]
            return True
        return False

    def transfer_with_credentials(self, device_type, data, username, password):
        '''
        Transfer data to the backend with login information.
        '''
        response = self._transfer_to_backend(device_type, data, username, password)
        return 'OK' in response

    def transfer(self, device_type, data):
        '''
        Transfer data to backend without any user information. The device
        (that the data belongs to) must previously be registered on a 
        user/clinic that is allowed to transfer data using the Uploader).
        '''
        response = self._transfer_to_backend(device_type, data, '', '')
        return 'OK' in response
