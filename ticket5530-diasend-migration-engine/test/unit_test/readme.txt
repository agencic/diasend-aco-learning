This is a start (?) for unit tests that are not pure data transmissions. These 
tests requires a regression database in place for them to work. This is implemented
as the mysql database dump available in this directory. Create a new database and 
import one of the templates available in article A06110. Then import the
unit_test_regression.sql file that adds the necessary users / clinics etc for the 
unit tests available in this directory. 