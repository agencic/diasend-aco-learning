# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -------------------

# This is an unordinary testcase - it is interpreted and run by the TestcaseRunner, but lacks most data.
# We use it to modify the database. It is possible for "setup-cases" to communicate with the backend
# just as testcases do if necessary.

def beforeFunc():

    import os

    if "DEVICE" in os.environ.keys():
        DEVICE_TO_REMOVE = os.environ["DEVICE"]
    else:
        DEVICE_TO_REMOVE="CWDD29Z8600258"

    # Get device sequence number.
    if TestVariables.PerformDatabaseQuery("DELETE FROM device_ownership WHERE device_id in (SELECT seq FROM devices WHERE serialnumber=%s)", (DEVICE_TO_REMOVE)) == False:
        return False

    return True

TestVariables.functionBeforeTestrun = beforeFunc
TestVariables.responses=['!NAK']  #Since we have no valid header and no data we will be NAKed. OK!

