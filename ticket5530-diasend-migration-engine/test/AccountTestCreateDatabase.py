# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright(C 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import MySQLdb
import inspect

connection = None

#default_host = Config.settings["database-address"]
#default_passwd = Config.settings["database-password"]
#default_user = Config.settings["database-user"]
#default_port = int(Config.settings["database-port"])
#default_db = Config.settings["database-db"]

#host   = default_host
#user   = default_user
#passwd = default_passwd
#port   = default_port
#db     = default_db

def ConnectToDatabase():
    host        = "localhost"
    port        = 3306
    db          = "diasend"
    user        = "root"
    passwd      = ""
    try:
        connection = MySQLdb.connect( host=host, user=user, passwd=passwd, port=port, db=db )
    except MySQLdb.MySQLError, e:
        print "Connect to database error: ", repr(e)

    print connection

    return connection

def PerformQuery(connection, query, param):
    result = ()
    try:
        cursor = connection.cursor()
        cursor.execute(query, param)
        result = cursor.fetchall()
        cursor.close()
        
    except MySQLdb.MySQLError, e:
        print e
        
    return result

def BuildName(l):
    res = ''
    for item in l:
        if item:
            res += 'Y'
        else:
            res += 'N'
    return res

def CreateAdmin(connection, prefix, has_dda_access, hide_pii):
    name = '%s_adm_%s' % (prefix, BuildName([has_dda_access, hide_pii]))

    query = 'INSERT INTO admins (name, password, title, phone, support, misc) VALUES (%s, %s, "", "", "", "")'
    param = (name, name) 

    PerformQuery(connection, query, param)

    if has_dda_access:
        query = 'INSERT INTO admin_properties (admin, property, value) VALUES (%s, "allow_dda", "1")' 
        param = (name) 
        PerformQuery(connection, query, param)

    if hide_pii:
        query = 'INSERT INTO admin_properties (admin, property, value) VALUES (%s, "hide_pii", "1")' 
        param = (name) 
        PerformQuery(connection, query, param)

    return name

def CreateTerminal(connection, prefix, admin):
    name = 'DDA-DIAS-%s-%s' % (prefix, admin)

    query = 'INSERT INTO terminals (terminal_id, admin, terminalclass) VALUES (%s, %s, "terminal_dda")'
    param = (name, admin)

    PerformQuery(connection, query, param)

    query = 'INSERT INTO terminal_permissions VALUES (%s, "device_pump")'
    param = (name)

    PerformQuery(connection, query, param)

    # Todo : terminal permissions

    return name

def CreateClinicUser(connection, prefix, admin):
    name = "%s_clinic_user_%s" % (prefix, admin)

    query = 'INSERT INTO clinic_users (username, password, firstname, lastname, admin) VALUES (%s, %s, "first", "last", %s)'
    param = (name, name, admin)

    PerformQuery(connection, query, param)

    return name

def CreateUser(connection, prefix, has_file_access, has_file_access_by_clinic, active, french):

    name = '%s_user_%s' % (prefix, BuildName([has_file_access, has_file_access_by_clinic, active, french]))

    if active:
        query = 'INSERT INTO users (pcode, pnumber, password, archived) VALUES (%s, "1234", %s, "No")'
    else:
        query = 'INSERT INTO users (pcode, pnumber, password, archived) VALUES (%s, "1234", %s, "Yes")'

    param = (name, name)
    
    PerformQuery(connection, query, param)

    if has_file_access:
        query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "fileupload_srpa", "1")'
        param = (name)
        PerformQuery(connection, query, param)

    if has_file_access_by_clinic:
        query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "fileupload_admin", "1")'
        param = (name)
        PerformQuery(connection, query, param)
    
    if french:
        query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "country", "109")'
        param = (name)
        PerformQuery(connection, query, param)
        
    query = 'INSERT INTO user_properties (pcode, property, value) VALUES (%s, "account_expire", "2630573273")'
    param = (name)
    PerformQuery(connection, query, param)

    return name

# ---

prefix = "t01"

connection = ConnectToDatabase()
admins = []
admins.append(CreateAdmin(connection, prefix, False, False))
admins.append(CreateAdmin(connection, prefix, True, False))
admins.append(CreateAdmin(connection, prefix, False, True))
admins.append(CreateAdmin(connection, prefix, True, True))

for admin in admins:
    terminals = []
    terminals.append(CreateTerminal(connection, prefix, admin))
    clinic_users = []
    clinic_users.append(CreateClinicUser(connection, prefix, admin))

users = []
users.append(CreateUser(connection, prefix, False, False, False, False))
users.append(CreateUser(connection, prefix, False, False, False, True))
users.append(CreateUser(connection, prefix, False, False, True, False))
users.append(CreateUser(connection, prefix, False, False, True, True))
users.append(CreateUser(connection, prefix, False, True, False, False))
users.append(CreateUser(connection, prefix, False, True, False, True))
users.append(CreateUser(connection, prefix, False, True, True, False))
users.append(CreateUser(connection, prefix, False, True, True, True))
users.append(CreateUser(connection, prefix, True, False, False, False))
users.append(CreateUser(connection, prefix, True, False, False, True))
users.append(CreateUser(connection, prefix, True, False, True, False))
users.append(CreateUser(connection, prefix, True, False, True, True))
users.append(CreateUser(connection, prefix, True, True, False, False))
users.append(CreateUser(connection, prefix, True, True, False, True))
users.append(CreateUser(connection, prefix, True, True, True, False))
users.append(CreateUser(connection, prefix, True, True, True, True))

connection.commit()
connection.close()

