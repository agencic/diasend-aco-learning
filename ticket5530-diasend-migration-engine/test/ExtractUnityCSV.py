# -*- coding: utf8 -*-
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Extract Unity control data from csv-file.

import csv
import sys
import datetime
import os
from decimal import *
from Defines import *
from PumpAnimasIR import GetAlarmCode

from DeviceDataCollection import DeviceDataCollection

# Item types according to the Unity History CSV format
ITEM_TYPE_ALARM         = 1
ITEM_TYPE_PRIME         = 2
ITEM_TYPE_SUSPEND       = 3
ITEM_TYPE_TDD           = 4
ITEM_TYPE_BASAL         = 5
ITEM_TYPE_BOLUS         = 6
ITEM_TYPE_OTPINGBG      = 7
ITEM_TYPE_CGM_WARNING   = 8
ITEM_TYPE_CGM_DATA      = 9
ITEM_TYPE_BG_CAL        = 10

# Reference to the last CGM value inserted into the DeviceCollection. Used for adding 
# a stop session flag later (when thes top session event occurs).
last_cgm_item = None

# Set when a start session event is detected. Indicates that the next CGM values should 
# have a start session flag added. 
mark_next_cgm_as_start_session = False

def ConvertItemAlarm(collection, item):
    """
    ['1', '30', '2/10/2010', '4:01', '1', '30', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
    Type,Pump Rec,Date,Time,Code,Data,,,,,,,,,,,,,,,,,
    """

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_CODE = 4
    ITEM_DATA = 5

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    flags = []

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    # The alarm is in the ITEM_CODE column and has to be converted
    # to the codes used in the backend.
    alarm_code = GetAlarmCode(int(item[ITEM_CODE]))

    collection.AddItem(VALUE_TYPE_ALARM, dt, alarm_code, flags)

def ConvertItemPrime(collection, item):
    """
    Type,Pump Rec,Date,Time,Vol,Flags,0=Blank..1=NotPrimed..2=Primed..3=Cannula,,,,,,,,,,,,,,,,
    2,60,1/30/2010,4:11,10,2,,,,,,,,,,,,,,,,,
    """

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_VOL = 4
    ITEM_FLAG = 5

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    flags = []

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    volume = int(Decimal(item[ITEM_VOL]) * 100)

    # Convert flag to backend codes.
    if int(item[ITEM_FLAG]) == 1:
        flags.append(FLAG_NOT_PRIMED)
    elif int(item[ITEM_FLAG]) == 2:
        flags.append(FLAG_PRIMED)
    elif int(item[ITEM_FLAG]) == 3:
        flags.append(FLAG_CANNULA_BOLUS)

    collection.AddItem(VALUE_TYPE_INS_PRIME, dt, volume, flags)

def ConvertItemTDD(collection, item):
    """
    **TDD,,,,,,,,,,,,,,,,,,,,,,
    NOTE: Record #120 will automatically be replaced with a TDD record when the pump is powered up after programming the s-record history.,,,,,,,,,,,,,,,,,,,,,,
    "           So, the pump will only contain records 1-119 below.",,,,,,,,,,,,,,,,,,,,,,
    Type,Pump Rec,Date,Time,Total,Basal,Flag,1=TempBasal..2=SuspendedToday,,,,,,,,,,,,,,,
    4,120,10/20/2009,4:41,36,3,3,,,,,,,,,,,,,,,,
    4,119,10/21/2009,8:42,39,3.3,1,,,,,,,,,,,,,,,,
    """

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_TOTAL = 4
    ITEM_BASAL = 5
    ITEM_FLAG = 6

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    flags = []

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    total = int(Decimal(item[ITEM_TOTAL]) * 10000)
    basal = int(Decimal(item[ITEM_BASAL]) * 10000)

    sec_values = {VALUE_TYPE_INS_BASAL_TDD:basal}

    flag_raw = int(item[ITEM_FLAG])

    if flag_raw & 0x01:
        flags.append(FLAG_TEMP_MODIFIED)
    if flag_raw & 0x02:
        flags.append(FLAG_SUSPENDED)

    collection.AddItem(VALUE_TYPE_INS_TDD, dt, total, flags, sec_values)

def ConvertItemBasal(collection, item):
    """
    **Basal,,,,,,,,,,,,,,,,,,,,,,
    NOTE: Record #270 will automatically be replaced with a basal record when the pump is powered up after programming the s-record history.,,,,,,,,,,,,,,,,,,,,,,
    "           So, the pump will only contain records 1-269 below.",,,,,,,,,,,,,,,,,,,,,,
    Type,Pump Rec,Date,Time,Rate,Temp,1=TempBasalActive,,,,,,,,,,,,,,,,
    5,270,12/24/2009,20:35,13,0,,,,,,,,,,,,,,,,,
    5,269,12/25/2009,4:31,14,0,,,,,,,,,,,,,,,,,
    """

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_RATE = 4
    ITEM_TEMP = 5

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    rate = int(item[ITEM_RATE]) * 1000

    flags = []

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    if int(item[ITEM_TEMP]) & 0x01:
        flags.append(FLAG_TEMP_MODIFIED)

    collection.AddItem(VALUE_TYPE_INS_BASAL, dt, rate, flags)

def ConvertItemBolus(collection, item):
    """
    **Bolus,,,,,,TYPE: 0=None..1=Normal..2=Audio..3=Combo,,,,,,,,,,,,,,,,
    **Bolus,,,,,,,,STATUS: 0=Inactive..1=Active..2=Canceled..3=Completed,,,,,,,,,,,,,,
    **Bolus,,,,,,,,,TRIGGERED: 0=Pump..1=Meter,,,,,,,,,,,,,
    **Bolus,,,,,,,,,,CANCELED: 0=Pump..1=Meter,,,,,,,,,,,,
    **Bolus,,,,,,,,,,,BOLUS: 0=Neither..1=ezBG..2=ezCarb..3=ezCarb,,,,,,,UOM: 0=mg/dL..1=mmol/L,,,,
    **Bolus,,,,,,,,,,,,,,,,,,,IOB Enabled: 0=Disabled..1=Enabled,,,
    **Bolus,,,,,,,,,,,,,,,,,,,,BG Correction: 0=No Corr..1=Corr added,,
    Type,Pump Rec,Date,Time,Attempted,Actual,duration,Type,Status,Triggered,Canceled,ezBG/Carb, I:C, Carbs, ISF, BG Act,BG Trg,BG Dlt,BG UOM, IOB, Corr, IOB Dur., IOB value
    6,500,1/29/2010,13:55,13,13,1.5,3,3,0,0,2,20,200,50,80,120,10,0,0,1,1.5,0
    6,499,1/29/2010,14:46,14,14,0,1,3,0,0,2,20,201,50,81,120,10,0,0,1,2,0
    6,498,1/29/2010,15:37,15,15,0,1,3,0,0,2,20,202,50,82,120,10,0,0,1,2.5,0
    """

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_ATTEMPTED = 4
    ITEM_ACTUAL = 5
    ITEM_DURATION = 6
    ITEM_TYPE = 7
    ITEM_STATUS = 8
    ITEM_TRIGGERED = 9
    ITEM_CANCELLED = 10
    ITEM_EZBG_CARB = 11
    ITEM_I_C = 12
    ITEM_CARBS = 13
    ITEM_ISF = 14
    ITEM_BG_ACTUAL = 15
    ITEM_BG_TARGET = 16
    ITEM_BG_DELTA = 17
    ITEM_BG_UOM = 18
    ITEM_IOB = 19
    ITEM_BG_CORRECTION = 20
    ITEM_IOB_DURATION = 21
    ITEM_IOB_VALUE = 22
    ITEM_SUGGESTED_BOLUS = 25

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    # Add BOLUS value
    bolus = int(Decimal(item[ITEM_ATTEMPTED]) * 10000)
    flags = []
    sec_values = {}

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    status = int(item[ITEM_STATUS])
    if status == 0:
        flags.append(FLAG_BOLUS_INACTIVE)
    elif status == 1:
        flags.append(FLAG_BOLUS_ACTIVE)
    elif status == 2:
        flags.append(FLAG_CANCELED)
    elif status == 3:
        flags.append(FLAG_BOLUS_COMPLETED)

    remote = int(item[ITEM_TRIGGERED])
    if remote == 1:
        flags.append(FLAG_BOLUS_REMOTE_INITIATED)

    item_type = int(Decimal(item[ITEM_TYPE]))
    if item_type == 2:
        flags.append(FLAG_BOLUS_TYPE_AUDIO)
    elif item_type == 3:
        flags.append(FLAG_BOLUS_TYPE_COMBO)

    ezbg_carb = int(item[ITEM_EZBG_CARB])
    if ezbg_carb == 1:
        flags.append(FLAG_BOLUS_TYPE_EZBG)
    elif ezbg_carb in [2,3]:
        flags.append(FLAG_BOLUS_TYPE_EZCARB)

    duration = int(Decimal(item[ITEM_DURATION]) * 60)
    if duration != 0:
        sec_values[VALUE_TYPE_DURATION] = duration;

    try:
        if item[ITEM_SUGGESTED_BOLUS] != None:
            suggested_bolus = int(Decimal(item[ITEM_SUGGESTED_BOLUS]) * 10000)
            if suggested_bolus != 0:
                sec_values[VALUE_TYPE_INS_BOLUS_SUGGESTED] = suggested_bolus
    except IndexError:
        pass    # No such column in this file

    collection.AddItem(VALUE_TYPE_INS_BOLUS, dt, bolus, flags, sec_values)

    # Add CARB value
    flags = []

    ezbg_carb = int(item[ITEM_EZBG_CARB])
    if ezbg_carb == 1:
        flags.append(FLAG_BOLUS_TYPE_EZBG)
    elif ezbg_carb in [2,3]:
        flags.append(FLAG_BOLUS_TYPE_EZCARB)

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    bolus = int(item[ITEM_EZBG_CARB])
    if bolus in [2,3]:
        carb = int(Decimal(item[ITEM_CARBS]))
        collection.AddItem(VALUE_TYPE_CARBS, dt, carb, flags)

    # Add Glucose value
    flags = []

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    if remote == 1:
        flags.append(FLAG_METER_REMOTE)
    
    bolus = int(item[ITEM_EZBG_CARB])
    bg_corr = int(item[ITEM_BG_CORRECTION])
    if (bolus == 1) or (bolus == 2 and bg_corr == 1):
        value = round(int(item[ITEM_BG_ACTUAL]) * 1000 / 18.0)
        flags.append(FLAG_MANUAL)
        collection.AddItem(VALUE_TYPE_GLUCOSE, dt, value, flags)

def ConvertItemCGMData(collection, item):
    """
    **UNITY_CGM_DATA,,,,"CGM Values: 2=Transmitter out of range, 4=Warm up period, 5=Calibration required",,,,,,,,,,,,,,,,,,
    **UNITY_CGM_DATA,,,,,Time adjust flag[*1024(4xx)]: 0=none..1=accumulated adjust time record before time lost,,,,,,,,,,,,,,,,,
    **UNITY_CGM_DATA,,,,,,BG Cal[*2048(8xx)]: 0=no..1=BG cal record,,,,,,,,,,,,,,,,
    **UNITY_CGM_DATA,,,,,,,Time Changed[*4096(1xxx)]: 0=no..1=Time Changed,,,,,,,,,,,,,,,
    **UNITY_CGM_DATA,,,,,,,,Time Lost[*8192(2xxx)]: 0=no..1=Time Lost,,,,,,,,,,,,,,
    **UNITY_CGM_DATA,,,,,,,,,Stop Session[*16384(4xxx)]: 0=no..1=Session stopped,,,,,,,,,,,,,
    **UNITY_CGM_DATA,,,,,,,,,,Start Session[*32768(8xxx)]: 0=no..1=Session Started,,,,,,,,,,,,
    **UNITY_CGM_DATA,,4 BYTES--------------,,2 BYTES----------------------------------------------------------------------------,,,,,,,,,,,,,,,,,,
    Type,Pump Rec,Date,Time,CGM Value,Adjust,BG cal,Time Chng,Time Lost,Stp Sess,Strt Sess,,,,,,,,,,,,
    ['9', '34', '2/16/2010', '20:44:53', '130', '0', '0', '0', '0', '0', '0', '', '', '2/16/2010', '20:44', '', '', '', '', '', '', '', '']
    """

    global last_cgm_item
    global mark_next_cgm_as_start_session

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_CGM_VALUE = 4
    ITEM_ADJUST = 5
    ITEM_BG_CAL = 6
    ITEM_TIME_CHANGE = 7
    ITEM_TIME_LOST = 8
    ITEM_STOP_SESSION = 9
    ITEM_START_SESSION = 10
    # This item is added by me Anders and is not part of the original control data. It is a
    # column with the PUMP TOD (Time-On-Display). 
    ITEM_TOD_TIME = 11

    flags = []

    # ITEM_CGM_VALUE that we should ignore
    # 2 = transmitter out of range
    # 4 = warm up period
    # 5 = calibration required
    if int(item[ITEM_CGM_VALUE]) in [2, 4, 5]:
        return

    if int(item[ITEM_BG_CAL]) == 1:
        flags.append(FLAG_CALIBRATION)

    if int(item[ITEM_STOP_SESSION]) == 1:
        if last_cgm_item != None:
            last_cgm_item.flags.append(FLAG_LAST_GLUCOSE_VALUE_IN_GROUP)
        return
    elif int(item[ITEM_START_SESSION]) == 1:
        mark_next_cgm_as_start_session = True
        return

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    try:
        # First we try to use the extra column with Pump TOD time inserted.
        dt = datetime.datetime.strptime(item[ITEM_TOD_TIME], "%m/%d/%Y %H:%M")
    except ValueError:
        try:
            # If that isn't available we use the CGM time. This works for the
            # Unity1 test case.
            dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M:%S")
        except ValueError:
            # Some csv-files have had no seconds.
            dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    raw_value = int(item[ITEM_CGM_VALUE])

    # If in the range 0-5 we have no real glucose values (these are events 
    # such as warmup).
    if raw_value > 5:
        value = round(int(item[ITEM_CGM_VALUE]) * 1000 / 18.0)
        flags.append(FLAG_CONTINOUS_READING)
        
        if mark_next_cgm_as_start_session:
            mark_next_cgm_as_start_session = False
            flags.append(FLAG_FIRST_GLUCOSE_VALUE_IN_GROUP)
        
        last_cgm_item = collection.AddItem(VALUE_TYPE_GLUCOSE, dt, value, flags)

def ConvertItemCGMWarning(collection, item):
    """
    **UNITY_CGM_WARN,,,,,,,,,,,,,,,,,,,,,,
    "*NOTE: only these are stored in pump history for CGM:  201,202,203,205,206,208,211,212,213,214,215,216,217,218,221,223",,,,,,,,,,,,,,,,,,,,,,
    Type,Pump Rec,Date,Time,Warning,Data (9 bytes),,,,,,,,,,,,,,,,,
    8,447,11/18/2009,1:25,201,447,,,,,,,,,,,,,,,,,
    8,446,11/19/2009,3:50,202,446,,,,,,,,,,,,,,,,,
    8,445,11/19/2009,6:15,203,445,,,,,,,,,,,,,,,,,
    """

    ITEM_TYPE = 0
    ITEM_PUMP_REC = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_WARNING = 4
    ITEM_DATA = 5

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    flags = []

    if datetime.datetime.now() < dt:
        flags.append(FLAG_TIMESTAMP_IN_FUTURE)

    warning_code = ALARM_CGM_EAW_BASE + int(item[ITEM_WARNING])

    collection.AddItem(VALUE_TYPE_ALARM, dt, warning_code, flags)
            
def ConvertOTPingBG(collection, item):
    """
    **OTPING_BG (Max = 1000),,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
    **OTPING_BG,,,,,,,TYPE[*1024(4xx)]: 0=blood..1=control,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
    **OTPING_BG,,,,,,,,SITE[*2048(8xx)]: 0=normal..1=alternate,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
    **OTPING_BG,,,,,,,,,MEAL[*4096(1xxx)]: 0=none..1=BfrBkfst..2=AftBkfst..3=BfrLnch..4=AftLnch..5=BfrDnr..6=AftDnr..7=Nite,,,,,,,,,,,,,,,,,,,,,,,,,,,,
    **OTPING_BG,,,,,,,,,,EXERCISE[*65536(1xxxx)]: 0=none..1=BfrExcr..2=DurExcr..3=AftExcr,,,,,,,,,,,,,,,,,,,,,,,,,,,
    **OTPING_BG,,,,,,,,,,,HEALTH[*262144(4xxxx)]: 0=stress..1=FeelHypo..2=Sick..3=Menses..4=Vac..5=Other,,,,,,,,,,,,,,,,,,,,,,,,,,
    **OTPING_BG,,,,3 BYTES--------------,,3 BYTES-----------------------------------------------------------------------,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
    Type,Index,Date,Time,Meter Date,Meter Time,Val(10bits),Type(1bit),Site(1bit),Meal(4bits),Exer(2bits),Health(6bits),,,,,,,,,,,,,,,,,,,,,,,,,,
    7,1000,1/31/2010,12:15,1/31/2010,12:14,590,0,0,1,1,0,,,,,,,,,,,,,,,,,,,,,,,,,,
    7,999,1/31/2010,13:20,1/31/2010,13:18,570,0,0,2,2,1,,,,,,,,,,,,,,,,,,,,,,,,,,
    7,998,1/31/2010,14:25,1/31/2010,14:23,560,0,0,3,3,2,,,,,,,,,,,,,,,,,,,,,,,,,,
    7,997,1/31/2010,15:30,1/31/2010,15:28,540,0,0,4,0,3,,,,,,,,,,,,,,,,,,,,,,,,,,
    7,996,1/31/2010,16:34,1/31/2010,16:33,500,0,0,5,1,4,,,,,,,,,,,,,,,,,,,,,,,,,,
    """
    
    ITEM_TYPE = 0
    ITEM_TYPE_INDEX = 1
    ITEM_TYPE_DATE = 2
    ITEM_TYPE_TIME = 3
    ITEM_TYPE_METER_DATE = 4
    ITEM_TYPE_METER_TIME = 5
    ITEM_TYPE_VALUE = 6
    ITEM_TYPE_TYPE = 7
    ITEM_TYPE_SITE = 8
    ITEM_TYPE_MEAL = 9
    ITEM_TYPE_EXERCISE = 10
    ITEM_TYPE_HEALTH = 11
      
    item_value      = int(round((Decimal(item[ITEM_TYPE_VALUE]) / Decimal(18))*1000))
    item_type       = int(Decimal(item[ITEM_TYPE_TYPE]))
    item_site       = int(Decimal(item[ITEM_TYPE_SITE]))
    item_meal       = int(Decimal(item[ITEM_TYPE_MEAL]))
    item_exersice   = int(Decimal(item[ITEM_TYPE_EXERCISE]))
    item_health     = int(Decimal(item[ITEM_TYPE_HEALTH]))
    
    dt_str = "%s %s" % (item[ITEM_TYPE_METER_DATE], item[ITEM_TYPE_METER_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    flags = []

    # This type is always remote
    flags.append(FLAG_METER_REMOTE)
    
    # Type 
    flag = {0:None, 1:FLAG_RESULT_CTRL}[item_type]
    if flag != None:
        flags.append(flag)

    # Site
    flag = {0:None, 1:FLAG_RESULT_ALT_TESTSITE}[item_site]
    if flag != None:
        flags.append(flag)

    # Meal
    flag = {0:None, 1:FLAG_BEFORE_BREAKFAST, 2:FLAG_AFTER_BREAKFAST, 3:FLAG_BEFORE_LUNCH, 4:FLAG_AFTER_LUNCH, 5:FLAG_BEFORE_DINNER, 6:FLAG_AFTER_DINNER, 7:FLAG_NIGHT}[item_meal]
    if flag != None:
        flags.append(flag)
    
    # Exercise
    flag = {0:None, 1:FLAG_BEFORE_EXERCISE, 2:FLAG_DURING_EXERCISE, 3:FLAG_AFTER_EXERCISE}[item_exersice]
    if flag != None:
        flags.append(flag)

    # Health
    health_flags = {1:FLAG_STRESS, 2:FLAG_FEEL_HYPO, 4:FLAG_ILLNESS, 8:FLAG_MENSES, 16:FLAG_VACATION, 32:FLAG_OTHER}
    for bit_value in [1,2,4,8,16,32]:
        if (bit_value & item_health) != 0:
            flags.append(health_flags[bit_value])

    if item_value < LIFESCAN_BG_VALUE_LOW_MMOL * 1000:
        flags.append(FLAG_RESULT_LOW)
    if item_value > LIFESCAN_BG_VALUE_HIGH_MMOL * 1000:
        flags.append(FLAG_RESULT_HIGH)

    collection.AddItem(VALUE_TYPE_GLUCOSE, dt, item_value, flags)

def ConvertItemBGCal(collection, item):
    """
    **UNITY_BG_CAL (Max = 1022),,,,,,,,,,,,,,,,,,,,,,
    Type,Index,Date,Time,CGM BG Calibration Value (20-600),,,,,,,,,,,,,,,,,,
    10,1022,12/2/2009,9:43,122,,,,,,,,,,,,,,,,,,
    10,1021,12/2/2009,11:31,105,,,,,,,,,,,,,,,,,,
    10,1020,12/2/2009,13:19,88,,,,,,,,,,,,,,,,,,
    """

    ITEM_TYPE = 0
    ITEM_INDEX = 1
    ITEM_DATE = 2
    ITEM_TIME = 3
    ITEM_BG_VALUE = 4

    dt_str = "%s %s" % (item[ITEM_DATE], item[ITEM_TIME])
    dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")

    # TODO : Where?

def ConvertUnityHistoryCSV(filename, ignore_sec_values = False):
    """
    Convert Unity History file in CSV format to objects
    (in the format supported by the CompareWithDatabase module).
    """

    global last_cgm_value
    global mark_next_cgm_as_start_session
    
    last_cgm_value = None
    mark_next_cgm_as_start_session = False

    collection = DeviceDataCollection(ignore_sec_values)

    reader = csv.reader(open(filename, "rb"))
    for item in reader:

        # Use some duck-typing - try to convert the first
        # column to an item type. If it fails we do not
        # have a record.
        try:
            current_item_type = int(item[0])
        except ValueError:
            continue

        if current_item_type == ITEM_TYPE_ALARM:
            ConvertItemAlarm(collection, item)

        elif current_item_type == ITEM_TYPE_PRIME:
            ConvertItemPrime(collection, item)

        elif current_item_type == ITEM_TYPE_SUSPEND:
            # N/A
            pass

        elif current_item_type == ITEM_TYPE_TDD:
            ConvertItemTDD(collection, item)

        elif current_item_type == ITEM_TYPE_BASAL:
            ConvertItemBasal(collection, item)

        elif current_item_type == ITEM_TYPE_BOLUS:
            ConvertItemBolus(collection, item)

        elif current_item_type == ITEM_TYPE_OTPINGBG:
            ConvertOTPingBG(collection, item)

        elif current_item_type == ITEM_TYPE_CGM_WARNING:
            ConvertItemCGMWarning(collection, item)

        elif current_item_type == ITEM_TYPE_CGM_DATA:
            ConvertItemCGMData(collection, item)

        elif current_item_type == ITEM_TYPE_BG_CAL:
            ConvertItemBGCal(collection, item)

        else:
            pass

    return collection


def Usage(program):
    print "Usage : python %s UnityHistoryFile.csv" % (program)

if __name__ == '__main__':

    if len(sys.argv) != 2:
        Usage(sys.argv[0])
        sys.exit(-1)

    collection = ConvertUnityHistoryCSV(sys.argv[1])
    collection.PrintStat()

