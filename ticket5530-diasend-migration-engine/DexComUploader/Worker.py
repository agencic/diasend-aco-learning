# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

try:
    import json
except ImportError:
    import simplejson as json
import unittest

from SendFileToSupport import SendDataToDexcom

import Log

class Worker:
    """
    Worker that accepts JSON formatted data and calls the SendFileToSupport
    functions to send the data to DexCom.
    """
    def __init__(self):
        pass
        
    def DoWork(self, data):
        res = False
        
        try:
            fdata = json.loads(data)
            if fdata.has_key("device_serial_id") and fdata.has_key("data") and fdata.has_key("crc"):
                res, res_str = SendDataToDexcom(fdata["data"], fdata["device_serial_id"], fdata["crc"])
                if res:
                    Log.Info("Terminal %s Device %s Sent data to DexCom : GUID %s" % (fdata["terminal_serial_id"], fdata["device_serial_id"], res_str))
                else:
                    Log.Error("Terminal %s Device %s Failed to send data to DexCom : %s" % (fdata["terminal_serial_id"], fdata["device_serial_id"], res_str))
            else:
                Log.Error("Missing keys in JSON data %s" % (fdata.keys()))

        except ValueError:
            Log.Error("Unable to convert JSON payload to python.")
            
        return res
            
    def ValidateData(self, data):
        """
        Check if data is valid; i.e. is it valid json data. 
        """
        ret = True
        try:
            json.loads(data)
        except ValueError:
            ret = False
            
        return ret

# --- Unit test ---

class WorkerUnitTest(unittest.TestCase):
    
    def setUp(self):
        self.worker = Worker()

    def test_invalid_data(self):
        self.assertFalse(self.worker.ValidateData("abc"))
        
    def test_valid_data(self):
        self.assertTrue(self.worker.ValidateData(json.dumps({"key":"value"})))

    def test_send_ok_server(self):
        data = json.dumps({"device_serial_id":"ABC123", "terminal_serial_id":"0", "data":"binary data will be here later on..."})
        self.assertTrue(self.worker.DoWork(data))
    
    def test_send_nok_server(self):
        data = json.dumps({"device_serial_id":"ABC124", "terminal_serial_id":"0", "data":"binary data will be here later on..."})
        self.assertFalse(self.worker.DoWork(data))
    
if __name__ == '__main__':
    unittest.main()
    
