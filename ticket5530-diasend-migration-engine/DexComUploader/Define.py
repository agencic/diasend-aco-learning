# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# Where do find the uploader?
DEXCOM_UPLOADER_HOST = ''
DEXCOM_UPLOADER_PORT = 50007

# Max number of tasks in the woker thread queue?
WORKER_THREAD_QUEUE_MAX_SIZE = 1000

# Timeout for reading data from async-server (the transfer will
# be aborted after the timeout (seconds)).
SERVER_READ_TIMEOUT = 3

# Max number of bytes allowed in an incoming task
SERVER_READ_MAX_DATA_SIZE = 1024*1024
