# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import socket
import time
try:
    import json
except ImportError:
    import simplejson as json
	
import unittest
import binascii
from zlib import crc32

from Define import DEXCOM_UPLOADER_HOST
from Define import DEXCOM_UPLOADER_PORT

def PostJob(data, terminal_serial_id, device_serial_id):
    """
    Post job to Animas Debug Uploader. Note that data should be text or base64
    encoded binary data. 
    """
    crc = crc32(data) & 0xffffffff
    load = json.dumps({"terminal_serial_id":str(terminal_serial_id), 
                       "device_serial_id":str(device_serial_id), 
                       "data":binascii.b2a_base64(data), 
                       "crc":str(crc)})
    
    ret = True

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((DEXCOM_UPLOADER_HOST, DEXCOM_UPLOADER_PORT))
        s.send(load)
        s.close()
    except socket.error:
        ret = False
    
    return ret
    
# --- Unit test ---

class APIUnitTest(unittest.TestCase):
    
    def setUp(self):
        pass

    def test_send_ok_server(self):
        self.assertTrue(PostJob("binary data will be here later on...", "0", "ABC123" ))
    
    def test_send_nok_server(self):
        self.assertTrue(PostJob("binary data will be here later on...", "0", "ABC124" ))
    
if __name__ == '__main__':
    unittest.main()

    
