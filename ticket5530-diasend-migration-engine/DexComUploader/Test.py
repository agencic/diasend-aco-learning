# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

from time import sleep
import Process

class DelayWorker:
    """
    Simple worker that prints incoming data, waits for a few seconds and 
    then returns. 
    """
    def __init__(self):
        pass
        
    def DoWork(self, data):
        print "do work %s " % (data)
        sleep(5)
        print "ready"
        
    def ValidateData(self, data):
        return True

worker = DelayWorker()
Process.Start(worker)
