# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

from suds.client import Client
from suds.bindings.binding import Binding
from suds.plugin import *
from suds.xsd.doctor import Import, ImportDoctor
from suds.transport import TransportError
import uuid
import logging

from xml.sax import SAXParseException

from zlib import crc32

def RemoveCrapXML(reply):
    """
    Remove non-XML crap that the DexCom web service adds before and after the SOAP envelope in all responses
    """ 
    tag_start = '<s:Envelope'
    tag_end = '</s:Envelope>'
    return reply[reply.index(tag_start):reply.index(tag_end)+len(tag_end)]

class MyMessagePlugin(MessagePlugin):
    """
    Plugin to handle the message before parsed
    """
    def received(self, context):
        context.reply = RemoveCrapXML(context.reply)

def MyReplyFilter(s, reply):
    """
    Similar functionality to the MessagePlugin above, but using a method that will be deprecated soon.

    However, the DexCom web service uses 'Internal Server Errors' as a way to pass
    normal errors and such replies are not handled by the MessageFilter. Since we need to 
    remove crap XML from that reply as well we need this implementation. 
    
    When future versions of SUDS (post 0.4?) removes this support, make sure that
    the errors are then handled by MessagePlugin. Otherwise contribute it to SUDS :) 
    """
    return RemoveCrapXML(reply)

def SendDataToDexcom(data, serialNumber, crc):
    """
    Send the data for the specified serial number to the DecCom Web service
    
    Successful Return:
     * GUID
    
    Possible errors are returned as a message string
    """
    
    # Enable this for debug information
    DEBUG_WEB_SERVICE_LOG = False
    if DEBUG_WEB_SERVICE_LOG:
        logging.basicConfig(level=logging.INFO)
        logging.getLogger('suds.transport').setLevel(logging.DEBUG)
        logging.getLogger('suds.client').setLevel(logging.DEBUG)

    # Fix missing types with ImportDoctor
    schemaUrl = 'http://schemas.microsoft.com/2003/10/Serialization/'
    schemaImport = Import(schemaUrl)
    schemaDoctor = ImportDoctor(schemaImport)

    # Login credentials
    systemId = uuid.UUID("C1E30906-7FBF-4BEB-A0C1-B5AF57377F53")
    productId = uuid.UUID("7060F8A8-EDC1-4088-8D66-273E61DE20D8")
    userId = uuid.UUID("196BAC79-65C4-4774-85A3-37B21D9B6DF1")
    password = "DexNET!Animas1"
    
    # Placeholders for the reply
    errMsg = None
    guid = None

    try:
        # Connect to the DexCom web service
        url = "https://data2.dexcom.com/AnimusWebServiceProxy/AnimusWebServiceProxy.svc?wsdl"
        client = Client(url, plugins=[MyMessagePlugin()], doctor=schemaDoctor)

        Binding.replyfilter = MyReplyFilter
        
        # Login to the service
        context = client.service.Login(systemId, productId, userId, password)
        #print "Login():" , context
        
        # 'Get' the support agreement (an RTF file), but don't store it since we will not need it
        client.service.GetSupportAgreement(context)
        #print "GetSupportAgreement()"
        
        # Acknowledge the support agreement
        res = client.service.AcknowledgeSupportAgreement(context)
        #print "AcknowledgeSupportAgreement():", res
        
        # Check if this serial is authorized
        authorized = client.service.IsAuthorizedToSendToSupport(context, serialNumber)
        #print "IsAuthorizedToSendToSupport():", authorized
        
        if authorized:
            # Create the binary package and send it away
            guid = client.service.SendToSupport(context, data, crc, serialNumber, "")
            #print "SendToSupport():", guid
        else:
            errMsg = "Not authorized"
        
        # Logout from the service
        res = client.service.Logout(context)
        #print "Logout():", res
        
    except SAXParseException, e:
        # Parsing exception - should not be needed since filter and plugin handles the incorrect XML
        # If this exception is reached there is another problem. Server might be down.
        #print ('%s:%s:%s: %s\n' % (e.getSystemId(),e.getLineNumber(),e.getColumnNumber(),e.getMessage()))
        errMsg = "Incorrect or no reply from server"
        
    except WebFault, e:
        # Normal errors are in the DexCom server communicated as internal server errors...
        # The text code might give a hint
        errMsg = "Web Service response:", e
        
    except TransportError, e:
        # The server has responded, but the reply was incorrect
        errMsg = "TransportError ", repr(e.httpcode)
        
    except:
        errMsg = "Unknown Error"
        
    # Create the reply
    if guid:
        return (True, guid)
    else:
        return (False, errMsg)

if __name__ == "__main__":
    # == Unit tests ==

    # Temporary data and serialnumber
    data = "binary data will be here later on..."
    
    # Test 1 - An OK transfer
    print "Test 1 started..."
    serialNumberOk = "ABC123"
    (transferOk, msg) = SendDataToDexcom(data, serialNumberOk)
    if transferOk:
        print "Test 1 succeeded. Got GUID ", msg
    else:
        print "Test 1 failed. Reason =", msg
    
    # Test 2 - A not OK transfer
    print "Test 2 started..."
    serialNumberNotOk = "ABC124"
    (transferOk, msg) = SendDataToDexcom(data, serialNumberNotOk)
    if not transferOk and msg == "Not authorized":
        print "Test 2 succeeded (Not authorized, which is correct)"
    else:
        print "Test 2 failed. TransferOK = %s, Message = %s" % (transferOk, msg)
