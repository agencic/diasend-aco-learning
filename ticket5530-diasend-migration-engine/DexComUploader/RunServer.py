# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

from Worker import Worker
from Server import Start

worker = Worker()
Start(worker)
