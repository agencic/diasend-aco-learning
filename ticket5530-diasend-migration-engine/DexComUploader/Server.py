# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

from Queue import Queue
from threading import Thread
import socket
import select

from Define import DEXCOM_UPLOADER_HOST
from Define import DEXCOM_UPLOADER_PORT
from Define import WORKER_THREAD_QUEUE_MAX_SIZE
from Define import SERVER_READ_TIMEOUT
from Define import SERVER_READ_MAX_DATA_SIZE
from Worker import Worker

import Log

def WorkerExecute(worker, queue):
    """
    Wait on queue for tasks to be executed. Let the worker
    perform the task when one arrives.
    """
    while(True):
        item = queue.get()
        worker.DoWork(item)
        queue.task_done()

def Start(worker):
    """
    Start the server, waiting for incoming tasks on TCP and 
    forward it to the worker thread using a thread-safe 
    queue. 
    """
    queue = Queue(WORKER_THREAD_QUEUE_MAX_SIZE)

    worker_thread = Thread(target=WorkerExecute, args=(worker, queue))
    
    # Set the thread as a daemon will make it possible to exit 
    # the process without waiting for the workher thread to 
    # exit. 
    worker_thread.setDaemon(True)
    worker_thread.start()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((DEXCOM_UPLOADER_HOST, DEXCOM_UPLOADER_PORT))
    s.listen(1)

    while(True):
        conn, addr = s.accept()
        data = ''
        while(True):    
            
            ready = select.select([conn], [], [], SERVER_READ_TIMEOUT)
            if ready[0]:
                recv_data = conn.recv(1024)
                # EOF
                if len(recv_data) == 0:
                    break
                    
                data += recv_data
                    
                if worker.ValidateData(data):
                    Log.Info("Add data to queue (size %d)" % len(data))
                    queue.put(data)
                    break;
                else:
                    if len(data) > SERVER_READ_MAX_DATA_SIZE:
                        Log.Error("Size of incoming data exceeded (allowed %d bytes, actual %d bytes)" % (SERVER_READ_MAX_DATA_SIZE, len(data)))
                        break
            else:
                Log.Error("Timeout when reading data")
                break;
        conn.close()
        
if __name__ == '__main__':
    worker = Worker()
    Start(worker)
