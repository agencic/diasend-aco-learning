# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import logging
import logging.handlers

def Debug(text):
    """log debug text to event logger"""
    logger.debug(text)

def Info(text):
    """log info text to event logger"""
    logger.info(text)

def Warning(text):
    """log warning text to event logger"""
    logger.warning(text)

def Error(text):
    """log error text to event logger"""
    logger.error(text)

# setup circular log-files with backup
log_handler   = logging.handlers.RotatingFileHandler('/tmp/dexcom_uploader.log', 'a', 10*1024*1024,50)
log_handler.setLevel(logging.DEBUG) 
log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
log_handler.setFormatter(log_formatter) 
logging.getLogger('diasend').addHandler(log_handler) 
logger = logging.getLogger('diasend')
logger.setLevel(logging.DEBUG)