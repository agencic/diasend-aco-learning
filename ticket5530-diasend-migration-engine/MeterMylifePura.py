# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2014 Diasend AB, http://diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE mylife Pura
#

from Generic import *
from Defines import *
import Buffer


def amount_records(buffer):
    local_result = []
    first = True
    while buffer.nr_bytes_left() > 0:
        try:
            buffer.set_mark()
            (header, data0, data1, data2, data3, data4, data5) = buffer.get_struct('I6B')
            frame                                              = buffer.get_slice_from_mark()
            (checksum,)                                        = buffer.get_struct('B')
        except Buffer.ExtractException:
            # Error is picked up by Generic.py.
            return []

        if not calculate_checksum(frame) == checksum:
            return []

        # The first record contains total amount of data
        if first:
            first = False
            total_amount = (data1 << 8) + data0
            max_amount = (data3 << 8) + data2
            local_result.append({ELEM_NR_RESULTS: total_amount})

        else:
            month = ((data1 & 0xC0) >> 4) + ((data0 & 0xC0) >> 6) + 1
            day = (data0 & 0x1F) + 1
            hour = (data1 & 0x1F)
            minute = (data2 & 0x3F)
            year = (data3 & 0x7F) + 2000
            value = ((data4 & 0x03) << 8) + data5
            del_flag = (data4 & 0x20)
            accepted_temp = (data4 & 0x10)
            control_solution = (data4 & 0x04)
            if del_flag != 0:
                local_result.append({ELEM_VAL: {"null_result": True}})
            else:
                flags = []
                flags.append(FLAG_RESULT_OUTSIDE_TEMP) if accepted_temp else None
                flags.append(FLAG_RESULT_CTRL) if control_solution else None
                flags.append(FLAG_RESULT_LOW) if value < 10 else None
                flags.append(FLAG_RESULT_HIGH) if value > 600 else None
                local_result.append({ELEM_VAL: {
                    ELEM_VAL_TYPE: VALUE_TYPE_GLUCOSE,
                    ELEM_TIMESTAMP: datetime(year, month, day, hour, minute),
                    ELEM_VAL: value * 18.0/VAL_FACTOR_CONV_MMOL_TO_MGDL,
                    ELEM_FLAG_LIST: flags
                }})

    return local_result


def model_name(buffer):

    try:
        buffer.set_mark()
        (header, name) = buffer.get_struct('H5s')
        frame          = buffer.get_slice_from_mark()
        (checksum,)    = buffer.get_struct('B')

        if name == 'GM550':
            name = 'mylife Pura'
        else:
            return []
    except Buffer.ExtractException:
        # Error is picked up by Generic.py.
        return []

    if not calculate_checksum(frame) == checksum:
        return []

    return [{ELEM_DEVICE_MODEL: name}]


def firmware_version(buffer):
    # Extract firmware information from buffer before continuing.
    try:
        buffer.set_mark()
        (header, model) = buffer.get_struct('H4s')
        frame           = buffer.get_slice_from_mark()
        (checksum,)     = buffer.get_struct('B')
    except Buffer.ExtractException:
        # Error is picked up by Generic.py.
        return []

    if not calculate_checksum(frame) == checksum:
        return []

    return []


def serial_number(buffer):
    try:
        buffer.set_mark()
        (header, slen)  = buffer.get_struct('HB')
        (serial,)        = buffer.get_struct(str(slen) + 's')
        (scs,)          = buffer.get_struct('B')
        frame           = buffer.get_slice_from_mark()
        (cs,)           = buffer.get_struct('B')
    except Buffer.ExtractException:
        # Error is picked up by Generic.py.
        return []

    if not calculate_checksum(serial) == scs:
        return []

    if not calculate_checksum(frame) == cs:
        return []

    return [{ELEM_DEVICE_SERIAL: serial}]


def calculate_checksum(frame):
    return sum([ord(byte) for byte in frame]) & 0xff


def parse_data(data):
    local_result = []
    buffer = Buffer.Buffer(data)

    local_result.extend(model_name(buffer))
    local_result.extend(firmware_version(buffer))
    local_result.extend(serial_number(buffer))
    local_result.extend(amount_records(buffer))

    return local_result

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalMylifePuraSerialRecord(record):
    return record if ELEM_DEVICE_SERIAL in record else {}

def EvalMylifePuraUnitRecord(line):
    return {ELEM_DEVICE_UNIT:"mg/dL"}

def EvalMylifePuraModelRecord(record):
    return record if ELEM_DEVICE_MODEL in record else {}

def EvalMylifePuraResultRecord(record):
    return record[ELEM_VAL] if ELEM_VAL in record else {}

def EvalMylifePuraChecksumRecord(line, record):
    return True

def EvalMylifePuraChecksumFile(inList):
    return True

def EvalMylifePuraNrResultsRecord(record):
    return record if ELEM_NR_RESULTS in record else {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterMylifePura(inList):
    """
    Detect if data comes from a mylife Pura.
    """

    return DetectDevice( 'Mylife_Pura', inList, DEVICE_METER )

def AnalyseMeterMylifePura(inData):
    """
    Analyse data from mylife Pura.
    """
    # Empty dictionary
    d = {"eval_device_model_record": EvalMylifePuraModelRecord,
         "eval_serial_record": EvalMylifePuraSerialRecord,
         "eval_unit": EvalMylifePuraUnitRecord,
         "eval_result_record": EvalMylifePuraResultRecord,
         "eval_checksum_record": EvalMylifePuraChecksumRecord,
         "eval_checksum_file": EvalMylifePuraChecksumFile,
         "eval_nr_results": EvalMylifePuraNrResultsRecord}

    in_list = parse_data(inData)

    res_list = AnalyseGenericMeter( in_list, d )
    
    return res_list

if __name__ == "__main__":
    test_file = open('test/testcases/test_data/MylifePura/MyLifePura_500full.log')
    testcase = test_file.read()
    test_file.close()
    
    results = AnalyseMeterMylifePura(testcase)
    print results[0]['header']
    for result in results[0]['results']:
        print result
