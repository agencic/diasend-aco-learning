# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2013 Diasend AB, Sweden, http://www.diasend.com
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE Abbott FreeStyle Navigator
# @DEVICE Abbott FreeStyle Navigator II

import sys
import re
import crcmod

from Generic import *
from Defines import *
from datetime import *

#Record types
NAV_CONT_BG_RESULT = 2
NAV_ALARM_LOW_GLUC = 16 
NAV_ALARM_HI_GLUC = 17
NAV_ALARM_PROJ_LOW_GLUC = 18
NAV_ALARM_PROJ_HI_GLUC = 19
NAV_BG_RESULT = 26
NAV_INSULIN = 128
NAV_CARB_RESULT = 129
NAV_EXERCISE = 130
NAV_HEALTH = 131
NAV_GENERIC_EVENT = 132

crc_func = crcmod.predefined.mkCrcFun('modbus')

local_number_of_results = 0

class Record(object):
    """
    A simple holder for attributes (of a record). The regular expression 
    functions will create an instance of this class and populate it with 
    attributes. 
    """
    def __init__(self):
        pass

class ParseException(Exception):
    """
    Exception that creates a Diasend error with the indicated error code
    (see Defines.py for error codes). 
    """
    def __init__(self, error_code):
        self.error_code = error_code

def VerifyChecksum(text, checksum):
    """
    Verify the checksum of the text provided.
    """
    calculated_crc = crc_func(text, 0xff)

    return calculated_crc == checksum

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Abbott Freestyle Navigator Serial record. Extracted groups : 

    group 1 : part of message used for checksum calculation
    group 2 : serial number
    group 3 : static text for verifying device type
    group 4 : checksum
    
    example Navigator 1:
    \r$$S_N BAAH243-80007 Log_Clears 1 Rev 0.81 #cdc

    example Navigator 2:
    \r$$S_N BBCR258-D0235 Log_Clear Reason 2 Count 5 Rev 1.03.00 Key 1100 #ddec None
    """
    m = re.match( r'\r(\$\$S_N (\w*-\w*) (Log_Clears)?.*#)([\da-f]{0,4})', line, re.IGNORECASE )
    if m:
        record = Record()
        record.serial_number = m.group(2)
        record.checksum      = int(m.group(4), 16)

        if m.group(3):
            record.navigator_2 = False
        else:
            record.navigator_2 = True

        if not VerifyChecksum(m.group(1), record.checksum):
            raise ParseException(ERROR_CODE_CHECKSUM)

        return record

    return None

def EvalRecordGeneric(line):
    """
    Evaluate a Abbott Freestyle Result record. Extracted groups : 

    group 1 : part of message used for checksum calculation
    group 2 : record id 
    group 3 : year (4 characters)
    group 4 : month (1-2 characters)
    group 5 : day (1-2 characters)
    group 6 : hour (1-2 characters)
    group 7 : minute (1-2 characters)
    group 8 : second (1-2 characters)
    group 9 : event number
    group 10/11 : the rest of the message (10 includes spaces)
    group 12 : checksum

    \r$$1073741889 2007-10-15 18:19:15 26

    """
    m = re.match( r'\r{0,1}(\$\$(\d+) (\d{4,4})-(\d{1,2})-(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2}) (\d*)( (.*))? #)([\da-f]{0,4})', line, re.IGNORECASE )
    if m:
        record = Record()

        record.id       = int(m.group(2))
        record.year     = int(m.group(3))
        record.month    = int(m.group(4))
        record.day      = int(m.group(5))
        record.hour     = int(m.group(6))
        record.minute   = int(m.group(7))
        record.second   = int(m.group(8))

        record.datetime = datetime(record.year, 
            record.month, 
            record.day, 
            record.hour, 
            record.minute, 
            record.second)

        record.event    = int(m.group(9))
        record.message  = m.group(11)
        record.checksum = int(m.group(12), 16)

        if not VerifyChecksum(m.group(1), record.checksum):
            raise ParseException(ERROR_CODE_CHECKSUM)

        return record

    return None

def EvalContinousResultRecord(line):
    """
    Evaluate a Abbott Freestyle Navigator Continuous data record. Extracted groups : 

    group 1 : Glucose value
    group 2 : Status
    group 3 : Trend

    Glucose value (Gl) -> Valid glucose value if St = 1, Error code if St = 0.
    Status (St) -> 0 = Bad, 1 = Ok.
    Trend (Tr) -> Trend (see table below) if St = 1, TX DQ code if St = 0. 

    Trend values 
    ============
    0   PosHi
    1   PosMed
    2   Level 
    3   NegMed
    4   NegHi 
    5   Not Available

    Note : Error codes etc (for when St = 0) is not defined in the current documentation.    

    Three examples below, tr is probably trend, the UI displays an arrow depending on the TR value
    Gl 103 St 1 Tr 1 #a575
    Gl 95 St 1 Tr 2 #d9b8
    Gl 78 St 1 Tr 3 #bc03
    """
    m = re.match( r'Gl (\d*) St (\d+) Tr (\d+)', line, re.IGNORECASE )
    if m:
        record = Record()
        record.value  = float(m.group(1))
        record.valid = int(m.group(2)) == 1
        record.trend  = int(m.group(3))

        return record

    return None

def EvalResultRecord( line ):
    """
    Evaluate a Abbott Navigator Result record. Extracted groups : 
    group 1 : Glucose value in mg/dL
    group 2 : Status (Bit 0 -> normal (0) / control (1), Bit 1 -> out of temperature (1))
    group 3 : CC - strip cal code - always 0 according to the documentation (see example below;)

                                   Gl 115 St 0 CC 14 #1559
    $$128040 2012-11-28 19:37:0 26 Gl 124 St 0 CC 0 #d5ec

    """ 
    m = re.match( r'Gl (\d+) St (\d+) CC (\d+)', line, re.IGNORECASE )
    if m:
        record = Record()
        record.value              = float(m.group(1))
        record.control            = int(m.group(2)) & 1 == 1
        record.out_of_temperature = int(m.group(2)) & 2 == 1

        return record

    return m

def EvalCarbResultRecord( line ):
    """
    Evaluate a Abbott Navigator Carb record. Extracted groups : 
    group 1 : food type optional field
    group 2 : food type, see below
    group 3 : carb value
    
    Food type
    =========
    255 = Unselected default
    0 = Breakfast 
    1 = Lunch
    2 = Dinner
    3 = Snack

    example Navigator 1.
    CarboGrams 60 #aaf6

    example Navigator 2.
    FoodType 1  CarboGrams 20 #6e53

    """    
    m = re.match( r'(FoodType (\d+))?\s*CarboGrams (\d+)', line, re.IGNORECASE )
    if m:
        record = Record()

        record.value          = float(m.group(3))
        record.food_type_flag = None

        if m.group(2):
            food_type_dict = {0:FLAG_BREAKFAST, 1:FLAG_LUNCH, 2:FLAG_DINNER, 3:FLAG_SNACK}
            if int(m.group(2)) in food_type_dict:
                record.food_type_flag = food_type_dict[int(m.group(2))]
            elif int(m.group(2)) == 255:
                # "Unselected" default means "a carb, but not known value". We remove those for now
                return None
            else:
                raise ParseException(ERROR_CODE_VALUE_ERROR)

        return record

    return None
    
def EvalInsulinRecord(line):
    """
    Evaluate an Abbott Navigator Insulin record. Extracted groups :
    group 1 : insulin type
    group 2 : bolus value
    
    Insulin 0  Unit 400 #3fdd
    
    == Insulin type = 
    255 = Unselected default
    0 Rapid-Acting
    1 Long-Acting
    2 Pre-Mix
    3 Intermediate
    4 Short-Acting

    """
    m = re.match(r'Insulin (\d+)  Unit (\d+)', line, re.IGNORECASE)
    if m:
        record = Record()

        # Insulin unit in U * 100 - Diasend store U * 10000
        record.value = float(m.group(2)) * 100
        
        _nav_insulin_type = int(m.group(1))

        # Insulin type
        insulin_type_dict = {
            0 : (VALUE_TYPE_INS_BOLUS, []), 
            1 : (VALUE_TYPE_INS_BOLUS_BASAL, [FLAG_INSULIN_IMPACT_LONG]), 
            2 : (VALUE_TYPE_INS_BOLUS_BASAL, [FLAG_INSULIN_IMPACT_MIX]), 
            3 : (VALUE_TYPE_INS_BOLUS_BASAL, [FLAG_INSULIN_IMPACT_MIX, FLAG_INSULIN_IMPACT_INTERMEDIATE]),
            4 : (VALUE_TYPE_INS_BOLUS, [FLAG_INSULIN_IMPACT_SHORT_ACTING])
        }

        if _nav_insulin_type in insulin_type_dict:
            record.insulin_type, record.insulin_type_flags = insulin_type_dict[int(m.group(1))]
        elif _nav_insulin_type == 255:
            # "Unselected" default means "an insulin bolus, but not known value". We remove those for now.
            return None
        else:
            raise ParseException(ERROR_CODE_VALUE_ERROR)

        return record

    return None

def EvalLowGlucoseAlarmRecord( line ):
    """
    Evaluate a Abbott Navigator Alarm record. Extracted groups : 
    group 1 : Glucose value

    Gl 85 #d3ed

    """
    m = re.match(r'Gl (\d*)', line, re.IGNORECASE)
    if m:
        record = Record()
        record.value = float(m.group(1))
        return record
    return None

def EvalProjGlucoseAlarmRecord( line ):
    """
    Evaluate a Abbott Navigator project hi or low Alarm record. Extracted groups : 
    group 1 : Glucose value
    group 2 : Ip?

    Gl 136 Ip 12 #c63a

    """
    m = re.match( r'Gl (\d*) Ip (\d*)', line, re.IGNORECASE )
    if m:
        record = Record()
        record.value = float(m.group(1))
        record.impendingness = float(m.group(2))
        return record
    return None

def EvalTerminationRecord( line ):
    """
    Evaluate a Abbott Navigator termination record
    
    example:
    \r$$END LOG #5aa
    \r$$END LOG 7821 RECORDS UPLOADED #c3f2 None
    """
    m = re.match(r'\r(\$\$END LOG ((\d+) RECORDS UPLOADED )?#)([\da-f]{0,4})', line, re.IGNORECASE)
    if m:
        record = Record()

        record.number_of_results = None

        if m.group(3):
            record.number_of_results = int(m.group(3))

        record.checksum = int(m.group(4), 16)

        if not VerifyChecksum(m.group(1), record.checksum):
            raise ParseException(ERROR_CODE_CHECKSUM)

        return record

    return None

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAbbottNavigatorSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    record = EvalSerialRecord( line )
    if record:
        res[ "meter_serial" ] = record.serial_number
    return res

def EvalAbbottNavigatorUnitRecord( line ):
    """
    Always return empty string (unit part of result)
    """
    res = { "meter_unit":"mg/dL" }
    return res


def EvalAbbottNavigatorResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    flags       > list of flags (int) if present
    """   
    res = {}

    try:

        # Is this an Abbott Navigator record?
        record = EvalRecordGeneric( line )
        if record:

            # Navigator 1 doesn't report number of records. 
            global local_number_of_results
            local_number_of_results += 1

            flags = []

            if record.event == NAV_CONT_BG_RESULT:
                partial_record = EvalContinousResultRecord(record.message)
                if partial_record:
                    if not partial_record.valid:
                        return {"null_result":True}
                    flags.append(FLAG_CONTINOUS_READING)
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
                    res[ELEM_VAL]      = partial_record.value

            elif record.event == NAV_ALARM_LOW_GLUC:
                partial_record = EvalLowGlucoseAlarmRecord(record.message)
                if partial_record:
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_ALARM
                    res[ELEM_VAL]      = ALARM_LOW_BLOOD_GLUCOSE

            elif record.event == NAV_ALARM_HI_GLUC:
                partial_record = EvalLowGlucoseAlarmRecord(record.message)
                if partial_record:
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_ALARM
                    res[ELEM_VAL]      = ALARM_HIGH_BLOOD_GLUCOSE
            
            elif record.event == NAV_ALARM_PROJ_HI_GLUC:
                partial_record = EvalProjGlucoseAlarmRecord(record.message)
                if partial_record:
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_ALARM
                    res[ELEM_VAL]      = ALARM_PROJ_HIGH_BLOOD_GLUCOSE

            elif record.event == NAV_ALARM_PROJ_LOW_GLUC:
                partial_record = EvalProjGlucoseAlarmRecord(record.message)
                if partial_record:
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_ALARM
                    res[ELEM_VAL]      = ALARM_PROJ_LOW_BLOOD_GLUCOSE
            
            elif record.event == NAV_BG_RESULT:
                partial_record = EvalResultRecord(record.message)
                if partial_record:
                    if partial_record.control:
                        flags.append(FLAG_RESULT_CTRL)
                    if partial_record.out_of_temperature:
                        flags.append(FLAG_RESULT_OUTSIDE_TEMP)                        
                        
                    # A regular BG value is actually considered a CGM Calibration value
                    flags.append(FLAG_CONTINOUS_READING)
                    flags.append(FLAG_CALIBRATION)
                    
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
                    res[ELEM_VAL]      = partial_record.value

            elif record.event == NAV_CARB_RESULT:
                partial_record = EvalCarbResultRecord(record.message)
                if partial_record:
                    res[ELEM_VAL_TYPE] = VALUE_TYPE_CARBS
                    res[ELEM_VAL]      = partial_record.value
                    if partial_record.food_type_flag:
                        flags.append(partial_record.food_type_flag)

            elif record.event == NAV_INSULIN:
                partial_record = EvalInsulinRecord(record.message)
                if partial_record:
                    res[ELEM_VAL_TYPE] = partial_record.insulin_type
                    res[ELEM_VAL]      = partial_record.value

                    # This is a manually entered dose
                    flags.append(FLAG_MANUAL)

                    # Add the flags from the insulin record
                    flags = flags + partial_record.insulin_type_flags

            elif record.event in [NAV_GENERIC_EVENT, NAV_EXERCISE, NAV_HEALTH]:
                pass

            else:
                # The rest are currently unknown
                pass
            
            res[ELEM_TIMESTAMP] = record.datetime
            res[ELEM_FLAG_LIST] = flags

    except ParseException, e:
        return {"error_code":e.error_code, "fault_data":line}

    except:
        return {"error_code":ERROR_CODE_VALUE_ERROR, "fault_data":line}

    return res
    
def EvalAbbottNavigatorChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    
    Don't got any spec yet, so the checksum format is unknown

    2013-01-29 - got spec and implemented the checksum calculation. 
    Decided to not implement it here for simplicity (the regular
    expression functions already knows which data that should be 
    used for checksum calculation)self.
    """    
    return True
    
def EvalAbbottNavigatorTerminationRecord( line ):
    """Is this line a termination record!"""
    return EvalTerminationRecord(line) != None

def EvalAbbottNavigatorModelRecord(line):
    """
    Return device model by looking at the serial 
    record. 
    """
    res = {}
    record = EvalSerialRecord( line )
    if record:
        if record.navigator_2:
            res["device_model"] = "FreeStyle Navigator II"
        else:
            res["device_model"] = "Navigator"
    return res

def EvalAbbottNavigatorNrResult(line):
    """
    Return number of results. For Navigator 2 use the reported
    number of records, for Navigator 1 use the actually received
    number of records. 
    """
    record = EvalTerminationRecord(line)
    if record:
        if record.number_of_results:
            return {ELEM_NR_RESULTS:record.number_of_results}
        else:
            return {ELEM_NR_RESULTS:local_number_of_results}

    return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectAbbottNavigator( inList ):
    """
    Detect if data comes from a Abbott Navigator.
    """
    return DetectDevice( 'AbbottNavigator', inList, DEVICE_METER )

def AnalyseAbbottNavigator( inData ):
    """
    Analyse Abbott Navigator
    """
    inList = inData.split('\n')

    global local_number_of_results
    local_number_of_results = 0

    # Empty dictionary
    d = {}

    d["eval_device_model_record"] = EvalAbbottNavigatorModelRecord
    d["eval_serial_record"]       = EvalAbbottNavigatorSerialRecord
    d["eval_unit"]                = EvalAbbottNavigatorUnitRecord
    d["eval_result_record"]       = EvalAbbottNavigatorResultRecord
    d["eval_checksum_record"]     = EvalAbbottNavigatorChecksumRecord
    d["eval_termination_record"]  = EvalAbbottNavigatorTerminationRecord
    d["eval_nr_results"]          = EvalAbbottNavigatorNrResult

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    
    #testfile = open('test/testcases/test_data/AbbottNavigator2/FreeStyleNavigator.log')
    testfile = open('test/testcases/test_data/AbbottNavigator2/FreeStyleNavigator-manual-insulin.log')
    #testfile = open('test/testcases/test_data/AbbottNavigator2/BBCR258-D0140.log')
    #testfile = open('test/testcases/test_data/AbbottNavigator2/Nav2-unselected-default-insulin.log')
    #testfile = open('test/testcases/test_data/AbbottNavigator2/FreeStyleNavigatorII_BBCS291-D0031-calibration.log')
    testcase = testfile.read()
    testfile.close()

    res    = AnalyseAbbottNavigator(testcase)
    print(res)