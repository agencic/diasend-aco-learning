# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Roche SmartPix supported device

import re
from Generic import *
from Defines import *
from datetime import datetime
from datetime import time
import EventLog
import string
try:
    from elementtree import ElementTree
except:
    # Python 2.5
    from xml.etree import ElementTree

from MeterRocheAccuchekAviva import model_ids

DEVICE_NAME_ACCU_CHEK_INSIGHT          = "Accu-Chek Insight"
DEVICE_NAME_ACCU_CHEK_SPIRIT_COMBO     = "Accu-Chek Spirit Combo"
DEVICE_NAME_ACCU_CHEK_SPIRIT           = "Accu-Chek Spirit"
DEVICE_NAME_ACCU_CHEK_DTRON            = "Accu-Chek D-TRON"
DEVICE_NAME_ACCU_CHEK_AVIVA_INSIGHT    = "Accu-Chek Aviva Insight"
DEVICE_NAME_ACCU_CHEK_PERFORMA_INSIGHT = "Accu-Chek Performa Insight"

class SmartPixAnalyzer(object):

    def __init__(self):
        self.current_bolus_total = -1
        self._global_bg_factor   = None

        self.model_number                 = None
        self.model_name                   = None
        self.concatenate_model_and_serial = True

    def flatten_tree(self, tree):
        """
        Returns a list of the leaves in a tree
        """
        ret_list = []

        if len(tree.getchildren()) == 0:
            # This is a leaf
            ret_list.append(tree)
        else:
            for elem in tree.getchildren():
                ret_list += self.flatten_tree(elem)

        return ret_list

    def split_data(self, in_data):
        """
        Splits incoming data into a list. The data is a mixture of ascii,
        (the header) and binary result records.
        The result records are just fixed length binary chunks
        """

        in_list = []

        data_type = 'UNKNOWN'

        # First find the import tag
        import_pos = in_data.find("<IMPORT")
        # Also strip everything after last >
        last_tag_end_pos = in_data.rfind(">")

        if import_pos > 0 and last_tag_end_pos > 0:
            data = in_data[import_pos:last_tag_end_pos + 1]
            
            data_type = 'IMPORT'

            try:
                tree = ElementTree.fromstring(data)

                # Get a list containing the leaves
                #in_list = self.flatten_tree(tree)
                in_list = tree.getchildren()
            except:
                pass

        elif in_data.find('<DEVICELIST') > 0:
            data_type = 'DEVICE_LIST'

        return (data_type, in_list)

    def parse_date_time(self, date, time):
        """
        Parses time of form: 13:02
        And date of form: 2007-07-23

        into a datetime
        """
        d_m = re.match( r"(\d{4,4})-(\d{2,2})-(\d{2,2})", date, re.IGNORECASE)
        t_m = re.match( r"(\d{2,2}):(\d{2,2})", time, re.IGNORECASE)

        if not d_m or not t_m:
            return None

        return datetime(int(d_m.group(1)), int(d_m.group(2)), int(d_m.group(3)), int(t_m.group(1)), int(t_m.group(2)))

    def eval_basal_program_periods(self, line, program_nr):
        """
        Evaluates a basal program, line must be an IPPROFILE element
        """
        res = {}
        res[PROGRAM_TYPE] = PROGRAM_TYPE_BASAL
        res[PROGRAM_NAME] = str(program_nr)
        periods = []
        res[PROGRAM_PERIODS] = periods

        for parent in line.getchildren():
            if parent.tag == 'IPTIMESLOT':
                rate = float(str(parent.get("IU")))
                # rate is stored in mU/H
                rate = int(rate * 1000)

                # In smartpix 2 a variant is introduced which contains
                # start and end time rather than just a number...
                num = parent.get("Number")
                if num:
                    start_time = time(int(str(num)) - 1, 0)
                else:
                    xml_time = parent.get("StartTime")
                    if xml_time:
                        # datetime.time doesnt have any parsing methods :/
                        parts = xml_time.split(":")
                        start_time = time(int(parts[0]), int(parts[1]))
                    else:
                        raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                            'Invalid IPTIMESLOT format')

                periods.append((start_time, rate))
            else:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                    'Unknown program tag: %s' % (parent.tag))
                
        return res


    def eval_insulin_pump_record(self, line):
        """
        Evaluates a xml element of from insulin pump data, child of the IP tag
        
        returns a list of results
        """
        res_list = []
        
        if line.tag == "IPPROFILE":
            insulin_prog_index = str(line.get("Name"))
            
            if str(line.get("Active")) == "1":
                # This is the active program
                settings = {}
                settings[SETTING_ACTIVE_BASAL_PROGRAM_ID] = insulin_prog_index 
                res_list.append({SETTINGS_LIST: settings})
                
            res_list.append(self.eval_basal_program_periods(line, insulin_prog_index))
        elif line.tag == "PUMP_SETTINGS":
            settings = {}

            # Reuse of the prime size from tandem
            c_prime_size = line.get("InfusionSetCannulaFillAmount")
            if c_prime_size:
                settings[SETTING_TANDEM_CANNULA_PRIME_SIZE] = int(float(c_prime_size) * 100)

            # @TODO: We lose the decimals
            low_level = line.get("LowCartridgeAlarmQuantity")
            if low_level:
                settings[SETTING_LOW_INSULIN_LEVEL] = int(float(low_level))

            t_prime_size = line.get("InfusionSetTubingFillAmount")
            if t_prime_size:
                settings[SETTING_TUBING_PRIME_SIZE] = int(float(t_prime_size) * 100)

            prime_size = line.get("PrimeQuantity")
            if prime_size:
                settings[SETTING_PRIME_SIZE] = int(float(prime_size) * 100)

            increment = line.get("BolusIncrement")
            if increment:
                settings[SETTING_BOLUS_INCREMENT] = int(float(increment) * 1000)

            speed = line.get("DeliverySpeed")
            if speed:
                speed = int(speed)
                # convert to diasend speed
                if speed == 0:
                    out_speed = 0
                elif speed == 1:
                    out_speed = 5
                elif speed == 2:
                    out_speed = 1
                elif speed == 3:
                    out_speed = 6
                else:
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'Unhandled speed: %d' % (speed))

                settings[SETTING_BOLUS_DELIVERY_SPEED] = out_speed

            if settings:
                res_list.append({SETTINGS_LIST: settings})
        elif line.tag == "PUMP_REMINDERS":
            # @todo : Implement
            pass
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'Unhandled pump tag: %s' % (line.tag))

        return res_list

    def eval_bolus_record(self, line):
        """
        Evaluates a Bolus element
        """
        res_list = []

        remark = str(line.get("remark"))
        
        if remark == "Bolus Total":
            # We count basal, total, so when we get total bolus + basal we can calculate basal tdd
            self.current_bolus_total = int(10000 * float(line.get("amount")))
        elif remark == "Bolus+Basal Total":
            val = int(10000 * float(line.get("amount")))
            res = {}                
            res[ELEM_VAL_TYPE] = VALUE_TYPE_INS_TDD
            res[ELEM_VAL] = val
            res[ELEM_TIMESTAMP] = self.parse_date_time(str(line.get("Dt")), "23:59")
            res_list.append(res)
            
            # Now to the basal-tdd
            if self.current_bolus_total != -1:
                res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : val - self.current_bolus_total, ELEM_VAL_TYPE : VALUE_TYPE_INS_BASAL_TDD}]
        else:
            # Normal bolus
            res = {}
            res[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BOLUS
            res[ELEM_VAL] = int(10000 * float(line.get("amount")))
            res[ELEM_TIMESTAMP] = self.parse_date_time(str(line.get("Dt")), str(line.get("Tm")))
            res[ELEM_FLAG_LIST] = []

            cmd = line.get("cmd")
            if cmd:
                #If value equals "1" this is a advice bolus. If value equals "2" this is a remote bolus.
                if cmd == "1":
                    res[ELEM_FLAG_LIST].append(FLAG_BOLUS_REMOTE_INITIATED)
                elif cmd == "2":
                    res[ELEM_FLAG_LIST].append(FLAG_ADVICED)
                elif cmd != "0":
                    # Not according to spec (4.2)
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'Unknown bolus cmd: %s' % (cmd))

            # type:
            # standard, scroll, extended, multiwave (only bolus); values to be used are "Std", "Scr", 
            # "Ext", "Mul", not needed when Tm has values "total" or "day total" (special functions 
            # for daily sums; see above)
            bolus_type = str(line.get("type"))
            if bolus_type == "Std":
                pass
            elif bolus_type == "Scr":
                # @todo audio?
                pass
            elif bolus_type == "Ext":

                # Add the duration, the remark contains it
                # Example remark: " 1:52 h"
                t_m = re.match( r" *(\d+):(\d{1,2}) *h", remark, re.IGNORECASE)
                if not t_m:
                    return  { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":remark }

                duration = int(t_m.group(1)) * 60 + int(t_m.group(2))
                res[ ELEM_VALUE_LIST ] = [{ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION}]   
                res[ELEM_FLAG_LIST].append(FLAG_BOLUS_TYPE_EXT)
                                
            elif bolus_type == "Mul":
                # Combo bolus
                # Example:
                # <BOLUS Dt=\"2008-11-23\" Tm=\"13:11\" type=\"Mul\" amount=\"4.00\" remark=\"2.60 / 1.40 0:58 h\" /> 
                c_m = re.match( r"(\d+\.\d+) */ *(\d+\.\d+) *\ (\d+):(\d+) *h", remark, re.IGNORECASE)
                if not c_m:
                    return  { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":remark }

                res[ELEM_FLAG_LIST].append(FLAG_BOLUS_TYPE_COMBO)
                
                duration = int(c_m.group(3)) * 60 + int(c_m.group(4))

                # Modify the value since the direct value is the main value, not the sum
                res[ELEM_VAL] = int(10000 * float(c_m.group(1)))

                res[ ELEM_VALUE_LIST ] = []
                
                res[ ELEM_VALUE_LIST ].append({ELEM_VAL : duration, ELEM_VAL_TYPE : VALUE_TYPE_DURATION})

                # Add in extended amount
                res[ ELEM_VALUE_LIST ].append({ELEM_VAL : int(10000 * float(c_m.group(2))), ELEM_VAL_TYPE : VALUE_TYPE_INS_BOLUS_EXT})
            
            res_list.append(res)

        return res_list 

    def eval_basal_record(self, line):
        """
        Evaluates a Basal element
        """
        res_list = []
        
        res = {}
        res[ELEM_VAL_TYPE] = VALUE_TYPE_INS_BASAL
        res[ELEM_FLAG_LIST] = []
        val = line.get("cbrf")
        # Shall be in mU/h
        res[ELEM_VAL] = int(float(val) * 1000)
        
        res[ELEM_TIMESTAMP] = self.parse_date_time(str(line.get("Dt")), str(line.get("Tm")))
        
        if line.get("TBRinc") or line.get("TBRdec"):
            res[ELEM_FLAG_LIST].append(FLAG_TEMP_MODIFIED)

        if line.get("cmd") == 2:
            # Remote event
            res[ELEM_FLAG_LIST].append(FLAG_BASAL_REMOTE_INITIATED)

        remark = line.get("remark")
        if remark:
            if remark.startswith("dur"):
                if not FLAG_TEMP_MODIFIED in res[ELEM_FLAG_LIST]: 
                    res[ELEM_FLAG_LIST].append(FLAG_TEMP_MODIFIED)
            elif remark.startswith("changed "):
                res[ELEM_FLAG_LIST].append(FLAG_PROGRAM_CHANGE)
            elif remark == 'Run':
                # Normal condition
                pass
            elif remark == 'Pause':
                res[ELEM_FLAG_LIST].append(FLAG_SUSPENDED)
            elif remark == 'Stop':
                res[ELEM_FLAG_LIST].append(FLAG_DELIVERY_STOPPED)
            elif remark == 'TBR End':
                # @TODO: asante flag
                res[ELEM_FLAG_LIST].append(FLAG_ASANTE_TEMP_BASAL_ENDED)
            elif remark == 'TBR End (cancelled)':
                res[ELEM_FLAG_LIST].append(FLAG_TEMP_BASAL_CANCELLED)
            elif remark == 'time / date set (time shift back)':
                res[ELEM_FLAG_LIST].append(FLAG_TIME_CHANGED)
            elif remark == 'time / date corrected':
                res[ELEM_FLAG_LIST].append(FLAG_TIME_CHANGED)
            elif remark == 'time / date set':
                res[ELEM_FLAG_LIST].append(FLAG_TIME_CHANGED)
            elif remark == 'power up':
                res[ELEM_FLAG_LIST].append(FLAG_POWER_ON)
            elif remark == 'power up (time shift back)':
                res[ELEM_FLAG_LIST].append(FLAG_POWER_ON)
                res[ELEM_FLAG_LIST].append(FLAG_TIME_CHANGED)
            elif remark == 'power down':
                res[ELEM_FLAG_LIST].append(FLAG_POWER_OFF)
            elif re.match("[1-5AB] \- [1-5AB]", remark):
                res[ELEM_FLAG_LIST].append(FLAG_PROGRAM_CHANGE)            
            else:
                # Unexisting according to spec (4.2)
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                    'Unknown basal remark: %s' % (remark))

        res_list.append(res)

        return res_list 


    def eval_event_record(self, line):
        """
        Evaluates an Event element
        """
        res_list = []
        
        res = {}
        res[ELEM_VAL_TYPE] = VALUE_TYPE_ALARM
        res[ELEM_TIMESTAMP] = self.parse_date_time(str(line.get("Dt")), str(line.get("Tm")))
        
        description = str(line.get("description"))
        if description == "prime infusion set":
            # Example:
            # <EVENT Dt="2006-09-20" Tm="08:44" shortinfo="9.50 IU" description="prime infusion set" />
            res[ELEM_VAL_TYPE] = VALUE_TYPE_INS_PRIME
            shortinfo = str(line.get("shortinfo"))
            p_m = re.match( r"(\d+\.\d*) *IU", shortinfo, re.IGNORECASE)
            if not p_m:
                p_m = re.match( r"- *U", shortinfo, re.IGNORECASE)
                if not p_m:
                    return  { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":shortinfo }
                else:
                    res[ELEM_VAL] = 0                
            else:
                res[ELEM_VAL] = int(float(p_m.group(1)) * 100)
        elif description == "cartridge changed":
            res[ELEM_VAL] = ALARM_CARTRIDGE_REPLACE
        elif description == "cartridge / adapter alert" or description == "cartridge error" \
            or description == "cartridge/adapter alert" or description == "cartridge missing":
            res[ELEM_VAL] = ALARM_CARTRIDGE
        elif description == "cartridge low" or description == "cartridge low warning":
            res[ELEM_VAL] = ALARM_CARTRIDGE_LOW
        elif description == "cartridge empty" or description == "empty cartridge":
            res[ELEM_VAL] = ALARM_CARTRIDGE_EMPTY
        elif description == "cartridge type":
            res[ELEM_VAL] = ALARM_CARTRIDGE_TYPE
        elif description == "battery low" or description == "low PowerPack warning":
            res[ELEM_VAL] = ALARM_BATTERY_LOW
        elif description == "battery depleted" or description == "PowerPack depleted" or description == "battery empty":
            res[ELEM_VAL] = ALARM_BATTERY_OUTAGE
        elif description == "battery type":
            res[ELEM_VAL] = ALARM_BATTERY_TYPE
        elif description == "power interrupt":
            res[ELEM_VAL] = ALARM_POWER_INTERRUPT
        elif description == "bolus cancelled":
            res[ELEM_VAL] = ALARM_BOLUS_CANCELLED
        elif description == "occlusion":
            res[ELEM_VAL] = ALARM_OCCLUSION_1
        elif description == "set not primed":
            res[ELEM_VAL] = ALARM_NOT_PRIMED
        elif description == "alarm clock":
            res[ELEM_VAL] = ALARM_ALARM_CLOCK
        elif description == "review time and date" or description == "set time/date" or description == "review time":
            res[ELEM_VAL] = ALARM_DATE_CHANGE_NEEDED
        elif description == "Auto Off" or description == "automatic off":
            res[ELEM_VAL] = ALARM_POWER_AUTO_OFF
        elif description == "mechanical error":
            res[ELEM_VAL] = ALARM_MECHANICAL_ERROR
        elif description == "electronic error":
            res[ELEM_VAL] = ALARM_ELECTRONICAL_ERROR
        elif description == "bluetooth fault":
            res[ELEM_VAL] = ALARM_BLUETOOTH_ERROR
        elif description == "missed bolus":
            # @TODO: Reuse of asante...
            res[ELEM_VAL] = ALARM_ASANTE_INFO_MISSED_BOLUS
        elif description == "change infusion set":
            res[ELEM_VAL] = ALARM_CHANGE_INFUSION_SET
        elif description == "end of use time alert" or \
            description == "end of use time" or \
            description == "remaining pump time" or \
            description == "end of operation" or \
            description == "data interrupted" or \
            description == "text error" or \
            description == "concentration changed" or \
            description == "call for update" or \
            description == "pump timer" or \
            description == "loantime warning" or \
            description == "language error" or \
            description == "insulin timeout over":
            # Known events, which are kind of unexplained,
            # according to Roche we should check each meters spec...
            pass
        else:
            # Unknown data...
            # Unexisting according to spec (4.2)
            EventLog.EventLogInfo('DeviceRocheSmartpix: Unknown event "%s" ' % (description)) 
            #raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
            #    'Unknown event: %s' % (description))

        res_list.append(res)

        return res_list 


    def eval_insulin_data_record(self, line):
        """
        Eval bolus basal and event elements
        """
        res = []
        
        if line.tag == "BOLUS":
            res = self.eval_bolus_record( line )
        elif line.tag == "BASAL":
            res = self.eval_basal_record( line )
        elif line.tag == "EVENT":
            res = self.eval_event_record( line )

        return res

    def eval_blood_glucose_record(self, line):
        """
        Evaluates a BG XML element
        
        <BG Val="8.0" Dt="2007-07-23" Tm="05:14" Flg="M1, M" Carb="36" Ins1="9.0" D="1"/>
        """
        AcsPixFlagToDiasendFlag = {
                            "A" : FLAG_RESULT_ABOVE_USER_RANGE, 
                            "B" : FLAG_RESULT_BELOW_USER_RANGE, 
                            "D" : FLAG_EXPIRED_DRUM, 
                            "E" : FLAG_STRIP_WARNING, 
                            "M" : FLAG_MANUAL,
                            "T" : FLAG_RESULT_OUTSIDE_TEMP, 
                            "M1" : FLAG_BEFORE_MEAL,
                            "M3" : FLAG_AFTER_MEAL,
                            "ME" : FLAG_MEAL,
                            "Hy" : FLAG_FEEL_HYPO,
                            "*" : FLAG_GENERAL
                            }
        results = []
        
        res_bg = {}
        flags = []
        
        time_stamp = self.parse_date_time(str(line.get("Dt")), str(line.get("Tm")))

        # Try to fetch the BG value
        val = line.get("Val")
        if val == "---":
            # The value is ---, which happens when the BG result is not available (only insulin for example)
            res_bg = {"null_result":True}
        else:
            # A valid BG
            if val == "LO":
                float_val = 0
                flags.append(FLAG_RESULT_LOW)
            elif val == "HI":
                float_val = 0
                flags.append(FLAG_RESULT_HIGH)
            else:
                if self._global_bg_factor:
                    float_val = float(val) * self._global_bg_factor
                else:
                    return [{ "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":"No BG factor yet" }]

            # This is a blood glucose value 
            res_bg[ELEM_VAL_TYPE] = VALUE_TYPE_GLUCOSE
            res_bg[ELEM_FLAG_LIST] = flags
            res_bg[ELEM_VAL] = round(float_val)
            res_bg[ELEM_TIMESTAMP] = time_stamp

            # Add other flags
            if line.get("Ctrl"):
                flags.append(FLAG_RESULT_CTRL)

            f_type = line.get("FType")
            if f_type:
                # testing type....
                if f_type == "1":
                    flags.append(FLAG_STRUCTURED_TESTING)
                else:
                    # Not according to spec (4.2)
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'Unknown FType: %s' % (f_type))

            # Decode flags
            inflags = line.get("Flg")
            if inflags:
                # The flags are separated with "," - like "M1, M"
                inflags = string.split(inflags, ",")
                for flag in inflags: 
                    try:
                        _diasend_flag = AcsPixFlagToDiasendFlag[ string.strip(flag) ]
                    except:
                        res_bg = {"error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":inflags}
        
                    flags.append(_diasend_flag)
            
        results.append(res_bg)
        
        # Add carbs as well, if available
        carbs = line.get("Carb")
        if carbs:
             res_carb = {}
             res_carb[ELEM_TIMESTAMP] = time_stamp
             res_carb[ELEM_FLAG_LIST] = []
             res_carb[ELEM_VAL_TYPE] = VALUE_TYPE_CARBS
             res_carb[ELEM_VAL] = round(float(carbs))
             results.append(res_carb)
        
        if line.get("D"):
            # The value has the device tag, we only support one device at the time, so check that the value is 1
            if str(line.get("D")) != "1":
                return [{ "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":line.get("D") }]

        return results

    def eval_reminder_record(self, line, index, typ):
        """
        Evaluates a DATE_REMINDER, TIME_REMINDER or a PUMP_REMINDER child XML element
        """
        ret = {}

        # @todo : Implement
        if line.tag == "DATE_REMINDER":
            pass
        elif line.tag == "TIME_REMINDER":
            pass
        elif line.tag == "PUMP_REMINDER":
            pass
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'Unknown reminder tag: %s' % (line.tag))
                            
        return ret

    def eval_bg_reminder_record(self, line, index):
        """
        Evaluates a BG_REMINDER child XML element
        """
        ret = {}

        # @todo : Implement!
        if line.tag == "BG_REMINDER":
            pass
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'Unknown BG reminder tag: %s' % (line.tag))

        return ret

    def eval_bolus_advice_settings_record(self, line):
        """
        Evaluates a Bolus Advice child XML element

        <ADVICE_OPTIONS ActingTime="04:00" OffsetTime="01:00" MealRise="2.8" SnackSize="20"/>

        <HEALTH_EVENTS Exercise1="-30" Exercise2="-50" Stress="20" Illness="50" Premenstrual="40" Customized1="0" Customized2="0" Customized3="0"/>

        <TIMEBLOCKS>
                   
        """
        settings = {}
        if line.tag == "ADVICE_OPTIONS":
            meal_rise = float(line.get("MealRise"))
            if self._global_bg_factor == VAL_FACTOR_GLUCOSE_MMOL:
                meal_rise = meal_rise * VAL_FACTOR_CONV_MMOL_TO_MGDL
            
            settings[SETTINGS_BOLUS_ADVICE_OPTIONS_MEAL_EXCURSION] = round(meal_rise, 2)
            settings[SETTINGS_BOLUS_ADVICE_OPTIONS_ACTIVE_TIMEOUT] = line.get("ActingTime")+":00"
            settings[SETTINGS_BOLUS_ADVICE_OPTIONS_OFFSET_TIMEOUT] = line.get("OffsetTime")+":00"
            settings[SETTINGS_BOLUS_ADVICE_OPTIONS_SNACK_LIMIT] = int(line.get("SnackSize"))

        elif line.tag == "HEALTH_EVENTS":
            settings[SETTING_HEALTH_EVENT_EXCERCISE_1_VALUE] = int(line.get("Exercise1"))
            settings[SETTING_HEALTH_EVENT_EXCERCISE_2_VALUE] = int(line.get("Exercise2"))
            settings[SETTING_HEALTH_EVENT_STRESS_VALUE] = int(line.get("Stress"))
            settings[SETTING_HEALTH_EVENT_ILLNESS_VALUE] = int(line.get("Illness"))
            settings[SETTING_HEALTH_EVENT_PREMENSTRUAL_VALUE] = int(line.get("Premenstrual"))
            customized = line.get("Customized1")
            if customized:
                settings[SETTING_HEALTH_EVENT_CUSTOMIZED_1] = int(customized)

            customized = line.get("Customized2")
            if customized:
                settings[SETTING_HEALTH_EVENT_CUSTOMIZED_2] = int(customized)

            customized = line.get("Customized3")
            if customized:
                settings[SETTING_HEALTH_EVENT_CUSTOMIZED_3] = int(customized)

        elif line.tag == "TIMEBLOCKS":
            index = 0
            for child in line.getchildren():
                setting = self.eval_time_block_settings_record( child, index )
                index += 1
                if setting:
                    settings.update(setting)
            pass
         
        return settings
        
    def eval_time_block_settings_record(self, line, index):
        """
        Evaluates a Time Block child XML element

        <TIMEBLOCK StartTime="00:00" EndTime="05:29" LowerLimit="4.0" UpperLimit="7.0" CarbRatioInsulin="1.0" CarbRatioCarb="12" 
                   InsulinSensitivityInsulin="1.0" InsulinSensitivityBG="3.0" />
                   
        """
        settings = {}
        
        if line.tag == "TIMEBLOCK":
            # Each block is offset 4 bits (16 bytes)
            offset = index * 16
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK                                   + offset] = line.get("StartTime")+":00"
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_END_TIME                          + offset] = line.get("EndTime")+":00"

            bg_lower_limit = float(line.get("LowerLimit"))
            bg_upper_limit = float(line.get("UpperLimit"))
            bg_sens = float(line.get("InsulinSensitivityBG"))
            if self._global_bg_factor == VAL_FACTOR_GLUCOSE_MMOL:
                bg_lower_limit = round(bg_lower_limit * VAL_FACTOR_CONV_MMOL_TO_MGDL, 2)
                bg_upper_limit = round(bg_upper_limit * VAL_FACTOR_CONV_MMOL_TO_MGDL, 2)
                bg_sens = round(bg_sens * VAL_FACTOR_CONV_MMOL_TO_MGDL, 2)

            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_MIN_BG_VALUE                      + offset] = bg_lower_limit
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_MAX_BG_VALUE                      + offset] = bg_upper_limit
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_CARB_RATIO_INSULIN_VALUE          + offset] = int(float(line.get("CarbRatioInsulin"))*10000)
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_CARB_RATIO_CARB_VALUE             + offset] = line.get("CarbRatioCarb")
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_INSULIN_SENSITIVITY_INSULIN_VALUE + offset] = int(float(line.get("InsulinSensitivityInsulin"))*10000)
            settings[SETTING_BOLUS_ADVICE_TIMEBLOCK_INSULIN_SENSITIVITY_BG_VALUE      + offset] = bg_sens
            pass

        return settings

    def eval_meter_settings_record(self, line):
        """
        Evaluates a Device child XML element
        """
        settings = {}

        if line.tag == "BOLUS_ADVICE":
            # The device tag contains settings
            for child in line.getchildren():
                setting = self.eval_bolus_advice_settings_record( child )
                if setting:
                    settings.update(setting)

            # The tag itself might contain some settings...
            settings[SETTING_BOLUS_ADVICE_ENABLED] = int(line.get("Enabled"))

        elif line.tag == "PAIRING":
            if line.get("Paired") == "1":
                settings[SETTING_PAIRED_DEVICE] = line.get("PumpSerialNr")
            else:
                return None
        elif line.tag == "DATE_REMINDERS":
            # @todo : Implement
            pass
        elif line.tag == "TIME_REMINDERS":
            # @todo : Implement
            pass
        elif line.tag == "BG_REMINDERS":
            # @todo : Implement
            pass
        elif line.tag == "INFUSION_SET_REMINDER":
            # @todo : Implement
            pass
        elif line.tag == "ST3DAY_CONFIG":
            if line.get("Configured") == "1":
                setting[SETTING_STRUCTURED_TESTING_ACTIVE]      = line.get("Active")
                setting[SETTING_STRUCTURED_TESTING_START]       = line.get("StartDt") + " 00:00"
                setting[SETTING_STRUCTURED_TESTING_BREAKFAST]   = line.get("BreakfastTm")
                setting[SETTING_STRUCTURED_TESTING_LUNCH]       = line.get("LunchTm")
                setting[SETTING_STRUCTURED_TESTING_DINNER]      = line.get("DinnerTm")
                setting[SETTING_STRUCTURED_TESTING_BEDTIME]     = line.get("BedTm")
        else:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                'Unknown meter device tag: %s' % (line.tag))

        return settings

    def eval_roche_smartpix_serial_record(self, line):
        """
        Is this line a serial record. If so, return a dictionary with serial
        number.
        
        Also check the Smartpix version
        """
        res = {}

        if line.tag == "DEVICE" or line.tag == "IP":
            # The serial can be padded by spaces, so strip!
            serial_number = str(line.get("SN")).rstrip()

            # Some times, the Smart Pix 2 can return an incorrect 'empty' serial number. Roche is investigating it, but we need to discard those devices.        
            if serial_number == "00000000":
                return res
            
            # Certain model numbers are known as ok to concatenate with serial to get true serial
            serialslist = []
            model_number_str = line.get("ModelNum")
            if model_number_str:
                self.model_number = int(model_number_str)
                for model in model_ids:
                    if self.model_number in model['ids']:
                        self.concatenate_model_and_serial = model['concatenate_model_and_serial']
                        self.model_name                   = model['name']

                if self.concatenate_model_and_serial:
                    serial_number = model_number_str + serial_number

            res[ "meter_serial" ] = serial_number

        return res
        

    def eval_roche_smartpix_device_model_record(self, line):
        """
        Is this line a device model record. If so, return a dictionary with device
        model.
        """
        res = {}
        if line.tag == "DEVICE" or line.tag == "IP":

            # Try to find model by using model id (already picked up
            # during serial extraaction)
            if self.model_name is not None:
                res[ "device_model" ] = self.model_name

            # Try to find model by using name
            if self.model_name is None:
                # List of known device maps
                device_name_map = {
                    "Antlia"      : DEVICE_NAME_ACCU_CHEK_INSIGHT,
                    "Combo"       : DEVICE_NAME_ACCU_CHEK_SPIRIT_COMBO,
                    "Spirit"      : DEVICE_NAME_ACCU_CHEK_SPIRIT,
#                    "D-TRON"      : DEVICE_NAME_ACCU_CHEK_DTRON,
                    "Aviva IC"    : DEVICE_NAME_ACCU_CHEK_AVIVA_INSIGHT,
                    "Performa IC" : DEVICE_NAME_ACCU_CHEK_PERFORMA_INSIGHT                
                }
                
                name = str(line.get("Name"))
                if name in device_name_map.keys():
                    self.model_name = device_name_map[name]
                    res[ "device_model" ] = self.model_name

        return res

    def eval_roche_smartpix_device_class_record(self, line):
        """
        Is this line a device model record. If so, return a dictionary with device
        model.
        """
        res = {}
        if line.tag == "DEVICE":
            res[ "device_class" ] = DEVICE_METER
        elif line.tag == "IP":
            res[ "device_class" ] = DEVICE_PUMP

        return res

    def eval_roche_smartpix_unit_record(self, line):
        """
        Check the unit used by this device
        """
        res = {}
        if line.tag == "DEVICE":
            res[ "meter_unit" ] = line.get("BGUnit")
            
            # Setup the factor (number of zeros added) depending on the unit
            if res["meter_unit"] == "mmol/L":
                self._global_bg_factor = VAL_FACTOR_GLUCOSE_MMOL
            else:
                self._global_bg_factor = VAL_FACTOR_GLUCOSE_MGDL        
        elif line.tag == "IP":
            res[ "meter_unit" ] = "U"

        return res

    def eval_roche_smartpix_version_record(self, line):
        """
        Check the version of the device
        """
        if line.tag == "ACSPIX":
            ver = str(line.get("Ver")).rstrip()
            typ = str(line.get("Type")).rstrip()
            if ver:
                is_smartpix2 = typ and typ == "2107"
                recommended = True
                # The version is on the format x.xx.xx
                # We require version 2.0 and up of smartpix 1 which has type "2106"
                # Smartpix2 is always recommended until proven guilty ;)
                m = re.match( r"(\d{1,2})\.(\d{2,2})\.(\d{2,2})", ver, re.IGNORECASE)
                if not is_smartpix2 and m and int(m.group(1)) < 2:
                    recommended = False

                return {ELEM_TERMINAL_VERSION : ver, PARAM_TERMINAL_VERSION_RECOMMENDED : recommended}

        return {}

    def eval_roche_smartpix_result_record(self, line):
        """
        Is this a result record? If so, return a dictonary with keys >
        
        date_time   > date in yyyy-mm-dd hh:mm:ss format
        value       > value
        unit        > unit if present (otherwise require headerunit)
        flags       > list of flags (int) if present
        """
        if line.tag == "BGDATA":
            res_list = []
            for child in line.getchildren():
                res_list.extend(self.eval_blood_glucose_record ( child ))
            return res_list
        elif line.tag == "DEVICE":
            # The device tag contains children settings
            settings = {}
            for child in line.getchildren():
                setting = self.eval_meter_settings_record( child )
                if setting:
                    settings.update(setting)

            # The tag itself might contain some settings as well:

            # Setup BG unit conversion
            bg_conv_factor = 1000.0
            if self._global_bg_factor == VAL_FACTOR_GLUCOSE_MGDL:
                bg_conv_factor = bg_conv_factor / VAL_FACTOR_CONV_MMOL_TO_MGDL
                
            trg_lo = line.get("TrgLo")
            if trg_lo:
                settings[SETTING_BG_GOAL_MIN] = int(float(trg_lo) * bg_conv_factor)

            trg_hi = line.get("TrgHi")
            if trg_hi:
                settings[SETTING_BG_GOAL_UPPER] = int(float(trg_hi) * bg_conv_factor)

            insulin_increment = line.get("InsulinIncrement")
            if insulin_increment:
                settings[SETTING_INSULIN_INCREMENT] = int(float(insulin_increment) * 1000)

            carb_unit = line.get("CarbUnit")
            carb_map = {'g': 0, 'BE': 1, 'CU': 2, 'BW': 3, 'CC': 4, 'KE': 5 }
            if carb_unit in carb_map.keys():
                settings[SETTING_CARB_UNIT] = carb_map[carb_unit]

            if settings:
                return [{SETTINGS_LIST: settings}]
            else:
                return {}
        elif line.tag == "IP":
            res_list = []
            for child in line.getchildren():
                res_list += self.eval_insulin_pump_record ( child )
            return res_list
        elif line.tag == "IPDATA":
            res_list = []
            for child in line.getchildren():
                res_list += self.eval_insulin_data_record ( child )
            return res_list
        elif line.tag == "ACSPIX":
            # Not a result...the smartpix info...
            return {}
        else:
            print "Unhandled tag:", line.tag
            return {}

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectRocheSmartpix( in_list ):
    """
    Detect if data comes from an Smartpix Device. 
    """
    return DetectDevice( "RocheSmartpix", in_list, DEVICE_TOO_EARLY )

def AnalyseRocheSmartpix( in_data ):
    """
    Analyse Roche Smartpix data
    """

    splits = []

    import string

    # Smart Pix 2 can deliver multiple files. Each file should be 
    # analysed seperately. 
    mark  = '<?xml version="1.0" encoding="'
    index = len(mark)
    left  = 0
    while index >= 0:
        index = string.find(in_data, mark, index)
        if index >= 0:
            splits.append(in_data[left:index])
            left  = index
            index += len(mark)
        else:
            splits.append(in_data[left:])

    res_list = []

    for split in splits:
 
        analyzer = SmartPixAnalyzer()

        d = {}

        d[ "eval_serial_record" ]       = analyzer.eval_roche_smartpix_serial_record
        d[ "eval_device_model_record" ] = analyzer.eval_roche_smartpix_device_model_record
        d[ "eval_unit"]                 = analyzer.eval_roche_smartpix_unit_record
        d[ "eval_result_record" ]       = analyzer.eval_roche_smartpix_result_record
        d[ "eval_device_class_record" ] = analyzer.eval_roche_smartpix_device_class_record
        d[ "eval_terminal_version" ]    = analyzer.eval_roche_smartpix_version_record
        
        (data_type, in_list) = analyzer.split_data(split)

        # We include the device list file (an XML file with data regarding the 
        # attached devices to the Smart Pix) in the transfer but do not 
        # currently use it. 
        if data_type != 'DEVICE_LIST':
            res_list += AnalyseGenericMeter(in_list, d)
    
    return res_list

if __name__ == "__main__":

    test_files = (
        'ActiveLCM-G000011819.xml', 
        'Aviva-G0006236.xml', 
        'Aviva-via-SmartPix1.log', 
        'Aviva-via-SmartPix2.log', 
        'AvivaCombo-G0400043.xml', 
        'AvivaConnect-G00006162.xml', 
        'CompactPlus-G0523452.xml', 
        'Insight-48200000000.log', 
        'Insight-ticket4195-Z34714.log', 
        'Insight-via-SmartPix2.log', 
        'InsightDataManager-G00002282.xml', 
        'InsightDataManager-I10240853.xml', 
        'Mobile-G100445.xml', 
        'MobileLCM-G000034261.xml', 
        'Performa-G1350932.xml', 
        'PerformaFacelift-G0038967.xml', 
        'RocheSmartpixMeter-also-transferred-with-Transmitter.xml', 
        'RocheSmartpixMeter.xml', 
        'RocheSmartpixMeterMgdl.xml', 
        'RocheSmartpixPumpExtendedBolus.xml', 
        'RocheSmartpixPumpSpiritCombo.xml', 
        'RocheSmartpixPumpSpiritCombo1.xml', 
        'Spirit-I2067645.xml', 
        'SpiritCombo-ILT_S_NO.xml', 
        'SpiritCombo-via-SmartPix1.log', 
        'SpiritCombo-via-SmartPix2.log', 
        'Voicemate-G0001281.xml'
    )

    for test_file in test_files:
        with open('test/testcases/test_data/RocheSmartpix/%s' % (test_file), 'r') as f:
            payload = f.read()

        res = AnalyseRocheSmartpix(payload)
        print res[0]['header']

        #import json, md5
        #m = md5.new()
        #try:
        #    m.update(json.dumps(res))
        #    print '%s MD5 %s' % (test_file, m.hexdigest())
        #except TypeError:
        #    print '%s MD5 unknown' % (test_file)
