# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2007 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------
# Supported devices:
# @DEVICE AgaMatrix WaveSense KeyNote (a.k.a Wellion Linus)

import re
from Generic import *
from Defines import *
from datetime import datetime

# -----------------------------------------------------------------------------
# Local variables
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a KeyNote serial nr record. Extract groups : 
    group 1 : serial number
    
    Serial number is a string _upto_ 20 characters. The character in position 2 is 
    either an E or an F. 
   
    example
    !TAG!SERNO!AFBE2796501051   
    """

    m = re.match( r'^!TAG!SERNO!(\w[EF]\w{8,18})', line, re.IGNORECASE )
    return m

def EvalResultRecord( line ):
    """
    Evaluate a KeyNote result record. Extract groups : 
    group 1 : glucose value in mg/dL
    group 2 : hexadecimal flag value
    group 3 : year (0..99)
    group 4 : month (1-12)
    group 5 : day (1-31)
    group 6 : hour (0-23)
    group 7 : minute (0-59)
    group 8 : seconds (0-58 even seconds only)
    group 9 : checksum 
        
    """
    m = re.match( r"^\d{1,3} (-?\d{1,4}) 0x([\da-f]{1,2}) \d{1,3} (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2}) (\d{1,2}) 0x([\da-f]{1,4})", line, re.IGNORECASE )
    return m

def EvalNrResultRecord( line ):
    """
    Evaluate a KeyNote nr of result record. Extract groups : 
    group 1 : Number of results

    example
    !TAG!RESULTS!300
    """

    m = re.match( r'^!TAG!RESULTS!(\d{1,3})', line, re.IGNORECASE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalAgaMatrixKeyNoteSerialRecord( line ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(1)
    return res
    
def EvalAgaMatrixKeyNoteUnitRecord( line ):
    """
    Always return mg/dl 
    """
    res = { "meter_unit":"mg/dL" }
    return res

def EvalAgaMatrixKeyNoteResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >
    
    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float) 
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    res = {}
    m = EvalResultRecord( line )
    
    if m:
        try:
            res[ ELEM_TIMESTAMP ] = datetime(2000 + int(m.group(3)), int(m.group(4)), int(m.group(5)), int(m.group(6)), int(m.group(7)), int(m.group(8)) )
        except:
            res[ ELEM_TIMESTAMP ] = None
        
                
        _flags = []
        res["meter_flaglist"] = _flags

        # Check status byte 
        # Bit 0x01 - Temperature out of range
        # Bit 0x02 - Control solution 
        # Bit 0x04 - High
        # Bit 0x08 - Low
        # Bit 0x10 - Invalid time
        # Bit 0x20 - Ketone 
        _status_byte_value = int(m.group(2),16)

        if _status_byte_value & 0x01:
            _flags.append(FLAG_RESULT_OUTSIDE_TEMP)    

        if _status_byte_value & 0x02:
            _flags.append(FLAG_RESULT_CTRL)    

        if _status_byte_value & 0x04:
            _flags.append(FLAG_RESULT_HIGH)    

        if _status_byte_value & 0x08:
            _flags.append(FLAG_RESULT_LOW)    

        if _status_byte_value & 0x10:
            _flags.append(FLAG_RESULT_ERRONEOUS)    

        if _status_byte_value & 0x20:
            _flags.append(FLAG_POSSIBLE_KETONES)
            
        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        
        try:
            res[ ELEM_VAL ] = float( m.group(1) )
            # Negative values might exist (with flags of course!)
            if res[ ELEM_VAL ] < 0:
                res[ ELEM_VAL ] = 0.0
        except ValueError:
            res[ ELEM_VAL ] = 0.0

    return res

def EvalAgaMatrixKeyNoteChecksumRecord( line, record ):
    """
    Evaluate checksum of result record. Not applicable for this meter.
    """
    return True

def EvalAgaMatrixKeyNoteChecksumFile( inList ):
    """
    Evaluate checksum of result record.
    """
    return True

def EvalAgaMatrixKeyNoteNrResultsRecord( line ):
    """
    Is this line a nr results. If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalNrResultRecord( line )
    if m:
        try:
            res[ "meter_nr_results" ] = int( m.group(1), 10 )
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
    return res


# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------
def DetectAgaMatrixKeyNote( inList ):
    """
    Detect if data comes from a Freestyle.
    """
    return DetectDevice( 'KEYNOTE', inList, DEVICE_METER );

def AnalyseAgaMatrixKeyNote( inData ):
    """
    Analyse KeyNote
    """

    inList = inData.split('\n')
    
    # Empty dictionary
    d = {}

    d[ "meter_type" ]           = "Linus"
    d[ "eval_serial_record" ]   = EvalAgaMatrixKeyNoteSerialRecord
    d[ "eval_unit"]             = EvalAgaMatrixKeyNoteUnitRecord
    d[ "eval_result_record" ]   = EvalAgaMatrixKeyNoteResultRecord
    d[ "eval_checksum_record" ] = EvalAgaMatrixKeyNoteChecksumRecord
    d[ "eval_checksum_file" ]   = EvalAgaMatrixKeyNoteChecksumFile
    d[ "eval_nr_results" ]      = EvalAgaMatrixKeyNoteNrResultsRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":
    
    _body = '!TAG!SERNO!AFBE2796501051\n'
    _body += '!TAG!RESULTS!300\n'
    _body += '0 50 0x0 200 6 6 7 23 59 0 0x7af9\n'
    _body += '1 298 0x2 200 6 6 6 22 58 0 0x3183\n'
    _body += '2 150 0x0 200 6 6 6 21 57 0 0x7c9d\n'
    _body += '3 296 0x13 200 6 6 5 20 56 0 0x40ec\n'
    _body += '4 200 0x0 200 6 6 5 19 55 0 0x75d\n'
    _body += '5 175 0x2 200 6 6 4 18 54 0 0x2e9f\n'
    _body += '6 293 0x20 200 6 6 4 17 53 0 0x326b\n'
    _body += '7 292 0x12 200 6 6 3 16 52 0 0x504b\n'
    _body += '8 225 0x0 200 6 6 3 15 51 0 0x2ace\n'
    _body += '9 25 0x2 200 6 6 2 14 50 0 0x6cdd\n'
    _body += '10 100 0x0 200 6 6 2 13 49 0 0xdcf7\n'
    _body += '11 225 0x13 200 6 6 1 12 48 0 0xcf57\n'
    _body += '12 175 0x0 200 6 6 1 11 47 0 0xc58b\n'
    _body += '13 125 0x2 200 6 6 30 10 46 0 0x655d\n'
    _body += '14 75 0x0 200 6 6 30 9 45 0 0x5eac\n'
    _body += '15 284 0x12 200 6 6 29 8 44 0 0xd113\n'
    _body += '16 283 0x20 200 6 6 29 7 43 0 0xc6ac\n'
    _body += '17 225 0x2 200 6 6 28 6 42 0 0xab46\n'
    _body += '18 281 0x20 200 6 6 28 5 41 0 0xdead\n'
    _body += '19 175 0x13 200 6 6 27 4 40 0 0x3796\n'
    _body += '20 625 0x24 200 6 6 27 3 39 0 0x17d6\n'
    _body += '21 278 0x2 200 6 6 26 2 38 0 0xcc88\n'
    _body += '22 277 0x20 200 6 6 26 1 37 0 0xfa3a\n'
    _body += '23 50 0x12 200 6 6 25 0 36 0 0x8527\n'
    _body += '24 150 0x0 200 6 6 25 23 35 0 0xf786\n'
    _body += '25 274 0x2 200 6 6 24 22 34 0 0x6c85\n'
    _body += '26 125 0x0 200 6 6 24 21 33 0 0x1855\n'
    _body += '27 100 0x13 200 6 6 23 20 32 0 0x10e\n'
    _body += '28 271 0x20 200 6 6 23 19 31 0 0x7d72\n'
    _body += '29 200 0x2 200 6 6 22 18 30 0 0x9969\n'
    _body += '30 269 0x20 200 6 6 22 17 29 0 0xef37\n'
    _body += '31 268 0x12 200 6 6 21 16 28 0 0x38f6\n'
    _body += '32 225 0x0 200 6 6 21 15 27 0 0x7678\n'
    _body += '33 75 0x2 200 6 6 20 14 26 0 0xb164\n'
    _body += '34 200 0x0 200 6 6 20 13 25 0 0x5cc0\n'
    _body += '35 150 0x13 200 6 6 19 12 24 0 0x1482\n'
    _body += '36 263 0x20 200 6 6 19 11 23 0 0xe8cb\n'
    _body += '37 262 0x2 200 6 6 18 10 22 0 0xf55\n'
    _body += '38 25 0x0 200 6 6 18 9 21 0 0x5a68\n'
    _body += '39 125 0x12 200 6 6 17 8 20 0 0x39d\n'
    _body += '40 175 0x0 200 6 6 17 7 19 0 0x37a4\n'
    _body += '41 225 0x2 200 6 6 16 6 18 0 0xfa67\n'
    _body += '42 257 0x20 200 6 6 16 5 17 0 0xb011\n'
    _body += '43 256 0x13 200 6 6 15 4 16 0 0xd63c\n'
    _body += '44 100 0x0 200 6 6 15 3 15 0 0xce45\n'
    _body += '45 254 0x2 200 6 6 14 2 14 0 0xdf8a\n'
    _body += '46 50 0x0 200 6 6 14 1 13 0 0x77c5\n'
    _body += '47 175 0x12 200 6 6 13 0 12 0 0xf85e\n'
    _body += '48 251 0x20 200 6 6 13 23 11 0 0x4469\n'
    _body += '49 200 0x2 200 6 6 12 22 10 0 0xa869\n'
    _body += '50 225 0x0 200 6 6 12 21 9 0 0x9fc1\n'
    _body += '51 625 0x17 200 6 6 11 20 8 0 0x3d1d\n'
    _body += '52 75 0x0 200 6 6 11 19 7 0 0x27fe\n'
    _body += '53 225 0x2 200 6 6 10 18 6 0 0x1bf2\n'
    _body += '54 175 0x0 200 6 6 10 17 5 0 0x7660\n'
    _body += '55 244 0x12 200 6 6 9 16 4 0 0x4d59\n'
    _body += '56 225 0x0 200 6 6 9 15 3 0 0x8eb1\n'
    _body += '57 150 0x2 200 6 6 8 14 2 0 0x452c\n'
    _body += '58 241 0x20 200 6 6 8 13 1 0 0xc2be\n'
    _body += '59 200 0x13 200 6 6 7 12 0 0 0x504d\n'
    _body += '60 239 0x0 200 6 6 7 11 59 0 0x93a1\n'
    _body += '61 100 0x2 200 6 6 6 10 58 0 0xc8a7\n'
    _body += '62 225 0x0 200 6 6 6 9 57 0 0x915\n'
    _body += '63 236 0x12 200 6 6 5 8 56 0 0xf2dd\n'
    _body += '64 200 0x0 200 6 6 5 7 55 0 0xbe5\n'
    _body += '65 125 0x2 200 6 6 4 6 54 0 0xd6b6\n'
    _body += '66 233 0x0 200 6 6 4 5 53 0 0x7b68\n'
    _body += '67 25 0x13 200 6 6 3 4 52 0 0x1230\n'
    _body += '68 150 0x0 200 6 6 3 3 51 0 0x8121\n'
    _body += '69 50 0x2 200 6 6 2 2 50 0 0xcef8\n'
    _body += '70 229 0x0 200 6 6 2 1 49 0 0x9c32\n'
    _body += '71 75 0x12 200 6 6 1 0 48 0 0x11f1\n'
    _body += '72 227 0x0 200 6 6 1 23 47 0 0xa6ca\n'
    _body += '73 226 0x2 200 6 6 30 22 46 0 0xaff\n'
    _body += '74 200 0x0 200 6 5 31 21 45 0 0x6506\n'
    _body += '75 175 0x13 200 6 5 30 20 44 0 0x3404\n'
    _body += '76 223 0x0 200 6 5 30 19 43 0 0x1d0\n'
    _body += '77 225 0x2 200 6 5 29 18 42 0 0xf4f0\n'
    _body += '78 100 0x0 200 6 5 29 17 41 0 0x6f23\n'
    _body += '79 150 0x12 200 6 5 28 16 40 0 0xabbe\n'
    _body += '80 225 0x0 200 6 5 28 15 39 0 0x36ec\n'
    _body += '81 218 0x2 200 6 5 27 14 38 0 0x4f86\n'
    _body += '82 625 0x24 200 6 5 27 13 37 0 0x889f\n'
    _body += '83 225 0x13 200 6 5 26 12 36 0 0xda8\n'
    _body += '84 200 0x0 200 6 5 26 11 35 0 0x79b1\n'
    _body += '85 214 0x2 200 6 5 25 10 34 0 0xe710\n'
    _body += '86 225 0x0 200 6 5 25 9 33 0 0x23a0\n'
    _body += '87 212 0x12 200 6 5 24 8 32 0 0x68ad\n'
    _body += '88 211 0x0 200 6 5 24 7 31 0 0xea4a\n'
    _body += '89 175 0x2 200 6 5 23 6 30 0 0x6fd1\n'
    _body += '90 75 0x0 200 6 5 23 5 29 0 0xa8f7\n'
    _body += '91 125 0x13 200 6 5 22 4 28 0 0x8f0b\n'
    _body += '92 50 0x0 200 6 5 22 3 27 0 0xa00\n'
    _body += '93 206 0x2 200 6 5 21 2 26 0 0x1d5d\n'
    _body += '94 200 0x0 200 6 5 21 1 25 0 0x7883\n'
    _body += '95 100 0x12 200 6 5 20 0 24 0 0xa76\n'
    _body += '96 25 0x0 200 6 5 20 23 23 0 0x2521\n'
    _body += '97 202 0x2 200 6 5 19 22 22 0 0x84c0\n'
    _body += '98 225 0x0 200 6 5 19 21 21 0 0xf9ba\n'
    _body += '99 200 0x13 200 6 5 18 20 20 0 0xaa7c\n'
    _body += '100 199 0x0 200 6 5 18 19 19 0 0x337c\n'
    _body += '101 150 0x2 200 6 5 17 18 18 0 0xa0e\n'
    _body += '102 197 0x0 200 6 5 17 17 17 0 0x8489\n'
    _body += '103 175 0x12 200 6 5 16 16 16 0 0xc23\n'
    _body += '104 125 0x0 200 6 5 16 15 15 0 0x3816\n'
    _body += '105 194 0x2 200 6 5 15 14 14 0 0x9ebc\n'
    _body += '106 193 0x0 200 6 5 15 13 13 0 0xa43e\n'
    _body += '107 225 0x13 200 6 5 14 12 12 0 0x556\n'
    _body += '108 191 0x0 200 6 5 14 11 11 0 0x3859\n'
    _body += '109 75 0x2 200 6 5 13 10 10 0 0xa9d6\n'
    _body += '110 175 0x0 200 6 5 13 9 9 0 0xa95f\n'
    _body += '111 188 0x12 200 6 5 12 8 8 0 0x7ec7\n'
    _body += '112 100 0x0 200 6 5 12 7 7 0 0x82b6\n'
    _body += '113 625 0x6 200 6 5 11 6 6 0 0xcf9c\n'
    _body += '114 200 0x0 200 6 5 11 5 5 0 0x9d63\n'
    _body += '115 50 0x13 200 6 5 10 4 4 0 0x2ab8\n'
    _body += '116 225 0x0 200 6 5 10 3 3 0 0xd504\n'
    _body += '117 125 0x2 200 6 5 9 2 2 0 0x6f1\n'
    _body += '118 181 0x0 200 6 5 9 1 1 0 0x900b\n'
    _body += '119 200 0x12 200 6 5 8 0 0 0 0x22a1\n'
    _body += '120 179 0x0 200 6 5 8 23 59 0 0x139c\n'
    _body += '121 178 0x2 200 6 5 7 22 58 0 0xf309\n'
    _body += '122 225 0x0 200 6 5 7 21 57 0 0x1d4e\n'
    _body += '123 150 0x13 200 6 5 6 20 56 0 0x5524\n'
    _body += '124 175 0x0 200 6 5 6 19 55 0 0xb804\n'
    _body += '125 25 0x2 200 6 5 5 18 54 0 0x92c7\n'
    _body += '126 173 0x0 200 6 5 5 17 53 0 0xe7f8\n'
    _body += '127 172 0x12 200 6 5 4 16 52 0 0x2dcc\n'
    _body += '128 75 0x0 200 6 5 4 15 51 0 0x6820\n'
    _body += '129 100 0x2 200 6 5 3 14 50 0 0xe63a\n'
    _body += '130 125 0x0 200 6 5 3 13 49 0 0x4daa\n'
    _body += '131 175 0x13 200 6 5 2 12 48 0 0xabd8\n'
    _body += '132 167 0x0 200 6 5 2 11 47 0 0xc189\n'
    _body += '133 166 0x2 200 6 5 1 10 46 0 0xc662\n'
    _body += '134 150 0x0 200 6 5 1 9 45 0 0x4391\n'
    _body += '135 164 0x12 200 6 5 31 8 44 0 0x95c6\n'
    _body += '136 163 0x0 200 6 5 31 7 43 0 0x49fe\n'
    _body += '137 225 0x2 200 6 5 30 6 42 0 0x7383\n'
    _body += '138 50 0x0 200 6 5 30 5 41 0 0xb3e1\n'
    _body += '139 200 0x13 200 6 5 29 4 40 0 0x9a07\n'
    _body += '140 225 0x0 200 6 5 29 3 39 0 0xb80c\n'
    _body += '141 158 0x2 200 6 5 28 2 38 0 0xbd91\n'
    _body += '142 157 0x0 200 6 5 28 1 37 0 0x9a17\n'
    _body += '143 125 0x12 200 6 5 27 0 36 0 0x5023\n'
    _body += '144 625 0x24 200 6 5 27 23 35 0 0x87a6\n'
    _body += '145 150 0x2 200 6 5 26 22 34 0 0xfabc\n'
    _body += '146 100 0x0 200 6 5 26 21 33 0 0xf472\n'
    _body += '147 75 0x13 200 6 5 25 20 32 0 0x3313\n'
    _body += '148 151 0x0 200 6 5 25 19 31 0 0x7edf\n'
    _body += '149 200 0x2 200 6 4 24 18 30 0 0x96be\n'
    _body += '150 149 0x0 200 6 4 24 17 29 0 0x16a8\n'
    _body += '151 148 0x12 200 6 4 23 16 28 0 0xa13\n'
    _body += '152 175 0x0 200 6 4 23 15 27 0 0xd697\n'
    _body += '153 146 0x2 200 6 4 22 14 26 0 0x1672\n'
    _body += '154 25 0x0 200 6 4 22 13 25 0 0x762d\n'
    _body += '155 225 0x13 200 6 4 21 12 24 0 0xb705\n'
    _body += '156 125 0x0 200 6 4 21 11 23 0 0xcd76\n'
    _body += '157 142 0x2 200 6 4 20 10 22 0 0x36c5\n'
    _body += '158 225 0x0 200 6 4 20 9 21 0 0x611e\n'
    _body += '159 175 0x12 200 6 4 19 8 20 0 0xbd09\n'
    _body += '160 139 0x0 200 6 4 19 7 19 0 0xab1d\n'
    _body += '161 50 0x2 200 6 4 18 6 18 0 0x2cb7\n'
    _body += '162 137 0x0 200 6 4 18 5 17 0 0xd19c\n'
    _body += '163 100 0x13 200 6 4 17 4 16 0 0x4bf7\n'
    _body += '164 200 0x0 200 6 4 17 3 15 0 0x71aa\n'
    _body += '165 134 0x2 200 6 4 16 2 14 0 0xedfc\n'
    _body += '166 75 0x0 200 6 4 16 1 13 0 0x39a8\n'
    _body += '167 150 0x12 200 6 4 15 0 12 0 0x7d48\n'
    _body += '168 131 0x0 200 6 4 15 23 11 0 0xf7e5\n'
    _body += '169 125 0x2 200 6 4 14 22 10 0 0xb7ce\n'
    _body += '170 225 0x0 200 6 4 14 21 9 0 0x5843\n'
    _body += '171 128 0x13 200 6 4 13 20 8 0 0xdd1b\n'
    _body += '172 127 0x0 200 6 4 13 19 7 0 0x5dde\n'
    _body += '173 175 0x2 200 6 4 12 18 6 0 0xe758\n'
    _body += '174 200 0x0 200 6 4 12 17 5 0 0xeaad\n'
    _body += '175 625 0x16 200 6 4 11 16 4 0 0x353c\n'
    _body += '176 225 0x0 200 6 4 11 15 3 0 0x427f\n'
    _body += '177 122 0x2 200 6 4 10 14 2 0 0xb431\n'
    _body += '178 150 0x0 200 6 4 10 13 1 0 0xe18c\n'
    _body += '179 200 0x13 200 6 4 9 12 0 0 0x1869\n'
    _body += '180 100 0x0 200 6 4 9 11 59 0 0x8617\n'
    _body += '181 118 0x2 200 6 4 8 10 58 0 0x85f8\n'
    _body += '182 125 0x0 200 6 4 8 9 57 0 0x5c5b\n'
    _body += '183 25 0x12 200 6 4 7 8 56 0 0x7527\n'
    _body += '184 50 0x0 200 6 4 7 7 55 0 0xe14a\n'
    _body += '185 75 0x2 200 6 4 6 6 54 0 0x4bc2\n'
    _body += '186 113 0x0 200 6 4 6 5 53 0 0xf530\n'
    _body += '187 175 0x13 200 6 4 5 4 52 0 0xa201\n'
    _body += '188 225 0x0 200 6 4 5 3 51 0 0x999f\n'
    _body += '189 150 0x2 200 6 4 4 2 50 0 0xafc3\n'
    _body += '190 109 0x0 200 6 4 4 1 49 0 0x8e85\n'
    _body += '191 225 0x12 200 6 4 3 0 48 0 0xa9f3\n'
    _body += '192 107 0x0 200 6 4 3 23 47 0 0x8e8c\n'
    _body += '193 106 0x2 200 6 4 2 22 46 0 0x57ed\n'
    _body += '194 175 0x0 200 6 4 2 21 45 0 0x1111\n'
    _body += '195 125 0x13 200 6 4 1 20 44 0 0x6a5c\n'
    _body += '196 103 0x0 200 6 4 1 19 43 0 0xef90\n'
    _body += '197 100 0x2 200 6 4 30 18 42 0 0xc68e\n'
    _body += '198 101 0x0 200 6 4 30 17 41 0 0x2ce5\n'
    _body += '199 200 0x12 200 6 4 29 16 40 0 0xbccf\n'
    _body += '200 150 0x0 200 6 4 29 15 39 0 0x1f2f\n'
    _body += '201 175 0x2 200 6 4 28 14 38 0 0xfe32\n'
    _body += '202 97 0x0 200 6 4 28 13 37 0 0x3ad6\n'
    _body += '203 225 0x13 200 6 4 27 12 36 0 0xc8c4\n'
    _body += '204 75 0x0 200 6 4 27 11 35 0 0x5b74\n'
    _body += '205 94 0x2 200 6 4 26 10 34 0 0xca81\n'
    _body += '206 625 0x24 200 6 4 26 9 33 0 0x3364\n'
    _body += '207 50 0x12 200 6 4 25 8 32 0 0xc800\n'
    _body += '208 125 0x0 200 6 4 25 7 31 0 0x72b5\n'
    _body += '209 200 0x2 200 6 4 24 6 30 0 0x9c54\n'
    _body += '210 89 0x0 200 6 4 24 5 29 0 0x39f6\n'
    _body += '211 150 0x13 200 6 4 23 4 28 0 0x5b12\n'
    _body += '212 25 0x0 200 6 4 23 3 27 0 0x5351\n'
    _body += '213 86 0x2 200 6 4 22 2 26 0 0xd728\n'
    _body += '214 100 0x0 200 6 4 22 1 25 0 0x34cb\n'
    _body += '215 175 0x12 200 6 4 21 0 24 0 0xde6f\n'
    _body += '216 83 0x0 200 6 4 21 23 23 0 0x7f2b\n'
    _body += '217 82 0x2 200 6 4 20 22 22 0 0x456\n'
    _body += '218 225 0x0 200 6 4 20 21 21 0 0xeb63\n'
    _body += '219 200 0x13 200 6 4 19 20 20 0 0x6f10\n'
    _body += '220 79 0x0 200 6 4 19 19 19 0 0xeac8\n'
    _body += '221 125 0x2 200 6 4 18 18 18 0 0xbfb9\n'
    _body += '222 150 0x0 200 6 4 18 17 17 0 0x6d79\n'
    _body += '223 75 0x12 200 6 4 17 16 16 0 0xa537\n'
    _body += '224 200 0x0 200 6 3 17 15 15 0 0x7c50\n'
    _body += '225 74 0x2 200 6 3 16 14 14 0 0xe653\n'
    _body += '226 73 0x0 200 6 3 16 13 13 0 0xb066\n'
    _body += '227 225 0x13 200 6 3 15 12 12 0 0x2c1\n'
    _body += '228 71 0x0 200 6 3 15 11 11 0 0xe8b3\n'
    _body += '229 175 0x2 200 6 3 14 10 10 0 0x4ed1\n'
    _body += '230 50 0x0 200 6 3 14 9 9 0 0xdc20\n'
    _body += '231 100 0x12 200 6 3 13 8 8 0 0x87f7\n'
    _body += '232 67 0x0 200 6 3 13 7 7 0 0x337\n'
    _body += '233 150 0x2 200 6 3 12 6 6 0 0x5a03\n'
    _body += '234 125 0x0 200 6 3 12 5 5 0 0xad1d\n'
    _body += '235 64 0x13 200 6 3 11 4 4 0 0xc66c\n'
    _body += '236 175 0x0 200 6 3 11 3 3 0 0xed3\n'
    _body += '237 625 0x6 200 6 3 10 2 2 0 0xa850\n'
    _body += '238 61 0x0 200 6 3 10 1 1 0 0x546f\n'
    _body += '239 200 0x12 200 6 3 9 0 0 0 0x5eee\n'
    _body += '240 59 0x0 200 6 3 9 23 59 0 0x89a8\n'
    _body += '241 25 0x2 200 6 3 8 22 58 0 0xea20\n'
    _body += '242 75 0x0 200 6 3 8 21 57 0 0xf1c5\n'
    _body += '243 175 0x13 200 6 3 7 20 56 0 0x8ee1\n'
    _body += '244 150 0x0 200 6 3 7 19 55 0 0xa52f\n'
    _body += '245 225 0x2 200 6 3 6 18 54 0 0x8ced\n'
    _body += '246 53 0x0 200 6 3 6 17 53 0 0x76a\n'
    _body += '247 125 0x12 200 6 3 5 16 52 0 0x825\n'
    _body += '248 100 0x0 200 6 3 5 15 51 0 0x6ab\n'
    _body += '249 200 0x2 200 6 3 4 14 50 0 0xc4b\n'
    _body += '250 175 0x0 200 6 3 4 13 49 0 0x1c\n'
    _body += '251 225 0x13 200 6 3 3 12 48 0 0x79b6\n'
    _body += '252 47 0x0 200 6 3 3 11 47 0 0xec2b\n'
    _body += '253 50 0x2 200 6 3 2 10 46 0 0xf306\n'
    _body += '254 200 0x0 200 6 3 2 9 45 0 0x6081\n'
    _body += '255 150 0x12 200 6 3 1 8 44 0 0xd04b\n'
    _body += '256 43 0x0 200 6 3 1 7 43 0 0x4938\n'
    _body += '257 175 0x2 200 6 3 31 6 42 0 0x2d6e\n'
    _body += '258 41 0x0 200 6 3 31 5 41 0 0xfb38\n'
    _body += '259 200 0x13 200 6 3 30 4 40 0 0x88b8\n'
    _body += '260 125 0x0 200 6 3 30 3 39 0 0x10e5\n'
    _body += '261 75 0x2 200 6 3 29 2 38 0 0x9a39\n'
    _body += '262 37 0x0 200 6 3 29 1 37 0 0xaa6e\n'
    _body += '263 225 0x12 200 6 3 28 0 36 0 0x6c87\n'
    _body += '264 175 0x0 200 6 3 28 23 35 0 0x521f\n'
    _body += '265 100 0x2 200 6 3 27 22 34 0 0xb751\n'
    _body += '266 150 0x0 200 6 3 27 21 33 0 0xb99f\n'
    _body += '267 32 0x13 200 6 3 26 20 32 0 0xbd0f\n'
    _body += '268 625 0x24 200 6 3 26 19 31 0 0x1d7f\n'
    _body += '269 200 0x2 200 6 3 25 18 30 0 0xa397\n'
    _body += '270 25 0x0 200 6 3 25 17 29 0 0x56b7\n'
    _body += '271 175 0x12 200 6 3 24 16 28 0 0xaf8\n'
    _body += '272 225 0x0 200 6 3 24 15 27 0 0x920c\n'
    _body += '273 125 0x2 200 6 3 23 14 26 0 0x34a2\n'
    _body += '274 200 0x0 200 6 3 23 13 25 0 0xcb0b\n'
    _body += '275 225 0x13 200 6 3 22 12 24 0 0x2b7b\n'
    _body += '276 50 0x0 200 6 3 22 11 23 0 0x6eba\n'
    _body += '277 150 0x2 200 6 3 21 10 22 0 0x24e5\n'
    _body += '278 175 0x0 200 6 3 21 9 21 0 0xcf7d\n'
    _body += '279 200 0x12 200 6 3 20 8 20 0 0x5f9e\n'
    _body += '280 75 0x0 200 6 3 20 7 19 0 0x828c\n'
    _body += '281 225 0x2 200 6 3 19 6 18 0 0x1992\n'
    _body += '282 100 0x0 200 6 3 19 5 17 0 0x97c1\n'
    _body += '283 16 0x1b 200 6 3 18 4 16 0 0x5c48\n'
    _body += '284 200 0x0 200 6 3 18 3 15 0 0xf55\n'
    _body += '285 175 0x2 200 6 3 17 2 14 0 0xd186\n'
    _body += '286 125 0x0 200 6 3 17 1 13 0 0x1db6\n'
    _body += '287 225 0x12 200 6 3 16 0 12 0 0xad0c\n'
    _body += '288 150 0x0 200 6 3 16 23 11 0 0xec53\n'
    _body += '289 200 0x2 200 6 3 15 22 10 0 0xda6f\n'
    _body += '290 225 0x0 200 6 3 15 21 9 0 0x55cb\n'
    _body += '291 8 0x1b 200 6 3 14 20 8 0 0x4c70\n'
    _body += '292 175 0x0 200 6 3 14 19 7 0 0x279d\n'
    _body += '293 225 0x2 200 6 3 13 18 6 0 0x4d17\n'
    _body += '294 200 0x0 200 6 3 13 17 5 0 0xe725\n'
    _body += '295 4 0x1a 200 6 3 12 16 4 0 0xcf7\n'
    _body += '296 225 0x0 200 6 3 12 15 3 0 0x8990\n'
    _body += '297 2 0xa 200 6 3 11 14 2 0 0x175c\n'
    _body += '298 1 0x8 200 6 3 11 13 1 0 0x519\n'
    _body += '299 625 0x17 200 6 2 10 12 0 0 0xd40e\n'
    _body += 'END\n'


    _body = '!TAG!SERNO!AFBE21Z6603359\r\n!TAG!RESULTS!249\r\n0 252 0x20 197 7 10 20 18 59 46 0xf9bf\r\n1 158 0x0 197 7 10 20 12 23 32 0x9e91\r\n2 138 0x0 197 7 10 20 9 29 16 0xf33a\r\n3 103 0x0 197 7 10 20 7 29 4 0x358c\r\n4 76 0x0 197 7 10 19 20 19 16 0x8e84\r\n5 262 0x20 197 7 10 19 18 45 54 0x344a\r\n6 233 0x0 197 7 10 19 17 41 16 0x16e4\r\n7 105 0x0 197 7 10 19 11 16 32 0x8d94\r\n8 88 0x0 197 7 10 19 6 46 32 0x9dc8\r\n9 87 0x0 197 7 10 18 21 33 52 0x7764\r\n10 69 0x0 197 7 10 18 19 25 20 0x95bb\r\n11 76 0x0 197 7 10 18 16 51 10 0xb8e7\r\n12 162 0x0 197 7 10 18 15 23 2 0x2f06\r\n13 157 0x0 197 7 10 18 12 4 6 0xd33b\r\n14 104 0x0 197 7 10 18 9 42 56 0x173a\r\n15 199 0x0 197 7 10 18 8 21 48 0x9ea6\r\n16 165 0x0 197 7 10 18 6 55 36 0x9f27\r\n17 290 0x20 197 7 10 18 5 54 14 0xdd45\r\n18 169 0x0 197 7 10 17 23 23 26 0xd1a3\r\n19 85 0x0 197 7 10 17 20 40 46 0x8cc3\r\n20 110 0x0 197 7 10 17 18 6 28 0xeabd\r\n21 168 0x0 197 7 10 17 11 31 50 0x1c1d\r\n22 164 0x0 197 7 10 17 10 17 8 0x9087\r\n23 74 0x0 197 7 10 17 5 37 48 0x809b\r\n24 107 0x0 197 7 10 17 1 31 6 0x5810\r\n25 159 0x0 197 7 10 16 21 41 30 0xf3cd\r\n26 84 0x0 197 7 10 16 18 52 34 0x655c\r\n27 276 0x20 197 7 10 16 14 48 52 0x599b\r\n28 119 0x0 197 7 10 16 11 21 50 0xd49b\r\n29 250 0x20 197 7 10 16 6 52 6 0x8e97\r\n30 187 0x0 197 7 10 16 0 35 38 0x2e40\r\n31 327 0x20 197 7 10 15 23 39 4 0xbabd\r\n32 279 0x20 197 7 10 15 22 35 54 0xa5bd\r\n33 107 0x0 197 7 10 15 20 22 30 0x80a4\r\n34 117 0x0 197 7 10 15 19 16 34 0xc1e3\r\n35 139 0x0 197 7 10 15 18 30 44 0x5600\r\n36 223 0x0 197 7 10 15 15 43 18 0xae53\r\n37 97 0x0 197 7 10 15 11 42 18 0xb60a\r\n38 157 0x0 197 7 10 15 7 0 56 0x2494\r\n39 320 0x20 197 7 10 15 6 9 6 0x48ab\r\n'
    _body += '40 207 0x0 197 7 10 15 1 37 8 0x60f2\r\n41 97 0x0 197 7 10 14 23 5 8 0x8a95\r\n42 124 0x0 197 7 10 14 20 59 38 0x93a2\r\n43 51 0x0 197 7 10 14 12 44 50 0x599f\r\n44 159 0x0 197 7 10 14 10 36 18 0x3829\r\n45 84 0x0 197 7 10 14 3 32 54 0x15c3\r\n46 44 0x0 197 7 10 13 17 9 50 0xa35c\r\n47 291 0x20 197 7 10 13 14 39 12 0x78c3\r\n48 81 0x0 197 7 10 13 9 9 26 0x9050\r\n49 -1 0x1 197 7 10 13 8 43 38 0x18a7\r\n50 190 0x0 197 7 10 13 5 4 22 0x757e\r\n51 67 0x0 197 7 10 12 20 22 14 0xd416\r\n52 136 0x0 197 7 10 12 18 51 56 0x9554\r\n53 100 0x0 197 7 10 12 16 44 48 0xe54c\r\n54 276 0x20 197 7 10 12 15 25 18 0xaa22\r\n55 319 0x20 197 7 10 12 14 29 32 0x7e53\r\n56 321 0x20 197 7 10 12 9 27 46 0xb49f\r\n57 69 0x0 197 7 10 12 2 53 26 0xc771\r\n58 180 0x0 197 7 10 11 22 0 28 0x3ae\r\n59 111 0x0 197 7 10 11 18 27 42 0x8312\r\n60 312 0x20 197 7 10 11 16 54 54 0x8b35\r\n61 232 0x0 197 7 10 11 11 18 14 0x9ddb\r\n62 104 0x0 197 7 10 11 8 4 10 0x15d0\r\n63 137 0x0 197 7 10 11 5 37 52 0x7a98\r\n64 233 0x0 197 7 10 10 21 27 14 0x4066\r\n65 169 0x0 197 7 10 10 16 40 44 0x13e5\r\n66 196 0x0 197 7 10 10 14 16 16 0x1760\r\n67 108 0x0 197 7 10 10 6 52 2 0x8ada\r\n68 243 0x20 197 7 10 9 23 28 50 0x6f46\r\n69 249 0x20 197 7 10 9 17 22 30 0xb6b4\r\n70 202 0x0 197 7 10 9 14 23 58 0x51f\r\n71 222 0x0 197 7 10 9 14 23 22 0x4c97\r\n72 155 0x0 197 7 10 9 11 4 40 0x2c11\r\n73 117 0x0 197 7 10 9 7 3 10 0x3120\r\n74 72 0x0 197 7 10 9 3 57 12 0x6181\r\n75 167 0x0 197 7 10 8 23 10 30 0x2e1d\r\n76 149 0x0 197 7 10 8 21 20 28 0xaa7\r\n77 172 0x0 197 7 10 8 20 1 6 0x1488\r\n78 305 0x20 197 7 10 8 17 33 14 0x68d9\r\n79 300 0x20 197 7 10 8 14 50 48 0xa3d5\r\n'
    _body += '80 149 0x0 197 7 10 8 12 46 28 0x32c0\r\n81 208 0x0 197 7 10 8 10 59 42 0x5d0e\r\n82 174 0x0 197 7 10 8 8 22 10 0x5804\r\n83 143 0x0 197 7 10 7 22 45 56 0xbc95\r\n84 339 0x20 197 7 10 7 20 55 12 0x2dcb\r\n85 254 0x20 197 7 10 7 17 5 40 0x356f\r\n86 109 0x0 197 7 10 7 7 34 40 0x7cba\r\n87 133 0x0 197 7 10 7 3 30 56 0xf5a9\r\n88 281 0x20 197 7 10 6 20 55 40 0x53d1\r\n89 284 0x20 197 7 10 6 15 16 18 0x72d6\r\n90 95 0x0 197 7 10 6 12 4 44 0x5c3b\r\n91 147 0x0 197 7 10 6 7 35 56 0xce6a\r\n92 171 0x0 197 7 10 5 23 3 0 0xd995\r\n93 196 0x0 197 7 10 5 19 11 54 0x13e8\r\n94 126 0x0 197 7 10 5 16 12 10 0x9f3b\r\n95 179 0x0 197 7 10 5 14 3 2 0xf8e1\r\n96 112 0x0 197 7 10 5 11 18 10 0xa2df\r\n97 78 0x0 197 7 10 5 7 37 12 0x1a84\r\n98 165 0x0 197 7 10 4 21 37 20 0x81f7\r\n99 89 0x0 197 7 10 4 17 14 12 0x4dc0\r\n100 84 0x0 197 7 10 4 15 44 2 0xfb4f\r\n101 142 0x0 197 7 10 4 13 41 40 0xb7\r\n102 119 0x0 197 7 10 4 11 13 0 0xfdc1\r\n103 78 0x0 196 7 10 4 6 51 42 0x7608\r\n104 164 0x0 196 7 10 3 22 5 44 0x82c1\r\n105 109 0x0 196 7 10 3 20 9 34 0x8bfd\r\n106 76 0x0 196 7 10 3 18 11 32 0x37e\r\n107 91 0x0 196 7 10 3 15 39 48 0xe7ea\r\n108 83 0x0 196 7 10 3 12 4 12 0xb7ba\r\n109 258 0x20 196 7 10 3 9 38 0 0x58a7\r\n110 248 0x20 197 7 10 3 9 37 20 0x9a3f\r\n111 204 0x0 197 7 10 3 7 22 14 0x7c68\r\n112 194 0x0 197 7 10 2 22 41 22 0xe8f5\r\n113 209 0x0 197 7 10 2 16 18 28 0x1a95\r\n114 159 0x0 197 7 10 2 10 44 44 0xb7a6\r\n115 138 0x0 197 7 10 2 8 37 10 0x431c\r\n116 226 0x0 197 7 10 2 7 35 58 0x998f\r\n117 149 0x0 197 7 10 1 21 46 14 0x4ff\r\n118 74 0x0 197 7 10 1 20 49 56 0xeb17\r\n119 87 0x0 197 7 10 1 19 11 14 0xb1b2\r\n'
    _body += '120 127 0x0 197 7 10 1 15 50 40 0xa11c\r\n121 222 0x0 197 7 10 1 11 17 52 0xc752\r\n122 81 0x0 197 7 10 1 8 47 46 0xa5ac\r\n123 121 0x0 197 7 10 1 5 37 26 0xaa41\r\n124 63 0x0 197 7 10 1 3 58 46 0xe9c2\r\n125 79 0x0 197 7 9 30 21 51 34 0xc933\r\n126 83 0x0 197 7 9 30 20 0 18 0xd715\r\n127 312 0x20 197 7 9 30 18 32 52 0xe3ed\r\n128 460 0x20 197 7 9 30 16 47 16 0xa4e\r\n129 411 0x20 197 7 9 30 16 46 38 0x89d3\r\n130 83 0x0 197 7 9 30 13 5 58 0x13eb\r\n131 259 0x20 197 7 9 30 11 34 54 0x6638\r\n132 321 0x20 197 7 9 30 10 30 0 0xa21b\r\n133 179 0x0 197 7 9 30 6 52 26 0x8b50\r\n134 380 0x20 197 7 9 29 22 34 14 0x7c41\r\n135 189 0x0 197 7 9 29 15 29 56 0xfa62\r\n136 146 0x0 197 7 9 29 9 43 20 0xc8d2\r\n137 148 0x0 197 7 9 29 6 52 16 0x7d8a\r\n138 320 0x20 197 7 9 29 4 58 36 0xc876\r\n139 234 0x0 197 7 9 28 21 55 16 0x400\r\n140 49 0x0 197 7 9 28 20 32 8 0xd4d6\r\n141 184 0x0 197 7 9 28 19 29 42 0x78a2\r\n142 195 0x0 197 7 9 28 17 15 4 0x8581\r\n143 222 0x0 197 7 9 28 13 54 46 0xeb7a\r\n144 99 0x0 197 7 9 28 11 41 42 0x3dda\r\n145 99 0x0 197 7 9 28 7 24 30 0x8098\r\n146 63 0x0 197 7 9 28 6 0 44 0xb4f8\r\n147 152 0x0 197 7 9 27 21 53 44 0xe0ef\r\n148 214 0x0 197 7 9 27 19 53 10 0x62e1\r\n149 60 0x0 197 7 9 27 16 16 26 0x8144\r\n150 198 0x0 197 7 9 27 11 50 12 0x56bd\r\n151 119 0x0 197 7 9 27 6 53 6 0xa54f\r\n152 251 0x20 197 7 9 26 22 18 12 0xe95a\r\n153 179 0x0 197 7 9 26 19 57 12 0x94de\r\n154 139 0x0 197 7 9 26 17 50 24 0xf428\r\n155 143 0x0 197 7 9 26 15 50 50 0x2ab9\r\n156 111 0x0 197 7 9 26 14 4 40 0x7c9d\r\n157 102 0x0 197 7 9 26 6 55 58 0x922e\r\n158 85 0x0 197 7 9 25 22 4 34 0x9f54\r\n159 222 0x0 197 7 9 25 19 4 12 0xba19\r\n'
    _body += '160 141 0x0 197 7 9 25 14 53 40 0x2ba8\r\n161 135 0x0 197 7 9 25 11 28 44 0x7987\r\n162 245 0x20 197 7 9 25 10 1 28 0x1f65\r\n163 71 0x0 197 7 9 25 8 41 34 0x7eec\r\n164 179 0x0 197 7 9 25 8 6 30 0xb5ca\r\n165 348 0x20 197 7 9 25 7 24 14 0x13b8\r\n166 100 0x0 197 7 9 25 0 8 12 0xfff2\r\n167 434 0x20 197 7 9 24 23 0 4 0x1784\r\n168 343 0x20 197 7 9 24 22 20 18 0x4224\r\n169 186 0x0 197 7 9 24 19 50 18 0xe305\r\n170 224 0x0 197 7 9 24 15 14 46 0xde87\r\n171 142 0x0 197 7 9 24 12 3 50 0x4063\r\n172 206 0x0 197 7 9 24 10 43 16 0x290d\r\n173 82 0x0 197 7 9 24 5 55 16 0xfbba\r\n174 172 0x0 197 7 9 23 22 11 28 0x7f85\r\n175 103 0x0 197 7 9 23 20 26 16 0xec04\r\n176 246 0x20 197 7 9 23 15 51 32 0xae79\r\n177 44 0x0 197 7 9 23 11 26 26 0x8c11\r\n178 82 0x0 197 7 9 23 8 16 28 0x7a35\r\n179 95 0x0 197 7 9 22 22 45 0 0x8ba3\r\n180 102 0x0 197 7 9 22 19 41 58 0x7f88\r\n181 122 0x0 197 7 9 22 15 30 58 0x2ffc\r\n182 87 0x0 197 7 9 22 13 56 54 0x9edf\r\n183 159 0x0 197 7 9 22 10 38 2 0xeabd\r\n184 201 0x0 197 7 9 21 22 28 58 0x7446\r\n185 95 0x0 197 7 9 21 20 55 24 0xc723\r\n186 120 0x0 197 7 9 21 18 41 20 0x18f5\r\n187 237 0x0 197 7 9 21 16 33 46 0x16a1\r\n188 160 0x0 197 7 9 21 13 13 22 0x8498\r\n189 254 0x20 197 7 9 21 11 10 2 0x1cd9\r\n190 79 0x0 197 7 9 21 6 53 40 0x60c6\r\n191 78 0x0 197 7 9 20 23 39 12 0x2a9\r\n192 124 0x0 197 7 9 20 19 36 22 0x750b\r\n193 289 0x20 197 7 9 20 16 11 40 0x277e\r\n194 178 0x0 197 7 9 20 13 51 2 0x24ff\r\n195 147 0x0 197 7 9 20 4 56 42 0x17f4\r\n196 260 0x20 197 7 9 19 23 22 52 0x8bdc\r\n197 303 0x20 197 7 9 19 21 31 18 0x61e\r\n198 117 0x0 197 7 9 19 18 9 38 0x3922\r\n199 77 0x0 197 7 9 19 11 50 56 0x5bb4\r\n'
    _body += '200 128 0x0 197 7 9 19 6 28 34 0x3133\r\n201 227 0x0 197 7 9 18 22 30 28 0xcd8f\r\n202 335 0x20 197 7 9 18 20 6 40 0xcaf5\r\n203 63 0x0 197 7 9 18 18 12 48 0xcdce\r\n204 106 0x0 197 7 9 18 15 14 54 0x231a\r\n205 149 0x0 197 7 9 18 12 19 56 0xb974\r\n206 307 0x20 197 7 9 18 9 41 36 0x2cd3\r\n207 217 0x0 197 7 9 18 7 17 46 0xbd59\r\n208 357 0x20 197 7 9 18 6 28 52 0x90ac\r\n209 348 0x20 197 7 9 18 6 28 2 0x3cf9\r\n210 82 0x0 197 7 9 17 23 22 24 0xf42a\r\n211 88 0x0 197 7 9 17 14 40 6 0x81ea\r\n212 145 0x0 197 7 9 17 11 13 34 0x7b41\r\n213 248 0x20 197 7 9 17 9 51 54 0x7faf\r\n214 84 0x0 197 7 9 17 6 4 42 0x26b2\r\n215 140 0x0 197 7 9 16 23 6 18 0x2cb2\r\n216 210 0x0 197 7 9 16 19 34 52 0x7dd9\r\n217 96 0x0 197 7 9 16 12 25 18 0x4b4d\r\n218 145 0x0 197 7 9 16 10 38 18 0xf2af\r\n219 157 0x0 197 7 9 16 10 37 24 0xdff1\r\n220 174 0x0 197 7 9 15 23 40 52 0xcf6a\r\n221 75 0x0 197 7 9 15 17 50 40 0xdb97\r\n222 72 0x0 197 7 9 15 15 39 12 0x8871\r\n223 154 0x0 197 7 9 15 13 18 4 0x2e21\r\n224 134 0x0 197 7 9 15 9 59 56 0xffea\r\n225 147 0x0 197 7 9 15 7 13 4 0xa715\r\n226 193 0x0 197 7 9 15 0 49 28 0x752\r\n227 78 0x0 197 7 9 14 22 47 6 0x94c2\r\n228 93 0x0 197 7 9 14 18 22 22 0x886c\r\n229 161 0x0 197 7 9 14 15 35 42 0xfac\r\n230 377 0x20 197 7 9 14 13 52 58 0xdefc\r\n231 225 0x0 197 7 9 14 12 3 56 0x1abb\r\n232 300 0x20 197 7 9 14 10 55 50 0x6159\r\n233 136 0x0 197 7 9 14 6 57 10 0x8024\r\n234 230 0x0 197 7 9 13 21 45 6 0x922a\r\n235 93 0x0 197 7 9 13 13 47 24 0x8ebb\r\n236 263 0x20 197 7 9 13 11 15 32 0xd186\r\n237 126 0x0 197 7 9 13 9 11 36 0x1564\r\n238 61 0x0 197 7 9 13 3 17 48 0x1f\r\n239 196 0x0 197 7 9 12 22 44 34 0x634d\r\n'
    _body += '240 249 0x20 197 7 9 12 21 35 10 0xe57b\r\n241 194 0x0 197 7 9 12 19 41 52 0x3950\r\n242 229 0x0 197 7 9 12 19 20 22 0x777d\r\n243 370 0x20 197 7 9 12 18 8 38 0xbfbb\r\n244 356 0x20 197 7 9 12 16 34 22 0x97d2\r\n245 120 0x0 197 7 9 12 12 59 24 0xa664\r\n246 177 0x0 197 7 9 12 11 59 14 0x19da\r\n247 206 0x2 197 7 9 1 11 20 12 0xa3f6\r\n248 203 0x2 197 7 9 1 9 42 58 0x839d\r\n'

    print AnalyseAgaMatrixKeyNote(_body)
