# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Roche Accu-Chek Compact
# @DEVICE Roche Accu-Chek Compact Plus
# @DEVICE Roche Accu-Chek Compact Plus GT

import re
from Generic import *
from Defines import *
from datetime import datetime

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate a Roche Compact Serial record. Extracted groups : 
    group 1 : serial number
    """
    m = re.match( r'!TAG!SERNO!\d{2,2}(\d+)', line, re.IGNORECASE )
    return m

def EvalUnitRecord( line ):
    """
    Evaluate a Roche Compact Unit record. Extracted groups : 
    group 1 : unit (mmol/l or mg/dl)
    """
    m = re.match( r'!TAG!UNIT!\d{2,2}(mmol/l|mg/dl)', line, re.IGNORECASE )
    return m

def EvalNrResultsRecord( line ):
    """
    Evaluate a Roche Compact Number Results record. Extracted groups : 
    group 1 : number of result records
    """
    m = re.match( r'!TAG!RESULTS!\d{2,2}(\d{4,4})\d+', line, re.IGNORECASE )
    return m

def EvalResultRecord( line ):
    """
    Evaluate a Roche Compact Result record. Extracted groups : 
    group 1 : value (4)
    group 2 : hour (2)
    group 3 : min (2)
    group 4 : day (2)
    group 5 : month (2)
    group 6 : year (2)
    group 7 : flags (2) 
    group 8 : checksum (2)
    
    Note : AAAA for testing purpose.
    """
    m = re.match( r'\d{2,2}(\d{4,4}|AAAA)(\d{2,2})(\d{2,2})(\d{2,2})(\d{2,2})(\d{2,2})([\d|a-f]{2,2})([\d|a-f]{2,2})', line, re.IGNORECASE )
    return m

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalRocheAccuchekCompactSerialRecord( line ):
    """
    Is this line a serial record? If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )
    if m:
        res[ "meter_serial" ] = m.group(1)[0:8]

    return res

def EvalRocheAccuchekCompactUnitRecord( line ):
    """
    Is this line a unit record? If so, return a dictionary with unit.
    """
    res = {}
    m = EvalUnitRecord( line )
    if m:
        res[ "meter_unit" ] = m.group(1)
    return res

def EvalRocheAccuchekCompactNrResults( line ):
    """
    Is this line a nr results record? If so, return a dictionary with nr results.
    """
    res = {}
    m = EvalNrResultsRecord( line )
    if m:
        try:
            res[ "meter_nr_results" ] = int( m.group(1) ) + 1
        except ValueError:
            res = { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
    return res

def EvalRocheAccuchekCompactResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    res = {}
    mu = EvalUnitRecord( line )
    if mu:
        if mu.group(1) == "mmol/l":
            res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MMOLL
        else:
            res[SETTING_BG_UNIT] = SETTING_BG_UNIT_MGDL
        return { SETTINGS_LIST: res }
        
    m = EvalResultRecord( line )
    if m:

        # Some values has timestamp 00-00-00 00:00, return None to Generic in that case...
        try:
            res[ ELEM_TIMESTAMP ] = datetime(2000 + int(m.group(6)), int(m.group(5)), int(m.group(4)), int(m.group(2)), int(m.group(3)) )
        except:
            res[ ELEM_TIMESTAMP ] = None

        res[ "meter_checksum" ] = m.group(8)
        try:
            _flags = int(m.group(7), 16)
            _flag_list = []
            
            if _flags & 0x01:
                _flag_list.append(FLAG_REUSED_DRUM)
            if _flags & 0x02:
                _flag_list.append(FLAG_EXPIRED_DRUM)
            if _flags & 0x04:
                _flag_list.append(FLAG_RESULT_LOW)
            if _flags & 0x08:
                _flag_list.append(FLAG_RESULT_HIGH)
            if _flags & 0x20:
                _flag_list.append(FLAG_RESULT_CTRL)
            if _flags & 0x40:
                _flag_list.append(FLAG_RESULT_OUTSIDE_TEMP)
            
            res[ "meter_flaglist" ] = _flag_list
            
            try:
                res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
                res[ ELEM_VAL ] = float( m.group(1) )
            except ValueError:
                res= { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(1) }
            
        except:
            res= { "error_code":ERROR_CODE_VALUE_ERROR, "line":line, "fault_data":m.group(7) }
            
    return res

def EvalRocheAccuchekCompactChecksumRecord( line, record ):
    """
    Evaluate checksum of result record.
    """
    m = EvalUnitRecord( line )
    if m:
        return True
    # calculate checksum
    checkSumma = 0x6E;
    for item in line[2:-2]:
        checkSumma = checkSumma ^ ord(item);
        
    try:
        if checkSumma == int(line[-2:], 16):
            return True
        else:
            return False
    except ValueError:
        return False

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectRocheAccuchekCompact( inList ):
    """
    Detect if data comes from a Roche Compact. 
    """
    return DetectDevice( "RocheAccuchekCompact", inList, DEVICE_METER )

def AnalyseRocheAccuchekCompact( inData ):
    """
    Analyse Roche Compact
    """
    
    # must make sure that the list is line-separated
    # remove 0x02 and 0x06, split using 0x03 and 0x04
    splitList = []

    # Remove some uncessary rep
    line = inData.replace(chr(2),'')
    line = line.replace(chr(6),'')        
    p = re.compile(r'[\x03|\x04]')
    list = p.split(line)
    # poor mans flatten list
    for item in list:
        splitList.append(item)

    # Empty dictionary
    d = {}
    
    d[ "meter_type" ]           = "Accu-Chek Compact"
    d[ "eval_serial_record" ]   = EvalRocheAccuchekCompactSerialRecord
    d[ "eval_unit"]             = EvalRocheAccuchekCompactUnitRecord
    d[ "eval_result_record" ]   = EvalRocheAccuchekCompactResultRecord
    d[ "eval_checksum_record" ] = EvalRocheAccuchekCompactChecksumRecord
    d[ "eval_nr_results" ]      = EvalRocheAccuchekCompactNrResults
    
    resList = AnalyseGenericMeter( splitList, d );
    return resList
    

if __name__ == "__main__":
    body = '!TAG!MODEL!\x06\x0204091066\x04\x06!TAG!SERNO!\x06\x0208037941856B\x04\x06!TAG!UNIT!\x06\x0206mmol/l2E\x04\x06!TAG!RESULTS!\x06\x0204010867\x04\x06\x06\x021201100743231007000069\x03\x02120064062323100700006C\x03\x021200692155221007000064\x03\x02120087180522100700006B\x03\x021200581647221007000061\x03\x021200731342221007000068\x03\x02120061122122100700006F\x03\x02120105074222100700006D\x03\x02120062062322100700006B\x03\x021200742159211007000067\x03\x02120071175021100700006E\x03\x02120049163921100700006B\x03\x02120101134221100700006F\x03\x02120052122221100700006F\x03\x021201050943211007000061\x03\x021200670817211007000064\x03\x02120055222620100700006E\x03\x021200981806201007000064\x03\x021200981649201007000061\x03\x021200941258201007000069\x03\x02120050114620100700006D\x03\x021201070856201007000067\x03\x021200690732201007000063\x03\x021200922202191007000069\x03\x021200541731191007000065\x03\x021200601617191007000067\x03\x02120086141119100700006B\x03\x021200521252191007000063\x03\x02120103092319100700006A\x03\x02120065080119100700006A\x03\x021200580353191007000068\x03\x021201022152181007000066\x03\x02120058174018100700006E\x03\x021200631627181007000066\x03\x02120069134618100700006E\x03\x021200741221181007000062\x03\x021201140742181007000064\x03\x021200690623181007000069\x03\x02120058035118100700006B\x03\x021200922202171007000067\x03\x02120081185017100700006B\x03\x021200761708171007000061\x03\x021200691407171007000063\x03\x02120055123117100700006F\x03\x02120121074117100700006E\x03\x02120072062417100700006B\x03\x021200650352171007000069\x03\x021200972156161007000061\x03\x02120100173416100700006F\x03\x02120065160416100700006F\x03\x021200691310161007000063\x03\x021200851150161007000067\x03\x021201130746161007000069\x03\x02120072063216100700006D\x03\x021200580352161007000066\x03\x02120052220015100700006B\x03\x021200771812151007000066\x03\x02120083163915100700006A\x03\x02120060145515100700006F\x03\x02120046134715100700006F\x03\x021200691210151007000061\x03\x021201240740151007000068\x03\x021200640632151007000069\x03\x021200580352151007000065\x03\x02120074214714100700006E\x03\x02120079180714100700006D\x03\x02120082162814100700006A\x03\x021201031309141007000064\x03\x02120052114314100700006D\x03\x021201150931141007000063\x03\x021200720802141007000062\x03\x02120063035414100700006A\x03\x021200902203131007000060\x03\x02120078181113100700006C\x03\x021200581627131007000065\x03\x021200721327131007000068\x03\x021200531151131007000068\x03\x021201040913131007000064\x03\x021200830755131007000066\x03\x02120065035213100700006D\x03\x021200782200121007000064\x03\x021200981832121007000062\x03\x021200681700121007000063\x03\x02120112133512100700006D\x03\x021200491100121007010067\x03\x021201250834121007000062\x03\x02120077070112100700006D\x03\x02120064030712100700006D\x03\x021200702109111007000065\x03\x02120110171511100700006A\x03\x021200691550111007000066\x03\x021200881303111007000069\x03\x02120050111611100700016B\x03\x021201430801111007000067\x03\x02120089063011100700006C\x03\x021200820309111007000068\x03\x02120103210710100700006F\x03\x02120092170910100700006D\x03\x02120089153510100700006A\x03\x02120117123010100700006E\x03\x021200691053101007000061\x03\x021201420800101007000066\x03\x021200950614101007000066\x03\x021200852107091007000068\x03\x021201131722091007000064\x03\x02120176171909100700006F\x03\x02120060150809100700006B\x03\x02120090122709100700006E\x04'

    print AnalyseRocheAccuchekCompact(body)
