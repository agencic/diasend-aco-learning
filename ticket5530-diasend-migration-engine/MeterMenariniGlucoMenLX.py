# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2008 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Nova Max
# @DEVICE Nova Max Plus
# @DEVICE Menarini GlucoMen LX
# @DEVICE Menarini GlucoMen LX PLUS
# @DEVICE Menarini GlucoMen LX2
# @DEVICE Menarini Glucofix Miò
# @DEVICE Menarini Glucofix Miò plus
# @DEVICE Menarini Glucofix Premium
# @DEVICE Menarini GlucoMen areo
# @DEVICE Menarini GlucoMen areo 2k
# @DEVICE Menarini Glucofix Tech

import re
from Generic import *
from Defines import *
from datetime import datetime

# -----------------------------------------------------------------------------
# DEFINES
# -----------------------------------------------------------------------------
MODEL_NAME_NOVA_MAX          = "Nova Max"
MODEL_NAME_NOVA_MAX_PLUS     = "Nova Max Plus"
MODEL_NAME_GLUCOMEN_LX       = "GlucoMen LX/LX PLUS"
MODEL_NAME_GLUCOMEN_LX2      = "GlucoMen LX2"
# Menarini wont share info how to keep mió/mió plus separate.
MODEL_NAME_GLUCOMEN_MIO      = "Glucofix Mio/Mio plus"
MODEL_NAME_GLUCOMEN_AREO     = "GlucoMen areo"
MODEL_NAME_GLUCOMEN_AREO_2K  = "GlucoMen areo 2k"
MODEL_NAME_GLUCOFIX_PREMIUM  = "Glucofix Premium"
MODEL_NAME_GLUCOFIX_TECH     = "Menarini GLUCOFIX TECH"
ALLOW_INCORRECT_AREO_SERIALS = True   # Some (development?) GlucoMen areo's has a serial starting with W instead of the specified E


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------

class Meta(object):

    model_name = ''

    def __init__(self, line):
        self.line = line

dallas_1wire_lookup = [
     0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65, 
   157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220, 
    35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98, 
   190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255, 
    70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7, 
   219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154, 
   101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36, 
   248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185, 
   140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205, 
    17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80, 
   175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238, 
    50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115, 
   202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139, 
    87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22, 
   233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168, 
   116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53]
 

def UpdateCRC(initial_crc, byte):
    """
    Update the initial crc with another checksum byte
    """
    return dallas_1wire_lookup[initial_crc ^ byte]
    

def SplitIndata(indata):
    """
    Splits incoming data into a list. 
    
    The data consists of records, all terminated by ']\r\n'

    """
    
    inList = []
    
    terminator = ']\r\n'
    
    while len(indata) > 0:
        idx = indata.find(terminator)

        # Terminator not found (?)
        if idx <= 0:
            return []

        inList.append(Meta(indata[:idx + len(terminator)]))

        indata = indata[idx + len(terminator):]
    
    return inList

# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSettingsRecord( line ):
    """
    Evaluate a Menarini Visio header record. Extracted groups : 
    serial_number
    n_results
    result_high_limit
    result_low_limit
    measurements_unit
    csum
    
    The settings record looks like this:
    '[\r\nT07333001803X,58,1F,D070048323XL, 1.0\r\n8F\r\n]\r\n'
    """
    if len(line) != 49:
        return None
    
    return re.match( r'\[\r\n(\w{13}),([\dA-F]{2}),([\dA-F]{2}), *(\w{8,12}), *([\w\d\.]*)\r\n[\dA-F]{2}\r\n]\r\n', line, re.IGNORECASE )

def EvalResultRecord( line ):
    """
    Evaluate a Menarini Visio binary represented Result record. Extracted value : 
    "Glu/Ctl"
    "Value"
    "Unit"
    "Flags"
    "Date"
    "Time"
    
    Lines look like this:
    '\nGlu,12.0,mmol/L,00,080904,1136'
    '\nKet, 0.9,mmol/L,00,101130,0915'
    
    """
    

    # The length of the result records are always 13 bytes
    if len(line) != 31:
        return None
    
    return re.match( r'\n(Glu|Ctl|Ket), *(LO|HI|[0-9\.]*), *(mmol/L|mg/dL),([0-9]{2,2}),([0-9]{6,6}),([0-9]{4,4})', line, re.IGNORECASE )

# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalMenariniType( meta ):
    """
    Is this line a serial record. If so, parse the serial number 
    for type information.
    """
    res = {}

    line = meta.line

    m = EvalSettingsRecord(line)
    if m:
        meter_serial = m.group(4) #Serial number
    else:
        return res

    kit_type    = meter_serial[-1]
    meter_type  = meter_serial[-2]
    config_code = meter_serial[0]
                                    
    if (config_code == "A" or config_code == "B") and meter_type == 'X' and kit_type == 'P':
        model_name = MODEL_NAME_NOVA_MAX_PLUS
    elif (config_code == "A" or config_code == "B") and meter_type == 'X' and (kit_type == 'R' or kit_type == 'D'):
        model_name = MODEL_NAME_NOVA_MAX
    elif (config_code == "C" or config_code == "D") and meter_type == 'X' and kit_type == 'L':
        model_name = MODEL_NAME_GLUCOMEN_LX
    elif(config_code == "C" or config_code == "D") and meter_type == 'X' and kit_type == 'N':
        model_name = MODEL_NAME_GLUCOMEN_LX2
    elif (config_code == "C" or config_code == "D") and meter_type == 'X' and kit_type == 'R':
        model_name = MODEL_NAME_GLUCOMEN_MIO
    elif (config_code == "C" or config_code == "D") and meter_type == 'X' and kit_type == 'P':
        model_name = MODEL_NAME_GLUCOFIX_PREMIUM
    elif (meter_serial[0:2] in ('EP', 'EQ')) or (ALLOW_INCORRECT_AREO_SERIALS and config_code == "W"):
        model_name = MODEL_NAME_GLUCOMEN_AREO
    elif (meter_serial[0:2] in ('GT', 'HC')):
        model_name = MODEL_NAME_GLUCOMEN_AREO_2K
    elif meter_serial[0:2] in ('ES', 'ER'):
        model_name = MODEL_NAME_GLUCOFIX_TECH
    else:
        return res

    Meta.model_name = model_name

    res[ ELEM_DEVICE_MODEL ] = model_name

    return res

def EvalMenariniVisioSerialRecord( meta ):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """

    line = meta.line

    res = {}

    m = EvalSettingsRecord(line)
    if m:
        res[ "meter_serial" ] = m.group(4)

    return res


def EvalMenariniGlucoMenLXResultRecord( meta ):
    """
    Is this a result record? If so, return a dictonary with keys >

    date_time   > date in yyyy-mm-dd hh:mm:ss format
    value       > value (float)
    unit        > unit if present (otherwise require headerunit)
    flags       > list of flags (int) if present
    """
    
    line = meta.line

    # The result record contains many results, split it first
    # The main split function does not split each result on this meter
    # because the checksum is common for all results
    
    # first check the length
    # The size is 10 + x * 32
    if ((len(line) - 10) % 32) != 0:
        return {}
        
    # The length seem correct -> split
    lines = line.split('\r')

    res = []

    for l in lines:
        m = EvalResultRecord(l)
        if m:
            rec = {} 
            
            flags = []

            if m.group(1) == "Ctl":
                flags.append(FLAG_RESULT_CTRL)

            value = 0
                
            if m.group(2) == "LO":
                flags.append(FLAG_RESULT_LOW)
            elif m.group(2) == "HI":
                flags.append(FLAG_RESULT_HIGH)
            else:
                # Normal value
                value = float(m.group(2))

            rec[ELEM_DEVICE_UNIT] = m.group(3)
            # The analyser shall return the value * 10 if a mmoll value
            if rec[ELEM_DEVICE_UNIT].lower() == "mmol/l":
                value *= 10
           
            # Flag handling is different on the areo, using bitfields instead of constants
            flag_value = int(m.group(4))
            if Meta.model_name == MODEL_NAME_GLUCOMEN_AREO or \
               Meta.model_name == MODEL_NAME_GLUCOMEN_AREO_2K:
                flags.append(FLAG_CHECK_MARK)      if flag_value & 0x01 else None
                flags.append(FLAG_BEFORE_MEAL)     if flag_value & 0x02 else None
                flags.append(FLAG_AFTER_MEAL)      if flag_value & 0x04 else None
                flags.append(FLAG_DURING_EXERCISE) if flag_value & 0x08 else None
            else:
                if m.group(4) == '01':
                    flags.append(FLAG_CHECK_MARK)
                elif m.group(4) == '02':
                    # The control value should already be flagged above.
                    if not FLAG_RESULT_CTRL in flags:
                        flags.append(FLAG_RESULT_CTRL)
                elif m.group(4) == '03':
                    flags.append(FLAG_BEFORE_MEAL)
                elif m.group(4) == '04':
                    flags.append(FLAG_AFTER_MEAL)
                elif m.group(4) == '05':
                    flags.append(FLAG_DURING_EXERCISE)

            year    = int(m.group(5)[:2]) + 2000
            month   = int(m.group(5)[2:4])
            day     = int(m.group(5)[4:6])
            
            hour    = int(m.group(6)[:2])
            min     = int(m.group(6)[2:4])
            
            try:
                rec[ ELEM_TIMESTAMP ] = datetime(year, month, day, hour, min)
            except:
                rec[ ELEM_TIMESTAMP ] = None
            
            rec[ ELEM_FLAG_LIST ] = flags
            if m.group(1) == 'Ket':
                rec[ ELEM_VAL_TYPE ] = VALUE_TYPE_KETONES
            else:
                rec[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
            rec[ ELEM_VAL ] = value

            res.append(rec)

    return res


def EvalMenariniGlucoMenLXChecksumRecord( meta, record ):
    """
    Evaluate checksum of result record. 
    
    The meter uses dallas one wire CRC
    """

    line = meta.line

    crc = 0
    
    for c in line[:len(line) - 7]:
        crc = UpdateCRC(crc, ord(c))

    given_crc = int(line[len(line) - 7: len(line) - 5], 16)
    
    return crc == given_crc

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMenariniGlucoMenLX( inList ):
    """
    Detect if data comes from a Menarini GlucoMen LX.
    """
    return DetectDevice( 'MenariniGlucoMenLX', inList, DEVICE_METER )

def AnalyseMenariniGlucoMenLX( inData ):
    """
    Analyse Menarini GlucoMen LX
    """

    inList = SplitIndata(inData)
    
    # Empty dictionary
    d = {}

    d[ "eval_device_model_record" ] = EvalMenariniType 
    d[ "eval_serial_record" ]       = EvalMenariniVisioSerialRecord
    d[ "eval_result_record" ]       = EvalMenariniGlucoMenLXResultRecord
    d[ "eval_checksum_record" ]     = EvalMenariniGlucoMenLXChecksumRecord

    resList = AnalyseGenericMeter( inList, d );
    return resList

if __name__ == "__main__":

    testfiles = (
        'test/testcases/test_data/NovaMaxPlus/NovaMaxPlus-B111065320XP.bin',
        'test/testcases/test_data/NovaMaxPlus/NovaMaxPlus-B124003093XP.bin',
        'test/testcases/test_data/GlucoMenLXPLUS/ep022.bin',
        'test/testcases/test_data/GlucoMenLXPLUS/ep054.bin',
        'test/testcases/test_data/GlucoMenLXPLUS/ep055.bin',
        'test/testcases/test_data/GlucoMenLXPLUS/ep056.bin',
        'test/testcases/test_data/GlucoMenAreo/USB/GlucoMenAreo_EQ021480.log',
        'test/testcases/test_data/GlucoMenAreo/USB/GlucoMenAreo_WW004720_600values.log',
        'test/testcases/test_data/MenariniGlucofixMio/MenariniGlucofixMioPlus-half.bin',
        'test/testcases/test_data/MenariniGlucofixTech/ES001342.log',
        'test/testcases/test_data/GlucoMenAreo2k/GT039756.txt',
        'test/testcases/test_data/GlucoMenAreo2k/GT039761.txt',
        'test/testcases/test_data/GlucoMenAreo2k/GT039771.txt',
        'test/testcases/test_data/GlucoMenLX2/glucomenlx2-DE0381.log',
        'test/testcases/test_data/GlucoMenLX2/glucomenlx2-DE0382.log',
        'test/testcases/test_data/GlucoMenLX2/glucomenlx2-DE0383.log',
        'test/testcases/test_data/GlucoMenLX2/glucomenlx2-DE0384.log',
        'test/testcases/test_data/GlucoMenLX2/glucomenlx2-DE0385.log',
        'test/testcases/test_data/GlucoMenLX2/glucomenlx2-DE0386.log'
    )

    for testfile in testfiles:
        with open(testfile) as f:
            data = f.read()
            result = AnalyseMenariniGlucoMenLX(data)
            print result
