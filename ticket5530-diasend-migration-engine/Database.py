# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import EventLog
import MySQLdb
import time
import Config
from Generic import *
from Defines import *
from Encryption import EncryptSerialNumber
import types

# -----------------------------------------------------------------------------
# "Global" variables (only global if you import them from another module)
# -----------------------------------------------------------------------------
connection = None

default_host = Config.settings["database-address"]
default_passwd = Config.settings["database-password"]
default_user = Config.settings["database-user"]
default_port = int(Config.settings["database-port"])
default_db = Config.settings["database-db"]

host   = default_host
user   = default_user
passwd = default_passwd
port   = default_port
db     = default_db

USER_PROPERTY_FILE_UPLOAD_SRPA  = "fileupload_srpa"
USER_PROPERTY_FILE_UPLOAD_ADMIN = "fileupload_admin"
USER_PROPERTY_ANIMAS_SUPPORT    = "animas"
ADMIN_PROPERTY_UNITED_SUPPORT   = "united"

# -----------------------------------------------------------------------------
# Function definitions
# -----------------------------------------------------------------------------

def ToStr(val):
    """
    Checks if it is a unicode -> decode as UTF8, otherwise to normal string conversion
    """
    # If we have a unicode string, create an UT8 string
    if type(val) == types.UnicodeType:
        return val.encode("utf8")
    else:
        # just create an ASCII string
        return str(val)
    

def IsDatabaseConnected():
    """
    Is database connected?
    """
    global connection

    if connection:
        return True
    else:
        return False
        
# ------------------------------------------------------------------------------

def ConnectToDatabase():
    """
    Establish connection to database
    """
    global connection

    if not connection:
        try:
            connection = MySQLdb.connect( host=host, user=user, passwd=passwd, port=port, db=db, charset="utf8" )
        except MySQLdb.MySQLError, e:
            print "Connect to database error: ", repr(e)
            EventLog.EventLogError('Database error when connection to database %s.' % (db,))
            connection = None   # to be sure

    if connection:
        return True
    else:
        return False

# ------------------------------------------------------------------------------

def ReconnectToDatabase():

    """
    There are situations when we want to switch database,
    for example if we have an ongoing transfer, but
    detect that server location has changed (DDA)
    This function will close any current connection and
    reconnect to the database that is currently set
    """
    DisconnectFromDatabase()
    return ConnectToDatabase()

# ------------------------------------------------------------------------------

def GetLastInsertID():
    """
    Return the ID returned during last insert
    """
    global connection

    if not connection:
        return None
        
    return connection.insert_id()

# ------------------------------------------------------------------------------

def DisconnectFromDatabase():
    """
    Disconnect from database.
    """
    global connection

    if connection:
        try:
            connection.commit()
            connection.close()
        except MySQLdb.MySQLError:
            print "Cannot Disconnect from database."
            EventLog.EventLogError('Cannot disconnect from database')
        connection = None

# ------------------------------------------------------------------------------

def DoesQueryReturnResults( sqlQ, paramQ ):
    """
    Check if a sql query with a given paramQ retuns any results. Note : also returns false
    when something goes wrong; it is the responsibility of another part of the
    code to handle 'no connection' etc. This function _only_ indicates true
    if it has found a value in the database.
    """
    global connection

    # Are we conntected?
    if not connection:
        return False

    foundValue = False

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, paramQ )
        cursor.fetchall()
        if cursor.rowcount == 0:
            foundValue = False
        else:
            foundValue = True
        cursor.close()

    except MySQLdb.MySQLError:
        foundValue = False

    return foundValue

# ------------------------------------------------------------------------------

def IsValuePresentInDatabase( value, device_id ):
    """
    Check if value is already present in database. Note : also returns false
    when something goes wrong; it is the responsibility of another part of the
    code to handle 'no connection' etc. This function _only_ indicates true
    if it has found a value in the database.
    """
    # Validate if dictionary contains the values that we need to determine
    # if this is a new value or not
    if ( not value.has_key( ELEM_TIMESTAMP ) ) or \
       ( not value.has_key( ELEM_VAL_TYPE ) ) or \
       ( not value.has_key( ELEM_VAL ) ):
        return False

    # If the value contains a UUID we can use that...
    uuid = None
    if ELEM_VALUE_LIST in value:
        value_list = value[ELEM_VALUE_LIST]
        for sec_val in value_list:
            if sec_val[ELEM_VAL_TYPE] == VALUE_TYPE_UUID:
                uuid = sec_val[ELEM_VAL]
    if uuid != None:
        # Build SQL query
        sqlQ = "SELECT telemeddata.seq FROM telemeddata,telemeddata_values WHERE device_id=%s AND telemeddata.type=%s AND telemeddata.seq=telemeddata_values.telemeddata_seq AND telemeddata_values.type=%s AND telemeddata_values.value=%s"
        paramQ = ( device_id, ToStr(value[ ELEM_VAL_TYPE ]),VALUE_TYPE_UUID, ToStr(uuid) )

        duplicate = DoesQueryReturnResults(sqlQ, paramQ)
    else:
        # Build SQL query
        sqlQ = "SELECT seq FROM telemeddata WHERE timestamp=%s AND device_id=%s AND type = %s AND value=%s"
        paramQ = ( ToStr(value[ ELEM_TIMESTAMP ]), device_id, ToStr(value[ ELEM_VAL_TYPE ]), ToStr(value[ ELEM_VAL ]) )

        duplicate = DoesQueryReturnResults(sqlQ, paramQ)

        # Damn ugly thing, the precision of the database was increase 08-09-02
        # So we need to round the values and check if they are in the databse too
        # otherwhise we would end up with duplicates, one rounded and one with higher precision
        # TODO: remove this after a while. We risk to have to test values at the same timestamp
        #       with different values, but they might be the same when we round them up....
        if not duplicate and (value[ ELEM_VAL_TYPE ] == VALUE_TYPE_GLUCOSE or value[ ELEM_VAL_TYPE ] == VALUE_TYPE_KETONES):
            rounded_val = (int(round(value[ ELEM_VAL ] / 100))) * 100

            # The rounded val differs.. check with the DB
            if rounded_val != value[ ELEM_VAL ]:
                paramQ = ( ToStr(value[ ELEM_TIMESTAMP ]), device_id, ToStr(value[ ELEM_VAL_TYPE ]), ToStr(rounded_val) )

                duplicate = DoesQueryReturnResults(sqlQ, paramQ)        
    
    return duplicate

# ------------------------------------------------------------------------------

def InsertHistoryIntoDatabase( terminal_serial, meter_serial, entry ):
    """
    Insert log entry into history. If the device with serial number "meter_serial" 
    isn't found the history entry will be created without a device.
    """

    SelectDatabase(terminal_serial)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return

    terminal_id = GetTerminalIdFromDatabase(terminal_serial)
    device_id = GetDeviceIdFromDatabase(meter_serial)

    # Build SQL query
    sqlQ = """
        INSERT INTO history (device_id, terminal_id, entry)
        VALUES (%s,%s,%s)
        """
    if device_id == None:
        paramQ = (None, ToStr(terminal_id), ToStr(entry))
    else:
        paramQ = ( ToStr( device_id ), ToStr( terminal_id ), ToStr( entry ) )

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, paramQ )
        cursor.close()

    except MySQLdb.MySQLError:
        pass

    DisconnectFromDatabase()

# ------------------------------------------------------------------------------
def PerformQuery( sqlQ, paramQ, retResults = False, force_commit = False ):
    """
    Performs an sql query, returns True on success, else False
    """
    global connection

    # Are we conntected?
    if not connection:
        return False
    
    result = False

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, paramQ )

        if retResults:
            result = cursor.fetchall()
            
        cursor.close()
        
        if force_commit:
            connection.commit()

        if not retResults:
            result = True

    except MySQLdb.MySQLError:
        result = False
    return result


def InsertFlagListIntoDatabase(flag_list, last_id):
    """
    Inserts a list of flags into a flag table
    """

    sqlQ = """
        INSERT INTO telemeddata_flags (telemeddata_seq, flag)
        VALUES (%s, %s)"""

    paramQ = [ToStr(last_id), ToStr(flag_list[0])]
    
    for flag in flag_list[1:]:
        sqlQ += ", (%s, %s)"
        paramQ += [ToStr(last_id), ToStr(flag)]
        
    return PerformQuery (sqlQ, paramQ)


def InsertValueListIntoDatabase(value_list, last_id):
    """
    Inserts a list of values into a flag table
    """

    sqlQ = """
        INSERT INTO telemeddata_values (telemeddata_seq, type, value)
        VALUES (%s, %s, %s)"""

    elem = value_list[0]

    paramQ = [ToStr(last_id), ToStr(elem[ELEM_VAL_TYPE]), ToStr(elem[ELEM_VAL])]

    for elem in value_list[1:]:
        sqlQ += ", (%s, %s, %s)"
        paramQ += [ToStr(last_id), ToStr(elem[ELEM_VAL_TYPE]), ToStr(elem[ELEM_VAL])]

    return PerformQuery (sqlQ, paramQ)
    
def InsertBlackBoxHistory(value, device_id, transfer_id):
    """
    Store device specific debug information (records) in the database.
    """

    if not BLACK_BOX_HISTORY_RECORD in value:
        return False

    sqlQ = "INSERT INTO device_debug_record (device_id, transfer_id, data) VALUES (%s, %s, %s)"
    paramQ = (ToStr(device_id), ToStr(transfer_id), ToStr(value[BLACK_BOX_HISTORY_RECORD]))

    return PerformQuery(sqlQ, paramQ)

# ------------------------------------------------------------------------------
def InsertValueIntoDatabase( value, device_id, transfer_id ):
    """
    Insert new value into database
    """
    
    sqlQ   = ""
    paramQ = ""

    # Validate if dictionary contains the values that we need
    if ( not value.has_key( ELEM_TIMESTAMP ) ) or \
       ( not value.has_key( ELEM_VAL ) ):
        return False

    # Build SQL query
    sqlQ = "INSERT INTO telemeddata (device_id, transfer_id, timestamp, type, value) VALUES (%s,%s,%s,%s,%s)"

    paramQ = ( ToStr(device_id), ToStr(transfer_id), ToStr(value[ ELEM_TIMESTAMP ]), ToStr(value[ ELEM_VAL_TYPE ]), ToStr(value[ ELEM_VAL ]))

    if not PerformQuery (sqlQ, paramQ):
        return False

    # Get the ID of the insert
    lastID = GetLastInsertID()
    if not lastID:
        return False

    if value.has_key( "meter_flaglist" ) and len(value[ "meter_flaglist"]) > 0:
        if not InsertFlagListIntoDatabase(value[ "meter_flaglist" ], lastID):
            return False
            
    if value.has_key( ELEM_VALUE_LIST ) and len(value[ ELEM_VALUE_LIST ]) > 0:
        if not InsertValueListIntoDatabase(value[ ELEM_VALUE_LIST ], lastID):
            return False
            
    return True        

# ------------------------------------------------------------------------------
def CreateDeviceSettingGroup(device_id, transfer_id):
    """
    Create a new device setting group (a group with all device settings for 
    a specific device / transfer combination).
    """
    sqlQ = "INSERT INTO device_setting_group (device_id, transfer_id) VALUES (%s, %s)"
    paramQ = (ToStr(device_id), ToStr(transfer_id))

    if not PerformQuery (sqlQ, paramQ):
        return 0

    return GetLastInsertID()

# ------------------------------------------------------------------------------
def InsertPumpSettingsIntoDatabaseNG(device_setting_group_id, value):
    """
    Insert pump settings into the new structure (where all pump settings are 
    stored and not only the settings from the last transfer).
    """
    if not value.has_key( SETTINGS_LIST ):
        return False

    # Build SQL query
    sqlQ = """
        INSERT INTO device_setting (device_setting_group_id, setting_id, value)
        VALUES """

    _first = True
    paramQ = []

    # Build query to insert all settings in one go
    for key in value[ SETTINGS_LIST ].keys():
        val = value[ SETTINGS_LIST ][key]

        if not _first:
            sqlQ += ", "
        else:
            _first = False

        sqlQ += "(%s, %s, %s)"
        
        # If we have a unicode string, create an UT8 string
        if type(val) == types.UnicodeType:
            val = val.encode("utf8")
        else:
            # just create an ASCII string
            val = ToStr(val)

        paramQ += [ ToStr(device_setting_group_id), ToStr(key), val]

    return PerformQuery(sqlQ, paramQ)


# ------------------------------------------------------------------------------
def InsertPumpProgramsInDatabaseNG( group_id, value ):
    """
    Inserts a new pump program using the new "save all pump settings" database.
    """
    
    if not (value.has_key(PROGRAM_TYPE) and value.has_key(PROGRAM_NAME) and value.has_key(PROGRAM_PERIODS)):
        return False

    # Insert...
    sqlQ = """
        INSERT INTO device_program (device_setting_group_id, type, name, period_start, period_rate) 
        VALUES ( %s, %s, %s, %s, %s )
        """

    # Insert...
    sqlQ_values = """
        INSERT INTO device_program_value (device_program_id, type, value) 
        VALUES (%s, %s, %s)
        """

    # Now insert all settings
    for period in value[ PROGRAM_PERIODS ]:
        t = period[0]
        ts = "%02d:%02d:%02d" % (t.hour, t.minute, t.second)
        paramQ = ( ToStr(group_id), value[PROGRAM_TYPE], value[PROGRAM_NAME], ts, ToStr(period[1]) )

        if not PerformQuery(sqlQ, paramQ):
            return False
            
        if len(period) > 2:
            # There are secondary values available
            lastID = GetLastInsertID()
            if not lastID:
                return False

            for (t,v) in period[2]:
                paramQ = (lastID, t, v)
                if not PerformQuery(sqlQ_values, paramQ):
                    return False

    return True

# ------------------------------------------------------------------------------


def RemovePresentElementsFromList(inList, device_id):
    """
    Remove all elements from the list that already exists in the database
    
    Returns a new filtered list
    """
    outList = []
    
    for value in inList:

        if value.has_key("meter_flaglist"):
            flags = ToStr(len(value["meter_flaglist"]))
        else:
            flags = "0"

        if value.has_key( ELEM_VAL_TYPE ):
            if not IsValuePresentInDatabase( value, device_id ):
                outList.append(value)
            else:
                pass
                # print "ALREADY IN DATABASE DEVICE %d DATE %s VALUE %d TYPE %s NR FLAGS %s" % (device_id, value[ELEM_TIMESTAMP], value[ELEM_VAL], value[ELEM_VAL_TYPE], flags)

        else:
            outList.append(value)

    return outList
    
# ------------------------------------------------------------------------------
def FindInDatabase(field, table, key, value, force_ci=False):
    """
    Checks in the database for a terminal.
    Returns the sequence number, or None
    """
    if force_ci:
        sqlQ = "SELECT " + field + " FROM " + table + " WHERE LOWER(" + key + ") = %s"
        paramQ = ( value.lower() )
    else:
        sqlQ = "SELECT " + field + " FROM " + table + " WHERE " + key + " = %s"
        paramQ = ( value )
     
    _ret = PerformQuery( sqlQ, paramQ, True )
   
    if _ret and len(_ret) > 0:
        return _ret[0]
    else:
        return None

def UpdateDeviceInDatabase(seq, devicetype = None, deviceclass = None):
    """
    Updates an existing device
    """
    sqlQ = "UPDATE devices SET devicetype = %s, deviceclass = %s  WHERE seq = %s"

    if devicetype == None:
        dev_type_str = None
    else:
        dev_type_str = ToStr(devicetype)

    if deviceclass == None:
        dev_class_str = None
    else:
        dev_class_str = ToStr(deviceclass)

    paramQ = ( dev_type_str, dev_class_str, ToStr(seq) )

    return PerformQuery( sqlQ, paramQ, True )

def UpdateDeviceVersionInDatabase(device_id, device_version, version_recommended = None):
    """
    Sets the version of a device
    """
    sqlQ = """INSERT INTO device_properties (device_id, property, value)
        VALUES (%s, 'device_version', %s)
        ON DUPLICATE KEY UPDATE
        value = %s
    """
    paramQ = (str(device_id), str(device_version), str(device_version))
    if not PerformQuery (sqlQ, paramQ):
        return None

    if version_recommended != None:
        sqlQ = """INSERT INTO device_properties (device_id, property, value)
            VALUES (%s, 'device_version_bad', %s)
            ON DUPLICATE KEY UPDATE
            value = %s 
        """
        if not version_recommended:
            value = 1
        else:
            value = 0
        paramQ = (str(device_id), str(value), str(value))
        if not PerformQuery (sqlQ, paramQ):
            return None
    
    return True

def InsertTerminalInDatabase( serial ):
    """
    Creates a new terminal entry in the database
    Returns the sequence number of it, or 0.
    """
    sqlQ = """
        INSERT INTO terminals (terminal_id, nr_transfers, amount_data) values (%s, '0', '0')
        """

    paramQ = ( ToStr(serial) )

    if not PerformQuery (sqlQ, paramQ):
        return None

    return GetLastInsertID()

def InsertTerminalServerLocation( serial, location ):
    """
    Create a new terminal location entry in database
    """

    SelectDefaultDatabase()
    if not ReconnectToDatabase():
        return None

    sqlQ = """
        INSERT INTO terminal_locations (terminal_id, database_server) values (%s, %s)
        """
    paramQ = ( serial, ToStr(location) )

    if not PerformQuery (sqlQ, paramQ, force_commit = True):
        return None

    return True

def UpdateTerminalServerLocation( serial, location ):
    """
    Update the server location (if it has changed).
    """

    if not location in database_servers:
        return False

    SelectDefaultDatabase()
    if not ReconnectToDatabase():
        return False

    sqlQ = """
        UPDATE terminal_locations SET database_server=%s WHERE terminal_id=%s AND database_server!=%s
        """

    paramQ = ( database_servers[location], serial, database_servers[location] )

    if not PerformQuery (sqlQ, paramQ ):
        return False

    return True


def SetTerminalLocationType( serial, location ):
    """
    Set the location field of terminals table
    valid locations are {home, clinic}
    """

    if not location in ("home", "clinic"):
        return False

    sqlQ = """
        UPDATE terminals SET location=%s WHERE terminal_id=%s
        """
    paramQ = ( location, serial )

    if not PerformQuery (sqlQ, paramQ):
        return False

    return True

def SetTerminalClass( serial, terminalclass ):
    """
    Set the terminal class field for terminal.
    """

    sqlQ = """
        UPDATE terminals SET terminalclass=%s WHERE terminal_id=%s
        """
    paramQ = ( terminalclass, serial)

    if not PerformQuery (sqlQ, paramQ):
        return None

    return True

def AddTerminalPermission( serial, permission ):
    """
    Set the terminal class field for terminal.
    """

    sqlQ = """
        INSERT INTO terminal_permissions (terminal_id, permission) VALUES (%s, %s)
        """
    paramQ = ( serial, permission )

    if not PerformQuery (sqlQ, paramQ):
        return False

    return True


def InsertSoftTerminalInDatabase( serial_prefix, location ):
    """
    Create a new terminal entry in database
    This is a soft terminal, so it will have a serial
    number on the form of <SERIAL-PREFIX>-XXXXXXXXXXXX
    """
    SelectDefaultDatabase()

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return None

    #Find latest existing serial number with "serial_prefix".
    sqlQ = "SELECT terminal_id FROM terminal_locations WHERE terminal_id LIKE %s ORDER BY terminal_id DESC"
        
    paramQ = ( serial_prefix+"%" )
    
    ret = PerformQuery( sqlQ, paramQ, True )
  
    if ret == False:
        return None 

    if len(ret) == 0:
        # There are no terminals with serial prefix create the first
        serial = "%s%s" % (serial_prefix, "000000000000")
    else:
        # Increment and create
        toincr = ret[0][0].lstrip(serial_prefix)
        serial = "%s%012d" % (serial_prefix, int(toincr) + 1)

    # Insert the newly created terminal serial in database and set location
    if not InsertTerminalServerLocation( serial, location ):
        print "Couldn't set terminal location", (serial)
        return None

    SelectDatabase( serial ) 
    ReconnectToDatabase() # Connect error will be trapped when InsertTerminalInDatabase()

    if not InsertTerminalInDatabase( serial ):
        print "Couldn't insert terminal in database", (serial)
        return None
    
    return serial

def IsTerminalRegistered( terminal_serial, location ):
    """
    Switches to the database specified by 'location' and checks
    whether the specified terminal serial is registered there
    """
    SelectDatabaseByLocation( location )
    if not ReconnectToDatabase():
        return False
    
    sqlQ = """
        SELECT * FROM terminals WHERE terminal_id=%s
    """

    paramQ = (terminal_serial)
    
    result = PerformQuery(sqlQ, paramQ, True)

    if len(result)  < 1:
        return False

    return True

def InsertTransferInDatabase( terminal_id, device_id ):
    """
    Creates a new terminal entry in the database
    Returns the sequence number of it, or 0.
    """
    sqlQ = """
        INSERT INTO transfers (timestamp, terminal_id, device_id) VALUES (now(), %s, %s)
        """

    paramQ = ( ToStr(terminal_id), ToStr(device_id) )

    if not PerformQuery (sqlQ, paramQ):
        return None

    return GetLastInsertID()


def InsertDeviceInDatabase( serial, obfuscated_serial, device_type = None, device_class = None):
    """
    Creates a new device entry in the database
    Returns the sequence number of it, or 0.
    """
    sqlQ = """
        INSERT INTO devices (serialnumber, serial_obfuscated, devicetype, deviceclass) VALUES (%s, %s, %s, %s)
        """

    if device_type == None:
        dev_type_str = None
    else:
        dev_type_str = ToStr(device_type)
        
    if device_class == None:
        dev_class_str = None
    else:
        dev_class_str = ToStr(device_class)
        
    if obfuscated_serial == None:
        obfuscated_serial_str = None
    else:
        obfuscated_serial_str = ToStr(obfuscated_serial)

    paramQ = ( ToStr(serial), obfuscated_serial_str, dev_type_str, dev_class_str )

    if not PerformQuery (sqlQ, paramQ):
        return None

    return GetLastInsertID()


def GetTerminalIdFromDatabase(terminal_serial):
    """
    Check the database for a specific terminal, 
    if not found, it inserts a new terminal and returns the ID
    """    
    _line = FindInDatabase("seq", "terminals", "terminal_id", terminal_serial)

    if _line == None:
        return InsertTerminalInDatabase(terminal_serial)

    return _line[0]
    

def GetDeviceIdFromDatabase(meter_serial, devicetype = None, device_class = None):
    """
    Retrieve device id for device with serial number. Will try both the serial number as is and the 
    encrypted variant. If no device is found None will be returned. 

    Note : device type will be updated if provided.
    """

    # First try to find a device with unencrypted serial number. If not found try encrypted 
    # serial number. 
    # Make sure the meter is provided as string for correct database queries when only numbers in serial.
    meter_serial = ToStr(meter_serial)
    _line = FindInDatabase("seq, devicetype", "devices", "serialnumber", meter_serial)
    if _line == None:
        _line = FindInDatabase("seq, devicetype", "devices", "serialnumber", EncryptSerialNumber(meter_serial)[0])

    if _line == None:
        return None
    elif devicetype != None and device_class != None:
        # Device exists, but update the type, should actually only be needed once
        UpdateDeviceInDatabase(_line[0], devicetype, device_class)

    return _line[0]

def HasDeviceTypeConflict(device_serial, device_type):
    """
    Check if we find at least one other device with same serial number and 
    a _different_ meter type in the database (serial number collision).

    Note : Will return False (no conflict) if devicetype in db is set to 
    NULL. 
    """

    # First, if the device type contains the wildcard we consider it being 'no conflict'
    if device_type[0: len(PREFIX_DEVICE_MODEL_NAME_WILDCARD)] == PREFIX_DEVICE_MODEL_NAME_WILDCARD:
        return False

    # Then, check if this device is already in the database - also taking the wildcard into account
    sqlQ   = "SELECT COUNT(*) FROM devices WHERE serialnumber = %s AND devicetype != %s AND LEFT(devicetype, LENGTH(%s)) != %s"
    device_serial = ToStr(device_serial)
    paramQ = (device_serial, device_type, PREFIX_DEVICE_MODEL_NAME_WILDCARD, PREFIX_DEVICE_MODEL_NAME_WILDCARD)
    result = PerformQuery (sqlQ, paramQ, True)

    if result[0][0] == 0:
        # There was no conflicting device, but we need to check again - there might be an encrypted device conflicting
        paramQ = (EncryptSerialNumber(device_serial)[0], device_type, PREFIX_DEVICE_MODEL_NAME_WILDCARD, PREFIX_DEVICE_MODEL_NAME_WILDCARD)
        result = PerformQuery (sqlQ, paramQ, True)

    # If there is something else than zero results, there is a conflict
    return result[0][0] != 0

def InsertListIntoDatabase( inList, device_class, allowDupsWithinTrans, unused_field1, unused_field2 ):
    """
    Insert list of values into database.
    
    if allowDupsWithinTrans is set to True, the code first checks if elements in the list exists in the database,
     and afterwards inserts the non-existing elements to the database. Otherwhise each element is handled
     individiually.
    """

    if inList.has_key("header"):
        header = inList["header"]
    else:
        return (False, ERROR_CODE_DATABASE)

    SelectDatabase(header["terminal_serial"])

    global connection
    
    allValuesInserted = True

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            pass

    device_id   = None
    transfer_id = None

    # If it is already in the database must make sure that it is the same meter-type. 
    if ((device_class is not DEVICE_USER) and HasDeviceTypeConflict(header["meter_serial"], header["meter_type"])):
        return (False, ERROR_CODE_SERIAL_NUMBER_ALREADY_REGISTERED)


    # Find the device and terminal and transfer ID:s
    terminal_id = GetTerminalIdFromDatabase(header["terminal_serial"])

    # First try to get an existing device (encrypted serial or not).
    device_id = GetDeviceIdFromDatabase(header["meter_serial"], header["meter_type"], device_class)
    if device_id == None:
        # If no device is found we need to get the admin of the terminal and see if it has the
        # hide_pii property set (= encrypted serial).
        admin = GetTerminalAdmin(header["terminal_serial"])
        device_serial = header["meter_serial"]
        obfuscated_serial = None
        if ClinicRequiresEncryptedDeviceSerial(header["terminal_serial"], admin, device_class):
            unencrypted_device_serial = device_serial 
            device_serial, obfuscated_serial = EncryptSerialNumber(unencrypted_device_serial)
            print "Create new device with encrypted serial from transmitter (%s -> %s)" % (unencrypted_device_serial, device_serial)
        device_id = InsertDeviceInDatabase(device_serial, obfuscated_serial, header["meter_type"], device_class)

    transfer_id = InsertTransferInDatabase(terminal_id, device_id)

    # If there is a device version available, store it
    if header.has_key(ELEM_SW_VERSION):
        version_recommended = None
        if header.has_key(PARAM_DEVICE_VERSION_RECOMMENDED):
            version_recommended = header[PARAM_DEVICE_VERSION_RECOMMENDED]
        else:
            version_recommended = None
        if not UpdateDeviceVersionInDatabase(device_id, header[ELEM_SW_VERSION], version_recommended):
            DisconnectFromDatabase()
            return (False, ERROR_CODE_DATABASE)


    results = []
        
    if inList.has_key("results"):
        results = inList["results"]

    if (len(results) > 0):
        # If duplicates within a transaction are allowed, the elements that already exists in
        # shall be removed from the list, and then the list is inserted without checks
        if allowDupsWithinTrans:
            results = RemovePresentElementsFromList(results, device_id)

    device_setting_group_id = -1

    for value in results:

        if value.has_key("meter_flaglist"):
            flags = ToStr(len(value["meter_flaglist"]))
        else:
            flags = "0"

        if value.has_key( ELEM_VAL_TYPE ):
            if allowDupsWithinTrans or not IsValuePresentInDatabase( value, device_id ):
                # print "INSERT INTO DATABASE SERIAL %s DATE %s VALUE %d TYPE %s NR FLAGS %s" % (header["meter_serial"], value[ELEM_TIMESTAMP], value[ ELEM_VAL ], value[ ELEM_VAL_TYPE ], flags)
                allValuesInserted = InsertValueIntoDatabase( value, device_id, transfer_id )
            else:
                pass
                # print "ALREADY IN DATABASE SERIAL %s DATE %s VALUE %d TYPE %s NR FLAGS %s" % (header["meter_serial"], value[ ELEM_TIMESTAMP], value[ ELEM_VAL ], value[ ELEM_VAL_TYPE ], flags)

        elif value.has_key( SETTINGS_LIST ):
            if device_setting_group_id < 0 :
                device_setting_group_id = CreateDeviceSettingGroup(device_id, transfer_id)
            
            if allValuesInserted:
                # print "INSERT INTO DATABASE SERIAL %s Advanced Settings" % (header["meter_serial"])
                InsertPumpSettingsIntoDatabaseNG(device_setting_group_id, value)

        elif value.has_key( PROGRAM_TYPE ):
            if device_setting_group_id < 0 :
                device_setting_group_id = CreateDeviceSettingGroup(device_id, transfer_id)
            
            if allValuesInserted:
                # print "INSERT INTO DATABASE SERIAL %s Device Program %s" % ( header["meter_serial"], ToStr(value[PROGRAM_NAME]) )
                InsertPumpProgramsInDatabaseNG(device_setting_group_id, value)

        elif value.has_key(COMMENT_TYPE):
            allValuesInserted = InsertDeviceComment(device_id, value[COMMENT_TYPE], value[ELEM_TIMESTAMP])

        elif BLACK_BOX_HISTORY_RECORD in value:
            allValuesInserted = InsertBlackBoxHistory(value, device_id, transfer_id)

        else:
            allValuesInserted = False
            
        # No meaning to continue if something went wrong
        # The transfer will be NAK:ed
        if not allValuesInserted:
            print "   FAILED TO INSERT: " + repr(value)
            break;
            
    DisconnectFromDatabase()

    return (allValuesInserted, ERROR_CODE_DATABASE)


# ------------------------------------------------------------------------------

def UpdateIncomingData( terminal_serial, sizeOfIncomingData ):
    """
    Update information about transfer
    """

    SelectDatabase(terminal_serial)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return

    # Build SQL query
    sqlQ = """
        UPDATE terminals SET amount_data=amount_data+%s, nr_transfers=nr_transfers+1 WHERE terminal_id=%s
        """
    paramQ = ( ToStr(sizeOfIncomingData), terminal_serial )

    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, paramQ )
        r = cursor.fetchall()
        cursor.close()

    except MySQLdb.MySQLError:
        pass

    DisconnectFromDatabase()


# ------------------------------------------------------------------------------

def InsertErrorIntoDatabase( value ):
    """
    Insert error into database
    """
    global connection

    # Are we conntected?
    if not connection:
        return False


    errorcode = "0"
    hw_nr = "0"
    sw_nr = "0"
    bl_nr = "0"
    serial_terminal = "0"
    serial_meter = "0"
    metertype = "unknown"
    data = ""

    if value.has_key("header"):
        h = value["header"]

        if h.has_key( "terminal_hwid" ):
            hw_nr = ToStr(h["terminal_hwid"])

        if h.has_key( "terminal_swid" ):
            sw_nr = ToStr(h["terminal_swid"])

        if h.has_key( "terminal_blid" ):
            bl_nr = ToStr(h["terminal_blid"])

        if h.has_key( "terminal_serial" ):
            serial_terminal = ToStr(h["terminal_serial"])

        if h.has_key( "meter_serial" ):
            serial_meter = ToStr(h["meter_serial"])

        if h.has_key( "meter_type" ):
            metertype = ToStr(h["meter_type"])

    if value.has_key("results"):
        r = value["results"][0]

        if r.has_key( "error_code" ):
            errorcode = ToStr(r["error_code"])

        if r.has_key( "fault_data" ):
            data = ToStr(r["fault_data"])
        
    EventLog.EventLogError('Insert error %s into database.' % (errorcode,))

    # Build SQL query
    sqlQ = """
        INSERT INTO errorlog (errorcode,hw_nr,sw_nr,bl_nr,serial_terminal,serial_meter,metertype,data)
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
        """
    paramQ = ( errorcode,hw_nr,sw_nr,bl_nr,serial_terminal,serial_meter,metertype,data)

    insertedValue = False
    try:
        cursor = connection.cursor()
        cursor.execute( sqlQ, paramQ )
        cursor.close()
        insertedValue = True

    except MySQLdb.MySQLError:
        insertedValue = False

    return insertedValue

# ------------------------------------------------------------------------------

def InsertErrorLogIntoDatabase( inList ):
    """
    Insert list of values into database.
    """
    
    if inList.has_key("header"):
        h = inList["header"]
        SelectDatabase(h["terminal_serial"])
    
    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            print "Failed to connect to database"
            pass

    if not InsertErrorIntoDatabase( inList ):
        # Todo : insert error into error table in database
        print "Failed to insert error into database"
                
    DisconnectFromDatabase() 

# ------------------------------------------------------------------------------

def CheckSubscriptionLevel(terminal_serial, device_class):
    """
    Check if the subscription for this terminal allows data from this device type
    """    

    SelectDatabase(terminal_serial)
    
    global connection

    # No device type found, we can not allow this
    if device_class == DEVICE_TOO_EARLY:
        return False

    # No special subscription is currently required for Meters or Pens
    if device_class in (DEVICE_METER, DEVICE_PEN, DEVICE_PEN_CAP, DEVICE_USER, DEVICE_CLOUD):
        return True

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            print "Failed to connect to database"
            return

    # Build SQL query
    sqlQ = """
        SELECT terminal_id FROM terminal_permissions WHERE terminal_id=%s AND permission=%s
        """
    
    paramQ = ( terminal_serial, device_class )

    result = DoesQueryReturnResults(sqlQ, paramQ)
      
    DisconnectFromDatabase() 

    return result

# ------------------------------------------------------------------------------

def SelectDefaultDatabase():
    """
    Selects the default database
    """
    global host
    global user
    global passwd
    global port
    global db
    
    host    = default_host
    user    = default_user
    passwd  = default_passwd
    port    = default_port
    db      = default_db
    
def CheckLatestOldTransmitterVersion():
    """
    Returns the current version of the old Transmitter software
    which can be downloaded from the update site
    """
    SelectDefaultDatabase()
    
    if not ConnectToDatabase():
        return None
        
    line = FindInDatabase("value", "metadata", "`key`", "old_terminal_version")
    if line:
        return line[0]
    return None
    

def SelectDatabase(terminal_serial):
    """
    Given a serial number of the terminal, 
    this function will set up the configuration of the database
    """
    global host
    global user
    global passwd
    global port
    global db

    SelectDefaultDatabase()

    _conn = None

    # First connect to the default DB and ask for server information
    try:
        _conn = MySQLdb.connect( host=default_host, user=default_user, passwd=default_passwd, port=default_port, db=default_db, charset="utf8" )
    except MySQLdb.MySQLError:
        print "Failed to connect to database, terminal will be directed to default DB"
        # Failed to connect, bad situation, default to the default database, which we couldn't connect to :(
        return
    
    # Now check info regarding this server.
    # Build SQL query
    sqlQ = """
        SELECT hostname, username, passwd, port, db FROM database_servers, terminal_locations WHERE database_servers.server_id=terminal_locations.database_server and terminal_locations.terminal_id=%s
        """
    paramQ = ( ToStr(terminal_serial) )

    # result
    r = ()

    try:
        cursor = _conn.cursor()
        cursor.execute( sqlQ, paramQ )
        r = cursor.fetchall()
        cursor.close()
    except MySQLdb.MySQLError:
        # Failed to perform query
        r = None

    results = 0;
    
    if r:
        results = len(r) 

    # No results or query failed, set default and return
    if results == 0:
        _conn.close()
        return

    #we have our server information
    try:
        host    = ToStr(r[0][0])
        user    = ToStr(r[0][1])
        passwd  = ToStr(r[0][2])
        port    = int(r[0][3])
        db      = ToStr(r[0][4])
        print "Terminal " + terminal_serial + " will be redirected to " + host 
    except:
        host    = default_host
        user    = default_user
        passwd  = default_passwd
        port    = default_port
        db      = default_db

    _conn.close()



def SelectDatabaseByLocation(location):
    
    global host
    global user
    global passwd
    global port
    global db

    if not location in database_servers:
        return False

    SelectDefaultDatabase()
    if not ReconnectToDatabase():
        return False

    sqlQ = """
        SELECT hostname, username, passwd, port, db FROM database_servers WHERE database_servers.server_id=%s
        """
    paramQ = ( database_servers[location] )

    result = PerformQuery(sqlQ, paramQ, True)

    print "SelectDatabaseByLocation()", location, result

    if result and len(result)>0:
        result = result[0]
        host = result[0]
        user = result[1]
        passwd = result[2]
        port = int(result[3])
        db = result[4]
    else:
        return False

    return True
     


def GetDeviceUsers(device_id):
    """
    For a specific device, find all users that "owns" the device
    """
    sqlQ = """
        SELECT device_ownership.pcode as pcode
            FROM device_ownership
                 LEFT JOIN users 
                    ON (device_ownership.pcode = users.pcode)
            WHERE device_ownership.device_id = %s
    """
    ret = []
    paramQ = (ToStr(device_id))
    users = PerformQuery(sqlQ, paramQ, True)
    if users:
        for u in users:
            ret.append(u[0].decode("utf8"))

    return ret

def GetUserDeviceFromUsername(user):
    """
    Finds the device_user for the user
    """
    sqlQ = """
        SELECT devices.serialnumber 
            FROM users
            LEFT JOIN device_ownership ON device_ownership.pcode = users.pcode
            LEFT JOIN devices on device_ownership.device_id = devices.seq
                WHERE users.pcode = %s
                AND deviceclass = 'device_user'
    """
    paramQ = (ToStr(user))
    
    # @TODO: Make sure that the correct database is selected
    
    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return None

    results = PerformQuery(sqlQ, paramQ, True)
    DisconnectFromDatabase()

    if len(results) == 1:
        return results[0][0]
    else:
        return None

def GetUserDeviceFromMobileNr(nr):
    """
    Finds the device_user for the user that has this nr as mobile nr
    """
    sqlQ = """
        SELECT devices.serialnumber 
            FROM user_strings 
            LEFT JOIN users ON user_strings.pcode = users.pcode 
            LEFT JOIN device_ownership ON device_ownership.pcode = users.pcode
            LEFT JOIN devices on device_ownership.device_id = devices.seq
                WHERE user_strings.string = 'mobile_tel'
                AND user_strings.value = %s
                AND deviceclass = 'device_user'
    """
    paramQ = (ToStr(nr))


    # The phone number happen to be the terminal serial
    SelectDatabase(nr)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return None

    results = PerformQuery(sqlQ, paramQ, True)
    DisconnectFromDatabase()

    if len(results) == 1:
        return results[0][0]
    else:
        return None
    
    
def InsertComment(pcode, comment, timestamp):
    """
    Inserts a comment in the database for a given user
    """
    sqlQ = """
        INSERT INTO communication (pcode, comment, timestamp)
            VALUES (%s, %s, %s)        
    """
    if type(pcode) == types.UnicodeType:
        pcode_val = pcode.encode("utf8")
    else:
        pcode_val = ToStr(pcode)

    if type(comment) == types.UnicodeType:
        comment_val = comment.encode("utf8")
    else:
        comment_val = ToStr(comment)
    
    paramQ = (pcode_val, comment_val, ToStr(timestamp))
    return PerformQuery(sqlQ, paramQ)
    
def InsertDeviceComment(device_id, comment, timestamp):
    """
    Inserts a comment to all users owning the device
    """
    users = GetDeviceUsers(device_id)
    if not users:
        return False
    
    for u in users:
        if not InsertComment(u, comment, timestamp):
            return False

    return True

def GetTerminalDefaultUnit(terminal_serial):
    """
    Checks which unit the customer that has this terminal uses (mg/dl or mmol/l)
    """
    unit = "mmol/l"

    SelectDatabase(terminal_serial)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return None

    # Build SQL query
    sqlQ = """
        SELECT admin_properties.value
        FROM admin_properties,admins,terminals
        WHERE terminals.terminal_id=%s 
            AND LOWER(terminals.admin) = LOWER(admins.name)
            AND admin_properties.admin = admins.name
            AND admin_properties.property='glucose_unit'
    """
    paramQ = ( terminal_serial )

    ret = PerformQuery( sqlQ, paramQ, True )
    if ret and len(ret) > 0:
        if int(ret[0][0]) == 1:
            unit = "mg/dl"

    DisconnectFromDatabase()

    return unit

def IsTerminalBlacklisted(terminal_serial):
    """
    Checks if the terminal has the blacklist property set
    """
    blacklisted = False

    SelectDatabase(terminal_serial)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return None

    # Build SQL query
    sqlQ = """
        SELECT terminal_properties.value FROM terminal_properties,terminals WHERE terminal_properties.terminal_id=terminals.seq
        AND terminals.terminal_id = %s AND terminal_properties.property="blacklisted";
    """

    paramQ = ( terminal_serial )

    ret = PerformQuery( sqlQ, paramQ, True )
    if ret and len(ret) > 0:
        if int(ret[0][0]) == 1:
            blacklisted = True

    DisconnectFromDatabase()

    return blacklisted

def GetDBVersion():
    """
    Check the version of the current database layout
    """
    if not ConnectToDatabase():
        print "Failed to connect to the database"
        # Todo : Failed to connect to database, can't write error to error table
        # in database, have to write to backup error log
        return None
    
    line = FindInDatabase("value", "metadata", "`key`", "db_version")
    DisconnectFromDatabase()

    if line:
        return line[0]
    else:
        return None
        
# ------------------------------------------------------------------------------
# Functions, used by functionality required by the software transmitter
# They are user-centered and knows things about the internal storage, so they differ from other Backend functions

def HasUserFileUploadAccess(user):
    """
    Checks if a user has fileupload access (srpa)
    """
    sqlQ = """
        SELECT * FROM user_properties WHERE pcode = %s AND (property = 
        '""" + USER_PROPERTY_FILE_UPLOAD_SRPA + """' AND value = 1)
    """

    paramQ = ( user )
    
    _ret = PerformQuery( sqlQ, paramQ, True )

    if _ret and len(_ret) > 0:
        return True
    else:
        return False

def HasUserFileUploadAccessByClinic(user):
    """
    Checks if a user has fileupload access by clinic.
    """
    sqlQ = """
        SELECT * FROM user_properties WHERE pcode = %s AND (property = 
        '""" + USER_PROPERTY_FILE_UPLOAD_ADMIN + """' AND value = 1)
    """

    paramQ = ( user )
    
    _ret = PerformQuery( sqlQ, paramQ, True )

    if _ret and len(_ret) > 0:
        return True
    else:
        return False

def IsUserExpired(user):
    """
    Checks if all owners of this device are expired
    """
    sqlQ = """
        SELECT * FROM user_properties WHERE pcode=%s AND property="account_expire" AND value>UNIX_TIMESTAMP()
    """

    paramQ = ( user )
    
    _ret = PerformQuery( sqlQ, paramQ, True )

    # If we got anything in return we managed to find one non-expired user.
    if _ret and len(_ret) > 0:
        return False

    return True

def IsUserActive(user):
    """
    Checks if all owners of this device are archived
    """
    sqlQ = """
        SELECT users.pcode,archived FROM users WHERE pcode=%s AND archived='No'
    """

    paramQ = ( user )
    
    _ret = PerformQuery( sqlQ, paramQ, True )

    # If we got anything in return we managed to find on or more non-archived users
    if _ret and len(_ret) > 0:
        return True

    return False

def HasClinicDDAAccess(terminal_id, admin):
    """
    Checks if clinic has allow_dda property
    """
    
    SelectDatabase(terminal_id)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return

    sqlQ = """
        SELECT value FROM admin_properties WHERE admin=%s AND property='allow_dda' AND value='1'
    """

    paramQ = ( admin )
    
    _ret = PerformQuery( sqlQ, paramQ, True )
   
    if _ret and len(_ret) > 0:
        return True
    else:
        return False
    
def SetFileUploadAccess(pcode):
    """
    Sets fileupload access as SRPA for the specified used
    """
    sqlQ = """
        REPLACE INTO user_properties (pcode, property, value)
        VALUES (%s, %s, %s)
    """

    paramQ = ( pcode, USER_PROPERTY_FILE_UPLOAD_SRPA, 1 )
    
    return PerformQuery( sqlQ, paramQ, True )
    
def CreateUser(pcode, password, firstname, lastname):
    """
    Create a user with the specified properties.
    """
    
    sqlQ = """
        INSERT INTO users (pcode, password, firstname, lastname)
        VALUES (%s, %s, %s, %s)
    """

    paramQ = ( pcode, password, firstname, lastname )
    
    return PerformQuery( sqlQ, paramQ, True )    


def RegisterTerminalOnClinic( terminal_id, clinic ):
    """
    Register a clinc (admin) for this terminal
    """
    SelectDatabase(terminal_id)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return

    sqlQ = """UPDATE terminals SET admin=%s WHERE terminal_id=%s"""

    paramQ = ( clinic, terminal_id )
   
    return PerformQuery( sqlQ, paramQ )

def GetTerminalAdmin( terminal_id ):
    """
    Checks if terminal is owned by a clinic
    """
    
    SelectDatabase(terminal_id)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return

    sqlQ = """
        SELECT name FROM terminals,admins
        WHERE LOWER(terminals.admin)=LOWER(admins.name) AND terminal_id = %s
    """

    paramQ = ( terminal_id )
   
    _ret = PerformQuery( sqlQ, paramQ, True )
   
    if _ret and len(_ret) > 0:
        return _ret[0][0]
    else:
        return None
   
def IsTerminalRegisteredOnClinic( terminal_id ):
    
    if GetTerminalAdmin( terminal_id ):
        return True

    return False
 
def RegisterDeviceOnUser(pcode, device_id, deviceclass):
    """
    Registers the device on the specific user.
    @TODO: Fill the "extra" parameter, and check the number of devices on each class
    """

    if deviceclass == DEVICE_PUMP:
        # Reset the primary device flag.
        sqlQ = 'UPDATE device_ownership SET primary_device = "0" WHERE pcode = %s'
        paramQ = (pcode)
        _ret = PerformQuery(sqlQ, paramQ)

        paramQ = (pcode, device_id, "1")
    else:
        paramQ = (pcode, device_id, "0")

    sqlQ = "REPLACE INTO device_ownership (pcode, device_id, primary_device) VALUES (%s, %s, %s)"
    _ret = PerformQuery( sqlQ, paramQ, True )
    
    return True
   
def DeviceRegisteredOnUser(pcode, device_id):
    """
    Is this device registered on this pcode?
    """
    sqlQ = 'SELECT device_id FROM device_ownership WHERE pcode = %s AND device_id = %s'
    paramQ = (pcode, device_id)
    ret = PerformQuery(sqlQ, paramQ, True)
    if ret:
        return len(ret) > 0
    return False

def UsersRegisteredAsOwnerOfDevice(device_id):
    """
    Return of list of users (pcodes) that is registered
    as owners of the device.
    """
    sqlQ  = 'SELECT pcode FROM device_ownership WHERE device_id = %s'
    paramQ = (device_id,)
    ret = PerformQuery(sqlQ, paramQ, True)
    if ret:
        # Return value is a tuple where each item is a tuple with the 
        # selected columns, e.g. (('f1@srpa.com',), ('f2@srpa.com',)). 
        # Let's unflatten the tuple and create a list of pcodes.
        return [pcode[0] for pcode in ret]
    return []    

def UnregisterDeviceOnUser(username, device_id, device_class):
    """
    Unregister device from user.
    """
    sqlQ = 'DELETE FROM device_ownership WHERE pcode = %s AND device_id = %s'
    paramQ = (username, device_id)

    print sqlQ, paramQ

    PerformQuery(sqlQ, paramQ, False)

def NumberOfRegisteredDevicesOnUser(pcode, device_class):
    """
    Return number of registered devices of class device_class on 
    user pcode. 
    """
    sqlQ = '''SELECT device_id FROM device_ownership 
                LEFT JOIN devices ON devices.seq = device_ownership.device_id
                    WHERE pcode = %s AND deviceclass = %s'''
    paramQ = (pcode,device_class)
    ret = PerformQuery(sqlQ, paramQ, True)
    if ret:
        return len(ret)
    return 0

def PumpIsPrimaryForNumberOfUsers(device_id):
    """
    Return number of users who has this pump as a primary device. 
    """
    sqlQ = 'SELECT device_id FROM device_ownership WHERE device_id=%s AND primary_device="1"'
    paramQ = (device_id)
    ret = PerformQuery(sqlQ, paramQ, True)
    if ret:
        return len(ret)
    return 0

def PumpIsPrimaryForUser(pcode, device_id):
    """
    Is this pump a primary pump for this user?
    """
    sqlQ = 'SELECT device_id FROM device_ownership WHERE pcode=%s AND device_id=%s AND primary_device="1"'
    paramQ = (pcode, device_id)
    ret = PerformQuery(sqlQ, paramQ, True)
    if ret:
        return len(ret) > 0
    return False

def GetClinicOfClinicUser(username):
    """
    Return which clinic a clinic user belongs to
    """
    _line = FindInDatabase("admin", "clinic_users", "username", username)

    return _line[0]

def ClinicRequiresEncryptedDeviceSerial(terminal_id, admin, device_class):
    """
    Check if the clinic requires encrypted serial numbers (for devices). 
    """
    
    if not (device_class in (DEVICE_PUMP)):
        return False

    SelectDatabase(terminal_id)

    global connection

    if not connection:
        if not ConnectToDatabase():
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            return

    sqlQ = 'SELECT value FROM admin_properties WHERE admin=%s AND property="hide_pii" AND value="1"'

    paramQ = (admin)
    
    _ret = PerformQuery(sqlQ, paramQ, True)
  
    return _ret and len(_ret) > 0

def UserRequiresEncryptedDeviceSerial(user, device_class):
    """
    Checks if a user (patient) requires encrypted serial numbers for devices.
    """
    if not (device_class in (DEVICE_PUMP)):
        return False

    sqlQ = 'SELECT * FROM user_properties WHERE pcode = %s AND property = "country" AND value = "109"'

    paramQ = (user)
    
    _ret = PerformQuery(sqlQ, paramQ, True)

    print sqlQ, _ret

    return _ret and len(_ret) > 0
    
    
def HasClinicUnitedSupport(clinic):
    """
    Checks if a clinic has United package support.
    """
    sqlQ = 'SELECT COUNT(*) as c FROM admins JOIN admin_properties ON admins.name = admin_properties.admin WHERE admins.name = %s AND property = "'+ADMIN_PROPERTY_UNITED_SUPPORT+'" AND value = "1"'

    paramQ = (clinic)
    
    _ret = PerformQuery(sqlQ, paramQ, True)
    
    return _ret and (_ret[0][0] == 1)

def HasPatientAnimasSupport(pcode):
    """
    Checks if a clinic has Animas package support.
    """
    sqlQ = 'SELECT COUNT(*) as c FROM user_properties WHERE pcode = %s AND property = "'+USER_PROPERTY_ANIMAS_SUPPORT+'" AND value = "1"'

    paramQ = (pcode)
    
    _ret = PerformQuery(sqlQ, paramQ, True)
    
    return _ret and (_ret[0][0] == 1)

    
def IsClinicBelongingToAnimasMiddleAdmins(clinic):
    """
    Checks if the specified clinic belongs to the configured Animas Middle admins
    """
    _line = FindInDatabase("super_admin", "admins", "name", clinic)
    
    if _line != None:
        return _line[0] in Config.settings["united_rules-animas_middle_admins"].split()

    return False
    
def IsDeviceCoveredByRule(device_type, rule):
    """
    Checks if a device is covered by a specified rule
    """
    m = re.match(Config.settings["united_rules-device_rule_"+rule.lower()], device_type)
    if m:
        return True
        
    return False

def EndSyncIntoDatabase(device_id, operating_system, operating_system_version, software_version, is_software_transmitter, patient_cookie):
    pass
    

if __name__ == "__main__":

#    global host
#    global passwd
#    host = "berserk"
#    passwd = ""
    
    if not connection:
        if not ConnectToDatabase():
            print "failed"
            # Todo : Failed to connect to database, can't write error to error table
            # in database, have to write to backup error log
            pass

#    print FindInDatabase("seq", "devices", "serialnumber", "VKF412BAY")

#    InsertTerminalInDatabase("aaa")
    #print InsertDeviceInDatabase("a2", "apa", "device_meter")
    
    #dev_id = GetDeviceIdFromDatabase("46708889791")
    #print InsertDeviceComment(dev_id, "hej hej", "2009-12-24 15:30")
    
    #print InsertSoftTerminalInDatabase('DDA-DIAS-', 1)
    
    #RegisterDeviceOnUser("tjtest7", 10001, DEVICE_PUMP)
    
    #assert (HasClinicUnitedSupport("clinic_u") == True)
    #assert (HasClinicUnitedSupport("clinic_c") == False)
    #assert (IsClinicBelongingToAnimasMiddleAdmins("clinic_c") == True)
    #assert (IsDeviceCoveredByRule("123456", "A") == False)
    #assert (HasPatientAnimasSupport("user.hej@diasend.com") == False)

    
    DisconnectFromDatabase()
