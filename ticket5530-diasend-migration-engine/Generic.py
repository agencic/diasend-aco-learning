# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import re
import types
from Defines import *
from datetime import datetime
import time

class CResult:
    
    def __init__(self, device_type = None, device_serial = None, device_class = None, device_bg_unit = None, device_version = None):
        self.__device_type = device_type
        self.__device_serial = device_serial
        self.__device_bg_unit = device_bg_unit
        self.__device_class = device_class
        self.__device_version = device_version
        self.__results = []
        self.__error = None

    def setDeviceType(self, device_type):
        self.__device_type = device_type
    
    def getDeviceType(self):
        return self.__device_type

    def hasDeviceType(self):
        return self.getDeviceType() != None

    def setDeviceSerial(self, serial):
        self.__device_serial = serial
        
    def getDeviceSerial(self):
        return self.__device_serial

    def hasDeviceSerial(self):
        return self.getDeviceSerial() != None

    def setDeviceBGUnit(self, unit):
        self.__device_bg_unit = unit
        
    def getDeviceBGUnit(self):
        return self.__device_bg_unit

    def hasDeviceBGUnit(self):
        return self.getDeviceBGUnit() != None

    def setDeviceClass(self, device_class):
        self.__device_class = device_class
    
    def getDeviceClass(self):
        return self.__device_class
    
    def hasDeviceClass(self):
        return self.getDeviceClass() != None
    
    def setDeviceVersion(self, version):
        self.__device_version = version

    def getDeviceVersion(self):
        return self.__device_version

    def hasDeviceVersion(self):
        return self.getDeviceVersion() != None

    def addResult(self, result):
        self.__results.append(result)

    def getResultList(self):
        return self.__results
    
    def clearResultList(self):
        self.__results = []
        
    def setDeviceError(self, error):
        self.__error = error

    def getDeviceError(self):
        return self.__error
        
    def hasDeviceError(self):
        return self.getDeviceError() != None

class CResults:
    def __init__(self, multiple_devices = False):
        self.__results = {}
        self.__global_model = None
        self.__multiple_devices = multiple_devices
        self.__global_device_unit = None
        self.__global_device_model = None
        self.__global_device_class = None
        self.__terminal_version = None
        self.__termination_record_found = False
        self.__nr_results = 0
        self.__nr_results_in_file = -1
        self.__global_device_version = None

    def setError(self, serial, error, faultdata = None):
        device = self.getDevice(serial)
        if not device:
            device = CResult(device_serial = serial, device_bg_unit = self.__global_device_unit, \
                device_type = self.__global_device_model, device_version = self.__global_device_version)

        if faultdata != None:
            device.setDeviceError({ "error_code":error, "fault_data":faultdata })
        else:
            device.setDeviceError(error)
            
        self.__results.clear()
        self.__results[device.getDeviceSerial()] = device
    
    def hasErrors(self):
        for r in self.__results:
            if self.__results[r].hasDeviceError():
                return True

        return False

    def supportsMultipleDevices(self):
        return self.__multiple_devices

    def canHaveMoreDevices(self):
        """
        Check if there might be more devices in the data
        
        Basically if we only support one and has found one, return false,
        otherwise true
        """
        if self.__multiple_devices or len(self.__results) == 0:
            return True
        else:
            return False

    def addDevice(self, serial):
        """
        Add new device, fails if the device exists already
        """
        if self.__results.has_key(serial):
            return False
        elif not self.__multiple_devices and len(self.__results) > 0:
            # Only support one device...
            return False
        else:
            self.__results[serial] = CResult(device_serial = serial, device_bg_unit = self.__global_device_unit, \
                device_type = self.__global_device_model)
            return self.__results[serial]

    def getDevice(self, serial):
        if not serial and not self.__multiple_devices and len(self.__results) == 1:
            # When only supporting one device, return it...
            return self.getFirstDevice()
        
        if self.__results.has_key(serial):
            return self.__results[serial]
            
        return None

    def getFirstDevice(self):
        return self.__results[min(self.__results)]

    def shouldLookForModel(self):
        """
        Do not look for model record if not needed
        """
        if self.__global_device_model:
            return False

        # No devices yet, look anyway
        if len(self.__results) == 0:
            return True

        for r in self.__results:
            if not self.getDevice(r).hasDeviceType():
                return True
                
        # All devices has a model already
        return False

    def setModel(self, serial, model):
        """
        If the serial is None, this is a global model
        apply it to all devices, and store it as global
        """
        if serial:
            device = self.getDevice(serial)
            if device:
                device.setDeviceType(model)
            else:
                return ERROR_CODE_NO_HEADER_FOUND
        else:
            # Global
            self.__global_device_model = model
            # Loop through all existing and set model
            for r in self.__results:
                self.getDevice(r).setDeviceType(model)
        return None

    def setGlobalModel(self, model):
        self.setModel(None, model)

    def isModelMissing(self):
        for r in self.__results:
            if not self.__results[r].hasDeviceType():
                return self.__results[r]
                
        return None

    def shouldLookForClass(self):
        """
        Do not look for class record if not needed
        """
        if self.__global_device_class:
            return False

        # No devices yet, look anyway
        if len(self.__results) == 0:
            return True

        for r in self.__results:
            if not self.getDevice(r).hasDeviceClass():
                return True

        # All devices has a clase already
        return False

    def setClass(self, serial, the_class):
        """
        If the serial is None, this is a global class
        apply it to all devices, and store it as global
        """
        if serial:
            device = self.getDevice(serial)
            if device:
                device.setDeviceClass(the_class)
            else:
                return ERROR_CODE_NO_HEADER_FOUND
        else:
            # Global
            self.__global_device_class = the_class
            # Loop through all existing and set class
            for r in self.__results:
                self.getDevice(r).setDeviceClass(the_class)
        return None

    def shouldLookForUnit(self):
        """
        Do not look for unit record if not needed
        """
        if self.__global_device_unit:
            return False
        
        # No devices yet, look anyway
        if len(self.__results) == 0:
            return True

        for r in self.__results:
            if not self.getDevice(r).hasDeviceBGUnit():
                return True

        # All devices has a unit already
        return False

    def setUnit(self, serial, unit):
        """
        If the serial is None, this is a global unit
        apply it to all devices, and store it as global
        """
        if serial:
            device = self.getDevice(serial)
            if device:
                device.setDeviceUnit(unit)
            else:
                return ERROR_CODE_NO_HEADER_FOUND
        else:
            # Global
            self.__global_device_unit = unit
            # Loop through all existing and set unit
            for r in self.__results:
                self.getDevice(r).setDeviceBGUnit(unit)
        return None

    def shouldLookForDeviceVersion(self):
        """
        Do not look for unit record if not needed
        """
        if self.__global_device_version:
            return False
        elif not self.__multiple_devices:
            return True

        for r in self.__results:
            if not self.getDevice(r).hasDeviceVersion():
                return True

        # All devices has a version already
        return False

    def setDeviceVersion(self, serial, version):
        """
        If the serial is None, this is only when only
        single device is supported
        """
        if serial:
            device = self.getDevice(serial)
            if device:
                device.setDeviceVersion(version)
            else:
                return ERROR_CODE_NO_HEADER_FOUND
        else:
            if self.__multiple_devices:
                return ERROR_CODE_SERIAL_MISSING_IN_RESULT
                
            self.__global_device_version = version
            # Loop through all existing and set version
            for r in self.__results:
                self.getDevice(r).setDeviceVersion(version)
        return None

    def shouldLookForNrResultsInFile(self):
        return self.__nr_results_in_file == -1
        
    def setNrResultsInFile(self, nr):
        self.__nr_results_in_file = nr

    def getNrResultsInFile(self):
        return self.__nr_results_in_file

    def shouldLookForTerminationRecord(self):
        return self.__termination_record_found != True

    def setTerminationRecordFound(self, found):
        self.__termination_record_found = found

    def isTerminationRecordFound(self):
        return self.__termination_record_found

    def shouldLookForTerminalVersion(self):
        return not self.__terminal_version
        
    def setTerminalVersion(self, version):
        self.__terminal_version = version

    def incNrResults(self):
        self.__nr_results += 1

    def getNrResults(self):
        return self.__nr_results

    def foundAnyResults(self):
        return len(self.__results) > 0

    def generateOutList(self):
        
        outList = []
        for serial in self.__results:
            r = self.__results[serial]
            output = {}
            if r.hasDeviceError():
                output["results"] = [r.getDeviceError()]
            else:
                output["results"] = r.getResultList()
            header = {}
            header[ "meter_type" ] = r.getDeviceType()
            header[ "meter_serial" ] = r.getDeviceSerial()
            if r.hasDeviceClass():
                header["device_class"] = r.getDeviceClass()
            if r.hasDeviceVersion():
                header.update(r.getDeviceVersion())
            if self.__terminal_version:
                header.update(self.__terminal_version)
            output["header"] = header
            outList.append(output)
        return outList
                
    def clearNrResults(self):
        self.__nr_results = 0

    def clearResults(self):
        for r in self.__results:
            self.__results[r].clearResultList()
        self.clearNrResults()

# -----------------------------------------------------------------------------
# Misc. functions
# -----------------------------------------------------------------------------

def CreateErrorResponseList(meter_type, meter_serial, error_code, description):
    """
    Create a response list that the generic analyzer in this module understands. 
    
    @param meter_type
        String with meter type (e.g. Dexcom Seven Plus). 
        
    @param meter_serial
        Meter serial number.
        
    @param error_code
        Error code from Defines.py. 
        
    @param description 
        Description of the error. Will end up in the data column in the errorlog table. 
    """
    return [{ \
                'header': {'meter_type': meter_type, 'meter_serial': meter_serial}, \
                'results': [{'fault_data': description, 'error_code': error_code}] \
           }]

def DetectDevice( meterStr, headerDeviceType, deviceType ):
    """
    Detect if data comes from specified meter.
    The function returns None or the passed device type
    """
    if meterStr in headerDeviceType:
        return deviceType

    return None

def ResultIsError( resultList ):
    for result in resultList:
        if result.has_key( "error_code" ):
            return True
    return False

def ResultIsErrorOfType( resultList, errorKey ):
    for result in resultList:
        if result.has_key( "error_code" ):
            if result[ "error_code" ] == errorKey:
                return True
    return False

def ResultDeviceClass(resultList):
    """
    Returns the device class from a result list, note that it might be None!
    """
    if resultList.has_key("header"):
        header = resultList["header"]
        if header.has_key("device_class"):
            return header["device_class"]
    
    return None

def GetResultSerial(result):
    """
    Check if a result record has a serial otherwise return None
    """
    if result.has_key(ELEM_DEVICE_SERIAL):
        return result[ELEM_DEVICE_SERIAL]
    else:
        return None

def HandleResults(results, meterDict, resDicts, checkSumFun):
    # Some devices might return more than one result per record
    # So if it did not, convert it to a list of results
    if type(resDicts) != types.ListType:
        if len(resDicts) > 0:
            resDicts = [resDicts]
        else:
            resDicts = []

    for resDict in resDicts:
        current_device = results.getDevice(GetResultSerial(resDict))
        if not current_device:
            if results.supportsMultipleDevices() and not results.getDevice(GetResultSerial(resDict)):
                results.setError(None, ERROR_CODE_SERIAL_MISSING_IN_RESULT, resDict)
                return
            else:
                # Single device support, the serial must be found before any results....
                results.setError(None, ERROR_CODE_NO_HEADER_FOUND, resDict)
                return
        
        if any(key in resDict for key in (ELEM_VAL_TYPE, SETTINGS_LIST, PROGRAM_TYPE, BLACK_BOX_HISTORY_RECORD)):

            # Calculate checksum
            if checkSumFun:
                if not checkSumFun(resDict):
                    results.setError(current_device.getDeviceSerial(), ERROR_CODE_CHECKSUM, resDict)
                    return

            # Create an empty dictionary
            d = {}

            # Add the header information
            if resDict.has_key(ELEM_TIMESTAMP):
                # there are some meters that has elements without timestamp!
                if resDict[ ELEM_TIMESTAMP ] != None:
                    d[ ELEM_TIMESTAMP ]  = resDict[ ELEM_TIMESTAMP ].isoformat(' ')

                    # Check if this timestamp is in the future, due to some time difference
                    # Check if the timestamp is more than one day in the future
                    ref = datetime.now()
                    ref =  datetime.fromtimestamp(time.mktime(ref.timetuple()) + 60 * 60 * 24)
                    if ref < resDict[ ELEM_TIMESTAMP ]:
                        if not resDict.has_key( ELEM_FLAG_LIST ):
                            resDict[ ELEM_FLAG_LIST ] = []
                        resDict[ ELEM_FLAG_LIST ].append(FLAG_TIMESTAMP_IN_FUTURE)
                else:
                    d[ ELEM_TIMESTAMP ] = "2000-00-00 00:00:00"                    

            # Check if this is a value
            if resDict.has_key( ELEM_VAL_TYPE ):
                
                if resDict.has_key( ELEM_VAL ):
                    _val = resDict[ ELEM_VAL ]
                else:
                    _val = 0
                
                # Convert value to mmol/l if it is a ketone or glucose value
                if resDict[ELEM_VAL_TYPE] == VALUE_TYPE_GLUCOSE or resDict[ELEM_VAL_TYPE] == VALUE_TYPE_KETONES:
                    if  resDict.has_key( ELEM_DEVICE_UNIT ):
                        unit = resDict[ELEM_DEVICE_UNIT]
                    else:
                        unit = current_device.getDeviceBGUnit()

                    if not unit:
                        results.setError(current_device.getDeviceSerial(), ERROR_CODE_UNIT_ERROR, resDict)
                        return
                        
                    if "mg/dl" in unit.lower():
                        # Convert value to mmol/L
                        _val = round( float(_val*1000) / 18.0, 1 )
                    else:
                        # Precision in database is higher than in the analysers...
                        _val = _val * 100

                d[ ELEM_VAL_TYPE ] = resDict[ELEM_VAL_TYPE]
                # Add value to dictionary
                d[ ELEM_VAL ] = _val
            
            elif resDict.has_key( SETTINGS_LIST ):
                # Add value to dictionary
                d[ SETTINGS_LIST ] = resDict[ SETTINGS_LIST ]

            elif resDict.has_key( PROGRAM_TYPE ):
                d[PROGRAM_TYPE] = resDict[PROGRAM_TYPE]
                d[PROGRAM_NAME] = resDict[PROGRAM_NAME]
                d[PROGRAM_PERIODS] = resDict[PROGRAM_PERIODS]

            if resDict.has_key( ELEM_FLAG_LIST ):
                d[ ELEM_FLAG_LIST ] = resDict[ ELEM_FLAG_LIST ]
            else:
                d[ ELEM_FLAG_LIST ] = []

            if resDict.has_key( ELEM_VALUE_LIST ):
                d[ELEM_VALUE_LIST] = resDict[ELEM_VALUE_LIST]
            else:
                d[ELEM_VALUE_LIST] = []

            if BLACK_BOX_HISTORY_RECORD in resDict:
                d[BLACK_BOX_HISTORY_RECORD] = resDict[BLACK_BOX_HISTORY_RECORD]

            # Store result in result list
            current_device.addResult(d)

        elif resDict.has_key( "error_code" ):
            results.setError(current_device.getDeviceSerial(), resDict)
            return
        
        elif resDict.has_key( "null_result" ):
            # Null result - nothing of use but no error
            pass
    

def AnalyseGenericMeter( inList, meterDict ):
    """
    Generic code for analysing meter input.
    """
    # Check if the list contains data from several meters
    if meterDict.has_key("multiple_devices"):
        results = CResults(meterDict["multiple_devices"])
    else:
        results = CResults()

    # Device model set, use this as the model for all devices
    if meterDict.has_key("meter_type"):
        results.setGlobalModel(meterDict["meter_type"])

    for line in inList:

        # Second part of analyse - found meterdata packet, analyse it!

        # Try to find serial number,
        # function should return dictionary with keys
        #   serial_number   > serial number
        # Only look for serial number if we have at most one results yet
        # It is only supported when there is one single device in the data
        if results.canHaveMoreDevices():
            resDict = meterDict["eval_serial_record"](line)
            if resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break
            serial = GetResultSerial(resDict)
            if serial:
                results.addDevice(serial)

        # Try to find device model,
        # function should return dictionary with keys
        #   device_model    > model name
        # Only look for model if no devices has been found
        if results.shouldLookForModel() and meterDict.has_key("eval_device_model_record"):
            resDict = meterDict[ "eval_device_model_record" ]( line )
            if resDict.has_key(ELEM_DEVICE_MODEL):
                err = results.setModel(GetResultSerial(resDict), resDict[ELEM_DEVICE_MODEL])
                if err:
                    results.setError(GetResultSerial(resDict), err, resDict)
                    break
            elif resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break

        if results.shouldLookForClass() and meterDict.has_key("eval_device_class_record"):
            # The analyser wants us to ask for the device type...
            resDict = meterDict[ "eval_device_class_record" ](line)
            if resDict.has_key(ELEM_DEVICE_CLASS):
                err = results.setClass(GetResultSerial(resDict), resDict[ELEM_DEVICE_CLASS])
                if err:
                    results.setError(GetResultSerial(resDict), err, resDict)
                    break
            elif resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break
                

        if results.shouldLookForUnit() and meterDict.has_key("eval_unit"):
            resDict = meterDict[ "eval_unit" ]( line )
            if resDict.has_key(ELEM_DEVICE_UNIT):
                err = results.setUnit(GetResultSerial(resDict), resDict[ELEM_DEVICE_UNIT])
                if err:
                    results.setError(GetResultSerial(resDict), err, resDict)
                    break
            elif resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break

        # Try find the version of the device
        if results.shouldLookForDeviceVersion() and meterDict.has_key("eval_device_version"):
            resDict = meterDict["eval_device_version"](line)
            if resDict.has_key(ELEM_SW_VERSION):
                err = results.setDeviceVersion(GetResultSerial(resDict), resDict)
                if err:
                    results.setError(GetResultSerial(resDict), err, resDict)
                    break
            elif resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break

        # Try to find nr results
        if results.shouldLookForNrResultsInFile() and meterDict.has_key("eval_nr_results"):
            resDict = meterDict[ "eval_nr_results" ](line)
            if resDict.has_key(ELEM_NR_RESULTS):
                results.setNrResultsInFile(resDict[ELEM_NR_RESULTS])
            elif resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break

        # Try to find termination record
        if results.shouldLookForTerminationRecord() and meterDict.has_key("eval_termination_record"):
            results.setTerminationRecordFound(meterDict[ "eval_termination_record" ](line))

        # Find version of terminal, if any
        if results.shouldLookForTerminalVersion() and meterDict.has_key("eval_terminal_version"):
            resDict = meterDict["eval_terminal_version"](line)
            if resDict.has_key(ELEM_TERMINAL_VERSION):
                results.setTerminalVersion(resDict)
            elif resDict.has_key( "error_code" ):
                results.setError(current_device.getDeviceSerial(), resDict)
                break

        # Try to find result record
        # function should return dictionary with keys
        #   date_time   > date in YYYY-MM-DD HH:MM:SS format
        #   value       > value (float)
        #   unit        > unit if present (otherwise require headerUnit)
        #   flags       > list of flags (int) if present

        resDicts = meterDict[ "eval_result_record" ]( line )
        if len(resDicts) != 0:
            # Found a result record!
            results.incNrResults()
            

        # Create a checksum function and pass it:
        checkSumFun = None
        if meterDict.has_key("eval_checksum_record") and meterDict["eval_checksum_record"]:
            checkSumFun = lambda x: meterDict[ "eval_checksum_record" ](line, x)

        HandleResults(results, meterDict, resDicts, checkSumFun)
        
        if results.hasErrors():
            break;

    # Now check if the analyser wants to be called when all lines are handled
    if meterDict.has_key("all_lines_evaluated"):
        resDicts = meterDict["all_lines_evaluated"]()

        if resDicts == False:
            # The analyser found that the data is invalid and should be deleted
            results.clearResults()
        else:
            HandleResults(results, meterDict, resDicts, None)

    print "NR OF RESULTS %d %d" % (results.getNrResults(), results.getNrResultsInFile())

    if not results.foundAnyResults():
        results.setError(None, ERROR_CODE_NO_HEADER_FOUND, "")

    if not results.hasErrors():
        # Sanity checks
        device = results.isModelMissing()
        if device:
            results.setError(device.getDeviceSerial(), ERROR_CODE_NO_MODEL_FOUND, "")

    if meterDict.has_key("eval_nr_results"):
        # Only use nr results if it is possible (not all meters report this)
        if not results.hasErrors():
            if results.getNrResults() <> results.getNrResultsInFile():
                results.setError(results.getFirstDevice().getDeviceSerial(), ERROR_CODE_NR_RESULTS, "")

    if meterDict.has_key("eval_checksum_file"):
        # Calculate checksum
        if not results.hasErrors():
            if not meterDict[ "eval_checksum_file" ](inList):
                results.setError(results.getFirstDevice().getDeviceSerial(), ERROR_CODE_CHECKSUM_FILE, "")

    if meterDict.has_key("eval_termination_record"):
        # Try to find termination record
        if not results.hasErrors():                
            if not results.isTerminationRecordFound():
                results.setError(results.getFirstDevice().getDeviceSerial(), ERROR_CODE_NR_RESULTS, "")

    return results.generateOutList()
