// -----------------------------------------------------------------------------
// Copyright (C) 2006 Aidera AB, Sweden, http://www.aidera.se
// Developed by Endian Technologies AB, Sweden, http://endian.se
// -----------------------------------------------------------------------------

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <memory.h>
#include <stdint.h>

uint16_t        crc16table[256];

// -----------------------------------------------------------------------------
// CRC stuff
// -----------------------------------------------------------------------------

static void crc16_inittable(void)
{
    uint16_t i, j, reg;
    
    // For all possible byte values...
    for (i=0; i<256; i++)
    {
        // Set at top of register
        reg = i<<8;
        
        // For all possible bit values...
        for (j=0; j<8; j++)
        {
            if (reg&0x8000)
                reg = (reg << 1) ^ 0x8005;
            else
                reg <<= 1;
        }
        
        crc16table[i] = reg;
    }
}

static uint16_t crc16_calculate(uint16_t *address, uint32_t size)
{
    uint16_t    data;
    uint16_t    crc     = 0;    

    // Calculate CRC
    crc16_inittable();
    while (size > 1)
    {
        data = *address++;
        // Calculate CRC with table values
        crc = crc16table[((crc >> 8) ^ (data >> 8))] ^ (crc << 8);
        crc = crc16table[((crc >> 8) ^ (data&0xff))] ^ (crc << 8);
        
        size -= 2;
    }
    
    return crc;
}

// -----------------------------------------------------------------------------
// Python stuff
// -----------------------------------------------------------------------------

static PyObject *calculate_crc(PyObject *self, PyObject *args)
{
    const char *src;
    int len;
    uint16_t crc;

    if(!PyArg_ParseTuple(args, "s#", &src,&len))
    {
        return NULL;
    }

    crc = crc16_calculate((uint16_t *)(src), len);
    
    return Py_BuildValue("i",crc);
}

static PyMethodDef CrcMethods[] = 
{
    { "calculate_crc", (PyCFunction)calculate_crc, METH_VARARGS, "crc." }, 
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initcrc(void)
{
    (void)Py_InitModule("crc", CrcMethods );
}
