# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2011 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

import hashlib
import base64
import re

# Encrypt a serial number (device) for use in France. The encrypted serial number
# will have the form : 
# 
# ESaaaaaaaaaaaaaaaaaaaaaa
#
# where 
#  a = URL safe base64 encoded md5 sum of the serial number. 
#
# Note on size : The serial number column in the database is a varchar(30). The digest
# from the md5 sum is 16 bytes long. A base64 version of this digest is 4 * (16 / 3) = 22
# characters long. This is then padded to 24 bytes (the original size is divided by 3 and
# '==' is added if the last group contained only 1 byte, and '=' if it contained 2 bytes). 
# Add the 'ES' and we have 2 + 24 = 26 bytes < 30 bytes.
#
def EncryptSerialNumber(serial):
    """
    Encrypt serial number. 
    """
    # Looks like some analysers returns serial number as an integer. 
    serial=str(serial)
    m = hashlib.md5()
    m.update(serial)
    encrypted_serial = "ES%s" % (base64.urlsafe_b64encode(m.digest()))
        
    print "encrypted serial %s -> %s" % (serial, encrypted_serial)
    
    obfuscated_serial = ObfuscateSerialNumber(serial)

    return encrypted_serial, obfuscated_serial

#
# Create an obfuscated serial number.
#
# 1             ->
# 12            -> 
# 123           -> 
# 1234          -> #234
# 12345         -> ##345
# 123456        -> ###456
#
# 123-4         -> 123-#
# 123-45        -> 123-##
# 123456-78     -> ###456-##    
# 
# 12-34         -> 
#
def ObfuscateSerialNumber(serial):
        
    serial = str(serial)    
    pattern = re.compile('[^-]')
    index = serial.rfind('-')

    obfuscated_string = ''

    # Use the last three digits. 
    if (len(serial) > 3) and (index < (len(serial) - 3)):
        obfuscated_string = pattern.sub('#', serial)[:-3] + serial[-3:]
    
    # Use the last three digits before the '-'
    elif index >= max(3, len(serial) - 3):
        obfuscated_string = pattern.sub('#', serial)[:index-3] + serial[index-3:index] + \
            pattern.sub('#', serial)[index:]

    # At least one '#' in the obfuscated serial.
    if obfuscated_string == serial:
        obfuscated_string = ''

    return obfuscated_string

if __name__ == '__main__':
    serials = ['', '1', '12', '123', '-', '-1', '-12', '-123', 'A-', 'AB-', 'ABC-', \
               'ABCD-', 'A-1', 'AB-1', 'ABC-1', 'ABCD-1', 'A-12', 'AB-12', 'ABC-12', \
               'ABCD-12', 'A-123', 'AB-123', 'ABC-123', 'ABCD-123', '23-37217-99', 'AE67885']

    for serial in serials:
        enc = EncryptSerialNumber(serial)
        print "%s    => %s" % (serial, enc)
              
