# -*- coding: utf8 -*-
# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2010 Aidera AB, Sweden, http://www.aidera.se
# Developed by Endian Technologies AB, Sweden, http://endian.se
# -----------------------------------------------------------------------------

# @DEVICE Intuity OnQ Pogo

import re
from Generic import *
from Defines import *
from datetime import datetime
from datetime import time
import re

# -----------------------------------------------------------------------------
# DEFINES 
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# LOCAL FUNCTIONS
# -----------------------------------------------------------------------------

def SplitData( inData ):
    """
    Splits incoming data into a list. The data is ASCII.
    """
   
    records = []

    # Split on <CR><LF> characters
    for record in inData.split("\r\n"):
        # Remove <STX> checksum and <ETX> 
        records.append(record[1:-5])
        
    return records
    
# -----------------------------------------------------------------------------
# REGULAR EXPRESSIONS FOR VALIDATING SINGLE LINES
# -----------------------------------------------------------------------------

def EvalSerialRecord( line ):
    """
    Evaluate an Intuity OnQ Pogo Serial record. Extracted elements: 
    serial
    """
    m = re.match( r'\?(.{12}) V(.{5})', line)

    return m

def EvalSetupRecord( line ):
    """
    Evaluate an Intuity OnQ Pogo Setup record. Extracted elements: 
    vacuum setting          (1)
    date/time format        (2)
    beeper setting          (3)
    high glucose threshold  (4)
    low glucose threshold   (5)
    """

    m = re.match( r'SV([\+\-])F(12|24)B([\+\-])H(\S{3})L(\S{3})', line)

    return m
        
def EvalHistoryRecord( line ):
    """
    Evaluate an Intuity OnQ Pogo Result Record. Extracted elements:
    record type (1)
    hours       (2)
    minutes     (3)
    month       (4)
    day         (5)
    year        (6)
    data        (7)
    """
   
    m = re.match( r'D([GECPI]) (\d{2})\:(\d{2}) (\d{2})\/(\d{2})\/(\d{2})\=(.*)', line)

    return m

def EvalGlucoseEvent( data ):
    """
    Evalute the data part of an Intuity OnQ Pogo Result Record. 
    Extracted elements:
    glucose value     (1)
    sample type       (2)
    meal flag         (3)
    before/after      (4)
    temp flag         (5)
    vacuum flag pre   (6)
    vacuum flag post  (7)
    arrival time      (8)
    """

    m = re.match( r'(\d{3}) ([BCF]) ([BLDN])([\+\- ]) ([\+\-]) ([Vv])([Vv]) \@(\d)', data)

    return m


# -----------------------------------------------------------------------------
# GENERIC METER FUNCTIONS
# -----------------------------------------------------------------------------

def EvalIntuitySerialRecord( line ):
    """
    Is this line a serial record? If so, return a dictionary with serial
    number.
    """
    res = {}
    m = EvalSerialRecord( line )

    if m:
        res[ ELEM_DEVICE_SERIAL ] = m.group(1).strip()
    return res
    
def EvalIntuitySetupRecord( line ):
    """
    Is this line a setup record? If so return a dictionary with settings.
    """
    res = {}
    m = EvalSetupRecord( line )

    if m:
        # Asume
        res[SETTING_VACUUM_ENABLED] = 0
        res[SETTING_TIME_FORMAT] = 0
        res[SETTING_SOUND_ENABLED] = 0
        res[SETTING_BG_HIGH_ALERT_ENABLE] = 0
        res[SETTING_BG_LOW_ALERT_ENABLE] = 0

        # Update
        if m.group(1) == '+':
            res[SETTING_VACUUM_ENABLED] = 1
        if m.group(2) == '24':
            res[SETTING_TIME_FORMAT] = 1
        if m.group(3) == '+':
            res[SETTING_SOUND_ENABLED] = 1
        if m.group(4) != 'off':
            res[SETTING_BG_HIGH_ALERT_ENABLE] = 1
            res[SETTING_BG_HIGH_ALERT_LIMIT_MGDL] = m.group(4)
        if m.group(5) != 'off':
            res[SETTING_BG_LOW_ALERT_ENABLE] = 1
            res[SETTING_BG_LOW_ALERT_LIMIT_MGDL] = m.group(5)

    return res        

def EvalIntuityDeviceModelRecord( line ):
    """
    The device model is fixed
    """
    res = { ELEM_DEVICE_MODEL:"Intuity OnQ Pogo" }
    return res

def EvalIntuityUnitRecord( line ):
    """
    The unit is fixed
    """
    res = { ELEM_DEVICE_UNIT:"mg/dl" }
    return res

def EvalIntuityResultRecord( line ):
    """
    Is this a result record? If so, return a dictonary with keys
    """
    res = {}

    m = EvalHistoryRecord(line)
    if m:
        res[ ELEM_TIMESTAMP ] = datetime(2000+int(m.group(6)), int(m.group(4)), int(m.group(5)), int(m.group(2)), int(m.group(3)), 0)        
        res[ ELEM_VAL_TYPE ] = VALUE_TYPE_GLUCOSE
        if m.group(1) == "G":
            n = EvalGlucoseEvent(m.group(7))
            if n:
                flags = []
                res[ ELEM_FLAG_LIST ] = flags
                res[ ELEM_VAL ] = int(n.group(1))
                if n.group(2) != "B":
                    flags.append(FLAG_RESULT_CTRL)
                if n.group(3) != "N":
                    if (n.group(3) == "B") and (n.group(4) == "-"):
                        flags.append(FLAG_BEFORE_BREAKFAST)
                    if (n.group(3) == "B") and (n.group(4) == "+"):
                        flags.append(FLAG_AFTER_BREAKFAST)
                    if (n.group(3) == "L") and (n.group(4) == "-"):
                        flags.append(FLAG_BEFORE_LUNCH)
                    if (n.group(3) == "L") and (n.group(4) == "+"):
                        flags.append(FLAG_AFTER_LUNCH)
                    if (n.group(3) == "D") and (n.group(4) == "-"):
                        flags.append(FLAG_BEFORE_DINNER)
                    if (n.group(3) == "D") and (n.group(4) == "+"):
                        flags.append(FLAG_AFTER_DINNER)
                if n.group(5) == "-":
                    flags.append(FLAG_RESULT_OUTSIDE_TEMP)
                if n.group(6) == "v":
                    # Pre-vaccum was not good - Should be ignored according to Aidera
                    pass
                if n.group(7) == "v":
                    # Post-vacuum was not good - Should be ignored according to Aidera
                    pass
                arrival_time = int(n.group(8)) # blood arrival time - currently not used
                    
        elif (m.group(1) == "E") or (m.group(1) == "I"):
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_ALARM
            res[ ELEM_VAL ] = ALARM_BAD_HISTORY_DATA
        elif m.group(1) == "C":
            #We don't save the actual data from this record, just the event itself
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
            res[ ELEM_VAL ] = DIASEND_EVENT_DATETIME_CHANGE
        elif m.group(1) == "P":
            res[ ELEM_VAL_TYPE ] = VALUE_TYPE_EVENT
            res[ ELEM_VAL ] = DIASEND_EVENT_POWER_ON
        else:
            # Should not occur
            return {}

    else:
        m = EvalIntuitySetupRecord(line)
    
        if m:
            res[ SETTINGS_LIST ] = m    

    return res
    

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectIntuityOnQPogo( inList ):
    """
    Detect if data comes from an Intuity OnQ Pogo. 
    """
    return DetectDevice( "IntuityOnQPogo", inList, DEVICE_METER )

def AnalyseIntuityOnQPogo( inData ):
    """
    Analyse Intuity OnQ Pogo meter
    """
    inList = SplitData (inData)

    # Checksum for individual frames is not verified since the Transmitter does that.
    # There are checksums on the blocks transfered from the Transmitter as well as in
    # TCP/IP.
    
    # Eval dict
    d = { \
        "eval_serial_record" : EvalIntuitySerialRecord, \
        "eval_device_model_record" : EvalIntuityDeviceModelRecord, \
        "eval_unit" : EvalIntuityUnitRecord, \
        "eval_result_record" : EvalIntuityResultRecord
    }

    resList = AnalyseGenericMeter( inList, d );

    return resList


if __name__ == "__main__":
     data = '\x02?RD-002       V0.720B28A\x03\r\n\x02SV+F12B+HoffL050EE1D\x03\r\n\x02U2062222040          4AC7\x03\r\n\x02T01:11 09/07/103B5A\x03\r\n\x02n6001C2E\x03\r\n\x02DG 12:04 02/16/05=076 B D+ + VV @09DA0\x03\r\n\x02DG 12:05 02/16/05=132 B D- + VV @03EE7\x03\r\n\x02DG 12:06 02/18/05=131 B D+ + VV @0EF07\x03\r\n\x02DG 12:07 02/19/05=130 B D- + VV @050D6\x03\r\n\x02DG 08:30 10/01/05=100 B L+ + VV @08BC0\x03\r\n\x02DG 11:59 10/01/05=138 B L+ + Vv @0BD8C\x03\r\n\x02DG 00:00 10/02/05=168 B L+ + VV @0D040\x03\r\n\x02DG 11:59 10/03/05=096 B L- + VV @364BB\x03\r\n\x02DG 12:00 10/06/05=050 B N  + VV @0C1AC\x03\r\n\x02DG 23:59 10/09/05=020 B N  + VV @05FDF\x03\r\n\x02DG 05:48 10/10/05=019 B N  + VV @0E76D\x03\r\n\x02DG 07:49 10/12/05=501 C N  - VV @046B8\x03\r\n\x02DG 09:50 10/12/05=501 B B+ - VV @06002\x03\r\n\x02DG 10:51 10/12/05=500 B B- + VV @05A3C\x03\r\n\x02DG 11:52 10/13/05=138 B B+ - VV @058D6\x03\r\n\x02DG 13:53 10/13/05=105 B B- + VV @00DE4\x03\r\n\x02DG 10:51 10/14/05=500 B B- + VV @0E954\x03\r\n\x02DG 17:52 10/14/05=138 B B+ - VV @01EF8\x03\r\n\x02DG 11:54 10/15/05=200 B D+ - VV @04BF1\x03\r\n\x02DG 05:05 10/15/05=133 C N  + VV @03547\x03\r\n\x02DG 12:04 10/16/05=076 B L+ + VV @0467A\x03\r\n\x02DG 12:05 10/16/05=132 B L- + VV @0E53D\x03\r\n\x02DG 12:06 10/18/05=131 B L+ + VV @56370\x03\r\n\x02DG 12:07 10/19/05=130 B L- + VV @08B0C\x03\r\n\x02DG 12:07 10/19/05=130 B L- + VV @08B0C\x03\r\n\x02DG 08:30 12/01/05=100 B B+ + VV @0BCB3\x03\r\n\x02DG 11:59 12/01/05=138 B L+ + VV @08457\x03\r\n\x02DG 00:00 12/02/05=168 B D+ + VV @00FE1\x03\r\n\x02DG 11:59 12/03/05=096 B B- + VV @06153\x03\r\n\x02DG 12:00 12/06/05=050 B L+ + VV @1972E\x03\r\n\x02DG 23:59 12/09/05=020 B D- + VV @0D5E2\x03\r\n\x02DG 05:48 12/10/05=019 B D+ + VV @0C94F\x03\r\n\x02DG 07:49 12/12/05=501 C N  - VV @0F030\x03\r\n\x02DG 09:50 12/12/05=501 B D+ - VV @03E58\x03\r\n\x02DG 10:51 12/12/05=500 B D- + VV @00466\x03\r\n\x02DG 11:52 12/13/05=138 B L+ - VV @06FA5\x03\r\n\x02DG 13:53 12/13/05=105 B L- + VV @03A97\x03\r\n\x02DG 10:51 12/14/05=500 B L- + VV @0DE27\x03\r\n\x02DG 17:52 12/14/05=138 B L+ - VV @0298B\x03\r\n\x02DG 11:54 12/15/05=200 B B+ - VV @015AB\x03\r\n\x02DG 05:05 12/15/05=133 C N  + VV @083CF\x03\r\n\x02DG 12:04 12/16/05=076 B B+ + VV @07109\x03\r\n\x02DG 12:05 12/16/05=132 B B- + VV @0D24E\x03\r\n\x02DG 12:06 12/18/05=131 B B+ + VV @003AE\x03\r\n\x02DG 12:07 12/19/05=130 B L- + VV @03D84\x03\r\n\x02DG 11:59 01/01/06=138 B L+ + VV @06CD8\x03\r\n\x02DG 00:00 01/02/06=168 B L+ + VV @08E47\x03\r\n\x02DG 11:59 01/03/06=096 B L- + VV @00827\x03\r\n\x02DG 12:00 01/06/06=050 B L+ + VV @06E28\x03\r\n\x02DG 23:59 01/09/06=020 B D- + VV @03D6D\x03\r\n\x02DG 05:48 01/10/06=019 B D+ + vV @04151\x03\r\n\x02DG 07:49 01/12/06=501 C N  - VV @018BF\x03\r\n\x02DG 09:50 01/12/06=501 B D+ - VV @0D6D7\x03\r\n\x02DG 10:51 01/12/06=500 B D- + VV @0ECE9\x03\r\n\x02DG 11:52 01/13/06=138 B D+ - VV @0EE03\x03\r\n\x02DG 13:53 01/13/06=105 B D- + VV @0BB31\x03\r\n\x02DG 10:51 01/14/06=500 B B- + VV @0B753\x03\r\n\x02DG 17:52 01/14/06=138 B B+ - VV @040FF\x03\r\n\x02DG 11:54 01/15/06=200 B B+ - VV @0FD24\x03\r\n\x02DG 05:05 01/15/06=133 C N  + VV @06B40\x03\r\n\x02DG 12:04 01/16/06=076 B B+ + VV @09986\x03\r\n\x02DG 12:05 01/16/06=132 B B- + VV @03AC1\x03\r\n\x02DG 12:06 01/18/06=131 B B+ + VV @0EB21\x03\r\n\x02DG 12:07 01/19/06=130 B B- + VV @054F0\x03\r\n\x02DG 08:30 02/01/06=100 B B+ + VV @0B9F0\x03\r\n\x02DG 11:59 02/01/06=138 B N  + VV @07097\x03\r\n\x02DG 00:00 02/02/06=168 B N  + VV @09208\x03\r\n\x02DG 11:59 02/03/06=096 B N  + VV @0B077\x03\r\n\x02DG 12:00 02/06/06=050 B N  + Vv @0FD34\x03\r\n\x02DG 23:59 02/09/06=020 B B- + VV @03873\x03\r\n\x02DG 05:48 02/10/06=019 B B+ + VV @024DE\x03\r\n\x02DG 07:49 02/12/06=501 C N  - VV @0F573\x03\r\n\x02DG 09:50 02/12/06=501 B B+ - VV @0D3C9\x03\r\n\x02DG 10:51 02/12/06=500 B B- + VV @0E9F7\x03\r\n\x02DG 11:52 02/13/06=138 B L+ - VV @06AE6\x03\r\n\x02DG 13:53 02/13/06=105 B L- + VV @03FD4\x03\r\n\x02DG 10:51 02/14/06=500 B L- + VV @0DB64\x03\r\n\x02DG 17:52 02/14/06=138 B D+ - VV @045E1\x03\r\n\x02DG 11:54 02/15/06=200 B D+ - VV @0F83A\x03\r\n\x02DG 05:05 02/15/06=133 C N  + VV @7F233\x03\r\n\x02DG 12:04 02/16/06=076 B D+ + VV @09C98\x03\r\n\x02DG 12:05 02/16/06=132 B D- + VV @03FDF\x03\r\n\x02DG 12:06 02/18/06=131 B D+ + VV @0EE3F\x03\r\n\x02DG 12:07 02/19/06=130 B D- + VV @051EE\x03\r\n\x02DG 08:30 03/01/06=100 B B+ + VV @0E2B4\x03\r\n\x02DG 11:59 03/01/06=138 B N  + VV @02BD3\x03\r\n\x02DG 00:00 03/02/06=168 B N  + VV @0C94C\x03\r\n\x02DG 11:59 03/03/06=096 B N  + VV @0EB33\x03\r\n\x02DG 12:00 03/06/06=050 B N  + VV @02923\x03\r\n\x02DG 23:59 03/09/06=020 B B- + VV @06337\x03\r\n\x02DG 05:48 03/10/06=019 B B+ + VV @07F9A\x03\r\n\x02DG 07:49 03/12/06=501 C N  - VV @0AE37\x03\r\n\x02DG 09:50 03/12/06=501 B B+ - VV @0888D\x03\r\n\x02DG 10:51 03/12/06=500 B B- + VV @0B2B3\x03\r\n\x02DG 11:52 03/13/06=138 B L+ - VV @031A2\x03\r\n\x02DG 13:53 03/13/06=105 B L- + VV @06490\x03\r\n\x02DG 10:51 03/14/06=500 B L- + VV @08020\x03\r\n\x02DG 17:52 03/14/06=138 B D+ - VV @32C3E\x03\r\n\x02DG 11:54 03/15/06=200 B D+ - VV @0A37E\x03\r\n\x02DG 05:05 03/15/06=133 C N  + VV @0DDC8\x03\r\n\x02DG 12:04 03/16/06=076 B D+ + VV @0C7DC\x03\r\n\x02DG 12:05 03/16/06=132 B D- + VV @0649B\x03\r\n\x02DG 12:06 03/18/06=131 B D+ + VV @0B57B\x03\r\n\x02DG 12:07 03/19/06=130 B D- + VV @00AAA\x03\r\n\x02DG 11:59 04/01/06=138 B L+ + VV @0529D\x03\r\n\x02DG 00:00 04/02/06=168 B L+ + VV @0B002\x03\r\n\x02DG 11:59 04/03/06=096 B L- + VV @03662\x03\r\n\x02DG 12:00 04/06/06=050 B N  + Vv @02EBD\x03\r\n\x02DG 23:59 04/09/06=020 B N  + Vv @0B0CE\x03\r\n\x02DG 05:48 04/10/06=019 B N  + VV @0872F\x03\r\n\x02DG 07:49 04/12/06=501 C N  - VV @026FA\x03\r\n\x02DG 09:50 04/12/06=501 B B+ - VV @00040\x03\r\n\x02DG 10:51 04/12/06=500 B B- + VV @03A7E\x03\r\n\x02DG 11:52 04/13/06=138 B B+ - VV @03894\x03\r\n\x02DG 13:53 04/13/06=105 B B- + VV @06DA6\x03\r\n\x02DG 10:51 04/14/06=500 B B- + VV @08916\x03\r\n\x02DG 17:52 04/14/06=138 B B+ - VV @07EBA\x03\r\n\x02DG 11:54 04/15/06=200 B D+ - VV @02BB3\x03\r\n\x02DG 05:05 04/15/06=133 C N  + VV @05505\x03\r\n\x02DG 12:04 04/16/06=076 B L+ + VV @02638\x03\r\n\x02DG 12:05 04/16/06=132 B L- + VV @0857F\x03\r\n\x02DG 12:06 04/18/06=131 B L+ + VV @0549F\x03\r\n\x02DG 12:07 04/19/06=130 B L- + VV @0EB4E\x03\r\n\x02DG 12:07 04/19/06=130 B L- + VV @0EB4E\x03\r\n\x02DG 11:59 05/01/06=138 B L+ + VV @009D9\x03\r\n\x02DG 00:00 05/02/06=168 B L+ + vV @6EEE1\x03\r\n\x02DG 11:59 05/03/06=096 B L- + VV @06D26\x03\r\n\x02DG 12:00 05/06/06=050 B N  + VV @0FAAA\x03\r\n\x02DG 23:59 05/09/06=020 B N  + VV @064D9\x03\r\n\x02DG 05:48 05/10/06=019 B N  + VV @0DC6B\x03\r\n\x02DG 07:49 05/12/06=501 C N  - VV @07DBE\x03\r\n\x02DG 09:50 05/12/06=501 B B+ - VV @27816\x03\r\n\x02DG 10:51 05/12/06=500 B B- + VV @0613A\x03\r\n\x02DG 11:52 05/13/06=138 B B+ - VV @063D0\x03\r\n\x02DG 13:53 05/13/06=105 B B- + VV @036E2\x03\r\n\x02DG 10:51 05/14/06=500 B B- + VV @0D252\x03\r\n\x02DG 17:52 05/14/06=138 B B+ - VV @025FE\x03\r\n\x02DG 11:54 05/15/06=200 B D+ - VV @070F7\x03\r\n\x02DG 05:05 05/15/06=133 C N  + VV @00E41\x03\r\n\x02DG 12:04 05/16/06=076 B L+ + VV @07D7C\x03\r\n\x02DG 12:05 05/16/06=132 B L- + VV @0DE3B\x03\r\n\x02DG 12:06 05/18/06=131 B L+ + VV @00FDB\x03\r\n\x02DG 12:07 05/19/06=130 B L- + VV @0B00A\x03\r\n\x02DG 12:07 05/19/06=130 B L- + VV @0B00A\x03\r\n\x02DG 08:30 06/01/06=100 B B+ + VV @3EE6A\x03\r\n\x02DG 11:59 06/01/06=138 B N  + VV @01596\x03\r\n\x02DG 00:00 06/02/06=168 B N  + vv @45EEF\x03\r\n\x02DG 11:59 06/03/06=096 B N  + VV @0D576\x03\r\n\x02DG 12:00 06/06/06=050 B N  + VV @763D9\x03\r\n\x02DG 23:59 06/09/06=020 B B- + VV @05D72\x03\r\n\x02DG 05:48 06/10/06=019 B B+ + VV @041DF\x03\r\n\x02DG 07:49 06/12/06=501 F N  - VV @01051\x03\r\n\x02DG 09:50 06/12/06=501 B B+ - VV @0B6C8\x03\r\n\x02DG 10:51 06/12/06=500 B B- + VV @08CF6\x03\r\n\x02DG 11:52 06/13/06=138 B L+ - VV @00FE7\x03\r\n\x02DG 13:53 06/13/06=105 B L- + VV @05AD5\x03\r\n\x02DG 10:51 06/14/06=500 B L- + VV @0BE65\x03\r\n\x02DG 17:52 06/14/06=138 B D+ - VV @020E0\x03\r\n\x02DG 11:54 06/15/06=200 F N  - VV @000E7\x03\r\n\x02DG 05:05 06/15/06=133 C N  + VV @0E38D\x03\r\n\x02DG 12:04 06/16/06=076 B D+ + VV @0F999\x03\r\n\x02DG 12:05 06/16/06=132 B D- + VV @279CC\x03\r\n\x02DG 12:06 06/18/06=131 B D+ + VV @08B3E\x03\r\n\x02DG 12:07 06/19/06=130 F N  + VV @00D2C\x03\r\n\x02DG 08:30 10/01/06=100 B L+ + VV @08AF8\x03\r\n\x02DG 11:59 10/01/06=138 B L+ + VV @033E7\x03\r\n\x02DG 00:00 10/02/06=168 B L+ + VV @0D178\x03\r\n\x02DG 11:59 10/03/06=096 B L- + VV @05718\x03\r\n\x02DG 12:00 10/06/06=050 B N  + VV @0C094\x03\r\n\x02DG 23:59 10/09/06=020 B N  + VV @05EE7\x03\r\n\x02DG 05:48 10/10/06=019 B N  + VV @0E655\x03\r\n\x02DG 07:49 10/12/06=501 F N  - VV @0C7A3\x03\r\n\x02DG 09:50 10/12/06=501 B B+ - VV @0613A\x03\r\n\x02DG 10:51 10/12/06=500 B B- + VV @05B04\x03\r\n\x02DG 11:52 10/13/06=138 B B+ - VV @059EE\x03\r\n\x02DG 13:53 10/13/06=105 B B- + VV @00CDC\x03\r\n\x02DG 10:51 10/14/06=500 B B- + VV @0E86C\x03\r\n\x02DG 17:52 10/14/06=138 B B+ - VV @01FC0\x03\r\n\x02DG 11:54 10/15/06=200 B D+ - VV @04AC9\x03\r\n\x02DG 05:05 10/15/06=133 C N  + VV @0347F\x03\r\n\x02DG 12:04 10/16/06=076 B L+ + VV @26450\x03\r\n\x02DG 12:05 10/16/06=132 B L- + VV @0E405\x03\r\n\x02DG 12:06 10/18/06=131 B L+ + VV @035E5\x03\r\n\x02DG 12:07 10/19/06=130 B L- + VV @08A34\x03\r\n\x02DG 12:07 10/19/06=130 B L- + VV @08A34\x03\r\n\x02DG 08:30 12/01/07=100 F N  + VV @03062\x03\r\n\x02DG 11:59 12/01/07=138 B L+ + VV @07D88\x03\r\n\x02DG 00:00 12/02/07=168 B D+ + VV @0F63E\x03\r\n\x02DG 11:59 12/03/07=096 B B- + VV @0988C\x03\r\n\x02DG 12:00 12/06/07=050 B L+ + VV @07F78\x03\r\n\x02DG 23:59 12/09/07=020 B D- + VV @02C3D\x03\r\n\x02DG 05:48 12/10/07=019 B D+ + VV @03090\x03\r\n\x02DG 07:49 12/12/07=501 F N  - VV @089CC\x03\r\n\x02DG 09:50 12/12/07=501 B D+ - VV @0C787\x03\r\n\x02DG 10:51 12/12/07=500 B D- + VV @0FDB9\x03\r\n\x02DG 11:52 12/13/07=138 B L+ - VV @0967A\x03\r\n\x02DG 13:53 12/13/07=105 B L- + VV @0C348\x03\r\n\x02DG 10:51 12/14/07=500 B L- + VV @027F8\x03\r\n\x02DG 17:52 12/14/07=138 B L+ - VV @0D054\x03\r\n\x02DG 11:54 12/15/07=200 B B+ - VV @3DEEF\x03\r\n\x02DG 05:05 12/15/07=133 C N  + Vv @0F543\x03\r\n\x02DG 12:04 12/16/07=076 B B+ + VV @088D6\x03\r\n\x02DG 12:05 12/16/07=132 B B- + VV @02B91\x03\r\n\x02DG 12:06 12/18/07=131 B B+ + VV @0FA71\x03\r\n\x02DG 12:07 12/19/07=130 B L- + VV @0C45B\x03\r\n\x02DG 08:30 01/01/08=100 B D+ + VV @041D1\x03\r\n\x02DG 11:59 01/01/08=138 B B+ + VV @0101C\x03\r\n\x02DG 00:00 01/02/08=168 B L+ + VV @07378\x03\r\n\x02DG 11:59 01/03/08=096 B D- + VV @09C31\x03\r\n\x02DG 12:00 01/06/08=050 B B+ + VV @012EC\x03\r\n\x02DG 23:59 01/09/08=020 B B- + VV @02880\x03\r\n\x02DG 05:48 01/10/08=019 B B+ + VV @0342D\x03\r\n\x02DG 07:49 01/12/08=501 F N  - VV @065A3\x03\r\n\x02DG 09:50 01/12/08=501 B N  - VV @0B342\x03\r\n\x02DG 10:51 01/12/08=500 B N  + vV @04DF2\x03\r\n\x02DG 11:52 01/13/08=138 B N  - VV @08B96\x03\r\n\x02DG 13:53 01/13/08=105 B N  + VV @07ABB\x03\r\n\x02DG 10:51 01/14/08=500 B N  + VV @09E0B\x03\r\n\x02DG 17:52 01/14/08=138 B D+ - vV @03583\x03\r\n\x02DG 11:54 01/15/08=200 B D+ - VV @0E8C9\x03\r\n\x02DG 05:05 01/15/08=103 C N  + Vv @3F3C9\x03\r\n\x02DG 12:04 01/16/08=076 B D+ + VV @08C6B\x03\r\n\x02DG 12:05 01/16/08=132 B D- + VV @02F2C\x03\r\n\x02DG 12:06 01/18/08=131 B D+ + VV @0FECC\x03\r\n\x02DG 12:07 01/19/08=130 B D- + VV @0411D\x03\r\n\x02DG 08:30 02/01/08=100 B D+ + VV @0AC1D\x03\r\n\x02DG 11:59 02/01/08=138 B L+ + VV @07C2B\x03\r\n\x02DG 00:00 02/02/08=168 B L+ + VV @09EB4\x03\r\n\x02DG 11:59 02/03/08=096 B L- + VV @018D4\x03\r\n\x02DG 12:00 02/06/08=050 B L+ + VV @07EDB\x03\r\n\x02DG 23:59 02/09/08=020 B L- + VV @044B7\x03\r\n\x02DG 05:48 02/10/08=019 B L+ + VV @0581A\x03\r\n\x02DG 07:49 02/12/08=501 C N  - VV @0084C\x03\r\n\x02DG 09:50 02/12/08=501 B L+ - Vv @0205E\x03\r\n\x02DG 10:51 02/12/08=500 B B- + VV @014C8\x03\r\n\x02DG 11:52 02/13/08=138 B B+ - VV @01622\x03\r\n\x02DG 13:53 02/13/08=105 B B- + VV @04310\x03\r\n\x02DG 10:51 02/14/08=500 B B- + VV @0A7A0\x03\r\n\x02DG 17:52 02/14/08=138 B B+ - VV @0500C\x03\r\n\x02DG 11:54 02/15/08=200 B B+ - VV @0EDD7\x03\r\n\x02DG 05:05 02/15/08=133 C N  + VV @07BB3\x03\r\n\x02DG 12:04 02/16/08=076 B B+ + VV @08975\x03\r\n\x02DG 12:05 02/16/08=132 B B- + VV @02A32\x03\r\n\x02DG 12:06 02/18/08=131 B N  + VV @08BAA\x03\r\n\x02DG 12:07 02/19/08=130 B N  + VV @09064\x03\r\n\x02DG 08:30 03/01/08=100 B N  + VV @06FF3\x03\r\n\x02DG 11:59 03/01/08=138 B N  + VV @0D6EC\x03\r\n\x02DG 00:00 03/02/08=168 B D+ + VV @0ACD9\x03\r\n\x02DG 11:59 03/03/08=096 B D- + VV @02AB9\x03\r\n\x02DG 12:00 03/06/08=050 B D+ + VV @04CB6\x03\r\n\x02DG 23:59 03/09/08=020 B D- + VV @076DA\x03\r\n\x02DG 05:48 03/10/08=019 B B+ + Vv @77949\x03\r\n\x02DG 07:49 03/12/08=501 C N  - VV @05308\x03\r\n\x02DG 09:50 03/12/08=501 B B+ - VV @075B2\x03\r\n\x02DG 10:51 03/12/08=500 B B- + VV @04F8C\x03\r\n\x02DG 11:52 03/13/08=138 B L+ - VV @0CC9D\x03\r\n\x02DG 13:53 03/13/08=105 B L- + VV @099AF\x03\r\n\x02DG 10:51 03/14/08=500 B L- + Vv @0F24C\x03\r\n\x02DG 17:52 03/14/08=138 B L+ - VV @08AB3\x03\r\n\x02DG 11:54 03/15/08=200 B D+ - VV @05E41\x03\r\n\x02DG 05:05 03/15/08=133 C N  + VV @020F7\x03\r\n\x02DG 12:04 03/16/08=076 B D+ + VV @03AE3\x03\r\n\x02DG 12:05 03/16/08=132 B D- + VV @099A4\x03\r\n\x02DG 12:06 03/18/08=131 B D+ + VV @04844\x03\r\n\x02DG 12:07 03/19/08=130 B D- + VV @0F795\x03\r\n\x02DG 08:30 04/01/08=100 B D+ + VV @07F94\x03\r\n\x02DG 11:59 04/01/08=138 B D+ + VV @0C68B\x03\r\n\x02DG 00:00 04/02/08=168 B D+ + VV @02414\x03\r\n\x02DG 11:59 04/03/08=096 B B- + VV @04AA6\x03\r\n\x02DG 12:00 04/06/08=050 B B+ + VV @02CA9\x03\r\n\x02DG 23:59 04/09/08=020 B B- + VV @016C5\x03\r\n\x02DG 05:48 04/10/08=019 B B+ + VV @00A68\x03\r\n\x02DG 07:49 04/12/08=501 C N  - VV @0DBC5\x03\r\n\x02DG 09:50 04/12/08=501 B B+ - VV @0FD7F\x03\r\n\x02DG 10:51 04/12/08=500 B D- + VV @02F93\x03\r\n\x02DG 11:52 04/13/08=138 B D+ - VV @02D79\x03\r\n\x02DG 13:53 04/13/08=105 B D- + VV @43E6F\x03\r\n\x02DG 10:51 04/14/08=500 B D- + VV @09CFB\x03\r\n\x02DG 17:52 04/14/08=138 B D+ - VV @06B57\x03\r\n\x02DG 11:54 04/15/08=200 B D+ - VV @0D68C\x03\r\n\x02DG 05:05 04/15/08=133 C N  + VV @0A83A\x03\r\n\x02DG 12:04 04/16/08=076 B N  + VV @02A84\x03\r\n\x02DG 12:05 04/16/08=132 B N  + VV @02DDC\x03\r\n\x02DG 12:06 04/18/08=131 B N  + VV @05823\x03\r\n\x02DG 12:07 04/19/08=130 B N  + VV @043ED\x03\r\n\x02DG 08:30 05/01/08=100 B N  + VV @0BC7A\x03\r\n\x02DG 11:59 05/01/08=138 B L+ + VV @0F4E6\x03\r\n\x02DG 00:00 05/02/08=168 B L+ + VV @01679\x03\r\n\x02DG 11:59 05/03/08=096 F N  + VV @0C0F3\x03\r\n\x02DG 12:00 05/06/08=050 B L+ + VV @0F616\x03\r\n\x02DG 23:59 05/09/08=020 B L- + VV @0CC7A\x03\r\n\x02DG 05:48 05/10/08=019 B L+ + VV @0D0D7\x03\r\n\x02DG 07:49 05/12/08=501 C N  - VV @19108\x03\r\n\x02DG 09:50 05/12/08=501 B B+ - VV @0A63B\x03\r\n\x02DG 10:51 05/12/08=500 B B- + VV @09C05\x03\r\n\x02DG 11:52 05/13/08=138 B B+ - VV @5C942\x03\r\n\x02DG 13:53 05/13/08=105 B B- + VV @0CBDD\x03\r\n\x02DG 10:51 05/14/08=500 B B- + VV @02F6D\x03\r\n\x02DG 17:52 05/14/08=138 B B+ - VV @0D8C1\x03\r\n\x02DG 11:54 05/15/08=200 B B+ - VV @0651A\x03\r\n\x02DG 05:05 05/15/08=133 C N  + VV @0F37E\x03\r\n\x02DG 12:04 05/16/08=076 B D+ + VV @1F8E3\x03\r\n\x02DG 12:05 05/16/08=132 B D- + VV @04A2D\x03\r\n\x02DG 12:06 05/18/08=131 B D+ + VV @09BCD\x03\r\n\x02DG 12:07 05/19/08=130 B D- + VV @0241C\x03\r\n\x02DG 08:30 06/01/08=100 B L+ + VV @0A035\x03\r\n\x02DG 11:59 06/01/08=138 B B+ + VV @098D1\x03\r\n\x02DG 00:00 06/02/08=168 B L+ + VV @0FBB5\x03\r\n\x02DG 11:59 06/03/08=096 B D- + VV @014FC\x03\r\n\x02DG 12:00 06/06/08=050 B N  + VV @0EA59\x03\r\n\x02DG 23:59 06/09/08=020 B B- + VV @0A04D\x03\r\n\x02DG 05:48 06/10/08=019 B L+ + VV @03D1B\x03\r\n\x02DG 07:49 06/12/08=501 C N  - VV @06D4D\x03\r\n\x02DG 09:50 06/12/08=501 B N  - VV @03B8F\x03\r\n\x02DG 10:51 06/12/08=500 B L- + VV @0F032\x03\r\n\x02DG 11:52 06/13/08=138 B B+ - VV @07323\x03\r\n\x02DG 13:53 06/13/08=105 B D- + VV @0CEC3\x03\r\n\x02DG 10:51 06/14/08=500 B L- + VV @0435A\x03\r\n\x02DG 17:52 06/14/08=138 B L+ - VV @0B4F6\x03\r\n\x02DG 11:54 06/15/08=200 B L+ - VV @0092D\x03\r\n\x02DG 05:05 06/15/08=133 C N  + VV @01EB2\x03\r\n\x02DG 12:04 06/16/08=076 B D+ + VV @004A6\x03\r\n\x02DG 12:05 06/16/08=132 B D- + VV @0A7E1\x03\r\n\x02DG 12:06 06/18/08=131 B N  + VV @0EEAB\x03\r\n\x02DG 12:07 06/19/08=130 B N  + VV @0F565\x03\r\n\x02DG 08:30 07/01/08=100 B N  + VV @00AF2\x03\r\n\x02DG 11:59 07/01/08=138 B B+ + VV @0C395\x03\r\n\x02DG 00:00 07/02/08=168 B B+ + VV @0210A\x03\r\n\x02DG 11:59 07/03/08=096 B B- + VV @0A76A\x03\r\n\x02DG 12:00 07/06/08=050 B B+ + VV @0C165\x03\r\n\x02DG 23:59 07/09/08=020 B B- + VV @0FB09\x03\r\n\x02DG 05:48 07/10/08=019 B B+ + VV @0E7A4\x03\r\n\x02DG 07:49 07/12/08=501 C N  - VV @03609\x03\r\n\x02DG 09:50 07/12/08=501 B D+ - VV @0F861\x03\r\n\x02DG 10:51 07/12/08=500 B D- + VV @0C25F\x03\r\n\x02DG 11:52 07/13/08=138 B D+ - VV @0C0B5\x03\r\n\x02DG 13:53 07/13/08=105 B D- + VV @09587\x03\r\n\x02DG 10:51 07/14/08=500 B L- + VV @0181E\x03\r\n\x02DG 17:52 07/14/08=138 B L+ - VV @0EFB2\x03\r\n\x02DG 11:54 07/15/08=200 B L+ - VV @05269\x03\r\n\x02DG 05:05 07/15/08=133 C N  + VV @045F6\x03\r\n\x02DG 12:04 07/16/08=076 B D+ + VV @05FE2\x03\r\n\x02DG 12:05 07/16/08=132 B D- + VV @0FCA5\x03\r\n\x02DG 12:06 07/18/08=131 B L+ + VV @0446C\x03\r\n\x02DG 12:07 07/19/08=130 B L- + VV @0FBBD\x03\r\n\x02DG 08:30 08/01/08=100 B L+ + VV @0B9BE\x03\r\n\x02DG 11:59 08/01/08=138 B B+ + VV @5D6F7\x03\r\n\x02DG 00:00 08/02/08=168 B B+ + VV @063C5\x03\r\n\x02DG 11:59 08/03/08=096 B B- + VV @0E5A5\x03\r\n\x02DG 12:00 08/06/08=050 B B+ + VV @083AA\x03\r\n\x02DG 23:59 08/09/08=020 B B- + VV @0B9C6\x03\r\n\x02DG 05:48 08/10/08=019 B B+ + VV @0A56B\x03\r\n\x02DG 07:49 08/12/08=501 C N  - VV @074C6\x03\r\n\x02DG 09:50 08/12/08=501 B N  - VV @02204\x03\r\n\x02DG 10:51 08/12/08=500 B D- + VV @08090\x03\r\n\x02DG 11:52 08/13/08=138 B D+ - VV @0827A\x03\r\n\x02DG 13:53 08/13/08=105 B D- + VV @0D748\x03\r\n\x02DG 10:51 08/14/08=500 B D- + VV @033F8\x03\r\n\x02DG 17:52 08/14/08=138 B D+ - VV @0C454\x03\r\n\x02DG 11:54 08/15/08=200 B L+ - VV @010A6\x03\r\n\x02DG 05:05 08/15/08=133 C N  + VV @00739\x03\r\n\x02DG 12:04 08/16/08=076 B L+ + VV @07404\x03\r\n\x02DG 12:05 08/16/08=132 B L- + VV @0D743\x03\r\n\x02DG 12:06 08/18/08=131 B L+ + VV @006A3\x03\r\n\x02DG 12:07 08/19/08=130 B L- + VV @0B972\x03\r\n\x02DG 08:30 10/01/08=100 B L+ + VV @077C7\x03\r\n\x02DG 11:59 10/01/08=138 B L+ + VV @0CED8\x03\r\n\x02DG 00:00 10/02/08=168 B B+ + VV @4EB98\x03\r\n\x02DG 11:59 10/03/08=096 B B- + VV @02BDC\x03\r\n\x02DG 12:00 10/06/08=050 B B+ + VV @04DD3\x03\r\n\x02DG 23:59 10/09/08=020 B B- + VV @077BF\x03\r\n\x02DG 05:48 10/10/08=019 B B+ + VV @06B12\x03\r\n\x02DG 07:49 10/12/08=501 C N  - VV @0BABF\x03\r\n\x02DG 09:50 10/12/08=501 B N  - VV @0EC7D\x03\r\n\x02DG 10:51 10/12/08=500 B N  + VV @0725C\x03\r\n\x02DG 11:52 10/13/08=138 B N  - VV @0D4A9\x03\r\n\x02DG 13:53 10/13/08=105 B N  + VV @02584\x03\r\n\x02DG 10:51 10/14/08=500 B N  + VV @0C134\x03\r\n\x02DG 17:52 10/14/08=138 B N  - VV @09287\x03\r\n\x02DG 11:54 10/15/08=200 B N  - VV @02F5C\x03\r\n\x02DG 05:05 10/15/08=133 C N  + VV @0C940\x03\r\n\x02DG 12:04 10/16/08=076 B D+ + VV @0D354\x03\r\n\x02DG 12:05 10/16/08=132 B D- + VV @07013\x03\r\n\x02DG 12:06 10/18/08=131 B D+ + VV @0A1F3\x03\r\n\x02DG 12:07 10/29/08=130 F N  + VV @0D417\x03\r\n\x02DG 08:30 11/01/08=100 B D+ + VV @045AA\x03\r\n\x02DG 11:59 11/01/08=138 B L+ + VV @0959C\x03\r\n\x02DG 00:00 11/02/08=168 B L+ + VV @07703\x03\r\n\x02DG 11:59 11/03/08=096 B L- + VV @0F163\x03\r\n\x02DG 12:00 11/06/08=050 B L+ + VV @0976C\x03\r\n\x02DG 23:59 11/09/08=020 B L- + VV @0AD00\x03\r\n\x02DG 05:48 11/10/08=019 B L+ + VV @0B1AD\x03\r\n\x02DG 07:49 11/12/08=501 F N  - VV @061D8\x03\r\n\x02DG 09:50 11/12/08=501 B B+ - VV @3F5DA\x03\r\n\x02DG 10:51 11/12/08=500 B B- + VV @0FD7F\x03\r\n\x02DG 11:52 11/13/08=138 B B+ - vv @547FA\x03\r\n\x02DG 13:53 11/13/08=105 B B- + VV @0AAA7\x03\r\n\x02DG 10:51 11/14/08=500 B B- + VV @04E17\x03\r\n\x02DG 17:52 11/14/08=138 B L+ - VV @03840\x03\r\n\x02DG 11:54 11/15/08=200 B L+ - VV @0859B\x03\r\n\x02DG 05:05 11/15/08=133 C N  + VV @09204\x03\r\n\x02DG 12:04 11/16/08=076 B D+ + VV @08810\x03\r\n\x02DG 12:05 11/16/08=132 B D- + VV @02B57\x03\r\n\x02DG 12:06 11/18/08=131 B D+ + VV @0FAB7\x03\r\n\x02DG 12:07 11/19/08=130 B B- + VV @0ADB4\x03\r\n\x02DG 08:30 12/01/08=100 B B+ + VV @040B4\x03\r\n\x02DG 11:59 12/01/08=138 F N  + VV @08CA5\x03\r\n\x02DG 00:00 12/02/08=168 B B+ + VV @01B34\x03\r\n\x02DG 11:59 12/03/08=096 B B- + VV @09D54\x03\r\n\x02DG 12:00 12/06/08=050 B B+ + VV @0FB5B\x03\r\n\x02DG 23:59 12/09/08=020 B N  + vV @075C1\x03\r\n\x02DG 05:48 12/10/08=019 B N  + VV @0ADE2\x03\r\n\x02DG 07:49 12/12/08=501 C N  - VV @00C37\x03\r\n\x02DG 09:50 12/12/08=501 B N  - VV @05AF5\x03\r\n\x02DG 10:51 12/12/08=500 B D- + VV @0F861\x03\r\n\x02DG 11:52 12/13/08=138 B D+ - VV @0FA8B\x03\r\n\x02DG 13:53 12/13/08=105 B D- + VV @0AFB9\x03\r\n\x02DG 10:51 12/14/08=500 B D- + VV @04B09\x03\r\n\x02DG 17:52 12/14/08=138 B D+ - VV @0BCA5\x03\r\n\x02DG 11:54 12/15/08=200 B D+ - VV @0017E\x03\r\n\x02DG 05:05 12/15/08=133 C N  + VV @07FC8\x03\r\n\x02DG 12:04 12/16/08=076 B L+ + VV @00CF5\x03\r\n\x02DG 12:05 12/16/08=132 B L- + VV @0AFB2\x03\r\n\x02DG 12:06 12/18/08=131 B L+ + VV @07E52\x03\r\n\x02DG 12:07 12/19/08=130 B L- + VV @0C183\x03\r\n\x02DG 08:30 01/01/09=100 B L+ + VV @0D01F\x03\r\n\x02DG 11:59 01/01/09=138 B L+ + VV @06900\x03\r\n\x02DG 00:00 01/02/09=168 B L+ + VV @08B9F\x03\r\n\x02DG 11:59 01/03/09=096 B L- + VV @00DFF\x03\r\n\x02DG 12:00 01/06/09=050 B L+ + VV @3596B\x03\r\n\x02DG 23:59 01/09/09=020 B D- + VV @038B5\x03\r\n\x02DG 05:48 01/10/09=019 B D+ + VV @02418\x03\r\n\x02DG 07:49 01/12/09=501 C N  - VV @01D67\x03\r\n\x02DG 09:50 01/12/09=501 B D+ - vv @21FDF\x03\r\n\x02DG 10:51 01/12/09=500 B D- + VV @0E931\x03\r\n\x02DG 11:52 01/13/09=138 B D+ - VV @0EBDB\x03\r\n\x02DG 13:53 01/13/09=105 B D- + VV @0BEE9\x03\r\n\x02DG 10:51 01/14/09=500 B B- + VV @0B28B\x03\r\n\x02DG 17:52 01/14/09=138 B B+ - VV @04527\x03\r\n\x02DG 11:54 01/15/09=200 B B+ - VV @0F8FC\x03\r\n\x02DG 05:05 01/15/09=133 C N  + VV @06E98\x03\r\n\x02DG 12:04 01/16/09=076 B B+ + VV @09C5E\x03\r\n\x02DG 12:05 01/16/09=132 B B- + VV @21C0B\x03\r\n\x02DG 12:06 01/18/09=131 B B+ + VV @0EEF9\x03\r\n\x02DG 12:07 01/19/09=130 B B- + VV @05128\x03\r\n\x02DG 08:30 02/01/09=100 B B+ + VV @0BC28\x03\r\n\x02DG 11:59 02/01/09=138 B N  + VV @0754F\x03\r\n\x02DG 00:00 02/02/09=168 B N  + VV @097D0\x03\r\n\x02DG 11:59 02/03/09=096 B N  + VV @0B5AF\x03\r\n\x02DG 12:00 02/06/09=050 B N  + VV @077BF\x03\r\n\x02DG 23:59 02/09/09=020 B B- + VV @03DAB\x03\r\n\x02DG 05:48 02/10/09=019 B B+ + VV @02106\x03\r\n\x02DG 07:49 02/12/09=501 C N  - VV @0F0AB\x03\r\n\x02DG 09:50 02/12/09=501 B B+ - VV @0D611\x03\r\n\x02DG 10:51 02/12/09=500 B B- + VV @0EC2F\x03\r\n\x02DG 11:52 02/13/09=138 B L+ - vV @22CBD\x03\r\n\x02DG 13:53 02/13/09=105 B L- + VV @03A0C\x03\r\n\x02DG 10:51 02/14/09=500 B L- + VV @0DEBC\x03\r\n\x02DG 17:52 02/14/09=138 B D+ - VV @04039\x03\r\n\x02DG 11:54 02/15/09=200 B D+ - VV @0FDE2\x03\r\n\x02DG 05:05 02/15/09=133 C N  + VV @08354\x03\r\n\x02DG 12:04 02/16/09=076 B D+ + VV @09940\x03\r\n\x02DG 12:05 02/16/09=132 B D- + VV @03A07\x03\r\n\x02DG 12:06 02/18/09=131 B D+ + VV @0EBE7\x03\r\n\x02DG 12:07 02/19/09=130 B D- + VV @05436\x03\r\n\x02DG 08:30 10/01/09=100 B L+ + VV @08F20\x03\r\n\x02DG 11:59 10/01/09=138 B L+ + VV @0363F\x03\r\n\x02DG 00:00 10/02/09=168 B L+ + VV @0D4A0\x03\r\n\x02DG 11:59 10/03/09=096 B L- + VV @052C0\x03\r\n\x02DG 12:00 10/06/09=050 B N  + VV @0C54C\x03\r\n\x02DG 23:59 10/09/09=020 B N  + VV @05B3F\x03\r\n\x02DG 05:48 10/10/09=019 B N  + VV @0E38D\x03\r\n\x02DG 07:49 10/12/09=501 C N  - VV @04258\x03\r\n\x02DG 09:50 10/12/09=501 B B+ - VV @064E2\x03\r\n\x02DG 10:51 10/12/09=500 B B- + VV @05EDC\x03\r\n\x02DG 11:52 10/13/09=138 B B+ - Vv @6B653\x03\r\n\x02DG 13:53 10/13/09=105 B B- + VV @00904\x03\r\n\x02DG 10:51 10/14/09=500 B B- + VV @0EDB4\x03\r\n\x02DG 17:52 10/14/09=138 B B+ - VV @01A18\x03\r\n\x02DG 11:54 10/15/09=200 B D+ - VV @04F11\x03\r\n\x02DG 05:05 10/15/09=133 C N  + VV @031A7\x03\r\n\x02DG 12:04 10/16/09=076 B L+ + VV @0429A\x03\r\n\x02DG 12:05 10/16/09=132 B L- + VV @0E1DD\x03\r\n\x02DG 12:06 10/18/09=131 B L+ + VV @0303D\x03\r\n\x02DG 12:07 10/19/09=130 B L- + VV @08FEC\x03\r\n\x02DG 12:07 10/19/09=130 B L- + VV @08FEC\x03\r\n\x02DP 08:10 11/12/09=2FC0\x03\r\n\x02DC 08:15 11/12/09=00:00 01/01/006B8B\x03\r\n\x02DG 08:30 11/12/09=100 B D+ + VV @04DA4\x03\r\n\x02DG 11:59 11/19/09=138 B D+ + VV @01E7E\x03\r\n\x02DG 00:00 11/26/09=168 B D+ + VV @061D6\x03\r\n\x02DG 11:59 12/03/09=096 B D- + VV @08D61\x03\r\n\x02DG 12:00 12/06/09=050 B D+ + VV @0EB6E\x03\r\n\x02DG 10:51 12/08/09=300 B D- + VV @04821\x03\r\n\x02DG 23:59 12/09/09=020 B D- + VV @0D102\x03\r\n\x02DG 05:48 12/10/09=019 B B+ + VV @0257D\x03\r\n\x02DG 07:49 12/12/09=501 C N  - vV @09441\x03\r\n\x02DG 09:50 12/12/09=501 B B+ - VV @0D26A\x03\r\n\x02DG 10:51 12/12/09=500 B B- + VV @0E854\x03\r\n\x02DG 11:52 12/13/09=138 B B+ - VV @0EABE\x03\r\n\x02DG 13:53 12/13/09=105 B B- + VV @0BF8C\x03\r\n\x02DG 10:51 12/14/09=500 B B- + VV @05B3C\x03\r\n\x02DG 17:52 12/14/09=138 B L+ - vV @73945\x03\r\n\x02DG 11:54 12/15/09=200 B L+ - VV @090B0\x03\r\n\x02DG 05:05 12/15/09=133 C N  + VV @0872F\x03\r\n\x02DG 12:04 12/16/09=076 B D+ + VV @09D3B\x03\r\n\x02DG 12:05 12/16/09=132 B D- + VV @03E7C\x03\r\n\x02DG 12:06 12/18/09=131 B D+ + VV @0EF9C\x03\r\n\x02DG 12:07 12/19/09=130 B N  + VV @06CF8\x03\r\n\x02DE 12:58 12/20/09=1-0397D7\x03\r\n\x02DG 21:08 12/20/09=129 B N  + VV @0D163\x03\r\n\x02DG 11:09 12/21/09=019 C N  - VV @078FC\x03\r\n\x02DG 11:10 12/21/09=127 B N  - VV @36666\x03\r\n\x02DG 11:11 12/22/09=126 B N  + VV @07926\x03\r\n\x02DG 11:12 12/23/09=125 B N  - VV @06361\x03\r\n\x02DG 11:13 12/24/09=124 B N  + VV @0930A\x03\r\n\x02DG 11:14 12/25/09=499 B D+ - VV @09EA6\x03\r\n\x02DG 12:30 12/26/09=074 B D+ + Vv @00748\x03\r\n\x02DG 12:32 12/27/09=021 B D+ + VV @0F627\x03\r\n\x02DG 12:33 12/28/09=077 B D- + VV @059FB\x03\r\n\x02DG 12:34 12/29/09=078 F N  + VV @07F59\x03\r\n\x02DG 21:34 12/29/09=201 B B+ + VV @05970\x03\r\n\x02DG 07:30 12/30/09=100 B B+ + VV @09199\x03\r\n\x02DG 11:44 12/30/09=150 B B+ + VV @0CD5D\x03\r\n\x02DG 12:29 12/31/09=073 C N  + VV @0D60E\x03\r\n\x02DG 11:45 12/31/09=100 B L- + VV @0ED0E\x03\r\n\x02DC 00:00 01/01/10=23:58 12/31/070738\x03\r\n\x02DG 08:46 01/01/10=050 B D+ + VV @05C8D\x03\r\n\x02DG 11:47 01/01/10=020 B D- + VV @0B053\x03\r\n\x02DG 07:48 01/02/10=019 B D+ + VV @0369A\x03\r\n\x02DG 11:48 01/02/10=119 B D+ + VV @0BF94\x03\r\n\x02DG 19:46 01/03/10=050 B N  + VV @04F38\x03\r\n\x02DG 20:47 01/03/10=020 B N  + VV @08580\x03\r\n\x02DG 11:48 01/04/10=150 F N  + vv @6A2E4\x03\r\n\x02DG 12:00 01/04/10=168 B N  - VV @2A298\x03\r\n\x02DG 11:59 04/03/10=096 B D- + VV @0CE72\x03\r\n\x02DG 12:00 04/06/10=050 B D+ + VV @0A87D\x03\r\n\x02DG 10:51 04/08/10=300 B D- + VV @00B32\x03\r\n\x02DG 23:59 04/09/10=020 B D- + VV @09211\x03\r\n\x02DG 05:48 04/10/10=019 B B+ + VV @0666E\x03\r\n\x02DG 07:49 04/12/10=501 C N  - vV @0D752\x03\r\n\x02DG 09:50 04/12/10=501 B B+ - VV @09179\x03\r\n\x02DG 10:51 04/12/10=500 B B- + VV @0AB47\x03\r\n\x02DG 11:52 04/13/10=138 B B+ - VV @0A9AD\x03\r\n\x02DG 13:53 04/13/10=105 B B- + VV @0FC9F\x03\r\n\x02DG 10:51 04/14/10=500 B B- + VV @0182F\x03\r\n\x02DG 17:52 04/14/10=138 B L+ - vV @77A56\x03\r\n\x02DG 11:54 04/15/10=200 B L+ - VV @0D3A3\x03\r\n\x02DG 05:05 04/15/10=133 C N  + VV @0C43C\x03\r\n\x02DG 12:04 04/16/10=076 B D+ + VV @0DE28\x03\r\n\x02DG 12:05 04/16/10=132 B D- + VV @07D6F\x03\r\n\x02DG 12:06 04/18/10=131 B D+ + VV @0AC8F\x03\r\n\x02DG 12:07 04/19/10=130 B N  + VV @02FEB\x03\r\n\x02DE 12:58 04/20/10=1-032F11\x03\r\n\x02DG 21:08 04/20/10=129 B N  + VV @09270\x03\r\n\x02DG 11:09 04/21/10=019 C N  - VV @03BEF\x03\r\n\x02DG 11:10 04/21/10=127 B N  - VV @32575\x03\r\n\x02DG 11:11 04/22/10=126 B N  + VV @03A35\x03\r\n\x02DG 11:12 04/23/10=125 B N  - VV @02072\x03\r\n\x02DG 11:13 04/24/10=124 B N  + VV @0D019\x03\r\n\x02DG 11:14 04/25/10=499 B D+ - VV @0DDB5\x03\r\n\x02DG 12:30 04/26/10=074 B D+ + Vv @0445B\x03\r\n\x02DG 12:32 04/27/10=021 B D+ + VV @0B534\x03\r\n\x02DG 12:33 04/28/10=077 B D- + VV @01AE8\x03\r\n\x02DG 12:34 04/29/10=078 F N  + VV @03C4A\x03\r\n\x02DG 21:34 04/29/10=201 B B+ + VV @01A63\x03\r\n\x02DG 07:30 04/30/10=100 B B+ + VV @0D28A\x03\r\n\x02DG 11:44 04/30/10=150 B B+ + VV @08E4E\x03\r\n\x02DG 08:46 05/01/10=050 B D+ + VV @0398C\x03\r\n\x02DG 11:47 05/01/10=020 B D- + VV @0D552\x03\r\n\x02DG 07:48 05/02/10=019 B D+ + VV @0539B\x03\r\n\x02DG 11:48 05/02/10=119 B D+ + VV @0DA95\x03\r\n\x02DG 19:46 05/03/10=050 B N  + VV @02A39\x03\r\n\x02DG 20:47 05/03/10=020 B N  + VV @0E081\x03\r\n\x02DG 11:48 05/04/10=150 F N  + vv @6C7E5\x03\r\n\x02DG 12:00 05/04/10=168 B N  - VV @2C799\x03\r\n\x02DE 12:58 05/04/10=7-038675\x03\r\n\x02DE 12:58 05/04/10=7-06D1D8\x03\r\n\x02DG 12:00 05/05/10=250 B N  - VV @25DC9\x03\r\n\x02DE 16:02 05/10/10=6-02047E\x03\r\n\x02DE 16:02 05/10/10=1-0161C4\x03\r\n\x02DP 15:47 05/11/10=EE34\x03\r\n\x02DE 15:47 05/11/10=1-0165F0\x03\r\n\x02DE 15:48 05/11/10=6-02A1FF\x03\r\n\x02DE 15:49 05/11/10=1-018C6F\x03\r\n\x02DE 14:44 05/12/10=6-02F6D4\x03\r\n\x02DE 14:44 05/12/10=1-01936E\x03\r\n\x02DE 15:45 05/12/10=6-024619\x03\r\n\x02DE 15:46 05/12/10=1-01FBDD\x03\r\n\x02DE 15:52 05/12/10=6-0238CE\x03\r\n\x02DE 15:53 05/12/10=1-01155E\x03\r\n\x02DE 09:02 05/14/10=6-02AD0A\x03\r\n\x02DE 09:02 05/14/10=1-01C8B0\x03\r\n\x02DE 04:28 05/24/10=6-02F073\x03\r\n\x02DE 04:29 05/24/10=1-01DDE3\x03\r\n\x02DE 04:29 05/24/10=6-02B859\x03\r\n\x02DP 04:28 05/24/10=E95C\x03\r\n\x02DE 04:28 05/24/10=1-0195C9\x03\r\n\x02DP 01:05 09/07/10=FF22\x03\r\n\x02DE 01:05 09/07/10=1-013282\x03\r\n'
     
     res = AnalyseIntuityOnQPogo(data)

     print res


