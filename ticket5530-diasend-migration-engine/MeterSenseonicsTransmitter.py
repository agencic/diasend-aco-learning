# encoding: utf-8
# -----------------------------------------------------------------------------
# Copyright (C) 2015 Diasend AB, Sweden, http://www.diasend.com
# Developed by PuffinPack, Sweden, http://www.puffinpack.se
# -----------------------------------------------------------------------------

# Supported devices:
# @DEVICE Senseonics Transmitter

from SenseonicsCommon import *
from Generic import *


# --- controller


class SenseonicsDataExtractor(object):

    def __init__(self, memory):
        self.__memory  = memory
        self.__version = None
        # Store timezone changes to be able to calc local time of all records
        self.__tzs = None

    def get_local_time(self, in_dt):
        """
            Given a date time, check the timezone and timezone changes to
            determine the local time of the datetime
        """
        final_td = None
        for dt in self.__tzs:
            if dt <= in_dt:
                final_td = self.__tzs[dt]
            else:
                break

        return in_dt + final_td

    def __get_timedelta(self, offset, sign):
        """
            Given a senseonics time zone offset and time zone offset sign
            return a timedelta

            Offset:
            Hour (5 bits, bits 11-15);
            Minutes (6 bits, bits 5-10);
            Seconds (5 bits, bits 0-4)
        """
        hours = offset >> 11
        minutes = (offset >> 5) & 0x3f
        seconds = offset & 0x0f
        delta = timedelta(hours=hours, minutes=minutes, seconds=seconds)
        if sign.to_text() == 'negative' or sign.to_text() == 'negative1':
            delta = -delta

        return delta

    def __prepare_tz(self, patient_settings, tz_changes):
        """
            Prepare a structure for timezone changed
            An ordered struct with the current timedelta at given times
        """
        tzs = {}
        if not tz_changes:
            # No changes, consider the current zone being used since beginning
            # of time
            dt = datetime(MINYEAR, 1, 1)
            delta = self.__get_timedelta(patient_settings.timezone_offset,
                                         patient_settings.timezone_offset_sign)
            tzs[dt] = delta
        else:
            oldest = None

            for tz_change in tz_changes:
                # Validate the checksum....
                if not tz_change.validate_crc():
                    raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                        'SenseonicsDataExtractor : Invalid CRC of tz change')

                dt = tz_change.get_utc_datetime()
                if not oldest or dt < oldest.get_utc_datetime():
                    oldest = tz_change

                from_delta = self.__get_timedelta(tz_change.old_tz_offset,
                                                  tz_change.old_tz_offset_sign)

                to_delta = self.__get_timedelta(tz_change.new_tz_offset,
                                                tz_change.new_tz_offset_sign)
                # Senseonics had some kind of "snafu" in the device where
                # 0xff and 0x55 of the sign means negative, sometimes we
                # see a timezone change from 0xff->0x55
                # But this is no change since both means negative..
                # Avoid insert changes for these.
                if from_delta != to_delta:
                    tzs[dt] = to_delta

            dt = datetime(MINYEAR, 1, 1)
            delta = self.__get_timedelta(oldest.old_tz_offset,
                                         oldest.old_tz_offset_sign)
            tzs[dt] = delta

        self.__tzs = OrderedDict(sorted(tzs.items()))

    def __split_chunk(self, addr, klass, crc_func):
        out = []
        buf = Buffer(self.__memory[addr])
        while len(buf) - buf.index > klass.record_length() and \
              buf.peek_slice(3) != "\xff\xff\xff":

            create_obj = getattr(klass, "create_obj", None)
            if callable(create_obj):
                obj = create_obj(buf)
            else:
                obj = klass(buf)
            obj.set_crc_func(crc_func)
            obj.set_extractor(self)
            out.append(obj)

        return out

    def split(self):
        records = []
        buf = Buffer(self.__memory[0] + self.__memory[0x1000] + self.__memory[0x2000])
        production_mode = ProductionModeRegisters(buf)
        records.append(production_mode)

        buf.index = PassThroughRegisters.address
        pass_through = PassThroughRegisters(buf)
        records.append(pass_through)

        buf.index = PatientSettingRegisters.address
        patient_settings = PatientSettingRegisters(buf)
        records.append(patient_settings)

        buf.index = GlucoseAlarmAlertRegisters.address
        glucose_alarm_alert = GlucoseAlarmAlertRegisters(buf)
        records.append(glucose_alarm_alert)

        buf.index = MathParameters.address
        math_parameters = MathParameters(buf)
        records.append(math_parameters)

        buf.index = LogRegisters.address
        log_registers = LogRegisters(buf)
        records.append(log_registers)

        # Create one and attach it to all objects, saves a lot of time!
        crc_f = crcmod.predefined.mkCrcFun('crc-8-maxim')

        tz_changes = []

        for addr in self.__memory:
            # First three chunks were handled above:
            if addr > 0x2000:
                if addr >= ADDR_SENSOR_GLUC_DATA_LOG_START:
                    recs = self.__split_chunk(addr, SensorGlucoseRecordCrc, crc_f)
                elif addr >= ADDR_MISC_EVENT_LOG_START:
                    recs = self.__split_chunk(addr, MiscEventRecord, crc_f)
                    # For now we just keep timezone changes...
                    for rec in recs:
                        if isinstance(rec, MiscTimeZoneChangeRecord):
                            tz_changes.append(rec)

                    recs = None
                elif addr >= ADDR_PATIENT_EVENT_LOG_START:
                    recs = self.__split_chunk(addr, PatientEventRecordCrc, crc_f)
                elif addr >= ADDR_BLOOD_GLUC_DATA_LOG_START:
                    recs = self.__split_chunk(addr, BloodGlucoseRecordCrc, crc_f)

                if recs:
                    records.extend(recs)

        # Scrap the memory to save memory :-)
        self.__memory = None
        self.__prepare_tz(patient_settings, tz_changes)
        return records

def EvalSenseonicsTransmitterSerialRecord(obj):
    """
    Is this line a serial record. If so, return a dictionary with serial
    number.
    """
    if isinstance(obj, PingResponse):
        return {ELEM_DEVICE_SERIAL:'%d' % (obj.id)}
    else:
        return {}

def EvalSenseonicsTransmitterModelRecord(obj):
    """
    Detect the model by looking at the serial record
    In some cases, the exact model will not be known - and will be detected when
    the serial number is decoded
    """
    if isinstance(obj, ProductionModeRegisters):
        #TODO: which are the models? 
        if obj.model == 101567:
            # Capri
            pass
        elif obj.model == 101743 or obj.model == 101743005:
            # Novara 1.0 and Novara 2.2
            return {ELEM_DEVICE_MODEL: DEVICE_MODEL_TRANSMITTER}

    return {}

def EvalSenseonicsTransmitterChecksumRecord(obj, record):
    """
    Valid the 1-wire checksum of the object
    """
    if isinstance(obj, SenseonicsRecord):
        return obj.validate_crc()
    else:
        return False

# -----------------------------------------------------------------------------
# THESE FUNCTIONS SHOULD BE CALLED FROM OUTSIDE THIS MODULE
# -----------------------------------------------------------------------------

def DetectMeterSenseonicsTransmitter( inList ):
    """
    Detect if data comes from a Senseonics transmitter.
    """
    return DetectDevice( 'Senseonics Transmitter', inList, DEVICE_METER )

def AnalyseMeterSenseonicsTransmitter( inData ):
    """
    Analyse Senseonics transmitter.
    """

    # Empty dictionary
    callbacks = {
        "eval_serial_record"        : EvalSenseonicsTransmitterSerialRecord,
        "eval_device_model_record"  : EvalSenseonicsTransmitterModelRecord,
        "eval_unit"                 : EvalSenseonicsUnitRecord,
        "eval_result_record"        : EvalSenseonicsResultRecord,
        "eval_checksum_record"      : EvalSenseonicsTransmitterChecksumRecord,
    }


    # The data we get is basically a dump of the internal memory
    # With gaps, we dump the first 12K, then the last parts of the
    # different logs...
    memory_chunks = {}

    # CRC CITT is used
    crc_func = crcmod.predefined.mkCrcFun('crc-ccitt-false')
    prev_addr = None

    objects = []
    index = 0
    while index < len(inData):
        # first we get the request from the diasend transmitter..
        (start,length) = unpack("<BH", inData[index:index + 3])
        if start != ST_START_BYTE:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                            'Invalid startbyte', hex(start))
        # Skip start byte
        index += 1
        calc_crc = crc_func(inData[index:index + length + 2])
        # Skip the length
        index += 2
        (read_crc,) = unpack("<H", inData[index + length:index + length + 2])
        if calc_crc != read_crc:
            raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                            'Invalid CRC 0x%04x != 0x%04x' % \
                             (read_crc, calc_crc))

        cmd = ord(inData[index])
        if cmd == ST_CMD_READ_BLOCK:
            # Block read is followed by the nice THREE!! bytes address LE...
            prev_addr = ord(inData[index + 1]) | ord(inData[index + 2]) << 8 | \
                        ord(inData[index +3]) << 16
        elif cmd == (ST_CMD_READ_BLOCK | ST_RSP_BIT):
            if prev_addr == None:
                raise Exception(ERROR_CODE_PREPROCESSING_FAILED,
                                'Block response without request?')

            memory_chunks[prev_addr] = inData[index + 1:index + length]
            prev_addr = None
        elif cmd == ST_CMD_PING or cmd == ST_CMD_READ32 or \
             cmd == (ST_CMD_READ32 | ST_RSP_BIT):
            # All the info is in the memory blocks, just skip the ping
            pass
        else:
            objects.append(SenseonicsResponse.create_obj(Buffer(inData[index:index + length])))

        # Skip the payload and the CRC
        index += length + 2

    try:
        extractor = SenseonicsDataExtractor(memory_chunks)
        objects.extend(extractor.split())

    except Exception, e:

        import traceback
        traceback.print_exc()

        # TODO: Get real serial number
        serial_number = 'UNKNOWN'

        # The 'standard' way is to raise Exception with two parameters, diasend error code and
        # description.
        if len(e.args) == 2:
            error_response = CreateErrorResponseList(DEVICE_MODEL_TRANSMITTER,
                                                     serial_number, e.args[0],
                                                     e.args[1])
        # If not we probably got an exception from the standard library. Simply concenate all arguments into
        # a description and give it a default diasend error code.
        else:
            error_response = CreateErrorResponseList(DEVICE_MODEL_TRANSMITTER,
                                                     serial_number,
                                                     ERROR_CODE_PREPROCESSING_FAILED,
                                                     ''.join(map(str, e.args)))

        return error_response

    return AnalyseGenericMeter(objects, callbacks)

if __name__ == "__main__":

#    testfile = open('test/testcases/test_data/SenseonicsTransmitter/SenseonicsTransmitter-de0291-full.bin')
    testfile = open('test/testcases/test_data/SenseonicsTransmitter/SenseonicsTransmitter-de0290.bin')
    testcase = testfile.read()
    testfile.close()
    
    ret = AnalyseMeterSenseonicsTransmitter(testcase)
#    print ret
